<?php

namespace app\models;

use common\models\Config;
use Yii;

/**
 * This is the model class for table "role".
 *
 * @property string $id
 * @property string $name
 * @property string $createTime
 * @property string $updateTime
 */
class WeixinJs extends \yii\db\ActiveRecord
{

  private $appId;
  private $appSecret;

    /**
     * @inheritdoc
     */
    // public static function tableName()
    // {
    //     return 'WeixinJs';
    // }
  public function __construct() {
    $config= Config::find()->where(["cKey"=>'weixin'])->one();
    $model = json_decode($config->cValue);

    $this->appId = $model->appid;
    $this->appSecret = $model->appsecret;
 
  }

  
  public function getSignPackage() {
    $jsapiTicket = $this->getJsApiTicket();

    // 注意 URL 一定要动态获取，不能 hardcode.
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    $timestamp = time();
    $nonceStr = $this->createNonceStr();

    // 这里参数的顺序要按照 key 值 ASCII 码升序排序
    $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";

    $signature = sha1($string);

    $signPackage = array(
      "appId"     => $this->appId,
      "nonceStr"  => $nonceStr,
      "timestamp" => $timestamp,
      "url"       => $url,
      "signature" => $signature,
      "rawString" => $string
    );
    return $signPackage; 
  }

  private function createNonceStr($length = 16) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = "";
    for ($i = 0; $i < $length; $i++) {
      $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
    }
    return $str;
  }

  private function getJsApiTicket() {
    // jsapi_ticket 应该全局存储与更新，以下代码以写入到文件中做示例
    // $data = json_decode($this->get_php_file("jsapi_ticket.php"));
    if (empty($_SESSION['jsapi_ticket'])||intval($_SESSION['expire_time_jsapi']) < time()) {
      $accessToken = $this->getAccessToken();
      // 如果是企业号用以下 URL 获取 ticket{"jsapi_ticket":"","expire_time":0}
      // $url = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=$accessToken";
      $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
      $res = $this->GetObject($url);
      $ticket =$res['ticket'];
      if ($ticket) {
        // $data->expire_time = time() + 7000;
        // $data->jsapi_ticket = $ticket;
        $_SESSION['expire_time_jsapi'] = time() + 7000;
        $_SESSION['jsapi_ticket'] = $ticket;
        //$this->set_php_file("jsapi_ticket.php", json_encode($data));
      }
    } else {
      $ticket = $_SESSION['jsapi_ticket'];
    }

    return $ticket;
  }

  private function getAccessToken() {
    // access_token 应该全局存储与更新，以下代码以写入到文件中做示例
    // if (empty($_SESSION['access_token'])) {
     
    // }
    // $data = json_decode('{"access_token":"","expire_time":0}');
    if (empty($_SESSION['access_token'])||intval($_SESSION['expire_time']) < time()) {
      // 如果是企业号用以下URL获取access_token
      // $url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=$this->appId&corpsecret=$this->appSecret";
      $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$this->appId&secret=$this->appSecret";
    
      $res = $this->GetObject($url);
      $access_token = $res['access_token'];

      if ($access_token) {
        $_SESSION['expire_time'] = time() + 7000;
        $_SESSION['access_token'] = $access_token;
        // $this->set_php_file("access_token.php", json_encode($data));
      }
    } else {
      $access_token = $_SESSION['access_token'];
    }
    return $access_token;
  }

  private function httpGet($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 500);
    // 为保证第三方服务器与微信服务器之间数据传输的安全性，所有微信接口采用https方式调用，必须使用下面2行代码打开ssl安全校验。
    // 如果在部署过程中代码在此处验证失败，请到 http://curl.haxx.se/ca/cacert.pem 下载新的证书判别文件。
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);
    curl_setopt($curl, CURLOPT_URL, $url);

    $res = curl_exec($curl);
    curl_close($curl);

    return $res;
  }
  public function GetObject($url)
  {
    
    $ch = curl_init();
    //设置超时
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,FALSE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    //运行curl，结果以jason形式返回
    $res = curl_exec($ch);
    curl_close($ch);
    //取出openid
    $data = json_decode($res,true);
    return $data;
  }
  private function get_php_file($filename) {
    return trim(substr(file_get_contents($filename), 15));
  }

  private function set_php_file($filename, $content) {
    $fp = fopen($filename, "w");
    fwrite($fp, "<?php exit();?>" . $content);
    fclose($fp);
  }


}
