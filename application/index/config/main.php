<?php
$params = array_merge(
    require(ROOT_PATH . '/common/config/params.php'),
    require(ROOT_PATH . '/common/config/params-local.php'),
    require(__DIR__ . '/params.php')
);

return [
    'id' => 'app-pc',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => MODULE_NAME . '\controllers',
    'defaultRoute' => 'index',
    'components' => [
         'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'enableStrictParsing'=>false, 
            //'suffix'=>'.html',
            //'baseUrl' => '/',
            
            'showScriptName' => false,
            'rules' => [

            //'index'=>'index/index',
            'register'=>"member/reg",
            'login'=>'member/login',
              '<SecretCityId:\w+>' => 'index/index',


             // 'shop/<id:\d+>'=>'shop/index',
//            '<controller:\w+>/<action:\w+>_<cid:\w+>/<enid:\w+>'=>'<controller>/index',
//            '<controller:\w+>/index'=>'<controller>/index',
//            '<controller:\w+>/<id:\d+>'=>'<controller>/view',

//			'<cityId:\d+>'=>'index/index',
                [
                    'class' => 'app\models\CarUrlRule',
                ],

			//'jz/index_<ticketId:\d+>'=>'jia-zhuang/index',
			//'jz/index2_<ticketId:\d+>'=>'jia-zhuang/index2',
//			'jz/jingxuan_<ticketId:\d+>'=>'jia-zhuang/jing-xuan',
//			'jz/yuyue_<ticketId:\d+>'=>'jia-zhuang/yu-yue',
//			'jz/youhui_<ticketId:\d+>'=>'jia-zhuang/you-hui',
//			'jz/brand_<ticketId:\d+>'=>'jia-zhuang/brand-activity',

//			'jz/index'=>'jia-zhuang/index',
			//'jz/index2'=>'jia-zhuang/index2',
//			'jz/jingxuan'=>'jia-zhuang/jing-xuan',
//			'jz/yuyue'=>'jia-zhuang/yu-yue',
//			'jz/youhui'=>'jia-zhuang/you-hui',
//			'jz/brand'=>'jia-zhuang/brand-activity',

			'jh/index_<ticketId:\d+>'=>'jie-hun/index',
			'jh/index2_<ticketId:\d+>'=>'jie-hun/index2',
			'jh/jingxuan_<ticketId:\d+>'=>'jie-hun/jing-xuan',
			'jh/yuyue_<ticketId:\d+>'=>'jie-hun/yu-yue',
			'jh/youhui_<ticketId:\d+>'=>'jie-hun/you-hui',
			'jh/brand_<ticketId:\d+>'=>'jie-hun/brand-activity',
			'jh/index'=>'jie-hun/index',
			'jh/index2'=>'jie-hun/index2',
			'jh/jingxuan'=>'jie-hun/jing-xuan',
			'jh/yuyue'=>'jie-hun/yu-yue',
			'jh/youhui'=>'jie-hun/you-hui',
			'jh/brand'=>'jie-hun/brand-activity',

			'yy/index_<ticketId:\d+>'=>'yun-ying/index',
			'yy/index2_<ticketId:\d+>'=>'yun-ying/index2',
			'yy/jingxuan_<ticketId:\d+>'=>'yun-ying/jing-xuan',
			'yy/yuyue_<ticketId:\d+>'=>'yun-ying/yu-yue',
			'yy/youhui_<ticketId:\d+>'=>'yun-ying/you-hui',
			'yy/brand_<ticketId:\d+>'=>'yun-ying/brand-activity',
			'yy/index'=>'yun-ying/index',
			'yy/index2'=>'yun-ying/index2',
			'yy/jingxuan'=>'yun-ying/jing-xuan',
			'yy/yuyue'=>'yun-ying/yu-yue',
			'yy/youhui'=>'yun-ying/you-hui',
			'yy/brand'=>'yun-ying/brand-activity',

            '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
//            '<controller:[\w|-]+>/<action:[a-zA-Z|\d|-]+>_ticketId<ticketId:\d+>'=>'<controller>/<action>',

			//'jz/<action:[a-zA-Z|\d|-]+>_<ticketId:\d+>'=>'jia-zhuang/<action>',
			'jh/<action:[a-zA-Z|\d|-]+>_<ticketId:\d+>'=>'jie-hun/<action>',
			'yy/<action:[a-zA-Z|\d|-]+>_<ticketId:\d+>'=>'yun-ying/<action>',
             
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\Member',
            'enableAutoLogin' => true,
        ],
        'request' => [
            'enableCsrfValidation' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'aliyun' => [
            'class' => 'saviorlv\aliyun\Sms',
            'accessKeyId' => 'LTAIIVTLKjAyv1oU',
            'accessKeySecret' => 'l5HCoXh6X7KMqfMYUErY5npgg8dqF4'
        ],
        // 'errorHandler' => [
        //     'errorAction' => 'site/error',
        // ],

      'assetManager'=>[
            // 设置存放assets的文件目录位置
           'basePath'=> PUBLIC_PATH . '/web/assets',
           // 设置访问assets目录的url地址
           'baseUrl'=> '/public/web/assets',
            'bundles'=>[
                'yii\web\JqueryAsset'=>[
                    'sourcePath'=>null,
                    'js'=>[]
                ],
            ],
        ],
    ],
    'params' => $params,
];
