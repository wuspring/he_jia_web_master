<?php
$params = array_merge(
    require(ROOT_PATH . '/common/config/params.php'),
    require(ROOT_PATH . '/common/config/params-local.php'),
    require(__DIR__ . '/params.php')
);

return [
    'id' => 'app-wap',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => MODULE_NAME . '\controllers',
    'defaultRoute' => 'index',
    'components' => [
         'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'enableStrictParsing'=>false, 
            'suffix'=>'',
            //'suffix'=>'.html',
            //'baseUrl' => '/',
            
            'showScriptName' => false,
            'rules' => [
            'index'=>'index/index',
            'register'=>"member/reg",
            'login'=>'member/login',
             // 'shop/<id:\d+>'=>'shop/index',
            '<controller:\w+>/<action:\w+>_<cid:\w+>/<enid:\w+>'=>'<controller>/index',
            '<controller:\w+>/index'=>'<controller>/index',
            '<controller:\w+>/<id:\d+>'=>'<controller>/view',

            '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            '<controller:[\w|-]+>/<action:[a-zA-Z|\d|-]+>_ticketId<ticketId:\d+>'=>'<controller>/<action>',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\Member',
            'enableAutoLogin' => true,
        ],
        'request' => [
            'enableCsrfValidation' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'aliyun' => [
            'class' => 'saviorlv\aliyun\Sms',
            'accessKeyId' => 'LTAIIVTLKjAyv1oU',
            'accessKeySecret' => 'l5HCoXh6X7KMqfMYUErY5npgg8dqF4'
        ],
        // 'errorHandler' => [
        //     'errorAction' => 'site/error',
        // ],

      'assetManager'=>[
            // 设置存放assets的文件目录位置
           'basePath'=> PUBLIC_PATH . '/web/assets',
           // 设置访问assets目录的url地址
           'baseUrl'=> '/public/web/assets',
            'bundles'=>[
                'yii\web\JqueryAsset'=>[
                    'sourcePath'=>null,
                    'js'=>[]
                ],
            ],
        ],
    ],
    'params' => $params,
];
