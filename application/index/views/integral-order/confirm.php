<?php

use DL\service\UrlService;
use common\models\Order;
include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>
<script>
    $("body").removeClass('bg_glay');
</script>
<style>
    .order_address_choose {
        border-color: #459329!important;
        background-image: url('/public/index/images/icon_ok1.png')!important;
    }
</style>
<div class="mx-auto w1200 clearfix casesure">
    <p class="case_title"><span class="green">订单确认</span></p>
    <div class="casesure_address">

        <?php foreach ($addresses AS $address) :?>
        <div class="d_list <?= $address->id == $order->receiver ? 'order_address_choose' :''; ?>" data-id="setAddress" data-val="<?= $address->id; ?>">
            <p class="p_name"><span><?= $address->name; ?></span><span><?= $address->mobile; ?></span>
                <?php if ($address->isDefault) :?>
                <span class="s_tag active">默认地址</span>
                <?php endif; ?>
            </p>
            <p class="p_con"><?= "{$address->provinceInfo} {$address->address}"; ?></p>
            <p class="p_btns text-right"><a href="javascript:void(0)" class="green a_set">设为默认地址</a></p>
        </div>
        <?php endforeach; ?>

        <a href="javascript:void(0)" class="d_list" data-id="addAddress">
            <p class="p_add">+</p>
            <p class="p_addmore">添加新地址</p>
        </a>

    </div>
    <div data-id="modal" style="display: none">
        <p class="casesure_title border-bottom-0 mt-3">填写收货地址</p>
        <form action="" class="user_form border mt-1 pt-4">
            <ul>
                <li class="user_li2">
                    <label for="" class="user_li2_label">所在地区</label>
                    <div class="user_select">
                        <select name="province" id="province">
                            <?php foreach ($provinces as $key => $item):?>
                                <option value="<?=$key?>"><?=$item?></option>
                            <?php endforeach;?>
                        </select>
                        <span class="info"></span>

                        <select id="city"  class="form-control">
                            <option value="">请选择</option>
                        </select>
                        <span class="info"></span>

                        <select id="district"  class="form-control">
                            <option value="">请选择</option>
                        </select>
                        <span class="info"></span>
                    </div>
                </li>
                <li class="user_li2">
                    <label for="" class="user_li2_label">详细地址</label>
                    <textarea class="user_input textarea"  id="info" placeholder="建议您如实填写详细收货地址，例如街道名称，门牌号码，楼层和房间号等消息"></textarea>
                    <span class="info"></span>
                </li>
                <li class="user_li2">
                    <label for="" class="user_li2_label">收货人姓名</label>
                    <input type="text" class="user_input" id="uname" placeholder="请输入收件人姓名">
                    <span class="info"></span>
                </li>
                <li class="user_li2">
                    <label for="" class="user_li2_label">手机号码</label>
                    <input type="text" class="user_input"  id="mobile" placeholder="请输入收件人手机号码">
                    <span class="info"></span>
                </li>
                <li class="user_li2"><a href="javascript:void(0)" class="settlement_but user_btn1">保存收货地址</a></li>
            </ul>
        </form>
    </div>
    <script>
        var modal = $('div[data-id="modal"]');
        $('[data-id="addAddress"]').click(function () {
            modal.show();
            $('#province').change();
        });

        $(function () {
            //选择省
            $('#province').on('change',function () {
                changeProvince();
            });
            //选择市
            $('#city').on('change',function () {
                changeCity();
            });

            function changeProvince() {
                var upid = $('#province').val();
                if(upid>0){
                    $.get("<?=UrlService::build(['address/city'])?>",{upid:upid},function (r) {
                        if(r.code==0){
                            var html = '';
                            $.each(r.data,function (key,val) {
                                html += '<option value="'+ key +'">'+ val +'</option>';
                            });
                            $('#city').html(html);
                            changeCity();
                        }
                    },'json');
                }
            }
            function changeCity() {
                var upid = $('#city').val();
                if(upid>0){
                    $.get("<?=UrlService::build(['address/city'])?>",{upid:upid},function (r) {
                        if(r.code==0){
                            var html = '';
                            $.each(r.data,function (key,val) {
                                html += '<option value="'+ key +'">'+ val +'</option>';
                            });
                            $('#district').html(html);
                        }
                    },'json');
                }
            }

            $(document).on('click','.settlement_but',function () {
                if(checkForm()){
                    var id = $(this).attr('data-id');
                    var province = $('#province').val();
                    var city = $('#city').val();
                    var district = $('#district').val();
                    var info = $('#info').val();
                    var uname = $('#uname').val();
                    var mobile = $('#mobile').val();
                    if(id>0){
                        $.post("<?=UrlService::build(['address/updatem'])?>&id="+id,{
                            id:id,
                            province:province,
                            city:city,
                            region:district,
                            address:info,
                            name:uname,
                            mobile:mobile,
                        },function (r) {
                            if(r.code==0){
                                location.reload();
                            }else{
                                alert(r.msg);
                            }
                        },'json');
                    }else{
                        $.post("<?=UrlService::build(['address/create'])?>",{
                            province:province,
                            city:city,
                            region:district,
                            address:info,
                            name:uname,
                            mobile:mobile,
                        },function (r) {
                            if(r.code==0){
                                location.reload();
                            }else{
                                alert(r.msg);
                            }
                        },'json');
                    }
                }
            });

            $(document).on('click','.address_btn',function () {
                var id = $(this).attr('data-id');
                $('#del-btn').attr('data-id',id);
            });

            $(document).on('click','#btn-edit',function () {
                $('#myModalUpdateAddress').find('.modal-header span.etitle').html('编辑收货人地址');
                var id = $(this).attr('data-id');
                $('.settlement_but').attr('data-id',id);
                if(id){
                    $.get("<?=UrlService::build(['address/updatem'])?>",{id:id},function (r) {
                        if(r.code==0){
                            var cityhtml = districthtml = '';
                            $.each(r.cities,function (key,val) {
                                cityhtml += '<option value="'+ key +'">'+ val +'</option>';
                            });
                            $('#city').html(cityhtml);
                            $.each(r.districts,function (key,val) {
                                districthtml += '<option value="'+ key +'">'+ val +'</option>';
                            });
                            $('#district').html(districthtml);

                            $('#province').val(r.data.province);
                            $('#city').val(r.data.city);
                            $('#district').val(r.data.district);
                            $('#info').val(r.data.info);
                            $('#uname').val(r.data.name);
                            $('#mobile').val(r.data.phone);
                        }
                    },'json');
                }
            });

            $('[data-id="setAddress"]').click(function () {
                var that = $(this),
                    type = that.attr('data-id'),
                    val = that.attr('data-val'),
                    data = {
                        number : '<?= $order->orderid; ?>'
                    };
                data[type] = val;

                $.ajax({
                    url: '<?= UrlService::build('integral-order/update'); ?>',
                    type : 'post',
                    async : false,
                    data : data,
                    dataType:'json',
                    success:function(data) {
                        if (data.status) {
                            window.location.reload();
                            return false;
                        }
                        alert(data.msg);
                    }
                });
            })

            //验证是否通过
            function checkForm() {
                var flag = true;
                var province = $('#province').val();
                var city = $('#city').val();
                var district = $('#district').val();
                $('.input_item .info').html('');
                if(province.length==0){
                    $('#province').next('.info').html('<i class="icon icon_warn_sm"></i> 请选择省');
                    flag = false;
                }
                if(city.length==0){
                    $('#city').next('.info').html('<i class="icon icon_warn_sm"></i> 请选择市');
                    flag = false;
                }
                if(district.length==0){
                    $('#district').next('.info').html('<i class="icon icon_warn_sm"></i> 请选择区');
                    flag = false;
                }
                var info = $('#info').val();
                var uname = $('#uname').val();
                var mobile = $('#mobile').val();

                if(info.length==0){
                    $('#info').next('.info').html('<i class="icon icon_warn_sm"></i> 详细地址必填');
                    flag = false;
                }
                if(uname.length==0){
                    $('#uname').next('.info').html('<i class="icon icon_warn_sm"></i> 收货人必填');
                    flag = false;
                }
                if(mobile.length==0){
                    $('#mobile').next('.info').html('<i class="icon icon_warn_sm"></i> 手机号必填');
                    flag = false;
                }else{
                    var check_phone_number = /^1[34578]\d{9}$/;
                    if (!mobile.match(check_phone_number)) {
                        $('#mobile').next('.info').html('<i class="icon icon_warn_sm"></i> 请输入正确的手机号');
                        flag = false;
                    }
                }
                return flag;
            }

        });
    </script>
    <p class="casesure_title border-bottom-0 mt-3">选择收货时间</p>
    <div class="casesure_type">
        <?php foreach ($expand AS $ex) :?>
        <a href="javascript:void(0)" data-id="setExpand" class="<?= $order->expand==$ex ? 'active' : ''; ?>" data-val="<?= $ex; ?>"><?= $ex; ?></a>
        <?php endforeach; ?>
    </div>
    <p class="casesure_title">商品信息</p>
    <?php foreach ($order->goods AS $good) :?>
    <div class="casesure_product">
        <a href="<?= UrlService::build(['intergral-goods/detail', 'id' => $good->goods_id]); ?>" title="" class="a_img">
            <img src="<?= $good->goods_pic; ?>" alt="<?= $good->goods_name; ?>"></a>
        <p class="p_title"><?= $good->goods_name; ?></p>
        <p class="p_earnest">积分：<?= $good->goods_pay_price; ?></p>
    </div>
    <?php endforeach; ?>

    <div class="casesure_btns">
        <p>提示：下单后进入“<span class="green">我们和家</span>”查看订单，到店消费时出示！</p>

        <a href="<?= UrlService::build(['integral-order/pay', 'number' => $order->orderid]); ?>"  class="btn btn-danger float-right clickSubmitIntergralOrder" >提交订单</a>
        <p class="p">总积分：<span class="orange"><?= $order->order_amount; ?></span></p>
    </div>
</div>


<?php include APP_PATH . '/views/common/footer.php'; ?>

<script type="text/javascript">
    $('[data-id="setPay"]').click(function () {
        var that = $(this),
            type = that.attr('data-id'),
            val = that.attr('data-val'),
            data = {
                number : '<?= $order->orderid; ?>'
            };
        data[type] = val;

        $.ajax({
            url: '<?= UrlService::build('order/update'); ?>',
            type : 'post',
            async : false,
            data : data,
            dataType:'json',
            success:function(data) {
                if (data.status) {
                    window.location.reload();
                    return false;
                }
                alert(data.msg);
            }
        });
    });

    $('[data-id="setExpand"]').click(function () {
        var val = $(this).attr('data-val');
        $.ajax({
            url: '<?= UrlService::build('integral-order/update'); ?>',
            type : 'post',
            async : false,
            data : {
                expand : val,
                number : '<?= $order->orderid; ?>'
            },
            dataType:'json',
            success:function(data) {
                if (data.status) {
                    window.location.reload();
                    return false;
                }
                alert(data.msg);
            }
        });
    });

</script>
