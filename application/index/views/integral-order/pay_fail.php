<?php

use DL\service\UrlService;

include APP_PATH . '/views/common/header.php'; ?>

<div class="mx-auto w1200 clearfix pay_result">
    <p class="case_title"><span class="green">积分支付</span></p>
    <div class="pay_result_con">
        <img src="/public/index/images/img_result_fail.png" alt="订单支付失败" class="img">
        <div class="con">
            <p class="p1 mt-5 pt-4">抱歉，积分支付失败！<?= $msg; ?></p>
        </div>
    </div>
    <div class="pay_result_btns text-right"><a href="<?= UrlService::build(['personal/integral-order']); ?>" class="btn btn-danger">返回订单列表</a></div>


</div>


<?php include APP_PATH . '/views/common/footer.php'; ?>

    
