<?php
use DL\service\UrlService;
use common\models\GoodsClass;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/integral-header.php';
?>

	<div class="mx-auto w1200 clearfix">
		<nav class="breadcrumb com_crumbs">
			<span>当前位置：</span>
			<a class="breadcrumb-item" href="<?= UrlService::build('integral/index'); ?>">积分商城</a>
			<span class="breadcrumb-item active">商品详情</span>
		</nav>
		<div class="integraldetail_top clearfix">
			<div class="idt_left float-left">
				<!-- 最多只能有四个图片，点击切换大图 -->
                <?php
                    $img = [];
                    if (strlen($treasure->goods_image) > 4) {
                        $img = arrayGroupsAction(json_decode($treasure->goods_image), function ($data) {
                            return $data->imgurl;
                        });
                    }
                ?>
                <?php if ($img) :?>
				<img src="<?= reset($img); ?>" alt="" class="img">
				<div class="d_imgs">
                    <?php foreach ($img AS $index => $i) :?>
					<img src="<?= $i; ?>" alt="" class="<?= $index ? '' : 'active'; ?>">
					<?php endforeach; ?>
				</div>
                <?php endif; ?>
			</div>
			<div class="idt_con">
				<h3><?= $oldTreasureAmount>1 ? "(第{$oldTreasureAmount}期)" : ''; ?><?= $treasure->goods_name; ?></h3>
				<p>价值：￥<?= $treasure->goods_marketprice; ?></p>
				<div class="progress">
				  	<div class="progress-bar" style="width:<?php
                    $n = 0;
                    if ($treasure->goods_storage) {
                        $n = (int)($treasure->goods_salenum * 100 / $treasure->goods_storage);
                    }
                    echo $n; ?>%"></div>
				</div>
				<div class="d_more">
					<dl>
						<dt class="orange"><?= $treasure->goods_salenum; ?></dt>
						<dd>已参与人次</dd>
					</dl>
					<dl class="text-center">
						<dt><?= $treasure->goods_storage; ?></dt>
						<dd>总需人次</dd>
					</dl>
					<dl class="text-right">
						<dt><?php
                            $rest = $treasure->goods_storage - $treasure->goods_salenum;
                            echo $rest >0 ? $rest: 0;
                            ?></dt>
						<dd>剩余人次</dd>
					</dl>
				</div>
				<p><span><?= $treasure->goods_integral; ?></span>积分/人次</p>
				<div class="product_nums">
					<span>我要夺宝</span>
					<a href="javascript:void(0)" class="a_num a_reduce" data-id="amountCtrl" data-type="reduce">-</a>
					<input type="text" name="amount" value="1" class="input">
					<a href="javascript:void(0)" class="a_num a_plus" data-id="amountCtrl" data-type="plus">+</a>
					<span>人次</span>
					<span class="s">购买人次越多获得几率越大哦！</span>
				</div>
                <?php if ($treasure->goods_state) :?>
				    <a href="javascript:void(0)" class="btn btn-warning" data-id="getTreasure">立即夺宝</a>
                <?php else :?>
                    <a href="javascript:alert('活动已结束')" class="btn btn-warning" style="background: #ccc;border: solid 1px #ccc;">立即夺宝</a>
                <?php endif; ?>
                <?php if (!$treasure->goods_state) : ?>
				<div class="treasure_result">
					<div class="treasure_result_left">
						<img src="/public/index/images/img_tx_default.png">
						<div class="con">
							<p class="p1">本期获得者</p>
							<p class="p2">中奖号码：<span class="orange"><?php
                                    $keys = arrayGroupsAction($treasure->successLog, function ($o) {
                                        return $o->key;
                                    });
                                    echo implode('、', $keys);?></span></p>
						</div>
					</div>
					<div class="treasure_result_right">
						<p>揭晓时间：<span><?= $treasure->end_time; ?></span></p>
						<p>夺宝时间：<span><?= $treasure->goods_addtime; ?></span></p>
					</div>
				</div>
                <?php endif; ?>
			</div>
		</div>
		<div class="integraldetail_rule">
			<p class="p1">计算规则：</p>
			<p>1.商品的最后一个号码分配完毕后，将公示该分配时间点钱全部积分夺宝商品的最后50个参与记录时间；</p>
			<p>2.将这50个时间的数值求和得出数值A（每个时间按时、分、秒、毫秒的顺序组合，如20:15:24:128则为102524128）；</p>
			<p>3.将该商品所需总人次数记作数值B；</p>
			<p>4.（数值A/数值B）取余数+原始数100000001得到最终幸运号码，拥有该幸运号码者，就能拥有该宝贝啦！</p>
		</div>
		<div class="integraldetail_bottom mt-4">
			<div class="div_title nav nav-tabs">
				<a href="#pro_detail1" data-toggle="tab" class="active">商品介绍</a>
				<a href="#pro_detail2" data-toggle="tab">本期参与记录</a>
				<a href="#pro_detail3" data-toggle="tab">往期揭晓</a>
			</div>		

			<div class="tab-content">
				<!-- 商品介绍	 -->
				<div class="idb_content tab-pane active" id="pro_detail1">
                    <?= $treasure->goods_body; ?>
				</div>	
				<!-- 本期参与记录 -->
				<div class="idb_content tab-pane" id="pro_detail2">
					<p>以下为本期参与记录</p>
					<table class="productdetail_table">
						<tr><th>参与人</th><th>参与时间</th><th>参与人次</th></tr>
                        <?php if ($logs) : foreach ($logs AS $log) :?>
						<tr>
							<td><img src="<?= $log->member->avatar; ?>"><?= $log->member->nickname; ?></td>
							<td><?= $log->create_time; ?></td>
							<td><?= $log->times; ?>人次</td>
						</tr>
                        <?php endforeach;else: ?>
                        <tr>
                            <td colspan="3" style="text-align: center">暂无记录</td>
                        </tr>
                        <?php endif; ?>
					</table>
				</div>

				<!-- 往期揭晓 -->
				<div class="idb_content tab-pane" id="pro_detail3">
					<p>以下为往期揭晓</p>
					<table class="productdetail_table">
						<tr><th>获得者</th><th>中奖号码</th><th>揭晓时间</th><th>夺宝时间</th><th>参与人次</th></tr>
                        <?php if ($treasureHistorys) : foreach ($treasureHistorys AS $history) :?>
						<tr>
							<td>
                                <?php
                                arrayGroupsAction($history->successLog, function ($o) {
                                    echo "<img src='{$o->member->avatar}'>{$o->member->nickname}";
                                });
                                ?>
                            </td>
							<td class="orange"><?php
                                $keys = arrayGroupsAction($history->successLog, function ($o) {
                                    return $o->key;
                                });
                                echo implode('、', $keys);
							?></td>
							<td><?= $history->end_time; ?></td>
                            <td><?php
                                $keys = arrayGroupsAction($history->successLog, function ($o) {
                                    return $o->create_time;
                                });
                                echo implode('、', $keys);
                                ?></td>
                            <td><?php
                                $num = arrayGroupsAction($history->successLog, function ($o) {
                                    $amount = \common\models\IntergralTreasureLog::find()->where([
                                        'treasure_id' => $o->treasure_id,
                                        'member_id' => $o->member_id,
                                    ])->count();
                                    return "{$amount}人次";
                                });
                                echo implode('、', $num);
                                ?></td>
						</tr>
                        <?php endforeach;else: ?>
                            <tr>
                                <td colspan="5" style="text-align: center">暂无记录</td>
                            </tr>
                        <?php endif; ?>
					</table>
				</div>
			</div>
		</div>
	</div>

<?php include __DIR__ . '/../common/cart.php'; ?>
    <script type="text/javascript">
    	//商品详情，点击小图切换大图
    	$('.idt_left .d_imgs img').bind('click',function(){
    		$(this).addClass('active').siblings().removeClass('active');
    		$('.idt_left .img').attr('src',$(this).attr('src'));
    	});

        $('[data-id="getTreasure"]').click(function () {
            var amount = $('input[name="amount"]').val(),
                data = {id : '<?= $treasure->id; ?>'};
            if (parseInt(amount)) {
                data.amount = amount;
                $.get('<?= UrlService::build(['integral-treasure/get-treasure']); ?>', data, function(res) {
                    res = JSON.parse(res);
                    if (res.status) {
                        alert("夺宝成功");
                        window.location.reload();
                        return false;
                    }

                    alert(res.msg);
                })
            }
        });
    </script>
<?php include APP_PATH . '/views/common/footer.php';?>
