<?php
global $cityPhone;
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>积分商品详情-和家网</title>
	<meta name="keywords" content="和家网,和家家博会,和家婚恋展,中国和家网">
	<meta name="description" content="和家网隶属于先和家电子商务有限公司新疆分公司，公司运营网站为中国和家网，是一家以专业从事大型会展、网络商城交易为一体的综合商务运营公司。">
	<link rel="icon" href="images/favicon.ico" mce_href="favicon.ico" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="skin_jia/css/skin.css">

  </head>
  <body class="bg_glay skin">
  	<!-- 已登录 -->
  	<div class="top bg_glay" id="top">
		<div class="mx-auto w1200">
			<span class="navbar-text float-left ml-2">Hi，欢迎来到和家网！ </span>
			<ul class="nav top_left float-left">
				<li class="nav-item"><a href="login.html" class="nav-link a">登录</a></li>
				<li class="nav-item"><a href="reg.html" class="nav-link a orange">免费注册</a></li>
			</ul>
			<span class="navbar-text float-right mr-2">服务热线：<span class="orange"><?= $cityPhone; ?></span></span>
			<ul class="justify-content-end nav top_right float-right">
				<li class="nav-item dropdown">
					<a href="javascript:void(0)" class="nav-link dropdown-toggle a" data-toggle="dropdown">我的和家</a>
					<div class="dropdown-menu">
				        <a class="dropdown-item" href="javascript:void(0)">我的订单</a>
				        <a class="dropdown-item" href="javascript:void(0)">我的订单</a>
				        <a class="dropdown-item" href="javascript:void(0)">我的订单</a>
				    </div>
				</li>
				<li class="nav-item"><a href="help.html" class="nav-link a"><img src="images/icon_ask.png">帮助中心</a></li>
				<li class="nav-item"><a href="javascript:void(0)" class="nav-link a">收藏本站</a></li>
			</ul>			
		</div>
  	</div>  	
  	
  	<div class="header">
		<div class="mx-auto w1200 clearfix">
			<a href="index.html" class="float-left logo"><img src="images/logo.png" alt="和家网"></a>
			<div class="float-left header_local">
				<a href="javascript:void(0)" class="a"><span>乌鲁木齐</span><i></i></a>
				<div class="header_local_city">
					<a href="javascript:void(0)">乌鲁木齐</a>
					<a href="javascript:void(0)">西安</a>
					<a href="javascript:void(0)">哈尔滨</a>
					<a href="javascript:void(0)">北京</a>
					<a href="javascript:void(0)">重庆</a>
					<a href="javascript:void(0)">成都</a>
					<a href="javascript:void(0)">哈尔滨</a>
					<a href="javascript:void(0)">北京</a>
					<a href="javascript:void(0)">哈尔滨</a>
					<a href="javascript:void(0)">北京</a>
				</div>

			</div>
			<div class="float-right header_ewm">
				<img src="images/img_ewm.jpg" alt="微信公众号" class="float-left">
				<p class="p1 green">微信扫一扫</p>
				<p class="p2">关注和家网微信公众号</p>
				<p class="p3 orange">随时了解展会动态！</p>
			</div>
			<div class="header_search">
				<form action="">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="同价双十二，爆品天天有">
						<div class="input-group-append">
							<button class="btn btn-success" type="submit">搜索</button>  
						</div>
					</div>
				</form>	
				<ul class="nav float-left header_search_groom">
					<li class="nav-item"><a href="javascript:void(0)" class="nav-link a">电视柜/茶几两件8折</a></li>
					<li class="nav-item"><a href="javascript:void(0)" class="nav-link a">吊顶</a></li>
					<li class="nav-item"><a href="javascript:void(0)" class="nav-link a">婴童用品</a></li>
					<li class="nav-item"><a href="javascript:void(0)" class="nav-link a">婚纱礼服</a></li>
					<li class="nav-item"><a href="javascript:void(0)" class="nav-link a">衣柜大促</a></li>
					<li class="nav-item"><a href="javascript:void(0)" class="nav-link a">灯饰</a></li>
				</ul>				
			</div>
		</div>
  	</div>
    <div class="index_top_nav index_top_nav_integral">
		<div class="mx-auto w1200">
			<div class="index_top_nav_l float-left">
				<div class="index_top_nav_top">
					<a href="javascript:void(0)"><img src="images/icom_menu.png" alt="">礼品分类</a>
				</div>
				<div class="index_top_nav_main nav_main">		
					<div class="index_category">
						<div class="index_category_list">
							<a href="productlist_integral.html" class="icl_list">
								<img src="images/icon_integral_01.png" alt="家居用品">
								<span>家居用品</span>
							</a>
							<a href="productlist_integral.html" class="icl_list">
								<img src="images/icon_integral_02.png" alt="时尚运动">
								<span>时尚运动</span>
							</a>
							<a href="productlist_integral.html" class="icl_list">
								<img src="images/icon_integral_03.png" alt="数码电器">
								<span>数码电器</span>
							</a>
							<a href="productlist_integral.html" class="icl_list">
								<img src="images/icon_integral_04.png" alt="车品车饰">
								<span>车品车饰</span>
							</a>
							<a href="productlist_integral.html" class="icl_list">
								<img src="images/icon_integral_05.png" alt="商旅办公">
								<span>商旅办公</span>
							</a>
							<a href="productlist_integral.html" class="icl_list">
								<img src="images/icon_integral_01.png" alt="母婴生活">
								<span>母婴生活</span>
							</a>
						</div>

					</div>
				</div>
			</div>
			<div class="index_top_nav_r float-left">
				<a href="integral_index.html">首页</a>	
				<a href="index_integral.html">积分夺宝</a>
				<a href="javascript:void(0)">积分抽奖</a>
				<a href="javascript:void(0)">我能兑换</a>
			</div>
		</div>
	</div>


	<div class="mx-auto w1200 clearfix">
		<nav class="breadcrumb com_crumbs">
			<span>当前位置：</span>
			<a class="breadcrumb-item" href="integral_index.html">积分商城</a>
			<span class="breadcrumb-item active">商品详情</span>
		</nav>
		<div class="integraldetail_top clearfix">
			<div class="idt_left float-left">
				<!-- 最多只能有四个图片，点击切换大图 -->
				<img src="images/_temp/img1.jpg" alt="" class="img">
				<div class="d_imgs">
					<img src="images/_temp/img1.jpg" alt="" class="active">
					<img src="images/_temp/img2.jpg" alt="" >
					<img src="images/_temp/img3.jpg" alt="" >
					<img src="images/_temp/img4.jpg" alt="" >						
				</div>
			</div>
			<div class="idt_con">
				<h3>(第6期)苹果（Apple）iMac ME086CH/A 21.5英寸一体电脑</h3>
				<p class="alert alert-success">重要提示：如有质量问题或使用咨询，请拨打售后服务热线：<span class="green"><?= $cityPhone; ?></span></p>
				<div class="d_type mt-4 pt-3">
					<span class="s1">积分</span>
					<a href="javascript:void(0)" class="a active">800</a>
				</div>
				<div class="d_type">
					<span class="s1">选择规格</span>
					<a href="javascript:void(0)" class="a active">女孩零钱包</a>
					<a href="javascript:void(0)" class="a">女孩零钱包</a>
				</div>
				<div class="d_type">
					<span class="s1">选择样式</span>
					<a href="javascript:void(0)" class="a">红色</a>
					<a href="javascript:void(0)" class="a">蓝色</a>
					<a href="javascript:void(0)" class="a">颜色随机</a>
				</div>
				
				<div class="product_nums mb-4">
					<span class="s1">选择数量</span>
					<a href="javascript:void(0)" class="a_num a_reduce">-</a>
					<input type="text" name="" value="1" class="input">
					<a href="javascript:void(0)" class="a_num a_plus">+</a>
					<span>件</span>
				</div>
				<a href="javascript:void(0)" class="btn btn-warning">立即兑换</a>
				<a href="javascript:void(0)" class="btn btn-warning a_cart"><img src="images/icon_cart.png">加入购物车</a>
			</div>
		</div>
		<div class="integraldetail_rule">
			<p class="p1">计算规则：</p>
			<p>1.商品的最后一个号码分配完毕后，将公示该分配时间点钱全部积分夺宝商品的最后50个参与记录时间；</p>
			<p>2.将这50个时间的数值求和得出数值A（每个时间按时、分、秒、毫秒的顺序组合，如20:15:24:128则为102524128）；</p>
			<p>3.将该商品所需总人次数记作数值B；</p>
			<p>4.（数值A/数值B）取余数+原始数100000001得到最终幸运号码，拥有该幸运号码者，就能拥有该宝贝啦！</p>
		</div>
		<div class="integraldetail_bottom mt-4">
			<div class="div_title nav nav-tabs">
				<a href="#pro_detail1" data-toggle="tab" class="active">商品介绍</a>
				<a href="#pro_detail2" data-toggle="tab" >规格参数</a>
				<a href="#pro_detail3" data-toggle="tab" >售后保障</a>
			</div>		
			<div class="tab-content">
				<!-- 商品介绍	 -->
				<div class="idb_content tab-pane active" id="pro_detail1">
					<p>这里是详情文案，可以没有文字，只有图片</p>
					<img src="images/_temp/img0.jpg" alt="">
				</div>	
				<!-- 规格参数 -->
				<div class="idb_content tab-pane" id="pro_detail2">
					<p>这里是规格参数，后台维护什么就显示什么</p>
				</div>	
				<!-- 售后保障 -->
				<div class="idb_content tab-pane" id="pro_detail3">
					<p>这里是售后保障售后保障售后保障售后保障，后台维护什么就显示什么</p>
				</div>
			</div>
		</div>

	</div>
	

	<div class="promise">
		<!-- <div class="mx-auto w1200">
			<img src="images/img_promise.png" alt="品质承诺">
		</div>	 -->	
	</div>
	<!-- footer -->
	<div class="footer">
		<div class="mx-auto w1200 clearfix">
			<div class="footer_all float-left"><img src="images/img_footer.jpg"></div>
			<ul class="nav flex-column float-left">
				<li class="nav-item li0">
					<a class="nav-link disabled" href="javascript:void(0)">和家网</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:void(0)">企业文化</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:void(0)">关于我们</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:void(0)">联系我们</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:void(0)">用户协议</a>
				</li>
			</ul>
			<ul class="nav flex-column float-left">
				<li class="nav-item li0">
					<a class="nav-link disabled" href="javascript:void(0)">指南</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:void(0)">逛展攻略</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:void(0)">如何索票</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:void(0)">返现指南</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:void(0)">积分规则</a>
				</li>
			</ul>
			<ul class="nav flex-column float-left">
				<li class="nav-item li0">
					<a class="nav-link disabled" href="javascript:void(0)">服务</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:void(0)">展会保障</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:void(0)">展览品类</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:void(0)">投诉处理</a>
				</li>
			</ul>
			<ul class="nav flex-column float-left">
				<li class="nav-item li0">
					<a class="nav-link disabled" href="javascript:void(0)">帮助</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:void(0)">联系客服</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:void(0)">商务合作</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:void(0)">免责声明</a>
				</li>
			</ul>
			<div class="footer_ewm float-left">
				<p>扫码关注公众号</p>
				<img src="images/img_ewm.jpg" alt="公众号">
			</div>
		</div>
	</div>
	<div class="footer_friends">
		<div class="mx-auto w1200 clearfix">
			友情链接：<a href="javascript:void(0)">衣柜</a><a href="javascript:void(0)">招生信息</a><a href="javascript:void(0)">自动门</a><a href="javascript:void(0)">上海装修公司</a><a href="javascript:void(0)">装修日记</a><a href="javascript:void(0)">暖气片</a><a href="javascript:void(0)">众易居装修网</a><a href="javascript:void(0)">净水器</a><a href="javascript:void(0)">不锈钢板</a><a href="javascript:void(0)">集成灶的优缺点</a><a href="javascript:void(0)">断桥铝门窗</a><a href="javascript:void(0)">净水器哪个牌子好</a>
		</div>
	</div>
	<div class="footer_copyright">CopyRight &#169 2007-2018 南京新与力文化传播有限公司 苏ICP备09011225号 NewPower Co. 版权所有 经营许可证编号：苏B2-20120395</div>
	<div class="footer_icp"><a href="javascript:void(0)"><img src="images/img_icp1.jpg"></a><a href="javascript:void(0)"><img src="images/img_icp2.jpg"></a><a href="javascript:void(0)"><img src="images/img_icp3.jpg"></a><a href="javascript:void(0)"><img src="images/img_icp4.jpg"></a></div>
	


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
    <script type="text/javascript" src="js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="js/popper.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/loading.js"></script>
    <script type="text/javascript" src="js/base.js"></script>
    <script type="text/javascript">
    	//商品详情，点击小图切换大图
    	$('.idt_left .d_imgs img').bind('click',function(){
    		$(this).addClass('active').siblings().removeClass('active');
    		$('.idt_left .img').attr('src',$(this).attr('src'));
    	})
    	//点击切换选择的规格
    	$('.idt_con .d_type a').bind('click',function(){
    		$(this).addClass('active').siblings().removeClass('active');
    	})    	

    </script>
  </body>
</html>