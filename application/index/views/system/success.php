<?php
use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';
?>

	<div class="mx-auto w1200 clearfix mycart">
		<!-- <p class="case_title"><span class="green">提醒页面</span></p> -->
		<div class="com_nodata">
			<img src="/public/index/images/img_result_suc.png" alt="提醒" class="img">
			<div class="con">
				<p class="p1"><?= $error['msg']; ?></p>
                <p class="p2">页面将在 <span class="orange" data-id="clock"><?= $timeOut; ?></span><span class="orange">s</span>&nbsp;后自动跳转</p>

                <p class="p2">您也可以点击这里返回 <a href="<?= UrlService::build('index/index'); ?>"  class="orange">首页</a></p>

            </div>
		</div>

	</div>

<script>
    var clockTime = <?= $timeOut; ?>,
        clock = setInterval(function () {
        if (clockTime <= 0) {
            window.location.href = '<?= $error['href']; ?>';
            clearInterval(clock);
            return false;
        }
        clockTime--;
        $('[data-id="clock"]').text(clockTime);
    }, 1000);
</script>

<?php include APP_PATH . '/views/common/footer.php';?>