<?php
use DL\service\UrlService;

?>
<div class="mx-auto w1200">
    <form data-id="join" action="<?= UrlService::build('jia-zhuang/ask-ticket'); ?>" method="post" class="explosin_form float-right">
        <h3>免费索票</h3>
        <p class="p1">已有<span class="orange"><?= $formData['amount']; ?></span>人索票</p>
        <input type="text" class="input" autocomplete="off" name="name" placeholder="<?= isset($formData['name']) ? $formData['name'] : '请输入收件人姓名'; ?>" data-default="<?= isset($formData['name']) ? $formData['name'] : ''; ?>" value="">
        <input type="text" class="input" autocomplete="off" name="mobile" placeholder="<?= isset($formData['mobile']) ? $formData['mobile'] : '请输入手机号码'; ?>" data-default="<?= isset($formData['mobile']) ? $formData['mobile'] : ''; ?>" value="">
        <div class="d_yzm clearfix">
            <a href="javascript:void(0)" data-id="getCode1" class="btn btn-danger">验证码</a>
            <!-- <a href="javascript:void(0)" class="btn btn-danger disabled">50s</a> -->
            <input type="text" class="input" autocomplete="off" name="code" placeholder="请输入短信验证码" required="">
        </div>
        <input type="hidden" class="input" autocomplete="off" name="address" placeholder="<?= isset($formData['address']) ? $formData['address'] : '请输入收货地址'; ?>" data-default="<?= isset($formData['address']) ? $formData['address'] : ''; ?>" value="">
        <input type="hidden" class="input" name="ticketId" value="<?= $formData['ticketId']; ?>">
        <input type="hidden" class="input" name="cityId" value="<?= $formData['cityId']; ?>">
        <input type="hidden" class="input" name="type" value="<?= $type; ?>">
        <input type="hidden" class="input" name="r" value="jia-zhuang/ask-ticket">
        <input type="hidden" class="input" name="t" value="jia-zhuang/index">
        <select class="select float-right" name="resource" style="display: none">
            <option>选择了解渠道</option>
            <?php
            $resource = isset($formData['resource']) ? $formData['resource'] : '';
            foreach ($resourceDics AS $resourceDic) :?>
                <option value="<?= $resourceDic; ?>" <?= $resourceDic==$resource ? 'selected' : ''; ?>><?= $resourceDic; ?></option>
            <?php endforeach; ?>
        </select>
        <a href="javascript:void(0)" class="btn" data-id="submit">免费索票</a>
        <?php if ($ticket) :?>
        <p class="p3">请详细填写以上信息，我们将免费为您快递一份价值<?= $ticket->ticket_price; ?>元的门票及展会资料</p>
        <?php endif; ?>
    </form>
    <script>
        $('[data-id="submit"]').click(function () {
            var name = $('input[name="name"]'),
                mobile = $('input[name="mobile"]'),
                address = $('input[name="address"]'),
                code = $('input[name="code"]');

            if (code.val().length < 1) {
                alert("请输入验证码");
                return false;
            }

            if (name.val().length < 1) {
                if (name.attr('data-default').length < 1) {
                    alert("请输入收件人姓名");
                    return false;
                }
                name.val(name.attr('data-default'));
            } else if (name.val().length > 200) {
                if (name.attr('data-default').length < 1) {
                    alert("用户名过长");
                    return false;
                }
            }

            if (!/^1\d{10}$/.test(mobile.val())) {
                console.log(mobile.attr('data-default'));
                if (!/^1\d{10}$/.test(mobile.attr('data-default'))) {
                    alert("请输入正确的手机号码");
                    return false;
                }

                mobile.val(mobile.attr('data-default'));
            }

            // if (address.val().length < 1) {
            //     if (address.attr('data-default').length < 1) {
            //         alert("请输入收货地址");
            //         return false;
            //     }
            //
            //     address.val(address.attr('data-default'));
            // }

            $('form[data-id="join"]').submit();
        })

        var mobileDom = $('input[name="mobile"]'),
            codeDom = $('a[data-id="getCode1"]'),
            getCodeCycle = 60;

        codeDom.click(function () {
            var mobile = mobileDom.val();
            if (!/^1\d{10}$/.test(mobile)) {
                alert("请输入正确的手机号");
                return false;
            }

            if (!codeDom.hasClass('disabled')) {
                $.post(
                    '<?=\yii\helpers\Url::toRoute('api/get-code'); ?>',
                    {mobile : mobile},
                    function (res) {
                        res = JSON.parse(res);
                        if (res.status) {
                            if (codeDom.hasClass('disabled')) {
                                return false;
                            }

                            codeDom.attr('data-cycle', getCodeCycle).addClass('disabled');

                            var cycleClock = setInterval(function () {
                                var cycle = parseInt(codeDom.attr('data-cycle'));
                                if (cycle > 0) {
                                    codeDom.text("" + cycle +"s").attr('data-cycle', cycle-1);
                                } else {
                                    codeDom.text('验证码').removeClass('disabled');
                                    clearInterval(cycleClock);
                                }
                            }, 1000);
                        }
                    }
                )
            }

        });
    </script>
</div>