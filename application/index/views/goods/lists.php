<?php

use DL\service\UrlService;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php';

?>

<div class="mx-auto w1200 clearfix">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="<?= UrlService::build('index/index'); ?>">首页</a>
        <span class="breadcrumb-item active">商品列表</span>
    </nav>
    <div class="com_type clearfix">
        <div class="com_type_title float-left" style="padding-bottom: 10px">类型：</div>
        <div class="com_type_con" style="padding-bottom: 10px">
            <a href="<?= UrlService::build(['goods/lists', 'gc_id' => 0, 'keyword' => $searchKeyword]); ?>" class="<?= $topGc==0 ? 'active' : ''; ?>">全部</a>

            <?php foreach ($secondGcs AS $secondGc) :?>
                <a href="<?= UrlService::build(['goods/lists', 'gc_id' => $secondGc->id, 'keyword' => $searchKeyword]); ?>" class="<?= $topGc==$secondGc->id ? 'active' : ''; ?>"><?= $secondGc->name; ?></a>
            <?php endforeach; ?>
        </div>
        <div class="com_type_title float-left" style="padding-top: 10px">分类：</div>
        <div class="com_type_con" style="padding-top: 10px">
            <a href="<?= UrlService::build(['goods/lists', 'gc_id' => $topGc, 'keyword' => $searchKeyword]); ?>" class="<?= $goodGc == 0? 'active' : ''; ?>">全部</a>
            <?php foreach ($gcFilters AS $gcFilter) :?>
            <a href="<?= UrlService::build(['goods/lists', 'gc' => $gcFilter->id, 'keyword' => $searchKeyword]); ?>" class="<?= $goodGc==$gcFilter->id ? 'active' : ''; ?>"><?= $gcFilter->name; ?></a>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="productlist clearfix">
        <?php if ($goods) :?>
        <?php foreach ($goods AS $good) :?>
        <a href="<?= UrlService::build(['goods/detail', 'id'=> $good->id]); ?>" target="_blank" class="productlist_con">
            <div class="img" style="background-image: url(<?=$good->goods_pic?>);"></div>
            <p class="p_title text-truncate"><?= \yii\helpers\StringHelper::truncate($good->goods_name, 18); ?></p>
            <p class="p_price red">￥<span><?= $good->goods_price; ?></span>/<?= $good->unit; ?></p>
            <p class="p_shop"><img src="/public/index/images/icon_shop.png"><?= $good->store->nickname; ?></p>
        </a>
        <?php endforeach; ?>
        <?php else :?>
            <div class="clo-12" style="text-align: center;height: 60px;line-height: 60px;">暂无商品</div>
        <?php endif; ?>
    </div>
    <?= $pageHtml; ?>
</div>


<?php include APP_PATH . '/views/common/footer.php'; ?>

