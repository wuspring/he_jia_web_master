<?php

use DL\service\UrlService;
use \common\models\TicketApplyGoods;
include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>


<div class="mx-auto w1200 clearfix">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="<?= UrlService::build(['goods/lists']); ?>">首页</a>
        <a class="breadcrumb-item" href="<?= UrlService::build(['goods/lists', 'gc' => $goodDetail->gc_id]); ?>"><?= $goodDetail->gclass->name; ?></a>
        <span class="breadcrumb-item active"><?= $goodDetail->goods_name; ?></span>
    </nav>
    <div class="productdetail_top clearfix">
        <div class="pdt_left float-left">
            <!-- 最多只能有四个图片，点击切换大图 -->
            <?php if ($goodDetail->goods_image) :?>
            <img src="<?= $goodDetail->goods_image[0]->imgurl?>" alt="" class="img">
            <div class="d_imgs">
                <?php foreach ($goodDetail->goods_image as $k=>$v):?>
                        <img src="<?=$v->imgurl?>" alt="" class="<?=($k==0)?'active':''?>">
                <?php endforeach; ?>
            </div>
            <?php endif; ?>

            <div class="bdsharebuttonbox"><span>分享 :</span>
                <a href="#" class="bds_more" data-cmd="more"></a>
                <a href="#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a>
                <a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a>
                <a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a>
            </div>
            <script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"分享到：","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"16"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>

            <div class="d_collect float-left"><a href="javascript:void(0)" class="a_collect"><span><?= ($isCollection==1)?'已收藏':'收藏'?> :</span><img class="click_collect" data-collect="<?=$isCollection?>" src="/public/index/images/icon_star.png"></a></div>

        </div>
        <div class="pdt_right float-right" style="display: none">
            <img src="<?=$store->avatarTm?>" alt="" class="img">
            <p class="p1"><?= $store->nickname; ?></p>
            <?php if ($store->defaultShop) : $defaultShop = $store->defaultShop;?>
            <p class="p2">地址：<?= $defaultShop->address; ?></p>
            <p class="p2">电话：<?= $defaultShop->mobile; ?></p>
            <p class="p2">营业时间：<?php implode(' ', $defaultShop->workDaysInfo); ?></p>
            <p class="p2"><?= $defaultShop->work_times; ?></p>
            <a href="javascript:void(0)" data-id="view-map" data-lat="<?= $defaultShop->lat; ?>" data-lng="<?= $defaultShop->lng; ?>" data-toggle="modal" data-target="#myModalMap" class="btn btn-sm btn-success">查看地图</a>
            <?php endif; ?>

        </div>
        <div class="pdt_con">


            <h3><?=$goodDetail->goods_name?></h3>
            <p class="p_remark"><?= $goodDetail->mobile_body; ?></p>
<!--            <div class="d_spec" style="max-height: 200px;overflow: hidden;">-->
<!--                <span class="title float-left">参数</span>-->
<!--                -->
<!--            </div>-->


            <div class="d_spec">
                <div class="ds_con">
                    <span class="title float-left">市场价</span>
                    <p class="p1">￥<span><?= $goodDetail->goods_marketprice; ?></span></p>
                </div>
                <div class="ds_con mt-2 mb-2">
                    <span class="title float-left">
                        <?php $typeKeys = array_keys($joinType); $type = reset($typeKeys);if ($type==TicketApplyGoods::TYPE_ORDER): ?>
                            预存价
                        <?php elseif ($type==TicketApplyGoods::TYPE_COUPON) : ?>
                            预约价
                        <?php endif; ?>
                    </span>
                    <p class="p2 orange">￥<span><?= $goodDetail->goods_price; ?></span></p>
                </div>
                <div class="ds_con">
                    <span class="title float-left">参数</span>
                    <?= $goodDetail->goods_jingle; ?>
                </div>
            </div>

            <?php if ($joinType) :?>
                <div class="d_coupon clearfix">
                    <span class="title float-left"></span>
                    <?php $typeKeys = array_keys($joinType); $type = reset($typeKeys);if ($type==TicketApplyGoods::TYPE_ORDER): ?>
                        <a href="javascript:void(0)"  data-order="true" data-val="<?= $goodDetail->id;?>" data-type="<?= $joinType[TicketApplyGoods::TYPE_ORDER]; ?>" class="btn btn-danger  rounded-0 float-left" >立即预存</a>
                    <?php elseif ($type==TicketApplyGoods::TYPE_COUPON) : ?>
                        <a href="javascript:void(0)"  data-order-price="true" data-val="<?= $goodDetail->id;?>" data-type="<?= $joinType[TicketApplyGoods::TYPE_COUPON]; ?>" class="btn btn-danger  rounded-0 float-left" >立即预约</a>
                    <?php endif; ?>
                </div>
            <?php else :?>
            <div class="d_coupon clearfix">
                <span class="title float-left">优惠</span>
                    <?php if ($goodDetail->goodsCoupon ):foreach ($goodDetail->goodsCoupon as $k=>$v): ?>

                            <a href="javascript:void(0)" class="com_coupon">
                                 <?php if (in_array($v->id, $hasArr)):?>
                                   <p class="p_r getCoupin" data-id="getCoupin" data-store="<?=$goodDetail->user_id?>" data-val="<?=$v->id?>">已经领取</p>
                                 <?php else:?>
                                     <p class="p_r getCoupin" data-id="getCoupin" data-store="<?=$goodDetail->user_id?>" data-val="<?=$v->id?>"">点击领取</p>
                                 <?php endif;?>
                                <div class="d">
                                    <p class="p1 red"><span>￥</span><?=intval($v->money)?></p>
                                    <p class="p2"><?=$v->describe?></p>
                                </div>
                            </a>

                    <?php endforeach;endif;?>
            </div>
            <?php endif; ?>
            <p class="p_warn green"><img src="/public/index/images/icon_warn.png">重要说明：为保障您能够享受最大幅度优惠，领取返现券后，请至商户店铺挑选您所需产品，自行洽谈产品价格至最低价，付款时再出示返现券。
            </p>
            <a href="<?= UrlService::build(['dian-pu/index', 'storeId' => $store->user_id]); ?>" class="a_shop"><img src="/public/index/images/icon_shop_green.png">进入店铺>></a>
        </div>
    </div>
    <div class="productdetail_bottom mt-3">
        <p class="p_title green"><span>详情</span></p>
        <div class="pdb_right float-right">
            <p class="p">猜你喜欢<span></span></p>
            <ul class="list-unstyled">
                <?php foreach ($rlCategorys as $k=>$v):?>
                    <li>
                        <a href="<?=UrlService::build(['goods/detail','id'=>$v->id])?>" class="a_img" title="<?=$v->goods_name?>"
                           style="background-image: url('<?= strlen($v->goods_pic)? $v->goods_pic : '#' ?>');"></a>
                        <a href="<?=UrlService::build(['goods/detail','id'=>$v->id])?>" class="a_title text-truncate"><?=$v->goods_name?></a>
                        <p>市场价：<span class="orange">￥<span><?= $v->goods_price; ?></span></span>/<?= $v->unit; ?></p>
                    </li>
                <?php endforeach;?>

            </ul>
        </div>
        <div class="pdb_left">
            <?=$goodDetail->goods_body?>

        </div>
    </div>

</div>
<!-- 查看地图 -->
<div class="modal fade" id="myModalMap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title pl-3"><?= $store->nickname; ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body pt-5 pb-4" id="map" style="height: 500px;">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://map.qq.com/api/js?v=2.exp&key=<?= MAP_KEY; ?>"></script>
<script>
    // map setting
    var qqMap = new qq.maps.Geocoder();
    // console.log(res,res.detail.location)//得到经纬度
    var dom = document.getElementById('map'), map, markerInfo;
    var setMap = function (postion) {
        var mapDom = new qq.maps.Map(dom, {
            center: postion,//将经纬度加到center属性上
            zoom: 16,//缩放
            draggable: true,//是否可拖拽
            scrollwheel: true,//是否可滚动缩放
            disableDoubleClickZoom: false
        });

        return mapDom;
    };

    var marker = function (position) {
        $('input[name="Devices[lng]"]').val(position.lng.toFixed(8));
        $('input[name="Devices[lat]"]').val(position.lat.toFixed(8));
        // map.center = position;
        return new qq.maps.Marker({
            position: position,//标记的经纬度
            animation: qq.maps.MarkerAnimation.BOUNCE,
            map: map//标记的地图
        })
    };

    $("a[data-id='view-map']").bind('click', function() {

        var position = new qq.maps.LatLng(
            $(this).attr('data-lat'),
            $(this).attr('data-lng'),
        );

        map = setMap(position);
        markerInfo = marker(position);

    });

    //点击收藏 点击取消
    $(".click_collect").on('click',function () {
        var good_id=<?=$goodDetail->id?>;
        var is_collect=$(this).attr('data-collect');
        var url="<?=UrlService::build(['personal/click-collection'])?>";

        if (Object.keys(userInfo).length < 1) {
            alert ("您还没有登录哦");
            return false;
        }

        if (is_collect==1){   //取消收藏
          $.get(url,{is_collect:1,good_id:good_id},function (res) {
                if (res.code==1){

                    $('.click_collect').attr('data-collect','0');
                    $('.click_collect').siblings('span').text('收藏');
                }
                alert(res.msg);
          },'json');

        }else {
            $.get(url,{is_collect:0,good_id:good_id},function (res) {
                if (res.code==1){

                    $(".click_collect").attr('data-collect','1');
                    $('.click_collect').siblings('span').text('已收藏');
                }
                alert(res.msg);
            },'json');

        }

    });

    $(function () {
       $('div.header_local').hide();
    });
</script>

<?php include APP_PATH . '/views/common/footer.php'; ?>

<script type="text/javascript">
    //商品详情，点击小图切换大图
    $('.pdt_left .d_imgs img').bind('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
        $('.pdt_left .img').attr('src', $(this).attr('src'));

    });
</script>