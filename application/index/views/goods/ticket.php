<?php

use DL\service\UrlService;
use \common\models\TicketApplyGoods;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php';

?>
<style>
    .productlist_con .btn {
        line-height: 28px;
    }

    .productlist_con {
    	height: 298px;
    	overflow: hidden;
    }
</style>
<div class="mx-auto w1200 clearfix">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="<?= UrlService::build('index/index'); ?>">首页</a>
        <span class="breadcrumb-item active">商品列表</span>
    </nav>
    <div class="com_type clearfix">
        <div class="com_type_title float-left" style="padding-bottom: 10px">类型：</div>
        <div class="com_type_con" style="padding-bottom: 10px">
            <a href="<?= UrlService::build(['goods/ticket', 'gc_id' => 0, 'type' => $ticketType]); ?>" class="<?= $topGc==0 ? 'active' : ''; ?>">全部</a>

            <?php foreach ($goodClasses AS $gcFilter) :?>
                <a href="<?= UrlService::build(['goods/ticket', 'gc_id' => $gcFilter->id, 'type' => $ticketType]); ?>" class="<?= $topGc==$gcFilter->id ? 'active' : ''; ?>"><?= $gcFilter->name; ?></a>
            <?php endforeach; ?>
        </div>
        <div class="com_type_title float-left" style="padding: 10px 15px">分类：</div>
        <div class="com_type_con" style="padding: 10px 15px">
            <a href="<?= UrlService::build(['goods/ticket', 'gc_id' => $topGc, 'type' => $ticketType]); ?>" class="<?= $goodGc == 0? 'active' : ''; ?>">全部</a>
            <?php foreach ($gcFilters AS $gcFilter) :?>
            <a href="<?= UrlService::build(['goods/ticket', 'gc' => $gcFilter->id, 'type' => $ticketType]); ?>" class="<?= $goodGc==$gcFilter->id ? 'active' : ''; ?>"><?= $gcFilter->name; ?></a>
            <?php endforeach; ?>
        </div>
        <div class="com_type_title float-left"  style="padding-top: 10px">类型：</div>

        <div class="com_type_con"  style="padding-top: 10px">

            <a href="<?= UrlService::build(['goods/ticket', 'gc_id' => $topGc]); ?>" class="<?= $ticketType==false ? 'active' : ''; ?>">全部</a>

            <a href="<?= UrlService::build(['goods/ticket', 'gc_id' => $topGc, 'type' => TicketApplyGoods::TYPE_ORDER]); ?>" class="<?= $ticketType==TicketApplyGoods::TYPE_ORDER ? 'active' : ''; ?>">预存商品</a>

            <a href="<?= UrlService::build(['goods/ticket', 'gc_id' => $topGc, 'type' => TicketApplyGoods::TYPE_COUPON]); ?>" class="<?= $ticketType==TicketApplyGoods::TYPE_COUPON ? 'active' : ''; ?>">预约商品</a>
        </div>
    </div>
    <div class="productlist clearfix">
        <?php if ($goods) :?>
        <?php foreach ($goods AS $good) :?>
                <a href="<?= UrlService::build(['goods/detail', 'id'=> $good->id]); ?>" target="_blank" class="productlist_con">
                    <div class="img" style="background-image: url(<?=$good->goods_pic?>);"></div>
                    <p class="p_title ellipsis-2"><?= \yii\helpers\StringHelper::truncate($good->goods_name, 15); ?></p>
                    <p class="btn btn-warning float-right"><?= (isset($goodsDics[$good->id]) and $goodsDics[$good->id]==TicketApplyGoods::TYPE_COUPON )?'立即预约':'立即预存'?> </p>
                    <p class="p_price red">预约价:￥<span><?= $good->goods_price; ?></span>/<?= $good->unit; ?></p>
                    <p class="p_price1">市场价:￥<span><?= $good->goods_marketprice ?></span>/<?= $good->unit; ?></p>
                    <!-- <p class="p_shop"><img src="images/icon_shop.png">集美家具大世界</p> -->
                </a>
        <?php endforeach; ?>
        <?php else :?>
            <div class="clo-12" style="text-align: center;height: 60px;line-height: 60px;">暂无商品</div>
        <?php endif; ?>
    </div>
    <?= $pageHtml; ?>
</div>




<?php include APP_PATH . '/views/common/footer.php'; ?>

