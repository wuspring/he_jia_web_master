<?php

use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/integral-header.php';
?>
<div class="integral_banner">
    <a href="javascript:void(0)"><img src="/public/index/images/banner_treasure.jpg" alt="积分夺宝，感恩回馈"></a>
</div>
<div class="integral_menu">
    <div class="mx-auto w1200 clearfix">
        <a href="<?=UrlService::build(['integral-treasure/index'])?>" >夺宝商品</a>
        <a href="<?=UrlService::build(['integral-treasure/new-record'])?>" class="active" >最新揭晓</a>
        <a href="<?=UrlService::build(['integral-treasure/my-record'])?>" >我的夺宝记录</a>
        <a href="<?=UrlService::build(['integral-treasure/treasure-rule'])?>">夺宝规则</a>
    </div>
</div>
<div class="mx-auto w1200 treasure_content">
    <div class="idb_content">
        <p>以下为最新揭晓</p>
        <table class="productdetail_table">
            <tr>
                <th>获得者</th>
                <th>中奖号码</th>
                <th>揭晓时间</th>
                <th>夺宝时间</th>
                <th>参与人次</th>
            </tr>
            <?php if ($records) : foreach ($records AS $record) :?>
            <tr>
                <td>
                    <?php
                        arrayGroupsAction($record->successLog, function ($o) {
                            echo "<img src='{$o->member->avatar}'>{$o->member->nickname}";
                        });
                    ?>
                </td>
                <td class="orange">
                    <?php
                        $keys = arrayGroupsAction($record->successLog, function ($o) {
                            return $o->key;
                        });
                        echo implode('、', $keys);
                    ?>
                </td>
                <td><?= $record->end_time; ?></td>
                <td><?= $record->goods_addtime; ?></td>
                <td><?php
                    $num = arrayGroupsAction($record->successLog, function ($o) {
                        $amount = \common\models\IntergralTreasureLog::find()->where([
                            'treasure_id' => $o->treasure_id,
                            'member_id' => $o->member_id,
                        ])->count();
                        return "{$amount}人次";
                    });
                    echo implode('、', $num);
                    ?></td>
            </tr>
            <?php endforeach; else :?>
                <tr>
                    <td colspan="5">暂无记录</td>
                </tr>
            <?php endif; ?>
        </table>
    </div>
</div>
<?php include __DIR__ . '/../common/cart.php'; ?>
<script type="text/javascript">
    //商品详情，点击小图切换大图
    $('.idt_left .d_imgs img').bind('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
        $('.idt_left .img').attr('src', $(this).attr('src'));
    })
    //点击切换选择的规格
    $('.idt_con .d_type a').bind('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
    })

</script>
<?php include APP_PATH . '/views/common/footer.php';?>
