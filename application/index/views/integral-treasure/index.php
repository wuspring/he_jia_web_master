<?php
use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/integral-header.php';
?>

	<div class="integral_banner">
		<a href="javascript:void(0)"><img src="/public/index/images/banner_treasure.jpg" alt="积分夺宝，感恩回馈"></a>
	</div>
	<div class="integral_menu">
		<div class="mx-auto w1200 clearfix">
			<a href="<?=UrlService::build(['integral-treasure/index'])?>" class="active">夺宝商品</a>
			<a href="<?=UrlService::build(['integral-treasure/new-record'])?>">最新揭晓</a>
			<a href="<?=UrlService::build(['integral-treasure/my-record'])?>">我的夺宝记录</a>
			<a href="<?=UrlService::build(['integral-treasure/treasure-rule'])?>">夺宝规则</a>
		</div>		
	</div>
	<div class="mx-auto w1200">
		<div class="treasure_product clearfix">
            <?php foreach ($treasures AS $treasure) :?>
			<div class="treasure_list">
				<a href="<?=UrlService::build(['integral-order/treasure', 'id' => $treasure->id]); ?>" class="a_img" style="background-image: url('<?= $treasure->goods_pic; ?>');" title="<?= $treasure->goods_name; ?>"></a>
				<div class="con">
					<p class="p_name text-truncate"><?= $treasure->goods_name; ?></p>
					<p class="p_need">总需：<span><?= $treasure->goods_storage; ?></span>人次</p>
					<div class="progress">
					  	<div class="progress-bar" style="width:<?php
                        $n = 0;
                        if ($treasure->goods_storage) {
                            $n = (int)($treasure->goods_salenum * 100 / $treasure->goods_storage);
                        }
                        echo $n; ?>%"></div>
					</div>
					<div class="progress_remark"><span class="s_r"><span><?php $rest = $treasure->goods_storage - $treasure->goods_salenum;
					echo $rest >0 ? $rest:0; ?></span>剩余人次</span><span class="orange"><span><?= $treasure->goods_salenum; ?></span>人次已参与</span></div>
					<a href="<?=UrlService::build(['integral-order/treasure', 'id' => $treasure->id])?>" class="btn btn-warning">立即夺宝</a>
				</div>
			</div>
            <?php endforeach; ?>
            <div style="clear: both"></div>

            <?= $page->show(); ?>
		</div>
	</div>
<?php include __DIR__ . '/../common/cart.php'; ?>
    <script type="text/javascript">
    	//商品详情，点击小图切换大图
    	$('.idt_left .d_imgs img').bind('click',function(){
    		$(this).addClass('active').siblings().removeClass('active');
    		$('.idt_left .img').attr('src',$(this).attr('src'));
    	});
    	//点击切换选择的规格
    	$('.idt_con .d_type a').bind('click',function(){
    		$(this).addClass('active').siblings().removeClass('active');
    	})    	

    </script>

<?php include APP_PATH . '/views/common/footer.php';?>