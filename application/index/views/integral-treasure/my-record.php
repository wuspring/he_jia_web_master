<?php
use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/integral-header.php';
?>

	<div class="integral_banner">
		<a href="javascript:void(0)"><img src="/public/index/images/banner_treasure.jpg" alt="积分夺宝，感恩回馈"></a>
	</div>
	<div class="integral_menu">
		<div class="mx-auto w1200 clearfix">
            <a href="<?=UrlService::build(['integral-treasure/index'])?>" >夺宝商品</a>
            <a href="<?=UrlService::build(['integral-treasure/new-record'])?>" >最新揭晓</a>
            <a href="<?=UrlService::build(['integral-treasure/my-record'])?>" class="active">我的夺宝记录</a>
            <a href="<?=UrlService::build(['integral-treasure/treasure-rule'])?>">夺宝规则</a>
		</div>		
	</div>
	<div class="mx-auto w1200 treasure_content">
		<div class="idb_content">
			<p>以下为我的夺宝记录</p>
			<table class="productdetail_table">
				<tr><th>参与人</th><th>参与时间</th><th>参与人次</th></tr>
                <?php if ($records) :foreach ($records AS $record) :?>
				<tr>
					<td><img src="<?= $record->member->avatar; ?>"><?= $record->member->nickname; ?></td>
					<td><?= $record->create_time; ?></td>
					<td><?= $record->times; ?>人次</td>
				</tr>
                <?php endforeach; else :?>
                    <tr>
                        <td colspan="3">暂无记录</td>
                    </tr>
                <?php endif; ?>
			</table>
		</div>

        <?= $pageHtml; ?>
	</div>
<?php include __DIR__ . '/../common/cart.php'; ?>
    <script type="text/javascript">
    	//商品详情，点击小图切换大图
    	$('.idt_left .d_imgs img').bind('click',function(){
    		$(this).addClass('active').siblings().removeClass('active');
    		$('.idt_left .img').attr('src',$(this).attr('src'));
    	});
    	//点击切换选择的规格
    	$('.idt_con .d_type a').bind('click',function(){
    		$(this).addClass('active').siblings().removeClass('active');
    	})    	

    </script>
<?php include APP_PATH . '/views/common/footer.php';?>