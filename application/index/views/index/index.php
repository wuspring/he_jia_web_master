<?php
use DL\service\UrlService;
use common\models\GoodsClass;
use \common\models\TicketApplyGoods;

include APP_PATH . '/views/common/header.php'; ?>

<?php
include APP_PATH . '/views/common/guider.php';

function getBanner($pics)
{
    $pics = (array)$pics;
    return arrayGroupsAction($pics, function ($pic) {
        list($href, $img) = explode('@', $pic);
        return ['url' => $href, 'img' => $img];
    });
}

function getHuodong($huodongs)
{
    strpos($huodongs, '@') < 0 and $huodongs .= '@';
    list($href, $img, $expand, $button) = explode('@', $huodongs);

    return (object)[
            'url' => strlen($href) ? $href : 'javascript:void(0)',
            'img' => $img,
            'expand' => is_null($expand) ? '' : $expand,
            'button' => (is_null($button) or strlen($button) < 1) ? '' : $button
    ];
}
?>

<div id="slider" class="carousel slide" data-ride="carousel">
    <!-- 指示符 -->
    <ul class="carousel-indicators">
        <?php if(count(getBanner($configIndex['banner']) ) >1): foreach (getBanner($configIndex['banner']) AS $index => $pic) :?>
        <li data-target="#slider" data-slide-to="<?= $index; ?>" class="<?= $index ? '' : 'active'; ?>"></li>
        <?php endforeach;endif; ?>
    </ul>
    <!-- 轮播图片 -->
    <div class="carousel-inner">
    <?php foreach (getBanner($configIndex['banner']) AS $index => $pic) :?>
        <a href="<?= $pic['url']; ?>" class="carousel-item <?= $index ? '' : 'active'; ?>" style="background-image: url('<?= $pic['img']; ?>');"></a>
    <?php endforeach; ?>
    </div>

    <!-- 左右切换按钮 -->
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>
</div>

<!-- ad -->
<div class="index_ad mt35">
    <div class="index_ad_con mx-auto w1200 clearfix">
        <a href="javascript:void(0)" title="prev" class="ipl_btn a_prev"></a>
        <a href="javascript:void(0)" title="next" class="ipl_btn a_next"></a>
        <div class="ipl_main">
            <ul class="iplm_ul list-unstyled clearfix">
                <?php
                    $huodongDatas = $configIndex['huodongs'];
                    $hKeys = get_object_vars($huodongDatas);

                    if ($hKeys) : foreach ($hKeys AS $hKey) :
                    if (strlen(getHuodong($hKey)->img)) :
                ?>
                <li>
                    <a href="<?= getHuodong($hKey)->url; ?>"  target="_blank">
                        <img src="<?= getHuodong($hKey)->img; ?>" class="loading_img" data-url="<?= getHuodong($hKey)->img; ?>" alt="">
                        <?php if (strlen(getHuodong($hKey)->expand)) :?>
                        <p>
                            <span class="s1 ellipsis-3"><?= getHuodong($hKey)->expand; ?></span>
                            <?php if (strlen(getHuodong($hKey)->button)) :?>
                                <span class="s2 btn"><?= getHuodong($hKey)->button; ?></span>
                            <?php endif; ?>
                        </p>
                        <?php endif; ?>
                    </a>

                </li>
                <?php endif;endforeach;endif; ?>
            </ul>
        </div>
    </div>
</div>
    <script type="text/javascript">

        sliderEvent( $('.index_ad_con .a_prev'), $('.index_ad_con .a_next') , $('.iplm_ul') , $('.index_ad_con') , 500 ,
            <?= $hKeys && count($hKeys)>4 ? 1 :0 ;?> , 4);

        //轮播效果1：左右轮播，自动轮播
        //参数说明：left,right为左右按钮，list为列表的上级，stopObj为鼠标移入停止自动轮播的对象，times为动画时间,auto
        //Flag是否自动轮播,ind为一次轮播的张数
        function sliderEvent(left,right,list,stopObj,times,autoFlag,ind){
            stopObj.each(function(){
                var btnLeft = left,
                    btnRight = right,
                    teamsList=list;
                var teamsWidth = parseInt(teamsList.children().outerWidth())*ind;
                var teamsWidthS = parseInt(teamsList.children().outerWidth());
                var teamsLength = teamsList.children().length;
                var teamsTime = times;
                var sliderEventID;
                if(teamsLength>ind){
                    teamsList.width(teamsWidth*teamsLength/ind);

                    btnLeft.bind('click',function(){
                        btnLeftFun()
                    });
                    btnRight.bind('click',function(){
                        btnRightFun();
                    });
                    if(autoFlag){
                        sliderEventID = setInterval(function(){
                            btnRightFun();
                        },2500);
                    }
                    stopObj.hover(function(){
                        clearInterval(sliderEventID);
                    },function(){
                        clearInterval(sliderEventID);
                        if(autoFlag){
                            sliderEventID = setInterval(function(){
                                btnRightFun();
                            },2500);
                        }
                    })
                }else{
                    left.hide();
                    right.hide();
                }
                function btnRightFun(){
                    teamsList.stop(true,true).animate({left:parseInt(teamsList.css("left"))-teamsWidthS},teamsTime,function(){
                        if(parseInt(teamsList.css("left"))<=-teamsWidth*teamsLength/ind+teamsWidthS*ind){
                            teamsList.css("left",0);
                        }
                    });
                }
                function btnLeftFun(){
                    if(parseInt(teamsList.css("left"))>=0){
                        teamsList.css("left",-teamsWidth*teamsLength/ind);
                    }
                    teamsList.stop(true,true).animate({left:parseInt(teamsList.css("left"))+teamsWidthS},teamsTime,function(){
                        if(parseInt(teamsList.css("left"))>=0){
                            teamsList.css("left",-teamsWidth*teamsLength/ind+teamsWidthS*ind);
                        }
                    });
                }
            })
        };
    </script>
<div class="com_title mt35" style="display: none;">
    <em class="line line1"></em>
    <h3>和家网 博览会</h3>
    <em class="line line2"></em>
    <p class="p2">我们已成功举办<span class="green">22届</span>家博会、<span>10届</span>婚博会、<span>10届</span>孕婴展</p>
</div>
<div class="index_ticket" style="display: none;">
    <ul class="mx-auto w1200 list-unstyled">
        <li>
            <a href="<?= UrlService::build(['jia-zhuang/index'])?>" class="a_img"><img src="/public/index/images/loading.jpg" class="loading_img" data-url="<?php
                $indexTop = json_decode($askTicketConfig['jia_zhuang']['index_top']);
                echo isset($indexTop->pic_1) ? getHuodong($indexTop->pic_1)->img : '';
                ?>" alt=""></a>
            <div class="index_ticket_more">
                <a href="<?= UrlService::build(['jia-zhuang/index'])?>" class="float-right btn btn-success">免费索票</a>
                <p>和家网家博会</p>
                <div class="d_list">
                    <?php
                    $indexFooters = get_object_vars(json_decode($askTicketConfig['jia_zhuang']['index_footer']));
                    if ($indexFooters) : foreach ($indexFooters AS $indexFooter) :if (strlen(getHuodong($indexFooter)->img)) :
                        ?>
                        <a href="<?= getHuodong($indexFooter)->url; ?>"><img src="<?= getHuodong($indexFooter)->img; ?>"></a>
                    <?php endif; endforeach; endif;?>
                </div>
            </div>
        </li>
        <li>
            <a href="<?= UrlService::build(['jie-hun/index'])?>" class="a_img"><img src="/public/index/images/loading.jpg" class="loading_img" data-url="<?php
                $indexTop = json_decode($askTicketConfig['jie_hun']['index_top']);
                echo isset($indexTop->pic_1) ? getHuodong($indexTop->pic_1)->img : '';
                ?>" alt=""></a>
            <div class="index_ticket_more">
                <a href="<?= UrlService::build(['jie-hun/index'])?>" class="float-right btn btn-success">免费索票</a>
                <p>和家网婚博会</p>
                <div class="d_list">
                    <?php
                    $indexFooters = get_object_vars(json_decode($askTicketConfig['jie_hun']['index_footer']));
                    if ($indexFooters) : foreach ($indexFooters AS $indexFooter) :if (strlen(getHuodong($indexFooter)->img)) :
                        ?>
                        <a href="<?= getHuodong($indexFooter)->url; ?>"><img src="<?= getHuodong($indexFooter)->img; ?>"></a>
                    <?php endif; endforeach; endif;?>
                </div>
            </div>
        </li>
        <li>
            <a href="<?= UrlService::build(['yun-ying/index'])?>" class="a_img"><img src="/public/index/images/loading.jpg" class="loading_img" data-url="<?php
                $indexTop = json_decode($askTicketConfig['yun_ying']['index_top']);
                echo isset($indexTop->pic_1) ? getHuodong($indexTop->pic_1)->img : '';
                ?>" alt=""></a>
            <div class="index_ticket_more">
                <a href="<?= UrlService::build(['yun-ying/index'])?>" class="float-right btn btn-success">免费索票</a>
                <p>和家网孕婴展</p>
                <div class="d_list">
                    <?php
                        $indexFooters = get_object_vars(json_decode($askTicketConfig['yun_ying']['index_footer']));
                        if ($indexFooters) : foreach ($indexFooters AS $indexFooter) :if (strlen(getHuodong($indexFooter)->img)) :
                    ?>
                    <a href="<?= getHuodong($indexFooter)->url; ?>"><img src="<?= getHuodong($indexFooter)->img; ?>"></a>
                    <?php endif; endforeach; endif;?>
                </div>
            </div>
        </li>
    </ul>
</div>
<?php if ($hasCityTickets) :?>
<!-- 当前城市开展展会 -->
<?php if ($dingJins) :?>
<div class="com_title mx-auto mt35 w1200">
    <em class="line line1"></em>
    <h3>预存 享特价</h3>
    <em class="line line2"></em>
    <p class="p2">平台交付订单的客户都可享受最超值的价格</p>
    <a href="<?=UrlService::build(["goods/ticket", 'type' => TicketApplyGoods::TYPE_ORDER])?>" class='float-right'>更多>></a>
</div>
<div class="index_rushtobuy">
    <div class="mx-auto w1200">
       <?php foreach ($dingJins as $good):?>
           <div class="index_rushtobuy_con">
               <a href="<?= UrlService::build(['goods/detail', 'id' => $good->id]); ?>" target="_blank"  class="a_img" style="background-image: url(<?=$good->goods_pic?>);"></a>
               <div class="con">
                   <a href="<?= UrlService::build(['goods/detail', 'id' => $good->id]); ?>" target="_blank"  class="p_name text-truncate"><?=$good->goods_name?></a>
                   <p class="p_reserve red">订金:<span class="s1">￥</span><span class="s2"><?=$good->ticket_money?></span></p>
                   <p class="p_price">专享价:<span class="s1"><?=$good->goods_price?></span><span class="s2">元/<?=$good->unit?></span></p>
                   <p class="p_price"><span class="s2">市场价:</span><span class="s3"><?=$good->goods_marketprice?>元/<?=$good->unit?></span></p>
                   <p class="p_surplus red"><img src="/public/index/images/icon_time_red.png">仅余<?=$good->goods_storage?>件</p>
                   <?php if(false): //已废弃?>
                       <a href="<?= UrlService::build(['goods/detail', 'id' => $good->id]); ?>" data-dlauto data-val="<?= $good->id; ?>" target="_blank" class="btn btn-danger" >立即预存</a>
                   <?php endif;?>
                   <a href="javascript:void(0)" data-order="true" data-dlauto data-type="<?= $good->type; ?>" data-val="<?= $good->id; ?>"  class="btn btn-danger" >立即预存</a>
               </div>
           </div>
       <?php endforeach;?>
    </div>
</div>
<?php endif;?>
<?php if ($reMais) :?>
<div class="com_title mx-auto w1200">
    <em class="line line1"></em>
    <h3>热卖商品 爆款预约</h3>
    <em class="line line2"></em>
    <p class="p2">为您推荐最值得购买的商品预约订购</p>
    <a href="<?=UrlService::build(["goods/ticket", 'type' => TicketApplyGoods::TYPE_COUPON])?>" class='float-right'>更多>></a>
</div>
<div class="index_reserve">
    <div class="mx-auto w1200">
        <!-- 一行三个，每行最后一个加con_last -->
        <?php foreach ($reMais as $good):?>
            <div class="index_reserve_con">
                <a href="<?= UrlService::build(['goods/detail', 'id' => $good->id]); ?>" target="_blank" class="a_img" style="background-image: url(<?=$good->goods_pic?>);"></a>
                <div class="con">
                    <a href="<?= UrlService::build(['goods/detail', 'id' => $good->id]); ?>" target="_blank" class="p_name text-truncate"><img src="<?=$good->store->avatar?>"><?=$good->goods_name?></a>
                    <div class="float-right">
                        <p class="p_surplus"><img src="/public/index/images/icon_time.png">仅余<?=$good->goods_storage?>件</p>
                        <a href="javascript:void(0)" data-order-price="true" data-val="<?= $good->id;?>" data-type="<?=$good->type; ?>"   class="btn btn-danger btn-sm">立即预约</a>
                    </div>
                    <p class="p_price red"><span class="s1">￥</span><span class="s2"><?= $good->goods_price; ?></span><span class="s3">/<?=$good->unit?></span></p>
                </div>
            </div>
       <?php endforeach;?>
    </div>
</div>
<?php endif; ?>
<!-- 当前城市开展展会 -->
<?php endif; ?>

<div class="com_title mx-auto w1200">
    <em class="line line1"></em>
    <h3>优选品牌 精选商品</h3>
    <em class="line line2"></em>
    <p class="p2">您所需要的都是我们精心挑选的</p>
</div>
<div class="index_boutique">
    <div class="mx-auto w1200">
        <?php
            $expands = $configIndex['expand'];
            $hKeys = get_object_vars($expands);
        ?>

        <a href="<?= getHuodong($hKeys['huodong_1'])->url; ?>" target="_blank" class="index_boutique_left float-left"><img src="/public/index/images/loading.jpg" class="loading_img" data-url="<?= getHuodong($hKeys['huodong_1'])->img; ?>"></a>
        <div class="index_boutique_right float-right">
            <a href="<?= getHuodong($hKeys['huodong_2'])->url; ?>" target="_blank" class="con_top"><img src="/public/index/images/loading.jpg" class="loading_img" data-url="<?= getHuodong($hKeys['huodong_2'])->img; ?>"></a>
            <div class="con_bottom">
                <a href="<?= getHuodong($hKeys['huodong_3'])->url; ?>" target="_blank" class="float-left"><img src="/public/index/images/loading.jpg" class="loading_img" data-url="<?= getHuodong($hKeys['huodong_3'])->img; ?>"></a>
                <a href="<?= getHuodong($hKeys['huodong_4'])->url; ?>" target="_blank" class="float-right"><img src="/public/index/images/loading.jpg" class="loading_img" data-url="<?= getHuodong($hKeys['huodong_4'])->img; ?>"></a>
            </div>
        </div>
    </div>
</div>

<!-- 楼层-1楼 -->
<?php
$categorys = GoodsClass::findAll([
    'fid' => 0,
    'is_del' => '0',
    'is_show'=>0
]);

?>
<?php foreach ($categorys AS $index => $category) :?>
    <div class="index_floor bg_glay floor1">
        <div class="mx-auto w1200">
            <div class="if_title clearfix">
                <ul class="nav nav-tabs float-right if_title_link">
                    <?php
                    $sonCategorys = $category->sonCategory;
                    $firstCategory = $sonCategorys ? reset($sonCategorys) : [];

                    foreach ($sonCategorys AS $num => $sonCategory) :?>
                    <li>
                        <a href="#floor<?=$index;?>_tab<?=$num;?>" data-toggle="tab" class="<?= $num ? '' : 'active show'; ?>">
                            <?= $sonCategory->name;?>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </ul>
                <a href="<?= $firstCategory ? UrlService::build(['goods/lists', 'gc_id' => $firstCategory->id]) : 'javascript:void(0)'; ?>" class="float-left title"><span><?= $index+1; ?>F</span><?= $category->name;?></a>
            </div>
            <div class="tab-content if_list">
                <?php foreach ($sonCategorys AS $num => $sonCategory) :?>
                <div class="tab-pane clearfix if_list_con <?= $num ? '' : 'active show'; ?>" id="floor<?=$index;?>_tab<?=$num;?>">
                    <?php
                    $cityConfigInfo = $cityConfigs[$sonCategory->id];
                    ?>
                    <a href="<?= strlen($cityConfigInfo->img['pic_1']['href']) ? $cityConfigInfo->img['pic_1']['href'] : 'javascript:void(0)';; ?>" class="if_list_left float-left">
                        <p class="p1"><?= $index+1; ?>F</p>
                        <p class="p2"><?= $category->name;?></p>
                        <img src="<?= $cityConfigInfo->img['pic_1']['src']; ?>" alt="<?= $category->name;?>">
                    </a>
                    <ul class="if_list_middle clearfix float-left list-unstyled">
                        <li><a href="<?= strlen($cityConfigInfo->img['pic_2']['href']) ? $cityConfigInfo->img['pic_2']['href'] : 'javascript:void(0)'; ?>" style="background-image: url('<?= $cityConfigInfo->img['pic_2']['src']; ?>');"></a></li>
                        <li><a href="<?= strlen($cityConfigInfo->img['pic_3']['href']) ? $cityConfigInfo->img['pic_3']['href'] : 'javascript:void(0)'; ?>" style="background-image: url('<?= $cityConfigInfo->img['pic_3']['src']; ?>');"></a></li>
                        <li><a href="<?= strlen($cityConfigInfo->img['pic_4']['href']) ? $cityConfigInfo->img['pic_4']['href'] : 'javascript:void(0)'; ?>" style="background-image: url('<?= $cityConfigInfo->img['pic_4']['src']; ?>');"></a></li>
                        <li><a href="<?= strlen($cityConfigInfo->img['pic_5']['href']) ? $cityConfigInfo->img['pic_5']['href'] : 'javascript:void(0)'; ?>" style="background-image: url('<?= $cityConfigInfo->img['pic_5']['src']; ?>');"></a></li>
                    </ul>
                    <div class="if_list_right float-right">
                        <p>品牌门店</p>
                        <ul class="list-unstyled clearfix brand_list">
                            <?php foreach ($cityConfigInfo->brand AS $brand) :?>
                            <li><a href="<?= UrlService::build(['dian-pu/index', 'storeId' => $brand->user_id]); ?>"><img src="<?= $brand->avatar; ?>" data-url="<?= $brand->avatar; ?>" class="loading_img" alt="<?= $brand->nickname; ?>"></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>


<div class="com_title mx-auto w1200">
    <em class="line line1"></em>
    <h3>和家资讯 最新报道</h3>
    <em class="line line2"></em>
    <p class="p2">最具价值的专业知识与实时的会展资讯</p>
    <a href="<?=UrlService::build(['fitup-school/news-list'])?>" class='float-right'>更多>></a>
</div>
<div class="mx-auto w1200 clearfix">
    <!-- 会展资讯 -->
    <div class="index_news float-left">
        <p class="title"><a href="<?=UrlService::build(['fitup-school/news-list'])?>" class="float-right">更多>></a>会展资讯</p>
        <ul class="list-unstyled clearfix">
            <?php foreach ($news as $k=>$v):?>
                   <?php if (($k)% 2 == 0):?>
                    <li class="float-left">
                        <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$v->id])?>" class="a_img"><img src="<?=$v->themeImg?>" alt=""></a>
                        <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$v->id])?>" class="a_title text-truncate"><?=$v->title?></a>
                        <p class="p ellipsis-3"><?=$v->brief?></p>
                    </li>
                    <?php else:?>
                    <li class="float-right">
                        <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$v->id])?>" class="a_img"><img src="<?=$v->themeImg?>" alt=""></a>
                        <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$v->id])?>" class="a_title text-truncate"><?=$v->title?></a>
                        <p class="p ellipsis-3"><?=$v->brief?></p>
                    </li>
                    <?php endif;?>

            <?php endforeach;?>
        </ul>

    </div>
    <div class="index_knowledge float-right">
        <ul class="nav nav-tabs index_knowledge_tab">
            <li><a href="#knowledge_tab1" data-toggle="tab"  class="active">专业知识</a></li>
            <li><a href="#knowledge_tab2" data-toggle="tab">行业资讯</a></li>
        </ul>
        <div class="tab-content index_knowledge_list">
            <ul class="tab-pane list-unstyled ikl_list active" id="knowledge_tab1">
                <?php foreach ($newsZhuanYe as $zhuanYeK=>$zhuanYeV):?>
                    <li> <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$zhuanYeV->id])?>">
                            <img src="<?=$zhuanYeV->themeImg?>" alt="">
                            <p class="ellipsis-2"><?=$zhuanYeV->title?></p>
                        </a>
                    </li>
                <?php endforeach;?>

            </ul>
            <ul class="tab-pane list-unstyled ikl_list" id="knowledge_tab2">
                <?php foreach ($newsHangYe as $hangYeK=>$hangYeV):?>
                <li> <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$hangYeV->id])?>">
                        <img src="<?=$hangYeV->themeImg?>" alt="">
                        <p class="ellipsis-2"><?=$hangYeV->title?></p>
                    </a>
                </li>
                <?php endforeach;?></a></li>
            </ul>

        </div>
    </div>

</div>
<div class="mx-auto w1200">
    <div class="index_problem clearfix">
        <div class="index_problem_left float-left">
            <div class="links float-right">
                <a href="<?=UrlService::build(['fitup-school/answer','param'=>0])?>" class="a1">我要提问</a>
                <a href="<?=UrlService::build(['fitup-school/answer','param'=>2])?>" class="a2">我来回答</a>
            </div>
            <p class="title">问答<span>已累计解决<span class="red"><?=$problemNum?></span>例问题</span></p>
            <div class="index_problem_list clearfix">
                <?php foreach ($problem as $problem_k=>$problem_v):?>
                    <a href="<?=UrlService::build(['fitup-school/answer-detail','id'=>$problem_v->id])?>" class="ipl_con">
                        <p class="p1 text-truncate"><?=$problem_v->problem?></p>
                        <p class="p2 ellipsis-2"> <?=$problem_v->oneAnswer->content?></p>
                    </a>
                <?php endforeach;?>
            </div>
        </div>
        <div class="index_problem_right float-right">
            <p class="title">常见问题</p>
            <a href="<?=UrlService::build(['member/help','contId'=>17])?>"><img src="/public/index/images/img_problem.jpg" alt="常见问题"></a>
        </div>
    </div>
</div>
    <script type="text/javascript">
        $(function () {
            $('.if_title_link li a,.index_knowledge_tab li a').hover(function () {
                $(this).tab('show');
            });
        })
    </script>
<?php include APP_PATH . '/views/common/footer.php';?>