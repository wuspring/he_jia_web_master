<?php

use DL\service\UrlService;
use common\models\GoodsClass;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/integral-header.php';
?>

<link rel="stylesheet" href="/public/index/css/prize.css">
<div class="integral_banner">
    <a href="javascript:void(0)"><img src="/public/index/images/banner_luckdraw.jpg" alt="积分抽大奖"></a>
</div>
<div class="luckdraw_content">
    <div class="mx-auto w1200 luckdraw_content_top">
        <div class="luckdraw_title"><img src="/public/index/images/icon_luckdraw_01.png"><span>积分抽奖</span></div>
        <div class="lct_con clearfix">
            <div class="lctc_left float-left">
                <div class="g-content">
                    <div class="g-lottery-case">
                        <div class="g-left">
                            <h2 style="font-size:16px;">您已拥有<span class="playnum"><?=$playNum?></span>次抽奖机会，点击立刻抽奖！~</h2>
                            <div class="g-lottery-box">
                                <div class="g-lottery-img">
                                    <a class="playbtn" href="javascript:;" title="开始抽奖"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="lctc_right float-right">
                <div class="con mt-5">
                    <p class="p_title">活动规则</p>
                    <div>
                        <p>1.每次点击消耗10积分，未中奖积分不退还</p>
                        <p>2.获得奖品，客服人员会与您联系确认收货地址等信息</p>
                        <p>3.奖品以实物为准</p>
                        <p>4.所有解释权归和家网所有</p>
                    </div>
                </div>
                <div class="con mt-3">
                    <p class="p_title">获奖名单</p>
                    <div class="text-center luckdraw_notice"  style="height: 180px;overflow: hidden">
                    <ul class="list-unstyled">
                        <?php foreach ($getPizeList as $key=>$value):?>
                            <li><p><span><?=substr_cut($value->member->mobile)?></span>获得<span><?=$value->intergralTurntable->prize?></span></p></li>
                        <?php endforeach;?>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="mx-auto w1200 luckdraw_content_strategy">
        <div class="luckdraw_title"><img src="/public/index/images/icon_luckdraw_02.png"><span>赚积分攻略</span></div>
        <div class="lcs_con">
            <img src="/public/index/images/img_luckdraw_green.jpg">
        </div>
    </div>
    <div class="mx-auto w1200 luckdraw_content_product">
        <div class="luckdraw_title"><img src="/public/index/images/icon_luckdraw_03.png"><span>本期奖品展示</span></div>
        <div class="lcp_list clearfix">

            <?php foreach ($prize as $k=>$v):?>
                <div class="lcpl_con">
                    <a href="javascript:void(0)"><img src="<?=$v->cover_pic?>" alt=""></a>
                    <p class="p1"><?=$v->prize_grade?></p>
                    <p class="p2 text-truncate"><?=$v->prize_name?></p>
                </div>
            <?php endforeach;?>


        </div>
    </div>


</div>
<?php include __DIR__ . '/../common/cart.php'; ?>
<script type="text/javascript" src="/public/index/js/jquery.rotate.min.js"></script>
<script type="text/javascript">

    $(".playbtn").on('click',function () {
        lottery();
    });


    function lottery() {
        $.ajax({
            type: 'POST',
            url: '<?=UrlService::build(['integral-luckdraw/doaward'])?>',
            dataType: 'json',
            cache: false,
            success: function (json) {
                 if(json.code==1){
                     var a = json.angle; //角度
                     var p = json.prize; //奖项
                     $(".playbtn").rotate({
                         duration: 3000, //转动时间
                         angle: 0,
                         animateTo: 1800 + a, //转动角度
                         callback: function () {
                             $(".playnum").text(json.playNum);
                             var con = confirm('当前积分还可以有' + json.playNum + '机会。\n还要再来一次吗？');
                             if (con) {
                                 lottery();
                             } else {
                                 return false;
                             }
                         }
                     });
                 }else {
                      alert(json.msg, function () {
                          return false;
                      });

                      return false;
                 }
            }
        });
    }


    //公告滚动
    noticeScroll();
    //公告滚动
    function noticeScroll(){
        $.fn.myScroll = function(options){
            var defaults = {
                speed:40,
                rowHeight:30
            };
            var opts = $.extend({}, defaults, options),intId = [];
            function marquee(obj, step){
                obj.find('ul').animate({
                    marginTop: '-=1'
                },0, function() {
                    var s = Math.abs(parseInt($(this).css("margin-top")));
                    if(s >= step){
                        $(this).find("li").slice(0, 1).appendTo($(this));
                        $(this).css("margin-top", 0);
                    }
                });
            };
            this.each(function(i){
                var sh = opts["rowHeight"],speed = opts["speed"],_this = $(this);
                intId[i] = setInterval(function(){
                    if(_this.find("ul").height()<=_this.height()){
                        clearInterval(intId[i]);
                    }else{
                        marquee(_this, sh);
                    }
                }, speed);

                _this.hover(function(){
                    clearInterval(intId[i]);
                },function(){
                    intId[i] = setInterval(function(){
                        if(_this.find("ul").height()<=_this.height()){
                            clearInterval(intId[i]);
                        }else{
                            marquee(_this, sh);
                        }
                    }, speed);
                });
            });
        };
        $(".luckdraw_notice").myScroll({
            speed:80,
            rowHeight:30
        });
    }






</script>
<?php include APP_PATH . '/views/common/footer.php'; ?>
