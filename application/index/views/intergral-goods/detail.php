<?php
use DL\service\UrlService;
use common\models\GoodsClass;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/integral-header.php';
?>
<script>
    // sku data;
    var goodSku = <?= json_encode($goodDetail->skuInfoData); ?>,
        // 选择的SKU
        setGoodSkuInfo = {},
        GOOD_ID = <?= (int)$goodDetail->id; ?>,
        skuCondition = <?= (int)$goodDetail->use_attr; ?>,
        ORDER_ROUT_PAY = '<?= UrlService::build('integral-order/create'); ?>',
        CART_ROUTE_ADD = '<?= UrlService::build('integral-cart/add'); ?>';
</script>

	<div class="mx-auto w1200 clearfix">
		<nav class="breadcrumb com_crumbs">
			<span>当前位置：</span>
			<a class="breadcrumb-item" href="<?= UrlService::build('integral/index'); ?>">积分商城</a>
			<span class="breadcrumb-item active">商品详情</span>
		</nav>
		<div class="integraldetail_top clearfix">
			<div class="idt_left float-left">
				<!-- 最多只能有四个图片，点击切换大图 -->
				<img src="<?= $goodDetail->first_goods_image; ?>" data-id="good-image" alt="" class="img">
				<div class="d_imgs">
                    <?php foreach ($goodDetail->goods_image AS $index =>  $img) : ?>
                        <img src="<?= $img; ?>" alt="" class="<?= $index ? '' : 'active'; ?>">
                    <?php endforeach; ?>
				</div>
			</div>
			<div class="idt_con">
				<h3><?= $goodDetail->goods_name; ?></h3>
                <?php
                    global $cityPhone;
                ?>
				<p class="alert alert-success">重要提示：如有质量问题或使用咨询，请拨打售后服务热线：<span class="green"><?= $cityPhone; ?></span></p>
				<div class="d_type mt-4 pt-3">
					<span class="s1">积分</span>
                    <span data-id="price" class="a active"><?= $goodDetail->goods_integral; ?></span>
				</div>

                <?php if ($goodDetail->use_attr) : ?>
                    <?php foreach ($goodDetail->attrInfoData AS $gid => $attr) :?>
                        <div class="d_type">
                            <span class="s1">选择<?= $attr['name']; ?></span>
                            <?php foreach ($attr['sons'] AS $aid => $attrInfo) :?>
                            <a data-id="attr" data-type="<?=$gid;?>" data-val="<?= $aid;?>" href="javascript:void(0)" class="a">
                                <?= $attrInfo['name']; ?></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                <?php else :?>
                    <div class="d_type">
                        <span class="s1">选择规格</span>
                        <a href="javascript:void(0)" class="a active">默认</a>
                    </div>
                <?php endif; ?>

				<div class="product_nums mb-4">
					<span class="s1">选择数量</span>
					<a href="javascript:void(0)" class="a_num a_reduce"  data-id="amountCtrl" data-type="reduce">-</a>
					<input type="text" name="amount" value="1" class="input">
					<a href="javascript:void(0)" class="a_num a_plus" data-id="amountCtrl" data-type="plus">+</a>
					<span>件</span>
				</div>
                <?php
                echo '<a data-type="pay" href="javascript:void(0);" data-id="btn_buy" ' . ((int)$goodDetail->use_attr ? '' : 'data-sku="0"') . 'class="btn btn-warning">立即兑换</a><a data-type="pay" href="javascript:void(0);" class="btn btn-warning a_cart " data-id="btn_cart"' . ((int)$goodDetail->use_attr ? '' : 'data-sku="0"')  . '><img src="/public/index/images/icon_cart.png" alt="">加入购物车</a>';
                ?>
			</div>
		</div>
		<div class="integraldetail_rule">
			<p class="p1">计算规则：</p>
			<p>1.商品的最后一个号码分配完毕后，将公示该分配时间点钱全部积分夺宝商品的最后50个参与记录时间；</p>
			<p>2.将这50个时间的数值求和得出数值A（每个时间按时、分、秒、毫秒的顺序组合，如20:15:24:128则为102524128）；</p>
			<p>3.将该商品所需总人次数记作数值B；</p>
			<p>4.（数值A/数值B）取余数+原始数100000001得到最终幸运号码，拥有该幸运号码者，就能拥有该宝贝啦！</p>
		</div>
		<div class="integraldetail_bottom mt-4">
			<div class="div_title nav nav-tabs">
				<a href="#pro_detail1" data-toggle="tab" class="active">商品介绍</a>
				<a href="#pro_detail2" data-toggle="tab" >规格参数</a>
				<a href="#pro_detail3" data-toggle="tab" >售后保障</a>
			</div>		
			<div class="tab-content">
				<!-- 商品介绍	 -->
				<div class="idb_content tab-pane active" id="pro_detail1">
					<?= $goodDetail->goods_body; ?>
				</div>	
				<!-- 规格参数 -->
				<div class="idb_content tab-pane" id="pro_detail2">
                    <?= $goodDetail->goods_jingle; ?>
				</div>	
				<!-- 售后保障 -->
				<div class="idb_content tab-pane" id="pro_detail3">
                    <?= $goodDetail->mobile_body; ?>
				</div>
			</div>
		</div>

	</div>
<?php include __DIR__ . '/../common/cart.php'; ?>
<?php if ($goodDetail->use_attr) : ?>
    <script>
        $('a[data-id="attr"]').click(function () {
            var i = $(this).attr('data-type'),
                val = $(this).attr('data-val');
            $('a[data-id="attr"]').each(function () {
                if ($(this).attr('data-type') == i ) {
                    if ($(this).attr('data-val') == val) {
                        $(this).addClass('guige_cur');
                    } else {
                        $(this).removeClass('guige_cur');
                    }
                }
            });
            setGoodSkuInfo[i] = val;
            setSkuInfo();
        });
        var setSkuInfo = function () {
            var i = <?= count($goodDetail->attrInfoData); ?>,
                k = Object.keys(setGoodSkuInfo),
                v = Object.values(setGoodSkuInfo);

            if (k.length >= i) {
                var key = numArraySort(v).join('-');console.log(key);
                if (goodSku[key] == undefined) {
                    alert("获取信息异常，请稍后重试");
                    return false;
                }

                // setting
                $('[data-type="pay"]').attr('data-sku', goodSku[key].id);
                $('a[data-id="collect"]').attr('data-sku', goodSku[key].id);

                $('img[data-id="good-image"]').attr({
                    'src' : goodSku[key].picimg,
                    'jqimg' : goodSku[key].picimg,
                });
                $('span[data-id="storage"]').text(goodSku[key].num);
                $('input[name="amount"]').attr('data-limit', goodSku[key].num);
                $('span[data-id="price"]').text(goodSku[key].price);
            }
        }
    </script>
<?php endif; ?>
    <script type="text/javascript">
    	//商品详情，点击小图切换大图
    	$('.idt_left .d_imgs img').bind('click',function(){
    		$(this).addClass('active').siblings().removeClass('active');
    		$('.idt_left .img').attr('src',$(this).attr('src'));
    	})
    	//点击切换选择的规格
    	$('.idt_con .d_type a').bind('click',function(){
    		$(this).addClass('active').siblings().removeClass('active');
    	})
    </script>
<?php include APP_PATH . '/views/common/footer.php';?>
