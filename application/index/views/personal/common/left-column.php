<?php
use DL\service\UrlService;

isset($pageIndexKey) or $pageIndexKey = 0;
?>
<style>
    .hide {
        display: none;
    }
</style>
<div class="user_menu float-left">
    <img src="<?=($model->avatar)?$model->avatar:'/public/index/images/img_default_user.jpg'?>" alt="95991480-811161" class="user_menu_img">
    <h3 class="user_menu_name"><?=$model->nickname?></h3>
    <div class="user_menu_list">
        <a href="#userMeun1" class="nav-header" data-toggle="collapse">订单中心</a>
        <ul class="nav flex-column collapse show" id="userMeun1">
            <li class="nav-item"><a class="nav-link <?= $pageIndexKey==3 ? 'active' : ''; ?>" href="<?=UrlService::build(['personal/appointment-order'])?>">定金订单</a></li>
            <li class="nav-item"><a class="nav-link <?= $pageIndexKey==1 ? 'active' : ''; ?>" href="<?=UrlService::build(['personal/integral-order'])?>">积分订单</a></li>
            <li class="nav-item"><a class="nav-link <?= $pageIndexKey==2 ? 'active' : ''; ?>" href="<?=UrlService::build(['personal/coupon-order'])?>">预约订单</a></li>
            <li class="nav-item"><a class="nav-link <?= $pageIndexKey==11 ? 'active' : ''; ?>" href="<?=UrlService::build(['personal/coupon-verifition-order'])?>">优惠劵订单</a></li>
        </ul>
        <a href="#userMeun2" class="nav-header" data-toggle="collapse">会员中心</a>
        <ul class="nav flex-column collapse show" id="userMeun2">
            <li class="nav-item"><a class="nav-link  <?= $pageIndexKey==5 ? 'active' : ''; ?>" href="<?=UrlService::build(['personal/index'])?>">用户信息</a></li>
            <li class="nav-item"><a class="nav-link <?= $pageIndexKey==6 ? 'active' : ''; ?>" href="<?=UrlService::build(['personal/my-account'])?>">账户安全</a></li>
            <li class="nav-item"><a class="nav-link <?= $pageIndexKey==4 ? 'active' : ''; ?>" href="<?=UrlService::build(['personal/my-address'])?>">收货地址</a></li>
            <li class="nav-item"><a class="nav-link <?= $pageIndexKey==7 ? 'active' : ''; ?>" href="<?=UrlService::build(['personal/my-history'])?>">我的足迹</a></li>
            <li class="nav-item"><a class="nav-link <?= $pageIndexKey==12 ? 'active' : ''; ?>" href="<?=UrlService::build(['personal/my-collection'])?>">我的收藏</a></li>
        </ul>
        <a href="#userMeun3" class="nav-header" data-toggle="collapse">账户中心</a>
        <ul class="nav flex-column collapse show" id="userMeun3">
            <li class="nav-item hide"><a class="nav-link <?= $pageIndexKey==8 ? 'active' : ''; ?>" href="<?=UrlService::build(['personal/my-recharge'])?>">我的充值</a></li>
            <li class="nav-item"><a class="nav-link <?= $pageIndexKey==9 ? 'active' : ''; ?>" href="<?=UrlService::build(['personal/my-integral'])?>">我的积分</a></li>
            <li class="nav-item"><a class="nav-link <?= $pageIndexKey==10 ? 'active' : ''; ?>" href="<?=UrlService::build(['personal/my-coupon'])?>">我的优惠劵</a></li>
        </ul>
    </div>

</div>