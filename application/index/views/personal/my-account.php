<?php

use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>

<div class="mx-auto w1200 clearfix pb-5 mt-3">
    <?php include __DIR__ . '/common/left-column.php' ?>
    <div class="user_content float-right">
        <p class="case_title"><span class="green">账户安全</span></p>
        <div class="myaccount">
            <div class="myaccount_list">
                <div class="myaccount_list_l">
                    <img src="/public/index/images/myaccount_icon1.jpg" alt="">
                </div>
                <div class="myaccount_list_c">
                    <h2>登录密码</h2>
                    <p class="orange">互联网账号存在被盗风险，建议您定期更改密码以保护账户安全。</p>
                </div>
                <div class="myaccount_list_r">
                    <a href="<?= UrlService::build(['personal/my-account-pass']) ?>" class="account_btn1">修改</a>
                </div>
            </div>
            <div class="myaccount_list">
                <div class="myaccount_list_l">
                    <img src="/public/index/images/myaccount_icon2.jpg" alt="">
                </div>
                <div class="myaccount_list_c">
                    <h2>手机验证</h2>
                    <p>在忘记密码，订单处理过程中需要帮助</p>
                </div>
                <div class="myaccount_list_r">
                    <a href="<?= UrlService::build(['personal/my-account-phone']) ?>" class="account_btn2">立即验证</a>
                </div>
            </div>

        </div>


    </div>

</div>

<?php include APP_PATH . '/views/common/footer.php'; ?>
