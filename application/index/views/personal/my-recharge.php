<?php

use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>
<div class="mx-auto w1200 clearfix pb-5 mt-3">
    <?php include __DIR__ . '/common/left-column.php' ?>
    <div class="user_content float-right">
        <p class="case_title"><span class="green">我的充值</span></p>
        <div class="myaccount">
            <form action="" class="account_pass">
                <ul>
                    <li><label for="">充值金额：</label><input type="text" class="account_input"><span>元</span></li>
                    <li class="recharge">
                        <label for="">充值方式：</label>
                        <div class="casesure_pay">
                            <a href="javascript:void(0)"><img src="/public/index/images/icon_pay_ye.png">余额支付</a>
                            <a href="javascript:void(0)"><img src="/public/index/images/icon_pay_zfb.png">支付宝</a>
                            <a href="javascript:void(0)"><img src="/public/index/images/icon_pay_wx.png">微信支付</a>
                        </div>
                    </li>

                    <a href="javascript:void(0)" class="btn btn-warning account_btn">立即充值</a>
                </ul>
            </form>

        </div>


    </div>

</div>


<script type="text/javascript">
    $('.casesure_pay a').bind('click', function () {
        $(this).toggleClass('active').siblings().removeClass('active');
    })
</script>


<?php include APP_PATH . '/views/common/footer.php'; ?>
