<?php
use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;
use yii\bootstrap\ActiveForm;
include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>

	<div class="mx-auto w1200 clearfix pb-5 mt-3">

        <?php include __DIR__.'/common/left-column.php' ?>
		<div class="user_content float-right">
			<p class="case_title"><span class="green">用户信息</span></p>
			<div class="user_box">
				<form data-id="join" action="<?= UrlService::build(['personal/index']); ?>" method="post" class="user_form">
                    <input type="hidden" id="header_pic" name="avatar" value="">
                    <input type="hidden" id="header_picTm" name="avatarTm" value="">
					<ul>
						<li class="user_li1">
							<label for="" class="user_li1_label"><span>*</span>当前头像</label>
							<div class="user_head">
					            <div class="pic"><img id="memberPic" src="<?=($model->avatar)?$model->avatar:'/public/index/images/img_default_user.jpg'?>" alt="头像"></div>
					            <div class="sc_head_btn"><input type="file" name="" value="" accept="image/*"><a href="javascript:void(0)" class="sc_head">上传头像</a></div>
					        </div>
						</li>
						<li class="user_li2">
							 <label for="" class="user_li2_label"><span>*</span>用户名</label>
                             <input type="user" name="name"  value="<?=isset($model['nickname']) ? $model['nickname'] : ''; ?>" class="user_input"  data-default="<?= isset($model['nickname']) ? $model['nickname'] : ''; ?>" placeholder="请输入您的用户名" >
						</li>
						<!-- 
						<li class="user_li2">
							<label for="" class="user_li2_label"><span>*</span>手机号码</label>
							 <input type="user" class="user_input" placeholder="请输入您的手机号码">
						</li> -->
						<!-- <li class="user_li3">
							<label for="" class="user_li3_label"><span>*</span>二维码</label>
							<img src="/public/index/images/erweima.jpg" alt="" class="weixin">
						</li> -->
						<li class="user_li2">
							<label for="" class="user_li2_label"><span>*</span>生日</label>
							<div class="user_select">
								<select name="year" id="">
									<option value="">请选择年份</option>
                                    <?php print_r($model->birthday_year);?>
                                    <?php   foreach ($model->year as $k=>$v):?>
                                      <?php if ($model->birthdayArr[0]==$v):?>
                                            <option  selected = "selected" value="<?=$v?>"><?=$v?>年</option>
                                        <?php else:?>
                                            <option value="<?=$v?>"><?=$v?>年</option>
                                      <?php endif;?>

                                    <?php endforeach;?>

								</select>
								<select name="moth" id="">
									<option value="">请选择月份</option>
                                    <?php foreach ($model->moth as $mk=>$mv):?>

                                        <?php if ($model->birthdayArr[1]==$mv):?>
                                            <option selected = "selected" value="<?=$mv?>"><?=$mv?>月</option>
                                        <?php else:?>
                                            <option value="<?=$mv?>"><?=$mv?>月</option>
                                        <?php endif;?>

                                    <?php endforeach;?>

								</select>
								<select name="day" id="">
									<option value="">请选择日期</option>
                                    <?php foreach ($model->day as $dk=>$dv):?>

                                        <?php if ($model->birthdayArr[2]==$dv):?>
                                            <option selected = "selected" value="<?=$dv?>"><?=$dv?>日</option>
                                      <?php else:?>
                                            <option value="<?=$dv?>"><?=$dv?>日</option>
                                      <?php endif;?>

                                    <?php endforeach;?>

								</select>
							</div>
						</li>
						<li class="user_li2">
                             <input type="hidden"  name="sex" value="">
							<label for="" class="user_li2_label"><span>*</span>性别</label>
							<div class="user_sex">

                                    <label class="radio-inline radio_fr <?=$model->sex==1?'active':''?>" tab="1">
                                        <i class="icon icon_select_lg"></i>男
                                    </label>

                                    <label class="radio-inline <?=$model->sex==2?'active':''?>" tab="2">
                                        <i class="icon icon_select_lg"></i>女
                                    </label>

							</div> 
						</li>
						<li class="user_li2"><a href="javascript:void(0)" class="user_btn1" data-id="submit">保存信息</a><a href="" class="user_btn2">取消</a></li>

					</ul>
				</form>
			</div>
			

		</div>

	</div>
<script>
    //图片上传
    $(".sc_head_btn").on('change',"input[type=file]",function () {

        var filePath = $(this).val();  //读取图片路径
        var imgobj=this.files[0];  //读取图片对象
        var formData = new FormData();
        formData.append("file", imgobj);
        $.ajax({
            type: "POST", // 数据提交类型
            url: "<?=UrlService::build(['personal/uploadimg'])?>", // 发送地址
            data: formData, //发送数据
            async: true, // 是否异步
            dataType: 'json',
            processData: false, //processData 默认为false，当设置为true的时候,jquery ajax 提交的时候不会序列化 data，而是直接使用data
            contentType: false, //
            success: function (re) {
                if (re.code == 0) {
                    $("#memberPic").attr('src',re.attachment);
                    $("#header_pic").val(re.attachment);
                    $("#header_picTm").val(re.attachmentTm);
                }
            },
            error: function () {
                alert("上传失败")
            },
        });
    });

    $('[data-id="submit"]').click(function () {


        var name = $('input[name="name"]');
            // birthday_year = $('select[name="year"]'),
            // birthday_moth = $('select[name="moth"]'),
            // birthday_day = $('select[name="day"]');

        if (name.val().length <1) {
            if (name.attr('data-default').length < 1) {
                alert("请输入收件人姓名");
                return false;
            }
            name.val(name.attr('data-default'));
        }
        var sexNum=$(".user_sex").children('.active').attr('tab');
        $('input[name="sex"]').val(sexNum);
        $('form[data-id="join"]').submit();
    });

    //性别选择
    $('.radio-inline').click(function(e){
        $(this).addClass('active').siblings().removeClass('active');
    });
</script>
<?php include APP_PATH . '/views/common/footer.php';?>

