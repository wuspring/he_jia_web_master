
<?php
use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;
use common\models\IntergralOrder;
include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>


	<div class="mx-auto w1200 clearfix pb-5 mt-3">
        <?php include __DIR__.'/common/left-column.php' ?>
		<div class="user_content float-right">
			<p class="case_title"><span class="green">全部订单</span></p>
			<div class="casedetail">
							<div class="casedetail_flow">								
								<ul class="list-unstyled cdf_right">
									<li class="<?=($intergralOrder->order_state >= IntergralOrder::STATUS_WAIT_PAY)?'active':'grays'?>">
										<p class="p1">拍下商品</p>
										<span>1</span>

									</li>
									<li class="<?=($intergralOrder->order_state >= IntergralOrder::STATUS_WAIT_SEND)?'active_line':'state_line'?>">
									</li>
									<li class="<?=($intergralOrder->order_state >= IntergralOrder::STATUS_WAIT_SEND)?'active':'grays'?>">
										<p class="p1">付款成功</p>
										<span>2</span>
									</li>
									<li class="<?=($intergralOrder->order_state >= IntergralOrder::STATUS_WAIT_SEND)?'active_line':'state_line'?>">
									</li>
									<li class="<?=($intergralOrder->order_state >= IntergralOrder::STATUS_WAIT_RECIVE)?'active':'grays'?>">
										<p class="p1">卖家发货</p>
										<span>3</span>

									</li>
									<li class="<?=($intergralOrder->order_state >= IntergralOrder::STATUS_WAIT_RECIVE)?'active_line':'state_line'?>">
									</li>
									<li class="<?=($intergralOrder->order_state >= IntergralOrder::STATUS_WAIT_JUDGE)?'active':'grays'?>">
										<p class="p1">确认收货</p>
										<span>4</span>
									</li>
									<li style="display: none" class="<?=($intergralOrder->order_state >= IntergralOrder::STATUS_WAIT_JUDGE)?'active_line':'state_line'?>">
									</li>
									<li style="display: none" class="<?=($intergralOrder->order_state >= IntergralOrder::STATUS_CLOSE)?'active':'grays'?>">
										<p class="p1">立即评价</p>
										<span>5</span>
									</li>
								</ul>
							</div>
							<div class="case_detail_c">
								<div class="case_detai_c_tit">订单信息</div>
								<div class="case_detail_c_main">
									<div class="case_detail_l" style="border-right: 1px solid #BADE6E;">
										<div class="case_detail_l_li">
											<span>收货地址：</span>
											<div class="case_detail_l_li_main"><?=$intergralOrder->receiver_address?></div>
										</div>
										<div class="case_detail_l_li">
											<span>买家留言：</span>
											<div class="case_detail_l_li_main">轻拿轻放</div>
										</div>
										<div class="case_detail_l_li">
											<span>订单编号：</span>
											<div class="case_detail_l_li_main"><?=$intergralOrder->orderid?></div>
										</div>
									</div>
									<div class="case_detail_r" style="border-left: none">
										<div class="case_detail_r_li">
											<span>订单状态：</span>
											<div class="case_detail_r_li_main"><?=$intergralOrder->orderState?></div>
										</div>
										<div class="case_detail_r_li">
											<p style="display: none">您可以<a href="javascript:void(0)">申请退款</a></p>
										</div>
										
									</div>
								</div>
							</div>
							<div class="casedetail_con">
								<table class="table mycase_list_table order_detail_title">
									<thead>
										<tr>
											<th class="td1">商品</th>
											<th class="td2">单价</th>
											<th class="td3">数量</th>
											<th class="td8">状态</th>
										</tr>
									</thead>
								</table>
								<table class="table mycase_list_table">
									<tbody>
										<tr class="tr_id">
											<td colspan="9"><span><?=$intergralOrder->add_time?></span>订单号：<?=$intergralOrder->orderid?></td>
										</tr>
                                        <?php foreach ($intergralOrderGood as $orderGood_k=>$orderGood_v):?>
                                            <tr>
                                                <td class="td1">
                                                    <a href="<?=UrlService::build(['intergral-goods/detail','id'=>$orderGood_v->goods_id])?>" class="media">
                                                        <img src="<?=$orderGood_v->goods_pic?>" class="align-self-center"
                                                             style="width:80px">
                                                        <div class="media-body mt-1">
                                                            <h4><?=$orderGood_v->goods_name?></h4>
                                                            <p><?=$orderGood_v->attr?></p>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td class="td2"><span><?=intval($orderGood_v->goods_price)?></span></td>
                                                <td class="td3">× <?=$orderGood_v->goods_num?></td>
                                                <td class="td8" rowspan="2"><?=$intergralOrder->orderState?></td>
                                            </tr>
                                        <?php endforeach;?>
									</tbody>
								</table>					
								<div class="order_detail_bot">					
									<dl>
										<dd class="dd"><span><?=intval($intergralOrder->order_amount)?></span>积分</dd>
										<dt>实付款：</dt>
									</dl>
								</div>
							</div>
						</div>
			

		</div>
		
	</div>

<?php include APP_PATH . '/views/common/footer.php';?>



