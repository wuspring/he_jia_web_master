<?php

use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>

<div class="mx-auto w1200 clearfix pb-5 mt-3">
    <?php include __DIR__ . '/common/left-column.php' ?>
    <div class="user_content float-right">
        <p class="case_title"><span class="green">我的积分</span></p>
        <div class="myjifen mt-4">
            <div class="myjifen_top">
                <div class="myjifen_top_l">
                    <p>我的可用积分</p>
                    <h1><?=$model->integral?></h1>
                </div>
                <div class="myjifen_top_r">
                    <a href="javascript:void(0)" class="btn btn-warning myjifen_btn1" data-toggle="modal" data-target="#myModal"><img src="/public/index/images/icon_signin.jpg" alt="">每日签到</a>
                    <a href="<?=UrlService::build(['integral/index'])?>" class="btn btn-warning myjifen_btn">积分换好礼</a>
                </div>
            </div>
            <div class="myjifen_bot">
                <div class="myjifen_bot_tit"><span>积分明细</span></div>
                <div class="myjifen_bot_main">
                    <div class="myjifen_bot_main_top">
                        <ul class="list-unstyled">
                            <li>时间</li>
                            <li>金额</li>
                            <li>描述</li>
                        </ul>
                    </div>
                    <div class="myjifen_bot_main_list">
                        <?php foreach ($integrallon as $k=>$v):?>
                            <ul class="list-unstyled">
                                <li><?=$v->create_time?></li>
                                <li class="<?= $v->pay_integral>0 ? 'green' : 'red'; ?>"><?= abs($v->pay_integral)?></li>
                                <li><?=$v->expand?></li>
                            </ul>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
            <?php echo $pageHtml?>
        </div>


    </div>

</div>
<!-- 签到弹框 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-qiandao">
        <div class="modal-content">
            <div class="modal-header modal-header2">
                <p>签到提示</p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body pt-4 pb-4">
                <div class="qiandao">
                    <div class="qiandao_l">
                        <div style="" id="calendar"></div>
                    </div>
                    <div class="qiandao_r">
                        <?php if ($isSign) :?>
                        <div class="qiandao_r_top">
                            <h2><img src="/public/index/images/icon_signin_ok.jpg" alt="">今日已签到成功</h2>
                            <p>获得<span><?= $signIntegral; ?>积分</span></p>
                        </div>
                        <?php else :?>
                            <div class="qiandao_r_top">
                                <h2><img src="/public/index/images/icon_signin.jpg" alt="">今日尚未签到</h2>
                            </div>
                        <?php endif; ?>
                        <div class="qiandao_r_bot">
                            <?= $explain; ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!--签到js-->
<script type="text/javascript" src="/public/index/js/calendar2.js"></script>
<script type="text/javascript">
    var signList= <?= json_encode($signLogs); ?>;
    $(function(){
        //ajax获取日历json数据
        calUtil.init(signList);
    });
</script>

<?php include APP_PATH . '/views/common/footer.php'; ?>

