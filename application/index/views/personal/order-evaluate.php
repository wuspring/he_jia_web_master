<?php

use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>


<div class="mx-auto w1200 clearfix" id="shopdetail1">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="index.html">首页</a>
        <span class="breadcrumb-item active">商品评价</span>
    </nav>
    <?php foreach ($order->orderGoodsList as $k=>$v):?>
       <div class="myassess_product clearfix">
           <div class="myassess_proimg float-left">
               <img src="<?=$v['goods_pic']?>" alt="" class="img">
           </div>
           <div class="myassess_procon float-left">
               <h3><?=$v['goods_name']?></h3>
               <p class="p_price red">促销价<span>￥<?=$v['goods_price']?></span></p>
               <div class="d_spec">
                   <span class="title float-left">规格</span>
                   <p><?=$v['attr']?></p>
               </div>
           </div>
       </div>
        <div class="myassess_content">

            <p class="p">我们很在意您的评价，感谢您的描述！</p>
            <p class="p_star">
                商品质量：<span class="spzl"><em></em><em></em><em></em><em></em><em></em></span>商户服务：<span class="shfw"><em></em><em></em><em></em><em></em><em></em></span>
            </p>
            <textarea class="textarea" placeholder="请输入您的评语~"></textarea>
            <div class="pingjia_pics">
                <div class="tuihuo_li4">
                    <div class="tuihuo_pics">
                        <ul class="list-unstyled">
                            <li class="upload_btn">
                                <input type="file"  multiple accept="image/*" class="sc_zhaopian">
                                <img src="/public/index/images/img_add.jpg" class="sc_bg" alt="">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach;?>

    <a href="javascript:void(0)" class="btn btn-danger tiJiao" style="margin-top: 30px">提交评价</a>

</div>

<script type="text/javascript">
    var imgArr = new Array();
    var spzl=new Array();  //质量评分
    var shfw=new Array(); //商户评分
    var textarea=new Array();  //内容
    var is_sucess=0;
    $(function () {
        $(".p_star span em").mouseenter(function () {
            $(this).addClass('active').siblings().addClass('active');
            $(this).nextAll().removeClass('active');
        })
    });

    $(".tiJiao").on('click',function () {
        //质量评分
        $(".spzl").each(function(index,item){
            spzl.push($(this).children('.active').length);
        });

        //服务评分
        $(".shfw").each(function(index,item){
            shfw.push($(this).children('.active').length);
        });

        //内容信息
        $(".textarea").each(function (index,item) {
            textarea.push($(this).val());
        });
        //图片数组
        $(".list-unstyled").each(function (index,item) {

           var img_arr=$(this).children('li:not(".upload_btn")').children('img');
           var arr=[];
           $.each(img_arr,function (ind,objff) {
               arr.push($(objff).attr('src'));
           });
            imgArr.push(arr);
            arr=[];
        });
        $.ajax({
            type: "POST", // 数据提交类型
            url: "<?=UrlService::build(['personal/submit-evaluate']);?>", // 发送地址
            data: {'order_id':"<?=$order->orderid?>","spzl":spzl,"shfw":shfw,"textarea":textarea,"img":imgArr}, //发送数据
            async: true, // 是否异步
            dataType: 'json',
            success: function (res) {
                  is_sucess=res.code;
                  alert(res.msg ,function (res) {
                      if (is_sucess==1){
                          window.location.href=rebuildQueryString("<?=UrlService::build(['personal/appointment-order'])?>");
                      }
                  });
            },
        });

    });

    //当点击弹层
    $(document).on('click','.modal-backdrop',function () {
        if (is_sucess==1){
          window.location.href=rebuildQueryString("<?=UrlService::build(['personal/appointment-order'])?>");
        }
    });
    // 上传图片过程
    $(".upload_btn").on('change',"input[type=file]",function () {
        var parent_ul=$(this).parent().parent().children('li:not(".upload_btn")').length;
        var filePath = $(this).val();  //读取图片路径
        var imgobj=this.files;  //读取图片对象
        var skuimg=$(this);
        var count=parseInt(parent_ul)+imgobj.length;
        if (count>6){
            alert("不能超过5张");
            return false;
        }

         for (var i = 0; i < imgobj.length; i++) {
                if ((imgobj.length + i) <= 5) {
                    var formData = new FormData();
                    formData.append("file", this.files[i]);
                    $.ajax({
                        type: "POST", // 数据提交类型
                        url: "<?=UrlService::build(['personal/uploadimgv']);?>", // 发送地址
                        data: formData, //发送数据
                        async: true, // 是否异步
                        dataType: 'json',
                        processData: false, //processData 默认为false，当设置为true的时候,jquery ajax 提交的时候不会序列化 data，而是直接使用data
                        contentType: false, //
                        success: function (re) {
                            if (re.code == 0) {

                                var html = '<li>\n' +
                                    ' <em class="closeImg">×</em>\n' +
                                    ' <img src="'+re.attachment+'" class="sc_pic" alt="">\n' +
                                    ' </li>';
                                $(skuimg).parent().parent().append(html);

                            }
                        },
                        error: function () {
                            alert("上传失败")
                        },
                    });
                } else {
                    //file.val('');
                    alert("最多上传5张图片");
                    break;
                }
            }

    });

    //删除图片
    $(document).on('click', '.closeImg', function () {
        $(this).parent().remove();
        var url = $(this).parent().children("img").attr("src");
        // imgArr.splice($.inArray(url, imgArr), 1);
    });


</script>

<?php include APP_PATH . '/views/common/footer.php'; ?>