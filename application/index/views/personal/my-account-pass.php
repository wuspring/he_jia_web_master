<?php

use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;
use yii\bootstrap\ActiveForm;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>


    <div class="mx-auto w1200 clearfix pb-5 mt-3">
        <?php include __DIR__ . '/common/left-column.php' ?>
        <div class="user_content float-right">
            <p class="case_title"><span class="green">修改密码</span></p>

            <div class="myaccount">

                <div class="account_pass">
                    <ul>
                        <li><label for="">原密码：</label><input type="password" name="oldPwd" autocomplete="off"
                                                             class="account_input"></li>
                        <li><label for="">新密码：</label><input type="password" name="newPwd" autocomplete="off"
                                                             class="account_input"></li>
                        <li><label for="">确认新密码：</label><input type="password" name="newPwdTwo" autocomplete="off"
                                                               class="account_input"></li>
                        <a href="javascript:void(0)" class="btn btn-success account_btn">提交修改</a>
                    </ul>
                </div>

            </div>

        </div>

    </div>

    <script>

        //点击提交
        $(".account_btn").on('click', function () {
            var oldPwd = $("input[name='oldPwd']").val(),
                newPwd = $("input[name='newPwd']").val(),
                newPwdTwo = $("input[name='newPwdTwo']").val();
            $.ajax({
                type: "POST", // 数据提交类型
                url: "<?=UrlService::build(['personal/my-account-pass'])?>", // 发送地址
                data: {"oldPwd": oldPwd, "newPwd": newPwd, "newPwdTwo": newPwdTwo}, //发送数据
                async: true, // 是否异步
                dataType: 'json',
                success: function (re) {
                    if (re.code == 1) {
                        alert("修改密码成功");
                        window.location.href = "<?=UrlService::build(['personal/my-account'])?>"
                    } else {
                        alert("修改密码失败")
                    }
                },
                error: function () {
                    alert("异常")
                },
            });


        })

    </script>


<?php include APP_PATH . '/views/common/footer.php'; ?>