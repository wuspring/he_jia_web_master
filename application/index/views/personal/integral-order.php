<?php

use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;
use \common\models\IntergralOrder;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>

<div class="mx-auto w1200 clearfix pb-5 mt-3">
    <?php include __DIR__.'/common/left-column.php' ?>
    <script>
        $(function () {
            <?php
            if ($_GET) {
                echo 'var obj=' . json_encode($_GET) . ';';
            }
            ?>
            if (typeof(obj) != 'undefined') {
                for(k in obj){
                    if (k=='near_time'){

                        $("option[data-type='"+obj[k]+"']").attr('selected','true');
                    }
                    if (k=='order_status'){
                        if (obj[k]){
                            $("option[data-type='"+obj[k]+"']").attr('selected','true');
                        }
                    }
                    if (k=='orderid'){
                        if (obj[k]){
                            $("#orderNum").val(obj[k]);
                        }
                    }
                }
            }
        });

    </script>
    <div class="user_content float-right">

        <p class="case_title"><span class="green">全部订单</span></p>
        <div class="case_search">
            <div class="form-inline row">
                <div class="form-group col-md-6">
                    <input type="input" class="form-control" id="orderNum"  placeholder="请输入订单号">
                    <button type="submit" id="searchSingleOrder" class="btn btn-success">搜索</button>
                </div>
                <!-- <div class="col-md-2"></div> -->
                <div class="form-group  col-md-6">
                    <label for="sel1">下单时间：</label>
                    <!-- 下拉框内容待定 -->
                    <select class="form-control" id="sel1">
                        <option value="all" data-type="all">全部</option>
                        <option value="week" data-type="week">近一周</option>
                        <option value="month" data-type="month">近一个月</option>
                        <option value="threeMonth" data-type="threeMonth">近三个月</option>
                    </select>
                    <label for="sel2">订单状态：</label>
                    <select class="form-control" id="sel2">
                        <option value=" ">全部</option>
                        <option value="0" data-type="0">待付款</option>
                        <option value="1" data-type="1">待使用</option>
                        <option value="3" data-type="3">待评价</option>
                        <option value="4" data-type="4">已评价</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="mycase_list">
            <!-- 全部订单 -->
            <table class="table mycase_list_table">
                <thead>
                <tr>
                    <th class="td1">商品</th>
                    <th class="td2">单价</th>
                    <th class="td3">数量</th>
                    <th class="td4">实付款</th>
                    <th class="td8">订单状态</th>
                    <th class="td9">操作</th>
                </tr>
                </thead>
            </table>

            <?php if ($orders) :?>
                <?php foreach ($orders AS $order) :?>
                    <table class="table mycase_list_table">
                        <tbody>
                        <!-- 一条订单-start -->
                        <tr class="tr_id">
                            <td colspan="9"><span><?= $order->add_time; ?></span>订单号：<?= $order->orderid; ?></td>
                        </tr>
                        <?php foreach ($order->goods AS $good) :?>
                        <tr>
                            <td class="td1">
                                <a href="<?= UrlService::build(['intergral-goods/detail', 'id' => $good->goods_id]); ?>" class="media">
                                    <img src="<?= $good->goods_pic; ?>" class="align-self-center" style="width:80px">
                                    <div class="media-body mt-1">
                                        <h4><?= $good->goods_name; ?></h4>
                                        <p><?= $good->attr; ?></p>
                                    </div>
                                </a>
                            </td>
                            <td class="td2"><span><?= intval($good->goods_price); ?></span>积分</td>
                            <td class="td3">× 1</td>
                            <td class="td4"><span><?= intval($good->goods_pay_price); ?></span>积分</td>

                            <td class="td8" rowspan="2">
                                <p class="p_lg"><?= $order->orderState; ?></p>
                                <a href="<?=UrlService::build(['personal/integral-order-detail', 'number' => $order->orderid]); ?>" class="detail">订单详情</a>
                                <?php if (!in_array($order->order_state, [IntergralOrder::STATUS_WAIT_SEND, IntergralOrder::STATUS_WAIT_PAY,IntergralOrder::STATUS_CLOSE])) :?>
                                <a style="display: none" href="<?=UrlService::build(['personal/integral-order-logistics', 'number' => $order->orderid])?>" class="detail">物流详情</a>
                                <?php endif; ?>
                            </td>
                            <td class="td9" rowspan="2">
                                <?php if ($order->order_state==IntergralOrder::STATUS_WAIT_RECIVE):?>
                                    <a href="javascript:void(0)" class="a a_color a_sure" data-toggle="modal"
                                       data-target="#myModalSure" data-id="confirm" data-val="<?= $order->orderid; ?>">确认收货</a>
                                <?php elseif ($order->order_state==IntergralOrder::STATUS_WAIT_PAY):?>
                                    <a href="javascript:void(0)" class="a a_color a_sure" data-toggle="modal"
                                       data-target="#myModalCancel" data-id="cancel" data-orderid="<?= $order->orderid; ?>">取消订单</a>
                                <?php elseif ($order->order_state==IntergralOrder::STATUS_WAIT_JUDGE):?>
                                    <a href="<?=UrlService::build(['personal/order-comment','orderId'=>$order->orderid])?>" class="a a_assess">立即评价</a>
                                <?php endif;?>

                            </td>
                        </tr>
                        <?php endforeach; ?>
                        <!-- 一条订单-end -->
                        </tbody>
                    </table>
                <?php endforeach; ?>
            <?php else :?>
            <div class="com_nodata">
                <img src="/public/index/images/img_result_nodata.png" alt="订单支付成功" class="img">
                <div class="con">
                    <p class="p1">亲，您最近没有下过订单哦~</p>
                    <a href="<?= UrlService::build('integral/index'); ?>" class="btn btn-warning">去首页逛逛</a>
                </div>
            </div>
            <?php endif; ?>
            <!-- 等待付款 -->
            <!-- 添加table_line后，边框是粉色 -->

            <?=$pageHtml; ?>

        </div>
    </div>

</div>


<!-- 模态框-确认收货 -->
<div class="modal fade" id="myModalSure" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>

            </div>
            <div class="modal-body pt-5 mt-3 mb-5 pb-4">
                是否确认收货？
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal" data-toggle="modal"
                        data-target="#myModalSuc" data-id="sendGoods">确定
                </button>
            </div>
        </div>
    </div>
</div>

<!-- 模态框-确认取消 -->
<div class="modal fade" id="myModalCancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="height: 292px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>

            </div>
            <div class="modal-body pt-5 mt-3 mb-5 pb-4">
                是否确认取消预定？
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-warning" data-id="cancelButton">确定</button>
            </div>
        </div>
    </div>
</div>


<?php include APP_PATH . '/views/common/footer.php'; ?>
<script>

    //点击取消订单
    $("[data-id=\"cancel\"]").on('click',function () {
        $('[data-id="cancelButton"]').attr('data-val', $(this).attr('data-orderid'));
        autoDomHeight($("#myModalCancel"));
    });

    //取消订单
    $("[data-id=\"cancelButton\"]").on('click',function () {

        var orderid = $(this).attr('data-val');

        var url="<?=UrlService::build(['personal/integral-order-cancel'])?>";
        $.get(url, {number : orderid}, function (res) {
                alert(res.msg);
        },'json');
        $("#myModalCancel").modal('hide');
    });

   $(document).on('click','#globalAlterShadow .btn-warning',function () {
       window.location.reload();
   });

    $('[data-id="confirm"]').click(function() {
       $('[data-id="sendGoods"]').attr('data-val', $(this).attr('data-val'));
    });

    $('[data-id="sendGoods"]').click(function () {
        var number = $(this).attr('data-val');
        $.get('<?= UrlService::build(['personal/received-intergral']); ?>', {number : number}, function (res) {
            res = JSON.parse(res);
            if (res.status) {
                alert("确认收货成功", function () {
                    window.location.reload();
                });
                return false;
            }
            alert(res.msg);
        })
    });

    //点击搜索
    $("#searchSingleOrder").on('click',function () {
        var orderNum=$("#orderNum").val();

        window.location.href = rebuildQueryString("<?=UrlService::build(['personal/integral-order'])?>", {orderid : orderNum});
    });
    //下拉搜索
    $("#sel1").on('change',function () {
        var near_time=$(this).val();
        window.location.href= rebuildQueryString("<?=UrlService::build(['personal/integral-order'])?>", {near_time : near_time});
    });
    $("#sel2").on('change',function () {
        var order_status=$(this).val();
        window.location.href= rebuildQueryString("<?=UrlService::build(['personal/integral-order'])?>", {order_status : order_status});
    })
</script>
