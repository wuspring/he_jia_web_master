<?php

use common\models\Provinces;
use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>
<style>
    .field-addressmember-province {
        float: left
    }

    .form-control-inline {
        float: left
    }

    .help-block {
        color: #f42422
    }

    .field-addressmember-address > p {
        margin-left: 120px;
    }

    .field-addressmember-nation {
        float: left;
    }
</style>
<div class="mx-auto w1200 clearfix pb-5 mt-3">
    <?php include __DIR__ . '/common/left-column.php' ?>
    <div class="user_content float-right">
        <p class="case_title"><span class="green">收货地址</span></p>
        <div class="user_box">

            <?php $form = ActiveForm::begin([
                'id' => 'formlogin',
                'class' => '',
//            'action' => Url::toRoute(['site/reg']),
                'fieldConfig' => [
                    'options' => ['class' => ''],
                    'template' => "{input}\n{hint}\n{error}",
                    'horizontalCssClasses' => [
                        'label' => '',
                        'offset' => 'col-sm-offset-4',
                        'wrapper' => '',
                        'error' => '',
                        'hint' => '',
                    ],
                ],

            ]); ?>
            <ul>
                <!--
                <li class="user_li2">
                    <label for="" class="user_li2_label"><span>*</span>手机号码</label>
                     <input type="user" class="user_input" placeholder="请输入您的手机号码">
                </li> -->
                <!-- <li class="user_li3">
                    <label for="" class="user_li3_label"><span>*</span>二维码</label>
                    <img src="/public/index/images/erweima.jpg" alt="" class="weixin">
                </li> -->
                <li class="user_li2">
                    <label for="" class="user_li2_label"><span>*</span>所在地区</label>
                    <div class="user_select">
                        <?php $form->field($address, 'nation')->dropDownList($address->typeDic) ?>
                        <?php
                        echo $form->field($address, 'province')->widget(\chenkby\region\Region::className(), [
                            'model' => $model,
                            'url' => Url::toRoute(['get-region']),
                            'province' => [
                                'attribute' => 'province',
                                'items' => Provinces::getRegion(),
                                'options' => ['class' => 'form-control form-control-inline', 'prompt' => '选择省份']
                            ],
                            'city' => [
                                'attribute' => 'city',
                                'items' => Provinces::getRegion($address['province']),
                                'options' => ['class' => 'form-control form-control-inline', 'prompt' => '选择城市']
                            ],
                            'district' => [
                                'attribute' => 'region',
                                'items' => Provinces::getRegion($address['city']),
                                'options' => ['class' => 'form-control form-control-inline', 'prompt' => '选择县/区']
                            ]

                        ])->label("城市");
                        ?>
                    </div>
                </li>
                <li class="user_li2">
                    <label for="" class="user_li2_label"><span>*</span>详细地址</label>
                    <?= $form->field($address, 'address')->textarea(['class' => 'user_input textarea', 'placeholder' => '建议您如实填写详细收货地址，例如街道名称，门牌号码，楼层和房间号等消息']) ?>
                </li>
                <li class="user_li2">
                    <label for="" class="user_li2_label">邮政编码</label>
                    <?= $form->field($address, 'post_code')->textInput(['class' => 'user_input', 'placeholder' => '如您不清楚邮编区号，请填写000000']); ?>

                </li>
                <li class="user_li2">
                    <label for="" class="user_li2_label"><span>*</span>收货人姓名</label>
                    <?= $form->field($address, 'name')->textInput(['class' => 'user_input', 'placeholder' => '请输入收件人姓名']); ?>
                </li>
                <li class="user_li2">
                    <label for="" class="user_li2_label"><span>*</span>手机号码</label>
                    <?= $form->field($address, 'mobile')->textInput(['class' => 'user_input', 'placeholder' => '请输入收件人手机号码']); ?>
                </li>
                <li class="user_li2 user_li_checkbox"><label>
                        <?= $form->field($address, 'isDefault')->checkbox()->label('设置默认收货地址') ?>
                        <!--                            <input name="is_default" type="checkbox" value="">-->
                    </label></li>
                <li class="user_li2">
                    <a href="javascript:document:formlogin.submit();" class="user_btn1" data-id="submit">保存收货地址</a>
                </li>

            </ul>
            <?php ActiveForm::end(); ?>
            <script>
                $("#formlogin").addClass('user_form');
                $("[data-id='submit']").submit();
            </script>
<!--            <p class="green mb-3">已保存了<span>2</span>条地址，还能保存<span>4</span>条</p>-->
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="receiver">收货人</th>
                    <th class="address">所在地区</th>
                    <th class="detailed">详细地址</th>
                    <th class="code">邮编</th>
                    <th class="phone">手机号码</th>
                    <th class="operating">操作</th>
                    <th class="tolerate"></th>
                </tr>
                </thead>
                <?php foreach ($alladdress as $dk => $dv): ?>
                    <tr>
                        <td class="receiver"><?= $dv->name ?></td>
                        <td class="address"><?= $dv->provinceInfo ?></td>
                        <td class="detailed"><?= $dv->address ?></td>
                        <td class="code"><?= $dv->post_code ?></td>
                        <td class="phone"><?= $dv->mobile ?></td>
                        <td class="operating blue">
                            <a href="<?=UrlService::build(['personal/my-address','addressId'=>$dv->id])?>">修改</a>|<a href="javascript:void(0)" data-id="<?= $dv->id ?>"
                                                                   data-toggle="modal"
                                                                   data-target="#myModalDelAddress">删除</button></a>
                        </td>

                        <?php if ($dv->isDefault == 1): ?>
                            <td class="tolerate blue cur">
                                <p>默认地址</p><a href="javascript:void(0)" data-id="<?= $dv->id ?>"
                                              class="a_set setDefault">设为默认地址</a>
                            </td>
                        <?php else: ?>
                            <td class="tolerate blue ">
                                <p>默认地址</p><a href="javascript:void(0)" data-id="<?= $dv->id ?>"
                                              class="a_set setDefault">设为默认地址</a>
                            </td>
                        <?php endif; ?>

                    </tr>
                <?php endforeach; ?>

            </table>
        </div>

    </div>

</div>


<!-- 模态框-确认删除 -->
<div class="modal fade" id="myModalDelAddress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>

            </div>
            <div class="modal-body pt-5 mt-3 mb-5 pb-4">
                确定要删除该地址吗？
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-warning" id="sureDelete">确定</button>
            </div>
        </div>
    </div>
</div>

<script>
    var id = 0;
    $("[data-toggle='modal']").on('click', function () {
        id = $(this).attr('data-id');
    });

    //确认设为默认地址
    $(".setDefault").on('click', function () {
        var default_id = $(this).attr('data-id');
        var url = "<?=UrlService::build(['personal/set-address'])?>";
        $.get(url, {id: default_id}, function (res) {
            if (res.code == 1) {
                alert(res.msg, function () {
                    window.location.reload();
                });
                return false;
            }

            alert(res.msg);

        }, 'json');
    });
    //确认删除
    $("#sureDelete").on('click', function () {
        $("#myModalDelAddress").hide();
        var url = "<?=UrlService::build(['personal/delete-address'])?>";
        $.get(url, {id: id}, function (res) {
            if (res.code == 1) {
                alert("删除成功", function () {
                    window.location.reload();
                });

                return false;
            }

            alert("删除失败")

        }, 'json');
    })


</script>


<?php include APP_PATH . '/views/common/footer.php'; ?>
