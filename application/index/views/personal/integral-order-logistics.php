<?php

use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>


<div class="mx-auto w1200 clearfix pb-5 mt-3">
    <?php include __DIR__ . '/common/left-column.php' ?>
    <div class="user_content float-right">
        <p class="case_title"><span class="green">全部订单</span></p>
        <div class="wuliu_top">
            <div class="wuliu_top_li">
                <ul class="list-unstyled">
                    <li>运单号码：<span>75115748563547895</span></li>
                    <li>物流公司：<span>中通快递</span></li>
                    <li>客服电话：<span>95311</span></li>
                </ul>
            </div>
            <div class="wuliu_top_li">
                <ul class="list-unstyled">
                    <li>收货地址：<span>河南省洛阳市洛龙区</span><span>000000</span><span>张三</span><span>15900000005</span></li>
                </ul>
            </div>
        </div>
        <div class="wuliu_main">
            <div class="wuliu_main_top">已签收</div>
            <div class="wuliu_main_lists">
                <div class="wuliu_main_list1"><span></span>2019-01-01 12:00:00 您已在政和国际四楼中细软完成签收</div>
                <div class="wuliu_main_list"><span></span>2019-01-01 12:00:00 [洛阳市]快件已到达洛阳快递员正在派送中</div>
                <div class="wuliu_main_list"><span></span>2019-01-01 12:00:00 [洛阳市]快件已到达洛阳快递员正在派送中</div>
                <div class="wuliu_main_list"><span></span>2019-01-01 12:00:00 [洛阳市]快件已到达洛阳快递员正在派送中</div>
                <div class="wuliu_main_list"><span></span>2019-01-01 12:00:00 [洛阳市]快件已到达洛阳快递员正在派送中</div>
                <div class="wuliu_main_list"><span></span>2019-01-01 12:00:00 [洛阳市]快件已到达洛阳快递员正在派送中</div>
                <div class="wuliu_main_list"><span></span>2019-01-01 12:00:00 包裹正在等待揽件</div>
            </div>

        </div>


    </div>

</div>

<?php include APP_PATH . '/views/common/footer.php'; ?>

  
