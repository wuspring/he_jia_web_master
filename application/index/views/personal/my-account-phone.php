<?php

use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>

<div class="mx-auto w1200 clearfix pb-5 mt-3">
    <?php include __DIR__ . '/common/left-column.php' ?>
    <div class="user_content float-right">
        <p class="case_title"><span class="green">手机验证</span></p>
        <div class="myaccount">
            <div class="account_pass">
                <ul>
                    <li><label for="">手机号：</label><input type="text" name="mobile" class="account_input"></li>
                    <li><label for="">验证码：</label><input type="text" name="yzm" class="account_inputs"><a
                                href="javascript:void(0)" class="yzm">验证码</a></li>
                    <a href="javascript:void(0)" class="btn btn-success account_btn tiyz">提交验证</a>
                </ul>
            </div>

        </div>


    </div>

</div>
<script>
    //点击提交
    $(".tiyz").on('click', function () {
        var mobile = $("input[name='mobile']").val(),
            yzm = $("input[name='yzm']").val();

        $.ajax({
            type: "POST",   // 数据提交类型
            url: "<?=UrlService::build(['personal/my-account-phone'])?>", // 发送地址
            data: {"mobile": mobile, "yzm": yzm}, //发送数据
            async: true,   // 是否异步
            dataType: 'json',
            success: function (re) {
                if (re.code == 1) {
                    alert("绑定成功");
                    window.location.href = "<?=UrlService::build(['personal/my-account'])?>"
                } else {
                    alert("绑定失败")
                }
            },
            error: function () {
                alert("异常")
            },
        });

    });
    //验证码
    $(".yzm").on('click', function () {
        var mobile = $("input[name='mobile']").val();

        if (/^1\d{10}$/.test(mobile)) {
            $.ajax({
                type: "POST", // 数据提交类型
                url: "<?=UrlService::build(['api/get-code'])?>", // 发送地址
                data: {"mobile": mobile}, //发送数据
                async: true, // 是否异步
                dataType: 'json',
                success: function (re) {
                    if (re.status == 1) {
                        sendmsg();
                    }
                },
                error: function () {
                    alert("异常")
                },
            });

        } else {
            alert("请输入正确的手机号码");
        }

    })

    //验证码60秒倒计时
    var countdown = 60;

    function sendmsg() {
        if (countdown == 0) {
            $(".yzm").attr("disabled", false);
            $(".yzm").text("验证码");
            countdown = 60;
            return false;
        } else {

            $(".yzm").attr("disabled", true);
            $(".yzm").text(countdown + "s");
            countdown--;
        }
        setTimeout(function () {
            sendmsg();
        }, 1000);
    }


</script>

<?php include APP_PATH . '/views/common/footer.php'; ?>
