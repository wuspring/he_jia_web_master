<?php

use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>
<div class="mx-auto w1200 clearfix pb-5 mt-3">
    <?php include __DIR__ . '/common/left-column.php' ?>
    <div class="user_content float-right">

        <p class="case_title"><span class="green">我的足迹</span></p>
        <?php foreach ($timeArr as $k => $v): ?>
            <div class="collect_main">
                <div class="footer_time"><?= $v ?></div>
                <?php foreach ($history as $hk => $hv): ?>
                    <?php $ymd = date('Y-m-d', strtotime($hv->create_time));
                    if ($ymd == $v): ?>
                        <div class="collect_list">
                            <div class="collect_list_top">
                                <a href="javascript:void(0)" class="deleteHistory" data-id="<?= $hv->id ?>">删除记录</a>
                            </div>
                            <div class="collect_list_cont">
                                <a href="<?=UrlService::build(['goods/detail','id'=>$hv->goods_id])?>">
                                    <img src="<?=$hv->Goods->goods_pic?>" alt="<?=$hv->Goods->goods_name?>">
                                    <div class="collect_pro_tit ellipsis-2"><?=$hv->Goods->goods_name?></div>
                                    <div class="collect_pro_price">￥<?=$hv->Goods->goods_price?></div>
                                </a>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>

        <?php endforeach; ?>

    </div>

</div>
<script>
    //删除商品
    $(".deleteHistory").on('click', function () {
        var id = $(this).attr('data-id');
        var url = "<?=UrlService::build(['personal/delete-history'])?>";
        $.get(url, {'id': id}, function (res) {
                alert(res.msg)
        }, 'json');
    });
    //点击确定
    $(document).on('click','#globalAlterShadow .btn-warning',function () {
        window.location.reload();
    });
    //添加购物车
    $(".addCart").on('click', function () {
        var id = $(this).attr('data-id');
        var url = "<?=UrlService::build(['personal/add-cart'])?>";
        $.get(url, {'good_id': id}, function (res) {
            if (res.code == 1) {
                alert(res.msg);
            } else {
                alert(res.msg)
            }
            window.location.reload();
        }, 'json');
    })
</script>

<?php include APP_PATH . '/views/common/footer.php'; ?>

