<?php

use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>

<div class="mx-auto w1200 clearfix pb-5 mt-3">
    <?php include __DIR__ . '/common/left-column.php' ?>
    <div class="user_content float-right">
        <p class="case_title"><span class="green">用户信息</span></p>
        <div class="user_box">
            <form action="" class="user_form">
                <ul>
                    <li class="user_li1">
                        <label for="" class="user_li1_label"><span>*</span>当前头像</label>
                        <div class="user_head">
                            <div class="pic"><img src="/public/index/images/img_default_user.jpg" alt="头像"></div>
                            <div class="sc_head_btn"><input type="file" name=""><a href="javascript:void(0)"
                                                                                   class="sc_head">上传头像</a></div>
                        </div>
                    </li>
                    <li class="user_li2">
                        <label for="" class="user_li2_label"><span>*</span>用户名</label>
                        <input type="user" class="user_input" placeholder="请输入您的用户名">
                    </li>
                    <!--
                    <li class="user_li2">
                        <label for="" class="user_li2_label"><span>*</span>手机号码</label>
                         <input type="user" class="user_input" placeholder="请输入您的手机号码">
                    </li> -->
                    <!-- <li class="user_li3">
                        <label for="" class="user_li3_label"><span>*</span>二维码</label>
                        <img src="/public/index/images/erweima.jpg" alt="" class="weixin">
                    </li> -->
                    <li class="user_li2">
                        <label for="" class="user_li2_label"><span>*</span>生日</label>
                        <div class="user_select">
                            <select name="" id="">
                                <option value="">请选择年份</option>
                                <option value="">2019年</option>
                                <option value="">2018年</option>
                                <option value="">2017年</option>
                                <option value="">2016年</option>
                                <option value="">2015年</option>
                                <option value="">2014年</option>
                                <option value="">2013年</option>
                                <option value="">2012年</option>
                            </select>
                            <select name="" id="">
                                <option value="">请选择月份</option>
                                <option value="">1月</option>
                                <option value="">2月</option>
                                <option value="">3月</option>
                                <option value="">4月</option>
                                <option value="">5月</option>
                                <option value="">6月</option>
                                <option value="">7月</option>
                                <option value="">8月</option>
                                <option value="">9月</option>
                                <option value="">10月</option>
                                <option value="">11月</option>
                                <option value="">12月</option>
                            </select>
                            <select name="" id="">
                                <option value="">请选择日期</option>
                                <option value="">1日</option>
                                <option value="">2日</option>
                                <option value="">3日</option>
                                <option value="">4日</option>
                                <option value="">5日</option>
                                <option value="">6日</option>
                                <option value="">7日</option>
                                <option value="">8日</option>
                                <option value="">9日</option>
                            </select>
                        </div>
                    </li>
                    <li class="user_li2">
                        <label for="" class="user_li2_label"><span>*</span>性别</label>
                        <div class="user_sex">
                            <label class="radio-inline radio_fr">
                                <i class="icon icon_select_lg"></i>男
                            </label>
                            <label class="radio-inline active">
                                <i class="icon icon_select_lg"></i>女
                            </label>
                        </div>
                    </li>
                    <li class="user_li2"><a href="" class="user_btn1">保存信息</a><a href="" class="user_btn2">取消</a></li>


                </ul>
            </form>
        </div>


    </div>

</div>
<?php include APP_PATH . '/views/common/footer.php'; ?>

