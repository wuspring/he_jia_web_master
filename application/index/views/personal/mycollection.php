<?php

use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;
use common\models\Collect;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>

 <div class="mx-auto w1200 clearfix pb-5 mt-3">

     <?php include __DIR__ . '/common/left-column.php' ?>

     <div class="user_content float-right">
			<div class="my_collect_top float-right">
				<!-- <a href="" class="collect_guanli">批量管理</a> -->
<!--				<form action="" class="collect_form">-->
<!--					<input type="text" class="collect_input" placeholder="请输入商品名称">-->
<!--					<button class="collect_btn">搜 索</button>-->
<!--				</form>-->
			</div>
           <div class="caselist_status float-right">
             <a href="<?=UrlService::build(['personal/my-collection','type' => Collect::TYPE_GOOD])?>" class="active">商品收藏</a>|<a href="<?=UrlService::build(['personal/my-collection','type' => Collect::TYPE_STORE])?>">店铺收藏</a>
             </div>
          <p class="case_title"><span class="green">我的收藏</span></p>

       <?php foreach ($collectDay as $k=>$v):?>
           <div class="collect_main">
               <div class="footer_time"><?=date('Y-m-d',strtotime($v))?></div>
                   <?php foreach ($collect as $collect_k=>$collect_v):?>
                    <?php if ($collect_v->create_time==$v):?>
                           <div class="collect_list">
                           <div class="collect_list_top cancel-collect" data-collectType="<?=$collect_v->type?>" data-goodid="<?=$collect_v->id?>" >
                               <a href="javascript:void(0)">取消收藏</a>
                           </div>
                           <div class="collect_list_cont">
                               <a href="<?=UrlService::build(['goods/detail','id'=>$collect_v->goods_id])?>">
                                   <img src="<?=$collect_v->goods->goods_pic?>" alt="">
                                   <div class="collect_pro_tit ellipsis-2"><?=$collect_v->goods->goods_name?></div>
                                   <div class="collect_pro_price">¥<?=$collect_v->goods->goods_price?></div>
                               </a>
                           </div>
                          </div>
                    <?php endif;?>
                   <?php endforeach;?>
           </div>
        <?php endforeach;?>
         <?php echo  $pageHtml?>
		</div>


 </div>


 <?php include APP_PATH . '/views/common/footer.php'; ?>
    
<script>

    //取消收藏商品
    $(".cancel-collect").on('click',function () {

        var collectId=$(this).attr('data-goodid');
        var collectType=$(this).attr('data-collectType');
        var url="<?=UrlService::build(['personal/cancel-my-collect'])?>";
        $.get(url,{collectId:collectId,collectType:collectType},function (res) {
                alert(res.msg);
        },'json');
    });
    //点击取消
    $(document).on('click','#globalAlterShadow .btn-warning',function () {
        window.location.reload();
    })

</script>
