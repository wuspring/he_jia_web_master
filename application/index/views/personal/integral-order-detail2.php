<?php

use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>


<div class="mx-auto w1200 clearfix pb-5 mt-3">
    <?php include __DIR__.'/common/left-column.php' ?>
    <div class="user_content float-right">
        <p class="case_title"><span class="green">全部订单</span></p>
        <div class="casedetail">
            <div class="casedetail_flow">
                <ul class="list-unstyled cdf_right">
                    <li class="active">
                        <p class="p1">拍下商品</p>
                        <span>1</span>
                        <p class="p2">2018-07-22 10:21:50</p>
                    </li>
                    <li class="active_line">
                    </li>
                    <li class="active">
                        <p class="p1">付款成功</p>
                        <span>2</span>
                        <p class="p2">2018-07-22 10:21:50</p>
                    </li>
                    <li class="active_line">
                    </li>
                    <li class="active">
                        <p class="p1">卖家发货</p>
                        <span>3</span>
                        <p class="p2">2018-07-22 10:21:50</p>
                    </li>
                    <li class="active_line">
                    </li>
                    <li class="grays">
                        <p class="p1">确认收货</p>
                        <span>4</span>
                    </li>
                    <li class="state_line">
                    </li>
                    <li class="grays">
                        <p class="p1">立即评价</p>
                        <span>5</span>
                    </li>
                </ul>
            </div>
            <div class="case_detail_c">
                <div class="case_detai_c_tit">订单信息</div>
                <div class="case_detail_c_main">
                    <div class="case_detail_l">
                        <div class="case_detail_l_li">
                            <span>收货地址：</span>
                            <div class="case_detail_l_li_main">张三，12232899879，北京市房山区长阳路中细软大厦</div>
                        </div>
                        <div class="case_detail_l_li">
                            <span>买家留言：</span>
                            <div class="case_detail_l_li_main">轻拿轻放</div>
                        </div>
                        <div class="case_detail_l_li">
                            <span>订单编号：</span>
                            <div class="case_detail_l_li_main">2839128390128021</div>
                        </div>
                    </div>
                    <div class="case_detail_r">
                        <div class="case_detail_r_li">
                            <span>订单状态：</span>
                            <div class="case_detail_r_li_main">卖家已发货，等待收货</div>
                        </div>
                        <div class="case_detail_r_li">
                            <span>物流状态：</span>
                            <div class="case_detail_r_li_main">
                                韵达快递 运单号：28392813
                                <p>2019-1-1 12:00:00 洛阳市中细软国际前台已签收</p>
                            </div>

                        </div>
                        <div class="case_detail_r_li">
                            <p class="pl-4">您可以<a href="mylogistics.html" class="btn btn-warning">查看物流</a></p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="casedetail_con">
                <table class="table mycase_list_table order_detail_title">
                    <thead>
                    <tr>
                        <th class="td1">商品</th>
                        <th class="td2">单价</th>
                        <th class="td3">数量</th>
                        <th class="td8">状态</th>
                    </tr>
                    </thead>
                </table>
                <table class="table mycase_list_table">
                    <tbody>
                    <tr class="tr_id">
                        <td colspan="9"><span>2018-07-22 10:21:50</span>订单号：201807221021501111</td>
                    </tr>

                        <tr>
                            <td class="td1">
                                <a href="productdetail.html" class="media">
                                    <img src="/public/index/images/_temp/img11.jpg" class="align-self-center"
                                         style="width:80px">
                                    <div class="media-body mt-1">
                                        <h4>女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装女装</h4>
                                        <p>颜色：白色</p>
                                    </div>
                                </a>
                            </td>
                            <td class="td2">￥<span>120.00</span></td>
                            <td class="td3">× 2</td>
                            <td class="td8" rowspan="2">卖家发货</td>
                        </tr>



                    </tbody>
                </table>
                <div class="order_detail_bot">
                    <dl>
                        <dd class="dd"><span>2207</span>积分</dd>
                        <dt>实付款：</dt>
                    </dl>
                </div>
            </div>
        </div>


    </div>

</div>

<?php include APP_PATH . '/views/common/footer.php'; ?>


