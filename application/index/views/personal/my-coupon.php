<?php

use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>
    <style>
        .hxm {
            font-size: 15px;
            margin-top: 10px;
        }

    </style>

    <div class="mx-auto w1200 clearfix pb-5 mt-3">
        <?php include __DIR__ . '/common/left-column.php' ?>
        <div class="user_content float-right">
            <p class="case_title"><span class="green">我的优惠劵</span></p>

            <div class="mycoupon clearfix">

                <?php foreach ($memberStoreCoupon as $k => $v): ?>
                    <!--     判断是否过期 和 使用-->
                    <div class="mycoupon_list <?=($v->is_use==0 && $v->is_expire==0)?'':'mycoupon_ed'?>">
                        <div class="mycoupon_list_left">
                            <p class="p_price">￥<span class="s_p"><?= $v->goodsCoupon->money ?></span>
                                <span class="s_tag"><?= $v->goodsCoupon->describe ?></span>
                            </p>
                            <p class="p_name text-truncate pb-2"><?=$v->user->nickname?></p>
                            <p>有效期<span><?=date('Y-m-d',strtotime($v->begin_time))?></span>~<span><?=date('Y-m-d',strtotime($v->end_time))?></span></p>
                        </div>
                        <?php if ($v->is_use==1):?>
                            <a href="javascript:void(0)" class="mycoupon_list_right">已经使用</a>
                        <?php else:?>
                            <?php if ($v->is_expire==1):?>
                            <a href="javascript:void(0)" class="mycoupon_list_right">已经过期</a>
                            <?php else:?>
                                <a href="javascript:void(0)" class="mycoupon_list_right checkState"  data-couponId="<?=$v->id?>">立即使用</a>
                            <?php endif;?>
                        <?php endif;?>
                    </div>
                <?php endforeach; ?>

            </div>

            <?php echo $pageHtml?>
        </div>

    </div>

    <!-- 模态框-优惠劵弹框 -->
    <div class="modal fade" id="myModalCoupon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title pl-3">优惠劵</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body pt-4 pb-4">
                    <div class="case_detail_l_li">
                        <span>使用范围：</span>
                        <div class="case_detail_l_li_main"></div>
                    </div>
                    <div class="case_detail_l_li">
                        <span>核销码：</span>
                        <div class="case_detail_l_li_main"></div>
                    </div>
                    <div class="case_detail_l_li">
                        <span>状态：</span>
                        <div class="case_detail_l_li_main orange"></div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>


    <script>
        //  查询优惠劵
        $(".checkState").on('click',function () {
            var url="<?=UrlService::build(['personal/member-coupon'])?>";
            var id=$(this).attr('data-couponId');
            $.get(url,{id:id},function (res) {
                if (res.code==1){
                    $("#myModalCoupon").modal('show');
                    $(".case_detail_l_li_main:eq(0)").text(res.content);
                    $(".case_detail_l_li_main:eq(1)").text(res.code_verify);
                    $(".case_detail_l_li_main:eq(2)").text(res.state);

                } else {
                    alert(res.msg);
                }

            },'json')
        })


    </script>

<?php include APP_PATH . '/views/common/footer.php'; ?>