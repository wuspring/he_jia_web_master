<?php

use DL\service\UrlService;
use common\models\FloorIndex;
use common\models\Ticket;
use common\models\Order;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>
<div class="mx-auto w1200 clearfix pb-5 mt-3">

    <?php include __DIR__.'/common/left-column.php' ?>
<script>
    $(function () {
        <?php
        if ($_GET) {
            echo 'var obj=' . json_encode($_GET) . ';';
        }
        ?>
        if (typeof(obj) != 'undefined') {
            for(k in obj){
                if (k=='near_time'){

                     $("option[data-type='"+obj[k]+"']").attr('selected','true');
                }
                if (k=='order_status'){
                    if (obj[k]){
                        $("option[data-type='"+obj[k]+"']").attr('selected','true');
                    }
                }
                if (k=='orderid'){
                    if (obj[k]){
                        $("#orderNum").val(obj[k]);
                    }
                }
            }
        }
    });

</script>
    <div class="user_content float-right">
        <p class="case_title"><span class="green">全部订单</span></p>
        <div class="case_search">
            <div class="form-inline row">
                <div class="form-group col-md-6">
                    <input type="input" class="form-control" id="orderNum"  placeholder="请输入订单号">
                    <button type="submit" id="searchSingleOrder" class="btn btn-success">搜索</button>
                </div>
                <!-- <div class="col-md-2"></div> -->
                <div class="form-group  col-md-6">
                    <label for="sel1">下单时间：</label>
                    <!-- 下拉框内容待定 -->
                    <select class="form-control" id="sel1">
                        <option value="all" data-type="all">全部</option>
                        <option value="week" data-type="week">近一周</option>
                        <option value="month" data-type="month">近一个月</option>
                        <option value="threeMonth" data-type="threeMonth">近三个月</option>
                    </select>
                    <label for="sel2">订单状态：</label>
                    <select class="form-control" id="sel2">
                        <option value=" ">全部</option>
                        <option value="0" data-type="0">待付款</option>
                        <option value="1" data-type="1">待核销</option>
                        <option value="3" data-type="3">待评价</option>
                        <option value="4" data-type="4">已评价</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="mycase_list">
            <!-- 全部订单 -->
            <table class="table mycase_list_table">
                <thead>
                <tr>
                    <th class="td1">商品</th>
                    <th class="td2">单价（元）</th>
                    <th class="td3">数量</th>
                    <th class="td4">总价</th>
                    <th class="td4">订金</th>
                    <th class="td8">订单状态</th>
                    <th class="td9">操作</th>
                </tr>
                </thead>
            </table>
           <?php if (empty($order)):?>
               <div class="com_nodata">
                   <img src="/public/index/images/img_result_nodata.png" alt="订单支付成功" class="img">
                   <div class="con">
                       <p class="p1">亲，您最近没有下过订单哦~</p>
                       <a href="<?=UrlService::build(['index/index'])?>" class="btn btn-warning">去首页逛逛</a>
                   </div>
               </div>
           <?php else:?>

            <?php foreach ($order as $k=>$v):?>

                       <!-- 等待付款 -->
                       <!-- 添加table_line后，边框是粉色 -->
                       <table class="table mycase_list_table">
                           <tbody>
                           <!-- 一条订单-start -->
                           <tr class="tr_id">
                               <td colspan="9"><span><?=$v->add_time?></span>订单号：<?=$v->orderid?>
                                   <?php if ($v->type ==Order::TYPE_ZHAN_HUI and in_array($v->order_state, [Order::STATUS_PAY, Order::STATUS_SEND, Order::STATUS_RECEIVED, Order::STATUS_JUDGE])) :?>
                                   <span style="margin-left: 15px;">优惠码 ：<?= $v->systemCoupon->code;?> </span>
                                   <?php  endif; ?>
                               </td>
                           </tr>
                            <?php foreach ($v->orderGoodsList as $listk=>$listv): ?>

                                <?php if ($listk==0):?>
                                    <tr>
                                        <td class="td1">
                                            <a href="<?=UrlService::build(['goods/detail','id'=>$listv['goods_id']])?>" class="media">
                                                <img src="<?=$listv['goods_pic']?>" class="align-self-center" style="width:80px">
                                                <div class="media-body mt-1">
                                                    <h4><?=$listv['goods_name']?></h4>
                                                    <p>颜色分类：<?=$listv['attr']?></p>
                                                </div>
                                            </a>
                                        </td>
                                        <td class="td2"><span><?= $listv['goods_price'] ?></span>元</td>
                                        <td class="td3">× <?=$listv['goods_num']?></td>
                                        <td class="td4"><span><?= $v->pay_amount; ?></span>元</td>
                                        <td class="td4"><span><?= $v->order_amount; ?></span>元</td>
                                        <!-- 待支付-->
                                        <?php if ($v->order_state==Order::STATUS_WAIT):?>
                                            <td class="td8" rowspan="2">
                                                <p class="p_lg"><?= $v->orderState; ?></p>
                                            </td>
                                            <td class="td9" rowspan="2">
                                                <a href="<?= UrlService::build(['order/submit', 'number' => $v->orderid ]); ?>" class="a a_color a_pay">立即付款</a>
                                                <a href="javascript:void(0)" class="a a_cancel" data-toggle="modal"
                                                   data-target="#myModalCancel" data-id="cancel" data-orderid="<?=$v->orderid?>">取消订单</a>
                                            </td>
                                            <!-- 已支付-->
                                        <?php elseif ($v->order_state==Order::STATUS_PAY):?>
                                            <td class="td8" rowspan="2">
                                                <p class="p_lg"><?= $v->orderState; ?></p>
<!--                                                <a href="--><?php //UrlService::build(['personal/integral-order-detail2'])?><!--" class="detail">订单详情</a>-->
                                            </td>
                                            <td class="td9" rowspan="2"></td>
                                            <!-- 待评价-->
                                        <?php elseif ($v->order_state==Order::STATUS_RECEIVED):?>
                                            <td class="td8" rowspan="2">
                                                <p class="p_lg"><?= $v->orderState; ?></p>
                                            </td>
                                            <td class="td9" rowspan="2">
                                                <a href="<?=UrlService::build(['personal/order-comment','orderId'=>$v->orderid])?>" class="a a_assess">立即评价</a>
                                            </td>
                                            <!-- 已评价-->
                                        <?php elseif ($v->order_state==Order::STATUS_JUDGE):?>
                                            <td class="td8" rowspan="2">
                                                <p class="p_lg"><?= $v->orderState; ?></p>
                                            </td>
                                            <td class="td9" rowspan="2"></td>
                                        <?php elseif ($v->order_state==Order::STATUS_CLOSE):?>

                                            <td class="td8" rowspan="2">
                                                <p class="p_lg"><?= $v->orderState; ?></p>
                                            </td>
                                            <td class="td9" rowspan="2"></td>
                                        <?php endif;?>

                                    </tr>
                                <?php else:?>
                                    <tr>
                                        <td class="td1">
                                            <a href="<?=UrlService::build(['goods/detail','id'=>$listv['goods_id']])?>" class="media">
                                                <img src="<?=$listv['goods_pic']?>" class="align-self-center" style="width:80px">
                                                <div class="media-body mt-1">
                                                    <h4><?=$listv['goods_name']?></h4>
                                                    <p>颜色分类：<?=$listv['attr']?></p>
                                                </div>
                                            </a>
                                        </td>
                                        <td class="td2"><span><?=$listv['goods_price']?></span>元</td>
                                        <td class="td3">× <?=$listv['goods_num']?></td>
                                        <td class="td4"><span><?=$v->pay_amount?></span>元</td>
                                    </tr>
                                <?php endif;?>

                            <?php endforeach;?>

                           <!-- 一条订单-end -->
                           </tbody>
                       </table>

             <?php  endforeach;?>

           <?php endif;?>

            <?php echo $pageHtml?>

        </div>


    </div>

</div>


<!-- 模态框-确认取消 -->
<div class="modal fade" id="myModalCancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="height: 292px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>

            </div>
            <div class="modal-body pt-5 mt-3 mb-5 pb-4">
                是否确认取消预定？
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-warning" data-id="cancelButton">确定</button>
            </div>
        </div>
    </div>
</div>

<!-- 模态框-删除订单 -->
<div class="modal fade" id="myModalDeleCase" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>

            </div>
            <div class="modal-body pt-5 mt-3 mb-5 pb-4">
                是否确认删除订单？
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-warning " data-dismiss="modal" >确定</button>
            </div>
        </div>
    </div>
</div>


    <script>

     //点击取消订单
     $("[data-id=\"cancel\"]").on('click',function () {
         $('[data-id="cancelButton"]').attr('data-val', $(this).attr('data-orderid'));
         autoDomHeight($("#myModalCancel"));
     });

    //取消订单
    $("[data-id=\"cancelButton\"]").on('click',function () {

        var orderid = $(this).attr('data-val');

        var url="<?=UrlService::build(['payment/cancel'])?>";
        $.get(url, {number : orderid}, function (res) {
            if (res.status){
                alert("取消订单成功", function () {
                    window.location.reload();
                });

                return false;
            }

            alert("取消订单失败");
        },'json');

        $("#myModalCancel").modal('hide');

    });

     //点击搜索
     $("#searchSingleOrder").on('click',function () {
         var orderNum=$("#orderNum").val();
         window.location.href = rebuildQueryString("<?=UrlService::build(['personal/appointment-order'])?>", {orderid : orderNum});
     });
     //下拉搜索
     $("#sel1").on('change',function () {
         var near_time=$(this).val();
         window.location.href= rebuildQueryString("<?=UrlService::build(['personal/appointment-order'])?>", {near_time : near_time});
     });
     $("#sel2").on('change',function () {
         var order_status=$(this).val();
         window.location.href= rebuildQueryString("<?=UrlService::build(['personal/appointment-order'])?>", {order_status : order_status});
     })

</script>


<?php include APP_PATH . '/views/common/footer.php'; ?>