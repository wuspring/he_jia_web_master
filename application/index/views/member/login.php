<?php

use DL\service\UrlService;
use common\models\GoodsClass;

include APP_PATH . '/views/common/header.php'; ?>

<?php // include APP_PATH . '/views/common/guider.php'; ?>
<style>
    form input:focus {
        outline: none!important;
        box-shadow: none!important;
    }
</style>
<div class="login_banner" style="background-image: url(<?=!empty($pic)?$pic->picture:'/public/index/images/bg_login.jpg'?>);">
    <div class="mx-auto w1200">
        <div class="login_content float-right">
            <h3><a href="<?=UrlService::build(['member/quick-login'])?>" class="float-right">快捷登录</a><em>账号登录</em></h3>
            <form id="login" action="<?= UrlService::build('member/login'); ?>" type="multipart/form-data" data-id="login" method="post" class="login_form">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><img src="/public/index/images/icon_user.png"></span>
                    </div>
                    <input type="text" name="username" class="form-control"  placeholder="请输入账号" required="">
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><img src="/public/index/images/icon_pwd.png"></span>
                    </div>
                    <input type="password" name="password" class="form-control" placeholder="请输入密码" required="">
                </div>
                <div class="form-check mb-3">
                    <a href="<?=UrlService::build(['forget-password'])?>" class="float-right">找回密码？</a>
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox"> 记住密码
                    </label>
                </div>
                <button type="button" data-id="login" class="btn btn-warning">登 录</button>
                <p>提示：如果您登录，表示您已经阅读过并且您已同意和家网的关于<a href="<?=UrlService::build(['member/help','contId'=>15])?>" class="orange">《会员注册协议》</a>的条款</p>
            </form>
        </div>
    </div>

</div>
<script>
    $('button[data-id="login"]').click(function () {

        if ($('input[name="username"]').val().length < 4) {
            alert("请输入正确的账户名");
            return false;
        }

        if ($('input[name="password"]').val().length < 4) {
            alert("请输入正确的密码");
            return false;
        }

        var form = $('form[data-id="login"]'),
            formData = new FormData(form.get(0)),
            url = form.attr('action');

        $.ajax({
            url: url,
            data : formData,
            type: 'POST',
            cache: false,
            processData: false,
            contentType: false,
            success: function (res) {
                res = JSON.parse(res);
                if (res.status) {
                    window.location.href = res.data.location;
                    return false;
                }

                alert (res.msg, function () {
                    return false;
                })
            }
        });
    });
</script>
<?php include APP_PATH . '/views/common/footer.php'; ?>






	


