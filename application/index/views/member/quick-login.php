<?php

use DL\service\UrlService;
use common\models\GoodsClass;

include APP_PATH . '/views/common/header.php'; ?>

<?php //include APP_PATH . '/views/common/guider.php'; ?>
    <style>
        form input:focus {
            outline: none!important;
            box-shadow: none!important;
        }
    </style>
	<div class="login_banner" style="background-image: url(/public/index/images/bg_login.jpg);">
		<div class="mx-auto w1200 ">
			<div class="login_content float-right">
				<h3><a href="<?=UrlService::build(['member/login'])?>" class="float-right">账号登录</a><em>快捷登录</em></h3>
				<form action="<?= UrlService::build('member/quick-login'); ?>" data-id="login" method="post" class="login_form">
				    <div class="input-group">
					    <div class="input-group-prepend">
					        <span class="input-group-text"><img src="/public/index/images/icon_phone2.png"></span>
					    </div>
				      <input type="text" name="mobile" class="form-control" placeholder="请输入手机号" required="">
				    </div>		
				    <div class="input-group div_yzm">
				      	<div class="input-group-prepend">
				        	<span class="input-group-text"><img src="/public/index/images/icon_yzm.png"></span>
				      	</div>
				      	<input type="text" name="code" class="form-control" placeholder="请输入验证码" required="">
						<div class="input-group-append">
						    <button class="btn btn-dark" data-id="getCode" type="button">验证码</button>
						    <!-- <button class="btn btn-dark disabled" type="button">50s</button>   -->
						</div>
					</div>		
				    <div class="form-check mb-3">
						<a href="<?=UrlService::build(['forget-password'])?>" class="float-right">找回密码？</a>
				      	<label class="form-check-label">

				      	</label>
				    </div>
				    <button type="button" data-id="login" class="btn btn-warning">登 录</button>
				    <p>提示：如果您登录，表示您已经阅读过并且您已同意和家网的关于<a href="<?=UrlService::build(['member/help'])?>" class="orange">《会员注册协议》</a>的条款</p>
				</form>
			</div>
		</div>
	</div>

<script>
    var mobileDom = $('input[name="mobile"]'),
        codeDom = $('button[data-id="getCode"]'),
        getCodeCycle = 60;

    codeDom.click(function () {
        var mobile = mobileDom.val();
        if (!/^1\d{10}$/.test(mobile)) {
            alert("请输入正确的手机号");
            return false;
        }

        if (!codeDom.hasClass('disabled')) {
            $.post(
                '<?=\yii\helpers\Url::toRoute('api/get-code'); ?>',
                {mobile : mobile},
                function (res) {
                    res = JSON.parse(res);console.log(res);
                    if (res.status) {
                        if (codeDom.hasClass('disabled')) {
                            return false;
                        }

                        codeDom.attr('data-cycle', getCodeCycle).addClass('disabled');

                        var cycleClock = setInterval(function () {
                            var cycle = parseInt(codeDom.attr('data-cycle'));
                            if (cycle > 0) {
                                codeDom.text("" + cycle +"s").attr('data-cycle', cycle-1);
                            } else {
                                codeDom.text('验证码').removeClass('disabled');
                                clearInterval(cycleClock);
                            }
                        }, 1000);
                    }
                }
            )
        }

    });

    $('button[data-id="login"]').click(function () {
        if (!/^1\d{10}$/.test(mobileDom.val())) {
            alert("请输入正确的手机号");
            return false;
        }

        if ($('input[name="code"]').val().length < 4) {
            alert("请输入正确的验证码");
            return false;
        }

        var form = $('form[data-id="login"]'),
            formData = new FormData(form.get(0)),
            url = form.attr('action');

        $.ajax({
            url: url,
            data : formData,
            type: 'POST',
            cache: false,
            processData: false,
            contentType: false,
            success: function (res) {
                res = JSON.parse(res);
                if (res.status) {
                    window.location.href = res.data.location;
                    return false;
                }

                alert (res.msg, function () {
                    return false;
                })
            }
        });
    });
</script>
<?php include APP_PATH . '/views/common/footer.php'; ?>