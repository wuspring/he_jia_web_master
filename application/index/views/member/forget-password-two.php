<?php

use DL\service\UrlService;
use common\models\GoodsClass;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>


<div class="mx-auto w1200 clearfix">
    <div class="password_cont">
        <div class="password_cont">
            <div class="password_top">
                <div class="password_infor password_infor_cur"><span>1</span>验证身份</div>
                <div class="password_line  password_line_cur"></div>
                <div class="password_infor password_infor_cur"><span>2</span>重置密码</div>
                <div class="password_line password_line_cur"></div>
                <div class="password_infor password_infor_cur"><span>3</span>完成</div>
            </div>
            <div class="password_main">
                <div class="password_suc">
                    <div class="password_suc_l"><img src="/public/index/images/img_password_suc.jpg" alt="设置成功"></div>
                    <div class="password_suc_r">
                        <h2>新密码设置成功！</h2>
                        <p>请牢记您的新密码，<a href="<?=UrlService::build(['member/login'])?>" class="back orange">返回登录</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include APP_PATH . '/views/common/footer.php'; ?>

	