<?php

use DL\service\UrlService;
use common\models\GoodsClass;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>
<div class="mx-auto w1200 clearfix">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="index.html">首页</a>
        <span class="breadcrumb-item active">帮助中心</span>
    </nav>
    <div class="help_menu float-left">
        <?php foreach ($helpType as $k=>$v):?>
            <h4 class="nav-header"><?=$v->type?></h4>
            <ul class="nav flex-column">
                 <?php foreach ($v->helpcont as $helpConK=>$helpConV):?>
                     <li class="nav-item"><a class="nav-link <?=($helpConV->id==$helpCont->id)?'active':'' ?>" href="<?=UrlService::build(['member/help','contId'=>$helpConV->id])?>"><?=$helpConV->title?></a></li>
                 <?php endforeach;?>
            </ul>
        <?php endforeach;?>

    </div>
    <div class="help_content float-right">
        <h4> <?=$helpCont->title?></h4>
        <div class="con">
            <?=$helpCont->content?>
        </div>
    </div>

</div>

<?php include APP_PATH . '/views/common/footer.php'; ?>
    
