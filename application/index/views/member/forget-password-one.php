<?php

use DL\service\UrlService;
use common\models\GoodsClass;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>


<div class="mx-auto w1200 clearfix">
		<div class="password_cont">
			<div class="password_top">
				<div class="password_infor password_infor_cur"><span>1</span>验证身份</div>
				<div class="password_line  password_line_cur"></div>
				<div class="password_infor password_infor_cur"><span>2</span>重置密码</div>
				<div class="password_line password_line_cur"></div>
				<div class="password_infor"><span>3</span>完成</div>
			</div>
			<div class="password_main">	
				<form action="<?= UrlService::build('member/forget-password-two'); ?>" method="post" data-id="login" class="password_form">
					<div class="password_form_li">
						<label class="password_form_li_name" for="">账户：</label>
						<input type="text" class="password_form_li_input" disabled value="<?= $member->username; ?>" placeholder="请输入您的用户名" required="">
					</div>
					<div class="password_form_li">
						<label class="password_form_li_name" for=""><span>*</span>新密码：</label>
						<input type="text" name="password" class="password_form_li_input"  placeholder="请输入密码" required="">
						<p class="password_form_li_tip">请输入密码</p>
					</div>
					<div class="password_form_li">
						<label class="password_form_li_name" for=""><span>*</span>确认新密码：</label>
						<input type="text" name="c_password" class="password_form_li_input"  placeholder="请再次输入密码" required="">
						<p class="password_form_li_tip">两次密码不一致，请重新输入</p>
					</div>
                    <input type="hidden" name="secret" value="<?= $secret; ?>" placeholder="请再次输入密码" required="">
					<a href="javascript:void(0)" data-id="login" class="btn btn-warning password_btn">下一步</a>
				</form>
			</div>
		</div>
	</div>
<script>
    $('a[data-id="login"]').click(function () {

        if ($('input[name="password"]').val().length < 6) {
            alert("请输入长度不小于6位的密码");
            return false;
        }

        if ($('input[name="password"]').val() != $('input[name="c_password"]').val()) {
            alert("两次输入的密码不一致");
            return false;
        }

        $('form[data-id="login"]').submit();
    });
</script>
<?php include APP_PATH . '/views/common/footer.php'; ?>
