<?php

use DL\service\UrlService;
use common\models\GoodsClass;

include APP_PATH . '/views/common/header.php'; ?>


    <div class="login_banner" style="background-image: url(<?=!empty($pic)?$pic->picture:'/public/index/images/bg_login.jpg'?>);">
        <div class="mx-auto w1200 ">
            <div class="login_content reg_content float-right">
                <h3><em>注册</em></h3>
                <form action="<?=UrlService::build(['member/reg'])?>" data-id="login" method="post" class="login_form">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><img src="/public/index/images/icon_user.png"></span>
                        </div>
                        <input type="text" name="username" class="form-control" placeholder="请输入账号" required="">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><img src="/public/index/images/icon_phone2.png"></span>
                        </div>
                        <input type="text" name="mobile" class="form-control" placeholder="请输入手机号" required="">
                    </div>
                    <div class="input-group div_yzm">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><img src="/public/index/images/icon_yzm.png"></span>
                        </div>
                        <input type="text" class="form-control" name="code" placeholder="请输入验证码" required="">
                        <div class="input-group-append">
                            <button class="btn btn-dark"  data-id="getCode" type="button">验证码</button>
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><img src="/public/index/images/icon_pwd.png"></span>
                        </div>
                        <input type="password" name="password" class="form-control" placeholder="请输入密码" required="">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><img src="/public/index/images/icon_pwd.png"></span>
                        </div>
                        <input type="password" name="c_password" class="form-control" placeholder="请确认密码" required="">
                    </div>
                    <button type="button" data-id="login" class="btn btn-warning">立即注册</button>
                    <p class="text-right">已经有账号 <a href="<?=UrlService::build(['member/login'])?>" class="orange">立即登录</a></p>
                </form>
            </div>
        </div>

    </div>
<script>
    var mobileDom = $('input[name="mobile"]'),
        codeDom = $('button[data-id="getCode"]'),
        getCodeCycle = 60;

    codeDom.click(function () {
        var mobile = mobileDom.val();
        if (!/^1\d{10}$/.test(mobile)) {
            alert("请输入正确的手机号");
            return false;
        }
        if (!codeDom.hasClass('disabled')) {

                        $.post(
                            '<?=\yii\helpers\Url::toRoute('api/get-code'); ?>',
                            {mobile : mobile},
                            function (res) {
                                res = JSON.parse(res);console.log(res);
                                if (res.status) {
                                    if (codeDom.hasClass('disabled')) {
                                        return false;
                                    }

                                    codeDom.attr('data-cycle', getCodeCycle).addClass('disabled');

                                    var cycleClock = setInterval(function () {
                                        var cycle = parseInt(codeDom.attr('data-cycle'));
                                        if (cycle > 0) {
                                            codeDom.text("" + cycle +"s").attr('data-cycle', cycle-1);
                                        } else {
                                            codeDom.text('验证码').removeClass('disabled');
                                            clearInterval(cycleClock);
                                        }
                                    }, 1000);
                                }
                            }
                        )
                    }

    });

    $('button[data-id="login"]').click(function () {
        if ($('input[name="username"]').val().length < 6) {
            alert("请输入长度不小于6位的用户名");
            return false;
        }
        if (!/^1\d{10}$/.test(mobileDom.val())) {
            alert("请输入正确的手机号");
            return false;
        }

        if ($('input[name="code"]').val().length < 4) {
            alert("请输入正确的验证码");
            return false;
        }
        if ($('input[name="password"]').val().length < 6) {
            alert("请输入长度不小于6位的密码");
            return false;
        }

        if ($('input[name="password"]').val() != $('input[name="c_password"]').val()) {
            alert("两次输入的密码不一致");
            return false;
        }


        $('form[data-id="login"]').submit();
    });
</script>
<?php include APP_PATH . '/views/common/footer.php'; ?>