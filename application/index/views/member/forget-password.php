<?php

use DL\service\UrlService;
use common\models\GoodsClass;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>

<div class="mx-auto w1200 clearfix">
		<div class="password_cont">
			<div class="password_top">
				<div class="password_infor password_infor_cur"><span>1</span>验证身份</div>
				<div class="password_line  password_line_cur"></div>
				<div class="password_infor"><span>2</span>重置密码</div>
				<div class="password_line"></div>
				<div class="password_infor"><span>3</span>完成</div>
			</div>
			<div class="password_main">	
				<form action="<?= UrlService::build('member/forget-password'); ?>" method="post" class="password_form" data-id="login">
					<div class="password_form_li">
						<label class="password_form_li_name" for="">账户：</label>
						<input type="text" name="username" class="password_form_li_input" placeholder="请输入您的用户名" required="">
						<p class="password_form_li_tip">请输入您的用户名</p>
					</div>
					<div class="password_form_li">
						<label class="password_form_li_name" for=""><span>*</span>手机号：</label>
						<input type="text"  name="mobile" class="password_form_li_input" placeholder="请输入正确的手机号" required="">
						<p class="password_form_li_tip">请输入正确的手机号</p>
					</div>
					<div class="password_form_li">
						<label class="password_form_li_name" for=""><span>*</span>验证码：</label>
						<input type="text" name="code" class="password_form_li_input1" placeholder="请输入验证码" required="">
						<a href="javascript:void(0)"  data-id="getCode" class="password_form_li_yzm">验证码</a>
						<!-- <a href="javascript:void(0)" class="password_form_li_yzm disabled">50s</a> -->
					</div>
					<a href="javascript:void(0)" data-id="login" class="btn btn-warning password_btn">下一步</a>
				</form>
			</div>
		</div>
	</div>
<script>
    var mobileDom = $('input[name="mobile"]'),
        codeDom = $('a[data-id="getCode"]'),
        getCodeCycle = 60;

    codeDom.click(function () {
        var mobile = mobileDom.val();
        if (!/^1\d{10}$/.test(mobile)) {
            alert("请输入正确的手机号");
            return false;
        }

        if (!codeDom.hasClass('disabled')) {
            $.post(
                '<?=\yii\helpers\Url::toRoute('api/get-code'); ?>',
                {mobile : mobile},
                function (res) {
                    res = JSON.parse(res);console.log(res);
                    if (res.status) {
                        if (codeDom.hasClass('disabled')) {
                            return false;
                        }

                        codeDom.attr('data-cycle', getCodeCycle).addClass('disabled');

                        var cycleClock = setInterval(function () {
                            var cycle = parseInt(codeDom.attr('data-cycle'));
                            if (cycle > 0) {
                                codeDom.text("" + cycle +"s").attr('data-cycle', cycle-1);
                            } else {
                                codeDom.text('验证码').removeClass('disabled');
                                clearInterval(cycleClock);
                            }
                        }, 1000);
                    }
                }
            )
        }

    });

    $('a[data-id="login"]').click(function () {
        if ($('input[name="username"]').val().length < 6) {
            alert("请输入用户名");
            return false;
        }
        if (!/^1\d{10}$/.test(mobileDom.val())) {
            alert("请输入正确的手机号");
            return false;
        }

        if ($('input[name="code"]').val().length < 4) {
            alert("请输入正确的验证码");
            return false;
        }

        $('form[data-id="login"]').submit();
    });
</script>

<?php include APP_PATH . '/views/common/footer.php'; ?>
