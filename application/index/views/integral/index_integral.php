<?php
use DL\service\UrlService;
use common\models\GoodsClass;


include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/integral-header.php';
?>
	<div class="mx-auto w1200 clearfix ">
		<div class="integral_top">
			<div class="integral_top_right float-right">
                <?php if ($userInfo) :?>
                    <div class="itr_user">
                        <img src="<?= strlen($userInfo->avatar) ? $userInfo->avatar : '/public/index/images/img_tx_default.png'; ?>" alt="" class="img">
                        <p><?= $userInfo->nickname; ?></p>
                        <div class="itru_btns">
                            <a href="<?=UrlService::build(['integral-cart/index'])?>" class="btn btn-success">购物车</a>
                        </div>
                    </div>
                <?php else :?>
				<div class="itr_user">
					<img src="/public/index/images/img_tx_default.png" alt="" class="img">
					<p>Hi，欢迎来到和家网积分商城</p>
					<div class="itru_btns">
						<a href="<?=UrlService::build(['member/login'])?>" class="btn btn-success">登录</a>
						<a href="<?=UrlService::build(['member/reg'])?>" class="btn btn-success">注册</a>
					</div>
				</div>
                <?php endif; ?>
				<div class="itr_news">
                    <?php if ($informations) : foreach ($informations AS $information) :?>
					<a href="<?= UrlService::build(['integral-news/index', 'id' => $information->id]); ?>" class="text-truncate"><?= \yii\helpers\StringHelper::truncate($information->title, 10); ?></a>
                    <?php endforeach; else :?>
                    <span style="text-align: center;width: 100%;display: block;color: #ccc">(暂无消息)</span>

                    <?php endif; ?>
				</div>
			</div>
			<div class="integral_top_slider">
			    <div class="carousel slide integral_slider" data-ride="carousel" id="slider1">
			      <!-- 指示符 -->
			      <ul class="carousel-indicators">
                      <?php if ($banners) :?>
                      <?php foreach ($banners AS $index => $banner) :?>
			        <li data-target="#slider1" data-slide-to="<?= $index;?>" class="<?= $index ? '' : 'active'; ?>"></li>
                          <?php endforeach; ?>
                      <?php endif; ?>
			      </ul> 
			      <!-- 轮播图片 -->
			      <div class="carousel-inner">
                      <?php if ($banners) :?>
                      <?php foreach ($banners AS $index => $banner) :?>
			        <a href="javascript:void(0)" class="carousel-item <?= $index ? '' : 'active'; ?>"><img src="<?= $banner;?>"></a>
                      <?php endforeach; ?>
                      <?php endif; ?>
			      </div>
			    </div>
			</div>		
		</div>
		<div class="integral_explain">
			<div class="integtal_title"><em></em><img src="/public/index/images/icon_integral_title01.png" alt="积分赚取攻略"><span>积分赚取攻略</span><em></em></div>
			<div class="con"><img src="/public/index/images/img_luckdraw_green.jpg"></div>
		</div>
		<div class="integral_product">
			<div class="integtal_title"><em></em><img src="/public/index/images/icon_integral_title02.png" alt="积分礼品"><span>积分礼品</span><em></em></div>
			<div class="clearfix">
                <?php foreach ($goods AS $good) :?>
				<div class="integral_product_list">
					<a href="<?=UrlService::build(['intergral-goods/detail', 'id' => $good->id])?>" class="a_img" style="background-image: url('<?= $good->goods_pic; ?>');" title="<?= $good->goods_name; ?>"></a>
					<div class="con">
						<p class="p_name text-truncate"><?= $good->goods_name; ?></p>
						<p class="p_more"><a href="<?=UrlService::build(['intergral-goods/detail', 'id' => $good->id])?>" class="btn btn-warning float-right">我要兑换</a><span class="organe"><?= $good->goods_integral; ?></span>积分</p>
					</div>
				</div>
                <?php endforeach; ?>
			</div>
			<ul class="pagination justify-content-center">
                <?= $page->show(); ?>
			</ul>
		</div>
	</div>
<?php include __DIR__ . '/../common/cart.php'; ?>
    <script type="text/javascript">
    	//商品详情，点击小图切换大图
    	$('.idt_left .d_imgs img').bind('click',function(){
    		$(this).addClass('active').siblings().removeClass('active');
    		$('.idt_left .img').attr('src',$(this).attr('src'));
    	});
    	//点击切换选择的规格
    	$('.idt_con .d_type a').bind('click',function(){
    		$(this).addClass('active').siblings().removeClass('active');
    	})    	

    </script>
<?php include APP_PATH . '/views/common/footer.php';?>