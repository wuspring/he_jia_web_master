
<?php
use DL\service\UrlService;
use common\models\GoodsClass;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/integral-header.php';
?>

	<div class="mx-auto w1200 clearfix ">
		<nav class="breadcrumb com_crumbs">
			<span>当前位置：</span>
			<a class="breadcrumb-item" href="<?= UrlService::build(['integral/lists']); ?>">积分商城</a>
			<span class="breadcrumb-item active">积分商品列表</span>
		</nav>
        <?php if ($userInfo) :?>
		<div class="integral_user clearfix">
            <?php if ($memberInfo) : ?>
                <img src="<?= strlen($memberInfo->avatar) ? $memberInfo->avatar : '/public/index/images/img_tx_default.png'; ?>" alt="<?= $memberInfo->nickname; ?>">
            <?php else :?>
                <img src="/public/index/images/img_tx_default.png" alt="未登录">
            <?php endif; ?>
			
			<p><?= $memberInfo ? $memberInfo->nickname : '未登录'; ?></p>
			<p>积分余额：<span><?= $memberInfo ? $memberInfo->integral : '-'; ?></span></p>
		</div>
        <?php endif; ?>
		<form>
		<table class="table integral_type clearfix">
			<tr>
				<td class="td1" rowspan="3">礼品筛选</td>
				<td class="td2">礼品分类</td>
				<td class="td3">
                    <a class="<?= $gc==0 ? 'active' : ''; ?>" href="<?= UrlService::build(['integral/lists', 'inTop' => $inTop, 'inButton' => $inButton, 'sort' => $sort]); ?>">不限</a>
                    <?php foreach ($goodClasses AS $goodClass) :?>
                        <a class="<?= $goodClass->id==$gc ? 'active' : ''; ?>" href="<?= UrlService::build(['integral/lists', 'gc'=> $goodClass->id, 'inTop' => $inTop, 'inButton' => $inButton, 'sort' => $sort]); ?>"><?= $goodClass->name; ?></a>
                    <?php endforeach; ?>
				</td>
			</tr>
			<tr>
				<td class="td2">积分范围</td>
				<td class="td3">
					<a href="<?= UrlService::build([
					        'integral/lists',
					        'gc'=> $gc,
					        'sort' => $sort
                        ]); ?>" class="<?php if (0==$inTop and 0==$inButton)
                    {echo 'active'; }?>">不限</a>
                    <?php if ($inFilter): foreach ($inFilter AS $filter) :?>
					<a href="<?php
					   $filter[0] = isset($filter[0]) ? $filter[0] : '0';
					   $filter[1] = isset($filter[1]) ? $filter[1] : '0';
					   echo UrlService::build([
					        'integral/lists',
					        'gc'=> $gc,
					        'inTop' => $filter[1],
					        'inButton' => $filter[0],
					        'sort' => $sort
                        ]);
					   ?>
					" class="<?php if ($filter[1]==$inTop and $filter[0]==$inButton)
					    {echo 'active'; }?>"><?= "{$filter[0]}-{$filter[1]}"; ?></a>
                    <?php endforeach; endif;?>
				</td>
				<td class="td4 text-right">
					<div class="d_input">
						<input type="text" name="inButton" value="<?= $inButton ?:''; ?>">
                        <span>-</span>
                        <input type="text" name="inTop"  value="<?= $inTop ?:''; ?>">
					</div>
					<a href="javascript:void(0)" class="a_btn" data-id="setFilter">确定</a>
				</td>				
			</tr>
			<tr>
				<td class="td2">排序</td>
				<td class="td3" colspan="2">
					<a href="<?= UrlService::build(['integral/lists', 'gc'=> $gc, 'inTop' => $inTop, 'inButton' => $inButton, 'sort' => $sort+1]); ?>" class="<?php
					if ($sort==1) {
					    echo 'a_sort a_asc';
					} else if ($sort==2) {
					    echo 'a_sort a_desc';
					}
					?>"><span>积分值</span><em></em></a>
				</td>				
			</tr>
		</table>
		</form>
        <script>
            $('[data-id="setFilter"]').click(function () {
                var top = $('input[name="inTop"]').val(),
                    bottom = $('input[name="inButton"]').val();

                var data = {
                    'gc' : '<?= $gc; ?>',
                    'inTop' : top,
                    'inButton' : bottom,
                    'sort' : '<?= $sort; ?>'
                };

                window.location.href = rebuildQueryString('<?= UrlService::build(['integral/lists']); ?>', data);
            })
        </script>
		<div class="integral_product">
			<div class="integtal_title"><em></em><img src="/public/index/images/icon_integral_title02.png" alt="积分礼品"><span>积分礼品</span><em></em></div>
			<div class="clearfix">
                <?php foreach ($goods AS $good) :?>
                    <div class="integral_product_list">
                        <a href="<?=UrlService::build(['intergral-goods/detail', 'id' => $good->id])?>" class="a_img" style="background-image: url('<?= $good->goods_pic; ?>');" title="<?= $good->goods_name; ?>"></a>
                        <div class="con">
                            <p class="p_name text-truncate"><?= $good->goods_name; ?></p>
                            <p class="p_more"><a href="<?=UrlService::build(['intergral-goods/detail', 'id' => $good->id])?>" class="btn btn-warning float-right">我要兑换</a><span class="organe"><?= $good->goods_integral; ?></span>积分</p>
                        </div>
                    </div>
                <?php endforeach; ?>
			</div>
			<ul class="pagination justify-content-center">
                <?= $page->show(); ?>
			</ul>
		</div>
	</div>
<?php include __DIR__ . '/../common/cart.php'; ?>

    <script type="text/javascript">
    	//商品详情，点击小图切换大图
    	$('.idt_left .d_imgs img').bind('click',function(){
    		$(this).addClass('active').siblings().removeClass('active');
    		$('.idt_left .img').attr('src',$(this).attr('src'));
    	})
    	//点击切换选择的规格
    	$('.idt_con .d_type a').bind('click',function(){
    		$(this).addClass('active').siblings().removeClass('active');
    	})    	

    </script>

	<?php include APP_PATH . '/views/common/footer.php';?>
