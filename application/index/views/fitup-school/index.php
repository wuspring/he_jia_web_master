<?php

use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/guider.php';
?>
<div class="mx-auto w1200 clearfix pb-5">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="<?=UrlService::build(['index/index'])?>">首页</a>
        <span class="breadcrumb-item active">装修学堂</span>
    </nav>
    <ul class="fitup_nav list-unstyled clearfix">
        <!-- 最多8个 -->
        <?php foreach ($newsClass as $k=>$v):?>
            <li><a href="<?=UrlService::build(['fitup-school/news-list','pid'=>$v->id])?>"><?=$v->name?></a></li>
        <?php endforeach;?>

    </ul>
    <div class="fitup_containter clearfix">
        <div class="fitup_list float-left">
            <?php foreach ($newsClass as $k=>$v):?>
                <?php if ($k%2==0):?>
                    <div class="fitup_list_con1 con <?=$k>0?'mt-3':''?>">
                        <div class="fitup_title">
                            <a href="<?=UrlService::build(['fitup-school/news-list','pid'=>$v->id])?>" class="a_more">更多>></a>
                            <span class="s_title"><?=$v->name?></span>
                            <div class="d_links">
                                <?php foreach ($v->levelSecond as $levelK=>$leveV):?>
                                    <a href="<?=UrlService::build(['fitup-school/news-list-more','newsClassId'=>$leveV->id])?>"><?=$leveV->name?></a>
                                <?php endforeach;?>

                            </div>
                        </div>
                        <div class="fitup_list1 index_news">
                            <ul class="list-unstyled clearfix">
                                <?php foreach ($v->getChildrenNewsContent($cityId) as $contK=>$contV):?>
                                    <li class="<?=($contK%2==0)?'float-left':'float-right';?>">
                                        <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$contV->id])?>" class="a_img">
                                            <img src="<?=$contV->themeImg?>" alt=""></a>
                                        <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$contV->id])?>" class="a_title text-truncate"><?=$contV->title?></a>
                                        <p class="p ellipsis-3">
                                            <?=$contV->brief?>
                                        </p>
                                    </li>
                                <?php endforeach;?>

                            </ul>

                        </div>

                    </div>
                <?php else:?>
                    <div class="fitup_list_con2 con mt-3">
                        <div class="fitup_title">
                            <a href="<?=UrlService::build(['fitup-school/news-list','pid'=>$v->id])?>" class="a_more">更多>></a>
                            <span class="s_title"><?=$v->name?></span>
                            <div class="d_links">
                                <?php foreach ($v->levelSecond as $levelK=>$leveV):?>
                                    <a href="<?=UrlService::build(['fitup-school/news-list-more','newsClassId'=>$leveV->id])?>"><?=$leveV->name?></a>
                                <?php endforeach;?>
                            </div>
                        </div>
                        <div class="fitup_list2 clearfix">

                            <?php foreach ($v->getNewsContentThree($cityId) as $threeK=>$threeV):?>

                                 <?php if ($threeK==0):?>
                                    <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$threeV->id])?>" class="a_right mt-4 float-right">
                                        <img src="<?=$threeV->themeImg?>">
                                        <p><?=$threeV->title?></p>
                                    </a>
                                 <?php else:?>
                                    <div class="fitup_list2_con mt-4 float-left">
                                        <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$threeV->id])?>" class="a_img"><img src="<?=$threeV->themeImg?>" alt=""></a>
                                        <div class="d_con">
                                            <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$threeV->id])?>" class="a_name  ellipsis-2"><?=$threeV->title?></a>
                                            <p class="ellipsis-3">
                                                <?=$contV->brief?>
                                             </p>
                                            <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$threeV->id])?>" class="a_more">[详情]</a>
                                        </div>
                                    </div>
                                 <?php endif;?>

                            <?php endforeach;?>
                        </div>
                    </div>
                <?php endif;?>

            <?php endforeach;?>
                <!--装修案例-->
            <div class="fitup_list_con4 con mt-3">
                <div class="fitup_title">
                    <a href="<?=UrlService::build(['fitup-school/fitup-case'])?>" class="a_more">更多>></a>
                    <span class="s_title">装修案例</span>
                    <div class="d_links">
                        <?php foreach ($fitup_case_style as $case_style):?>
                            <a href="<?=UrlService::build(['fitup-school/fitup-case','paixu'=>'','cx1'=>$case_style->id])?>"><?=$case_style->name?></a>
                        <?php endforeach;?>
                    </div>
                </div>
                <div class="fitup_list4 clearfix">

                    <div class="fitup_list4_left float-left">
                        <?php foreach ($fitup_case as $case_one_k=>$case_one_v):?>
                            <?php if ($case_one_k<2):?>
                                <a href="<?=UrlService::build(['fitup-school/fitup-case-detail','caseId'=>$case_one_v->id])?>"
                                   style="background-image: url(<?=$case_one_v->cover_pic?>)"><p class="text-truncate">
                                        <?=$case_one_v->name?></p></a>
                            <?php endif;?>
                        <?php endforeach;?>

                    </div>

                    <div class="fitup_list4_middle float-left">

                        <?php foreach ($fitup_case as $case_two_k=>$case_two_v):?>
                            <?php if ($case_two_k==2):?>
                                <a href="<?=UrlService::build(['fitup-school/fitup-case-detail','caseId'=>$case_two_v->id])?>"
                                   style="background-image: url(<?=$case_two_v->cover_pic?>)"><p class="text-truncate">
                                        <?=$case_two_v->name?></p></a>
                            <?php endif;?>
                        <?php endforeach;?>

                    </div>

                    <div class="fitup_list4_right float-right">

                        <?php foreach ($fitup_case as $case_three_k=>$case_three_v):?>
                            <?php if ($case_three_k>2):?>
                                <a href="<?=UrlService::build(['fitup-school/fitup-case-detail','caseId'=>$case_three_v->id])?>"
                                   style="background-image: url(<?=$case_three_v->cover_pic?>)"><p class="text-truncate">
                                        <?=$case_three_v->name?></p></a>
                            <?php endif;?>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>


        </div>
        <div class="news_sidebar float-right">
            <div class="news_sidebar_ask con">
                <div class="fitup_title">
                    <span class="s_title">装修问答</span>
                </div>
                <p class="p_all">已有<span><?=$problemNum?></span>位</p>
                <p class="p_more">业主都在这里找到了她们想要的答案</p>
                <div class="btns">
                    <a href="javascript:void(0)" class="a_ask" data-toggle="modal" data-target="#myModalAsk">我要提问</a>
                    <a href="<?=UrlService::build(['fitup-school/answer'])?>" class="a_answer">我来回答</a>
                </div>


            </div>
        </div>
    </div>


</div>
<!-- 模态框-我要提问弹框 -->
<div class="modal fade" id="myModalAsk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title pl-3">我要提问</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-5 pb-4">
                <form action="" class="password_form">
                    <div class="password_form_li">
                        <textarea class="textarea" placeholder="请输入您的问题"></textarea>
                        <p class="password_form_li_tip">请输入您的问题</p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-warning"  data-id="submitProblem" data-dismiss="modal">确定</button>
            </div>
        </div>
    </div>
</div>
<?php include APP_PATH . '/views/common/footer.php'; ?>

 
