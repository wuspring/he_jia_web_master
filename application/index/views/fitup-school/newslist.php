<?php

use DL\service\UrlService;
use common\models\Newsclassify;
include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/guider.php';
?>

<div class="mx-auto w1200 clearfix pb-5">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="<?=UrlService::build(['index/index'])?>">首页</a>
        <a class="breadcrumb-item" href="<?=UrlService::build(['fitup-school/index'])?>">装修学堂</a>
        <span class="breadcrumb-item active"><?=Newsclassify::findOne($pid)->name?></span>
    </nav>
    <ul class="fitup_nav list-unstyled clearfix">
        <!-- 最多8个 -->
        <?php foreach ($newsclassify_children as $k=>$v):?>
            <li><a href="<?=UrlService::build(['fitup-school/news-list-more','newsClassId'=>$v->id])?>"><?=$v->name?></a></li>
        <?php endforeach;?>
    </ul>
    <div class="fitup_containter clearfix">
        <div class="fitup_list float-left">
            <?php foreach ($newsclassify_children as $children=>$childrenV):?>
                <div class="fitup_list_con1 con <?=($children==0)?'':'mt-3'?>">
                    <div class="fitup_title">
                        <a href="<?=UrlService::build(['fitup-school/news-list-more','newsClassId'=>$childrenV->id])?>" class="a_more">更多>></a>
                        <span class="s_title"><?=$childrenV->name?></span>
                    </div>
                    <div class="fitup_list1 index_news">

                        <ul class="list-unstyled clearfix">
                            <?php foreach ($childrenV->getNewsContent($cityId) as $news=>$news_V):?>
                                <li class="<?=($news%2==0)?'float-left':'float-right'?>">
                                    <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$news_V->id])?>" class="a_img"><img src="<?=$news_V->themeImg?>"
                                                                                 alt=""></a>
                                    <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$news_V->id])?>" class="a_title text-truncate"><?=$news_V->title?></a>
                                    <p class="p ellipsis-3">
                                        <?=$news_V->brief?>
                                    </p>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
            <?php endforeach;?>

        </div>
        <div class="news_sidebar float-right">
            <?php include __DIR__ . '/_form.php'; ?>
            <div class="news_sidebar_hot con mt-3 pb-2">
                <div class="fitup_title">
                    <span class="s_title">热门资讯</span>
                </div>
                <div class="nsh_list">
                    <?php foreach ($hotNews as $hotk=>$hotv):?>
                        <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$hotv->id])?>"><img src="<?=$hotv->themeImg?>" alt="">
                            <p class="ellipsis-3"><?=$hotv->title?></p></a>
                    <?php endforeach;?>
                </div>

            </div>
        </div>
    </div>


</div>
<?php include APP_PATH . '/views/common/footer.php'; ?>
