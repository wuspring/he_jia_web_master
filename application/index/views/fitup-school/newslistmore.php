<?php

use DL\service\UrlService;
use common\models\Newsclassify;
include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/guider.php';



?>

<div class="mx-auto w1200 clearfix pb-5">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="<?=UrlService::build(['index/index'])?>">首页</a>
        <a class="breadcrumb-item" href="<?=UrlService::build(['fitup-school/index'])?>">装修学堂</a>
        <a class="breadcrumb-item" href="<?=UrlService::build(['fitup-school/news-list','pid'=>$getClassName->fid])?>"><?=Newsclassify::findOne($getClassName->fid)->name?></a>
        <span class="breadcrumb-item active"><?=$getClassName->name?></span>
    </nav>
    <div class="fitup_containter clearfix">
        <div class="fitup_list float-left">
            <div class="fitup_list_con1 con">
                <div class="fitup_title">
                    <span class="s_title"><?=$getClassName->name?></span>
                </div>
                <div class="fitup_list1 index_news">
                    <ul class="list-unstyled clearfix newslist2">
                        <?php foreach ($model as $k=>$v):?>
                            <li class="clearfix">
                                <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$v->id])?>" class="a_img"><img src="<?=$v->themeImg?>" alt=""></a>
                                <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$v->id])?>" class="a_title text-truncate"><?=$v->title?></a>
                                <p class="p ellipsis-3">
                                  <?=$v->brief?>
                                </p>
                            </li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
            <?php echo $pageHtml?>

        </div>
        <div class="news_sidebar float-right">
            <?php include __DIR__ . '/_form.php'; ?>
            <div class="news_sidebar_hot con mt-3 pb-2">
                <div class="fitup_title">
                    <span class="s_title">热门资讯</span>
                </div>
                <div class="nsh_list">

                    <?php foreach ($hotNews as $hotk=>$hotv):?>
                        <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$hotv->id])?>"><img src="<?=$hotv->themeImg?>" alt="">
                            <p class="ellipsis-3"><?=$hotv->title?></p></a>
                    <?php endforeach;?>

                </div>

            </div>
        </div>
    </div>


</div>

<?php include APP_PATH . '/views/common/footer.php'; ?>
