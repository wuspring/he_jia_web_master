<?php

use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/guider.php';
?>
<div class="mx-auto w1200 clearfix pb-5">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="<?=UrlService::build(['index/index'])?>">首页</a>
        <span class="breadcrumb-item active">装修资讯</span>
    </nav>
    <ul class="fitup_nav list-unstyled clearfix">
        <!-- 最多8个 -->
        <?php foreach ($newsClass as $k=>$v):?>
            <li><a href="<?=UrlService::build(['fitup-school/news-list','pid'=>$v->id])?>"><?=$v->name?></a></li>
        <?php endforeach;?>

    </ul>
    <div class="fitup_containter clearfix">
        <div class="fitup_list float-left">
            <?php foreach ($newsClass as $k=>$v):?>
                <?php if ($k%2==0):?>
                    <div class="fitup_list_con1 con <?=$k>0?'mt-3':''?>">
                        <div class="fitup_title">
                            <a href="<?=UrlService::build(['fitup-school/news-list','pid'=>$v->id])?>" class="a_more">更多>></a>
                            <span class="s_title"><?=$v->name?></span>
                            <div class="d_links">
                                <?php foreach ($v->levelSecond as $levelK=>$leveV):?>
                                    <a href="<?=UrlService::build(['fitup-school/news-list-more','newsClassId'=>$leveV->id])?>"><?=$leveV->name?></a>
                                <?php endforeach;?>

                            </div>
                        </div>
                        <div class="fitup_list1 index_news">
                            <ul class="list-unstyled clearfix">
                                <?php foreach ($v->getChildrenNewsContent($cityId) as $contK=>$contV):?>
                                    <li class="<?=($contK%2==0)?'float-left':'float-right';?>">
                                        <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$contV->id])?>" class="a_img">
                                            <img src="<?=$contV->themeImg?>" alt=""></a>
                                        <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$contV->id])?>" class="a_title text-truncate"><?=$contV->title?></a>
                                        <p class="p ellipsis-3">
                                            <?=$contV->brief?>
                                        </p>
                                    </li>
                                <?php endforeach;?>

                            </ul>

                        </div>

                    </div>
                <?php else:?>
                    <div class="fitup_list_con2 con mt-3">
                        <div class="fitup_title">
                            <a href="<?=UrlService::build(['fitup-school/news-list','pid'=>$v->id])?>" class="a_more">更多>></a>
                            <span class="s_title"><?=$v->name?></span>
                            <div class="d_links">
                                <?php foreach ($v->levelSecond as $levelK=>$leveV):?>
                                    <a href="<?=UrlService::build(['fitup-school/news-list-more','newsClassId'=>$leveV->id])?>"><?=$leveV->name?></a>
                                <?php endforeach;?>
                            </div>
                        </div>
                        <div class="fitup_list2 clearfix">

                            <?php foreach ($v->getNewsContentThree($cityId) as $threeK=>$threeV):?>

                                 <?php if ($threeK==0):?>
                                    <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$threeV->id])?>" class="a_right mt-4 float-right">
                                        <img src="<?=$threeV->themeImg?>">
                                        <p><?=$threeV->title?></p>
                                    </a>
                                 <?php else:?>
                                    <div class="fitup_list2_con mt-4 float-left">
                                        <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$threeV->id])?>" class="a_img"><img src="<?=$threeV->themeImg?>" alt=""></a>
                                        <div class="d_con">
                                            <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$threeV->id])?>" class="a_name  ellipsis-2"><?=$threeV->title?></a>
                                            <p class="ellipsis-3">
                                                <?=$contV->brief?>
                                             </p>
                                            <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$threeV->id])?>" class="a_more">[详情]</a>
                                        </div>
                                    </div>
                                 <?php endif;?>

                            <?php endforeach;?>
                        </div>
                    </div>
                <?php endif;?>

            <?php endforeach;?>

        </div>
        <div class="news_sidebar float-right">
            <?php include __DIR__ . '/_form.php'; ?>
            <div class="news_sidebar_hot con mt-3 pb-2">
                <div class="fitup_title">
                    <span class="s_title">热门资讯</span>
                </div>
                <div class="nsh_list">
                    <?php foreach ($hotNews as $hotk=>$hotv):?>
                        <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$hotv->id])?>"><img src="<?=$hotv->themeImg?>" alt="">
                            <p class="ellipsis-3"><?=$hotv->title?></p></a>
                    <?php endforeach;?>
                </div>

            </div>
        </div>
    </div>

</div>
<?php include APP_PATH . '/views/common/footer.php'; ?>

 
