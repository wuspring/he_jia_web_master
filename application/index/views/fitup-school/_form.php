<?php
use DL\service\UrlService;

if (isset($ticket) and $ticket) :

    switch ($ticket->type) {
        case $ticket::TYPE_JIE_HUN :
            $title = '婚庆采购';
            break;
        case $ticket::TYPE_JIA_ZHUANG :
            $title = '家装采购';
            break;
        case $ticket::TYPE_YUN_YING :
            $title = '孕婴采购';
            break;
    }

?>

<div class="news_sidebar_ticket con">
    <form action="<?= UrlService::build('jia-zhuang/ask-ticket'); ?>" data-id="join" method="post" class="explosin_form">
        <h3><span class="green"><?= $title ?></span> <span class="orange">来家博会</span></h3>
        <p class="p1"><?= date('m.d', strtotime($ticket->open_date)) . ' ~ ' . date('m.d', strtotime($ticket->end_date)) .  " {$ticket->address}"; ?></p>
        <input type="text" class="input" autocomplete="off" name="name" placeholder="请输入收件人姓名" required="">
        <input type="text" class="input" autocomplete="off" name="mobile" placeholder="请输入手机号码" required="">
        <div class="d_yzm clearfix">
            <a href="javascript:void(0)" data-id="getCode1" class="btn btn-danger" style="margin-top: 10px">验证码</a>
            <!-- <a href="javascript:void(0)" class="btn btn-danger disabled">50s</a> -->
            <input type="text" class="input" autocomplete="off" name="code" placeholder="请输入短信验证码" required="">
        </div>
        <input type="hidden" class="input"  autocomplete="off" name="address" placeholder="请输入收货地址" required="">
        <input type="hidden" class="input" name="ticketId" value="<?= $ticket->id; ?>">
        <input type="hidden" class="input" name="cityId" value="<?= $ticket->citys; ?>">
        <input type="hidden" class="input" name="type" value="<?= $ticket->type; ?>">
        <input type="hidden" class="input" name="r" value="jia-zhuang/ask-ticket">
        <input type="hidden" class="input" name="t" value="<?= Yii::$app->requestedRoute; ?>">
        <input type="hidden" class="input" name="resource" value="默认">
        <a href="javascript:void(0)" class="btn btn-warning" data-id="submit">免费索票</a>
        <?php if ($ticket) :?>
        <p class="p3">请详细填写以上信息，我们将免费为您快递一份价值<?= $ticket->ticket_price; ?>元的门票及展会资料</p>
        <?php endif; ?>
    </form>
</div>
    <script>
        $('[data-id="submit"]').click(function () {
            var name = $('input[name="name"]'),
                mobile = $('input[name="mobile"]'),
                address = $('input[name="address"]'),
                code = $('input[name="code"]');

            if (code.val().length < 1) {
                alert("请输入验证码");
                return false;
            }

            if (name.val().length < 1) {
                if (name.attr('data-default').length < 1) {
                    alert("请输入收件人姓名");
                    return false;
                }
                name.val(name.attr('data-default'));
            }

            if (!/^1\d{10}$/.test(mobile.val())) {
                if (!/^1\d{10}$/.test(mobile.attr('data-default'))) {
                    alert("请输入正确的手机号码");
                    return false;
                }

                mobile.val(mobile.attr('data-default'));
            }
            //
            // if (address.val().length < 1) {
            //     if (address.attr('data-default').length < 1) {
            //         alert("请输入收货地址");
            //         return false;
            //     }
            //
            //     address.val(address.attr('data-default'));
            // }

            $('form[data-id="join"]').submit();
        })

        var mobileDom = $('input[name="mobile"]'),
            codeDom = $('a[data-id="getCode1"]'),
            getCodeCycle = 60;

        codeDom.click(function () {
            var mobile = mobileDom.val();
            if (!/^1\d{10}$/.test(mobile)) {
                alert("请输入正确的手机号");
                return false;
            }

            if (!codeDom.hasClass('disabled')) {
                $.post(
                    '<?=\yii\helpers\Url::toRoute('api/get-code'); ?>',
                    {mobile : mobile},
                    function (res) {
                        res = JSON.parse(res);
                        if (res.status) {
                            if (codeDom.hasClass('disabled')) {
                                return false;
                            }

                            codeDom.attr('data-cycle', getCodeCycle).addClass('disabled');

                            var cycleClock = setInterval(function () {
                                var cycle = parseInt(codeDom.attr('data-cycle'));
                                if (cycle > 0) {
                                    codeDom.text("" + cycle + "s").attr('data-cycle', cycle-1);
                                } else {
                                    codeDom.text('验证码').removeClass('disabled');
                                    clearInterval(cycleClock);
                                }
                            }, 1000);
                        }
                    }
                )
            }

        });
    </script>
<?php endif;?>

