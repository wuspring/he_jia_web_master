<?php

use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/guider.php';
?>
	<div class="mx-auto w1200 clearfix">
		<nav class="breadcrumb com_crumbs">
			<span>当前位置：</span>
			<a class="breadcrumb-item" href="<?=UrlService::build(['index/index'])?>">首页</a>
			<a class="breadcrumb-item" href="<?=UrlService::build(['fitup-school/index'])?>">装修学堂</a>
			<span class="breadcrumb-item active"><?=$fitupCase->name?></span>
		</nav>	
	</div>
	<div class="fitup_case_detail">
		<div class="mx-auto w1200">
			<h3><?=$fitupCase->name?></h3>
			<a href="javascript:void(0)" class="btn btn-warning float-right" data-toggle="modal" data-target="#myModalAppointment">免费预约</a>
			<div class="d_more clearfix">
				<span class="s">装修公司：<a href="<?=UrlService::build(['dian-pu/index','storeId'=>$fitupCase->user_id])?>" class="blue"><?=$fitupCase->store->nickname?></a></span>
				<span class="s">小区名称：<span><?=$fitupCase->address?></span></span>
				<span class="s">设计师：<span><?=$fitupCase->design?></span></span>
				<span class="s"></span>
				<span class="s">建筑面积：<span><?=$fitupCase->area?>㎡</span></span>
				<span class="s">房源户型：<span><?=$fitupCase->getFitupAttr($fitupCase->house_type)->name?></span></span>
				<span class="s">设计风格：<span><?=$fitupCase->getFitupAttr($fitupCase->design_style)->name?></span></span>
				<span class="s">半包预算：<span><span class="orange"><?=$fitupCase->budget?></span>万</span></span>
			</div>
		</div>
			
	</div>
	<div class="mx-auto w1200 clearfix">
		<nav class="navbar navbar-expand-sm fitup_casedetail_type">  
		  <ul class="navbar-nav">
		  	<!-- li宽度自动，配置几个就显示几个，最后一个是按钮 -->
            <?php foreach ($fitupCase->tab_text as $k=>$v):?>
                <li class="nav-item">
                    <a href="#tabgroup<?=$v->title?>" class="nav-link"><?=$fitupCase->getFitupAttr($v->title)->name?></a>
                </li>
            <?php endforeach;?>

			<li class="nav-item nav_btns">
				<a href="javascript:void(0)" class="nav-link btn btn-warning" data-toggle="modal" data-target="#myModalAppointment">免费预约</a>
			</li>

		  </ul>
		</nav>
		<!-- 滚动监听 -->
		<script type="text/javascript">
		    var header = document.querySelector('.navbar');
		    var origOffsetY = header.offsetTop;
		    function onScroll(e){
		        window.scrollY >= origOffsetY ? header.classList.add('position-sticky'):header.classList.remove('position-sticky');
		    }
		    document.addEventListener('scroll', onScroll);
		</script>
        <?php foreach ($fitupCase->tab_text as $contentk=>$contentv):?>
            <div class="fitup_casedetail_con" id="tabgroup<?=$contentv->title?>">
                <p class="p_title"><span><?=$fitupCase->getFitupAttr($contentv->title)->name?></span></p>
                <div class="content">
                   <?=$contentv->content?>
                </div>
            </div>
        <?php endforeach;?>


	</div>
	

	<!-- 模态框-预约服务弹窗 -->
	<div class="modal fade" id="myModalAppointment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	      	<h4 class="modal-title pl-3 orange">预约和家网装修服务</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body pt-4 pb-4">	        
	        <form action="" class="password_form">
	        	<p class="mb-3 orange">已有<b>9860</b>名用户预约</p>
					<div class="password_form_li">
						<label class="password_form_li_name" for="">姓名：</label>
						<input type="text" name="name" class="password_form_li_input" placeholder="请输入您的姓名" required="">
						<p class="password_form_li_tip">请输入您的姓名</p>
					</div>
					<div class="password_form_li">
						<label class="password_form_li_name" for=""><span>*</span>手机号：</label>
						<input type="text" name="mobile" class="password_form_li_input" placeholder="请输入手机号码" required="">
						<p class="password_form_li_tip">请输入手机号码</p>
					</div>
					<p>填写您的信息，我们为您提供以下服务：<br><span class="orange">免费量房</span>&nbsp;&nbsp;<span class="orange">免费户型设计</span>&nbsp;&nbsp;<span class="orange">免费预算报价</span></p>
				</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-warning" data-dismiss="modal" id="submit_appointment" style="width: 80%">免费预约</button>
	      </div>
	    </div>
	  </div>
	</div>
  <?php include APP_PATH . '/views/common/footer.php'; ?>
 <script>

     $("#submit_appointment").on('click',function () {
           var name=$("input[name='name']").val();
           var mobile=$("input[name='mobile']").val();
           var url="<?=UrlService::build(['fitup-school/appointment-fitup'])?>";
           $.get(url,{name:name,mobile:mobile},function (res) {
               if (res.code==1){
                   alert(res.msg);
               } else {
                   alert(res.msg)
               }
              window.location.reload();
           },'json');


     })
 </script>
    

