
<?php

use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/guider.php';
?>
<div class="mx-auto w1200 clearfix pb-5">
		<nav class="breadcrumb com_crumbs">
			<span>当前位置：</span>
			<a class="breadcrumb-item" href="index.html">首页</a>
			<a class="breadcrumb-item" href="fitup_school.html">装修学堂</a>
			<a class="breadcrumb-item" href="newslist.html">展会资讯</a>
			<span class="breadcrumb-item active">家博新闻</span>
		</nav>
		<ul class="fitup_nav list-unstyled clearfix">
			<!-- 最多8个 -->
		    <li><a class="active" href="javascript:void(0)">家博新闻</a></li>
		    <li><a href="javascript:void(0)">行业资讯</a></li>
		    <li><a href="javascript:void(0)">时事热点</a></li>
		    <li><a href="javascript:void(0)">最新报道</a></li>
		    <li><a href="javascript:void(0)">媒体聚焦</a></li>
		    <li><a href="javascript:void(0)">展会热文</a></li>
		    <li><a href="javascript:void(0)">展会公告</a></li>
		    <li><a href="javascript:void(0)">展会专栏</a></li>
		</ul>
		<div class="fitup_containter clearfix">
			<div class="fitup_list float-left">
				<div class="fitup_list_con1 con">
					<div class="fitup_title">					
						<span class="s_title">家博新闻</span>
					</div>
					<div class="fitup_list1 index_news">
						<ul class="list-unstyled clearfix newslist2">
							<li class="clearfix">
								<a href="newsdetail.html" class="a_img"><img src="/public/index/images/_temp/img_news1.jpg" alt=""></a>
								<a href="newsdetail.html" class="a_title text-truncate">2019年上海春季家博会开展时间表</a>
								<p class="p ellipsis-3">2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表</p>
							</li>
							<li class="clearfix">
								<a href="newsdetail.html" class="a_img"><img src="/public/index/images/_temp/img_news1.jpg" alt=""></a>
								<a href="newsdetail.html" class="a_title text-truncate">2019年上海春季家博会开展时间表</a>
								<p class="p ellipsis-3">2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表</p>
							</li>
							<li class="clearfix">
								<a href="newsdetail.html" class="a_img"><img src="/public/index/images/_temp/img_news1.jpg" alt=""></a>
								<a href="newsdetail.html" class="a_title text-truncate">2019年上海春季家博会开展时间表</a>
								<p class="p ellipsis-3">2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表</p>
							</li>
							<li class="clearfix">
								<a href="newsdetail.html" class="a_img"><img src="/public/index/images/_temp/img_news1.jpg" alt=""></a>
								<a href="newsdetail.html" class="a_title text-truncate">2019年上海春季家博会开展时间表</a>
								<p class="p ellipsis-3">2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表</p>
							</li>
							<li class="clearfix">
								<a href="newsdetail.html" class="a_img"><img src="/public/index/images/_temp/img_news1.jpg" alt=""></a>
								<a href="newsdetail.html" class="a_title text-truncate">2019年上海春季家博会开展时间表</a>
								<p class="p ellipsis-3">2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表</p>
							</li>
							<li class="clearfix">
								<a href="newsdetail.html" class="a_img"><img src="/public/index/images/_temp/img_news1.jpg" alt=""></a>
								<a href="newsdetail.html" class="a_title text-truncate">2019年上海春季家博会开展时间表</a>
								<p class="p ellipsis-3">2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表</p>
							</li>
							<li class="clearfix">
								<a href="newsdetail.html" class="a_img"><img src="/public/index/images/_temp/img_news1.jpg" alt=""></a>
								<a href="newsdetail.html" class="a_title text-truncate">2019年上海春季家博会开展时间表</a>
								<p class="p ellipsis-3">2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表</p>
							</li>
							<li class="clearfix">
								<a href="newsdetail.html" class="a_img"><img src="/public/index/images/_temp/img_news1.jpg" alt=""></a>
								<a href="newsdetail.html" class="a_title text-truncate">2019年上海春季家博会开展时间表</a>
								<p class="p ellipsis-3">2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表2019年上海春季家博会开展时间表</p>
							</li>
						</ul>					
					</div>
				</div>
				<ul class="pagination justify-content-center">
					<li class="page-item disabled"><a class="page-link" href="#">«</a></li>
					<li class="page-item active"><a class="page-link" href="#">1</a></li>
					<li class="page-item"><a class="page-link" href="#">2</a></li>
					<li class="page-item"><a class="page-link" href="#">3</a></li>
					<li class="page-item"><a class="page-link" href="#">»</a></li>
				</ul>	
				

			</div>
			<div class="news_sidebar float-right">
                <?php include __DIR__ . '/_form.php'; ?>
				<div class="news_sidebar_hot con mt-3 pb-2">
					<div class="fitup_title">					
						<span class="s_title">热门资讯</span>
					</div>
					<div class="nsh_list">
						<a href="newsdetail.html"><img src="/public/index/images/_temp/img40.jpg" alt="2019年北京春季家博会开展时间表"><p class="ellipsis-3">2019年北京春季家博会开展时间表</p></a>
						<a href="newsdetail.html"><img src="/public/index/images/_temp/img40.jpg" alt="2019年北京春季家博会开展时间表"><p class="ellipsis-3">2019年北京春季家博会开展时间表2019年北京春季家博会开展时间表2019年北京春季家博会开展时间表</p></a>
						<a href="newsdetail.html"><img src="/public/index/images/_temp/img40.jpg" alt="2019年北京春季家博会开展时间表"><p class="ellipsis-3">2019年北京春季家博会开展时间表2019年北京春季家博会开展时间表</p></a>
						<a href="newsdetail.html"><img src="/public/index/images/_temp/img40.jpg" alt="2019年北京春季家博会开展时间表"><p class="ellipsis-3">2019年北京春季家博会开展时间表</p></a>
						<a href="newsdetail.html"><img src="/public/index/images/_temp/img40.jpg" alt="2019年北京春季家博会开展时间表"><p class="ellipsis-3">2019年北京春季家博会开展时间表</p></a>
						<a href="newsdetail.html"><img src="/public/index/images/_temp/img40.jpg" alt="2019年北京春季家博会开展时间表"><p class="ellipsis-3">2019年北京春季家博会开展时间表</p></a>
						
					</div>

				</div>
			</div>
		</div>
		


	</div>

<?php include APP_PATH . '/views/common/footer.php'; ?>

