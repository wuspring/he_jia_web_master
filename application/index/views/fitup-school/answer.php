<?php

use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/guider.php';
?>
<div class="mx-auto w1200 clearfix pb-5">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="<?=UrlService::build(['index/index'])?>">首页</a>
        <a class="breadcrumb-item" href="<?=UrlService::build(['fitup-school/index'])?>">装修学堂</a>
        <span class="breadcrumb-item active">装修问答</span>
    </nav>
    <div class="fitup_containter clearfix">
        <div class="fitup_list float-left">
            <div class="fitup_list_ask con">
                <div class="fla_title">
                    <a href="<?=UrlService::build(['fitup-school/answer','param'=>0])?>" class="<?=($param==0)?'active':''?>">所有问题</a>
                    <a href="<?=UrlService::build(['fitup-school/answer','param'=>1])?>" class="<?=($param==1)?'active':''?>">已回答</a>
                    <a href="<?=UrlService::build(['fitup-school/answer','param'=>2])?>" class="<?=($param==2)?'active':''?>">待回答</a>
                </div>
                <div class="fla_list">
                    <?php foreach ($model as $k=>$v):?>
                        <div class="flal_con">
                            <a href="<?=UrlService::build(['fitup-school/answer-detail','id'=>$v->id])?>" class="a_title text-truncate"><?=$v->problem?></a>
                            <p class="p_num"><a href="javascript:void(0)" class="red"><?=$v->answerNum?></a>回答</p>
                            <p class="p_more">提问者：<span><?= $v->member->nickname; ?></span><span><?=$v->create_time?></span>
                        </div>
                    <?php endforeach;?>

                </div>


            </div>

          <?php echo $pageHtml?>


        </div>
        <div class="news_sidebar float-right">
            <div class="news_sidebar_ask con">
                <div class="fitup_title">
                    <span class="s_title">装修问答</span>
                </div>
                <p class="p_all">已有<span><?=$problemNum?></span>位</p>
                <p class="p_more">业主都在这里找到了她们想要的答案</p>
                <div class="btns">
                    <a href="javascript:void(0)" class="a_ask" data-toggle="modal" data-target="#myModalAsk">我要提问</a>
                    <a href="<?=UrlService::build(['fitup-school/answer'])?>" class="a_answer">我来回答</a>
                </div>


            </div>
        </div>
    </div>


</div>

<!-- 模态框-我要提问弹框 -->
<div class="modal fade" id="myModalAsk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title pl-3">我要提问</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-5 pb-4">
                <form action="" class="password_form">
                    <div class="password_form_li">
                        <textarea class="textarea" placeholder="请输入您的问题"></textarea>
                        <p class="password_form_li_tip">请输入您的问题</p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-warning"  data-id="submitProblem" data-dismiss="modal">确定</button>
            </div>
        </div>
    </div>
</div>

<?php include APP_PATH . '/views/common/footer.php'; ?>
