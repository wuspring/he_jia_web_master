<?php

use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/guider.php';
?>

    <div class="mx-auto w1200 clearfix pb-5">
        <nav class="breadcrumb com_crumbs">
            <span>当前位置：</span>
            <a class="breadcrumb-item" href="<?= UrlService::build('index/index'); ?>">首页</a>
            <a class="breadcrumb-item" href="<?= UrlService::build('fitup-school/index'); ?>">装修学堂</a>
            <span class="breadcrumb-item active">装修问答</span>
        </nav>
        <div class="fitup_containter clearfix">
            <div class="fitup_list float-left">
                <div class="fitup_ask_title con">
                    <p class="p_title"><img src="/public/index/images/icon_ask1.jpg"><?=$problem->problem?></p>
                    <p class="p_user"><span class="float-right"><?=$problem->create_time?></span><img
                                src="<?=($problem->member->avatarTm)?:'/public/index/images/img_default_user.jpg'?>"><span>
                            <?= $problem->member->nickname; ?></span></p>
                </div>
                <div class="fitup_ask_content con mt-3">
                    <?php foreach ($problem->answerObj as $k=>$v):?>
                        <div class="fac_list">
                            <img src="<?=($v->member->avatarTm)?:'/public/index/images/img_default_user.jpg'?>" alt="头像" class="img float-left">
                            <p class="p_con"><span><?= $v->member->nickname; ?></span><?=$v->content?></p>
                            <p class="p_time text-right"><?=$v->create_time?></p>
                        </div>
                    <?php endforeach;?>
                </div>
                <div class="fitup_answer con mt-3">
                    <p class="p_title red">我也要回答</p>
                    <form action="" class="fitup_answer_form">
                        <textarea class="textarea" placeholder="对于这个问题，我有些话想要说……"></textarea>
                        <!-- 如果没有登录，则出来下面的 -->
                        <?php if ($is_login==0):?>
                            <div class="a_btns">
                                <a href="<?=UrlService::build(['member/login'])?>" class="a_login">登录</a>
                                <a href="<?=UrlService::build(['member/reg'])?>" class="a_reg">注册</a>
                            </div>
                        <?php else:?>
                            <a href="javascript:void(0)" id="submit_answer" class="btn btn-success">提交</a>
                        <?php endif;?>

                    </form>
                </div>


            </div>
            <div class="news_sidebar float-right">
                <div class="news_sidebar_ask con">
                    <div class="fitup_title">
                        <span class="s_title">装修问答</span>
                    </div>
                    <p class="p_all">已有<span><?=$problemNum?></span>位</p>
                    <p class="p_more">业主都在这里找到了她们想要的答案</p>
                    <div class="btns">
                        <a href="javascript:void(0)" class="a_ask" data-toggle="modal"
                           data-target="#myModalAsk">我要提问</a>
                        <a href="<?=UrlService::build(['fitup-school/answer'])?>" class="a_answer">我来回答</a>
                    </div>


                </div>
            </div>
        </div>


    </div>

    <!-- 模态框-我要提问弹框 -->
    <div class="modal fade" id="myModalAsk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title pl-3">我要提问</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body pt-5 pb-4">
                    <form action="" class="password_form">
                        <div class="password_form_li">
                            <textarea class="textarea" placeholder="请输入您的问题"></textarea>
                            <p class="password_form_li_tip">请输入您的问题</p>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="button" class="btn btn-warning" data-id="submitProblem" data-dismiss="modal">确定</button>
                </div>
            </div>
        </div>
    </div>
<?php include APP_PATH . '/views/common/footer.php'; ?>
<script>

    //回答的问题提交
     $("#submit_answer").on('click',function () {
        var text=$(".fitup_answer_form").children('textarea').val();
        var url="<?=UrlService::build(['fitup-school/member-answer'])?>";

        if (text.length < 1) {
            alert("您还没有填写回答哦");
            return false;
        }

        $.get(url,{problemId:<?=$problem->id?>,text:text},function (res) {
             if (res.code==1 || res.code==0){
                alert(res.msg);
                window.location.reload();
             }
             if (res.code==2){
                alert(res.msg);
                window.location.href="<?=UrlService::build(['member/login'])?>";
             }
        },'json')

     })

</script>
