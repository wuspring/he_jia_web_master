<?php

use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/guider.php';

function getAttr($pid){
    $attr=\common\models\FitupAttr::findAll(['pid'=>$pid]);
     return $attr;
}
?>
<script>
    $(function () {
        <?php
        if ($_GET) {
            echo 'var obj=' . json_encode($_GET) . ';';
        }
        ?>
        if (typeof(obj) != 'undefined') {

            for(k in obj){

               if (k!='r' && k!='paixu'){
                       $("#"+k).val(obj[k]);
                       $("a["+k+"="+obj[k]+"]").addClass("active").siblings().removeClass("active");
               }
               if (k=='paixu' && obj[k].length>0){

                    $("a[tab="+obj[k]+"]").addClass("active");
               }
            }
        }
    });
    function Filter(a, b) {

        var $ = function (e) {
            return document.getElementById(e);
        };
        var ipts = $('filterForm').getElementsByTagName('input'), result = [];

        for (var i = 0, l = ipts.length; i < l; i++) {
            if (ipts[i].getAttribute('to') == 'filter') {
                result.push(ipts[i]);
            }
        }
        if ($(a)) {
            $(a).value = b;
            for (var j = 0, len = result.length; j < len; j++) {

                if (result[j].value == '' || result[j].value == '0') {
                    result[j].parentNode.removeChild(result[j]);
                }
            }
           document.forms['filterForm'].submit();
        }
        return false;
    }
</script>
<div class="mx-auto w1200 clearfix pb-5">
		<nav class="breadcrumb com_crumbs">
			<span>当前位置：</span>
			<a class="breadcrumb-item" href="<?=UrlService::build(['index/index'])?>">首页</a>
			<a class="breadcrumb-item" href="<?=UrlService::build(['fitup-school/index'])?>">装修学堂</a>
			<span class="breadcrumb-item active">装修案例</span>
		</nav>
    <form id="filterForm" name="filterForm" method="get">
        <input type="hidden" name="r" value="fitup-school/fitup-case">
        <input id="<?='paixu' ?>" type="hidden" value="" name="<?='paixu' ?>">
		<div class="fitup_case_search">
            <?php foreach ($fitupAttr as $sttr_k=>$attr_v):?>
                <input id="<?='cx'.$sttr_k ?>" type="hidden" value="" name="<?='cx'.$sttr_k ?>" to="filter">
                <div class="com_type clearfix">
                    <div class="com_type_title float-left"><?=$attr_v->name?></div>
                    <div class="com_type_con">
                          <a  class="active clickActive" <?='cx'.$sttr_k?>='0' href="javascript:Filter('<?='cx'.$sttr_k ?>','0');" >全部</a>
                          <?php foreach (getAttr($attr_v->id) as $children_k=>$children_v):?>
                           <a class="clickActive" <?='cx'.$sttr_k?>=<?= $children_v->id ?>  href="javascript:Filter('<?='cx'.$sttr_k?>','<?=$children_v->id ?>');"><?=$children_v->name?></a>
                          <?php endforeach;?>
                    </div>
                </div>
            <?php endforeach;?>
		</div>

		<div class="fitup_case_all mt-4">
			<p class="p_right float-right">共检索到<span><?=$amount?></span>个精品案例</p>
			<a  href="javascript:void(0)" tab="tj" class="a_groom">推荐</a>
			<a  href="javascript:void(0)" tab="zx" class="a_new">最新</a>
		</div>
    </form>
		<div class="fitup_case_list clearfix mt-4">

            <?php foreach ($model as $k=>$v):?>
                <div class="fcl_con">
                    <a href="<?=UrlService::build(['fitup-school/fitup-case-detail','caseId'=>$v->id])?>" class="a_img"><img src="<?=$v->cover_pic?>" alt=""></a>
                    <p class="p_name">
                        <a href="<?=UrlService::build(['dian-pu/index','storeId'=>$v->user_id])?>" class="a_shop text-truncate float-right"><?=$v->store->nickname?></a>
                        <a href="<?=UrlService::build(['fitup-school/fitup-case-detail','caseId'=>$v->id])?>" class="a_name text-truncate"><?=$v->name?></a>
                    </p>
                    <p class="p_tag"><span class="float-right"><span>半包</span>|<span><?=$v->budget?>万</span></span><span><span><?=$v->getFitupAttr($v->design_style)->name?></span>|<span><?=$v->area?>㎡</span></span></p>
                </div>
            <?php endforeach;?>

		</div>

        <?php echo $pageHtml?>

	</div>
<?php include APP_PATH . '/views/common/footer.php'; ?>
<script>
    //查询改变
    $(document).on('click','.clickActive',function () {
        $(this).addClass("active").siblings().removeClass("active");
    });
    //点击查询 a
    $(document).on('click','.fitup_case_all a',function () {
        var tab=$(this).attr("tab");
        $(this).addClass('active').siblings().removeClass('active');
        replaceParamVal("paixu",tab);
    });

    //替换指定传入参数的值,paramName为参数,replaceWith为新值
    function replaceParamVal(paramName,replaceWith) {
        var oUrl = this.location.href.toString();
        var re=eval('/('+ paramName+'=)([^&]*)/gi');
        var nUrl = oUrl.replace(re,paramName+'='+replaceWith);
        this.location = nUrl;
        window.location.href=nUrl
    }

</script>