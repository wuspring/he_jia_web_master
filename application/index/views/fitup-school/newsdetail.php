<?php

use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/guider.php';
?>

<div class="mx-auto w1200 clearfix pb-5">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="<?=UrlService::build(['index/index'])?>">首页</a>
        <a class="breadcrumb-item" href="<?=UrlService::build(['fitup-school/index'])?>">装修学堂</a>
        <span class="breadcrumb-item active">展会资讯</span>
    </nav>
    <div class="fitup_containter clearfix">
        <div class="fitup_list float-left">
            <div class="fitup_list_con5 con">
                <h3><?=$news->title?></h3>
                <p class="p_remark">分类：<span><?=$news->class->name?></span> | <span><?=$news->createTime?></span> | 阅读数：<span><?=$news->clickRate?></span> |
                    来源：<span>和家网</span></p>
                <div class="fitup_content5">
                    <?=$news->content?>

                </div>
            </div>
            <div class="fitup_list_con6 con mt-3">
                <div class="fitup_title">
                    <span class="s_title">相关新闻</span>
                </div>
                <ul class="fitup_list6 list-unstyled clearfix">
                    <?php foreach ($relationNews as $k=>$v):?>
                    <li><a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$v->id])?>" class="text-truncate"><?=$v->title?></a></li>
                    <?php endforeach;?>
                </ul>

            </div>


        </div>
        <div class="news_sidebar float-right">
            <div class="news_sidebar_hot con pb-2">
                <div class="fitup_title">
                    <span class="s_title">热门资讯</span>
                </div>
                <div class="nsh_list">
                    <?php foreach ($hotNews as $hotk=>$hotv):?>
                        <a href="<?=UrlService::build(['fitup-school/newsdetail','newsId'=>$hotv->id])?>"><img src="<?=$hotv->themeImg?>" alt="">
                            <p class="ellipsis-3"><?=$hotv->title?></p></a>
                    <?php endforeach;?>

                </div>

            </div>
        </div>
    </div>


</div>


<?php include APP_PATH . '/views/common/footer.php'; ?>
