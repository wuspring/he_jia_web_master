<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\assets\AppAsset;
use wap\widgets\Alert;
use \DL\vendor\ConfigService;
use \common\models\OrderGoods;
use \common\models\Order;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
global $title, $keywords, $description, $userCurrentCity;

isset($title) or $title = ConfigService::init('system')->get('name');
isset($keywords) or $keywords = ConfigService::init('system')->get('keywords');
isset($description) or $description = ConfigService::init('system')->get('description');

?>

<?php $this->beginPage() ?>
    <!doctype html>
    <html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= $title; ?></title>
        <meta name="keywords" content="<?= $keywords; ?>">
        <meta name="description" content="<?= $description; ?>。">
        <link rel="icon" href="/public/index/images/favicon.ico" mce_href="favicon.ico" type="image/x-icon">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="/public/index/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="/public/index/css/style.css"/>

        <!--    <script type="text/javascript" src="/public/index/js/jquery.slim.min.js"></script>-->
        <script type="text/javascript" src="/public/js/jquery.min.js"></script>
        <script type="text/javascript" src="/public/index/js/popper.min.js"></script>
        <script type="text/javascript" src="/public/index/js/bootstrap.js"></script>
        <script type="text/javascript" src="/public/index/js/loading.js"></script>
        <script type="text/javascript" src="/public/js/expand/functions.js"></script>
        <script type="text/javascript" src="/public/js/base_bind.js"></script>

        <script>
            var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>",
                <?php if (Yii::$app->user->isGuest): ?>
                userInfo = [],
                hasCouponGoods = [],
                hasCouponIds = [],
                hasStoreIds = [];
                <?php else :
                $userInfo = Yii::$app->user->identity;
                $info = [
                    'id' => $userInfo->id,
                    'username' => $userInfo->username,
                    'nickname' => $userInfo->nickname,
                    'avatar' => $userInfo->avatar,
                    'birthday' => date('Y-m-d', strtotime($userInfo->birthday))
                ];

                $hasOrderGoodsIds = [];

                $orders = Order::find()->where([
                        'and',
                        ['=', 'buyer_id', $userInfo->id],
                        ['>', 'add_time', date('Y-m-d', strtotime('-30 days'))],
                        ['=', 'type', Order::TYPE_ZHAN_HUI],
                        [
                                'or',
                            [
                                'and',
//                                ['in', 'order_state', [Order::STATUS_WAIT, Order::STATUS_PAY]],
                                ['=', 'ticket_type', Order::TICKET_TYPE_COUPON]
                            ],
                            [
                                'and',
                                ['not in', 'order_state', [Order::STATUS_WAIT]],
                                ['=', 'ticket_type', Order::TICKET_TYPE_ORDER]
                            ]
                        ]
                ])->all();

                if ($orders) {
                    $orderIds = arrayGroupsAction($orders, function ($order) {
                        return $order->orderid;
                    });

                    $hasOrders = OrderGoods::find()->where([
                        'in', 'order_id', $orderIds
                    ])->all();

                    $hasOrderGoodsIds = arrayGroupsAction($hasOrders, function ($hasOrder) {
                        return $hasOrder->goods_id;
                    });
                }

                $memberStoreCoupons = \common\models\MemberStoreCoupon::findAll([
                        'member_id' => $userInfo->id
                ]);
                $couponIds = arrayGroupsAction($memberStoreCoupons, function ($memberStoreCoupon) {
                    return $memberStoreCoupon->coupon_id;
                });

                $dTickets = \DL\Project\CityTicket::init($userCurrentCity->id)->currentFactionTickets();
                $dTicketIds = $dTickets ? array_keys($dTickets) : [0];

                $dLogs = \common\models\MemberTicketStore::find()->where([
                        'and',
                        ['=', 'member_id', $userInfo->id],
                        ['in', 'ticket_id', $dTicketIds]
                ])->all();

                $hasStoreIds = $dLogs ? arrayGroupsAction($dLogs, function ($dLog) {
                    return $dLog->store_id;
                }) : [];
                ?>
                userInfo = <?= json_encode($info); ?>,
                hasCouponGoods = <?= json_encode($hasOrderGoodsIds); ?>,
                hasCouponIds = <?= json_encode($couponIds); ?>,
                hasStoreIds = <?= json_encode($hasStoreIds); ?>;
                <?php endif; ?>

            var LOGIN_PATH = '<?= \DL\service\UrlService::build('member/login'); ?>',
                REQUEST_CREATE_P_ORDER = '<?= \DL\service\UrlService::build('order/create-price'); ?>',
                REQUEST_CREATE_ORDER = '<?= \DL\service\UrlService::build('order/create'); ?>',
                GET_STORE_COUPON = '<?= \DL\service\UrlService::build(['dian-pu/receive-coupon'])?>',
                SUBMIT_PROBLEM='<?= \DL\service\UrlService::build(['fitup-school/submit-problem'])?>',
                CART_ROUTE_INFO = '<?= \DL\service\UrlService::build("integral-cart/cart-info"); ?>',
                CART_ROUTE_INDEX = '<?= \DL\service\UrlService::build("integral-cart/index"); ?>',
                JOIN_ORDER_PRICE = '<?= \DL\service\UrlService::build("api/goods-order-price"); ?>',
                JOIN_STORE_COUPON = '<?= \DL\service\UrlService::build("api/store-coupons"); ?>',
                MEMBER_LOGIN = '<?= \DL\service\UrlService::build("member/sign"); ?>',
                GET_STORE_APPOINT = '<?= \DL\service\UrlService::build("api/appoint-store"); ?>',
                JOIN_ORDER_PRICE_SUCCESS = '<?= \DL\service\UrlService::build("api/goods-order-success"); ?>';
        </script>
        <style>
            input:focus, textarea:focus {
                border: solid 1px #fc7202!important;
                outline: none!important;
            }

            .modal-success .modal-header {
                display: block;
            }

            .modal_suc_con .con .d {
                max-height: 160px;
                overflow: hidden;
            }

            .modal_product .p_remark {
                max-height: 80px;
                overflow: hidden;
            }
        </style>
    </head>
    <body class="bg_glay">

    <?= $content; ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <?php include __DIR__ . '/../common/modal.php'; ?>

    <script type="text/javascript" src="/public/index/js/base.js"></script>
    <script type="text/javascript">
        //展会页左侧导航
        navFun();
        //点击收藏本站
        $("#addcollect").click(function () {
            var ctrl = (navigator.userAgent.toLowerCase()).indexOf('mac') != -1 ? 'Command/Cmd' : 'CTRL';
            if (document.all) {
                window.external.addFavorite('http://hj.zxrweb.com', '和家网')
            } else if (window.sidebar) {
                window.sidebar.addPanel('和家网', 'http://hj.zxrweb.com', "")
            } else {
                alert('您可以尝试通过快捷键' + ctrl + ' + D 加入到收藏夹~')
            }
        })

    </script>

    <script>
        $('[data-order-price], [data-order], [data-dlauto]').each(function() {
            var val = $(this).attr('data-val'),
                text = $(this).text();

            if (hasCouponGoods.indexOf(val) > -1) {
                $(this).addClass('disabled').text('已' + text.slice(-2));
            }

        });

        $('[data-id="getCoupin"]').each(function() {
            var val = $(this).attr('data-val'),
                text = $(this).text();

            if (hasCouponIds.indexOf(val) > -1) {
                $(this).addClass('disabled').text('已' + text.slice(-2));
            }
        });

        $('[data-id="getStore"]').each(function() {
            var val = $(this).attr('data-val'),
                text = $(this).text();

            if (hasStoreIds.indexOf(val) > -1) {
                $(this).addClass('disabled').text('已' + text.slice(-2));
            }
        });
    </script>
    </body>
    </html>
<?php $this->endPage() ?>