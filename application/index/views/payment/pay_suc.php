<?php

use DL\service\UrlService;

include APP_PATH . '/views/common/header.php'; ?>


<div class="mx-auto w1200 clearfix pay_result">
    <p class="case_title"><span class="green">在线支付订单</span></p>
    <div class="pay_result_con">
        <img src="/public/index/images/img_result_suc.png" alt="订单支付成功" class="img">
        <div class="con">
            <p class="p1 mt-5 pt-4">您已经付款成功！~</p>

        </div>
    </div>
    <div class="pay_result_btns text-right"><span>您现在可以去：</span><a href="<?=UrlService::build(['index/index'])?>" class="btn btn-danger">再去逛逛</a><a
                href="<?=UrlService::build(['personal/appointment-order'])?>" class="a_show green">查看订单</a></div>


</div>


<?php include APP_PATH . '/views/common/footer.php'; ?>
