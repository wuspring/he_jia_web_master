<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\LinkPager;
include APP_PATH . '/views/common/header.php'; ?>

<script src="/public/js/jquery.qrcode.min.js"></script>
<style>

    .index_searches{
        display: none;
    }
    /*-------收银台-------*/
    .cash_con {
        padding: 75px 0;
        /*background-color: #FAFAFA*/
    }

    .cash_con .title {
        margin-bottom: 70px;
        font-size: 18px;
        font-weight: 400;
    }

    .cash_con span {
        font-size: 12px;
        color: #999;
        margin-left: 50px;
    }

    /*微信支付*/
    .wxSao {
        display: flex;
        flex-direction: row
    }

    .cash_con .erw_box {
        width: 500px;
        height: 550px;
        padding-left: 120px;
    }

    .cash_con .erw_box .pic {
        width: 360px;
        height: 360px;
        background: #fff;
        box-shadow: 0px 2px 4px 0px rgba(221, 221, 221, 1);
        border-radius: 2px;
        border: 1px solid #D8D8D8;
        padding: 5px;
    }

    .cash_con .erw_box .pic img {
        width: 350px;
        height: 350px;
    }

    .cash_con .erw_box .scanning img {
        width: 44px;
        height: 41px;
        margin: 27px 0 0 95px;
        float: left;
    }

    .cash_con .erw_box .scanning {
        width: 360px;
        height: 84px;
        background: url(../public/index/images/syt_hk.png) no-repeat;
        margin-top: 15px;
    }

    .cash_con .erw_box .scanning p {
        color: #fff;
        line-height: 20px;
        font-size: 14px;
        margin-left: 160px;
        padding-top: 25px;
    }

    .cash_con .example_pic {
        height: 550px;
        width: 500px;
    }

    .cash_con .example_pic img {
        width: 397px;
        height: 508px;
    }

    .cash_con .other {
        margin: 20px 0;
    }

    .cash_con .other a, .cash_con .other a:hover {
        color: #4A90E2;
    }

    .cash_con .other .icon {
        margin-top: -2px;
        margin-right: 10px;
    }
    .clearfixTitle{
        padding-left: 120px;
    }
</style>

<div class="mx-auto w1200 clearfix">
    <!-- 收银台内容 -->
    <div class="cash_con clearfix">
        <div class="title clearfix clearfixTitle">
            微信支付<span>过期后请刷新页面重新获取二维码</span>
        </div>
        <div class="wxSao">
            <div class="erw_box">
                <div class="pic" id="qrcodeCanvas"></div>
                <div class="scanning">
                    <img src="/public/index/images/scan_it.png" alt="扫一扫">
                    <p>请使用微信扫一扫<br/>扫描二维码支付</p>
                </div>
            </div>
            <div class="example_pic">
                <img src="/public/index/images/img_defaultwx.png" alt="微信支付">
            </div>
        </div>


        <div class="other">
<!--            <a href="javascript:history.back(-1)"><i class="icon icon_left"></i>选择其他支付方式</a></div>-->
    </div>
</div>
</div>
<!-- Optional JavaScript -->
<script>

    $('#qrcodeCanvas').qrcode({
        render: "canvas",
        width: 350,
        height: 350,
        text: "<?=$codeurl?>"
    });


    var ntimer = null;

    checkOrderPayed("<?=$order->orderid?>");

    function checkOrderPayed(orderId) {
        ntimer = setInterval(function () {
            $.get("<?php echo Url::toRoute(['order/status']);?>", {number: orderId}, function(res) {
                // res = JSON.parse(res);
                if(res.status){
                    clearInterval(ntimer);
                    window.location.href = '<?php echo Url::toRoute(["payment/success",'number'=>$order->orderid]);?>';
                    return false;
                }
            },'json');
        },3000);
    }

</script>
<?php include APP_PATH . '/views/common/footer.php'; ?>