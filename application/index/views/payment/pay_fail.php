<?php

use DL\service\UrlService;

include APP_PATH . '/views/common/header.php'; ?>

<div class="mx-auto w1200 clearfix pay_result">
    <p class="case_title"><span class="green">在线支付订单</span></p>
    <div class="pay_result_con">
        <img src="/public/index/images/img_result_fail.png" alt="订单支付失败" class="img">
        <div class="con">
            <p class="p1 mt-5 pt-4">抱歉，支付失败！请再次提交支付哦~</p>
        </div>
    </div>
    <div class="pay_result_btns text-right"><span>没有付款成功：</span><a href="javascript:void(0)"
                                                                   class="btn btn-danger">再次支付</a></div>


</div>


<?php include APP_PATH . '/views/common/footer.php'; ?>

    
