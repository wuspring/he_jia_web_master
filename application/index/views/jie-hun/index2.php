<?php
use DL\service\UrlService;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php';


function getBanner($pics)
{
    $pics = (array)$pics;
    return arrayGroupsAction($pics, function ($pic) {
        list($href, $img) = explode('@', $pic);
        return ['url' => $href, 'img' => $img];
    });
}

function getHuodong($huodongs)
{
    strpos($huodongs, '@') < 0 and $huodongs .= '@';
    list($href, $img, $expand, $button) = explode('@', $huodongs);
    return (object)[
        'url' => $href,
        'img' => $img,
        'expand' => is_null($expand) ? '' : $expand,
        'button' => (is_null($button) or strlen($button) < 1) ? '点击领取>' : $button
    ];
}

?>

<div id="slider" class="carousel slide" data-ride="carousel">
    <!-- 指示符 -->
    <ul class="carousel-indicators">
        <?php $bPics = getBanner($configIndex['banner']); if(count($bPics) >1): foreach ($bPics as  $pick=>$picv):?>
            <li data-target="#slider" data-slide-to="<?=$pick?>" class="<?=$pick==0?'active':''?>"></li>
        <?php endforeach;endif;?>
    </ul>
    <!-- 轮播图片 -->
    <div class="carousel-inner">
        <?php $bPics = getBanner($configIndex['banner']); foreach ($bPics as  $pick=>$picv):?>
            <a href="<?= $picv['url']; ?>" class="carousel-item <?= $pick ? '' : 'active'; ?>" style="background-image: url('<?= $picv['img']; ?>');"></a>
        <?php endforeach;?>
    </div>

    <!-- 左右切换按钮 -->
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>

    <div class="explosin_banner">
        <?php include __DIR__ . '/form.php'; ?>
    </div>
</div>
<!-- ad -->
<div class="index_ad pt-5 pb-5 bg_white">
    <div class="index_ad_con mx-auto w1200 clearfix">
        <a href="javascript:void(0)" title="prev" class="ipl_btn a_prev"></a>
        <a href="javascript:void(0)" title="next" class="ipl_btn a_next"></a>
        <div class="ipl_main">
            <ul class="iplm_ul list-unstyled clearfix">
                <?php
                $huodongDatas = $configIndex['huodongs'];
                $hKeys = get_object_vars($huodongDatas);

                if ($hKeys) : foreach ($hKeys AS $hKey) :
                    if (strlen(getHuodong($hKey)->img)) :
                        ?>
                        <li>
                            <a href="<?= getHuodong($hKey)->url; ?>" target="_blank">
                                <img src="<?= getHuodong($hKey)->img; ?>" class="loading_img" data-url="<?= getHuodong($hKey)->img; ?>" alt="">
                                <?php if (getHuodong($hKey)->expand) : ?>
                                <p>
                                    <span class="s1 ellipsis-3"><?= getHuodong($hKey)->expand; ?></span>
                                    <?php if (getHuodong($hKey)->button) : ?>
                                    <span class="s2 btn"><?= getHuodong($hKey)->button; ?></span>
                                    <?php endif; ?>
                                </p>
                                <?php endif; ?>
                            </a>
                        </li>

                    <?php endif;endforeach;endif; ?>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">

    sliderEvent( $('.index_ad_con .a_prev'), $('.index_ad_con .a_next') , $('.iplm_ul') , $('.index_ad_con') , 500 , true , 4);

    //轮播效果1：左右轮播，自动轮播
    //参数说明：left,right为左右按钮，list为列表的上级，stopObj为鼠标移入停止自动轮播的对象，times为动画时间,auto
    //Flag是否自动轮播,ind为一次轮播的张数
    function sliderEvent(left,right,list,stopObj,times,autoFlag,ind){
        stopObj.each(function(){
            var btnLeft = left,
                btnRight = right,
                teamsList=list;
            var teamsWidth = parseInt(teamsList.children().outerWidth())*ind;
            var teamsWidthS = parseInt(teamsList.children().outerWidth());
            var teamsLength = teamsList.children().length;
            var teamsTime = times;
            var sliderEventID;
            if(teamsLength>ind){
                teamsList.width(teamsWidth*teamsLength/ind);
                // console.log(teamsList.width());
                // teamsList[0].innerHTML +=  teamsList[0].innerHTML;
                btnLeft.bind('click',function(){
                    btnLeftFun()
                });
                btnRight.bind('click',function(){
                    btnRightFun();
                });
                if(autoFlag){
                    sliderEventID = setInterval(function(){
                        btnRightFun();
                    },2500);
                }
                stopObj.hover(function(){
                    clearInterval(sliderEventID);
                },function(){
                    clearInterval(sliderEventID);
                    if(autoFlag){
                        sliderEventID = setInterval(function(){
                            btnRightFun();
                        },2500);
                    }
                })
            }else{
                left.hide();
                right.hide();
            }
            function btnRightFun(){
                teamsList.stop(true,true).animate({left:parseInt(teamsList.css("left"))-teamsWidthS},teamsTime,function(){
                    if(parseInt(teamsList.css("left"))<=-teamsWidth*teamsLength/ind+teamsWidthS*ind){
                        teamsList.css("left",0);
                    }
                });
            }
            function btnLeftFun(){
                if(parseInt(teamsList.css("left"))>=0){
                    teamsList.css("left",-teamsWidth*teamsLength/ind);
                }
                teamsList.stop(true,true).animate({left:parseInt(teamsList.css("left"))+teamsWidthS},teamsTime,function(){
                    if(parseInt(teamsList.css("left"))>=0){
                        teamsList.css("left",-teamsWidth*teamsLength/ind+teamsWidthS*ind);
                    }
                });
            }
        })
    };
</script>

<div class="mx-auto w1200">
    <div class="mx-auto w1200">
        <?php foreach ($gcDatas AS $index => $gcData) :?>
            <div class="column_floor mt-4">
                <div class="column_floor_top">
                    <div class="cft_links float-right">
                        <a href="<?=\DL\service\UrlService::build(['jie-hun/jing-xuan'])?>">精选店铺 |</a>
                        <a href="<?=\DL\service\UrlService::build(['jie-hun/can-zhan'])?>">参展品牌 |</a>
                        <a href="<?=\DL\service\UrlService::build(['jie-hun/yu-yue'])?>">爆款预约 |</a>
                        <a href="<?=\DL\service\UrlService::build(['jie-hun/you-hui'])?>">抵用券 |</a>
                        <a href="<?=\DL\service\UrlService::build(['jie-hun/brand-activity'])?>">更多>></a>
                    </div>
                    <p><?= $gcData['info']->name; ?></p>
                </div>
                <div class="column_floor_bottom clearfix">
                    <div class="cfb_left float-left">
                        <p class="p_title">TOP排名<span>(口碑值)</span></p>
                        <?php foreach ($gcData['commends'] AS $store) :?>
                            <a href="<?=\DL\service\UrlService::build(['dian-pu/index', 'storeId' => $store->user_id])?>" class="cfbl_brand">
                                <img src="<?= $store->store->avatar; ?>" data-url="<?= $store->store->avatar; ?>" alt="<?= $store->store->nickname; ?>" class="loading_img">
                                <p class="p red"><?= $store->score; ?></p>
                                <p class="text-truncate"><?= $store->store->nickname; ?></p>
                            </a>
                        <?php endforeach; ?>

                    </div>
                    <div class="cfb_con float-right">
                        <div class="cfbc_slider">
                            <div class="carousel slide column_slider" data-ride="carousel" id="slider1">
                                <!-- 指示符 -->
                                <ul class="carousel-indicators">
                                    <?php $pic = $gcData['zhFirstPics']; foreach ($pic AS $i => $pic1) :?>
                                        <li data-target="#slider<?= "{$index}_{$i}"; ?>" data-slide-to="<?= "{$index}_{$i}" ?>" class="<?= $i ? '' : 'active'; ?>"></li>
                                    <?php endforeach; ?>
                                </ul>
                                <!-- 轮播图片 -->
                                <div class="carousel-inner">
                                    <?php $pic = $gcData['zhFirstPics'];  foreach ($pic AS $i => $pic1) :?>
                                        <a href="javascript:void(0)" class="carousel-item <?= $i ? '' : 'active'; ?>" style="background-image: url('<?= $pic1;?>');"></a>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <?php foreach ($gcData['pics'] AS $n => $pic) : ?>
                            <?php
                            $css = '';
                            switch ($n) {
                                case 'pic_1' :
                                    continue 2;
                                case 'pic_2' :
                                    $css = 'p_one';
                                default :
                                    $pic1 = reset($pic['src']);

                            }
                            ?>
                            <a href="<?= $pic['href'];?>" class="cfbc_product <?= $css; ?>">
                                <img src="<?= $pic1;?>" data-url="<?= $pic1;?>" class="loading_img" alt="">
                            </a>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?php include APP_PATH . '/views/common/footer.php';?>


