<?php
use DL\service\UrlService;
use common\models\StoreGcScore;
use \common\models\User;
use common\models\TicketApplyGoods;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php';

function getBanner($pics)
{
    $pics = (array)$pics;
    return arrayGroupsAction($pics, function ($pic) {
        list($href, $img) = explode('@', $pic);
        return ['url' => $href, 'img' => $img];
    });
}

function getHuodong($huodongs)
{
    strpos($huodongs, '@') < 0 and $huodongs .= '@';
    list($href, $img) = explode('@', $huodongs);
    return (object)[
        'url' => $href,
        'img' => $img
    ];
}

function getUser($userId)
{
    $model=User::findOne(['id'=>$userId]);
    return $model?:(new User());
}
function getZhPics($zhPics)
{
    $info = json_decode($zhPics, true);
    foreach ($info AS $key => $in) {
        list($href, $img, $expand, $button) = explode('@', $in);
        $info[$key] = (object)[
            'url' => strlen($href) ? $href : 'javascript:void(0)',
            'img' => $img,
            'expand' => is_null($expand) ? '' : $expand,
            'button' => is_null($button) ? '' : $button
        ];
    }

    return $info;
}

?>
    <div id="slider" class="carousel slide" data-ride="carousel">
        <!-- 指示符 -->
        <ul class="carousel-indicators">
            <?php $bPics = getBanner($configIndex['banner']);if(count($bPics) >1): foreach ($bPics as  $pick=>$picv):?>
                <li data-target="#slider" data-slide-to="<?=$pick?>" class="<?=$pick==0?'active':''?>"></li>
            <?php endforeach;endif;?>
        </ul>
        <!-- 轮播图片 -->
        <div class="carousel-inner">
            <?php $bPics = getBanner($configIndex['banner']); foreach ($bPics as  $pick=>$picv):?>
                <a href="<?= $picv['url']; ?>" class="carousel-item <?= $pick ? '' : 'active'; ?>" style="background-image: url('<?= $picv['img']; ?>');"></a>
            <?php endforeach;?>
        </div>

        <!-- 左右切换按钮 -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>

        <div class="explosin_banner">
            <?php include __DIR__ . '/form.php'; ?>
        </div>
    </div>



	<div class="com_title" id="nav0">
		<em class="line line1"></em>
		<h3>婚博会亮点</h3>
		<em class="line line2"></em>
	</div>
    <!-- ad -->
    <div class="index_ad explosin_ad">
        <div class="index_ad_con mx-auto w1200 clearfix">
            <a href="javascript:void(0)" title="prev" class="ipl_btn a_prev"></a>
            <a href="javascript:void(0)" title="next" class="ipl_btn a_next"></a>
            <div class="ipl_main">
                <!-- 按钮文字可自定义 -->
                <ul class="iplm_ul list-unstyled clearfix">
                    <?php
                    $huodongDatas = getZhPics($zhanhuiPic);

                    foreach ($huodongDatas AS $huodongData) :
                        ?>
                    <?php if (strlen($huodongData->img)) :?>
                        <li>
                            <a href="<?= $huodongData->url; ?>">
                                <img src="<?= $huodongData->img; ?>" class="loading_img" data-url="<?= $huodongData->img; ?>" alt="">
                                <?php if (strlen($huodongData->expand)) :?>
                                    <p>
                                        <span class="ellipsis-3"><?= $huodongData->expand; ?></span>
                                        <?php if (strlen($huodongData->button)) :?>
                                            <span class="s2 btn mt-4"><?= $huodongData->button; ?></span>
                                        <?php endif; ?>
                                    </p>
                                <?php endif;?>
                            </a>
                        </li>
                    <?php endif;endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
<?php if ($goodCoupon) :?>
	<div class="com_title mt-3 mx-auto w1200" id="nav1">
		<em class="line line1"></em>
		<h3>品牌抵用券</h3>
		<em class="line line2"></em>
		<p class="p2">&nbsp;</p>
		<a href="<?=UrlService::build(['jie-hun/you-hui'])?>" class='float-right'>更多>></a>
	</div>
<?php endif; ?>
	<div class="explosin_coupon">
		<div class="mx-auto w1200 clearfix">
            <?php foreach ($goodCoupon as $k=>$v):?>
                <div class="explosin_coupon_con">
                    <div class="ecc_top">
                        <span class="red s1">￥</span>
                        <span class="red s2"><?= intval($v->money); ?></span>
                        <p><?=$v->describe?></p>
                        <p class="p">已领<?=$v->num?>张</p>
                    </div>
                    <div class="ecc_bottom">
                        <img src="<?=$v->user->avatar?>" alt="<?=$v->user->nickname?>">
                        <p class="text-truncate"><?= $v->goods_id>0 ? $v->goods->goods_name : "{$v->user->nickname}品牌抵用券";?></p>
                        <a href="javascript:void(0)" data-id='getCoupin' data-val="<?=$v->id?>" data-store="<?=$v->user_id?>" class="btn btn-danger">立即领取</a>
                    </div>
                </div>
            <?php endforeach;?>
		</div>

	</div>
<?php if ($teJia_good) :?>
	<div class="com_title mt-3 mx-auto w1200" id="nav2">
		<em class="line line1"></em>
		<h3>预存 享特价</h3>
		<em class="line line2"></em>
		<p class="p2">&nbsp;</p>
		<a href="<?=UrlService::build(["goods/ticket", 'type' => TicketApplyGoods::TYPE_ORDER])?>" class='float-right'>更多>></a>
	</div>
<?php endif; ?>
	<div class="index_rushtobuy">
		<div class="mx-auto w1200">
           <?php foreach ($teJia_good as $tk=>$tv):?>
               <div class="index_rushtobuy_con">
                   <a href="<?= UrlService::build(['goods/detail', 'id' => $good->id]); ?>" class="a_img" style="background-image: url(<?=$tv->goods_pic?>);"></a>
                   <div class="con">
                       <a href="<?= UrlService::build(['goods/detail', 'id' => $good->id]); ?>" class="p_name text-truncate"><?=$tv->goods_name?></a>
                       <p class="p_reserve red">订金:<span class="s1">￥</span><span class="s2"><?= $ticket->order_price; ?></span></p>
                       <p class="p_price">专享价:<span class="s1">￥<?=$tv->goods_price?></span><span class="s2">/<?=$tv->unit;?></span></p>
                       <p class="p_price"><span class="s2">市场价:</span><span class="s3">￥<?=$tv->goods_marketprice?>/<?=$tv->unit;?></span></p>
                       <p class="p_surplus red"><img src="/public/index/images/icon_time_red.png">仅余<?=$tv->good_amount?>件</p>
                       <a href="<?= UrlService::build(['goods/detail', 'id' => $good->id]); ?>" data-dlauto data-val="<?=$tv->id?>" data-type="<?=$formType?>" class="btn btn-danger">立即抢订</a>
                   </div>
               </div>
          <?php endforeach;?>
		</div>
	</div>
<?php if ($baoKuan_good) :?>
	<div class="com_title mt-4 mx-auto w1200" id="nav3">
		<em class="line line1"></em>
		<h3>爆款预约</h3>
		<em class="line line2"></em>
		<p class="p2">&nbsp;</p>
		<a href="<?=UrlService::build(["goods/ticket", 'type' => TicketApplyGoods::TYPE_COUPON])?>" class='float-right'>更多>></a>
	</div>
<?php endif; ?>
	<div class="index_reserve">
		<div class="mx-auto w1200">
			<!-- 一行三个，每行最后一个加con_last -->
            <?php foreach ($baoKuan_good as $bgk=>$bgv):?>
                <div class="index_reserve_con">
                    <a href="<?= UrlService::build(['goods/detail', 'id' => $bgv->id]); ?>" data-val="<?=$bgv->id?>" data-type="<?=$formType?>" class="a_img" style="background-image: url(<?=$bgv->goods_pic?>);"></a>
                    <div class="con">
                        <a href="<?= UrlService::build(['goods/detail', 'id' => $bgv->id]); ?>" data-val="<?=$bgv->id?>" data-type="<?=$formType?>" class="p_name text-truncate"><img src="<?=getUser($bgv->user_id)->avatar?>"><?=$bgv->goods_name?></a>
                        <div class="float-right">
                            <p class="p_surplus"><img src="/public/index/images/icon_time.png">仅余<?=$bgv->good_amount?>件</p>
                            <a href="javascript:void(0)" data-order-price="true" data-val="<?=$bgv->id?>" data-type="<?=$formType?>" class="btn btn-danger btn-sm">立即预约</a>
                        </div>
                        <p class="p_price red"><span class="s1">￥</span><span class="s2"><?=$bgv->goods_price?></span><span class="s3">/<?=$bgv->unit;?></span></p>

                    </div>
                </div>
           <?php endforeach;?>
		</div>
	</div>

	<div class="com_title mx-auto w1200" id="nav4">
		<em class="line line1"></em>
		<h3>合作品牌</h3>
		<em class="line line2"></em>
	</div>
	<div class="explosion_brand mx-auto w1200">		
		<ul class="nav nav-tabs explosion_brand_tab">
            <?php foreach ($goodClass1 as $gck=>$gcv):?>
                <li><a href="#brand_tab<?=$gck?>" data-toggle="tab"  class="<?=($gck==0)?'active':''?>"><?=$gcv->name?></a></li>
             <?php endforeach;?>
		</ul>
		<div class="tab-content explosion_brand_list">
             <?php foreach ($goodClass1 as $gck=>$gcv):?>
                 <ul class="tab-pane list-unstyled clearfix ebl_list <?=($gck==0)?'active':''?>" id="brand_tab<?=$gck?>">
                     <!-- 婚纱摄影 -->
                     <?php if (isset($joinStore_gc[$gcv->id])) :?>
                     <?php foreach ($joinStore_gc[$gcv->id] as $son_v):?>
                        <li><a  href="javascript:void(0)" data-id="getStore" data-val="<?=$son_v->user_id;?>" data-type="JIE_HUN"><img  src="<?=$son_v->avatar?>" width="152px" height="74px" alt="<?=$son_v->nickname;?>"></a></li>
                      <?php endforeach;endif;?>
                 </ul>
             <?php endforeach;?>
		</div>
	</div>
    <script type="text/javascript">

        sliderEvent( $('.index_ad_con .a_prev'), $('.index_ad_con .a_next') , $('.iplm_ul') , $('.index_ad_con') , 500 , true , 5);

        //轮播效果1：左右轮播，自动轮播
        //参数说明：left,right为左右按钮，list为列表的上级，stopObj为鼠标移入停止自动轮播的对象，times为动画时间,auto
        //Flag是否自动轮播,ind为一次轮播的张数
        function sliderEvent(left,right,list,stopObj,times,autoFlag,ind){
            stopObj.each(function(){
                var btnLeft = left,
                    btnRight = right,
                    teamsList=list;
                var teamsWidth = parseInt(teamsList.children().outerWidth())*ind;
                var teamsWidthS = parseInt(teamsList.children().outerWidth());
                var teamsLength = teamsList.children().length;
                var teamsTime = times;
                var sliderEventID;
                if(teamsLength>ind){
                    teamsList.width(teamsWidth*teamsLength/ind);
                    // console.log(teamsList.width());
                    // teamsList[0].innerHTML +=  teamsList[0].innerHTML;
                    btnLeft.bind('click',function(){
                        btnLeftFun()
                    });
                    btnRight.bind('click',function(){
                        btnRightFun();
                    });
                    if(autoFlag){
                        sliderEventID = setInterval(function(){
                            btnRightFun();
                        },2500);
                    }
                    stopObj.hover(function(){
                        clearInterval(sliderEventID);
                    },function(){
                        clearInterval(sliderEventID);
                        if(autoFlag){
                            sliderEventID = setInterval(function(){
                                btnRightFun();
                            },2500);
                        }
                    })
                }else{
                    left.hide();
                    right.hide();
                }
                function btnRightFun(){
                    teamsList.stop(true,true).animate({left:parseInt(teamsList.css("left"))-teamsWidthS},teamsTime,function(){
                        if(parseInt(teamsList.css("left"))<=-teamsWidth*teamsLength/ind+teamsWidthS*ind){
                            teamsList.css("left",0);
                        }
                    });
                }
                function btnLeftFun(){
                    if(parseInt(teamsList.css("left"))>=0){
                        teamsList.css("left",-teamsWidth*teamsLength/ind);
                    }
                    teamsList.stop(true,true).animate({left:parseInt(teamsList.css("left"))+teamsWidthS},teamsTime,function(){
                        if(parseInt(teamsList.css("left"))>=0){
                            teamsList.css("left",-teamsWidth*teamsLength/ind+teamsWidthS*ind);
                        }
                    });
                }
            })
        };
    </script>
    <script type="text/javascript">
        $(function () {
            $('.explosion_brand_tab li a').hover(function () {
                $(this).tab('show');
            });
        })
    </script>

    <?php include __DIR__ . '/../common/zhanhuiInfo.php'; ?>
    <?php include APP_PATH . '/views/common/footer.php';?>