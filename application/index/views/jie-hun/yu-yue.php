<?php

use DL\service\UrlService;
use common\models\Goods;
include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>

<div class="mx-auto w1200 clearfix pb-5">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="<?= UrlService::build(["index/index"]); ?>">首页</a>
        <a class="breadcrumb-item" href="<?= UrlService::build(["{$basePathData['path']}/index"]); ?>"><?= $basePathData['info']; ?></a>
        <span class="breadcrumb-item active">预约爆款</span>
    </nav>
    <nav class="navbar navbar-expand-sm com_tabgroup" style="overflow-x: scroll">
        <ul class="navbar-nav">
            <!-- 这里最多只能有8个，与客户确定好的，不需要换行 -->
            <li class="nav-item">
                <a href="#tabgroup1" class="nav-link">精品推荐</a>
            </li>
            <?php foreach ($allGoodsTab as $k=>$v):?>
                <?php if (strlen($v->gclass->name)) :?>
                <li class="nav-item">
                    <a href="#tabgroup<?=$k?>" class="nav-link"><?=$v->gclass->name?></a>
                </li>
                <?php endif; ?>
            <?php endforeach;?>

        </ul>
    </nav>
    <!-- 滚动监听 -->
    <script type="text/javascript">
        var header = document.querySelector('.navbar');
        var origOffsetY = header.offsetTop;

        function onScroll(e) {
            window.scrollY >= origOffsetY ? header.classList.add('position-sticky') : header.classList.remove('position-sticky');
        }

        document.addEventListener('scroll', onScroll);
    </script>


    <div class="tabgroup_title" id="tabgroup1">精品推荐</div>

    <div class="index_reserve clearfix">
        <!-- 一行三个，每行最后一个加con_last -->
        <?php foreach ($jingPin as $k=>$v):?>
            <div class="index_reserve_con">
                <a href="<?= UrlService::build(['goods/detail', 'id' => $v->id]); ?>" class="a_img"
                   style="background-image: url(<?=$v->goods_pic?>);"></a>
                <div class="con">
                    <a href="<?= UrlService::build(['goods/detail', 'id' => $v->id]); ?>"  class="p_name text-truncate"><img
                                src="<?=$v->user->avatar?>"><?=$v->goods_name?></a>
                    <div class="float-right">
                        <p class="p_surplus"><img src="/public/index/images/icon_time.png">仅余<?= isset($storageRelations[$v->id]) ? $storageRelations[$v->id] : 0; ?>?>件</p>
                        <a href="javascript:void(0)" data-order-price="true" data-val="<?= $v->id;?>" data-type="<?= $type; ?>" class="btn btn-danger btn-sm">立即预约</a>
                    </div>
                    <p class="p_price red"><span class="s1">￥</span><span class="s2"><?=$v->goods_price?></span><span
                                class="s3">/<?=$v->unit?></span></p>
                </div>
            </div>
        <?php endforeach;?>


    </div>


        <?php foreach ($allGoodsTab as $k=>$v):?>
            <div class="tabgroup_title" id="tabgroup<?=$k?>"><?=$v->gclass->name?></div>
                 <div class="index_reserve clearfix">
                     <?php
                     $goodArr=Goods::find()->andFilterWhere(['in','id',$order_goods_id_all])->andFilterWhere(['gc_id'=>$v->gclass->id])->limit(6)->all();
                     foreach ($goodArr as $k=>$v):?>
                    <div class="index_reserve_con">
                        <a href="<?= UrlService::build(['goods/detail', 'id' => $v->id]); ?>" class="a_img"
                           style="background-image: url(<?=$v->goods_pic?>);"></a>
                        <div class="con">
                            <a href="<?= UrlService::build(['goods/detail', 'id' => $v->id]); ?>" class="p_name text-truncate"><img
                                        src="<?=$v->user->avatarTm?>"><?=$v->goods_name?></a>
                            <div class="float-right">
                                <p class="p_surplus"><img src="/public/index/images/icon_time.png">仅余<?= isset($storageRelations[$v->id]) ? $storageRelations[$v->id] : 0; ?>件</p>
                                <a href="javascript:void(0)" data-order-price="true" data-val="<?= $v->id;?>" data-type="<?= $type; ?>"  class="btn btn-danger btn-sm">立即预约</a>
                            </div>
                            <p class="p_price red"><span class="s1">￥</span><span class="s2"><?=$v->goods_price?></span><span
                                        class="s3">/<?=$v->unit?></span></p>
                        </div>
                    </div>
                     <?php endforeach;?>
                </div>
        <?php endforeach;?>

</div>

<?php include APP_PATH . '/views/common/footer.php'; ?>
