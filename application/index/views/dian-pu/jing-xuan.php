<?php

use DL\service\UrlService;
use yii\widgets\LinkPager;
use common\models\StoreInfo;
include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>

<div class="mx-auto w1200 clearfix">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="index.html">首页</a>
        <a class="breadcrumb-item" href="explosion_jia.html">家装采购</a>
        <span class="breadcrumb-item active">精选店铺</span>
    </nav>
    <div class="com_tab clearfix">

        <a href="<?= \DL\service\UrlService::build(["{$basePath}/jing-xuan"]) ?>" class="active">精选店铺</a>
        <a href="<?= \DL\service\UrlService::build(["{$basePath}/can-zhan"]) ?>">参展品牌</a>

    </div>
    <div class="com_type clearfix mt-4">
        <div class="com_type_title float-left">分类：</div>
        <div class="com_type_con">
            <a href="<?=UrlService::build(["{$basePathData['path']}/jing-xuan",'gc_id'=>0])?>" class="<?=(0==$gc_id)?'active':''?>">全部</a>
            <?php foreach ($jiaZhuangType as $k=>$v):?>
                <a href="<?=UrlService::build(["{$basePathData['path']}/jing-xuan",'gc_id'=>$v->gc_id])?>" class="<?=($v->gc_id==$gc_id)?'active':''?>"><?=$v->goodClass->name?></a>
            <?php endforeach;?>
        </div>
    </div>
    <?php foreach ($model as $k=>$v):?>

        <div class="shoplist">
            <div class="shoplist_detail clearfix">
                <a href="<?=UrlService::build(['dian-pu/index','storeId'=>$v->user_id])?>" title="<?=$v->nickname?>" class="a_img float-left">
                    <img src="<?=$v->avatarTm?>" alt="<?=$v->nickname?>"></a>
                <div class="btns float-right">
                    <a href="javascript:void(0)" class="btn btn-danger">免费预约</a>
                    <p>预约专项，凭短信至展会购买！</p>
                </div>
                <div class="con">
                    <p class="p_title"><?=$v->nickname?></p>
                    <?php foreach ($v->tabs as $tk=>$tv):?>
                       <?php if ($tk<3):?>
                            <p><span class="s_tag  <?php $arr=['s_red','s_orange','s_blue']; echo ($tk<3)?$arr[$tk]:''?>"><span><?=$tv['tab']?></span></span><?=$tv['info'] ?></p>
                        <?php endif;?>
                    <?php endforeach;?>

                </div>
            </div>

            <ul class="shoplist_product list-unstyled clearfix">

                 <?php foreach ($v->order_goods as $og=>$ogv):?>

                     <?php  if ($og<4): ?>
                         <li>
                             <a href="javascript:void(0)" class="a_img"
                                style="background-image: url(<?=($ogv)?$ogv->goods_pic:''?>);" title="<?= ($ogv)?$ogv->goods_name:''?>"></a>
                             <a href="javascript:void(0)" class="a_title text-truncate"> <?=($ogv)?$ogv->goods_name:''?></a>
                         </li>
                     <?php endif;?>
                <?php endforeach;?>


            </ul>
        </div>
    <?php endforeach;?>

    <?php echo  $pageHtml?>

</div>

<?php include APP_PATH . '/views/common/footer.php'; ?>


