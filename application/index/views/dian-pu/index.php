<?php


use DL\service\UrlService;
use common\models\Goods;
use common\models\User;
use common\models\GoodsCoupon;
include APP_PATH . '/views/common/header.php';

?>

<?php include APP_PATH . '/views/common/guider.php'; ?>

<?php include __DIR__ . '/common/header.php'; ?>

<?php
  function getCouponPic($id){
      $coupon=GoodsCoupon::findOne($id);
      $coverPic='';
      if ($coupon->goods_id==-1){
           $user=User::findOne($coupon->user_id);
           $coverPic=$user->avatar;
      }else{
          $good=Goods::findOne($coupon->goods_id);
          $coverPic=$good->goods_pic;
      }
      return $coverPic;
  }

?>
<div class="shopdetail_menu navbar navbar-expand-sm">
    <!-- 在首页滑动，有状态，顶悬浮。如果点击，就切换到对应的页面 -->
    <ul class="nav navbar-nav mx-auto w1200 clearfix">
        <li class="nav-item"><a href="#shopdetail1" class="nav-link active">店铺首页</a></li>
        <li class="nav-item"><a href="#shopdetail2" class="nav-link">全部商品</a></li>
        <li class="nav-item"><a href="#shopdetail3" class="nav-link">品牌介绍</a></li>
        <li class="nav-item"><a href="#shopdetail4" class="nav-link">到店体验</a></li>
        <li class="nav-item"><a href="#shopdetail5" class="nav-link">用户点评</a></li>
    </ul>
</div>

<!-- 滚动监听 -->
<script type="text/javascript">
    var header = document.querySelector('.navbar');
    var origOffsetY = header.offsetTop;
    function onScroll(e){
        window.scrollY >= origOffsetY ? header.classList.add('position-sticky'):header.classList.remove('position-sticky');
    }
    document.addEventListener('scroll', onScroll);
</script>

<div class="mx-auto w1200">
    <?php if ($storeInfo->coupons) :?>
    <div class="shopdetail_title mt-5">
        <p>店铺抵用券</p>
        <span></span>
    </div>
    <div class="shopdetail_warn"><img src="/public/index/images/icon_warn0.png">说明：为保障您能够享受最大幅度优惠，领取返现券后，请至商户店铺挑选您所需产品，自行洽谈产品价格至最低价，付款时再出示返现券。</div>
    <div class="coupon_list clearfix  mt-3 pt-3">

        <?php foreach ($storeInfo->coupons AS $coupon_k=>$coupon) :?>
            <div class="coupon_con clearfix <?=(($coupon_k+1)%3==0)?'con_last':''?>">
                <img src="<?=getCouponPic($coupon->id)?>" alt="" class="img">
                <div class="con">
                    <p class="p1 red"><span>￥</span><?= intval($coupon->money); ?></p>
                    <p class="p2"><?= $coupon->describe; ?></p>

                    <p class="p3">已领<span class="orange"><?=$coupon->num?></span>张</p>
                </div>
                <?php if (in_array($coupon->id, $hasStoreCoupon)):?>
                    <a href="javascript:void(0)" class="con_btn  receiveCoupon disabled" data-toggle="modal" data-id="<?= $coupon->id; ?>">已领取</a>
                <?php else:?>
                    <a href="javascript:void(0)" class="con_btn  receiveCoupon" data-toggle="modal" data-id="<?= $coupon->id; ?>">立即领取</a>
                <?php endif;?>
            </div>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>

    <?php if ($isJoinZh) :?>
    <!--  参加展会  -->
    <?php
        $orderGoodsAmounts = [];
        arrayGroupsAction($storeInfo->order_goods, function ($c) use (&$orderGoodsAmounts){
            $orderGoodsAmounts = array_merge($orderGoodsAmounts, $c);
        });

        if (count($orderGoodsAmounts)) :?>
    <div class="shopdetail_title mt-4 mb-4">
        <p>预存 享特价</p>
        <span></span>
    </div>
    <div class="index_rushtobuy">
        <?php foreach ($storeInfo->order_goods AS $type => $oGoods) :?>
            <?php if ($oGoods) : foreach ($oGoods AS $oGood) :?>
            <div class="index_rushtobuy_con">
                <a  href="<?= createUrlByCondition($zhType, ['goods/detail', 'id' => $oGood->id], ($cityHasZh and !$storeInfo->join_zh)); ?>" target="_blank" class="a_img" style="background-image: url('<?= $oGood->goods_pic;?>');"></a>
                <div class="con">
                    <a href="<?= createUrlByCondition($zhType, ['goods/detail', 'id' => $oGood->id], ($cityHasZh and !$storeInfo->join_zh)); ?>" class="p_name text-truncate"><?= $oGood->goods_name;?></a>
                    <p class="p_reserve red">订金:<span class="s1">￥</span><span class="s2"><?= $oGood->ticket_price;?></span>
</p>
                    <p class="p_price">专享价:<span class="s1"><?= $oGood->goods_price;?></span><span class="s2">元/<?=$oGood->unit?></span></p>
                    <p class="p_price"><span class="s2">市场价:</span><span class="s3"><?= $oGood->goods_marketprice;?>元/<?=$oGood->unit?></span></p>
                    <p class="p_surplus red"><img src="/public/index/images/icon_time_red.png">仅余<?= $oGood->goods_storage;?>件</p>
                    <a href="javascript:void(0)" data-order="true" data-val="<?= $oGood->id;?>" data-type="<?= $type; ?>" class="btn btn-danger">立即抢订</a>
                </div>
            </div>
            <?php endforeach; endif; ?>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>

    <?php
        $advanceGoodsAmounts = [];
        arrayGroupsAction($storeInfo->advance_goods, function ($c) use (&$advanceGoodsAmounts){
            $advanceGoodsAmounts = array_merge($advanceGoodsAmounts, $c);
        });

            if (count($advanceGoodsAmounts)) : ?>
            <div class="shopdetail_title mt-4 mb-4">
                <p>爆款预约</p>
                <span></span>
            </div>
    <?php foreach ($storeInfo->advance_goods AS $type => $advanceGoods) : if ($advanceGoods) :?>
            <?php foreach ($advanceGoods AS $advanceGood) :?>
                    <div class="shopdetail_appointment bg_white clearfix">
                        <a href="<?= createUrlByCondition($zhType, ['goods/detail', 'id' => $advanceGood->id], ($cityHasZh and !$storeInfo->join_zh)); ?>" target="_blank" class="img" style="background-image: url(<?= $advanceGood->goods_pic; ?>);"></a>
                        <div class="con">
                            <p class="p_title text-truncate"><?= $advanceGood->goods_name; ?></p>
                            <div class="p_spec ellipsis-2"><?= $advanceGood->goods_jingle; ?></div>
                            <p class="p_price"><span class="orange">预约价：￥</span><span class="s orange"> <?= $advanceGood->goods_price; ?></span></p>
                            <p class="p_price1">市场价：<span class="s"><span><?=$advanceGood->goods_marketprice?></span>元/<?=$advanceGood->unit?></span></p>
                            <p class="p_btn"> <a href="javascript:void(0)"  data-order-price="true" data-val="<?= $advanceGood->id;?>" data-type="<?= $type; ?>" class="btn btn-danger">立即预约</a><span class="s">仅剩<span class="orange"><?= $advanceGood->goods_storage; ?></span>件</span></p>
                            <p class="p_more">线上预约锁定价格，凭短信至展会现场下单</p>
                        </div>
                    </div>
            <?php endforeach; ?>
    <?php endif;endforeach; ?>
    <?php endif; ?>
    <!--  参加展会  -->
    <?php endif; ?>

    <div class="shopdetail_title mt-5 mb-4" id="shopdetail2">
        <a href="<?= UrlService::build(['dian-pu/goods', 'storeId' => $storeInfo->user_id]); ?>">更多>></a>
        <p>全部商品</p>
        <span></span>
    </div>
    <div class="shopdetail_product mt-3 clearfix">
        <?php if ($goods) :?>
            <?php foreach ($goods AS $good) :?>
                <div href="<?= createUrlByCondition(['goods/detail', 'id' => $good->id], $zhType, in_array($good->id, $joinTicketGoodIds)); ?>" class="shoppro_con">
                    <div class="img" style="background-image: url('<?= $good->goods_pic; ?>');" data-id="goDetail" data-href="<?= UrlService::build(['goods/detail', 'id' => $good->id]) ?>" ></div>
                    <p class="p_title ellipsis-2" data-id="goDetail" data-href="<?= UrlService::build(['goods/detail', 'id' => $good->id]) ?>"><?= $good->goods_name; ?></p>
                    <p class="p_price" data-id="goDetail" data-href="<?= UrlService::build(['goods/detail', 'id' => $good->id]) ?>">
                        <span class="red">￥<?= $good->goods_price; ?><span class="s2">/<?= $good->unit; ?></span></span>
                        <span class="s1">￥<?= $good->goods_marketprice; ?><span class="s2">/<?= $good->unit; ?></span></span>
                    </p>
                    <a href="<?= createUrlByCondition(['goods/detail', 'id' => $good->id], $zhType, in_array($good->id, $joinTicketGoodIds)); ?>" class="btn btn-danger">预约抢购</a>
                </div>
            <?php endforeach; ?>
        <?php else :?>
            <div style="text-align: center">暂无商品</div>
        <?php endif;?>
    </div>
    <div class="shopdetail_title mt-4 mb-4" id="shopdetail3">
        <p>品牌介绍</p>
        <span></span>
    </div>
    <div class="shopdetail_brand">
        <?= $storeInfo->describe; ?>
    </div>
    <?php if ($shops) :?>
    <div class="shopdetail_title mt-5 mb-4" id="shopdetail4">
        <a href="<?= UrlService::build(['dian-pu/infos', 'storeId' =>$storeInfo->user_id, 'type' => 'shops']); ?>">更多>></a>
        <p>到店体验</p>
        <span></span>
    </div>

    <?php include __DIR__ . '/common/map.php'; ?>
    <?php endif;?>

    <?php if ($shopComment) :?>
    <div class="shopdetail_title mt-5 mb-4" id="shopdetail5">
        <a href="<?= UrlService::build(['dian-pu/infos', 'storeId' =>$storeInfo->user_id, 'type' => 'judge']); ?>">更多>></a>
        <p>用户点评</p>
        <span></span>
    </div>
    <div class="shopdetail_assess mt-4">
        <div class="shopdetail_assess_list">
            <?php foreach ($shopComment as $sck=>$scv):?>
                <div class="shopass_con">
                    <div class="shopass_con_user float-left"><img src="<?=$scv->member->avatarTm?>"><p><?=substr_cut($scv->member->mobile)?></p></div>
                    <div class="shopass_content">
                        <p class="p1">
                            <span class="s">商品质量：<span class="red"><?=$scv->spzl?></span>星</span>
                            <span class="s">商户服务：<span class="red"><?=$scv->shfw?></span>星</span>
                        </p>
                        <p class="p2"><?= $scv->content; ?></p>
                        <div class="imgs">
                            <?php foreach ($scv->ImgArr as $imgk=>$imgv):?>
                                <img src="<?=$imgv?>" alt="">
                            <?php endforeach;?>


                        </div>
                        <p class="p3 text-right"><img src="/public/index/images/icon_time2.png"><?= $scv->create_time; ?></p>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
    <?php endif; ?>
</div>
<script>

    $(".receiveCoupon").on('click',function () {
        var id=$(this).attr('data-id');
        var url = "<?=UrlService::build(['dian-pu/receive-coupon'])?>";
        $.get(url, {id: id,storeId:'<?=$storeInfo->user_id?>'}, function (r) {
            if (r.code == 1) {
                alert(r.msg, function () {
                    window.location.reload();
                });
                return false;
            } else {
                alert(r.msg);
            }
        }, 'json');

    });


    $('.receiveCoupon').each(function() {
        var val = $(this).attr('data-id'),
            text = $(this).text();
        if (hasCouponIds.indexOf(val) > -1) {
            $(this).addClass('disabled').text('已' + text.slice(-2));
        }
    });


</script>
<?php include APP_PATH . '/views/common/footer.php';?>

