<?php
use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';

?>

<?php include APP_PATH . '/views/common/guider.php'; ?>
	
    <?php include __DIR__ . '/common/header.php'; ?>

    <?php include __DIR__ . '/common/guider.php'; ?>
	<div class="mx-auto w1200">
		<div class="shopdetail_store">
            <?php foreach ($shops AS $in => $shop) :?>
			<div class="shopdetail_store_con clearfix">
				<div class="shopsc_left">
                    <div class="carousel slide shopsc_slider" data-ride="carousel" id="slider_img<?= $in; ?>">
                      <!-- 指示符 -->
                      <ul class="carousel-indicators">
                          <?php foreach ($shop->picitures AS $index => $piciture) :?>
                        <li data-target="#slider_img<?= $in; ?>" data-slide-to="<?= $index; ?>" class="<?= $index ? '' : 'active'; ?>"></li>
                          <?php endforeach; ?>
                      </ul>
                      <!-- 轮播图片 -->
                      <div class="carousel-inner">
                          <?php foreach ($shop->picitures AS $index => $piciture) :?>
                        <a href="javascript:void(0)" class="carousel-item <?= $index ? '' : 'active'; ?>" style="background-image: url('<?= $piciture; ?>');"></a>
                          <?php endforeach; ?>
                      </div>
                    </div>

				</div>
				<div class="shopsc_con">
					<p class="p_title"><?= $shop->store_name; ?></p>
					<div class="p_gift"><?= $shop->notice;?></div>
					<p>营业时间：<?= $shop->work_times; ?>（<?= implode(', ', $shop->workDaysInfo); ?>）</p>
					<p>门店地址：<?= $shop->address; ?></p>
				</div>
			</div>
            <?php endforeach; ?>
		</div>

        <?php include __DIR__ . '/common/map.php';?>

	</div>


<?php include APP_PATH . '/views/common/footer.php';?>
