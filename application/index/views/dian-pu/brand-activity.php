<?php
use DL\service\UrlService;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>



<div class="mx-auto w1200 clearfix">
		<nav class="breadcrumb com_crumbs">
			<span>当前位置：</span>
			<a class="breadcrumb-item" href="index.html">首页</a>
			<a class="breadcrumb-item" href="explosion_jia.html">家装采购</a>
			<span class="breadcrumb-item active">品牌活动</span>
		</nav>
		<div class="container_left float-left">
			<!-- 后台维护 图/文/链接，如果有外联，则点击打开外链；如果没有链接，则显示后台维护的 -->
			<?php foreach ($newsAll as $k=>$v):?>
                <div class="brandactivity_list">
                    <a href="<?=UrlService::build(['dian-pu/brand-activity-detail','id'=>$v->id])?>" class="a_img"><img src="<?=$v->themeImg?>?>"></a>
                    <div class="con">
                        <p class="p1"><?=$v->title?></p>
                        <p class="p2"><span class="float-right"><img src="/public/index/images/icon_read.png"><?=$v->clickRate?></span><img src="/public/index/images/icon_time1.png"><?=$v->createTime?></p>
                    </div>
                </div>
            <?php endforeach;?>

          <?php echo  $pageHtml?>

		</div>
		<div class="container_right float-right">
			<form action="" class="explosin_form mt-0">
				<h3>免费索票</h3>
				<p class="p1">已有<span class="orange">40133</span>人索票</p>
				<input type="text" class="input" name="" placeholder="请输入收件人姓名" required="">
				<input type="text" class="input" name="" placeholder="请输入手机号码" required="">
				<input type="text" class="input" name="" placeholder="请输入收货地址" required="">
				<select class="select float-right">
					<option>选择了解渠道</option>
					<option>渠道1</option>
					<option>渠道2</option>
					<option>渠道3</option>
				</select>
				<p class="p2">您是怎么了解到孕婴展的？</p>
				<a href="javascript:void(0)" class="btn">免费索票</a>
				<p class="p3">请详细填写以上信息，我们将免费为您快递一份价值20元的门票及展会资料</p>
			</form>
			<div class="if_list_right mt-3">
				<p class="text-left">品牌推荐</p>
				<ul class="list-unstyled clearfix brand_list">
<li><a href="javascript:void(0)"><img src="/public/index/images/_temp/img_brand01.jpg" data-url="/public/index/images/_temp/img_brand01.jpg" class="loading_img" alt="欣百利"></a></li>
<li><a href="javascript:void(0)"><img src="/public/index/images/_temp/img_brand02.jpg" data-url="/public/index/images/_temp/img_brand02.jpg" class="loading_img" alt="欣百利"></a></li>
<li><a href="javascript:void(0)"><img src="/public/index/images/_temp/img_brand03.jpg" data-url="/public/index/images/_temp/img_brand03.jpg" class="loading_img" alt="欣百利"></a></li>
<li><a href="javascript:void(0)"><img src="/public/index/images/_temp/img_brand04.jpg" data-url="/public/index/images/_temp/img_brand04.jpg" class="loading_img" alt="欣百利"></a></li>
<li><a href="javascript:void(0)"><img src="/public/index/images/_temp/img_brand05.jpg" data-url="/public/index/images/_temp/img_brand05.jpg" class="loading_img" alt="欣百利"></a></li>
<li><a href="javascript:void(0)"><img src="/public/index/images/_temp/img_brand06.jpg" data-url="/public/index/images/_temp/img_brand06.jpg" class="loading_img" alt="欣百利"></a></li>
<li><a href="javascript:void(0)"><img src="/public/index/images/_temp/img_brand07.jpg" data-url="/public/index/images/_temp/img_brand07.jpg" class="loading_img" alt="欣百利"></a></li>
<li><a href="javascript:void(0)"><img src="/public/index/images/_temp/img_brand08.jpg" data-url="/public/index/images/_temp/img_brand08.jpg" class="loading_img" alt="欣百利"></a></li>
<li><a href="javascript:void(0)"><img src="/public/index/images/_temp/img_brand09.jpg" data-url="/public/index/images/_temp/img_brand09.jpg" class="loading_img" alt="欣百利"></a></li>
<li><a href="javascript:void(0)"><img src="/public/index/images/_temp/img_brand10.jpg" data-url="/public/index/images/_temp/img_brand10.jpg" class="loading_img" alt="欣百利"></a></li>
				</ul>
			</div>


		</div>

	</div>

<?php include APP_PATH . '/views/common/footer.php';?>