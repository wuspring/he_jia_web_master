<?php
use DL\service\UrlService;

?>

<div class="shopdetail_experience mt-5">
    <div class="shopexp_shoplist">
        <?php if ($shops) :?>
        <p>体验店（<?= count($shops); ?>家）</p>
        <?php foreach ($shops AS $shop) :?>
        <a href="javascript:void(0)" class="text-truncate storeMap" data-lat="<?= $shop->lat; ?>" data-lng="<?= $shop->lng; ?>">
            <?= $shop->store_name; ?>
        </a>
        <?php endforeach; ?>
        <?php else: ?>
            <p>线下店铺待完善</p>
        <?php endif; ?>
    </div>

    <div id="map" style="height: 600px;"></div>
</div>

<script type="text/javascript" src="https://map.qq.com/api/js?v=2.exp&key=<?= MAP_KEY; ?>"></script>
<script>
    // map setting
    var qqMap = new qq.maps.Geocoder();

    var dom = document.getElementById('map'), map, markerInfo;
    var setMap = function (postion) {
        var mapDom = new qq.maps.Map(dom, {
            center: postion,//将经纬度加到center属性上
            zoom: 12,//缩放
            draggable: true,//是否可拖拽
            scrollwheel: true,//是否可滚动缩放
            disableDoubleClickZoom: false
        });

        return mapDom;
    };

    var marker = function (position) {
        $('input[name="Devices[lng]"]').val(position.lng.toFixed(8));
        $('input[name="Devices[lat]"]').val(position.lat.toFixed(8));
        // map.center = position;
        return new qq.maps.Marker({
            position: position,//标记的经纬度
            animation: qq.maps.MarkerAnimation.BOUNCE,
            map: map//标记的地图
        })
    };

    $("a.storeMap").bind('click', function() {
        var position = new qq.maps.LatLng(
            $(this).attr('data-lat'),
            $(this).attr('data-lng')
        );

        $(this).addClass('active').siblings().removeClass('active');

        map = setMap(position);
        markerInfo = marker(position);
    });

    var stores = $("a.storeMap");
    if (stores.length) {
        stores.eq(0).click();
    }
</script>