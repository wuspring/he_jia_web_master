<?php
use DL\service\UrlService;

?>
<div class="shopdetail_menu">
    <ul class="nav mx-auto w1200 clearfix">
        <li class="nav-item"><a href="<?= UrlService::build(['dian-pu/index', 'storeId' =>$storeInfo->user_id]); ?>" class="nav-link">店铺首页</a></li>
        <li class="nav-item"><a href="<?= UrlService::build(['dian-pu/goods', 'storeId' => $storeInfo->user_id]); ?>" class="nav-link  <?= $type=='goods' ? 'active' : ''; ?>">全部商品</a></li>
        <li class="nav-item"><a href="<?= UrlService::build(['dian-pu/infos', 'storeId' =>$storeInfo->user_id, 'type' => 'description']); ?>" class="nav-link <?= $type=='description' ? 'active' : ''; ?>">品牌介绍</a></li>
        <li class="nav-item"><a href="<?= UrlService::build(['dian-pu/infos', 'storeId' =>$storeInfo->user_id, 'type' => 'shops']); ?>" class="nav-link <?= $type=='shops' ? 'active' : ''; ?>">到店体验</a></li>
        <li class="nav-item"><a href="<?= UrlService::build(['dian-pu/infos', 'storeId' =>$storeInfo->user_id, 'type' => 'judge']); ?>" class="nav-link <?= $type=='judge' ? 'active' : ''; ?>">用户点评</a></li>
    </ul>
</div>