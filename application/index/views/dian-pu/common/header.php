<?php
use DL\service\UrlService;
use \DL\Project\CityExpand;
isset($isJoinZh) or $isJoinZh = false;

$baseStoreTagsDetail = [];
$cityInfo = CityExpand::init()->get($storeInfo->provinces_id, CityExpand::TYPE_CITY_EXPAND);
if (isset($cityInfo->tag)) {
    $baseStoreTagsDetail = call_user_func(function($str) {
        $tabDatas = json_decode($str, true);
        return $tabDatas ? arrayGroupsAction($tabDatas, function ($str) {
            list($tab, $info) = explode('@', $str);

            return ['tab' => $tab, 'info' => $info];
        }) : [];
    }, $cityInfo->tag);
}

?>
<div class="mx-auto w1200 clearfix" id="shopdetail1">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="<?= UrlService::build('index/index'); ?>">首页</a>
        <a class="breadcrumb-item" href="<?= UrlService::build('jia-zhuang/index'); ?>">家装采购</a>
        <span class="breadcrumb-item active"><?= $storeInfo->nickname; ?></span>
    </nav>
</div>

<div class="shopdetail_top bg_white">
    <div class="mx-auto w1200 clearfix shoplist_detail">
        <a href="<?=UrlService::build(['dian-pu/index', 'storeId' => $storeInfo->user_id]); ?>" title="<?= $storeInfo->nickname; ?>" class="a_img float-left">
            <img src="<?= $storeInfo->avatar; ?>" alt="<?= $storeInfo->nickname; ?>"></a>

        <?php if ($isJoinZh) : ?>
        <div class="btns float-right mt-3">
            <a href="<?php
                if (isset($zhType)) {
                    $pathInfo = UrlService::build($zhType);
                } else {
                    $pathInfo = 'javascript:void(0)';
                }

                echo $pathInfo;
            ?>" class="btn btn-danger">免费预约</a>
            <p>添加关注，可在会员中心收藏中查看</p>
             <a class="click_collect" href="javascript:void(0)" data-collect=<?=$isCollection?>><?=($isCollection==1)?'取消收藏':'点击收藏'?></a>
        </div>
        <?php endif; ?>


        <div class="con">
            <p class="p_title"><?= $storeInfo->nickname; ?></p>
            <!-- 黄色星为icon_assess，灰色星为icon_assess1 -->
            <p class="p_assess">
                <?php

                for($i=0; $i<5; $i++) {
                    echo '<img src="/public/index/images/icon_assess'. ($i < $storeInfo->score ? '' : '1') . '.png">';
                }
                ?>
                <span class="orange"><?= round($storeInfo->score,1); ?>分</span>| <?= $storeInfo->judge_amount; ?>条</p>
            <p class="p_address"><img src="/public/index/images/icon_address.png">共<span class="orange"><?= $storeInfo->store_amount; ?></span>家体验店</p>
            <?php
            $dTags = $storeInfo->tabs ? :$baseStoreTagsDetail;
            foreach ($dTags AS $index => $tab) :?>
                <?php if ($index < 3) :?>
                    <p><span class="s_tag  <?php $arr=['s_red','s_orange','s_blue']; echo $arr[$index]?>"><span><?=$tab['tab']?></span></span><?=$tab['info'] ?></p>
                <?php else: break; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>

    </div>
</div>

<script>
    //点击收藏 点击取消
    $(".click_collect").on('click',function () {
        var shop_id=<?=$storeInfo->user_id?>;
        var is_collect=$(this).attr('data-collect');
        var url="<?=UrlService::build(['personal/click-collection-store'])?>";

        if (Object.keys(userInfo).length < 1) {
            alert ("您还没有登录哦");
            return false;
        }

        if (is_collect==1){   //取消收藏
            $.get(url,{is_collect:1,shop_id:shop_id},function (res) {
                if (res.code==1){

                    $('.click_collect').attr('data-collect','0');
                    $('.click_collect').text('点击收藏');
                }
                alert(res.msg);
            },'json');

        }else {
            $.get(url,{is_collect:0,shop_id:shop_id},function (res) {
                if (res.code==1){

                    $(".click_collect").attr('data-collect','1');
                    $('.click_collect').text('取消收藏');
                }
                alert(res.msg);
            },'json');
        }
    });
</script>