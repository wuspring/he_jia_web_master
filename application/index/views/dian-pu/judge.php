<?php
use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';

?>

<?php include APP_PATH . '/views/common/guider.php'; ?>

<?php include __DIR__ . '/common/header.php'; ?>

<?php include __DIR__ . '/common/guider.php'; ?>
<div class="mx-auto w1200">
    <div class="shopdetail_assess mt-4">
        <div class="shopdetail_assess_list">
            <?php foreach ($comments as $sck=>$scv):?>
                <div class="shopass_con">
                    <div class="shopass_con_user float-left"><img src="<?=$scv->member->avatarTm?>"><p><?=substr_cut($scv->member->mobile)?></p></div>
                    <div class="shopass_content">
                        <p class="p1">
                            <span class="s">商品质量：<span class="red"><?=$scv->spzl?></span>星</span>
                            <span class="s">商户服务：<span class="red"><?=$scv->shfw?></span>星</span>
                        </p>
                        <p class="p2"><?= $scv->content; ?></p>
                        <div class="imgs">
                            <?php foreach ($scv->ImgArr as $imgk=>$imgv):?>
                                <img src="<?=$imgv?>" alt="">
                            <?php endforeach;?>


                        </div>
                        <p class="p3 text-right"><img src="/public/index/images/icon_time2.png"><?= $scv->create_time; ?></p>
                    </div>
                </div>
            <?php endforeach;?>
        </div>

    </div>
    <?= $page->show(); ?>
</div>
<?php include APP_PATH . '/views/common/footer.php';?>