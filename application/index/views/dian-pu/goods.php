<?php
use DL\service\UrlService;

include APP_PATH . '/views/common/header.php';

?>

<?php include APP_PATH . '/views/common/guider.php'; ?>

<?php include __DIR__ . '/common/header.php'; ?>

<?php include __DIR__ . '/common/guider.php'; ?>

	<div class="mx-auto w1200">
		<div class="productlist clearfix mt-5">
            <?php if ($goods) :?>
            <?php foreach ($goods AS $good) :?>
			<a href="<?= createUrlByCondition(['goods/detail', 'id' => $good->id], $zhType, in_array($good->id, $joinTicketGoodIds)); ?>" class="productlist_con">
				<div class="img" style="background-image: url('<?= $good->goods_pic; ?>');"></div>
				<p class="p_title ellipsis-2"><?= $good->goods_name; ?></p>
				<p class="p_shop"><img src="/public/index/images/icon_shop.png"><?= $good->store->nickname; ?></p>
                <p class="p_price1 mt-2">价格:￥<?= $good->goods_price; ?>/<?= $good->unit; ?></p>

            </a>
            <?php endforeach; ?>
            <?php else :?>
                <div style="text-align: center">暂无商品</div>
            <?php endif;?>

		</div>
        <?= $pageHtml; ?>

	</div>

<?php include APP_PATH . '/views/common/footer.php';?>