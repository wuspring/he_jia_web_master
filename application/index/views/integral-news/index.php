<?php
use DL\service\UrlService;
use common\models\GoodsClass;


include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/integral-header.php';
?>

	<div class="mx-auto w1200 clearfix pb-5">
		<nav class="breadcrumb com_crumbs">
			<span>当前位置：</span>
			<a class="breadcrumb-item" href="<?= UrlService::build('integral/index'); ?>">首页</a>
			<span class="breadcrumb-item active">积分资讯</span>
		</nav>
		<div class="fitup_containter clearfix">
			<div class="fitup_list float-left">
				<div class="fitup_list_con5 con">
					<h3><?= $news->title; ?></h3>

					<div class="fitup_content5">
                        <?= $news->content; ?>
					</div>
				</div>
				<div class="fitup_list_con6 con mt-3">
					<div class="fitup_title">					
						<span class="s_title">相关资讯</span>
					</div>
					<ul class="fitup_list6 list-unstyled clearfix">
                        <?php if ($relationNews) : foreach ($relationNews AS $relationNew) :?>
						<li><a href="<?= UrlService::build(['integral-news/index', 'id' => $relationNew->id]); ?>" class="text-truncate"><?= $relationNew->title; ?></a></li>
                        <?php endforeach; endif; ?>
					</ul>

				</div>


			</div>
			<div class="news_sidebar float-right">
				<div class="news_sidebar_hot con pb-2">
					<div class="fitup_title">					
						<span class="s_title">热门资讯</span>
					</div>
					<div class="nsh_list  list-unstyled clearfix">
                        <?php if ($hotNews) : foreach ($hotNews AS $hotNew) :?>
                            <li><a href="<?= UrlService::build(['integral-news/index', 'id' => $hotNew->id]); ?>" class="text-truncate"><?= $hotNew->title; ?></a></li>
                        <?php endforeach; endif; ?>
					</div>
				</div>
			</div>
		</div>
		


	</div>

<?php include APP_PATH . '/views/common/footer.php';?>