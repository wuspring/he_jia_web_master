<?php

use DL\service\UrlService;
use common\models\Order;
include APP_PATH . '/views/common/header.php'; ?>

<script>
    $('body').removeClass('bg_glay');
</script>
<div class="mx-auto w1200 clearfix casesure">
    <p class="case_title"><span class="green">订单确认</span></p>
    <p class="casesure_title">商品信息</p>
    <?php foreach ($order->orderGoodsList AS $good) :?>
    <div class="casesure_product">
        <a href="<?= UrlService::build(['goods/detail', 'id' => $good['goods_id']]); ?>" title="" class="a_img">
            <img src="<?= $good['goods_pic']; ?>" alt="<?= $good['goods_name']; ?>"></a>
        <p class="p_title"><?= $good['goods_name']; ?></p>
        <p class="p_price">专享价：<?= $good['goods_price']; ?>元/套<span><?= $good['sell_price']; ?>元/套</span></p>
        <p class="p_earnest">订金：￥<?= $good['goods_pay_price']; ?></p>
    </div>
    <?php endforeach; ?>

    <p class="casesure_title">请选择支付方式</p>
    <div class="casesure_pay">
        <a href="javascript:void(0)" style="display: none" data-id="setPay" class="<?= $order->pay_method == Order::PAY_MECHOD_ACCOUNT?'active':''; ?>" data-val="<?=Order::PAY_MECHOD_ACCOUNT?>"><img src="/public/index/images/icon_pay_ye.png">余额支付</a>

        <a href="javascript:void(0)" data-id="setPay" class="<?= $order->pay_method == Order::PAY_MECHOD_WECHAT?'active':''; ?>" data-val="<?=Order::PAY_MECHOD_WECHAT?>"><img src="/public/index/images/icon_pay_wx.png">微信支付</a>
        <a href="javascript:void(0)" data-id="setPay" class="<?= $order->pay_method == Order::PAY_MECHOD_ALIPAY?'active':''; ?>" data-val="<?=Order::PAY_MECHOD_ALIPAY?>"><img src="/public/index/images/icon_pay_zfb.png">支付宝</a>
    </div>
    <div class="casesure_btns">
        <p>提示：下单后进入“<span class="green">我们和家</span>”查看订单，到店消费时出示！</p>
        <?php if (strlen($order->pay_method)) :?>
        <a href="<?= UrlService::build(['payment/pay', 'number' => $order->orderid]); ?>"  class="btn btn-danger float-right clickSubmitOrder" >提交订单</a>
        <?php else :?>
        <a href="javascript:void(0)" data-notice class="btn btn-danger float-right clickSubmitOrder" >提交订单</a>
        <?php endif; ?>
        <p class="p">实付款：<span class="orange">￥<?= $order->order_amount; ?></span></p>
    </div>


</div>


<?php include APP_PATH . '/views/common/footer.php'; ?>

<script type="text/javascript">
    $('[data-id="setPay"]').click(function () {
        var that = $(this),
            type = that.attr('data-id'),
            val = that.attr('data-val'),
            data = {
                number : '<?= $order->orderid; ?>'
            };
        data[type] = val;

        $.ajax({
            url: '<?= UrlService::build('order/update'); ?>',
            type : 'post',
            async : false,
            data : data,
            dataType:'json',
            success:function(data) {
                if (data.status) {
                    window.location.reload();
                    return false;
                }
                alert(data.msg);
            }
        });
    });

    $('[data-notice]').click(function () {
        alert('请选择支付方式');
    })
</script>
