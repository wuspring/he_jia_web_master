
<?php
use DL\service\UrlService;
use common\models\GoodsClass;

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/integral-header.php';


$cartAmount = count($carts);
?>
<script>
    var CART_ROUTE_DELETE = '<?= UrlService::build("integral-cart/delete"); ?>';
</script>
<?php include __DIR__ . '/../common/cart.php'; ?>
<div class="mx-auto w1200 clearfix mycart">
		<p class="case_title"><span class="green">全部商品<em>(<?= $amount; ?>)</em></span></p>
        <?php if ($carts) :?>
            <div class="car_content mt-4 pt-1">
                <!-- 下面购物车结构，用了跟单商户里面一样的，因为是积分 car.js 做了处理 -->
                <div class="cartMain">
                    <div class="cartMain_hd">
                        <ul class="order_lists cartTop list-unstyled clearfix">
                            <li class="list_chk">
                                <!--所有商品全选-->

                                <label for="all"><input type="checkbox" id="all" class="whole_check" data-id="checkAll" <?= $cartAmount==$amount ? 'checked' : ''; ?>>全选</label>
                            </li>
                            <li class="list_con">商品</li>
                            <li class="list_price">单价</li>
                            <li class="list_amount">数量</li>
                            <li class="list_sum">小计</li>
                            <li class="list_op">操作</li>
                        </ul>
                    </div>
                    <div class="cartBox">
                        <div class="order_content">
                            <?php foreach ($carts AS $cart) :?>
                            <ul class="order_lists list-unstyled clearfix">
                                <li class="list_chk">
                                    <input type="checkbox" id="checkbox_2"  data-id="checks" data-val="<?=$cart->id?>"
                                           <?= $cart->ispay ? 'checked' : ''; ?> class="son_check">
                                    <label for="checkbox_2"></label>
                                </li>
                                <li class="list_con">
                                    <div class="list_img"><a href="<?= UrlService::build(['intergral-goods/detail', 'id' => $cart->goodsId]); ?>"><img src="<?= $cart->goods_image; ?>" alt=""></a></div>
                                    <div class="list_text"><a href="<?= UrlService::build(['intergral-goods/detail', 'id' => $cart->goodsId]); ?>"><?= $cart->goodsName; ?></a></div>
                                </li>
                                <li class="list_info">
                                    <?php foreach (explode(',', $cart->getAttrInfo()) AS $attr) :?>
                                        <p><?= str_replace(':', ' : ', trim($attr)); ?></p>
                                    <?php endforeach; ?>
                                </li>
                                <li class="list_price">
                                    <p class="price"><span><?= $cart->goodsPrice; ?></span>积分</p>
                                </li>
                                <li class="list_amount">
                                    <div class="amount_box">
                                        <input type="text" name="amount" data-info="<?= $cart->goodsId;?>" data-sku="<?= $cart->sku_id;?>" value="<?= $cart->goodsNum; ?>" data-limit="<?= $cart->amountLimit; ?>" class="sum">
                                        <a href="javascript:;" data-id="amountCtrl" data-info="<?= $cart->goodsId;?>" data-sku="<?= $cart->sku_id;?>" data-type="plus" class="plus"><img src="/public/index/images/icon_jia.jpg" alt=""></a>
                                        <a href="javascript:;" data-id="amountCtrl" data-sku="<?= $cart->sku_id;?>" data-info="<?= $cart->goodsId;?>" data-type="reduce" class="reduce reSty"><img src="/public/index/images/icon_jian.jpg" alt=""></a>
                                    </div>
                                </li>
                                <li class="list_sum">
                                    <p class="sum_price"><span><?= $cart->totalPrice; ?></span>积分</p>
                                </li>
                                <li class="list_op">
                                    <p class="del"><a href="javascript:;" class="delBtn"  data-id="delete" data-info="<?= $cart->id; ?>">删除</a></p>
                                </li>
                            </ul>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="bar-wrapper">
                        <div class="bar-left">
                            <li class="list_chk">
                                <!--所有商品全选-->
                                <input type="checkbox" id="all" class="whole_check"  data-id="checkAll" <?= $cartAmount==$amount ? 'checked' : ''; ?>>
                                <label for="all"></label>
                                全选
                            </li>
                        </div>
                        <div class="bar-right">
                            <div class="piece">已选择<strong class="piece_num"><?= $amount; ?></strong>件商品</div>
                            <div class="totalMoney">合计: <strong class="total_text"><?= $totalPrice; ?>积分</strong></div>

                            <?php if ($amount) : ?>
                                <div class="calBtn"><a style="background-color: #d87f36;cursor: auto;" href="<?= UrlService::build('integral-cart/create-order'); ?>">结算</a></div>
                            <?php else :?>
                                <div class="calBtn"><a href="javascript:;">结算</a></div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php else :?>
		<div class="com_nodata">
			<img src="/public/index/images/img_result_nogoods.png" alt="购物车空" class="img">
			<div class="con">
				<p class="p1">亲，您的购物车还没有商品哦~</p>
				<a href="<?= UrlService::build('integral/index'); ?>" class="btn btn-warning">马上去逛逛</a>
			</div>
		</div>
        <?php endif; ?>

		<div class="mycart_groom">相关推荐</div>
		<div class="mycart_groom_product clearfix">
            <?php foreach ($intergralGoods AS $intergralGood) :?>
			<a href="<?= UrlService::build(['intergral-goods/detail', 'id' => $intergralGood->id]); ?>" class="mygp_con">
				<img src="<?= $intergralGood->goods_pic; ?>" alt="<?= $intergralGood->goods_name; ?>">
				<p class="p_name text-truncate"><?= $intergralGood->goods_name; ?></p>
				<p class="p_price orange"><span><?= $intergralGood->goods_integral; ?></span>积分</p>
			</a>
            <?php endforeach; ?>
		</div>

	</div>

<?php include APP_PATH . '/views/common/footer.php';?>
<script>
    // 全选
    $('input[data-id="checkAll"]').click(function () {
        var status = $(this).is(':checked');

        $.ajax({
            url: '<?= UrlService::build('integral-cart/check'); ?>',
            type : 'post',
            async : false,
            data:{
                status : status ? 1 : 0,
                ids : [].join(',')
            },
            dataType:'json',
            success:function(data) {
                if (data.status) {
                    window.location.reload();
                    return false;
                }
                alert(data.msg);
            }
        })
    });

    // 单选
    $('input[data-id="checks"]').click(function () {
        var status = $(this).is(':checked'),
            id = $(this).attr('data-val');

        $.ajax({
            url: '<?= UrlService::build('integral-cart/check'); ?>',
            type : 'post',
            async : false,
            data:{
                status : status ? 1 : 0,
                ids : [id].join(',')
            },
            dataType:'json',
            success:function(data) {
                if (data.status) {
                    window.location.reload();
                    return false;
                }
                alert(data.msg);
            }
        })
    });

    // 增减控件
    $('a[data-id="amountCtrl"]').click(function () {
        var type = $(this).attr('data-type'),
            id = $(this).attr('data-info'),
            skuId = $(this).attr('data-sku'),
            amountDom = $('input[name="amount"][data-info="' + id + '"][data-sku="' + skuId + '"]');

        switch (type) {
            case 'plus' :
                var val = parseInt(amountDom.val()) + 1;
                break;
            case 'reduce' :
                var val = parseInt(amountDom.val()) - 1;
                break;
        }

        amountDom.val(val).change();
    });

    // 数量输入控件
    $('input[name="amount"]').change(function () {
        var val = $(this).val(),
            id = $(this).attr('data-info'),
            skuId = $(this).attr('data-sku'),
            limit = $(this).attr('data-limit');
        if (!/^\d+$/.test(val)) {
            val = 0;
        }
        val = parseInt(val);

        if (val < 0 ) {
            alert("请输入有效的商品数量");
            val = 0;
        }
        if (val > parseInt(limit)) {
            alert("小二正在努力补货中...");
            val = limit;
        }

        $.ajax({
            url: '<?= UrlService::build('integral-cart/add'); ?>',
            type : 'get',
            async : false,
            data:{
                id : id,
                amount : val,
                skuId : skuId
            },
            dataType:'json',
            success:function(data) {
                if (data.status) {
                    window.location.reload();
                    return false;
                }
                $(this).val(val);
                alert(data.msg);
            }
        })
    });

    $('a[data-id="delete"]').click(function () {
        removeCart($(this));
    });
</script>
