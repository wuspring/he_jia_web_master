<?php

use DL\service\UrlService;
use common\models\StoreGcScore;

include APP_PATH . '/views/common/header.php'; ?>

<?php include APP_PATH . '/views/common/guider.php'; ?>


<div class="mx-auto w1200 clearfix pb-5">
    <nav class="breadcrumb com_crumbs">
        <span>当前位置：</span>
        <a class="breadcrumb-item" href="<?= UrlService::build(["index/index"]); ?>">首页</a>
        <a class="breadcrumb-item" href="<?= UrlService::build(["{$basePathData['path']}/index"]); ?>"><?= $basePathData['info']; ?></a>
        <span class="breadcrumb-item active">优惠劵</span>
    </nav>
    <nav class="navbar navbar-expand-sm com_tabgroup">
        <ul class="navbar-nav">
            <!-- 这里最多只能有8个，与客户确定好的，不需要换行 -->
            <li class="nav-item">
                <a href="#tabgroup0" class="nav-link">精品推荐</a>
            </li>
            <?php foreach ($store_class as $k=>$v):?>
                <li class="nav-item">
                    <a href="#tabgroup<?=$k+1?>" class="nav-link"><?=$v->goodClass->name?></a>
                </li>
            <?php endforeach;?>
        </ul>
    </nav>
    <!-- 滚动监听 -->
    <script type="text/javascript">
        var header = document.querySelector('.navbar');
        var origOffsetY = header.offsetTop;

        function onScroll(e) {
            window.scrollY >= origOffsetY ? header.classList.add('position-sticky') : header.classList.remove('position-sticky');
        }

        document.addEventListener('scroll', onScroll);
    </script>


    <div class="tabgroup_title" id="tabgroup0">精品推荐</div>

    <div class="coupon_list clearfix">

        <?php foreach ($recommendCoupon as $rk=>$rv):
          //  $goodDetailInfo = \common\models\Goods::findOne($rv->goods_id);
            $user=\common\models\User::findOne($rv->user_id);
            ?>
            <div class="coupon_con clearfix <?=(($rk+1)%3==0)?'con_last':''?>">
                <img src="<?= $user ? $user->avatar : ''; ?>" alt="<?= $user ? $user->nickname : ''; ?>" class="img">
                <div class="con">
                    <p class="p1 red"><?=intval($rv->money)?></p>
                    <p class="p2"><?=$rv->describe?></p>
                    <p class="p3">已领<?=$rv->num?>张</p>
                    <a href="javascript:void(0)" data-id="<?=$rv->id.'-'.$rv->user_id?>" class="btn btn-danger clickReceive">立即领取</a>
                    <!-- <a href="javascript:void(0)" class="btn btn-danger disabled">已领取</a> -->
                </div>
            </div>
        <?php endforeach;?>


    </div>

    <?php foreach ($store_class as  $sk=>$sv):?>
        <div class="tabgroup_title" id="tabgroup<?=$sk+1?>"><?=$sv->goodClass->name?></div>

            <div class="coupon_list clearfix">
           <?php foreach (StoreGcScore::getGoodsCoupon($sv->provinces_id,$sv->gc_id) as $gck=>$gcv):?>
                <div class="coupon_con clearfix <?=(($gck+1)%3==0)?'con_last':''?>">
                    <img src="<?=$gcv->user->avatar?>" alt="<?=$gcv->user->nickname?>" class="img">
                    <div class="con">
                        <p class="p1 red"><?=intval($gcv->money)?></p>
                        <p class="p2"><?=$gcv->describe?></p>
                        <p class="p3">已领<?=$gcv->num?>张</p>
                        <a href="javascript:void(0)"  data-id="<?=$gcv->id.'-'.$sv->user_id?>" class="btn btn-danger clickReceive">立即领取</a>
                    </div>
                </div>
            <?php endforeach;?>
            </div>

    <?php endforeach;?>

</div>
<script>
    //点击领取优惠劵
     $(".clickReceive").on('click',function () {

         <?php if(Yii::$app->user->isGuest):?>
         alert("你还没有登录", function () {
             window.location.href="<?=UrlService::build(['member/login'])?>";
         });
         <?php else:?>
         var url="<?=UrlService::build(['you-hui/get-coupon'])?>";
         var goodId_storeId=$(this).attr('data-id');
         var this_dom=$(this);
         $.get(url,{goodId_storeId:goodId_storeId},function (res) {
             if (res.code=1){
                 alert(res.msg);
                 this_dom.addClass('disabled');
             } else {
                 alert(res.msg)
             }
         },'json');
        <?php endif?>
     })

</script>


<?php include APP_PATH . '/views/common/footer.php'; ?>
