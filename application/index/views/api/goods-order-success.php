<?php
global $userCurrentCity;

?>

<!-- 模态框-预定成功弹框- -->
<!--<div class="modal fade" id="myModalTicketSuc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">-->
    <div class="modal-dialog modal-success" role="document">
        <div class="modal-content" style="height: 520px;">
            <div class="modal-header">
                <img src="/public/index/images/icon_ok3.png" alt="已预约成功" class="img">
                <div class="con">
                    <p class="p1">已预约成功</p>
                    <p class="p2">线上预约锁定，凭短信至展会现场下单~</p>
                </div>
                <button type="button" class="close" data-id="reloadclose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <?php
                $good = $orderGood->goods;
            ?>
            <div class="modal-body">
                <div class="modal_suc_con">
                    <img src="<?= $orderGood->goods_pic; ?>" alt="<?= $orderGood->goods_name; ?>" class="img">
                    <div class="con">
                        <p class="p1 text-truncate"><?= $good->goods_name; ?></p>
                        <div class="d">
                            <?= $good->goods_jingle; ?>
                        </div>
                        <p class="p_price orange"><span class="s_r">剩余<span><?= $ticketApplyGoodsInfo->good_amount; ?></span>个</span>抢购价：<span class="s1">￥<?= $orderGood->goods_price;?></span><span class="s2">/<?= $good->unit;?></span></p>
                    </div>
                </div>
                <div class="modal_suc_more orange"><img src="/public/index/images/icon_horn.png"><span><?= date('m月d日', strtotime($ticket->open_date)); ?></span>-<span><?= date('m月d日', strtotime($ticket->end_date)); ?></span>&nbsp;&nbsp;活动地址：<span><?= $ticket->open_place; ?></span><span><?= $ticket->address; ?></span></div>
            </div>
        </div>
    </div>
<!--</div>-->
<script>
    $('[data-id="reloadclose"]').click(function () {
        window.location.reload();
    })
</script>

