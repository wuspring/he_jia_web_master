<?php

?>
    <div class="modal-dialog modal-success-coupon" role="document">
        <div class="modal-content" style="height: 760px;">
            <div class="modal-body">
                <div class="modal_coupon_title">
                    <p class="p_title orange">优惠劵领取成功</p>
                    <?php if ($coupon->goods_id > 0) :?>
                    <p class="p_more"><?= $coupon->goods->goods_name; ?></p>
                    <?php endif; ?>
                </div>
                <div class="modal_coupon_remark">
                    <p class="p">活动规格：</p>
                    <div class="con">
                        <p>仅限于和家网家博会现场下单使用，单笔消费最多使用一张抵用券；</p>
                        <p>满减金额以现场实际支付金额为准；</p>
                        <p>凭抵用券短信至站位处下单，满足条件即可使用抵用券；</p>
                    </div>
                    <p class="p_date"><a href="<?= \DL\service\UrlService::build('personal/my-coupon'); ?>" class="a orange">查看已领优惠劵>></a>使用日期：<span><?= date('Y-m-d', strtotime($memberCoupon->begin_time)); ?></span>至<span><?= date('Y-m-d', strtotime($memberCoupon->end_time)); ?></span></p>
                </div>
                <div class="modal_coupon_btn">
                    <a href="javascript:void(0)" data-id="buttonCoupon" class="orange" data-dismiss="modal" >确 定</a>
                </div>

            </div>
        </div>
    </div>
<script>
    $('[data-id="buttonCoupon"]').click(function () {
        window.location.reload();
    })
</script>

