<?php

?>
<div class="modal-dialog modal-success" role="document">
    <div class="modal-content" style="height: 454px;">
        <div class="modal-header">
            <img src="/public/index/images/icon_ok3.png" alt="已预约成功" class="img">
            <div class="con">
                <p class="p1">店铺预约成功</p>
                <p class="p2"><?= $storeInfo->nickname; ?></p>
            </div>
            <button type="button" data-id="buttonCoupon" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <div class="clearfix shoplist_detail">
                <a href="javascript:void(0)" title="<?= $storeInfo->nickname; ?>" class="a_img float-left"><img src="<?= $storeInfo->avatar; ?>" alt="<?= $storeInfo->nickname; ?>"></a>
                <div class="con">
                    <p class="p_title"><?= $storeInfo->nickname; ?></p>
                    <?php
                    $n = intval($storeInfo->score);
                    ?>
                    <?php for ($i=0; $i<$n; $i++) : ?>
                        <img src="/public/index/images/icon_assess.png">
                    <?php endfor; ?>
                    <?php for ($i=5-$n; $i>0; $i--) : ?>
                        <img src="/public/index/images/icon_assess1.png">
                    <?php endfor; ?>
                    <span class="orange"><?= round($storeInfo->score,1); ?>分</span>| <?= $storeInfo->judge_amount; ?>条</p>
                    <p class="p_address"><img src="/public/index/images/icon_address.png">上海共<span class="orange"><?= count($shops); ?></span>家体验店</p>
                    <?php foreach ($storeInfo->tabs AS $index => $tab) :?>
                        <?php if ($index < 3) :?>
                            <p><span class="s_tag  <?php $arr=['s_red','s_orange','s_blue']; echo $arr[$index]?>"><span><?=$tab['tab']?></span></span><?=$tab['info'] ?></p>
                        <?php else: break; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="modal_suc_more orange"><img src="/public/index/images/icon_horn.png"><span><?= date('m月d日', strtotime($ticket->open_date)); ?></span>-<span><?= date('m月d日', strtotime($ticket->end_date)); ?></span>&nbsp;&nbsp;活动地址：<span><?= $ticket->open_place; ?></span><span><?= $ticket->address; ?></span></div>
        </div>
    </div>
</div>

<script>
    $('[data-id="buttonCoupon"]').click(function () {
        window.location.reload();
    })
</script>