﻿<?php
global $userCurrentCity;

?>

    <div class="modal-dialog modal_dialog_coupon" role="document">
        <div class="modal-content" style="height: 442px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body pt-5 pb-4">
                <div class="modal_coupon">
                    <img src="<?= $coupon->user->avatar; ?>" class="img float-left">
                    <div class="con">
                        <p class="p_name orange"><?= $coupon->goods->goods_name; ?></p>
                        <p class="p1"><?= $coupon->describe; ?></p>
                        <p class="p2">有效期：<span><?= $coupon->valid_day;?> 天</span></p>
                    </div>
                </div>
                <form action="<?= \DL\service\UrlService::build(['api/get-ticket-by-sign']); ?>" method="post" class="password_form">
                    <div class="password_form_li">
                        <label class="password_form_li_name" for=""><span>*</span>手机号：</label>
                        <input type="text" class="password_form_li_input" data-id="mobile000" name="mobile" placeholder="请输入手机号码" required="">
                        <p class="password_form_li_tip">请输入手机号码</p>
                    </div>
                    <div class="password_form_li">
                        <label class="password_form_li_name" for=""><span>*</span>验证码：</label>
                        <input type="text" class="password_form_li_input"  data-id="code000" name="code"  placeholder="请输入短信验证码" required="" style="width: 178px;">
                        <a href="javascript:void(0)" data-id="getCode000" class="btn btn-warning float-left" style="width: 80px;">验证码</a>
                        <!-- <a href="javascript:void(0)" class="btn btn-warning float-left disabled" style="width: 80px;">58s</a> -->
                        <p class="password_form_li_tip">请输入短信验证码</p>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="location" value="<?= $location; ?>">
                        <input type="hidden" name="cityId" value="<?= $userCurrentCity->id; ?>">
                        <button type="button" class="btn btn-warning"  data-id="confirm" data-dismiss="modal">立即领取</button>
                    </div>
<!--                    <p>√ 同时免费索票价值30元的和家家博会门票</p>-->
                </form>
                <a href="<?= \DL\service\UrlService::build('member/login'); ?>" class="modal_alink">已有账号？<br>登录领取更方便！</a>
            </div>
        </div>
    </div>

      <script>
          var mobileDom000 = $('input[data-id="mobile000"]'),
              codeDom000 = $('a[data-id="getCode000"]'),
              getCodeCycle000 = 60;

          codeDom000.click(function () {
              var mobile = mobileDom000.val();
              if (!/^1\d{10}$/.test(mobile)) {
                  alert("请输入正确的手机号");
                  return false;
              }

              if (!codeDom000.hasClass('disabled')) {
                  $.post(
                      '<?=\yii\helpers\Url::toRoute('api/get-code'); ?>',
                      {mobile : mobile},
                      function (res) {
                          res = JSON.parse(res);
                          if (res.status) {
                              if (codeDom000.hasClass('disabled')) {
                                  return false;
                              }

                              codeDom000.attr('data-cycle', getCodeCycle000).addClass('disabled');

                              var cycleClock = setInterval(function () {
                                  var cycle = parseInt(codeDom000.attr('data-cycle'));
                                  if (cycle > 0) {
                                      codeDom000.text("" + cycle +"s").attr('data-cycle', cycle-1);
                                  } else {
                                      codeDom000.text('验证码').removeClass('disabled');
                                      clearInterval(cycleClock);
                                  }
                              }, 1000);
                          }
                      }
                  )
              }
          });

          $('#globalLoginShadow').find('[data-id="confirm"]').bind('click', function () {
              var mobile = mobileDom000.val();
              if (!/^1\d{10}$/.test(mobile)) {
                  alert("请输入正确的手机号");
                  return false;
              }

              var code = $('input[data-id="code000"]');

              if (code.val().length < 1) {
                  alert("请输入验证码");
                  return false;
              }

              // $('#globalLoginShadow').find('form').eq(0).submit();
              // $('#globalLoginShadow').modal('hide');

              var form = $('#globalLoginShadow').find('form').eq(0),
                  formData = form.get(0),
                  data = new FormData(formData);

              $.ajax({
                  url: form.attr('action'),
                  data : data,
                  type: 'POST',
                  cache: false,
                  processData: false,
                  contentType: false,
                  success: function (res) {
                      res = JSON.parse(res);
                      if (res.code) {
                          alert(res.msg, function () {
                              $('#globalLoginShadow').modal('hide');
                              window.location.reload();
                          });
                          return false;
                      }

                      alert (res.msg, function () {
                          return false;
                      })
                  }
              });
          });
      </script>