<?php
use DL\service\UrlService;

global $userCurrentCity;
?>
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="height: 490px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body pt-5 pb-4">
            <div class="modal_product float-left shoplist_detail m-0 p-0">
                <a href="<?=UrlService::build(['dian-pu/index', 'storeId' => $storeInfo->user_id]); ?>" title="<?= $storeInfo->nickname; ?>" class="a_img"><img src="<?= $storeInfo->avatar; ?>" alt="<?= $storeInfo->nickname; ?>"></a>
                <div class="con ml-0">
                    <p class="p_title"><?= $storeInfo->nickname; ?></p>
                    <p class="p_assess">
                        <?php
                            $n = intval($storeInfo->score);
                        ?>
                        <?php for ($i=0; $i<$n; $i++) : ?>
                        <img src="/public/index/images/icon_assess.png">
                        <?php endfor; ?>
                        <?php for ($i=5-$n; $i>0; $i--) : ?>
                        <img src="/public/index/images/icon_assess1.png">
                        <?php endfor; ?>
                        <span class="orange"><?= round($storeInfo->score,1); ?>分</span>| <?= $storeInfo->judge_amount; ?>条</p>
                    <?php foreach ($storeInfo->tabs AS $index => $tab) :?>
                    <?php if ($index < 3) :?>
                    <p><span class="s_tag  <?php $arr=['s_red','s_orange','s_blue']; echo $arr[$index]?>"><span><?=$tab['tab']?></span></span><?=$tab['info'] ?></p>
                    <?php else: break; ?>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </div>

            </div>

            <form  method="post" action="<?= UrlService::build('api/get-ticket-by-sign'); ?>" class="password_form float-right">
                <h4 class="orange">店铺预约</h4>
                <div class="password_form_li">
                    <label class="password_form_li_name" for="" style="letter-spacing: 5px;">姓名：</label>
                    <input type="text" name="name" class="password_form_li_input" placeholder="请输入收件人姓名" required="">
                    <p class="password_form_li_tip">请输入收件人姓名</p>
                </div>
                <div class="password_form_li">
                    <label class="password_form_li_name" for=""><span>*</span>手机号：</label>
                    <input type="text" class="password_form_li_input" name='mobile' data-id="mobile000" placeholder="请输入手机号码" required="">
                    <p class="password_form_li_tip">请输入手机号码</p>
                </div>
                <div class="password_form_li">
                    <label class="password_form_li_name" for=""><span>*</span>验证码：</label>
                    <input type="text" class="password_form_li_input" name='code' placeholder="请输入短信验证码" required="" style="width: 178px;">
                    <a href="javascript:void(0)" data-id="getCode000" class="btn btn-warning float-left" style="width: 80px;">验证码</a>
                    <!-- <a href="javascript:void(0)" class="btn btn-warning float-left disabled" style="width: 80px;">58s</a> -->
                    <p class="password_form_li_tip">请输入短信验证码</p>
                </div>
                <input type="hidden" name="cityId" value="<?= $userCurrentCity->id; ?>">
                <input type="hidden" name="location" value="<?= $location; ?>">
                <div class="modal-footer mt-5">
                    <button type="button" class="btn btn-warning" data-id="confirm" data-dismiss="modal" data-toggle="modal" data-target="#myModalShopSuc">立 即 预 约</button>
                </div>
                <p class="mt-2" style=" text-align: left;padding-left: 26px;">√ 同时免费索票价值<?= $ticket->ticket_price; ?>元的和家家博会门票<br>&nbsp;&nbsp;&nbsp;时间地点：<?= date('m.d', strtotime($ticket->open_date)); ?>-<?= date('m.d', strtotime($ticket->end_date)); ?> <?= $ticket->open_place; ?></p>
            </form>
        </div>
    </div>
</div>
<script>
    var mobileDom000 = $('input[data-id="mobile000"]'),
        codeDom000 = $('a[data-id="getCode000"]'),
        getCodeCycle000 = 60;

    codeDom000.click(function () {
        var mobile = mobileDom000.val();
        if (!/^1\d{10}$/.test(mobile)) {
            alert("请输入正确的手机号");
            return false;
        }

        if (!codeDom000.hasClass('disabled')) {
            $.post(
                '<?=\yii\helpers\Url::toRoute('api/get-code'); ?>',
                {mobile : mobile},
                function (res) {
                    res = JSON.parse(res);
                    if (res.status) {
                        if (codeDom000.hasClass('disabled')) {
                            return false;
                        }

                        codeDom000.attr('data-cycle', getCodeCycle000).addClass('disabled');

                        var cycleClock = setInterval(function () {
                            var cycle = parseInt(codeDom000.attr('data-cycle'));
                            if (cycle > 0) {
                                codeDom000.text("" + cycle +"s").attr('data-cycle', cycle-1);
                            } else {
                                codeDom000.text('验证码').removeClass('disabled');
                                clearInterval(cycleClock);
                            }
                        }, 1000);
                    }
                }
            )
        }
    });

    var domShadow = $('#globalLoginShadow');
    domShadow.find('[data-id="confirm"]').bind('click', function () {
        var form = $('#globalLoginShadow').find('form').eq(0);
console.log(form);
        $.ajax({
            url: form.attr('action'),
            data : new FormData(form.get(0)),
            type: 'POST',
            cache: false,
            processData: false,
            contentType: false,
            success: function (res) {
                res = JSON.parse(res);
                if (res.status) {
                    console.log(res.data.key);
                    new Promise(function (resolve) {
                        domShadow.modal('hide');
                        setTimeout(resolve, 500, res.data)
                    }).then(function(data) {
                        orderPriceSuccess(data.key, data.type);
                    });

                    return false;
                }

                alert(res.msg);
            }
        });

        return false;
    });
</script>

