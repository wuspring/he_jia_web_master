﻿<?php
global $userCurrentCity;

?>
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content" style="height: 550px;">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body pt-5 pb-4">	
	      		<div class="modal_product float-left">
	      			<img src="<?= $good->goods_pic; ?>" alt="" class="img_product">
	      			<p class="p_name text-truncate"><?= $good->goods_name; ?></p>
	      			<div class="p_remark ellipsis-3"><?= $good->goods_jingle; ?></div>
	      			<p class="p_more">剩余<?= $good->goods_storage; ?>个</p>
	      			<p class="p_price orange">抢购价: ￥ <span><?= $good->goods_price; ?></span>元/件</p>
	      			<p class="p_remark1">线上预约锁定，凭短信至展会现场下单</p>
	      		</div>

	        	<form method="post" action="<?= \DL\service\UrlService::build('api/get-tickets'); ?>" class="password_form float-right">
	        		<h4 class="orange">爆款预约</h4>
					<div class="password_form_li">
						<label class="password_form_li_name" for="" style="letter-spacing: 5px;">姓名：</label>
						<input type="text" class="password_form_li_input" data-id="name000" name="name"  placeholder="请输入收件人姓名" required="">
						<p class="password_form_li_tip">请输入收件人姓名</p>
					</div>
					<div class="password_form_li">
						<label class="password_form_li_name" for=""><span>*</span>手机号：</label>
						<input type="text" class="password_form_li_input" data-id="mobile000" name="mobile"  placeholder="请输入手机号码" required="">
						<p class="password_form_li_tip">请输入手机号码</p>
					</div>
					<div class="password_form_li">
						<label class="password_form_li_name" for=""><span>*</span>验证码：</label>
						<input type="text" class="password_form_li_input" data-id="code000" name="code" placeholder="请输入短信验证码" required="" style="width: 178px;">
						<a href="javascript:void(0)" data-id="getCode000"  class="btn btn-warning float-left" style="width: 80px;">验证码</a>
						<p class="password_form_li_tip">请输入短信验证码</p>
                        <input type="hidden" name="cityId" value="<?= $userCurrentCity->id; ?>">
                        <input type="hidden" name="location" value="<?= $location; ?>">
					</div>
				      <div class="modal-footer mt-5">
				        <button type="button" class="btn btn-warning"  data-id="confirm" data-dismiss="modal">立 即 预 约</button>
				      </div>
					<p class="mt-2" style=" text-align: left;padding-left: 26px;">√ 同时免费索票价值<?= $ticket->ticket_price; ?>元的和家家博会门票<br>&nbsp;&nbsp;&nbsp;时间地点：<?= date('m.d', strtotime($ticket->open_date)); ?>-<?= date('m.d', strtotime($ticket->end_date)); ?> <?= $ticket->open_place; ?></p>
				</form>
	      </div>
	    </div>
	  </div>
      <script>
          var mobileDom000 = $('input[data-id="mobile000"]'),
              codeDom000 = $('a[data-id="getCode000"]'),
              getCodeCycle000 = 60;

          codeDom000.click(function () {
              var mobile = mobileDom000.val();
              if (!/^1\d{10}$/.test(mobile)) {
                  alert("请输入正确的手机号");
                  return false;
              }

              if (!codeDom000.hasClass('disabled')) {
                  $.post(
                      '<?=\yii\helpers\Url::toRoute('api/get-code'); ?>',
                      {mobile : mobile},
                      function (res) {
                          res = JSON.parse(res);
                          if (res.status) {
                              if (codeDom000.hasClass('disabled')) {
                                  return false;
                              }

                              codeDom000.attr('data-cycle', getCodeCycle000).addClass('disabled');

                              var cycleClock = setInterval(function () {
                                  var cycle = parseInt(codeDom000.attr('data-cycle'));
                                  if (cycle > 0) {
                                      codeDom000.text("" + cycle +"s").attr('data-cycle', cycle-1);
                                  } else {
                                      codeDom000.text('验证码').removeClass('disabled');
                                      clearInterval(cycleClock);
                                  }
                              }, 1000);
                          }
                      }
                  )
              }
          });

          var domShadow = $('#globalLoginShadow');
          domShadow.find('[data-id="confirm"]').bind('click', function () {
              var form = $('#globalLoginShadow').find('form').eq(0);
              //
              $.ajax({
                  url: form.attr('action'),
                  data : new FormData(form.get(0)),
                  type: 'POST',
                  cache: false,
                  processData: false,
                  contentType: false,
                  success: function (res) {
                      res = JSON.parse(res);
                      if (res.status) {
                          console.log(res.data.key);
                          new Promise(function (resolve) {
                              domShadow.modal('hide');
                              setTimeout(resolve, 500, res.data)
                          }).then(function(data) {
                              orderPriceSuccess(data.key, data.type);
                          });

                          return false;
                      }

                      alert(res.msg);
                  }
              });

              return false;
          });
      </script>