<?php
use DL\service\UrlService,
    DL\vendor\ConfigService,
    DL\Project\CityExpand;
global $cityPhone;
$pcSsdh = ConfigService::init(ConfigService::EXPAND)->get('pc_ssdh');

$logo = ConfigService::init('system')->get('logo');
//$erWeiMa = ConfigService::init('system')->get('erweima');
$cityConfig = CityExpand::init()->get($defaultCity->id, CityExpand::TYPE_CITY_EXPAND);
$erWeiMa = isset($cityConfig->erweima)?$cityConfig->erweima:'';


isset($searchKeyword) or $searchKeyword='';
?>
<div class="top bg_glay" id="top">
    <div class="mx-auto w1200">
        <span class="navbar-text float-left ml-2">Hi，欢迎来到和家网！ </span>
        <?php if (empty($userInfo)): ?>
        <ul class="nav top_left float-left">
            <li class="nav-item"><a href="<?= UrlService::build('member/login'); ?>" class="nav-link a">登录</a></li>
            <li class="nav-item"><a href="<?= UrlService::build('member/reg'); ?>" class="nav-link a orange">免费注册</a></li>
        </ul>
        <?php else: ?>
            <ul class="nav top_left float-left">
                <li class="nav-item"><a href="<?=UrlService::build(['personal/index'])?>" class="nav-link a"><?= $userInfo->nickname ?></a></li>
                <li class="nav-item"><a href="<?= UrlService::build('member/logout'); ?>" class="nav-link a orange">安全退出</a></li>
            </ul>
        <?php endif; ?>
        <span class="navbar-text float-right mr-2">服务热线：<span class="orange"><?= $cityPhone; ?></span></span>
        <ul class="justify-content-end nav top_right float-right">
            <?php if (!empty($userInfo)): ?>
            <li class="nav-item dropdown">
                <a href="javascript:void(0)" class="nav-link dropdown-toggle a" data-toggle="dropdown">我的和家</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?= UrlService::build('personal/index'); ?>">个人中心</a>
                    <a class="dropdown-item" href="<?= UrlService::build('personal/appointment-order'); ?>">订单列表</a>
                    <a class="dropdown-item" href="<?= UrlService::build('personal/my-address'); ?>">收货地址</a>
                    <a class="dropdown-item" href="<?= UrlService::build('personal/my-history'); ?>">我的足迹</a>
                    <a class="dropdown-item" href="<?= UrlService::build('personal/my-integral'); ?>">我的积分</a>
                </div>
                <?php endif; ?>
            </li>
            <li class="nav-item"><a href="<?=UrlService::build(['member/help'])?>" class="nav-link a"><img src="/public/index/images/icon_ask.png">帮助中心</a></li>
            <li class="nav-item"><a href="javascript:void(0)" id="addcollect"  class="nav-link a">收藏本站</a></li>
        </ul>
    </div>
</div>

<div class="header">
    <div class="mx-auto w1200 clearfix">
        <a href="/<?= "{$defaultCity->pinyin}"; ?>" class="float-left logo"><img src="<?= $logo ?>" alt="和家网"></a>
        <div class="float-left header_local">
            <a href="javascript:void(0)" class="a"><span><?= $defaultCity->cname; ?></span><i></i></a>
            <div class="header_local_city">
                <?php foreach ($groupCitys AS $groupCity) :?>
                <a href="<?= UrlService::build(['index/set-city', 'pr_id' => $groupCity->id]); ?>"><?= $groupCity->cname; ?></a>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="float-right header_ewm">
            <img src="<?= $erWeiMa; ?>" alt="微信公众号" class="float-left">
            <p class="p1 green">微信扫一扫</p>
            <p class="p2">关注和家网微信公众号</p>
            <p class="p3 orange">随时了解展会动态！</p>
        </div>
        <div class="header_search">
            <form action="<?= UrlService::build(['goods/lists']); ?>" method="get">
                <div class="input-group">
                    <input type="text" name="keyword" class="form-control" value="<?= $searchKeyword; ?>" placeholder="同价双十二，爆品天天有">
                    <input type="hidden" name="r" value="goods/lists">
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit">搜索</button>
                    </div>
                </div>
            </form>
            <ul class="nav float-left header_search_groom">
                <?php
                if (strlen($pcSsdh)) {
                    $pcSsdhArr=json_decode($pcSsdh);
                    foreach ($pcSsdhArr AS $kwd) {
                        $href = UrlService::build(['goods/lists', 'gc_id' => $kwd]);
                        $goodClass=\common\models\GoodsClass::findOne($kwd);
                        echo sprintf('<li class="nav-item"><a href="%s" class="nav-link a">%s</a></li>', $href, $goodClass->name);
                    }
                }
                ?>
            </ul>
        </div>
    </div>
</div>
