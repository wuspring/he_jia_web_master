<?php
use \common\models\GoodsClass;
use DL\service\UrlService;
use common\models\FloorIndex;
use \common\models\Ticket;

$categorys = GoodsClass::findAll([
    'fid' => 0,
    'is_del' => '0',
    'is_show'=>0
]);

isset($indexIndex) or $indexIndex = 0;

$jzTicket = Ticket::findOne([
    'ca_type' => Ticket::CA_TYPE_ZHAN_HUI,
    'type' => Ticket::TYPE_JIA_ZHUANG,
    'citys' => $defaultCity->id,
    'status' => 1
]);
$jzTicketId = $jzTicket ? $jzTicket->id : 0;

$jhTicket = Ticket::findOne([
    'ca_type' => Ticket::CA_TYPE_ZHAN_HUI,
    'type' => Ticket::TYPE_JIE_HUN,
    'citys' => $defaultCity->id,
    'status' => 1
]);
$jhTicketId = $jhTicket ? $jhTicket->id : 0;

$yyTicket = Ticket::findOne([
    'ca_type' => Ticket::CA_TYPE_ZHAN_HUI,
    'type' => Ticket::TYPE_YUN_YING,
    'citys' => $defaultCity->id,
    'status' => 1
]);
$yyTicketId = $yyTicket ? $yyTicket->id : 0;

$cityConfig = \DL\Project\CityExpand::init()->get($defaultCity->id, \DL\Project\CityExpand::TYPE_CITY_EXPAND);
$moreNavs = isset($cityConfig->pc_navs)?json_decode($cityConfig->pc_navs,true):'';
?>
<style>
    .hide {
        display: none!important;
    }
</style>
<div class="index_top_nav">
    <div class="mx-auto w1200">
        <div class="index_top_nav_l float-left">
            <div class="index_top_nav_top hide">
                <a href="javascript:void(0)"><img src="/public/index/images/icom_menu.png" alt="">商品分类</a>
            </div>
            <?php if (isset($guiderShow)) : ?>
            <div class="hide index_top_nav_main">
            <?php else: ?>
            <div class="hide index_top_nav_main nav_main">
            <?php endif; ?>
                <div class="index_category">
                    <?php
                        foreach ($categorys AS $category) :
                         $caSonCategorys = $category->sonCategory;
                        $firstCaSonCategorys = reset($caSonCategorys );
                    ?>
                    <div class="index_category_list">
                        <div class="index_category_list_l">
                            <img src="<?= $category->icoImg; ?>" alt="<?= $category->name;?>">
                            <a href="<?= UrlService::build(['goods/lists', 'gc_id' => $firstCaSonCategorys->id]); ?>"><?= $category->name;?></a>
                        </div>
                        <div class="index_category_list_r">
                            <?php foreach ($caSonCategorys AS $sonCategory) :?>
                            <a href="<?= UrlService::build([ 'goods/lists', 'gc_id' => $sonCategory->id]); ?>"><?= $sonCategory->name;?></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="index_top_nav_r float-left">
            <a href="<?= "/{$defaultCity->pinyin}"; ?>" class="<?= $indexIndex == 1 ? 'current' : ''; ?>">首页</a>
            <div class="explosion_menu">
                <a target="_blank" href="<?= UrlService::build(['jia-zhuang/index2', 'ticketId' => $jzTicketId])?>" class="<?= $indexIndex == 2 ? 'current' : ''; ?>">家装采购</a>
                <div class="explosion_menu_two">
                    <?php if(!empty($moreNavs)):?>
                        <?php foreach ($moreNavs as $nav):?>
                            <?php list($href,$name) = explode('@', $nav);?>
                            <a href="<?=$href?>" target="_blank"><?=$name?></a>
                        <?php endforeach; ?>
                    <?php endif;?>
                    <a target="_blank" href="<?=UrlService::build(['jia-zhuang/jing-xuan', 'ticketId' => $jzTicketId])?>">品牌馆</a>
                    <a target="_blank" href="<?=UrlService::build(['jia-zhuang/yu-yue', 'ticketId' => $jzTicketId])?>">预约爆款</a>
                    <a target="_blank" href="<?=UrlService::build(['jia-zhuang/you-hui', 'ticketId' => $jzTicketId])?>">抵用券</a>
                    <a target="_blank" href="<?=UrlService::build(['jia-zhuang/brand-activity', 'ticketId' => $jzTicketId])?>">品牌活动</a>
                </div>
            </div>

            <div class="explosion_menu">
                <a target="_blank" href="<?= UrlService::build(['jie-hun/index2', 'ticketId' => $jhTicketId])?>" class="hide <?= $indexIndex == 3 ? 'current' : ''; ?>">结婚采购</a>
                <div class="explosion_menu_two">
                    <?php if(!empty($moreNavs)):?>
                        <?php foreach ($moreNavs as $nav):?>
                            <?php list($href,$name) = explode('@', $nav);?>
                            <a target="_blank" href="<?=$href?>"><?=$name?></a>
                        <?php endforeach; ?>
                    <?php endif;?>
                    <a target="_blank" href="<?=UrlService::build(['jie-hun/jing-xuan', 'ticketId' => $jhTicketId])?>">品牌馆</a>
                    <a target="_blank" href="<?=UrlService::build(['jie-hun/yu-yue', 'ticketId' => $jhTicketId])?>">预约爆款</a>
                    <a target="_blank" href="<?=UrlService::build(['jie-hun/you-hui', 'ticketId' => $jhTicketId])?>">抵用券</a>
                    <a target="_blank" href="<?=UrlService::build(['jie-hun/brand-activity', 'ticketId' => $jhTicketId])?>">品牌活动</a>
                </div>
            </div>
            <div class="explosion_menu">
                <a target="_blank" href="<?= UrlService::build(['yun-ying/index2', 'ticketId' => $yyTicketId])?>" class="hide <?= $indexIndex == 4 ? 'current' : ''; ?>">孕婴童采购</a>
                <div class="explosion_menu_two">
                    <?php if(!empty($moreNavs)):?>
                        <?php foreach ($moreNavs as $nav):?>
                            <?php list($href,$name) = explode('@', $nav);?>
                            <a target="_blank" href="<?=$href?>"><?=$name?></a>
                        <?php endforeach; ?>
                    <?php endif;?>
                    <a target="_blank" href="<?=UrlService::build(['yun-ying/jing-xuan', 'ticketId' => $yyTicketId])?>">品牌馆</a>
                    <a target="_blank" href="<?=UrlService::build(['yun-ying/yu-yue', 'ticketId' => $yyTicketId])?>">预约爆款</a>
                    <a target="_blank" href="<?=UrlService::build(['yun-ying/you-hui', 'ticketId' => $yyTicketId])?>">抵用券</a>
                    <a target="_blank" href="<?=UrlService::build(['yun-ying/brand-activity', 'ticketId' => $yyTicketId])?>">品牌活动</a>
                </div>
            </div>


            <a target="_blank" href="<?= UrlService::build(['integral/index'])?>"  class="<?= $indexIndex == 6 ? 'current' : ''; ?>">积分夺宝</a>
            <div href="javascript:void(0);" class="explosion_menu">
                <a target="_blank" href="<?= UrlService::build(['fitup-school/index'])?>" class="<?= $indexIndex == 7 ? 'current' : ''; ?>">装修学堂</a>
                <div class="explosion_menu_two">
                    <a target="_blank" href="<?=UrlService::build(['fitup-school/fitup-case'])?>">装修案例</a>
                    <a target="_blank" href="<?= UrlService::build(['fitup-school/answer'])?>">装修问答</a>
                    <a target="_blank" href="<?= UrlService::build(['fitup-school/information'])?>">装修资讯</a>
                </div>
            </div>
            <div href="javascript:void(0);" class="explosion_menu">
                <a target="_blank" href="<?= UrlService::build(['jia-zhuang/index', 'ticketId' => $jzTicketId])?>"  class="<?= $indexIndex == 5 ? 'current' : ''; ?>">免费索票</a>
                <!--                <a href="--><?php//// UrlService::build(['jia-zhuang/index'])?><!--"  class="--><?php //= $indexIndex == 5 ? 'current' : ''; ?><!--">展会索票</a>-->
                <?php if (false) :?>
                    <div class="explosion_menu_two" style="display: none">
                        <a target="_blank" class="hide" href="<?= UrlService::build(['jie-hun/index', 'ticketId' => $jhTicketId])?>">婚庆展索票</a>
                        <a target="_blank" class="hide" href="<?= UrlService::build(['jia-zhuang/index', 'ticketId' => $jzTicketId])?>">家装展索票</a>
                        <a target="_blank" class="hide" href="<?= UrlService::build(['yun-ying/index', 'ticketId' => $yyTicketId])?>">孕婴展索票</a>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>