<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019-4-25
 * Time: 14:11
 */
global $userCurrentCity;

?>
<style>
    .hide {
        display: none!important;
    }
</style>

<!-- 模态框-索票弹框 -->
<div class="modal fade" id="globalLoginShadow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title pl-3">预约登录</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body pt-5 pb-4">
                <form method="post" action="<?= \DL\service\UrlService::build('api/get-ticket-by-sign'); ?>" class="password_form">
                    <div class="password_form_li">
                        <label class="password_form_li_name" for=""><span>*</span>姓名：</label>
                        <input type="text" class="password_form_li_input" data-id="name000" name="name" placeholder="请输入收件人姓名" required="">
                        <p class="password_form_li_tip">请输入手机号码</p>
                    </div>
                    <div class="password_form_li">
                        <label class="password_form_li_name" for=""><span>*</span>手机号：</label>
                        <input type="text" class="password_form_li_input" data-id="mobile000" name="mobile" placeholder="请输入手机号码" required="">
                        <p class="password_form_li_tip">请输入手机号码</p>
                    </div>

                    <div class="password_form_li">
                        <label class="password_form_li_name" for=""><span>*</span>验证码：</label>
                        <input type="text" class="password_form_li_input" name="code" placeholder="请输入短信验证码" required="" style="width: 190px;">
                        <a href="javascript:void(0)" data-id="getCode000" class="btn btn-warning float-left" style="width: 90px;">验证码</a>
                        <!-- <a href="javascript:void(0)" class="btn btn-warning float-left disabled" style="width: 80px;">58s</a> -->
                        <p class="password_form_li_tip">请输入短信验证码</p>
                    </div>
                    <div class="password_form_li hide">
                        <label class="password_form_li_name" for=""><span>*</span>地址：</label>
                        <input type="text" class="password_form_li_input" data-id="address000" name="address" placeholder="请输入收件地址" required="">
                        <p class="password_form_li_tip">请输入收件地址</p>
                    </div>
                    <p>请详细填写以上信息，<br>我们将免费为您快递一份价值20元的门票及展会资料</p>

                    <input type="hidden" name="cityId" value="<?= $userCurrentCity->id; ?>">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default hide" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-warning" data-id="confirm">免费索票</button>
            </div>
        </div>
    </div>
    <script>
        var mobileDom000 = $('input[data-id="mobile000"]'),
            codeDom000 = $('a[data-id="getCode000"]'),
            getCodeCycle000 = 60;

        codeDom000.click(function () {
            var mobile = mobileDom000.val();
            if (!/^1\d{10}$/.test(mobile)) {
                alert("请输入正确的手机号");
                return false;
            }

            if (!codeDom000.hasClass('disabled')) {
                $.post(
                    '<?=\yii\helpers\Url::toRoute('api/get-code'); ?>',
                    {mobile : mobile},
                    function (res) {
                        res = JSON.parse(res);
                        if (res.status) {
                            if (codeDom000.hasClass('disabled')) {
                                return false;
                            }

                            codeDom000.attr('data-cycle', getCodeCycle000).addClass('disabled');

                            var cycleClock = setInterval(function () {
                                var cycle = parseInt(codeDom000.attr('data-cycle'));
                                if (cycle > 0) {
                                    codeDom000.text("" + cycle +"s").attr('data-cycle', cycle-1);
                                } else {
                                    codeDom000.text('验证码').removeClass('disabled');
                                    clearInterval(cycleClock);
                                }
                            }, 1000);
                        }
                    }
                )
            }
        });

        $('#globalLoginShadow').find('[data-id="confirm"]').bind('click', function () {
            var mobile = mobileDom000.val();
            if (!/^1\d{10}$/.test(mobile)) {
                alert("请输入正确的手机号");
                return false;
            }

            var code = $('input[name="code"]');

            if (code.val().length < 1) {
                alert("请输入验证码");
                return false;
            }

            $('#globalLoginShadow').find('form').eq(0).submit();
            $('#globalLoginShadow').modal('hide');
        });
    </script>
</div>

