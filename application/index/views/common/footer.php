<?php
use DL\vendor\ConfigService;
use DL\service\UrlService;
use common\models\HelpType;
use DL\Project\CityExpand;

$company = ConfigService::init('system')->get('company');
$companyAddress = ConfigService::init('system')->get('companyAddress');
$icp = ConfigService::init('system')->get('icp');
$tel = ConfigService::init('system')->get('telephone');
//$erweima = ConfigService::init('system')->get('erweima');

$cityConfig = CityExpand::init()->get($defaultCity->id, CityExpand::TYPE_CITY_EXPAND);
$erWeiMa = isset($cityConfig->erweima)?$cityConfig->erweima:'';

$pcYjdb = ConfigService::init('system')->get('pc_yjdb');
$youLian=\common\models\Link::findAll(['type'=>1]);
$help_type=HelpType::find()->all();
?>
<?php
    isset($showPromise) or $showPromise = false;
    if ($showPromise) :
?>
<div class="promise" style="background: url('<?= $configIndex['promise']; ?>')  center no-repeat;"></div>
    <div class="explosin_nav">

        <!-- guider -->
        <!-- 楼层导航 -->
        <span class="navbar-text"><img src="/public/index/skin_jia/images/bg_float.png"></span>
        <ul class="nav flex-column">
            <li class="nav-item li">
                <a class="nav-link" href="#nav0">家博会亮点</a>
            </li>
            <li class="nav-item li">
                <a class="nav-link" href="#nav1">品牌优惠劵</a>
            </li>
            <li class="nav-item li">
                <a class="nav-link" href="#nav2">订金享特价</a>
            </li>
            <li class="nav-item li">
                <a class="nav-link" href="#nav3">爆款预约</a>
            </li>
            <li class="nav-item li">
                <a class="nav-link" href="#nav4">合作品牌</a>
            </li>
            <li class="nav-item li">
                <a class="nav-link" href="#nav5">逛展指南</a>
            </li>
            <li class="nav-item li">
                <a class="nav-link" href="#nav6">家博会介绍</a>
            </li>
            <li class="nav-item li">
                <a class="nav-link" href="#nav7">路线导航</a>
            </li>
            <li class="nav-item li_top">
                <a class="nav-link back-to-top" href="#top">
                    <i></i><p>返回顶部</p>
                </a>
            </li>
        </ul>
    </div>
<?php endif; ?>

<!-- guider -->
<!-- footer -->
<div class="footer">
    <div class="mx-auto w1200 clearfix">
        <div class="footer_all float-left"><img src="<?= $configIndex['smap']; ?>"></div>
        <?php foreach ($help_type as $help_type_k=>$help_type_v):?>
            <ul class="nav flex-column float-left">
                <li class="nav-item li0">
                    <a class="nav-link disabled" href="javascript:void(0)"><?=$help_type_v->type?></a>
                </li>
                  <?php foreach ($help_type_v->helpcont as $content_k=>$content_v):?>
                       <li class="nav-item">
                          <a class="nav-link" href="<?=UrlService::build(['member/help','contId'=>$content_v->id])?>"><?=$content_v->title?></a>
                       </li>
                 <?php endforeach;?>

            </ul>
        <?php endforeach;?>

        <div class="footer_ewm float-left">
            <p>扫码关注公众号</p>
            <img src="<?= $erWeiMa; ?>" alt="公众号">
        </div>
    </div>
</div>
<div class="footer_friends">
    <div class="mx-auto w1200 clearfix">
        友情链接：
        <?php foreach ($youLian as $youlian):?>
            <a href="<?=$youlian->url?>" target="_blank"><?=$youlian->name?></a>
        <?php endforeach;?>
    </div>
</div>
<div class="footer_copyright">CopyRight &#169 2007-2018  <?= "{$company} {$companyAddress} {$icp}"; ?></div>
<div class="footer_icp">
    <a href="javascript:void(0)"><img src="/public/index/images/img_icp1.jpg"></a>
    <a href="javascript:void(0)"><img src="/public/index/images/img_icp2.jpg"></a>
    <a href="javascript:void(0)"><img src="/public/index/images/img_icp3.jpg"></a>
    <a href="javascript:void(0)"><img src="/public/index/images/img_icp4.jpg"></a>
</div>