<?php
use DL\service\UrlService,
    DL\vendor\ConfigService,
    \common\models\IntergralGoodsClass;
isset($pageIndexKey) or $pageIndexKey = 1;

$categorys = IntergralGoodsClass::findAll([
    'fid' => 0,
    'is_del' => '0'
]);

isset($indexIndex) or $indexIndex = 0;
?>
<div class="index_top_nav index_top_nav_integral">
    <div class="mx-auto w1200">
        <div class="index_top_nav_l float-left">
            <div class="index_top_nav_top">
                <a href="javascript:void(0)"><img src="/public/index/images/icom_menu.png" alt="">礼品分类</a>
            </div>
            <div class="index_top_nav_main <?=($pageIndexKey==1)?'':'nav_main'?>">
                <div class="index_category">
                    <div class="index_category_list">
                        <?php foreach ($categorys AS $category) : ?>
                        <a href="<?=UrlService::build(['integral/lists', 'id' => $category->id,'gc' =>$category->id])?>" class="icl_list">
                            <img src="<?= $category->icoImg; ?>" alt="<?= $category->name;?>">
                            <span><?= $category->name;?></span>
                        </a>
                        <?php endforeach; ?>
                    </div>

                </div>
            </div>
        </div>
        <div class="index_top_nav_r float-left">
            <a href="<?=UrlService::build(['integral/index'])?>"  class="<?= $pageIndexKey==1 ? 'current' : ''; ?>">首页</a>
            <a href="<?=UrlService::build(['integral-treasure/index']) ?>"  class="<?= $pageIndexKey==2 ? 'current' : ''; ?>">积分夺宝</a>
            <a href="<?=UrlService::build(['integral-luckdraw/index'])  ?>"  class="<?= $pageIndexKey==3 ? 'current' : ''; ?>">积分抽奖</a>
            <a href="<?=UrlService::build(['integral-productlist/index']) ?>" class="<?= $pageIndexKey==4 ? 'current' : ''; ?>">我能兑换</a>
        </div>
    </div>
</div>