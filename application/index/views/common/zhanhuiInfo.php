<?php
function getGuides($lists)
{
    strpos($lists, '@') < 0 and $lists .= '@';
    list($href, $img, $desc, $title) = explode('@', $lists);

    return (object)[
        'img' => $img,
        'title' => is_null($title) ? '' : $title,
        'desc' => (is_null($desc) or strlen($desc) < 1) ? '' : $desc
    ];
}

?>
<?php if ($ticket) : ?>
    <?php
    $cityConfig = \DL\Project\CityExpand::init()->get($defaultCity->id, \DL\Project\CityExpand::TYPE_CITY_EXPAND);
    $guides = isset($cityConfig->guide) ? json_decode($cityConfig->guide, true) : '';
    $n = 0;
    $showTop = false;
    ?>
    <?php if (!empty($guides)): ?>
        <div class="com_title mt-3 mx-auto w1200" id="nav5">
            <em class="line line1"></em>
            <h3>逛展指南</h3>
            <em class="line line2"></em>
        </div>
        <div class="mx-auto w1200 clearfix explosin_guide">
            <?php foreach ($guides as $key => $item): ?>
                <?php
                $arr = getGuides($item);
                $n++;
                if(mb_strlen($arr->desc,'utf-8')>=50){
                    $showTop = true;
                }
                ?>
                <div class="explosin_guide_con float-left">
                    <img src="<?= $arr->img ?>" alt="<?= $arr->title ?>">
                    <h3 class="red"><?= $arr->title ?></h3>
                    <span class="red"><?= $n ?></span>
                    <p><?= $arr->desc ?></p>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <?php
    $introduce = isset($cityConfig->introduce)?json_decode($cityConfig->introduce, true):'';
    $n = 0;
    ?>
    <?php if(!empty($introduce)):?>
    <div class="com_title mt-3 mx-auto w1200" style="<?=$showTop?'margin-top:2rem !important;':''?>"  id="nav6">
        <em class="line line1"></em>
        <h3><?= $name; ?>介绍</h3>
        <em class="line line2"></em>
    </div>
    <div class="explosin_about clearfix mx-auto w1200">
        <?php foreach ($introduce as $key => $item): ?>
        <?php
            $arr = getGuides($item);
            $n++;
        ?>
        <div class="explosin_about_con float-left <?=$n%2==0?'con_even':''?>">
            <?php if($n%2==1):?>
            <img src="<?=$arr->img?>" alt="<?=$arr->title?>">
            <?php endif; ?>
            <div class="con">
                <em></em>
                <h3><?=$arr->title?></h3>
                <p><?=$arr->desc?></p>
            </div>
            <?php if($n%2==0):?>
            <img src="<?=$arr->img?>" alt="<?=$arr->title?>">
            <?php endif; ?>
        </div>
        <?php endforeach; ?>

    </div>
    <?php endif; ?>

    <div class="com_title mt-3 mx-auto w1200" id="nav7">
        <em class="line line1"></em>
        <h3>路线导航</h3>
        <em class="line line2"></em>
    </div>
    <div class="explosin_route mx-auto w1200">
        <div class="map" id="map"></div>
        <div class="clearfix">
            <div class="con1 float-left">
                <h2><?= $ticket->name; ?></h2>
                <p>时间：<?= $ticket->open_date; ?></p>
                <p>地点：<?= $ticket->address; ?></p>
            </div>
            <div class="con2 float-left">
                <?= $ticket->arrive_method; ?>
            </div>

        </div>

    </div>

    <script type="text/javascript" src="https://map.qq.com/api/js?v=2.exp&key=<?= MAP_KEY; ?>"></script>
    <script>
        var deviceLng = '<?= $ticket->lng; ?>',
            deviceLat = '<?= $ticket->lat; ?>';
        $(function () {
            init();
        });

        var init = function () {
            var center = new qq.maps.LatLng(deviceLat, deviceLng);
            var map = new qq.maps.Map(document.getElementById('map'), {
                center: center,
                zoom: 13
            });
            var infoWin = new qq.maps.InfoWindow({
                map: map
            });
            infoWin.open();
            //tips  自定义内容
            infoWin.setContent('<div class="map_address">' +
                '<p class="p"><img src="/public/index/images/icon_address.png"><?= (isset($ticket) and $ticket) ? $ticket->name : '和家家装展'; ?></p>' +
                '<input id="map_input_mobile" placeholder="请输入手机号，地址会发送到您手机上" type="text">' +
                '<button class="btn btn-warning map_btn">获取地址</button></div>');
            infoWin.setPosition(center);
        };

        //点击获取
        $(document).on('click', '.map_btn', function () {
            var map_input_mobile = $("#map_input_mobile").val();
            if (map_input_mobile.length > 0) {
                $url = "<?=\DL\service\UrlService::build(['api/send-sms', 'type' => 'map', 'ticket' => $ticket->id])?>";
                $.post($url, {
                    mobile: map_input_mobile,
                    codeType: "<?=\common\models\SentSms::TYPE_ADDRESS_NOTICE ?>"
                }, function (res) {

                    if (res.status == 1) {
                        alert('地址已经发送，请注意接收');
                        $("#map_input_mobile").val('');
                    }
                }, 'json');
            } else {
                alert("手机号不能为空")
            }
        })


    </script>
<?php endif; ?>
