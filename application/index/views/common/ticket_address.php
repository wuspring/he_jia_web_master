<?php

include APP_PATH . '/views/common/header.php';
include APP_PATH . '/views/common/guider.php';
?>

	<div class="mx-auto w1200 clearfix">
		<div class="ticketaddress">
			<p class="p1">您已报名成功，记得<span><?= date('Y年m月d日', strtotime($ticket->open_date)); ?>-<?= date('m月d日', strtotime($ticket->end_date)); ?></span> <span><?= $ticket->name; ?></span>（<span><?= $ticket->open_place; ?></span>）的约惠哦，更多惊喜等着您！<br>添加微信<span><?= $weiXinHao; ?></span>领取更多逛展福利</p>
			<div class="con clearfix">
				<div class="con_ewm float-right">
					<p class="p4">微信扫一扫</p>
					<img src="<?= $erWeiMa; ?>" alt="公众号二维码">
					<p class="p5">关注公众号</p>
					<p class="p5">【和家网<?= $ticketTypeName; ?>】享更多福利</p>
				</div>
				<form class="con_form float-left" data-id="setAddress" action="<?= \DL\service\UrlService::build('ticket-address'); ?>" method="post">
					<p class="p2">请进一步完善快递地址，以便签收<?= $ticket->ticket_price; ?>元的和家家博会门票</p>
					<p class="p3"><span>快递地址：</span><textarea name="address" class="textarea" placeholder="请填写详细地址"></textarea></p>

                    <input type="hidden" name="ask_id" value="<?= $ticketAsk->id; ?>">
					<a href="javascript:void(0)" data-id="submit" class="btn btn-warning">提交</a>
				</form>
                <script>
                    $('[data-id="submit"]').click(function () {
                        var address = $('[name="address"]').val();

                        if (address.length < 1) {
                            alert("请完善地址信息");
                            return false;
                        }

                        // $('form[data-id="setAddress"]').submit();


                        var form = $('form[data-id="setAddress"]'),
                            formData = new FormData(form.get(0)),
                            url = form.attr('action');

                        $.ajax({
                            url: url,
                            data : formData,
                            type: 'POST',
                            cache: false,
                            processData: false,
                            contentType: false,
                            success: function (res) {
                                res = JSON.parse(res);
                                if (res.status) {
                                    alert("快递地址提交成功，请注意接听来电", function () {
                                        window.location.href = '<?= \DL\service\UrlService::build("index/index"); ?>';
                                    });

                                    return false;
                                }

                                alert (res.msg, function () {
                                    return false;
                                })
                            }
                        });
                    });
                </script>
			</div>
			<p class="p_coupon">更多优惠：<a href="<?= \DL\service\UrlService::build(['you-hui']); ?>" class="btn btn-warning">电子优惠劵</a></p>
			<div class="content">
                <?= $ticket->notice_words; ?>
			</div>
		</div>


	</div>

<?php include APP_PATH . '/views/common/footer.php';?>