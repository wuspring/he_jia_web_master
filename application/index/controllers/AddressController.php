<?php

namespace index\controllers;

use common\models\Address;
use common\models\Provinces;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class AddressController extends BaseController
{
    public function init()
    {
        parent::init();


        empty($this->memberInfo) and apiSendError("用户未登录");
    }

    public function actionIndex()
    {
        $addressLists = Address::find()
            ->where([
                'memberId' => \Yii::$app->user->id,
                'is_delete' => 0,
//                'user_type' => 'member'
            ])->orderBy('isDefault DESC')->all();

        $provinces = Provinces::findAll([
            'upid' => 1
        ]);
        $provinces = ArrayHelper::map($provinces, 'id', 'cname');

        return $this->render('index', [
            'listKey' => 2,
            'provinces' => $provinces,
            'addressLists' => $addressLists,
        ]);
    }

    /**
     * ajax获取省市区
     * @param integer $upid
     * @return array
     * @author  黄西方
     */
    public function actionCity($upid = 1)
    {
        $provinces = Provinces::find()->where(['upid' => $upid])->asArray()->all();
        $cities = ArrayHelper::map($provinces, 'id', 'cname');
        echo json_encode([
            'code' => 0,
            'data' => $cities
        ]);
        \Yii::$app->end();
    }

    /**
     * 新增收货地址
     * @param integer $upid
     * @return array
     * @author  黄西方
     * @date  2018-10-22
     */
    public function actionCreate()
    {
        $json = ['code' => 1, 'msg' => '参数错误'];
        if (\Yii::$app->request->isPost) {
            $post = \Yii::$app->request->post();
            $amount = Address::find()->where([
                'memberId' => \Yii::$app->user->id,
//                'user_type' => 'member',
                'is_delete' => 0
            ])->count();
            $model = new Address();
            foreach ($post as $key => $val) {
                $model->$key = $val;
            }
//            $model->user_type = 'member';
            $model->isDefault = $amount > 0 ? 0 : 1;
            $model->memberId = \Yii::$app->user->id;
            $model->is_delete = 0;


            if ($model->save()) {
                $json = ['code' => 0, 'msg' => '操作成功'];
            } else {
                $json = ['code' => 1, 'msg' => '操作失败'];
            }

        }
        echo json_encode($json);
        \Yii::$app->end();
    }

    /**
     * 删除地址
     * @param integer $id
     * @return void
     * @author  黄西方
     * @date  2018-10-22
     */
    public function actionDelete($id)
    {
        Address::updateAll(['is_delete' => 1], [
            'id' => $id,
            'memberId' => $this->memberInfo->id
        ]);
        $json = ['code' => 0, 'msg' => '操作成功'];
        echo json_encode($json);
        \Yii::$app->end();
    }

    /**
     * 设置默认地址
     * @param integer $id
     * @return void
     * @author  黄西方
     * @date  2018-10-22
     */
    public function actionIsdefault($id)
    {
        Address::updateAll(['type' => 1], ['id' => $id]);
        Address::updateAll(['type' => 0], ['<>', 'id', $id]);
        $json = ['code' => 0, 'msg' => '操作成功'];
        echo json_encode($json);
        \Yii::$app->end();
    }

    /**
     * 编辑地址
     * @param integer $id
     * @return void
     * @author  黄西方
     * @date  2018-10-22
     */
    public function actionUpdatem($id)
    {
        $model = Address::findOne([
            'id' => $id,
            'memberId' => $this->memberInfo->id
        ]);
        if (\Yii::$app->request->isPost) {
            $post = \Yii::$app->request->post();
            foreach ($post as $key => $val) {
                $model->$key = $val;
            }

            if ($model->save()) {
                $json = ['code' => 0, 'msg' => '操作成功'];
            } else {
                $json = ['code' => 1, 'msg' => '操作失败'];
            }

        } else {
            $city_arr = $model->getChildProvince($model->province);
            $district_arr = $model->getChildProvince($model->city);
            $json = [
                'code' => 0,
                'data' => $model->attributes,
                'cities' => $city_arr,
                'districts' => $district_arr,
            ];
        }
        die(json_encode($json));
    }

    /**
     *
     */
    public function actionUpdate()
    {
        $userId = (int)\Yii::$app->user->id;
        $userId or die('get user info failed');

        $get = \Yii::$app->request->get();

        if (isset($get['id'])) {
            $get['id'] = (int)$get['id'];

            $address = Address::findOne([
                'id' => $get['id']
            ]);

            if (!$address) {
                $address = new Address();
                $address->user_id = $userId;
//                $address->user_type = 'member';
                $address->create_time = date('Y-m-d H:i:s');
            }
            $address->type = isset($get['type']) ? (int)$get['type'] : 0;

            isset($get['phone']) and $address->phone = $get['phone'];
            isset($get['name']) and $address->name = $get['name'];
            isset($get['info']) and $address->info = $get['info'];

            isset($get['province']) and $address->province = $get['province'];
            isset($get['city']) and $address->city = $get['city'];
            isset($get['district']) and $address->district = $get['district'];


            $address->place = $address->district;

            $mapKey = 'tlu4VQq7S2DdyFAOkjZYF9bzEgUxXOBk';
            $provinceinfo = (new Address)->getProvinceInfo($address->district);
            $search = str_replace('-', ',', $provinceinfo) . " {$address->info}";
            $placePosition = new PlacePosition($mapKey);
            $result = $placePosition->search($search);
            if ($result) {
                $address->lng = (string)$result['lng'];
                $address->lat = (string)$result['lat'];
            }

            $address->save() and apiSendSuccess();
        }

        apiSendError("网络错误");
    }

    /**
     * 设置默认地址
     */
    public function actionSetDefault()
    {
        $_POST['isDefault'] = 1;
        $id = (int)\Yii::$app->request->post('id');

        Address::updateAll(['isDefault' => 0], [
            'isDefault' => 1,
            'memberId' => $this->memberInfo->id
        ]);
        $this->actionUpdatem($id);
    }
}
