<?php
namespace index\controllers;

use common\models\Advertising;
use common\models\HelpCont;
use common\models\HelpType;
use common\models\IntegralLog;
use common\models\Member;
use common\models\MemberForm;
use common\models\MemberTicketStore;
use DL\Base\Config;
use DL\Project\CityTicket;
use DL\Project\Sms;
use DL\Project\Store;
use DL\service\UrlService;
use DL\vendor\ConfigService;
use \Yii;

class MemberController extends BaseController
{

    /**
     *  用户登录
     */
    public  function  actionLogin()
    {
        if (\Yii::$app->request->post()) {
            $post = \Yii::$app->request->post();
            $model = new MemberForm();
            $model->username = $post['username'];
            $model->password = $post['password'];

            if ($model->login()) {
                apiSendSuccess("登录成功", [
                    'location' => UrlService::build(['index/index'])
                ]);
//                return $this->redirect(['index/index']);
            }

            apiSendError('帐号或密码错误');
        }
        $cityId = $this->getRequestCity();
        $picture = Advertising::find()->where(['province_id'=>$cityId,'adid'=>15])->orderBy('id DESC')->one();
        return $this->render('login',[
            'pic'=>$picture
        ]);
    }
    /**
     *  用户注册
     */
    public function actionReg()
    {
        if (Yii::$app->request->isPost) {
            $username = Yii::$app->request->post('username', '');
            $mobile = Yii::$app->request->post('mobile', '');
            $code = Yii::$app->request->post('code', '');
            $password = Yii::$app->request->post('password', '');

            preg_match('/^1\d{10}$/', $mobile) or stopFlow("请输入正确的手机号", 'member/quick-login');
            $session = Yii::$app->session;
//            "{$mobile}-{$code}" == $session->get('SMS_CODE') or stopFlow("请输入正确的验证码", 'member/quick-login');
            $smsCode = $session->get('SMS_CODE');
            $smsCode or stopFlow("验证码无效", 'member/quick-login');
            $smsCode['expire'] > time()  or stopFlow("验证码已过期", 'member/quick-login');
            "{$mobile}-{$code}" == $smsCode['value'] or stopFlow("请输入正确的验证码", 'member/quick-login');

            $member = Member::find()->where([
                'or',
                ['=', 'username', trim($username)],
                ['=', 'mobile', trim($mobile)],
            ])->one();

            if ($member) {
                if ($member->mobile == trim($mobile) and preg_match('/^at_/', $member->username )) {
                    $hasMember = Member::findOne(['username' => trim($username)]);

                    $hasMember and stopFlow("该用户已注册", 'member/login');

                    $member->username = trim($username);
                    if ($member->save()) {
                        $memberForm = new MemberForm();
                        if ($memberForm->wxlogin($member->id)) {
                            return  $this->redirect(['index/index']);
                            die;
                        };
                    }

                }

                stopFlow("该用户已注册", 'member/login');
            }

            // 新用户注册

            $member = new Member();
            $member->username = $username;
            $member->mobile = trim($mobile);
            $member->wxnick = '用户_' . createRandKey();
            $member->nickname = $member->wxnick;
            $member->setPassword($password);
            $member->status = (string)$member::STATUS_ACTIVE;
            $member->save();

            Sms::init()->zhuCeHuiYuan($member->id, $password);

            $memberForm = new MemberForm();
            if ($memberForm->wxlogin($member->id)) {
                return  $this->redirect(['index/index']);
            };

            stopFlow("网络异常，请稍后重试");
        }
        $cityId = $this->getRequestCity();
        $picture = Advertising::find()->where(['province_id'=>$cityId,'adid'=>16])->orderBy('id DESC')->one();
        return $this->render('reg',[
            'pic'=>$picture
        ]);
    }

    /**
     * 判断手机号是否存在
     */
    public function actionExistMobile(){
        $json=array('code'=>0,'msg'=>'存在');
        $mobile = Yii::$app->request->get('mobile', '');
        $member = Member::find()->where(['=', 'mobile', trim($mobile)])->one();
       if ($member){
           $json=array('code'=>0,'msg'=>'存在');
       }else{
           $json=array('code'=>1,'msg'=>'不存在');
       }
       return  json_encode($json);

    }

    /**
     *   忘记密码
     */
    public  function  actionForgetPassword()
    {
        if (Yii::$app->request->isPost) {
            $username = Yii::$app->request->post('username', '');
            $mobile = Yii::$app->request->post('mobile', '');
            $code = Yii::$app->request->post('code', '');

            preg_match('/^1\d{10}$/', $mobile) or stopFlow("请输入正确的手机号", 'member/quick-login');
            $session = Yii::$app->session;
//            "{$mobile}-{$code}" == $session->get('SMS_CODE') or stopFlow("请输入正确的验证码", 'member/quick-login');
            $smsCode = $session->get('SMS_CODE');
            $smsCode or stopFlow("验证码无效", 'member/quick-login');
            $smsCode['expire'] > time()  or stopFlow("验证码已过期", 'member/quick-login');
            "{$mobile}-{$code}" == $smsCode['value'] or stopFlow("请输入正确的验证码", 'member/quick-login');

            $member = Member::find()->where([
                'and',
                ['=', 'username', trim($username)],
                ['=', 'mobile', trim($mobile)],
            ])->one();

            $member or stopFlow("您输入的用户信息不匹配", 'member/forget-password');

            $key = createRandKey(32);
            $session->set($key, $member->id);
            return  $this->redirect([
                    'member/forget-password-one',
                    'secret' => $key
           ]);

            stopFlow("网络异常，请稍后重试");
        }
        return $this->render('forget-password');
    }

    /**
     *   忘记密码 下一步
     */
    public  function  actionForgetPasswordOne()
    {
        $secret = Yii::$app->request->get('secret', '');
        strlen($secret) or stopFlow("重置密码秘钥无效", 'member/forget-password');
        $session = Yii::$app->session;
        $memberId = $session->get($secret, false);
        $memberId or stopFlow("未获取到有效的用户信息", 'member/forget-password');
        $member = Member::findOne($memberId);

        return $this->render('forget-password-one', [
            'member' => $member,
            'secret' => $secret
        ]);
    }

    /**
     *   忘记密码 下一步
     */
    public  function  actionForgetPasswordTwo()
    {
        $secret = Yii::$app->request->post('secret', '');
        strlen($secret) or stopFlow("重置密码秘钥无效", 'member/forget-password');

        $session = Yii::$app->session;
        $memberId = $session->get($secret, false);
        $memberId or stopFlow("未获取到有效的用户信息");
        $member = Member::findOne($memberId);

        $password = Yii::$app->request->post('password', '');
        strlen($password) or stopFlow("无效的密码", [
            'member/forget-password-one',
            'secret' => $secret
        ]);
        $member->setPassword($password);

        if ($member->save()) {
            $session->remove($secret);
            return $this->render('forget-password-two');
        }

        return $this->render('index/index');
    }




    /**
     *   快速登录
     */
    public  function  actionQuickLogin()
    {
        if (Yii::$app->request->isPost) {
            $username = Yii::$app->request->post('mobile', '');
            $code = Yii::$app->request->post('code', '');

//            preg_match('/^1\d{10}$/', $username) or stopFlow("请输入正确的手机号", 'member/quick-login');
            preg_match('/^1\d{10}$/', $username) or apiSendError("请输入正确的手机号");
            $session = Yii::$app->session;
//            "{$username}-{$code}" == $session->get('SMS_CODE') or apiSendError("请输入正确的验证码");
            $smsCode = $session->get('SMS_CODE');
            $smsCode or apiSendError("验证码无效");
            $smsCode['expire'] > time()  or apiSendError("验证码已过期");
            "{$username}-{$code}" == $smsCode['value'] or apiSendError("请输入正确的验证码");

            $member = Member::findOne([
                'mobile' => $username
            ]);

            // 用户不存在 自动注册
            if (!$member) {
                $seKey = createRandKey();
                // 新用户注册
                $member = new Member();
                $member->username = $member->getNewUserName();
                $member->mobile = $username;
                $member->wxnick = '用户_' . createRandKey();
                $member->nickname = $member->wxnick;
                $member->setPassword($seKey);
                $member->status = (string)$member::STATUS_ACTIVE;
                $member->save();


                Sms::init()->zhuCeHuiYuan($member->id, $seKey);
            }

            $memberForm = new MemberForm();
            if ($memberForm->wxlogin($member->id)) {
//                return  $this->redirect(['index/index']);
                apiSendSuccess("登录成功", [
                    'location' => UrlService::build(['index/index'])
                ]);
            };

            apiSendError("网络异常，请稍后重试");
        }


       return $this->render('quick-login');
    }

    /**
     *  帮助中心
     */
    public  function  actionHelp($contId=1){

        $helpType=HelpType::find()->andFilterWhere(['is_del'=>0])->orderBy('sort ASC')->all();
        $helpCont=HelpCont::findOne(['id'=>$contId]);


        if (strlen($helpCont->title)) {
            global $title;
            $title = $helpCont->title;
        }
        if (strlen($helpCont->keywords)) {
            global $keywords;
            $keywords = $helpCont->keywords;
        } 	
        if (strlen($helpCont->description)) {
            global $description;
            $description = $helpCont->description;
        }

        return $this->render('help',[
            'helpType'=>$helpType,
            'helpCont'=>$helpCont
        ]);
    }


    /**
     * 登出
     * @return yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['index/index']);
    }

    public function actionSign()
    {
        $this->memberInfo or apiSendError("用户未登录");
        if (yii::$app->request->isPost) {
            $member=Member::findOne($this->memberInfo->id);
            $isSignin= IntegralLog::find()->where("memberId=".$member->id." and DATE_FORMAT(create_time, '%Y%m%d') = DATE_FORMAT(now(), '%Y%m%d')")->count();
            $isSignin>0 and apiSendError('已签到', [
                'integral' => $member->integral
            ]);

            $model=new IntegralLog();
            $model->memberId = $member->id;
            $model->orderId = $model::ORDER_ID_LOGIN;
            $model->old_integral = $member->integral;


            $integral = ConfigService::init('INDEX_INTEGRAL')->get('sign_integral');
            $continuation = ConfigService::init('INDEX_INTEGRAL')->get('continuation');
            $model->expand = "签到获得{$integral}积分";
            $time=date('Y-m-d',strtotime("-{$continuation} day"));

            $num=  IntegralLog::find()->where("memberId=". $member->id ." and DATE_FORMAT(create_time, '%Y%m%d')>=  '".$time."'  and  DATE_FORMAT(create_time, '%Y%m%d') <= DATE_FORMAT(now(), '%Y%m%d')")->count();
            if (($num+1)>= $continuation){
                $reward = ConfigService::init('INDEX_INTEGRAL')->get('reward');
                $integral += $reward;
                $model->expand  .= "连续签到额外获得{$reward}积分";
            }

            $member->integral += $integral;
            $model->now_integral = $member->integral;
            $model->pay_integral = $model->old_integral - $model->now_integral;

            $model->create_time=date('Y-m-d H:i:s');
            if ($member->save()){

                if ($model->save()) {

                    $signLogDatas = IntegralLog::find()->where([
                        'and',
                        ['=', 'memberId', $this->memberInfo->id],
                        ['>=', 'create_time', date('Y-m-d')],
                    ])->all();
                    $signLogs = arrayGroupsAction($signLogDatas, function ($signLogData) {
                        return ['signDay' => date('m', strtotime($signLogData->create_time))];
                    });

                    apiSendSuccess("签到成功", [
                        'integral' => $member->integral,
                        'signLogs' => $signLogs
                    ]);
                }
            }
            apiSendError("签到失败");
        }

        apiSendError("签到失败");
    }

}
