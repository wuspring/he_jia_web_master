<?php
namespace index\controllers;

use common\models\GoodsCoupon;
use common\models\Member;
use common\models\MemberForm;
use common\models\News;
use common\models\Ticket;
use common\models\TicketApply;
use common\models\TicketApplyGoods;
use common\models\TicketAsk;
use common\models\TicketInfo;
use DL\Project\CityExpand;
use DL\Project\CityTicket;
use DL\Project\Sms;
use DL\service\CacheService;

use common\models\Goods;
use common\models\GoodsClass;
use common\models\StoreGcScore;
use common\models\StoreInfo;

use DL\service\UrlService;
use DL\vendor\ConfigService;
use DL\vendor\Page;
use DL\Project\Store;

use yii\helpers\ArrayHelper;

abstract class BaseTicketController extends BaseController
{
    // 票类型
    protected $formType;
    // 票分类
    protected $caType=Ticket::CA_TYPE_ZHAN_HUI;

    protected $ticketAmountKey;

    public function init()
    {
        parent::init();
    }

    protected function getSessionKey($ticketId)
    {
        session_status()==2 or session_start();

        return "{$this->formType}_{$ticketId}_" . session_id();
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    /**
     * 索票活动
     */
    public function actionAskTicket()
    {
        $post = \Yii::$app->request->post();
        $ticketId = isset($post['ticketId']) ? $post['ticketId'] : '0';

        $to = isset($post['t']) ? $post['t'] : 'index/index';


//        $uKey = $this->getSessionKey($ticketId);
//        $formData = CacheService::init()->get($uKey);
//        isset($formData['getTicketId']) and stopFlow("您已成功申请索票", $to);
//
//        $formData or $formData = $post;
        $formData = $post;
        $name = isset($post['name']) ? $post['name'] : $formData['name'];
        $mobile = isset($post['mobile']) ? $post['mobile'] : $formData['mobile'];
        $address = isset($post['address']) ? $post['address'] : '';
        $cityId = isset($post['cityId']) ? $post['cityId'] : '#';
        $type = isset($post['type']) ? $post['type'] : '#';
        $resource = isset($post['resource']) ? $post['resource'] : $formData['resource'];
        $code = isset($post['code']) ? $post['code'] : '###';

        $session = \Yii::$app->session;
//        "{$mobile}-{$code}" == $session->get('SMS_CODE') or stopFlow("请输入正确的验证码", $to);
        $smsCode = $session->get('SMS_CODE');
        $smsCode or stopFlow("验证码无效", $to);
        $smsCode['expire'] > time()  or stopFlow("验证码已过期", $to);
        "{$mobile}-{$code}" == $smsCode['value'] or stopFlow("请输入正确的验证码", $to);

        // 查询并注册手机号为新用户
        $member = Member::findOne([
            'mobile' => $mobile
        ]);

        $signMember = false;
        // 用户不存在 自动注册
        if (!$member) {
            $signMember = createRandKey();
            // 新用户注册
            $member = new Member();
            $member->username = $member->getNewUserName();
            $member->mobile = $mobile;
            $member->wxnick = '用户_' . createRandKey();
            $member->nickname = $member->wxnick;
            $member->setPassword($signMember);
            $member->status = (string)$member::STATUS_ACTIVE;
            $member->save();
        }
        // 自动登录
        $memberForm = new MemberForm();
        $memberForm->wxlogin($member->id);

        $ticketId or stopFlow("当前城市并未开展活动", $to);

        $time = time();
        $ticket = Ticket::findOne($ticketId);
        (strtotime($ticket->ask_ticket_start_date)) < $time or stopFlow("展会领票时间尚未开始");
        (strtotime($ticket->ask_ticket_end_date) + 3600 * 24) > $time or stopFlow("展会领票时间已截止");

        if (!strlen($name) or !preg_match('/^1\d{10}$/',$mobile)) {
            stopFlow("请完善收件人信息", $to);
        }

        $ticket = Ticket::findOne([
            'id' => $ticketId,
            'status' => Ticket::STATUS_NORMAL,
            'type' => $type
        ]);
        $ticket or stopFlow("该票已下线，下次快点哦", $to);

        $ticketAsk = TicketAsk::findOne([
            'ticket_id' => $ticket->id,
            'mobile' => $mobile
        ]);
        $ticketAsk and stopFlow("您已成功申请，不要贪心哦", $to);

        $ticketAsk = new TicketAsk();
        $ticketAsk->name = $name;
        $ticketAsk->member_id = $member->id;
        $ticketAsk->provinces_id = $cityId;
        $ticketAsk->ticket_id = $ticketId;
        $ticketAsk->mobile = $mobile;
        $ticketAsk->address = $address;
        $ticketAsk->resource = $resource;
        $channel=\yii::$app->session->get('channel');
        if (!empty($channel)){
            $ticketAsk->channel= $channel;
        }

        $ticketAsk->status = 0;
        $ticketAsk->create_time = date('Y-m-d H:i:s');

        if ($ticketAsk->save()) {
            $post['getTicketId'] = $ticketAsk->id;
    //        CacheService::init()->set($uKey, $post);

            $amount = TicketAsk::find()->where([
                'ticket_id' => $ticketId
            ])->sum('ticket_amount');

            $this->ticketAmountKey($ticketId, $amount);
        }

        Sms::init()->askTicktet($mobile, $ticketAsk->ticket_id);
        $signMember !== false and Sms::init()->zhuCeHuiYuan($ticketAsk->member_id, $signMember);

//        stopFlow("索票成功", $to);

        header('Location: ' . UrlService::build(['ticket-address', 'ask_id' => $ticketAsk->id]));
        die();
    }

    public function actionTicketAddress($ask_id=0)
    {
        $ask_id = (int)$ask_id;
        $ask_id or $ask_id = (int)\Yii::$app->request->post('ask_id', 0);
        $ticketAsk = TicketAsk::findOne($ask_id);
        $ticketAsk or stopFlow("未找到申请的信息", 'index');

        if (\Yii::$app->request->isPost) {
            if (\Yii::$app->user->isGuest) {
                apiSendError("您尚未登录");
            }

            $member = \Yii::$app->user->identity;
            if ($member->id != $ticketAsk->member_id) {
                apiSendError("申请信息与当前用户不符");
            }
            if ($ticketAsk->status) {
                apiSendError("该申请收货信息已确认");
            }

            $ticketAsk->address = \Yii::$app->request->post('address', '');
            $ticketAsk->save() and apiSendSuccess("保存成功");

        }

        $ticket = $ticketAsk->ticket;
        $erWeiMa = ConfigService::init('system')->get('erweima');

        switch ($ticket->type) {
            case $ticket::TYPE_JIA_ZHUANG :
                $ticketTypeName = '家博会';
                break;
            case $ticket::TYPE_JIE_HUN :
                $ticketTypeName = '婚庆会';
                break;
            case $ticket::TYPE_YUN_YING :
                $ticketTypeName = '孕婴会';
                break;
        }

        $this->render('../common/ticket_address', [
            'showPromise' => 1,
            'indexIndex' => 5,
            'weiXinHao' => 'HEJIA-7878',
            'erWeiMa' => translateAbsolutePath($erWeiMa),
            'ticketAsk' => $ticketAsk,
            'ticketTypeName' => $ticketTypeName,
            'ticket' => $ticket,
        ]);
    }

    // 获取当前城市发行票据
    protected function getTicketId()
    {
        $ticket = Ticket::findOne([
            'ca_type' => $this->caType,
            'type' => $this->formType,
            'citys' => $this->getRequestCity(),
            'status' => 1
        ]);

        return $ticket ? $ticket->id : 0;
    }

    public function ticketAmountKey($ticketId, $amount=false)
    {
        $ticketAmountKey = "AMOUNT_{$this->formType}_{$this->caType}_$ticketId";

        if ($amount === false) {
            $result = CacheService::init()->get($ticketAmountKey);

            if ($result === false) {
                $result = TicketAsk::find()->where([
                    'ticket_id' => $ticketId
                ])->sum('ticket_amount');

                CacheService::init()->set($ticketAmountKey, $amount);
            }

            return (int)$result;
        }

        CacheService::init()->set($ticketAmountKey, $amount);
    }

    /**
     *  爆品预约
     */
    public function actionYuYue()
    {
        $currentCity = $this->getRequestCity();    //城市id

        $tickets = CityTicket::init($currentCity)->currentTickets();

        $order_goods_id_all = [0];
        $allGoodsTab = [];
        $jingPin = [];
        $storageRelations = [];
        // 当前有效店铺
        $useFulStoreIds = Store::init()->usefulStoreIds($currentCity);
        if ($tickets[$this->formType]) {
            $ticket = $tickets[$this->formType];
//            ->limit(7)
            $ticketApplys = TicketApplyGoods::find()->alias('t')->leftJoin(['g' => Goods::tableName()], 't.good_id=g.id')->where([
                'and',
                ['=', 't.ticket_id', $ticket->id],
                ['=', 't.type', TicketApplyGoods::TYPE_COUPON],
                ['in', 't.user_id', $useFulStoreIds]
            ])->groupBy('g.gc_id')->orderBy('t.id desc')->all();

            $allGoodsTab =arrayGroupsAction($ticketApplys, function ($ticketApply) {
                $good =  Goods::findOne($ticketApply->good_id);
                $good->goods_price = $ticketApply->good_price;
                $good->goods_pic = $ticketApply->good_pic;
                $good->goods_storage = $ticketApply->good_amount;

                return $good;
            });

            $ticketApplyInfos = TicketApplyGoods::find()->where([
                'and',
                ['=', 'ticket_id', $ticket->id],
                ['=', 'type', TicketApplyGoods::TYPE_COUPON],
                ['in', 'user_id', $useFulStoreIds],
            ])->all();
            $order_goods_id_all = arrayGroupsAction($ticketApplyInfos, function ($ticketApplyInfo) {
                return $ticketApplyInfo->good_id;
            });

            //精品推荐->limit(6)
            $jingPin = Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 't.good_id=g.id')->andFilterWhere([
                'and',
                ['in','g.id',$order_goods_id_all],
                ['in', 'g.user_id', $useFulStoreIds],
                ['=', 't.type', TicketApplyGoods::TYPE_COUPON],
                ['=', 'g.isdelete', 0],
                ['=', 'g.goods_state', 1],
                ['=', 't.is_zhanhui', 1],
            ])->all();
            $storageRelations = ArrayHelper::map($ticketApplyInfos, 'good_id', 'good_amount');
        }

        $this->render('yu-yue', [
            'allGoodsTab'=>$allGoodsTab,
            'order_goods_id_all'=>$order_goods_id_all,
            'jingPin'=>$jingPin,
            'storageRelations' => $storageRelations,

            'type'=>$this->formType,
        ]);
    }

    /**
     *  家装采购优惠券-这里查询的是商品优惠劵
     */
    public function actionYouHui()
    {
        switch ($this->formType) {
            case Ticket::TYPE_JIA_ZHUANG :
                $goods_class_fid = 1;
                break;
            case Ticket::TYPE_JIE_HUN :
                $goods_class_fid = 2;
                break;
            case Ticket::TYPE_YUN_YING :
                $goods_class_fid = 3;
                break;
            default :
                stopFlow("数据异常");
        }
        $currentCity = $this->getRequestCity();    //城市id
        $jiaZhuang = GoodsClass::find()->andFilterWhere(['fid' =>$goods_class_fid])->asArray()->all();
        $arr = ArrayHelper::getColumn($jiaZhuang, 'id');

        // 当前有效店铺
        $useFulStoreIds = Store::init()->usefulStoreIds($currentCity);

        //查询类
        $sql = StoreGcScore::find()->where(
            ['in', 'user_id', $useFulStoreIds]
        )->andFilterWhere(['provinces_id' => $currentCity])->andFilterWhere(['in', 'gc_id', $arr]);
        $store_class = $sql->groupBy('gc_id')->limit(7)->all();

        //精品推荐
        $query = GoodsCoupon::find()->alias('gc');
        $recommendCoupon = $query->leftJoin('user as user', 'user.id = gc.user_id')
            ->andFilterWhere(['user.provinces_id' => $currentCity])->where(['gc.is_del' => 0,'gc.is_show'=>1])
            ->andFilterWhere(['in', 'user.id', $useFulStoreIds])
//            ->groupBy('gc.user_id')->limit(6)->all();
            ->all();

        $this->render('../common/you-hui',
            [
                'store_class' => $store_class,
                'recommendCoupon' => $recommendCoupon
            ]);
    }


    /**
     * 精选店铺-本地区的
     */
    public function actionJingXuan($gc_id=0)
    {
        $currentCity = $this->getRequestCity();      //城市id

        switch ($this->formType) {
            case Ticket::TYPE_JIA_ZHUANG :
                $goods_class_fid = 1;
                break;
            case Ticket::TYPE_JIE_HUN :
                $goods_class_fid = 2;
                break;
            case Ticket::TYPE_YUN_YING :
                $goods_class_fid = 3;
                break;
            default :
                stopFlow("数据异常");
        }
        //获取当前城市 分类的类型
        $goodClass=GoodsClass::findAll(['fid'=>$goods_class_fid]);
        $goodClassIdArr=ArrayHelper::getColumn($goodClass,'id');

        $useFulStoreIds = Store::init()->usefulStoreIds($currentCity);
        //筛选当前城市拥有的家装采购类型类型
        $query=StoreGcScore::find()->andFilterWhere(
            [
                'and',
                ['in','gc_id',$goodClassIdArr],
                ['in', 'user_id', $useFulStoreIds]
            ]
        )->andFilterWhere(['provinces_id'=>$currentCity]);

        $jiaZhuangType=$query->groupBy('gc_id')->all();
        $limit = 10;

        $amount=$query->andFilterWhere(['gc_id'=>($gc_id==0)?'':$gc_id])->groupBy('user_id')->count();
        $page = new Page($amount, $limit);

        $stores =$query->andFilterWhere(['gc_id'=>($gc_id==0)?'':$gc_id])->groupBy('user_id')->offset($page->firstRow)->limit($limit)->all();

        $useType = $this->formType;
        $model = arrayGroupsAction($stores, function ($store) use ($useType) {
            $store = Store::init()->info($store->user_id);

            $goods = [];
            $ticketInfos = CityTicket::init($store->provinces_id)->currentTickets();

            if (isset($ticketInfos[$useType])) {
                $ticketInfo = $ticketInfos[$useType];

                $orderGoods = (array)TicketApplyGoods::find()->where([
                    'user_id' => $store->user_id,
                    'ticket_id' => $ticketInfo->id,
//                    'type' => TicketApplyGoods::TYPE_ORDER
                ])->orderBy('type desc, sort desc')->all();
            }

            if ($orderGoods) {
                $goods = arrayGroupsAction($orderGoods, function ($apply) use ($ticketInfo) {
                    $good = $apply->goods;

                    $good->ticket_price = $ticketInfo->order_price;
                    $good->goods_price = $apply->good_price;
                    $good->goods_pic = $apply->good_pic;
                    $good->goods_storage = $apply->good_amount;

                    return $good;
                });
            }

            $store->order_goods = array_values($goods);
            return $store;
        });


        $baseStoreTags = [];
        $cityInfo = CityExpand::init()->get($currentCity, CityExpand::TYPE_CITY_EXPAND);
        if (isset($cityInfo->tag)) {
            $baseStoreTags = call_user_func(function($str) {
                $tabDatas = json_decode($str, true);
                return $tabDatas ? arrayGroupsAction($tabDatas, function ($str) {
                    list($tab, $info) = explode('@', $str);

                    return ['tab' => $tab, 'info' => $info];
                }) : [];
            }, $cityInfo->tag);
        }

        $pageHtml = $page->show();
        $this->render('../common/jing-xuan', [
            'model' => $model,
            'jiaZhuangType'=>$jiaZhuangType,
            'pageHtml'=>$pageHtml,
            'formType'=>$this->formType,
            'gc_id'=>$gc_id,
            'baseStoreTags' => $baseStoreTags
        ]);
    }

    /**
     *  参展品牌
     */
    public  function actionCanZhan($gc_id=0,$goods_class_fid=1)
    {
        $currentCity = $this->getRequestCity();    //城市id

        //获取参展的商户id数组
        $ticket=CityTicket::init($currentCity)->getTicket($this->formType,Ticket::CA_TYPE_ZHAN_HUI);
        $ticket_id=($ticket)?$ticket->id:0;
        $ticketApply=TicketApply::find()->andFilterWhere(['ticket_id'=>$ticket_id,'status'=>1])->all();
        $attendUserArr=ArrayHelper::getColumn($ticketApply,'user_id');
        if (empty($attendUserArr)){
            $attendUserArr=[0];
        }

        //获取当前城市 分类的类型
        $goodClass=GoodsClass::findAll(['fid'=>$goods_class_fid]);
        $goodClassIdArr=ArrayHelper::getColumn($goodClass,'id');
        //筛选当前城市拥有的家装采购类型类型
        $query=StoreGcScore::find()->andFilterWhere(['in','gc_id',$goodClassIdArr])
            ->andFilterWhere(['provinces_id'=>$currentCity])
            ->andFilterWhere(['in','user_id',$attendUserArr]);

        $jiaZhuangType=$query->groupBy('gc_id')->all();

        $amount=$query->andFilterWhere(['gc_id'=>($gc_id==0)?'':$gc_id])->count();
        $page = \Yii::$app->request->get('p', 1);
        $limit = 10;

        $stores = $query->andFilterWhere(['gc_id'=>($gc_id==0)?'':$gc_id])->groupBy('user_id')->offset(($page-1)* $limit)->limit($limit)->all();

        $model = arrayGroupsAction($stores, function ($store) {
            return Store::init()->info($store->user_id);
        });
        $page = new Page($amount, $limit);
        $pageHtml = $page->show();

        $baseStoreTags = [];
        $cityInfo = CityExpand::init()->get($currentCity, CityExpand::TYPE_CITY_EXPAND);
        if (isset($cityInfo->tag)) {
            $baseStoreTags = call_user_func(function($str) {
                $tabDatas = json_decode($str, true);
                return $tabDatas ? arrayGroupsAction($tabDatas, function ($str) {
                    list($tab, $info) = explode('@', $str);

                    return ['tab' => $tab, 'info' => $info];
                }) : [];
            }, $cityInfo->tag);
        }

        $this->render('../common/can-zhan', [
            'model' => $model,
            'jiaZhuangType'=>$jiaZhuangType,
            'pageHtml'=>$pageHtml,
            'gc_id'=>$gc_id,
            'formType' => $this->formType,
            'baseStoreTags' => $baseStoreTags
        ]);
    }

    /**
     * 品牌活动
     */
    public function actionBrandActivity($ticketId = 0)
    {
        $currentCity = $this->getRequestCity();    //城市id

        // 当前有效店铺
        $useFulStoreIds = Store::init()->usefulStoreIds($currentCity);

        $query=News::find()->andFilterWhere(['type'=>$this->formType,'cid'=>0,'isShow'=>1])
                           ->andFilterWhere(['like','provinces_id',$currentCity]);
        $amount=$query->count();
        $page = \Yii::$app->request->get('p', 1);
        $limit = 3;
        $newsAll=$query->offset(($page-1)* $limit)->limit($limit)->all();
        $page = new Page($amount, $limit);
        $pageHtml = $page->show();

        $tAmount = 0;
//        $ticketId = $this->getTicketId();
        if ($ticketId) {
            $tAmount = $this->ticketAmountKey($ticketId);
            $ticket = Ticket::findOne($ticketId);
            $tAmount=$tAmount+$ticket->virtual_number;
        }

        $uKey = $this->getSessionKey($ticketId);
        $formData = CacheService::init()->get($uKey);
        $formData or $formData = [];

        $formData = array_merge($formData, [
            'amount' => $tAmount,
            'ticketId' => $ticketId,
            'cityId' => $currentCity
        ]);

        switch ($this->formType) {
            case Ticket::TYPE_JIA_ZHUANG :
                $goods_class_fid = 1;
                break;
            case Ticket::TYPE_JIE_HUN :
                $goods_class_fid = 2;
                break;
            case Ticket::TYPE_YUN_YING :
                $goods_class_fid = 3;
                break;
            default :
                stopFlow("数据异常");
        }
        $goodClass = GoodsClass::findAll(['fid'=>$goods_class_fid]);
        $gcIds = ArrayHelper::getColumn($goodClass,'id');
        $gcIds or $gcIds =[0];

        $stores = StoreGcScore::find()->where([
            'and',
            ['in', 'gc_id', $gcIds],
            ['in', 'user_id', $useFulStoreIds]
        ])->orderBy('score desc')->groupBy('user_id')->limit(10)->all();
        $stores = arrayGroupsAction($stores, function ($store) {
            return Store::init()->info($store->user_id);
        });

        $this->render('../common/brand-activity',[
            'newsAll'=>$newsAll,
            'formData' => $formData,
            'stores' => $stores,
            'pageHtml'=>$pageHtml,
            'ticketId' => (int)$ticketId
        ]);
    }

    public function render($view, $params = [])
    {
        $resourceDics = ConfigService::init(ConfigService::EXPAND)->get('resource');
        $resourceDics = $resourceDics ? explode(',', $resourceDics) :  [];

        switch ($this->formType) {
            case Ticket::TYPE_JIA_ZHUANG :
                $basePathData = ['path' => 'jia-zhuang', 'info' => '家装采购'];
                break;
            case Ticket::TYPE_JIE_HUN :
                $basePathData = ['path' => 'jie-hun', 'info' => '结婚采购'];
                break;
            case Ticket::TYPE_YUN_YING :
                $basePathData = ['path' => 'yun-ying', 'info' => '孕婴采购'];
                break;
            default :
                $basePathData = [];
        }

        parent::render($view, array_merge([
            'type' => $this->formType,
            'resourceDics' => $resourceDics,
            'basePathData' => $basePathData,
        ], $params));
    }
}
