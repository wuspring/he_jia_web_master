<?php
namespace index\controllers;

use common\models\IntergralArticle;
use common\search\IntergralGoodsSearch;
use common\models\Goods;
use common\models\GoodsClass;
use common\models\GoodsCoupon;
use common\models\StoreGcScore;
use common\models\Ticket;
use common\models\TicketApply;
use common\models\TicketAsk;
use common\models\TicketInfo;
use DL\Project\AskTicket;
use DL\Project\FloorConfig;
use DL\service\CacheService;
use DL\vendor\ConfigService;
use \DL\vendor\Page;
use yii\helpers\ArrayHelper;

class IntegralNewsController extends BaseController
{
    /**
     * 积分首页
     */
    public function actionIndex($id=0)
    {
        $news = IntergralArticle::findOne($id);
        $news or stopFlow("积分资讯不存在");

        $hotNews = IntergralArticle::find()->where([
            'is_del' => 0,
        ])->orderBy('id desc')->limit(20)->all();
        $relationNews = IntergralArticle::find()->where([
            'is_show' => 1,
            'is_del' => 0,
        ])->orderBy('id desc')->limit(6)->all();

        if (strlen($news->title)) {
            global $title;
            $title = $news->title;
        }
        if (strlen($news->seokey)) {
            global $keywords;
            $keywords = $news->seokey;
        }
        if (strlen($news->seoDepict)) {
            global $description;
            $description = $news->seoDepict;
        }
        
        return $this->render('index', [
            'news' => $news,
            'hotNews' => $hotNews,
            'relationNews' => $relationNews,
            'pageIndexKey'=> 9
        ]);
    }

}
