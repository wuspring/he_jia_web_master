<?php
namespace index\controllers;

use common\models\FloorIndex;
use common\models\Goods;
use common\models\GoodsClass;
use common\models\GoodsCoupon;
use common\models\StoreGcScore;
use common\models\Ticket;
use common\models\TicketApply;
use common\models\TicketApplyGoods;
use common\models\TicketAsk;
use common\models\TicketInfo;
use DL\Project\AskTicket;
use DL\Project\CityTicket;
use DL\Project\FloorConfig;
use DL\Project\Page;
use DL\Project\Store;
use DL\service\CacheService;
use DL\vendor\ConfigService;
use yii\helpers\ArrayHelper;
use yii\web\User;

class JiaZhuangController extends BaseTicketController
{
    protected $formType = Ticket::TYPE_JIA_ZHUANG;

    public function actionIndex($ticketId = 0)
    {
        $ticketId or $ticketId = $this->getTicketId();

        $cityId = $this->getRequestCity();

        $amount = 0;
        $ticket = false;

        $joinTicketStoreIds = [0];
        if ($ticketId) {
            $amount = $this->ticketAmountKey($ticketId);
            $ticket = Ticket::findOne($ticketId);
            $amount=$amount+$ticket->virtual_number;
	        if (strlen($ticket->name)) {
	            global $title;
	            $title = $ticket->name;
	        }
	        if (strlen($ticket->keywords)) {
	            global $keywords;
	            $keywords = $ticket->keywords;
	        }
	        if (strlen($ticket->description)) {
	            global $description;
	            $description = $ticket->description;
	        }

            $storeApplys = TicketApply::findAll([
                'ticket_id' => $ticket->id,
                'status' => 1
            ]);



            if ($storeApplys) {
                $uStoreIds = Store::init()->usefulStoreIds();
                $joinTicketStoreIds = arrayGroupsAction($storeApplys, function ($storeApply) {
                    return $storeApply->user_id;
                });

                $joinTicketStoreIds = array_intersect($joinTicketStoreIds, $uStoreIds);
                $joinTicketStoreIds or $joinTicketStoreIds = [0];
            }
        }

        $uKey = $this->getSessionKey($ticketId);
        $formData = CacheService::init()->get($uKey);
        $formData or $formData = [];

        $formData = array_merge($formData, [
            'amount' => $amount,
            'ticketId' => $ticketId,
            'cityId' => $cityId
        ]);

        $banner = AskTicket::init()->get($this->getRequestCity(), Ticket::TYPE_JIA_ZHUANG)->banner;
        $zhanhuiPic = AskTicket::init()->get($this->getRequestCity(), Ticket::TYPE_JIA_ZHUANG)->zhanhui_pic;

        //参展商品
        $goods = TicketApplyGoods::find()->andFilterWhere(['ticket_id'=>$ticketId])->all();
        $goodsIds=ArrayHelper::getColumn($goods,'good_id');

        if (empty($goodsIds)){
            $goodsIds = [0];
        }

        $goodCoupon = [];
        if ($ticket) {
            //品牌优惠劵
            $goodCoupon=GoodsCoupon::find()->andFilterWhere([
                'and',
                ['in', 'user_id', $joinTicketStoreIds],
                ['>', 'num', 0],
//                ['=', 'is_show', 1],
                ['=', 'is_del', 0],
            ])->limit($ticket->couple_nums)->all();
        }

        //特价
         $teJia_good=Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 'g.id=t.good_id')->andFilterWhere(['t.ticket_id'=>$ticketId,'t.type'=>TicketApplyGoods::TYPE_ORDER, 'is_zhanhui' => 1])->select(['g.*', 't.good_amount'])->orderBy(' t.sort desc')->all();

        //爆款预约
        $baoKuan_good=Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 'g.id=t.good_id')->andFilterWhere(['t.ticket_id'=>$ticketId,'t.type'=>TicketApplyGoods::TYPE_COUPON, 'is_zhanhui' => 1])->select(['g.*', 't.good_amount'])->orderBy(' t.sort desc')->all();

        //家装类型
        $goodClass=GoodsClass::find()->andFilterWhere(['fid'=>1])->limit(7)->orderBy("id ASC")->all();
        //获取合作品牌
        $jieHunType=ArrayHelper::getColumn($goodClass,'id');
        $joinStore_gc=[];
        $storeGcScore=StoreGcScore::find()->select('*, `score` * `admin_score` as `a_score`')->andFilterWhere([
            'and',
            ['in','gc_id',$jieHunType],
            ['=', 'provinces_id',$cityId],
            ['in', 'user_id', $joinTicketStoreIds]
        ])->orderBy('a_score desc')->all();
        foreach ($storeGcScore as $store){
            $joinStore_gc[$store->gc_id][] = Store::init()->info($store->user_id);
        }

        $iConfig = Page::init()->get($cityId, Page::PAGE_INDEX);

        $configIndex = [
            'smap' => isset($iConfig->smap) ? $iConfig->smap : '',
            'promise' => Page::init()->get($cityId, Page::PAGE_INDEX)->promise,
            'banner' => Page::init()->get($cityId, Page::PAGE_CAI_GOU_JIAZHUANG)->banner,
            'huodongs' => Page::init()->get($cityId, Page::PAGE_CAI_GOU_JIAZHUANG)->huodongs
        ];

        $this->render('index', [
            'indexIndex' => 5,
            'showPromise' => 1,
            'configIndex' => $configIndex,
            'formData' => $formData,
            'zhanhuiPic' => $zhanhuiPic,
            'name' => '家博会',
            'ticket' => $ticket,
            'banner' => $banner,
            'formType'=>$this->formType,
            'goodCoupon'=>$goodCoupon,
            'teJia_good'=>$teJia_good,
            'baoKuan_good'=>$baoKuan_good,
            'goodClass1'=>$goodClass,
            'joinStore_gc'=>$joinStore_gc
        ]);
    }

    public function actionIndex2($ticketId = 0)
    {
        global $cityId;
        $cityId = $this->getRequestCity();
        $configIndex = [
            'banner' => Page::init()->get($cityId, Page::PAGE_CAI_GOU_JIAZHUANG)->banner,
            'huodongs' => Page::init()->get($cityId, Page::PAGE_CAI_GOU_JIAZHUANG)->huodongs
        ];

        $gcs = GoodsClass::findAll([
            'fid' => 1,
            'is_del'=>0,
            'is_show'=>0
        ]);

        $gcDatas = arrayGroupsAction($gcs, function (GoodsClass $gc) {
            global $cityId;
//            "select * from `store_gc_score` where `provinces_id`='{$cityId}' and `gc_id`='{$gc->id}' order by "
//           $commendStores = StoreGcScore::find()->where([
//               'provinces_id' => $cityId,
//               'gc_id' => $gc->id
//           ])->select('*, `score` * `admin_score` as `a_score`')->orderBy('a_score desc')->limit(6)->all();
            $brand = FloorConfig::init()->get($cityId, $gc->id)->brand;
            $commendStores = array_slice($brand, 0, 6);
            $pics = FloorConfig::init()->get($cityId, $gc->id)->zh_img;
            $zhFirstPics = FloorConfig::init()->get($cityId, $gc->id)->zh_first_imgs;


           return [
               'info' => $gc,
               'commends' => $commendStores,
               'pics' => $pics,
               'zhFirstPics' => $zhFirstPics,
               'indexIndex' => 5
           ];
        });

        $ticketId or $ticketId = $this->getTicketId();
        $uKey = $this->getSessionKey($ticketId);
        $formData = CacheService::init()->get($uKey);
        $formData or $formData = [];
        $amount = $this->ticketAmountKey($ticketId);
        $ticket = Ticket::findOne($ticketId);
        $amount=$amount+$ticket->virtual_number;
        $formData = array_merge($formData, [
            'amount' => $amount,
            'ticketId' => $ticketId,
            'cityId' => $cityId
        ]);

        $this->render('index2', [
            'configIndex' => $configIndex,
            'cityId' => $cityId,
            'gcDatas' => $gcDatas,
            'formData' => $formData,
            'basePathData' => ['path' => 'jia-zhuang', 'info' => '家装采购'],
            'indexIndex' => 2
        ]);
    }
}
