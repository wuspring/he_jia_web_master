<?php

namespace index\controllers;

use common\models\Address;
use common\models\Cart;
use common\models\Goods;
use common\models\GoodsSku;
use common\models\IntergralGoods;
use common\models\IntergralGoodsSku;
use common\models\IntergralOrder;
use common\models\IntergralOrderGoods;
use common\models\Order;
use common\models\OrderGoods;
use DL\Project\Credit;
use DL\service\UrlService;
use DL\vendor\ConfigService;
use yii;
use yii\helpers\Url;

class IntegralCartController extends BaseController
{
    public function init()
    {
        parent::init();

        empty($this->memberInfo) and apiSendError("用户未登录");
    }

    /**
     * 购物车列表
     */
    public function actionIndex()
    {
        $carts = Cart::findAll([
            'buyerId' => (int)$this->memberInfo->id,
//            'goods_type' => Cart::TYPE_INTERGRAL,
        ]);
        $amount = Cart::find()->where([
            'buyerId' => $this->memberInfo->id,
//            'goods_type' => Cart::TYPE_INTERGRAL,
            'ispay' => 1
        ])->count();

        $totalPrice = Cart::find()->where([
            'buyerId' => $this->memberInfo->id,
//            'goods_type' => Cart::TYPE_INTERGRAL,
            'ispay' => 1
        ])->sum('totalPrice');
        $totalPrice or $totalPrice = 0;

        $intergralGoods = IntergralGoods::find()->where([
            'goods_state' => '1',
            'isdelete' => '0',
            'goods_hot' => '1'
        ])->orderBy('id desc')->limit(5)->all();

        $this->render('index', [
            'carts' => $carts,
            'amount' => $amount,
            'totalPrice' => $totalPrice,
            'intergralGoods' => $intergralGoods,

            'pageIndexKey'=>9
        ]);
    }

    /**
     * 添加
     */
    public function actionAdd()
    {
        $id = (int)\Yii::$app->request->get('id');
        $amount = (int)\Yii::$app->request->get('amount');
        $skuId = (int)\Yii::$app->request->get('skuId');

        if ($id and $amount) {
            $goods = IntergralGoods::findOne([
                'id' => $id,
                'isdelete' => 0
            ]);
            $goods or apiSendError("商品已下架");


            $goodsImg = $goods->goods_pic;
            $attrInfo = [];
            $price = $goods->goods_integral;

            // 验证 SKU 正确性
            if ($skuId) {
                $sku = IntergralGoodsSku::findOne($skuId);
                $sku or apiSendError("商品信息有误，请稍后重试");

                if ($sku->num < $amount) {
                    apiSendError("库存不足");
                }

                $goodsImg = $sku->picimg;
                $attrInfo = json_decode($sku->attr, true);
                $price = $sku->integral;
            }

            $cart = Cart::findOne([
                'goodsId' => $id,
                'buyerId' => $this->memberInfo->id,
                'sku_id' => $skuId
            ]);

            if (!$cart) {
                $cart = new Cart();
                $cart->buyerId = (int)$this->memberInfo->id;
                $cart->goodsId = (int)$goods->id;
                $cart->goodsName = $goods->goods_name;
                $cart->goodsPrice = $price;
                $cart->goods_image = $goodsImg;
                $cart->attr = json_encode($attrInfo);
                $cart->sku_id = $skuId;
                $cart->ispay = 1;
            }

            $cart->goodsNum = $amount;
            $cart->totalPrice = ($cart->goodsNum * $cart->goodsPrice);
            $cart->goodsIntegral = 0;


            $cart->save() and apiSendSuccess();
        } else {
            $cart = Cart::findOne([
                'goodsId' => $id,
                'buyerId' => $this->memberInfo->id,
                'sku_id' => $skuId
            ]);

            if ($cart) {
                $this->actionDelete($cart->id);
            }
        }

        apiSendError("添加购物车失败");
    }

    /**
     * 移除
     */
    public function actionDelete($id=0)
    {
        $id or $id = (int)\Yii::$app->request->get('id');

        if ($id) {
            $cart = Cart::findOne([
                'id' => $id,
                'buyerId' => (int)$this->memberInfo->id
            ]);

            if ($cart) {
                $cart->delete() and apiSendSuccess();
            }
        }

        apiSendError("删除购物车物品失败");
    }


    /**
     * 购物车内容列表
     */
    public function actionInfo()
    {
        $cartsNumber = Cart::find()->where([
            'buyerId' => $this->memberInfo->id,
//            'goods_type' => Cart::TYPE_INTERGRAL,
            'ispay' => 1
        ])->count();

        $totalPrice = Cart::find()->where([
            'buyerId' => $this->memberInfo->id,
//            'goods_type' => Cart::TYPE_INTERGRAL,
            'ispay' => 1
        ])->sum('totalPrice');
        $totalPrice or $totalPrice = 0;

        $carts = Cart::find()->where([
            'buyerId' => $this->memberInfo->id,
            'ispay' => 1,
//            'goods_type' => Cart::TYPE_INTERGRAL
        ])->offset(0)->limit(8)->orderBy('id desc')->all();

        $cartLists = [];
        foreach ($carts AS $cart) {
            $data = $cart->attributes;
//            $data['detailInfo'] = $cart->detail->attributes;
            $data['attrInfo'] = $cart->attrInfo;
            $cartLists[] = $data;
        }

        apiSendSuccess("ok", [
            'amount' => $cartsNumber,
            'totalPrice' => $totalPrice,
            'list' => $cartLists
        ]);
    }

    /**
     * 设置选中状态
     */
    public function actionCheck()
    {
        $status = (int)Yii::$app->request->post('status', '0');
        $ids = Yii::$app->request->post('ids', '');

        $filter = [
            'and',
            ['=', 'buyerId', $this->memberInfo->id],
//            ['=', 'goods_type', Cart::TYPE_INTERGRAL],
        ];
        if (strlen($ids)) {
            $ids = arrayGroupsAction(explode(',', $ids), function ($id) {
                return (int)trim($id);
            });
            $ids = array_unique($ids);

            empty($ids) and apiSendError("请选择相应的商品");
            $filter = array_merge($filter, [
                ['in', 'id', $ids]
            ]);
        }

        $checked = $status > 0 ? 1 : 0;
        $carts = Cart::find()->where($filter)->all();

        foreach ($carts AS $cart) {
            $cart->ispay = $checked;
            $cart->save();
        }

        apiSendSuccess("ok");

    }

    /**
     * create
     * 创建购物车订单
     */
    public function actionCreateOrder()
    {
        $get = \Yii::$app->request->get();

        isset($get['type']) or $get['type'] = 'price';

        $carts = Cart::findAll([
            'buyerId' => \Yii::$app->user->id,
        ]);

        $scOrder  = new IntergralOrder();
        $scOrder->orderid = $scOrder->getNewNumber();

        $price = Cart::find()->where([
            'buyerId' => $this->memberInfo->id,
            'ispay' => 1
        ])->sum('totalPrice');
        foreach ($carts AS $cart) {
            if (((int)$cart->ispay)) {
//                $cart->delete();
            }
        }

        $scOrder->goods_amount = $price;
        $scOrder->order_amount = $price;
        $scOrder->integral_amount = $price;

        // 默认不使用优惠券
        $scOrder->buyer_id = (int)$this->memberInfo->id;
        $scOrder->buyer_name = $this->memberInfo->nickname;

        $scOrder->order_state = $scOrder::STATUS_WAIT_PAY;

        $scOrder->receiver = 0;
        $scOrder->add_time = date('Y-m-d H:i:s');

        $defaultAddress = Address::findOne([
            'memberId' => $this->memberInfo->id,
            'isDefault' => 1,
            'is_delete' => 0
        ]);
        if ($defaultAddress) {
            $scOrder->receiver = $defaultAddress->id;
            $scOrder->receiver_name = $defaultAddress->name;
            $scOrder->receiver_mobile = $defaultAddress->mobile;
            $scOrder->receiver_address = $defaultAddress->provinceInfo . ' ' . $defaultAddress->address;
        } else {
            $scOrder->receiver = 0;
        }

        $scOrder->order_state = $scOrder::STATUS_WAIT_PAY;

        if ($scOrder->save()) {
            // 创建商品快照
            foreach ($carts AS $cart) {
                $orderPhoto = new IntergralOrderGoods();
                $orderPhoto->order_id = $scOrder->orderid;
                $orderPhoto->goods_id = $cart->goodsId;
                $orderPhoto->goods_pic = $cart->goods_image;
                $orderPhoto->buyer_id = $this->memberInfo->id;
                $orderPhoto->goods_name = $cart->goodsName;
                $orderPhoto->goods_price = $cart->goodsPrice;

                $orderPhoto->goods_integral = 0;
                $orderPhoto->fallinto_state = 0;
                $orderPhoto->goods_num = $cart->goodsNum;
                $orderPhoto->fallInto = $cart->totalPrice;
                $orderPhoto->goods_pay_price = $cart->totalPrice;
                $orderPhoto->createTime = date('Y-m-d H:i:s');
                $orderPhoto->sku_id = $cart->sku_id;
                $orderPhoto->attr = $cart->attrInfo;
                $orderPhoto->modifyTime = date('Y-m-d H:i:s');

                $orderPhoto->save();
            }

            header("Location: " . UrlService::build(['integral-order/confirm', 'number' => $scOrder->orderid]));
        }

        die("创建订单失败，请重试");
    }

    public function actionCartInfo()
    {
        Yii::$app->user->isGuest and apiSendError("您尚未登录");

        $amount = Cart::find()->where([
            'buyerId' => Yii::$app->user->id,
            'ispay' => 1
        ])->count();

        apiSendSuccess("ok", [
            'amount' => $amount
        ]);
    }
}
