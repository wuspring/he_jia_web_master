<?php

namespace index\controllers;


use common\models\GoodsClass;
use common\models\GoodsCoupon;
use common\models\MemberStoreCoupon;
use common\models\StoreGcScore;
use common\models\Ticket;
use yii\helpers\ArrayHelper;
use yii\web\User;

class YouHuiController extends BaseTicketController
{

    protected $formType = Ticket::TYPE_JIA_ZHUANG;


    /**
     * 判断是否领取优惠劵
     */
    public function actionGetCoupon($goodId_storeId)
    {
        $json = array('code' => 0, 'msg' => '领取失败');

        if (!empty($goodId_storeId)){
            $gcArr = explode('-', $goodId_storeId);
            $memberStore = MemberStoreCoupon::findOne(['coupon_id' => $gcArr[0],'member_id'=>$this->memberInfo->id]);
            //查询优惠劵
            $good_coupon=GoodsCoupon::findOne(['id'=>$gcArr[0]]);
            if (!$memberStore) {
                $addCoupon = new MemberStoreCoupon();
                $addCoupon->store_id = $gcArr[1];
                $addCoupon->member_id = ($this->memberInfo) ? $this->memberInfo->id : '';
                $addCoupon->coupon_id = $gcArr[0];
                $addCoupon->type = 2;
                $addDay='+'.$good_coupon->valid_day.' day';
                $endTime=date('Y-m-d H:i:s',strtotime($addDay));
                $addCoupon->addtime = date('Y-m-d H:i:s');  //$good_coupon
                $addCoupon->begin_time = date('Y-m-d H:i:s');  //开始时间
                $addCoupon->end_time = $endTime;  //结束时间
               // $addCoupon->ticket_id = 6;  //票据id
                $addCoupon->good_id = $good_coupon->goods_id;  //商品id
               // $addCoupon->good_price =6 ;    //商品活动价格

                if ($addCoupon->create()) {
                    GoodsCoupon::addNum($gcArr[0]);
                    $json = array('code' => 1, 'msg' => '领取成功');
                } else {
                    $json = array('code' => 0, 'msg' => '已经失败');
                }
            } else {
                $json = array('code' => 0, 'msg' => '已经领取');
            }
        }

        return json_encode($json);
    }


}
