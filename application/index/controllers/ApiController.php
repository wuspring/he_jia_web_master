<?php
/**
 * Created by PhpStorm.
 * User: 何孝林
 * Date: 2018/7/10
 * Time: 16:02
 */

namespace index\controllers;
use common\models\Goods;
use common\models\GoodsCoupon;
use common\models\Member;
use common\models\MemberCoupon;
use common\models\MemberForm;
use common\models\MemberStoreCoupon;
use common\models\MemberTicketStore;
use common\models\Order;
use common\models\OrderGoods;
use common\models\SentSms;
use common\models\Sms;
use common\models\Ticket;
use common\models\TicketApply;
use common\models\TicketApplyGoods;
use common\models\TicketAsk;
use common\models\User;
use DL\Project\CityExpand;
use DL\Project\CityTicket;
use DL\Project\Store;
use DL\service\GoodsService;
use DL\service\UrlService;
use DL\vendor\ConfigService;
use DL\vendor\SignatureHelper;
use Yii;
use yii\web\Controller;
use DL\Project\Sms AS SmsService;

class ApiController extends BaseController
{
    public function actionGetCode()
    {
        $mobile = Yii::$app->request->post('mobile');
        preg_match('/^1\d{10}$/', $mobile) or apiSendError("请输入正确的手机号");

        $code = createRandKey(6);

        try {
            if ($this->memberInfo) {
                $smsModel = new Sms();
                $smsModel->user_id = $this->memberInfo->id;
                $smsModel->phone = $mobile;
                $smsModel->content = $code;
                $smsModel->type = 'sms_code';
                $smsModel->create_time = date('Y-m-d H:i:s');
                $smsModel->save();
            }

            $session = \Yii::$app->session;
            $session->set('SMS_CODE', [
                'value' => "{$mobile}-{$code}",
                'expire' => time() + 60 * 30   // 有效期30分钟
            ]);
            DL_DEBUG and apiSendSuccess($code);
            $cityId = $this->getRequestCity();
            $type = Yii::$app->request->get('type', 'code');
            switch ($type) {
                case 'map' :
                    $ticketId = Yii::$app->request->get('ticket', '0');
                    \DL\Project\Sms::init()->diZhiLan($mobile, $ticketId);
                    break;
                case 'code' :
                default :
                    \DL\Project\Sms::init()->code($mobile, $code, $cityId) and apiSendSuccess("OK");
                    break;
            }

            apiSendError("获取短信验证码失败");
        } catch (\Exception $e) {
            apiSendError("获取短信验证码失败");
        }
    }

    public function actionGetTicketBySign()
    {
        $mobile = Yii::$app->request->post('mobile', '');
        $code = Yii::$app->request->post('code', '###');

        $session = \Yii::$app->session;
//        "{$mobile}-{$code}" == $session->get('SMS_CODE') or stopFlow("请输入正确的验证码", 'index/index');
        $smsCode = $session->get('SMS_CODE');
        $smsCode or stopFlow("验证码无效", 'index/index');
        $smsCode['expire'] > time()  or stopFlow("验证码已过期", 'index/index');
        "{$mobile}-{$code}" == $smsCode['value'] or stopFlow("请输入正确的验证码", 'index/index');

        // 查询并注册手机号为新用户
        $member = Member::findOne([
            'mobile' => $mobile
        ]);

        $initMember = false;
        // 用户不存在 自动注册
        if (!$member) {
            $newPwd = createRandKey();
            // 新用户注册
            $member = new Member();
            $member->username = $member->getNewUserName();
            $member->mobile = $mobile;
            $member->wxnick = '用户_' . createRandKey();
            $member->nickname = $member->wxnick;
            $member->setPassword($newPwd);
            $member->status = (string)$member::STATUS_ACTIVE;
            $member->save($newPwd);
            if(!empty($session->get('channel'))){
                $member->channel = $session->get('channel');
            }
            $initMember = $newPwd;
            $session->set('INIT_PWD', $newPwd);
        }

        // 自动登录
        $memberForm = new MemberForm();
        $memberForm->wxlogin($member->id);

        $cityId = (int)Yii::$app->request->post('cityId', 0);
        if ($cityId) {
            $type = Yii::$app->request->post('type', '###');
            $tickets = CityTicket::init($cityId)->currentTickets($type);
            if (isset($tickets[$type]) and $tickets[$type]) {
                $ticketAsk = TicketAsk::findOne([
                    'mobile' => $member->mobile,
                    'ticket_id' => $tickets[$type]->id
                ]);

                if (!$ticketAsk) {
                    $ticketAsk = new TicketAsk();
                    $ticketAsk->name = Yii::$app->request->post('name', $member->nickname);
                    $ticketAsk->provinces_id = $cityId;
                    $ticketAsk->ticket_id = $tickets[$type]->id;
                    $ticketAsk->ticket_amount = 1;
                    $ticketAsk->mobile = $member->mobile;
                    $ticketAsk->resource = '默认';
                    $ticketAsk->address = Yii::$app->request->post('address', '');
                    $ticketAsk->status = 0;
                    $ticketAsk->create_time = date('Y-m-d H:i:s');
                    $ticketAsk->save();
                }
            }
        }

        $href = Yii::$app->request->post('location', '');
        if (strlen($href)) {
            if ($initMember) {
                if (strpos($href, '?') > -1 ) {
                    $href .= "&new_member=1";
                } else {
                    $href .= "?new_member=1";
                }
            }

            $url = urldecode($href);
            header("Location: {$url}");
        }

        die();
    }

    public function actionGetTickets()
    {
        $mobile = Yii::$app->request->post('mobile', '');
        $code = Yii::$app->request->post('code', '###');

        $session = \Yii::$app->session;
//        "{$mobile}-{$code}" == $session->get('SMS_CODE') or stopFlow("请输入正确的验证码", 'index/index');
        $smsCode = $session->get('SMS_CODE');
        $smsCode or apiSendError("验证码无效");
        $smsCode['expire'] > time()  or apiSendError("验证码已过期");
        "{$mobile}-{$code}" == $smsCode['value'] or apiSendError("请输入正确的验证码");

        // 查询并注册手机号为新用户
        $member = Member::findOne([
            'mobile' => $mobile
        ]);

        $initMember = false;
        // 用户不存在 自动注册
        if (!$member) {
            $newPwd = createRandKey();
            // 新用户注册
            $member = new Member();
            $member->username = $member->getNewUserName();
            $member->mobile = $mobile;
            $member->wxnick = '用户_' . createRandKey();
            $member->nickname = $member->wxnick;
            $member->setPassword($newPwd);
            $member->status = (string)$member::STATUS_ACTIVE;
            if(!empty($session->get('channel'))){
                $member->channel = $session->get('channel');
            }
            $member->save();

            $initMember = $newPwd;
            $session->set('INIT_PWD', $newPwd);
        }

        // 自动登录
        $memberForm = new MemberForm();
        $memberForm->wxlogin($member->id);

        $cityId = (int)Yii::$app->request->post('cityId', 0);
        if ($cityId) {
            $type = Yii::$app->request->post('type', '###');
            $tickets = CityTicket::init($cityId)->currentTickets($type);
            if (isset($tickets[$type]) and $tickets[$type]) {
                $ticketAsk = TicketAsk::findOne([
                    'mobile' => $member->mobile,
                    'ticket_id' => $tickets[$type]->id
                ]);

                if (!$ticketAsk) {
                    $ticketAsk = new TicketAsk();
                    $ticketAsk->name = Yii::$app->request->post('name', $member->nickname);
                    $ticketAsk->provinces_id = $cityId;
                    $ticketAsk->ticket_id = $tickets[$type]->id;
                    $ticketAsk->ticket_amount = 1;
                    $ticketAsk->mobile = $member->mobile;
                    $ticketAsk->resource = '默认';
                    $ticketAsk->address = Yii::$app->request->post('address', '');
                    $ticketAsk->status = 0;
                    $ticketAsk->create_time = date('Y-m-d H:i:s');
                    $ticketAsk->save();
                }
            }
        }

        $href = Yii::$app->request->post('location', '');
        if (strlen($href)) {
            if ($initMember) {
                if (strpos($href, '?') > -1 ) {
                    $href .= "&new_member=1";
                } else {
                    $href .= "?new_member=1";
                }
            }

            $url = urldecode($href);
            header("Location: {$url}");
        }

        die();
    }


    /**
     * 统一发送短信接口
     */
    public function actionSendSms()
    {
        $mobile = Yii::$app->request->post('mobile');

        $codeType = Yii::$app->request->post('codeType', 'DEFAULT');

        $getTicketType = Yii::$app->request->post('ticketType');

        preg_match('/^1\d{10}$/', $mobile) or apiSendError("请输入正确的手机号");

        $code = createRandKey(6);

        try {
            if ($this->memberInfo) {
                $smsModel = new Sms();
                $smsModel->user_id = $this->memberInfo->id;
                $smsModel->phone = $mobile;
                $smsModel->content = $code;
                $smsModel->type = 'sms_code';
                $smsModel->create_time = date('Y-m-d H:i:s');
                $smsModel->save();
            }

            $session = \Yii::$app->session;
//        "{$mobile}-{$code}" == $session->get('SMS_CODE') or stopFlow("请输入正确的验证码", 'index/index');
            $smsCode = $session->get('SMS_CODE');
            $smsCode or apiSendError("验证码无效");
            $smsCode['expire'] > time()  or apiSendError("验证码已过期");
            "{$mobile}-{$code}" == $smsCode['value'] or apiSendError("请输入正确的验证码");

            $accessKeyId = ConfigService::init(ConfigService::EXPAND)->get('access_key_id');
            $accessKeySecret = ConfigService::init(ConfigService::EXPAND)->get('access_key_secret');
            $params["PhoneNumbers"] = $mobile;
            $templateParam = array();
            switch ($codeType) {

                case SentSms::TYPE_REG :
                    $sentSms = SentSms::findOne(['type' => $codeType]);
                    $templateParam = ['code' => $code];
                    break;
                case SentSms::TYPE_CABLE_TICKET :   //展会索票
                    $sentSms = SentSms::findOne(['type' => $codeType]);
                    $ticketType = isset($getTicketType) ? $getTicketType : '-';
                    $templateParam = [
                        'code' => $code,
                        'ticketType' => $ticketType,
                    ];
                    break;
                case SentSms::TYPE_ADDRESS_NOTICE :
                    $sentSms = SentSms::findOne(['type' => $codeType]);
                    $templateParam = ['address' => $sentSms->content];
                    break;
                case SentSms::TYPE_FORGET_PASSWORD :
                    $sentSms = SentSms::findOne(['type' => $codeType]);
                    $templateParam = ['code' => $code];
                    break;
                case SentSms::TYPE_QUICK_LOGON :
                    $sentSms = SentSms::findOne(['type' => $codeType]);
                    $templateParam = ['code' => $code];
                    break;
                default:
                    apiSendError("请选择正确的类型");
            }

            $params["SignName"] = $sentSms->sms_name;
            $params["TemplateCode"] = $sentSms->sms_code;
            $params['TemplateParam'] = $templateParam;
            apiSendSuccess("信息", $templateParam);
            if (!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
                $params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
            }

            $helper = new SignatureHelper();

            $content = $helper->request(
                $accessKeyId,
                $accessKeySecret,
                "dysmsapi.aliyuncs.com",
                array_merge($params, array(
                    "RegionId" => "cn-hangzhou",
                    "Action" => "SendSms",
                    "Version" => "2017-05-25",
                ))
            );

            $content->Code == 'OK' and apiSendSuccess("OK");

            apiSendError("获取短信失败");

        } catch (\Exception $e) {
            apiSendError("获取短信失败");
        }
    }

    public function actionGoodsOrderPrice($goodId=0)
    {
        (int)$goodId or apiSendError("商品信息未找到");

        $cityId = $this->getRequestCity();

        $goodDetail = Goods::findOne(['id' => $goodId,
            'goods_state' => 1, 'isdelete' => 0
        ]);
        $goodDetail or apiSendError("商品已下架");
        $goodDetail->goods_image = strlen($goodDetail->goods_image) > 4 ? arrayGroupsAction(json_decode($goodDetail->goods_image, true), function ($detail) {
            return (object)$detail;
        }) : [];


        $zhGoods = Store::init()->goodsJoinZH($goodDetail->user_id);
        $rebuildZhGoodsInfo = [];
        arrayGroupsAction($zhGoods, function ($zhGood) use (&$rebuildZhGoodsInfo) {
            $rebuildZhGoodsInfo[$zhGood->good_id] = $zhGood;
        });
        $zhGoodsIds = array_keys($rebuildZhGoodsInfo);

        in_array($goodDetail->id, $zhGoodsIds) or apiSendError("该商品未参加展会");

        $goodType = $rebuildZhGoodsInfo[$goodDetail->id];

        $info = $goodDetail->attributes;

        $info['goods_price'] = $goodType->good_price;
        $info['goods_pic'] = $goodType->good_pic;
        $info['goods_storage'] = $goodType->good_amount;

        $info['store'] = $goodDetail->user;
        $goodInfo = (object)$info;

        $ticket = $goodType->ticket;

        switch ($goodType->type) {
            // 预存 享特价
            case TicketApplyGoods::TYPE_ORDER :
            // 爆款预约
            case TicketApplyGoods::TYPE_COUPON :
                $type = Yii::$app->request->get('type', '');

                $location = UrlService::build(['order/create-price', 'goodId' => $goodInfo->id, 'type' => $type]);
                $this->render('goods-order-price', [
                    'good' => $goodInfo,
                    'location' => $location,
                    'ticket' => $ticket
                ]);
                break;
            default :
                apiSendError("该商品已下架");
        }

    }

    public function actionStoreCoupons()
    {
        $id = Yii::$app->request->get('id', 0);
        $storeId = Yii::$app->request->get('storeId', 0);

        $goodCoupon = GoodsCoupon::findOne([
            'id' => $id,
            'user_id' => $storeId,
            'is_del' => 0,
        ]);

        $goodCoupon or apiSendError("未找到有效的优惠券");

        $this->render('store-coupons', [
            'coupon' => $goodCoupon,
            'location' => UrlService::build(['dian-pu/receive-coupon', 'id' => $id, 'storeId' => $storeId])
        ]);
    }

    public function actionGoodsOrderSuccess()
    {
        $this->memberInfo or apiSendError("您尚未登录");
        $key = Yii::$app->request->get('key', 0);
        $type = Yii::$app->request->get('type', '###');
        $orderId = strrev($key);

        switch ($type) {
            // 报款预约弹框
            case 'COUPON' :
                $order = Order::findOne(['orderid' => $orderId, 'buyer_id' => $this->memberInfo->id]);
                $order or apiSendError("未找到有效的订单信息");

                $orderGood = OrderGoods::findOne(['order_id' => $order->orderid]);
                $ticket = Ticket::findOne((int)$order->ticket_id);

                $ticketApplyGoodsInfo = TicketApplyGoods::findOne([
                    'ticket_id' => $order->ticket_id,
                    'good_id' => $orderGood->goods_id
                ]);

                $tmp = 'goods-order-success';
                $data = [
                    'ticket' => $ticket,
                    'order' => $order,
                    'orderGood' => $orderGood,
                    'ticketApplyGoodsInfo' => $ticketApplyGoodsInfo
                ];
                break;
            // 领取店铺优惠券
            case 'MEMBER_COUPON' :
                $memberCoupon = MemberStoreCoupon::findOne($key);
                $memberCoupon or apiSendError("未找到有效的领取信息");

                $ticket = Ticket::findOne((int)$memberCoupon->ticket_id);
                $tmp = 'member-coupon-success';

                $coupon = $memberCoupon->getGoodsCoupon();

                $data = [
                    'ticket' => $ticket,
                    'memberCoupon' => $memberCoupon,
                    'coupon' => $coupon,
                ];
                break;
            // 预约店铺
            case 'STORE' :
                try {
                    $log = MemberTicketStore::findOne([
                        'member_id' => $this->memberInfo->id,
                        'id' => $key
                    ]);
                    $log or apiSendError("未找到预定信息");

                    $ticket = Ticket::findOne($log->ticket_id);
                    $store = Store::init()->info($log->store_id);

                    $shops = Store::init()->shops($log->store_id);
                    $tmp = 'store-appoint-success';
                    $data = [
                        'ticket' => $ticket,
                        'shops' => $shops,
                        'storeInfo' => $store
                    ];

                } catch (\Exception $e) {
                    apiSendError($e->getMessage());
                }
        }

        return $this->render($tmp, $data);
    }

    public function actionAppointStore()
    {
        try {
            $city = $this->getRequestCity();
            $ticketData = CityTicket::init($city)->currentFactionTickets();
            $ticketData or apiSendError("当前城市未开展展会");

            $type = Yii::$app->request->get('type', '###');
            $storeId = Yii::$app->request->get('id', '0');

            $ticket = false;
            foreach ($ticketData AS $ticketInfo) {
                if ($ticketInfo->type == $type) {
                    $ticket = $ticketInfo;
                    break;
                }
            }
            $ticket or apiSendError("当前城市未开展此类型展会");

            $store = Store::init()->info($storeId);
            Store::init()->checkJoinZH($store->user_id, $type) or apiSendError("该店铺未参加此次展会");

            if (empty($this->memberInfo)) {
                $this->render('store-appoint', [
                    'storeInfo' => $store,
                    'ticket' => $ticket,
                    'location' => UrlService::build(['api/appoint-store', 'type' => $type, 'id' => $storeId])
                ]);
            }

            $log = MemberTicketStore::findOne([
                'ticket_id' => $ticket->id,
                'store_id' => $store->user_id,
                'member_id' => $this->memberInfo->id
            ]);
            $log and apiSendError("您已预约该店铺");

            $log = new MemberTicketStore();
            $log->member_id = $this->memberInfo->id;
            $log->ticket_id = $ticket->id;
            $log->store_id = $store->user_id;

            if ($log->save()) {
                \DL\Project\Sms::init()->storeHuoDongYuYue($this->memberInfo->mobile, $store->user_id, $ticket->id);

                $newMemberStatus = \Yii::$app->request->get('new_member', 0);
                if ($newMemberStatus) {
                    $session = \Yii::$app->session;
                    $initPwd = $session->get('INIT_PWD');
                    \DL\Project\Sms::init()->zhuCeHuiYuan($this->memberInfo->id, $initPwd);
                }

                apiSendSuccess("预约成功", [
                    'key' => $log->id,
                    'type' => 'STORE'
                ]);
            }

            apiSendError("预约失败");
        } catch (\Exception $e) {
            apiSendError($e->getMessage());
        }

    }

    public function renderContent($content)
    {
        return $content;
    }

}