<?php

namespace index\controllers;

use DL\service\IntergralGoodsService;
use Yii;
use common\models\IntergralGoods;
use common\search\IntergralGoodsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IntergralGoodsController implements the CRUD actions for IntergralGoods model.
 */
class IntergralGoodsController extends BaseController
{
    /**
     * 商品详情
     */
    public function actionDetail()
    {
        $id = (int)Yii::$app->request->get('id', 0);
        $id or stopFlow("商品信息未找到");

        try {
            $goodDetail = IntergralGoodsService::detail($id);

            if (strlen($goodDetail->goods_name)) {
	            global $title;
	            $title = $goodDetail->goods_name;
	        }
	        if (strlen($goodDetail->keywords)) {
	            global $keywords;
	            $keywords = $goodDetail->keywords;
	        }
	        if (strlen($goodDetail->description)) {
	            global $description;
	            $description = $goodDetail->description;
	        }
	        

            $this->render('detail', [
                'goodDetail' => $goodDetail,
                'pageIndexKey' => 4
            ]);

        } catch (\Exception $e) {
            stopFlow($e->getMessage());
        }
    }
}
