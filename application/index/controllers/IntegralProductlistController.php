<?php
namespace index\controllers;

use common\models\Goods;
use common\models\GoodsClass;
use common\models\GoodsCoupon;
use common\models\Member;
use common\models\StoreGcScore;
use common\models\Ticket;
use common\models\TicketApply;
use common\models\TicketAsk;
use common\models\TicketInfo;
use DL\Project\AskTicket;
use DL\Project\FloorConfig;
use DL\service\CacheService;
use DL\vendor\ConfigService;
use DL\Project\Page;
use yii\helpers\ArrayHelper;

/**
 * Class IntegralProductlistController
 * @package index\controllers
 *  我能兑换
 */
class IntegralProductlistController extends IntegralController
{
    public function init()
    {
        parent::init();

        $this->memberInfo or stopFlow("您尚未登录", 'member/login');
    }

    public function actionIndex()
    {
        $member = Member::findOne($this->memberInfo->id);

        $_GET['inTop'] = $member->integral;
        return $this->actionLists();
    }
}
