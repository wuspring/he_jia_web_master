<?php
/**
 * Created by PhpStorm.
 * User: 何孝林
 * Date: 2018/7/10
 * Time: 16:02
 */

namespace index\controllers;
use Yii;
use yii\web\Controller;


class Common extends Controller
{
    public function init()
    {
    
   
        $route=\yii::$app->session->get('routedata');
        if (empty($route)) {
          $route=array('','');
          $route[0]=Yii::$app->controller->getRoute();
          $route[1]=Yii::$app->controller->getRoute();
        }else{
            $route[0]=$route[1];
            $route[1]=Yii::$app->controller->getRoute();
            
        }
        \yii::$app->session->set('routedata',$route);
        parent::init();
    }
}