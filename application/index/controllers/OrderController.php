<?php
namespace index\controllers;


use common\models\Goods;
use common\models\Order;
use common\models\OrderGoods;
use common\models\Recharge;
use common\models\Ticket;
use common\models\TicketApplyGoods;
use DL\Project\CityTicket;
use DL\Project\Sms;
use DL\Project\Store;
use DL\service\OrderService;
use DL\service\UrlService;

class OrderController extends BaseController
{
    public function init()
    {
        parent::init();

        $this->memberInfo or stopFlow("您尚未登录", 'member/login');
    }

    /**
     * 预定商品
     */
    public function actionCreate()
    {
        try {

            $type = \Yii::$app->request->get('type', '###');
            $currentTickets = CityTicket::init($this->getRequestCity())->currentTickets();

            isset($currentTickets[$type]) or stopFlow("活动尚未开展", 'index/index');

            $ticket = $currentTickets[$type];
            $ticket or stopFlow("活动已结束", 'index/index');

            $goodId = \Yii::$app->request->get('goodId', 0);
            $ticketGood = TicketApplyGoods::findOne([
                'ticket_id' => $ticket->id,
                'good_id' => $goodId,
                'type' => TicketApplyGoods::TYPE_ORDER,
            ]);
            $ticketGood or stopFlow("该商品未参加本次活动");
            $good = $ticketGood->goods;

            $orderService = OrderService::init();
            $order = $orderService->createNewEmptyOrder($this->memberInfo->id, $good->user_id, Order::TYPE_ZHAN_HUI, $ticketType=Order::TICKET_TYPE_ORDER, $ticket->id);
            $orderService->addOrderGoodToOrder($order->orderid, $good->id, 1, $ticket->id);

            $url = UrlService::build(['order/submit', 'number' => $order->orderid]);
            header("Location: {$url}");
            die();
        } catch (\Exception $e) {
            stopFlow($e->getMessage());
        }
    }

    public function actionCreatePrice()
    {
        try {
            $type = \Yii::$app->request->get('type', '###');
            $currentTickets = CityTicket::init($this->getRequestCity())->currentTickets();

            isset($currentTickets[$type]) or apiSendError("活动尚未开展");

            $ticket = $currentTickets[$type];
            $ticket or apiSendError("活动已结束");

            $goodId = \Yii::$app->request->get('goodId', 0);
            $ticketGood = TicketApplyGoods::findOne([
                'ticket_id' => $ticket->id,
                'good_id' => $goodId,
                'type' => TicketApplyGoods::TYPE_COUPON,
            ]);
            $ticketGood or apiSendError("该商品未参加本次活动");
            $orders = Order::findAll([
                'buyer_id' => $this->memberInfo->id,
                'ticket_id' => $ticket->id,
            ]);

            if ($orders) {
                $orderIds = arrayGroupsAction($orders, function ($order) {
                    return $order->orderid;
                });

                $orderGoods = OrderGoods::find()->where([
                    'and',
                    ['in', 'order_id', $orderIds],
                    ['=', 'goods_id', $ticketGood->good_id]
                ])->all();

                $orderGoods and apiSendError("该商品已预订");
            }
            $good = $ticketGood->goods;

            $orderService = OrderService::init();
            $order = $orderService->createNewEmptyOrder($this->memberInfo->id, $good->user_id, Order::TYPE_ZHAN_HUI, TicketApplyGoods::TYPE_COUPON, $ticket->id);
            $orderService->addOrderGoodToOrder($order->orderid, $good->id, 1, $ticket->id);

            // 发送短信
            Sms::init()->baoKuanYuYue($this->memberInfo->mobile, $good->id, $ticket->id);
            $newMemberStatus = \Yii::$app->request->get('new_member', 0);
            if ($newMemberStatus) {
                $session = \Yii::$app->session;
                $initPwd = $session->get('INIT_PWD');
                Sms::init()->zhuCeHuiYuan($this->memberInfo->id, $initPwd);
            }

            apiSendSuccess("您已成功预约{$good->goods_name}产品", [
                'key' => strrev($order->orderid),
                'type' => 'COUPON',
                'href' => UrlService::build('personal/coupon-order')
            ]);
            die();
        } catch (\Exception $e) {
            apiSendError($e->getMessage());
        }
    }

    public function actionSubmit($number='###')
    {
        $order = Order::findOne([
            'orderid' => $number,
            'order_state' => Order::STATUS_WAIT
        ]);

        $order or stopFlow("未找到待支付的订单");

        $this->render('create', [
            'order' => $order
        ]);
    }

    /**
     * 订单支付状态查询
     */
    public function actionStatus()
    {
        $orderId = \Yii::$app->request->get('number', '###');

        if(preg_match('/^ch\d+$/', $orderId)) {
            $order = Recharge::findOne([
                'pay_sn' => $orderId,
                'state' => Order::STATUS_PAY
            ]);
        } elseif (preg_match('/^re\d+$/', $orderId)) {

        } else {
            $order = Order::findOne([
                'orderid' => $orderId,
                'order_state' => Order::STATUS_PAY
            ]);
        }

        if ($order) {
            $good = $order->getFirstGoods();
            Sms::init()->yuCunChanPin($order->receiver_mobile, $good->id, $order->ticket_id);

            apiSendSuccess();
        }

        apiSendError();
    }

    public function actionUpdate()
    {
        $this->memberInfo or apiSendError("用户未登录");
        $orderId = \Yii::$app->request->post('number', 0);
        $order = Order::findOne([
            'orderid' => $orderId,
            'buyer_id' => $this->memberInfo->id,
            'order_state' => Order::STATUS_WAIT
        ]);

        $order or apiSendError("订单信息未找到");

        $payMethod = \Yii::$app->request->post('setPay', '');
        if (strlen($payMethod)) {
            if (in_array($payMethod, [
                $order::PAY_MECHOD_ACCOUNT,
                $order::PAY_MECHOD_WECHAT,
                $order::PAY_MECHOD_ALIPAY,
            ])) {
                $order->pay_method = trim($payMethod);
            }
        }

        $order->save() and apiSendSuccess("OK");


        apiSendError("更新订单信息失败");
    }

    /**
     * 支付宝支付回调验证
     * @author daorli
     * @date 2019-6-21
     *
     * @param $number
     *
     */
    public function actionReturn($number)
    {
        $order = Order::findOne([
            'pay_sn' => $number
        ]);

        $order or stopFlow("未找到有效的订单信息");

        switch ($order->order_state) {
            case $order::STATUS_PAY :
                header('Location: ' . UrlService::build(['payment/success', 'number' => $order->pay_sn]));
                die();
            default :
                header('Location: ' . UrlService::build(['payment/pay-fail', 'number' => $order->pay_sn]));
                die();
        }
    }

}
