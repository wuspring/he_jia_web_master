<?php
namespace index\controllers;

use common\models\Address;
use common\models\Goods;
use common\models\GoodsClass;
use common\models\GoodsCoupon;
use common\models\IntegralLog;
use common\models\IntergralOrder;
use common\models\IntergralTreasure;
use common\models\IntergralTreasureLog;
use common\models\Member;
use common\models\Provinces;
use common\models\StoreGcScore;
use common\models\Ticket;
use common\models\TicketApply;
use common\models\TicketAsk;
use common\models\TicketInfo;
use DL\Project\AskTicket;
use DL\Project\FloorConfig;
use DL\service\CacheService;
use DL\service\IntergralOrderService;
use DL\service\UrlService;
use DL\vendor\ConfigService;
use DL\Project\Page;
use yii\helpers\ArrayHelper;

/**
 * Class IntegralProductlistController
 * @package index\controllers
 *  积分兑换
 */
class IntegralOrderController extends BaseController
{
    public function init()
    {
        parent::init();

        $this->memberInfo or stopFlow("您尚未登录", 'member/login');
    }

    /**
     * 夺宝商品详情
     */
    public  function  actionTreasure($id=0)
    {
        $treasure = IntergralTreasure::findOne([
            'id' => $id,
            'isdelete' => 0,
//            'goods_state' => 1,
        ]);

        // 展会开展票期数
        $oldTreasureAmount = 1;
        $treasureHistorys = [];
        if ($treasure->from_id) {
             $searchModel = IntergralTreasure::find()->where([
                'or',
                [
                    'and',
                    ['=', 'from_id', $treasure->from_id],
                    ['<>', 'id', $treasure->id]
                ],
                ['=', 'id', $treasure->from_id]
            ]);
            $oldTreasureAmount = $searchModel->count() + 1;
            $treasureHistorys = $searchModel->orderBy('id desc')->limit(5)->all();
        }

        $logs = IntergralTreasureLog::findBySql("select member_id, count(*) as times, create_time from `intergral_treasure_log` where `treasure_id`='{$treasure->id}' group by `member_id`;")->all();


        if (strlen($treasure->goods_name)) {
            global $title;
            $title = $treasure->goods_name;
        }
        if (strlen($treasure->keywords)) {
            global $keywords;
            $keywords = $treasure->keywords;
        }
        if (strlen($treasure->description)) {
            global $description;
            $description = $treasure->description;
        }

        return $this->render('treasure', [
            'treasure' => $treasure,
            'oldTreasureAmount' => $oldTreasureAmount,
            'treasureHistorys' => $treasureHistorys,
            'logs' => $logs,
            'pageIndexKey' => 4
        ]);
    }

    /**
     * 创建订单
     */
    public function actionCreate()
    {
        $id = \Yii::$app->request->get('id', 0);
        $skuId = (int)\Yii::$app->request->get('sku', 0);
        $amount = (int)\Yii::$app->request->get('amount', 1);

        try {
            $order = IntergralOrderService::init()->create($this->memberInfo, $id, $amount, $skuId);

            $url = UrlService::build(['integral-order/confirm', 'number' => $order->orderid]);
            header("Location: {$url}");
            die;
        } catch (\Exception $e) {
            stopFlow($msg = $e->getMessage());
        }

    }

    public function actionConfirm($number)
    {
        $order = IntergralOrder::findOne([
            'orderid' => $number,
            'order_state' => IntergralOrder::STATUS_WAIT_PAY,
        ]);

        $order or stopFlow("未找到待支付的积分订单");

        $provinces = Provinces::findAll([
            'upid' => 1
        ]);
        $provinces = ArrayHelper::map($provinces,'id','cname');

        $expand = [
            IntergralOrder::EXPAND_WORKDAY,
            IntergralOrder::EXPAND_WEEKEND,
            IntergralOrder::EXPAND_ANYTIME,
        ];
        $addresses = Address::find()->where([
            'memberId' => $this->memberInfo->id,
            'is_delete' => 0
        ])->orderBy('isDefault desc')->all();

        return $this->render('confirm', [
            'order' => $order,
            'provinces' => $provinces,
            'addresses' => $addresses,
            'expand' => $expand,
        ]);
    }

    public function actionUpdate()
    {
        $orderId = \Yii::$app->request->post('number', 0);
        $order = IntergralOrder::findOne([
            'orderid' => $orderId,
            'buyer_id' => $this->memberInfo->id,
            'order_state' => IntergralOrder::STATUS_WAIT_PAY
        ]);

        $order or apiSendError("订单信息未找到");
        $addressId = (int)\Yii::$app->request->post('setAddress', 0);
        if ($addressId) {
            $address = Address::findOne($addressId);

            $order->receiver = $address->id;
            $order->receiver_name = $address->name;
            $order->receiver_mobile = $address->mobile;
            $order->receiver_address = $address->provinceInfo . ' ' . $address->address;
        }
//
//        $payMethod = \Yii::$app->request->post('setPay', '');
//        if (strlen($payMethod)) {
//            if (in_array($payMethod, [
//                $order::PAY_CODE_ACCOUNT,
//                $order::PAY_CODE_WECHAT,
//                $order::PAY_CODE_ALIPAY,
//            ])) {
//                $order->pay_code = trim($payMethod);
//            }
//        }

        $expand = \Yii::$app->request->post('expand', '');
        if (strlen($expand)) {
            $order->expand = $expand;
        }

        $order->save() and apiSendSuccess("OK");


        apiSendError("更新订单信息失败");
    }

    public function actionPay($number)
    {
        $order = IntergralOrder::findOne([
            'orderid' => $number,
            'order_state' => IntergralOrder::STATUS_WAIT_PAY,
        ]);
        $order or stopFlow("未找到待支付的积分订单");

        $member = Member::findOne($this->memberInfo->id);
        if ($member->integral < $order->order_amount) {
            stopFlow("账户积分不足",'order/index');
        }

        $oldIntegral = $member->integral;
        $member->integral = $member->integral - $order->integral_amount;

        $integralLog = new IntegralLog();
        $integralLog->orderId = $order->orderid;
        $integralLog->memberId = $this->memberInfo->id;
        $integralLog->old_integral =$oldIntegral;
        $integralLog->now_integral = $member->integral;
        $integralLog->pay_integral = $order->integral_amount;
        $integralLog->create_time = date('Y-m-d H:i:s');

        $url = UrlService::build(['integral-order/pay-failed', 'number' => $order->orderid]);
        if ($integralLog->save()) {
            $order->order_state = $order::STATUS_WAIT_SEND;
            $order->save();

            $url = UrlService::build(['integral-order/pay-success', 'number' => $order->orderid]);
        };

        header("Location: {$url}");
        die();
    }

    public function actionPaySuccess($number)
    {
        $order = IntergralOrder::findOne($number);
        $order or stopFlow("未找到有效的积分订单");

        return $this->render('pay_suc', [
            'order' => $order
        ]);
    }

    public function actionPayFailed($number)
    {
        $order = IntergralOrder::findOne($number);
        $order or stopFlow("未找到有效的积分订单");

        return $this->render('pay_fail', [
            'order' => $order,
            'msg' => '积分支付失败'
        ]);
    }

}
