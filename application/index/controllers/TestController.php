<?php

namespace index\controllers;

use common\models\ConfigAskTicket;

use common\models\FloorIndex;
use DL\Project\Sms;
use yii\filters\VerbFilter;

/**
 * TicketApplyController implements the CRUD actions for TicketApply model.
 */
class TestController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TicketApply models.
     * @return mixed
     */
    public function actionFixConfigTicket()
    {die('stop');
        $datas = ConfigAskTicket::find()->where(
            ['in', 'type', ['PAGE_INDEX', 'PAGE_CAI_GOU_JIAZHUANG', 'PAGE_CAI_GOU_JIEHUN', 'PAGE_CAI_GOU_YUNYING']]
        )->all();
        if ($datas) {
            arrayGroupsAction($datas, function ($config) {
                $config->data = json_decode($config->data);

                if ($config->data) {
                    $huodong = $config->data->huodongs;
                    isset($huodong->huodong_5) or $huodong->huodong_5 = '@';
                    isset($huodong->huodong_6) or $huodong->huodong_6 = '@';
                    isset($huodong->huodong_7) or $huodong->huodong_7 = '@';
                    isset($huodong->huodong_8) or $huodong->huodong_8 = '@';

                    $config->data->huodongs = $huodong;

                    $config->data = json_encode($config->data);

                    $config->save();
                }
            });

            die('success');
        }

        die('failed');
    }

    public function actionFixIndexTicket()
    {die('stop');
        $datas = ConfigAskTicket::find()->where(
            ['in', 'type', ['JIE_HUN', 'JIA_ZHUANG', 'YUN_YING']]
        )->all();
        if ($datas) {
            arrayGroupsAction($datas, function ($config) {
                $config->data = json_decode($config->data);

                if ($config->data) {
                    $indexTop = $config->data->index_top;
                    $config->data->index_top = [];
                    foreach (explode(',', $indexTop) AS $index => $path) {
                        $config->data->index_top['pic_' . ($index+1)] = '@' . trim($path);
                    }
                    $config->data->index_top = json_encode($config->data->index_top);

                    $indexFooter = $config->data->index_footer;
                    $config->data->index_footer = [];
                    foreach (explode(',', $indexFooter) AS $index => $path) {
                        if ($index < 2) {
                            $config->data->index_footer['pic_' . ($index+1)] = '@' . trim($path);
                        }
                    }
                    isset($config->data->index_footer['pic_1']) or $config->data->index_footer['pic_1'] = '@';
                    isset($config->data->index_footer['pic_2']) or $config->data->index_footer['pic_2'] = '@';
                    isset($config->data->index_footer['pic_3']) or $config->data->index_footer['pic_3'] = '@';
                    $config->data->index_footer = json_encode($config->data->index_footer);

                    $config->data = json_encode($config->data);

                    $config->save();
                }
            });

            die('success');
        }

        die('failed');
    }

    public function actionFixExpandTicket()
    {die('die');
        $datas = ConfigAskTicket::find()->where(
            ['in', 'type', ['PAGE_INDEX', 'PAGE_CAI_GOU_JIAZHUANG', 'PAGE_CAI_GOU_JIEHUN', 'PAGE_CAI_GOU_YUNYING']]
        )->all();
        if ($datas) {
            arrayGroupsAction($datas, function ($config) {
                $data = json_decode($config->data, true);

                if ($data) {
                    isset($data['expand']) or $data['expand'] = [];
                    isset($data['expand']['huodong_1']) or $data['expand']['huodong_1'] = '@';
                    isset($data['expand']['huodong_2']) or $data['expand']['huodong_2'] = '@';
                    isset($data['expand']['huodong_3']) or $data['expand']['huodong_3'] = '@';
                    isset($data['expand']['huodong_4']) or $data['expand']['huodong_4'] = '@';

                    $config->data = (object)$data;

                    $config->data = json_encode($config->data);

                    $config->save();
                }
            });

            die('success');
        }

        die('failed');
    }

    public function actionFixConfigBanner()
    {die('not found');
        $datas = ConfigAskTicket::find()->where(
            ['in', 'type', ['PAGE_INDEX', 'PAGE_CAI_GOU_JIAZHUANG', 'PAGE_CAI_GOU_JIEHUN', 'PAGE_CAI_GOU_YUNYING']]
        )->all();
        if ($datas) {
            arrayGroupsAction($datas, function ($config) {
                $config->data = json_decode($config->data);

                if ($config->data) {
                    $banner = $config->data->banner;
                    $data = [];
                    foreach (explode(',', $banner) AS $i =>  $b) {
                        $n = $i+1;
                        $data[$n] = 'javascript:void(0)@' . trim($b);
                    }
                    $config->data->banner = (object)$data;

                    $config->data= json_encode($config->data);

                    $config->save();
                }
            });

            die('success');
        }

        die('failed');
    }


    public function actionFixZhTicket()
    {die('not found');
        $datas = ConfigAskTicket::find()->where(
            ['in', 'type', ['JIE_HUN', 'JIA_ZHUANG', 'YUN_YING']]
        )->all();
        if ($datas) {
            arrayGroupsAction($datas, function ($config) {
                $newData = json_decode($config->data, true);

                if ($newData) {
                    if (!isset($newData['zhanhui_pic'])) {
                        $newData['zhanhui_pic'] = json_encode([
                            'pic_1' => '@/public/index/images/_temp/skin/img_jia_ad01.jpg',
                            'pic_2' => '@/public/index/images/_temp/skin/img_jia_ad02.jpg',
                            'pic_3' => '@/public/index/images/_temp/skin/img_jia_ad03.jpg',
                            'pic_4' => '@/public/index/images/_temp/skin/img_jia_ad04.jpg',
                            'pic_5' => '@/public/index/images/_temp/skin/img_jia_ad05.jpg',
                        ]);

                        $config->data = json_encode($newData);
                        $config->save();
                    }
                }
            });

            die('success');
        }

        die('failed');
    }

    public function actionFixZhTicketPic()
    {die('not found');
        $datas = ConfigAskTicket::find()->where(
            ['in', 'type', ['JIE_HUN', 'JIA_ZHUANG', 'YUN_YING']]
        )->all();
        if ($datas) {
            arrayGroupsAction($datas, function ($config) {
                $newData = json_decode($config->data, true);

                if ($newData) {
                    $newData['zhanhui_pic'] = json_decode($newData['zhanhui_pic'], true);

                    isset($newData['zhanhui_pic']['pic_6']) or $newData['zhanhui_pic']['pic_6'] = '@';
                    isset($newData['zhanhui_pic']['pic_7']) or $newData['zhanhui_pic']['pic_7'] = '@';
                    isset($newData['zhanhui_pic']['pic_8']) or $newData['zhanhui_pic']['pic_8'] = '@';

                    $newData['zhanhui_pic'] = json_encode($newData['zhanhui_pic']);

                    $config->data = json_encode($newData);
                    $config->save();
                }
            });

            die('success');
        }

        die('failed');
    }

    public function actionFixZhFirstPic()
    {
        $alls = FloorIndex::find()->all();
        arrayGroupsAction($alls, function ($all) {
            $zh_img = json_decode($all->zh_img, true);

            if (isset($zh_img['pic_1'])) {
                list($h, $firPic) = explode('@', $zh_img['pic_1']);

                strlen($firPic) and $all->zh_first_imgs = $firPic;

                unset($zh_img['pic_1']);

                $all->zh_img = json_encode($zh_img);
            }

            $all->save();
        });
        die("success");
    }

    public function actionGetCode($phone='')
    {
//
//        preg_match('/^1\d{10}$/', $phone) or stopFlow("请输入有效的手机号");
//
//        $code = createRandKey(6);

//        Sms::init()->code(12345678901, 123456,110000);
//        Sms::init()->askTicktet(12345678901, 9);
//        Sms::init()->yuCunChanPin(12345678901, 69,9);
//        Sms::init()->baoKuanYuYue(12345678901, 69,9);
//        Sms::init()->diZhiLan(12345678901, 9);
//        Sms::init()->pinPaiYuYue(12345678901, 31, 9);
//        Sms::init()->storeHuoDongYuYue(12345678901, 31, 9);
//        Sms::init()->youHuiQuanYuYue(12345678901, 60, 9);
//        Sms::init()->zhuCeHuiYuan(9, 123456);
    }

}
