<?php

namespace index\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use common\models\Upload;

/**
 * UserController implements the CRUD actions for User model.
 */
class UploadController extends Controller
{

    public function init()
    {
        $this->enableCsrfValidation = false;
        parent::init();
    }

    public function actionImg()
    {
        if (Yii::$app->request->isPost) {

            $model = new Upload();
            $result = [];
            if (isset($_GET['more'])) {
                $files = UploadedFile::getInstancesByName('img');

                foreach ($files AS $file) {
                    $fileInfo = $this->_getUploadFilePath($file->extension);
                    $file->saveAs(ROOT_PATH . $fileInfo);

                    $result[] = $fileInfo;
                }
            } else {
                $model->file = UploadedFile::getInstanceByName('img');
                $file = $this->_getUploadFilePath($model->file->extension);
                $status = $model->file->saveAs(ROOT_PATH . $file);

                $result = $file;
            }

//            $this->sendSuccess('上传成功', $result);
            apiSendSuccess('上传成功', $result);
        }

//        $this->sendError('上传失败');
        apiSendError('上传失败');

    }

    protected function _getUploadFilePath($type, $saveNameLength=10)
    {
        $dictionary = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $max = strlen($dictionary)-1;

        $name = '';
        for ($i=0;$i<$saveNameLength;$i++) {
            $name .= $dictionary[rand(0, $max)];
        }
        $date = date('Y-m-d');
        $path = "/uploads/{$type}/{$date}/{$name}.$type";
        $pathInfo = pathinfo($path);
        $dir = ROOT_PATH . $pathInfo['dirname'];
        is_dir($dir) or $this->_mkdirs($dir);

        if (!is_file(ROOT_PATH . $path)) {
            return $path;
        }

        return $this->_getUploadFilePath($type, $saveNameLength);
    }

    private function _mkdirs($dir, $mode = 0777)
    {
        if (is_dir($dir) || @mkdir($dir, $mode, true)){
            return true;
        }

        if (!$this->_mkdirs(dirname($dir), $mode)){
            return false;
        }

        return @mkdir($dir, $mode);
    }

    protected function sendError($message)
    {
        $request = [
            'status' => 0,
            'msg' => $message
        ];

        die(json_encode($request));
    }

    protected function sendSuccess($message='ok', $data=[])
    {
        $request = [
            'status' => 1,
            'msg' => $message,
            'data' => $data
        ];

        die(json_encode($request));
    }
}