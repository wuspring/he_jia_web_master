<?php

namespace index\controllers;

use common\models\Order;
use common\models\Payment;
use common\models\Recharge;

use yii\web\Controller;
use Yii;

require_once ROOT_PATH . '/common/alipay/pagepay/service/AlipayTradeService.php';
require_once ROOT_PATH . '/common/alipay/pagepay/buildermodel/AlipayTradeQueryContentBuilder.php';

class AlipayController extends PaymentController
{
    /**
     * 支付宝异步回调地址
     * @author wufeng
     * @date 2018-11-08
     */
    public function actionNotify(){
        libxml_disable_entity_loader(true);
        $arr=$_POST;
        //配置信息
        $payment_config = Payment::findOne(['code'=>'alipay']);
        $config = json_decode($payment_config->payment_config,true);
        $alipaySevice = new \AlipayTradeService($config);
        $alipaySevice->writeLog(var_export($_POST,true));
        $result = $alipaySevice->check($arr);

        /* 实际验证过程建议商户添加以下校验。
        1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
        2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
        3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
        4、验证app_id是否为该商户本身。
        */


        if($result) {//验证成功
            //商户订单号
            $out_trade_no = $_POST['out_trade_no'];

            $order = Order::findOne([
                'pay_sn' => $out_trade_no,
                'order_state' => Order::STATUS_WAIT
            ]);

            if (!$order) {
                die('order status error');
            }
            //交易状态
            $trade_status = $_POST['trade_status'];
            if($_POST['trade_status'] == 'TRADE_FINISHED') {
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //请务必判断请求时的total_amount与通知时获取的total_fee为一致的
                //如果有做过处理，不执行商户的业务程序
                //注意：
                //退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知

                $this->_notifyOrderByPaySn($out_trade_no);
            }
            else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
                $this->_notifyOrderByPaySn($out_trade_no);
            }


            echo "success";	//请不要修改或删除
        }else {
            //验证失败
            echo "fail";
        }
    }

}
