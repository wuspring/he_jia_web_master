<?php
namespace index\controllers;

use common\models\FitupProblem;
use common\models\Goods;
use common\models\News;
use common\models\Newsclassify;
use common\models\Provinces;
use common\models\SelectCity;
use common\models\StoreInfo;
use common\models\Ticket;
use common\models\TicketApply;
use common\models\TicketApplyGoods;
use common\models\TicketInfo;
use DL\Project\AskTicket;
use DL\Project\CityTicket;
use DL\Project\FloorConfig;
use DL\Project\Page;
use DL\Project\Store;
use DL\service\CacheService;
use DL\service\UrlService;
use DL\vendor\ConfigService;
use yii\helpers\ArrayHelper;

class IndexController extends BaseController
{

    public function actionIndex()
    {
        $currentCity = $this->getRequestCity();

        $iConfig = Page::init()->get($currentCity, Page::PAGE_INDEX);
        $configIndex = [
            'banner' => $iConfig->banner,
            'huodongs' => $iConfig->huodongs,
            'expand' => $iConfig->expand,
            'promise' => isset($iConfig->promise) ? $iConfig->promise : '',
            'smap' => isset($iConfig->smap) ? $iConfig->smap : ''
        ];
        $jiaZhuang = [
            'index_top' => AskTicket::init()->get($currentCity, Ticket::TYPE_JIA_ZHUANG)->index_top,
            'index_footer' => AskTicket::init()->get($currentCity, Ticket::TYPE_JIA_ZHUANG)->index_footer,
        ];

        $jieHun = [
            'index_top' => AskTicket::init()->get($currentCity, Ticket::TYPE_JIE_HUN)->index_top,
            'index_footer' => AskTicket::init()->get($currentCity, Ticket::TYPE_JIE_HUN)->index_footer,
        ];

        $yunYing = [
            'index_top' => AskTicket::init()->get($currentCity, Ticket::TYPE_YUN_YING)->index_top,
            'index_footer' => AskTicket::init()->get($currentCity, Ticket::TYPE_YUN_YING)->index_footer,
        ];

        $askTicketConfig = [
            'jia_zhuang' => $jiaZhuang,
            'jie_hun' => $jieHun,
            'yun_ying' => $yunYing,
        ];

        $cityId = $this->getRequestCity();
        $cityConfigs = FloorConfig::init()->city($cityId);

        $cityInfo = Provinces::findOne($cityId);
        if (strlen($cityInfo->title)) {
            global $title;
            $title = $cityInfo->title;
        }
        if (strlen($cityInfo->keywords)) {
            global $keywords;
            $keywords = $cityInfo->keywords;
        }
        if (strlen($cityInfo->description)) {
            global $description;
            $description = $cityInfo->description;
        }

        // 定金商品
        $dingJins = [];
        // 热卖商品
        $reMais = [];

        $usefulStoreIds = Store::init()->usefulStoreIds();
        // 当前城市
        $hasCityTickets = CityTicket::init($cityId)->currentFactionTickets();
        $basePathData = [];
        if ($hasCityTickets) {
            $ticketIds = array_keys($hasCityTickets);
            $dingJinsGoods = TicketApplyGoods::find()->where([
                'and',
                ['in', 'ticket_apply_goods.ticket_id', $ticketIds],
                ['=', 'ticket_apply_goods.type', TicketApplyGoods::TYPE_ORDER],
                ['in', 'ticket_apply_goods.user_id', $usefulStoreIds],
                ['=', 'ticket_apply_goods.is_index', 1],
                ['>', 'ticket_apply_goods.good_amount', 0]
            ])->leftJoin('goods as g', 'g.id = ticket_apply_goods.good_id')->orderBy('g.admin_score desc')->select('ticket_apply_goods.*')->all();

            $dingJins = arrayGroupsAction($dingJinsGoods, function ($gid) {
                $good = Goods::findOne($gid->good_id);
                $good or $good = (new Goods());
                $info = $good->attributes;
                $info['goods_price'] = $gid->good_price;
                $info['goods_pic'] = $gid->good_pic;
                $info['goods_storage'] = $gid->good_amount;
                $info['ticket_money'] = $gid->ticket->order_price;

                $info['type'] = $gid->ticket->type;
                return (object)$info;
            });

            $reMaisData = TicketApplyGoods::find()->where([
                'and',
                ['in', 'ticket_apply_goods.ticket_id', $ticketIds],
                ['=', 'ticket_apply_goods.type', TicketApplyGoods::TYPE_COUPON],
                ['in', 'ticket_apply_goods.user_id', $usefulStoreIds],
                ['=', 'ticket_apply_goods.is_index', 1],
                ['>', 'ticket_apply_goods.good_amount', 0]
            ])->leftJoin('goods as g', 'g.id = ticket_apply_goods.good_id')->orderBy('g.admin_score desc')->select('ticket_apply_goods.*')->all();
            $reMais = arrayGroupsAction($reMaisData, function ($gid) {
                $good = Goods::findOne($gid->good_id);
                $good or $good = (new Goods());
                $info = $good->attributes;
                $info['goods_price'] = $gid->good_price;
                $info['goods_pic'] = $gid->good_pic;
                $info['goods_storage'] = $gid->good_amount;

                $info['store'] = $good->user;
                $info['type'] = $gid->ticket->type;
                return (object)$info;
            });
        }

       //查询最新报道
        $news=News::find()->andFilterWhere(['cid'=>9])->andFilterWhere(['like','provinces_id',$currentCity])->orderBy('sort desc,createTime DESC')->limit(4)->all();

        //专业知识
        $newClass=new Newsclassify();
        $newsZhuanYe=News::find()->andFilterWhere(['like','provinces_id',$currentCity])->andFilterWhere(['in','cid',$newClass->getLevelSecondId(2)])->orderBy('sort desc, createTime DESC')->limit(5)->all();
        //行业资讯
        $newsHangYe=News::find()->andFilterWhere(['like','provinces_id',$currentCity])->andFilterWhere(['in','cid',$newClass->getLevelSecondId(3)])->orderBy('sort desc, createTime DESC')->limit(5)->all();

       //资讯问答
        $problemNum = FitupProblem::find()->andFilterWhere(['like','provinces_id',$currentCity])->andFilterWhere(['is_del' => 0, 'is_show' => 1])->count();
        $problem= FitupProblem::find()->andFilterWhere(['like','provinces_id',$currentCity])->andFilterWhere(['is_del' => 0, 'is_show' => 1])->orderBy('sort desc')->limit(4)->all();

        $this->render('index', [
            'guiderShow' => true,
            'configIndex' => $configIndex,
            'cityConfigs' => $cityConfigs,
            'askTicketConfig' => $askTicketConfig,
            'indexIndex' => 1,
            'showPromise' => 1,
            'dingJins' => $dingJins,
            'reMais' => $reMais,
            'hasCityTickets'=>$hasCityTickets,
            'basePathData' => $basePathData,
            'news'=>$news,
            'problemNum'=>$problemNum,
            'problem'=>$problem,
            'newsZhuanYe'=>$newsZhuanYe,
            'newsHangYe'=>$newsHangYe
        ]);
    }

    /**
     * 设置默认城市
     */
    public function actionSetCity()
    {
        $provincesId = \Yii::$app->request->get('pr_id', 0);

        $provinces = Provinces::findOne($provincesId);

        $provinces or stopFlow("未找到有效的城市信息");

        $selectCity  = SelectCity::findOne([
            'provinces_id' => $provinces->id
        ]);
        $selectCity or stopFlow("暂不支持该城市");

        CacheService::init()->set($this->webBrowserKey, $selectCity->provinces_id);
        \yii::$app->session->set('citypinyin',$provinces->pinyin);
 		\yii::$app->session->set('citybianhao',$provinces->id);
 		$route=\yii::$app->session->get('routedata');
 		// print_r($route);exit;
 		// return $this->goBack();
        if (isset($route[0]['route'])){

                if ($route[0]['route']=='index/set-city'){
                    return $this->goHome();
                }
            if (empty($route[0]['params'])){
                return $this->redirect([$route[0]['route']]);
            }else{
                if (strpos($route[0]['route'],'jia-zhuang') !== false){



                    if (!empty($route[0]['params']['ticketId'])){
                        $ticket = Ticket::findOne([
                            'ca_type' => Ticket::CA_TYPE_ZHAN_HUI,
                            'type' =>Ticket::TYPE_JIA_ZHUANG,
                            'citys' => $provinces->id,
                            'status' => 1
                        ]);

                        $route[0]['params']['ticketId']=$ticket ? $ticket->id : 0;

                    }
                }else{
                    $route[0]['params']['SecretCityId']=$provinces->pinyin;
                }
//                print_r();exit;
//                print_r($route);exit;
               
                $list=array_merge([$route[0]['route']],$route[0]['params']);
                return $this->redirect($list);
            }
        }

        //header('Location: ' . "/{ ->pinyin}");
       // die;
    }
    // 获取当前城市发行票据


}
