<?php
namespace index\controllers;


use common\models\Collect;
use common\models\Comment;
use common\models\GoodsClass;
use common\models\GoodsCoupon;
use common\models\MemberStoreCoupon;
use common\models\News;
use common\models\StoreGcScore;
use common\models\Ticket;
use common\models\TicketApply;
use common\models\User;
use DL\Project\CityTicket;
use DL\Project\Sms;
use DL\Project\Store;
use DL\vendor\Page;
use wap\models\TicketApplyGoods;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

class DianPuController extends BaseController
{

    /**
     *  家装店铺
     */
    public function actionIndex($storeId=0)
    {
        try {
            $storeInfo = Store::init()->info($storeId);
            $shops = Store::init()->shops($storeId);

            //查询用户领取过的店铺优惠劵   如果打开失败请检查 member_id的参数
            $hasStoreCoupon = [];
            if ($this->memberInfo) {
                $hasStoreCoupon=MemberStoreCoupon::find()->andFilterWhere([
                    'member_id'=>$this->memberInfo->id,
                    'store_id'=>$storeId,
                    'is_expire'=>0,
                    'is_use'=>0,
                    'is_delete'=>0,
                ])->all();
            }

            $hasArr=[];
            foreach ($hasStoreCoupon as $k=>$v){
                $hasArr[]=$v->coupon_id;
            }

            // 当前城市展会
            $cityTickets = CityTicket::init($storeInfo->provinces_id)->currentFactionTickets();

            $isJoinZh = Store::init()->checkJoinZH($storeInfo->user_id);

            $path = (bool)$isJoinZh;
            if ($path) {
                $tickets = CityTicket::init($storeInfo->provinces_id)->currentFactionTickets();
                $ticketIds = array_keys($tickets);
                $ticketIds or $ticketIds = [0];

                $ticket = TicketApply::find()->where([
                    'and',
                    ['in', 'ticket_id', $ticketIds],
                    ['=', 'user_id', $storeInfo->user_id],
                    ['=', 'status', 1],
                ])->one();

                switch ($ticket->ticket->type) {
                    case Ticket::TYPE_JIA_ZHUANG :
                        $path = 'jia-zhuang/index';
                        break;
                    case Ticket::TYPE_JIE_HUN :
                        $path = 'jie-hun/index';
                        break;
                    case Ticket::TYPE_YUN_YING :
                        $path = 'yun-ying/index';
                        break;
                    default :
                        stopFlow('店铺信息异常，请联系管理员');
                }
            } else {
                if ($cityTickets) {
                    $cityTicket = reset($cityTickets);
                    switch ($cityTicket->type) {
                        case Ticket::TYPE_JIA_ZHUANG :
                            $path = 'jia-zhuang/index';
                            break;
                        case Ticket::TYPE_JIE_HUN :
                            $path = 'jie-hun/index';
                            break;
                        case Ticket::TYPE_YUN_YING :
                            $path = 'yun-ying/index';
                            break;
                        default :
                            stopFlow('展会信息异常，请联系管理员');
                    }
                }
            }
            $goods = Store::init()->goods($storeId, [], [0, 12]);

            //店铺评价
            $goodsWithDelete = Store::init()->goods($storeId, [], [], false);
            $goodIdArr=ArrayHelper::getColumn($goodsWithDelete,'id');
            $goodIdArr or $goodIdArr = [0];
            $shopComment=Comment::find()->andFilterWhere(['in','goods_id',$goodIdArr])->limit(2)->all();

            $isCollection=0;  //未收藏
            if (!empty($this->memberInfo)){
                $collect=Collect::find()->andFilterWhere(['member_id'=>$this->memberInfo->id,'type'=>Collect::TYPE_STORE,'shop_id'=>$storeId])->one();
                if (!empty($collect)){
                    $isCollection=1;
                }
            }
            $joinTicketGoodIds = [];
            if ($cityTickets) {
                $dTickets = array_keys($cityTickets);
                $gApplys = TicketApplyGoods::find()->where([
                    'and',
                    ['in', 'ticket_id', $dTickets],
                    ['=', 'user_id', $storeId],
                ])->all();

                $joinTicketGoodIds = ArrayHelper::getColumn($gApplys, 'good_id');
            }

            $this->render('index', [
                'storeInfo' => $storeInfo,
                'shops' => $shops,
                'goods' => $goods,
                'hasStoreCoupon'=>array_unique($hasArr),
                'cityHasZh' => (bool)$cityTickets,    // 城市是否开展展会
                'isJoinZh' => $isJoinZh,    // 是否参加展会
                'zhType' => $path,           // 展会类型
                'shopComment'=>$shopComment,
                'isCollection'=>$isCollection,
                'joinTicketGoodIds' => $joinTicketGoodIds
            ]);
        } catch (\Exception $e) {
            stopFlow($e->getMessage());
        }
    }

    /**
     * 获取店铺门店列表
     */
    public function actionInfos($storeId=0)
    {
        try {
            $type = \Yii::$app->request->get('type', '###');
            in_array($type, ['shops', 'judge', 'description']) or stopFlow("网页逃到火星了~");

            $storeInfo = Store::init()->info($storeId);
            $shops = Store::init()->shops($storeId);

            $storeGoods = Store::init()->goods($storeId, [], [], false);
            $goodIds = $storeGoods ? arrayGroupsAction($storeGoods, function ($storeGood) {
                return $storeGood->id;
            }) : [0];
            $query = Comment::find()->andFilterWhere(['in','goods_id',$goodIds]);
            $amount = $query->count();
            $page = new Page($amount, 15);
            $comments = $query->offset($page->firstRow)->limit($page->listRows)->all();

            $this->render($type, [
                'storeInfo' => $storeInfo,
                'shops' => $shops,
                'type' => $type,
                'comments' => $comments,
                'page' => $page,
            ]);


        } catch (\Exception $e) {
            stopFlow($e->getMessage());
        }
    }

    /**
     * 精选店铺-本地区的
     */
    public  function actionJingXuan($gc_id=0,$goods_class_fid=1)
    {die('dl delete');

        $currentCity = $this->getRequestCity();      //城市id

        //获取当前城市 分类的类型
        $goodClass=GoodsClass::findAll(['fid'=>$goods_class_fid]);
        $goodClassIdArr=ArrayHelper::getColumn($goodClass,'id');

        //筛选当前城市拥有的家装采购类型类型
        $query=StoreGcScore::find()->andFilterWhere(['in','gc_id',$goodClassIdArr])
                                   ->andFilterWhere(['provinces_id'=>$currentCity]);

        $jiaZhuangType=$query->groupBy('gc_id')->all();

        $amount=$query->andFilterWhere(['gc_id'=>($gc_id==0)?'':$gc_id])->groupBy('user_id')->count();
        $page = \Yii::$app->request->get('p', 1);
        $limit = 2;

        $stores =$query->andFilterWhere(['gc_id'=>($gc_id==0)?'':$gc_id])->groupBy('user_id')->offset(($page-1)* $limit)->limit($limit)->all();

        $model = arrayGroupsAction($stores, function ($store) {
            $store = Store::init()->info($store->user_id);
            $goods = [];
            foreach ($store->order_goods AS $type => $goodDatas) {
                foreach ($goodDatas AS $goodData) {
                    $goods[$goodData->id] = $goodData;
                }
            }
            $store->order_goods = array_values($goods);
            return $store;
        });

        $page = new Page($amount, $limit);
        $pageHtml = $page->show();
        $this->render('jing-xuan', [
            'model' => $model,
            'jiaZhuangType'=>$jiaZhuangType,
            'pageHtml'=>$pageHtml,
            'gc_id'=>$gc_id
        ]);
    }

    /**
     * 全部商品
     */
    public function actionGoods($storeId=0)
    {
        try {
            $page = \Yii::$app->request->get('p', 1);
            $limit = 12;

            $storeInfo = Store::init()->info($storeId);
            $amount = Store::init()->goodsAmount($storeId);
            $goods = Store::init()->goods($storeId, [], [($page-1) * $limit, $limit]);

            $page = new Page($amount, $limit);
            $pageHtml = $page->show();

            // 当前城市展会
            $cityTickets = CityTicket::init($storeInfo->provinces_id)->currentFactionTickets();

            $isJoinZh = Store::init()->checkJoinZH($storeInfo->user_id);

            $path = (bool)$isJoinZh;
            if ($path) {
                $tickets = CityTicket::init($storeInfo->provinces_id)->currentFactionTickets();
                $ticketIds = array_keys($tickets);
                $ticketIds or $ticketIds = [0];

                $ticket = TicketApply::find()->where([
                    'and',
                    ['in', 'ticket_id', $ticketIds],
                    ['=', 'user_id', $storeInfo->user_id],
                    ['=', 'status', 1],
                ])->one();

                switch ($ticket->ticket->type) {
                    case Ticket::TYPE_JIA_ZHUANG :
                        $path = 'jia-zhuang/index';
                        break;
                    case Ticket::TYPE_JIE_HUN :
                        $path = 'jie-hun/index';
                        break;
                    case Ticket::TYPE_YUN_YING :
                        $path = 'yun-ying/index';
                        break;
                    default :
                        stopFlow('店铺信息异常，请联系管理员');
                }
            } else {
                if ($cityTickets) {
                    $cityTicket = reset($cityTickets);
                    switch ($cityTicket->type) {
                        case Ticket::TYPE_JIA_ZHUANG :
                            $path = 'jia-zhuang/index';
                            break;
                        case Ticket::TYPE_JIE_HUN :
                            $path = 'jie-hun/index';
                            break;
                        case Ticket::TYPE_YUN_YING :
                            $path = 'yun-ying/index';
                            break;
                        default :
                            stopFlow('展会信息异常，请联系管理员');
                    }
                }
            }

            $joinTicketGoodIds = [];
            if ($cityTickets) {
                $dTickets = array_keys($cityTickets);
                $gApplys = TicketApplyGoods::find()->where([
                    'and',
                    ['in', 'ticket_id', $dTickets],
                    ['=', 'user_id', $storeId],
                ])->all();

                $joinTicketGoodIds = ArrayHelper::getColumn($gApplys, 'good_id');
            }

            $this->render('goods', [
                'storeInfo' => $storeInfo,
                'goods' => $goods,
                'type' => 'goods',
                'pageHtml' => $pageHtml,
                'cityHasZh' => (bool)$cityTickets,    // 城市是否开展展会
                'isJoinZh' => $isJoinZh,    // 是否参加展会
                'zhType' => $path,           // 展会类型
                'joinTicketGoodIds' => $joinTicketGoodIds
            ]);

        } catch (\Exception $e) {
            stopFlow($e->getMessage());
        }

    }

    /**
     *  参展品牌
     */
    public  function actionCanZhan($gc_id=0,$goods_class_fid=1)
    {die('dl delete');

           $currentCity = $this->getRequestCity();    //城市id

//            Store::init()->checkJoinZH()
           //获取参展的商户id数组
           $ticket=CityTicket::init($currentCity)->getTicket($this->formType,Ticket::CA_TYPE_ZHAN_HUI);
           $ticket_id=($ticket)?$ticket->id:0;
           $ticketApply=TicketApply::find()->andFilterWhere(['ticket_id'=>$ticket_id,'status'=>1])->all();
           $attendUserArr=ArrayHelper::getColumn($ticketApply,'user_id');
           if (empty($attendUserArr)){
               $attendUserArr=[0];
           }

        //获取当前城市 分类的类型
        $goodClass=GoodsClass::findAll(['fid'=>$goods_class_fid]);
        $goodClassIdArr=ArrayHelper::getColumn($goodClass,'id');
        //筛选当前城市拥有的家装采购类型类型
        $query=StoreGcScore::find()->andFilterWhere(['in','gc_id',$goodClassIdArr])
                                    ->andFilterWhere(['provinces_id'=>$currentCity])
                                    ->andFilterWhere(['in','user_id',$attendUserArr]);


        $jiaZhuangType=$query->groupBy('gc_id')->all();

        $amount=$query->andFilterWhere(['gc_id'=>($gc_id==0)?'':$gc_id])->count();
        $page = \Yii::$app->request->get('p', 1);
        $limit = 2;

        $stores = $query->andFilterWhere(['gc_id'=>($gc_id==0)?'':$gc_id])->groupBy('user_id')->offset(($page-1)* $limit)->limit($limit)->all();

        $model = arrayGroupsAction($stores, function ($store) {
            return Store::init()->info($store->user_id);
        });
        $page = new Page($amount, $limit);
        $pageHtml = $page->show();
        $this->render('can-zhan', [
            'model' => $model,
            'jiaZhuangType'=>$jiaZhuangType,
            'pageHtml'=>$pageHtml,
            'gc_id'=>$gc_id
        ]);
    }

    /***
     * 领取店铺优惠券
     */
    public  function actionReceiveCoupon($id,$storeId){
        try {
            $json=array('code'=>0,'msg'=>'领取失败');

            if (!$this->memberInfo) {
                return json_encode(['code'=>0, 'msg'=>'用户未登录']);
            }
            $goodCoupon=GoodsCoupon::findOne(['id'=>$id]);

            $memberStore = MemberStoreCoupon::findOne(['coupon_id' =>$id,'member_id'=>$this->memberInfo->id]);
            if (empty($memberStore)){
                $goodCoupon->num += 1;
                $goodCoupon->save();

                $user = User::findOne($goodCoupon->user_id);
                $user and $user->refreshCache();

                $memberStoreCoupon=new  MemberStoreCoupon();
                $memberStoreCoupon->store_id=$storeId;
                $memberStoreCoupon->member_id=$this->memberInfo->id;
                $memberStoreCoupon->coupon_id=$id;
                $memberStoreCoupon->addtime=date('Y-m-d H:i:s');
                $memberStoreCoupon->type=2;
                $memberStoreCoupon->price=$goodCoupon->money;
                if ($goodCoupon->type == 'ticket') {
                    $ticket = Ticket::findOne($goodCoupon->ticket_id);
                    $memberStoreCoupon->begin_time = $ticket->open_date;  //开始时间
                    $memberStoreCoupon->end_time = date('Y-m-d H:i:s', strtotime($ticket->end_date) + 24 * 3600);  //结束时间
                } else {
                    $addDay='+'.$goodCoupon->valid_day.' day';
                    $endTime=date('Y-m-d H:i:s',strtotime($addDay));
                    $memberStoreCoupon->begin_time = date('Y-m-d H:i:s');  //开始时间
                    $memberStoreCoupon->end_time = $endTime;  //结束时间
                }

                $memberStoreCoupon->ticket_id = 0;  //结束时间
                $store = $goodCoupon->getUser();
                $tickets = CityTicket::init($store->provinces_id)->currentFactionTickets();
                if ($tickets) {
                    $ticketIds = array_keys($tickets);
                    $applys = TicketApply::find()->where([
                        'and',
                        ['in', 'ticket_id', $ticketIds],
                        ['=', 'user_id', $store->id],
                        ['=', 'status', 1],
                    ])->one();
                    if ($applys) {
                        $memberStoreCoupon->ticket_id = $applys->ticket_id;
                    }
                }

                if ($memberStoreCoupon->create()){
                    Sms::init()->youHuiQuanYuYue($this->memberInfo->mobile, $memberStoreCoupon->coupon_id, $memberStoreCoupon->ticket_id);

                    $json=array('code'=>1,'msg'=>'领取成功', 'data' => [ 'key' => $memberStoreCoupon->id, 'type'=> 'MEMBER_COUPON']);
                    return json_encode($json);
                }
            }else{
                $json=array('code'=>0,'msg'=>'已经领取过了');
            }

            return json_encode($json);
        } catch (\Exception $e) {
            return json_encode(array('code'=>0,'msg'=> $e->getMessage()));
        }
    }

    /**
     * 品牌活动
     */
    public  function  actionBrandActivity(){
        die('dl-stop');
        $currentCity = $this->getRequestCity();    //城市id
        $query=News::find()->andFilterWhere(['provinces_id'=>$currentCity,'type'=>$this->formType]);
        $amount=$query->count();
        $page = \Yii::$app->request->get('p', 1);
        $limit = 3;
        $newsAll=$query->offset(($page-1)* $limit)->limit($limit)->all();
        $page = new Page($amount, $limit);
        $pageHtml = $page->show();
        $this->render('brand-activity',[
            'newsAll'=>$newsAll,
            'pageHtml'=>$pageHtml,
        ]);
    }
    /**
     * 品牌活动详情页
     */
    public  function actionBrandActivityDetail(){

        $data=\Yii::$app->request->get();

        $news=News::findOne($data['id']);
        $this->render('brand-activity-detail',[
            'news'=>$news
        ]);
     }



}
