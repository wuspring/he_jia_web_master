<?php

namespace index\controllers;

use common\models\FitupAnswer;
use common\models\FitupAppointment;
use common\models\FitupAttr;
use common\models\FitupCase;
use common\models\FitupProblem;
use common\models\News;
use common\models\Newsclassify;
use DL\Project\CityTicket;
use DL\Project\FloorConfig;
use DL\service\CacheService;
use DL\vendor\ConfigService;
use DL\vendor\Page;
use yii\helpers\ArrayHelper;

/**
 * Class IntegralController
 * @package index\controllers
 *  装修学堂
 */
class FitupSchoolController extends BaseTicketController
{

    public function actionIndex()
    {
        $cityId = $this->getRequestCity();
        $problemNum = FitupProblem::find()->andFilterWhere(['like','provinces_id',$cityId])->andFilterWhere(['is_del' => 0, 'is_show' => 0])->andFilterWhere(['>', 'answer_num', 0])->count();

        $newsClass=Newsclassify::find()->andFilterWhere(['fid'=>0,'isShow'=>1])->limit(8)->all();

        $fitup_case=FitupCase::find()->andFilterWhere(['like','provinces_id',$cityId])->orderBy('create_time DESC')->limit(5)->all();
        $fitup_case_style=FitupAttr::find()->andFilterWhere(['pid'=>2])->limit(8)->all();

        return $this->render('index', [
            'pageIndexKey' => 1,
            'newsClass'=>$newsClass,
            'problemNum' => $problemNum,
            'fitup_case'=>$fitup_case,
            'fitup_case_style'=>$fitup_case_style,
            'cityId'=>$cityId
        ]);
    }

    /**
     * @return string|void
     * 装修资讯
     */
    public function actionInformation()
    {
        $cityId = $this->getRequestCity();
        $problemNum = FitupProblem::find()->andFilterWhere(['like','provinces_id',$cityId])->andFilterWhere(['is_del' => 0, 'is_show' => 0])->andFilterWhere(['>', 'answer_num', 0])->count();

        $newsClass=Newsclassify::find()->andFilterWhere(['fid'=>0,'isShow'=>1])->limit(8)->all();
        //推荐热门
        $hotNews=News::find()->andFilterWhere(['isRmd'=>1,'isShow'=>1])->andFilterWhere(['like','provinces_id',$cityId])->limit(8)->all();


        return $this->render('information', [
            'pageIndexKey' => 1,
            'newsClass'=>$newsClass,
            'problemNum' => $problemNum,
            'cityId'=>$cityId,
            'hotNews'=>$hotNews,
        ]);
    }

    /**
    * 装修案例
    */
    public  function actionFitupCase(){
        $cityId = $this->getRequestCity();
        $fitupAttr=FitupAttr::findAll(['pid'=>0]);
        $data=\Yii::$app->request->get();
        $sql=FitupCase::find()->andFilterWhere(['like','provinces_id',$cityId]);

        $sql->andFilterWhere([
            'house_type'=>isset($data['cx0'])?$data['cx0']:'',
            'design_style'=>isset($data['cx1'])?$data['cx1']:'',
        ]);
        $sql->andFilterWhere([
          'like','tab',isset($data['cx2'])?$data['cx2']:'',
        ]);
        if(isset($data['paixu']) && $data['paixu']=='zx'){
            $order='create_time DESC';
        }else{
            $order='';
        }
        if(isset($data['paixu']) &&$data['paixu']=='tj'){
            $sql->andFilterWhere(['is_recommend'=>1]);
        }
        $amount=$sql->count();
        $limit = 6;
        $page = new Page($amount, $limit);
        $pageHtml = $page->show();
        $model = $sql->offset($page->firstRow)->limit($limit)->orderBy($order)->all();

        $this->render('fitup_case',[
            'model' => $model,
            'pageHtml' => $pageHtml,
            'fitupAttr'=>$fitupAttr,
            'amount'=>$amount
        ]);
    }
    /**
     * 装修案例 详情
     */
    public  function actionFitupCaseDetail($caseId){

        $fitupCase=FitupCase::findOne($caseId);
        $fitupCase->tab_text=json_decode($fitupCase->tab_text);

        if (strlen($fitupCase->name)) {
            global $title;
            $title = $fitupCase->name;
        }
        if (strlen($fitupCase->keywords)) {
            global $keywords;
            $keywords = $fitupCase->keywords;
        }
        if (strlen($fitupCase->description)) {
            global $description;
            $description = $fitupCase->description;
        }

        $this->render('fitup_casedetail',[
            'fitupCase'=>$fitupCase
        ]);
    }

    /**
     * @return string|void
     * 提交问题
     */

    public function actionSubmitProblem()
    {
        $cityId = $this->getRequestCity();
        $data = \Yii::$app->request->get();
        $json = array('code' => 0, 'msg' => '提问失败');
        if (\Yii::$app->user->isGuest) {
            $json = array('code' => 2, 'msg' => '请先登录');
        } else {
            $model = new  FitupProblem();
            $model->problem = $data['text'];
            $model->member_id = $this->memberInfo->id;
            $model->create_time = date('Y-m-d H:i:s');
            $model->provinces_id = $cityId;
            if ($model->save()) {
                $json = array('code' => 1, 'msg' => '提问成功');
            }
        }

        return json_encode($json);
    }

    /**
     * 回答
     */
    public function actionAnswer($param = 0)
    {
        $cityId = $this->getRequestCity();

        $problemNum = FitupProblem::find()->andFilterWhere(['is_del' => 0, 'is_show' => 1])->andFilterWhere(['>', 'answer_num', 0])->count();
        $sql = FitupProblem::find()->andFilterWhere(['is_del' => 0, 'is_show' => 1]);

        switch ($param) {
            case 0:
                $amount = $sql->count();
                break;
            case 1:
                $sql->andFilterWhere(['>', 'answer_num', 0]);
                $amount = $sql->count();
                break;
            case 2:
                $sql->andFilterWhere(['answer_num' => 0]);
                $amount = $sql->count();
                break;
        }

        $limit = 5;
        $page = new Page($amount, $limit);
        $pageHtml = $page->show();
        $model = $sql->offset($page->firstRow)->limit($limit)->orderBy('create_time DESC')->all();

        return $this->render('answer', [
            'pageIndexKey' => 1,
            'model' => $model,
            'param' => $param,
            'pageHtml' => $pageHtml,
            'problemNum' => $problemNum
        ]);
    }


    /**
     *  点击进入详情
     */
    public function actionAnswerDetail($id = 0)
    {
        $problemNum = FitupProblem::find()->andFilterWhere(['is_del' => 0, 'is_show' => 0])->andFilterWhere(['>', 'answer_num', 0])->count();
        $problem = FitupProblem::findOne($id);
        $is_login=1;
        if (\Yii::$app->user->isGuest){
            $is_login=0;
        }

        if (strlen($problem->problem)) {
            global $title;
            $title = $problem->problem;
        }

        return $this->render('answer-detail', [
            'problem' => $problem,
            'problemNum' => $problemNum,
            'is_login'=>$is_login
        ]);
    }

    /**
     *  用户回答的问题
     */
    public function actionMemberAnswer()
    {
        $data = \Yii::$app->request->get();
        $json = array('code' => 0, 'msg' => '信息错误');
        if (\Yii::$app->user->isGuest) {
            $json = array('code' => 2, 'msg' => '请先登录');
        } else {
            $fitupAnswer = new FitupAnswer();
            $fitupAnswer->problem_id = $data['problemId'];
            $fitupAnswer->member_id = $this->memberInfo->id;
            $fitupAnswer->content = $data['text'];
            $fitupAnswer->create_time = date('Y-m-d H:i:s');
            if ($fitupAnswer->save()) {
                $problemModel = FitupProblem::findOne($data['problemId']);
                $problemModel->answer_num += 1;
                $problemModel->save();
                $json = array('code' => 1, 'msg' => '添加成功');
            } else {
                $json = array('code' => 0, 'msg' => '添加失败');
            }
        }

        return json_encode($json);
    }


    /**
     *  资讯列表
     */
    public function actionNewsList($pid=1)
    {
        $cityId = $this->getRequestCity();
        $newsclassify=Newsclassify::find()->andFilterWhere(['fid'=>0,'isShow'=>1])->orderBy('sort ASC')->limit(8)->all();
        $newsclassify_children=Newsclassify::find()->andFilterWhere(['fid'=>$pid,'isShow'=>1])->orderBy('sort ASC')->all();
        //推荐热门
        $hotNews=News::find()->andFilterWhere(['isRmd'=>1,'isShow'=>1])->andFilterWhere(['like','provinces_id',$cityId])->limit(8)->all();

        //展会索票
        $ticketId = $this->getTicketId();



        return $this->render('newslist', [
            'pageIndexKey' => 1,
            'newsclassify'=>$newsclassify,
            'pid'=>$pid,
            'newsclassify_children'=>$newsclassify_children,
            'hotNews'=>$hotNews,
            'cityId'=>$cityId
        ]);
    }

    /**
     *  资讯列表 ---更多
     */
    public function actionNewsListMore($newsClassId)
    {
        $cityId = $this->getRequestCity();
        $getClassName=Newsclassify::findOne($newsClassId);
        $newsclassify=Newsclassify::find()->andFilterWhere(['fid'=>$getClassName->fid,'isShow'=>1])->orderBy('sort ASC')->limit(8)->all();

        $sql=News::find()->andFilterWhere(['cid'=>$newsClassId])->andFilterWhere(['like','provinces_id',$cityId]);

        $amount = $sql->count();
        $limit = 10;
        $page = new Page($amount, $limit);
        $pageHtml = $page->show();
        $model = $sql->offset($page->firstRow)->limit($limit)->all();

        //推荐热门
        $hotNews=News::find()->andFilterWhere(['isRmd'=>1,'isShow'=>1])->andFilterWhere(['like','provinces_id',$cityId])->limit(8)->all();
        return $this->render('newslistmore', [
            'pageIndexKey' => 1,
            'newsclassify'=>$newsclassify,
            'newsClassId'=>$newsClassId,
            'pageHtml'=>$pageHtml,
            'model'=>$model,
            'getClassName'=>$getClassName,
            'hotNews'=>$hotNews
        ]);
    }
    /**
     * 装修资讯详情
     */
    public  function actionNewsdetail($newsId){

        $news=News::findOne($newsId);
        $news->clickRate+=1;
        $news->save();

        if (strlen($news->title)) {
            global $title;
            $title = $news->title;
        }
        if (strlen($news->seokey)) {
            global $keywords;
            $keywords = $news->seokey;
        }
        if (strlen($news->seoDepict)) {
            global $description;
            $description = $news->seoDepict;
        }

        $relationNews=News::find()->andFilterWhere(['cid'=>$news->cid])->limit(4)->all();

        //推荐热门
        $hotNews=News::find()->andFilterWhere(['isRmd'=>1,'isShow'=>1])->limit(8)->all();
        return $this->render('newsdetail',[
            'news'=>$news,
            'relationNews'=>$relationNews,
            'hotNews'=>$hotNews
        ]);
    }

    /**
     * 预约装修
     */
    public  function  actionAppointmentFitup(){

        $json=array('code'=>0,'msg'=>'预约失败');
        $data=\Yii::$app->request->get();
        if (\Yii::$app->user->isGuest){
            $json=array('code'=>0,'msg'=>'请先登录');
        }else{

            if(empty($data['name'])){
                $json=array('code'=>0,'msg'=>'姓名不能为空');
                return json_encode($json);
            }
            if (empty($data['mobile'])){
                $json=array('code'=>0,'msg'=>'手机号不能为空');
                return json_encode($json);
            }else{
                if(!preg_match("/^1[345678]{1}\d{9}$/",$data['mobile'])){
                    $json=array('code'=>0,'msg'=>'手机号格式不正确');
                    return json_encode($json);
                }
            }

            $fitupAppoint=new  FitupAppointment();
            $fitupAppoint->member_id=$this->memberInfo->id;
            $fitupAppoint->name=$data['name'];
            $fitupAppoint->mobile=$data['mobile'];
            $fitupAppoint->create_time=date('Y-m-d H:i:s');
            if ($fitupAppoint->save()){
                $json=array('code'=>1,'msg'=>'预约已经提交');
            }

        }
        return json_encode($json);
    }

    public function render($view, $params = [])
    {
        $cityId = $this->getRequestCity();

        $tickets = CityTicket::init($cityId)->currentFactionTickets();
        $ticket = $tickets ? reset($tickets) : [];

        parent::render($view, array_merge(['ticket' => $ticket], $params));
    }

}
