<?php

namespace index\controllers;

use chenkby\region\RegionAction;
use common\models\Address;
use common\models\AddressMember;
use common\models\Cart;
use common\models\Collect;
use common\models\Comment;
use common\models\Goods;
use common\models\History;
use common\models\IntergralOrder;
use common\models\IntergralOrderGoods;
use common\models\Member;
use common\models\MemberStoreCoupon;
use common\models\Order;
use common\models\OrderGoods;
use common\models\Payment;
use common\models\Provinces;
use common\models\StoreInfo;
use common\models\Ticket;
use common\models\TicketApplyGoods;
use common\models\IntegralLog;
use common\models\UploadForm;
use common\models\User;
use common\search\IntergralGoodsSearch;
use common\search\IntergralOrderSearch;
use DL\vendor\ConfigService;
use DL\vendor\Page;
use DL\WechatRefund;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class PersonalController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']    //验证是否登录
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    $login = 'Location:' . Yii::$app->request->hostInfo.'/index.php?r=member/login';
                    header($login);
                },
            ],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['get-region'] = [
            'class' => RegionAction::className(),
            'model' => Provinces::className()
        ];
        return $actions;
    }

    /**
     * @return string|void
     *  用户信息
     */
    public function actionIndex()
    {

        $model = Member::findOne(['id' => $this->memberInfo->id]);

        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            ($data['avatar']) ? $model->avatar = $data['avatar'] : '';
            ($data['avatarTm']) ? $model->avatarTm = $data['avatarTm'] : '';
            if (isset($data['name'])) {
                $model->nickname = $data['name'];
                $model->wxnick = $data['name'];
            }
            ($data['sex']) ? $model->sex = $data['sex'] : '';
//            $model->sex = 1;
            $bir = $data['year'] . '-' . $data['moth'] . '-' . $data['day'];
            $model->birthday_year = $bir;
            $model->save();
        }

        $this->render('index', [
            'model' => $model,
            'pageIndexKey' => 5
        ]);
    }

    /**
     * @return string|void
     * 积分订单
     */
    public function actionIntegralOrder()
    {
        $model = Member::findOne(['id' => $this->memberInfo->id]);
        $data=Yii::$app->request->get();
        $orderNum=isset($data['orderid'])?$data['orderid']:'';

        $near_time=isset($data['near_time'])?$data['near_time']:'';
        $order_status=isset($data['order_status'])?$data['order_status']:'';

        $query=IntergralOrder::find()->andFilterWhere([
            'buyer_id'=>$model->id,
            'order_state'=>$order_status,
        ])->andFilterWhere(['like','orderid',$orderNum]);

        switch ($near_time){
            case 'all':
                break;
            case  'week':
                $time= date("Y-m-d H:i:s", strtotime("-1 week"));
                $query->andFilterWhere(['>','add_time',$time]);
                break;
            case 'month':
                $time= date("Y-m-d H:i:s", strtotime("-1 month"));
                $query->andFilterWhere(['>','add_time',$time]);
                break;
            case 'threeMonth':
                $time= date("Y-m-d H:i:s", strtotime("-3 month"));
                $query->andFilterWhere(['>','add_time',$time]);
                break;
        }


        $amount=$query->count();
        $page = \Yii::$app->request->get('p', 1);
        $limit = 10;
        $orders = $query->offset(($page-1)* $limit)->orderBy('add_time DESC')->limit($limit)->all();
        $page = new Page($amount, $limit);
        $pageHtml = $page->show();


        return $this->render('integral-order', [
            'model' => $model,
            'orders' => $orders,
            'page' => $page,
            'pageHtml'=>$pageHtml,
            'pageIndexKey' => 1
        ]);
    }

    public function actionReceivedIntergral($number)
    {
        $order = IntergralOrder::findOne([
            'orderid' => $number,
            'order_state' => IntergralOrder::STATUS_WAIT_RECIVE
        ]);

        $order or apiSendError("未找到该订单信息");
        $order->order_state = IntergralOrder::STATUS_FINISH;
        $order->save() and apiSendSuccess();

        apiSendError("确认收货失败");
    }

    /**
     * @return string|void
     * 积分订单 详情
     */
    public function actionIntegralOrderDetail()
    {
        $data=Yii::$app->request->get();
        $model = Member::findOne(['id' => $this->memberInfo->id]);
        $intergralOrder=IntergralOrder::find()->andFilterWhere(['orderid'=>$data['number']])->one();
        $intergralOrderGood=IntergralOrderGoods::find()->andFilterWhere(['order_id'=>$data['number']])->all();

        return $this->render('integral-order-detail', [
            'model' => $model,
            'intergralOrder'=>$intergralOrder,
            'intergralOrderGood'=>$intergralOrderGood
            ]);
    }

    /**
     * @return string|void
     * 积分订单取消
     */
    public function actionIntegralOrderCancel()
    {
        $json=array('code'=>0,'msg'=>'错误提示');
        $data=Yii::$app->request->get();
        $intergralOrder=IntergralOrder::find()->andFilterWhere(['orderid'=>$data['number']])->one();
        if (!empty($intergralOrder)){
            $intergralOrder->order_state=IntergralOrder::STATUS_CLOSE;
            $intergralOrder->save();
            $json=array('code'=>1,'msg'=>'取消成功');
        }
        return json_encode($json);
    }


    /**
     * 优惠劵核销订单
     */
    public  function actionCouponVerifitionOrder(){

        $data=Yii::$app->request->get();
        $model = Member::findOne(['id' => $this->memberInfo->id]);
        $orderNum=isset($data['orderid'])?$data['orderid']:'';

        $near_time=isset($data['near_time'])?$data['near_time']:'';
        $order_status=isset($data['order_status'])?$data['order_status']:'';

        $query=Order::find()->andFilterWhere([
            'buyer_id'=>$this->memberInfo->id,
            'type' => Order::STORE_TYPE_COUPON,
            'order_state'=>$order_status,
        ])->andFilterWhere(['like','orderid',$orderNum]);

        switch ($near_time) {
            case 'all':
                break;
            case  'week':
                $time= date("Y-m-d H:i:s", strtotime("-1 week"));
                $query->andFilterWhere(['>','add_time',$time]);
                break;
            case 'month':
                $time= date("Y-m-d H:i:s", strtotime("-1 month"));
                $query->andFilterWhere(['>','add_time',$time]);
                break;
            case 'threeMonth':
                $time= date("Y-m-d H:i:s", strtotime("-3 month"));
                $query->andFilterWhere(['>','add_time',$time]);
                break;
        }

        $amount=$query->count();
        $page = \Yii::$app->request->get('p', 1);
        $limit = 10;
        $order = $query->offset(($page-1)* $limit)->orderBy('add_time DESC')->limit($limit)->all();
        $page = new Page($amount, $limit);
        $pageHtml = $page->show();

        return $this->render('coupon-verifition-order',
        [
            'order'=>$order,
            'pageHtml'=>$pageHtml,
            'model'=>$model,
            'pageIndexKey' => 11
        ]);
    }
    /**
     * @return string|void
     * 积分订单 详情
     */
    public function actionIntegralOrderDetail2()
    {
        $model = Member::findOne(['id' => $this->memberInfo->id]);
        return $this->render('integral-order-detail2',
            ['model' => $model]);
    }


    /**
     * @return string|void
     * 订单 评价
     */
    public function actionOrderComment($orderId)
    {
        $model = Member::findOne(['id' => $this->memberInfo->id]);
        $order=Order::findOne(['orderid'=>$orderId]);
        return $this->render('order-evaluate', ['model' => $model,'order'=>$order]);
    }

    /**
     * @return string|void
     * 积分订单  物流详情
     */
    public function actionIntegralOrderLogistics()
    {
        $model = Member::findOne(['id' => $this->memberInfo->id]);
        return $this->render('integral-order-logistics', ['model' => $model]);
    }

    /**
     *  我的地址
     */
    public function actionMyAddress($addressId=0)
    {
        $model = Member::findOne(['id' => $this->memberInfo->id]);
        $alladdress = AddressMember::findAll(['memberId' => $this->memberInfo->id]);
        if ($addressId==0){
            $address = new AddressMember();
        }else{
            $address =AddressMember::findOne($addressId);
        }
        if ($address->load(Yii::$app->request->post()) && $address->validate()) {

            $address->memberId = $this->memberInfo->id;
            if ($address->isDefault == 1) {
                Address::updateAll(['isDefault' => 0], ['memberId' => $this->memberInfo->id]);
            }
            if ($address->save()) {
                return $this->redirect(['personal/my-address']);
            }
        }

        return $this->render('my-address', [
            'model' => $model, 'address' => $address, 'alladdress' => $alladdress,
            'pageIndexKey' => 4
            ]);
    }




    /**
     * 删除地址
     */
    public function actionDeleteAddress($id)
    {
        $json = array('code' => 0, 'msg' => '删除失败');
        $is_delete = Address::deleteAll(['id' => $id]);
        if ($is_delete > 0) {
            $json = array('code' => 1, 'msg' => '删除成功');
        }

        return json_encode($json);
    }

    /**
     * 设为默认地址
     */
    public function actionSetAddress($id)
    {
        $json = array('code' => 0, 'msg' => '设置失败');
        $allAddress = Address::findAll(['memberId' => $this->memberInfo->id]);
        foreach ($allAddress as $k => $v) {

            if ($id == $v->id) {
                $res = Address::updateAll(['isDefault' => 1], ['id' => $v->id]);
                if ($res > 0) {
                    $json = array('code' => 1, 'msg' => '设置成功');
                }
            } else {
                Address::updateAll(['isDefault' => 0], ['id' => $v->id]);
            }
        }

        return json_encode($json);
    }

    /**
     *  我们的信息
     */
    public function actionMyInfo()
    {
        $model = Member::findOne($this->memberInfo->id);
        return $this->render('my-info', ['model' => $model]);
    }

    /**
     * 账户安全
     */
    public function actionMyAccount()
    {
        $model = Member::findOne($this->memberInfo->id);
        return $this->render('my-account', [
            'model' => $model,
            'pageIndexKey' => 6
        ]);
    }

    /**
     *  修改密码
     */
    public function actionMyAccountPass()
    {
        $model = Member::findOne($this->memberInfo->id);

        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $json = array('code' => 0, 'msg' => "两次密码输入不正确");
            if ($data['newPwd'] == $data['newPwdTwo']) {
                $is_agree = Yii::$app->security->validatePassword($data['oldPwd'], $model->password);
                if ($is_agree) {
                    $model->password = Yii::$app->security->generatePasswordHash($data['newPwd']);
                    $model->save();
                    $json = array('code' => 1, 'msg' => "修改成功");
                }
            }
            return json_encode($json);
        }


        return $this->render('my-account-pass', [
            'model' => $model,
            'pageIndexKey' => 6
            ]);
    }


    /**
     *  手机验证
     */
    public function actionMyAccountPhone()
    {
        $json = array('code' => 0, 'msg' => "绑定失败");
        $session = Yii::$app->session;

        $model = Member::findOne($this->memberInfo->id);

        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $sms_code = $session->get('SMS_CODE');
            if (!$sms_code) {
                return ['code' => 0, 'msg' => "验证码无效"];
            }

            if ($sms_code['expire'] < time()) {
                return ['code' => 0, 'msg' => "验证码已过期"];
            }

            $yzm = explode('-', $sms_code['value']);
            if ($yzm[1] == $data['yzm']) {
                $json = array('code' => 1, 'msg' => "绑定成功");
                return json_encode($json);
            } else {
                return $yzm;
            }

        }
        return $this->render('my-account-phone', [
            'model' => $model,
            'pageIndexKey' => 6
        ]);
    }

    /**
     * 商品的 点击收藏 和 取消
     */
    public function actionClickCollection(){

        $data=Yii::$app->request->get();
        $collectType=isset($data['is_collect']) ?$data['is_collect']:1;

        $good_id=isset($data['good_id']) ?$data['good_id']:0;

        $json = array('code' => 0);

        if (Yii::$app->user->isGuest){
            $json=array('code' => 0,'msg' => '请先登录');
            return json_encode($json);
        }
        if ($collectType==1){     //取消收藏
           Collect::deleteAll(['type'=>Collect::TYPE_GOOD,'member_id'=>$this->memberInfo->id,'goods_id'=>$good_id]);
            $json=array('code' => 1,'msg' => '取消成功');
        }else{
            $collect=new  Collect();
            $collect->member_id=$this->memberInfo->id;
            $collect->goods_id=$good_id;
            $collect->create_time=date('Y-m-d');
            $collect->type=Collect::TYPE_GOOD;
            $collect->save();
            $json=array('code' => 1,'msg' => '收藏成功');
        }
        return json_encode($json);
    }


    /**
     * 店铺的 点击收藏 和 取消
     */
    public function actionClickCollectionStore(){

        $data=Yii::$app->request->get();
        $collectType=isset($data['is_collect']) ?$data['is_collect']:1;

        $shop_id=isset($data['shop_id']) ?$data['shop_id']:0;

        $json = array('code' => 0);

        if (Yii::$app->user->isGuest){
            $json=array('code' => 0,'msg' => '请先登录');
            return json_encode($json);
        }
        if ($collectType==1){     //取消收藏
            Collect::deleteAll(['type'=>Collect::TYPE_STORE,'member_id'=>$this->memberInfo->id,'shop_id'=>$shop_id]);
            $json=array('code' => 1,'msg' => '取消成功');
        }else{
            $collect=new  Collect();
            $collect->member_id=$this->memberInfo->id;
            $collect->shop_id=$shop_id;
            $collect->create_time=date('Y-m-d');
            $collect->type=Collect::TYPE_STORE;
            $collect->save();
            $json=array('code' => 1,'msg' => '收藏成功');
        }
        return json_encode($json);
    }
    /**
     * 我的收藏
     */
    public  function actionMyCollection(){

        $data=Yii::$app->request->get();
        $type=isset($data['type']) ? $data['type']: Collect::TYPE_GOOD;

        $query=Collect::find()->andFilterWhere(['type'=>$type,'member_id'=>$this->memberInfo->id]);

        $amount=$query->count();
        $page = \Yii::$app->request->get('p', 1);
        $limit = 8;
        $collect = $query->offset(($page-1)* $limit)->orderBy('create_time DESC')->limit($limit)->all();
        $collectDay=empty(ArrayHelper::getColumn($collect,'create_time')) ? [] : array_unique(ArrayHelper::getColumn($collect,'create_time'));
        $page = new Page($amount, $limit);
        $pageHtml = $page->show();

        $renderUrl= ($type == Collect::TYPE_GOOD) ? 'mycollection' : 'mycollectionstore';
        return $this->render($renderUrl,[
            'pageIndexKey' => 12,
            'pageHtml' => $pageHtml,
            'collect' => $collect,
            'collectDay'=>$collectDay
        ]);

    }


    /**
     * 店铺中的取消商品和取消店铺
     */
    public  function  actionCancelMyCollect(){

        $data=Yii::$app->request->get();
        $cancelType=isset($data['collectType']) ? $data['collectType'] : '###';
        $json = array('code'=>0);
        switch ($cancelType){
            case Collect::TYPE_GOOD :
                Collect::deleteAll(['id'=>$data['collectId']]);
                $json = array('code'=>1,'msg'=>'取消成功');
                break;
            case Collect::TYPE_STORE:
                Collect::deleteAll(['id'=>$data['collectId']]);
                $json = array('code'=>1,'msg'=>'取消成功');
                break;
            default:
                $json = array('code'=>0,'msg'=>'操作异常');
        }
        return json_encode($json);
    }


    /**
     * 我的足迹
     */
    public function actionMyHistory()
    {
        $model = Member::findOne($this->memberInfo->id);

        $history = History::find()->andFilterWhere(['member_id' =>$this->memberInfo->id])->orderBy("create_time DESC")->all();
        $timeArr = [];
        foreach ($history as $k => $v) {
            $timeArr[] = date('Y-m-d', strtotime($v->create_time));
        }
        $timeArr = array_unique($timeArr);
        return $this->render('my-history', [
            'model' => $model,
            'history' => $history,
            'timeArr' => $timeArr,
            'pageIndexKey' => 7
        ]);
    }



    /**
     *  删除我的足迹
     */
    public function actionDeleteHistory($id)
    {

        $json = array('code' => 0, 'msg' => '删除失败');
        $res = History::deleteAll(['id' => $id]);
        if ($res > 0) {
            $json = array('code' => 1, 'msg' => '删除成功');
        }
        return json_encode($json);
    }

    /**
     *  添加购物车
     */
    public function actionAddCart($good_id)
    {

        $json = array('code' => 0, 'msg' => '添加购物车失败');
        $cart = Cart::findOne(['goodsId' => $good_id]);
        if ($cart) {
            $json = array('code' => 1, 'msg' => '已经添加购物车');
        } else {
            $addcart = new Cart();
            $addcart->buyerId = 1;
            $addcart->goodsId = $good_id;
            $addcart->save();
            $json = array('code' => 1, 'msg' => '添加购物车成功');
        }
        return json_encode($json);
    }


    /**
     * 我的充值
     */
    public function actionMyRecharge()
    {
        $model = Member::findOne($this->memberInfo->id);


        return $this->render('my-recharge', [
            'model' => $model,
            'pageIndexKey' => 8
        ]);
    }

    /**
     * 我的积分
     */
    public function actionMyIntegral()
    {
        $model = Member::findOne(['id' => $this->memberInfo->id]);
        $query=IntegralLog::find()->andFilterWhere(['memberId'=>$this->memberInfo->id]);

        $amount = $query->count();
        $page = new Page($amount, 6);
        $integrallon = $query->offset($page->firstRow)->limit($page->listRows)->orderBy('create_time DESC')->all();
        $pageHtml = $page->show();

        $explain = ConfigService::init(ConfigService::INTEGRAL)->get("explain");
        $signIntegral = ConfigService::init(ConfigService::INTEGRAL)->get("sign_integral");

        $signLogDatas = IntegralLog::find()->where([
            'and',
            ['=', 'memberId', $this->memberInfo->id],
            ['>=', 'create_time', date('Y-m-d')],
        ])->all();
        $isSign = 0;
        $signLogs = arrayGroupsAction($signLogDatas, function ($signLogData) use (&$isSign) {
            $day = date('d', strtotime($signLogData->create_time));
            $isSign or $isSign = (int)date('d') == $day;

            return ['signDay' => $day];
        });

        return $this->render('my-integral', [
            'model' => $model,
            'explain' => $explain,
            'signIntegral' => $signIntegral,
            'signLogs' => $signLogs,
            'isSign' => $isSign,
            'page' => $page,
            'pageHtml'=>$pageHtml,
            'integrallon'=>$integrallon,
            'pageIndexKey' => 9,
        ]);

    }

    /**
     *  我的优惠券
     */
    public function actionMyCoupon()
    {
        $nowTime=date('Y-m-d H:i:s');
        //查询店铺优惠劵

        $model = Member::findOne(['id' => $this->memberInfo->id]);
        $query = MemberStoreCoupon::find()->andFilterWhere(['member_id' => $this->memberInfo->id,'is_delete'=>0]);

        $amount = $query->count();
        $page = new Page($amount, 6);
        $memberStoreCoupon = $query->offset($page->firstRow)->limit($page->listRows)->orderBy('addtime DESC')->all();
        $pageHtml = $page->show();

        //变更哪些过去的优惠劵
        foreach ($memberStoreCoupon as &$v){
            if ($v->end_time<= $nowTime && $v->is_expire==0){
                MemberStoreCoupon::updateAll(['is_expire'=>1],['id'=>$v->id]);
                $v->is_expire=1;
            }
        }
        return $this->render('my-coupon', [
            'memberStoreCoupon' => $memberStoreCoupon,
            'model'=>$model,
            'pageIndexKey' => 10,
            'pageHtml'=>$pageHtml
        ]);
    }

    /**
     *  查询优惠劵
     */
    public  function  actionMemberCoupon($id){

        $json=array('code'=>0,'msg'=>'查询错误');
        $coupon=MemberStoreCoupon::findOne($id);
        if (!empty($coupon)){
            $json['code']=1;
            $json['code_verify']=$coupon->code;
            //判断使用范围
            if (empty($coupon->good_id)){
                $json['content']='店铺通用';
            }else{
                $good=Goods::findOne($coupon->good_id);
                $json['content']=$good->goods_name;
            }
            //判断状态
            if ($coupon->is_use==0){
                if ($coupon->is_expire==0){
                    $json['state']='请使用';
                }else{
                    $json['state']='已过期';
                }
            }else{
                $json['state']='已使用';
            }
            $json['msg']='查询成功';
        }

        return json_encode($json);
    }
    /**
     *  预约订单
     */
    public function actionCouponOrder(){
        $data=Yii::$app->request->get();
        $model = Member::findOne(['id' => $this->memberInfo->id]);
        $orderNum=isset($data['orderid'])?$data['orderid']:'';
        $near_time=isset($data['near_time'])?$data['near_time']:'';
        $order_status=isset($data['order_status'])?$data['order_status']:'';

        $query=Order::find()->andFilterWhere([
            'buyer_id'=>$this->memberInfo->id,
            'type'=>Order::TYPE_ZHAN_HUI,
            'order_state'=>$order_status,
            'ticket_type' => TicketApplyGoods::TYPE_COUPON
        ])->andFilterWhere(['like','orderid',$orderNum]);

        isset($data['order_state']) and $query->andFilterWhere(['=','order_state',$data['order_state']]);

        switch ($near_time){
            case 'all':
                break;
            case  'week':
                $time= date("Y-m-d H:i:s", strtotime("-1 week"));
                $query->andFilterWhere(['>','add_time',$time]);
                break;
            case 'month':
                $time= date("Y-m-d H:i:s", strtotime("-1 month"));
                $query->andFilterWhere(['>','add_time',$time]);
                break;
            case 'threeMonth':
                $time= date("Y-m-d H:i:s", strtotime("-3 month"));
                $query->andFilterWhere(['>','add_time',$time]);
                break;
        }

        $amount=$query->count();
        $page = \Yii::$app->request->get('p', 1);
        $limit = 10;
        $order = $query->offset(($page-1)* $limit)->orderBy('add_time DESC')->limit($limit)->all();
        $page = new Page($amount, $limit);
        $pageHtml = $page->show();

        return $this->render('coupon-order',[
            'order'=>$order,
            'model'=>$model,
            'pageHtml'=>$pageHtml,
            'pageIndexKey' => 2
        ]);
    }

    /**
     *  定金订单
     */
    public function actionAppointmentOrder(){

        $data=Yii::$app->request->get();
        $model = Member::findOne(['id' => $this->memberInfo->id]);
        $orderNum=isset($data['orderid'])?$data['orderid']:'';

        $near_time=isset($data['near_time'])?$data['near_time']:'';
        $order_status=isset($data['order_status'])?$data['order_status']:'';

        $query=Order::find()->andFilterWhere([
            'buyer_id'=>$this->memberInfo->id,
            'type'=>Order::TYPE_ZHAN_HUI,
            'ticket_type' => TicketApplyGoods::TYPE_ORDER,
            'order_state'=>$order_status,
        ])->andFilterWhere(['like','orderid',$orderNum]);

         switch ($near_time){
             case 'all':
                 break;
             case  'week':
                 $time= date("Y-m-d H:i:s", strtotime("-1 week"));
                 $query->andFilterWhere(['>','add_time',$time]);
                 break;
             case 'month':
                 $time= date("Y-m-d H:i:s", strtotime("-1 month"));
                 $query->andFilterWhere(['>','add_time',$time]);
                 break;
             case 'threeMonth':
                 $time= date("Y-m-d H:i:s", strtotime("-3 month"));
                 $query->andFilterWhere(['>','add_time',$time]);
                 break;
         }

        $amount=$query->count();
        $page = \Yii::$app->request->get('p', 1);
        $limit = 10;
        $order = $query->offset(($page-1)* $limit)->orderBy('add_time DESC')->limit($limit)->all();
        $page = new Page($amount, $limit);
        $pageHtml = $page->show();

        return $this->render('appointment-order',[
           'order'=>$order,
           'model'=>$model,
           'pageHtml'=>$pageHtml,
            'pageIndexKey' => 3
       ]);
    }

    /**
     *  订单提交评价
     */
    public  function actionSubmitEvaluate(){

        $json=array('code'=>0,'msg'=>"评价失败");
        $data=Yii::$app->request->post();

        $goods_list = OrderGoods::find()->where(['order_id'=>$data['order_id']])->orderBy('id ASC')->all();

        foreach ($goods_list as $k=>$v){
            $comment=new  Comment();
            $comment->order_id=$data['order_id'];
            $comment->goods_id=$v->goods_id;
            $comment->content=$data['textarea'][$k];
            $comment->imgs=json_encode($data['img'][$k]);
            $comment->member_id=$this->memberInfo->id;
            $comment->create_time=date("Y-m-d H:i:s");
            $comment->spzl=$data['spzl'][$k];
            $comment->shfw=$data['shfw'][$k];
            $comment->save();
        }
        $this->getPingLunShu($data['order_id']);
        $is_update=Order::updateAll(['order_state'=>Order::STATUS_JUDGE],['orderid'=>$data['order_id']]);
         if ($is_update>0){

             $json=array('code'=>1,'msg'=>"评价成功");
         } else{
             $json=array('code'=>0,'msg'=>"评价失败");
         }
         return json_encode($json);
    }

    /**
     * 评论数量更新
     */
    public  function  getPingLunShu($order_id){

        $order=Order::findOne($order_id);
        $usrId=$order->user_id;
        if (!empty($usrId)){
            $allGood=Goods::find()->andFilterWhere(['user_id'=>$usrId])->all();
            $goodArr=ArrayHelper::getColumn($allGood,'id');
            $comment_all=Comment::find()->andFilterWhere(['in','goods_id',$goodArr])->all();
            $comment=count($comment_all);
            $spzl=0;  //商品质量
            $spfu=0;  //商品服务
            foreach ($comment_all as $value){
                $spzl+=floatval($value->spzl);
                $spfu+=floatval($value->shfw);
            }
            if ($comment>0){
                $scoreNum=($spzl+$spfu)/(2*$comment);
            }else{
                $scoreNum=0;
            }
            $storeInfo=StoreInfo::findOne(['user_id'=>$usrId]);
            $commentNum=empty($comment)?0:$comment;
            $storeInfo->judge_amount=$commentNum;
            $storeInfo->score=$scoreNum;
            $storeInfo->save();
            User::updateAll(['modifyTime'=>date('Y-m-d H:i:s')],['id'=>$usrId]);
        }

    }

    /**
     * 上传图片
     */
    public function actionUploadimg()
    {
        header("Access-Control-Allow-Origin:*");
        $json = array('code' => 0, 'error' => '参数错误');

        if (Yii::$app->request->isPost) {

            $uploadedFile = new UploadForm();

            $uploadedFile->file = UploadedFile::getInstanceByName("file");

            if ($uploadedFile->file && $uploadedFile->validate()) {

                //原图路径
                $filename = $uploadedFile->createImagePathWithExtension($uploadedFile->file->extension, '/uploads/member/');
                //缩略图片路径
                $fileThumbnailName = $uploadedFile->createImagePathWithExtension($uploadedFile->file->extension, '/uploads/member/headPicThumbnail/');
                $uploadedFile->saveImage($uploadedFile->file, $filename);

                //图片压缩
                $size_src = getimagesize(Yii::$app->request->hostInfo . $filename);
                $w = $size_src['0'];
                $h = $size_src['1'];
                $max = 160;
                if ($w > $h) {
                    $w = $max;
                    $h = $h * ($max / $size_src['0']);
                } else {
                    $h = $max;
                    $w = $w * ($max / $size_src['1']);
                }
                $w = number_format($w, 0);
                $h = number_format($h, 0);
                $uploadedFile->createThumbnail($filename, $fileThumbnailName, $w, $h);


                $json = array(
                    'code' => 0,
                    'url' => yii::$app->request->hostInfo . $filename,
                    'attachment' => $filename,
                    'attachmentTm' => $fileThumbnailName
                );
            } else {
                $json = array('code' => 1, 'msg' => '图片保存失败');

            }

        } else {
            $json = array('code' => 1, 'msg' => "上传失败");
        }

        echo json_encode($json);
        yii::$app->end();
    }


    /**
     * 上传评价照片
     */
    public function actionUploadimgv()
    {
        header("Access-Control-Allow-Origin:*");
        $json = array('code' => 0, 'error' => '参数错误');

        if (Yii::$app->request->isPost) {

            $uploadedFile = new UploadForm();

            $uploadedFile->file = UploadedFile::getInstanceByName("file");

            if ($uploadedFile->file && $uploadedFile->validate()) {

                $pieces = explode("/", $uploadedFile->file->type);
                $filename = $uploadedFile->createImagePathWithExtension($uploadedFile->file->extension, '/uploads/comment/');
//                $filename=$uploadedFile->createImagePathMD5($uploadedFile->file->extension,'/uploads/goods/'.$store_id.'/',$uploadedFile->file->tempName);
                $uploadedFile->saveImage($uploadedFile->file, $filename);

                $json = array(
                    'code' => 0,
                    'url' => yii::$app->request->hostInfo . $filename,
                    'attachment' => $filename
                );
            } else {
                $json = array('code' => 1, 'msg' => '图片保存失败');

            }

        } else {
            $json = array('code' => 1, 'msg' => "上传失败");
        }

        echo json_encode($json);
        yii::$app->end();
    }
}
