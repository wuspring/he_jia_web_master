<?php
namespace index\controllers;

use common\models\IntergralArticle;
use common\models\IntergralGoodsClass;
use common\search\IntergralGoodsSearch;
use common\models\Goods;
use common\models\GoodsClass;
use common\models\GoodsCoupon;
use common\models\StoreGcScore;
use common\models\Ticket;
use common\models\TicketApply;
use common\models\TicketAsk;
use common\models\TicketInfo;
use DL\Base\Config;
use DL\Project\AskTicket;
use DL\Project\FloorConfig;
use DL\service\CacheService;
use DL\vendor\ConfigService;
use \DL\vendor\Page;
use yii\helpers\ArrayHelper;

/**
 * Class IntegralController
 * @package index\controllers
 *  积分首页
 */
class IntegralController extends BaseController
{
    /**
     * 积分首页
     */
    public function actionIndex()
    {
        $cityId = $this->getRequestCity();

        $filter = \Yii::$app->request->queryParams;
        $filter['IntergralGoodsSearch']['goods_hot'] = 1;
        $filter['IntergralGoodsSearch']['goods_state'] = 1;
        $filter['IntergralGoodsSearch']['isdelete'] = 0;
        $searchModel = new IntergralGoodsSearch();
        $dataProvider = $searchModel->search($filter);
        $amount = $dataProvider->query->count();

        $limit = 12;
        $page = new Page($amount, $limit);
        $goods = $dataProvider->query->offset($page->firstRow)->limit($page->listRows)->all();


        $informations = IntergralArticle::find()->where([
            'is_show' => 1,
            'is_del' => 0,
        ])->orderBy('id desc')->limit(5)->all();

        $banners = ConfigService::init('INDEX_INTERGRAL_SHOP')->get('int_banner');
        $banners = arrayGroupsAction(explode(',', $banners), function ($banner) {
            return translateAbsolutePath($banner);
        });

        return $this->render('index_integral', [
            'dataProvider' => $dataProvider,
            'goods' => $goods,
            'page' => $page,
            'informations' => $informations,
            'banners' => $banners
        ]);
    }

    public function actionLists()
    {
//        $cityId = $this->getRequestCity();
        $memberInfo = \Yii::$app->user->identity;


        $filter = \Yii::$app->request->queryParams;
        $filter['IntergralGoodsSearch']['isdelete'] = 0;

        $gc = (int)\Yii::$app->request->get('gc', 0);
        $gc and $filter['IntergralGoodsSearch']['gc_id'] = $gc;

        $searchModel = new IntergralGoodsSearch();
        $inTop = (int)\Yii::$app->request->get('inTop', 0);
        $inBotton = (int)\Yii::$app->request->get('inButton', 0);
        $inTop and $searchModel->inTop = $inTop;
        $inBotton and $searchModel->inBotton = $inBotton;

        $sort = (int)\Yii::$app->request->get('sort', 0);
        $dataProvider = $searchModel->search($filter);
        $amount = $dataProvider->query->count();

        if ($sort == 1) {
            $dataProvider->query = $dataProvider->query->orderBy('goods_integral desc');
        } else if ($sort==2) {
            $dataProvider->query = $dataProvider->query->orderBy('goods_integral asc');
        } else {
            $sort = 0;
        }

        $limit = 15;
        $page = new Page($amount, $limit);
        $goods = $dataProvider->query->offset($page->firstRow)->limit($page->listRows)->all();

        $goodClasses = IntergralGoodsClass::find()->where([])->all();
        $inFilter = ConfigService::init('INDEX_INTERGRAL_SHOP')->get('int_score_filter');
        if ($inFilter) {
            $inFilter = arrayGroupsAction(json_decode($inFilter, true), function ($in) {
                return explode('@', $in);
            });
        } else {
            $inFilter = [];
        }


        return $this->render('/integral/lists', [
            'memberInfo' => $memberInfo,
            'dataProvider' => $dataProvider,
            'goods' => $goods,
            'page' => $page,

            'gc' => $gc,
            'inTop' => $inTop,
            'inButton' => $inBotton,
            'goodClasses' => $goodClasses,
            'inFilter' => $inFilter,
            'sort' => $sort,

            'pageIndexKey'=> 4
        ]);
    }


}
