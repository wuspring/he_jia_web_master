<?php

namespace index\controllers;

use app\search\GoodsCouponSearch;
use common\models\IntergralGoodsAttr;
use common\models\IntergralGoodsAttrGroup;
use common\models\GoodsCoupon;
use common\models\IntergralGoodsClass;
use common\models\IntergralGoodsSku;
use common\models\UploadForm;
use Yii;
use common\models\IntergralGoods;
use common\search\IntergralGoodsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * GoodsController implements the CRUD actions for Goods model.
 */
abstract class BaseIntegralController extends BaseController
{
    public function render($view, $params = [])
    {
        IntergralGoodsClass::findAll([

        ]);
        parent::render($view, $params);
    }
}
