<?php

namespace index\controllers;

use common\models\Advertising;
use common\models\Brand;
use common\models\Collect;
use common\models\Goods;
use common\models\GoodsClass;
use common\models\GoodsComment;
use common\models\GoodsSkLog;
use common\models\History;
use common\models\MemberStoreCoupon;
use common\models\StoreGcScore;
use common\models\Ticket;
use common\models\TicketApplyGoods;
use common\models\User;
use DL\Project\CityTicket;
use DL\Project\GoodsList;
use DL\Project\SecondKill;
use DL\Project\Store;
use DL\service\GoodsService;
use DL\vendor\Page;
use yii;
use yii\web\UploadedFile;
use common\models\UploadForm;
use yii\web\Session;
use yii\data\Pagination;

class GoodsController extends BaseController
{

    protected $formType = Ticket::TYPE_JIA_ZHUANG;
    /**
     * 商品列表 / 搜索
     */
    public function actionLists()
    {
        $cityId = $this->getRequestCity();
        $useUserIds = Store::init()->usefulStoreIds($cityId);
        $filter = [
            ['in', 'type', [1, 2]],
            ['=', 'isdelete', 0],
            ['=', 'goods_state', 1],
            ['in', 'user_id', $useUserIds],
        ];

        // 栏目筛选条件
        $get = Yii::$app->request->get();

        if (isset($get['gc'])) {
            $goodGc = $get['gc'];
            $goodClass = GoodsClass::findOne($get['gc']);
            // 三级筛选条件
            $gcFilters = GoodsClass::find()->where([
                'fid' => $goodClass->fid,
                'is_del' => 0,
                'is_show'=>0
            ])->orderBy('sort asc, id asc')->all();
            $gcFilters or stopFlow("未找到有效栏目");
            $topGc = $goodClass->fid;
            $goodGcIds = [$get['gc']];

            $fGoodClass = GoodsClass::findOne($goodClass->fid);
            // 二级筛选条件
            $goodClasses = GoodsClass::find()->where([
                'and',
                ['=', 'fid', $fGoodClass->fid],
                ['=', 'is_del', 0],
                ['=', 'is_show', 0]
            ])->orderBy('sort asc, id asc')->all();

        } elseif (isset($get['gc_id']) and $get['gc_id']) {
            $topGc = $get['gc_id'];
            $goodGc = 0;
            $goodClass = GoodsClass::findOne($get['gc_id']);
            $goodClasses = GoodsClass::find()->where([
                'and',
                ['=', 'fid', $goodClass->fid],
                ['=', 'is_del', 0],
                ['=', 'is_show', 0]
            ])->orderBy('sort asc, id asc')->all();

            $gcFilters = GoodsClass::find()->where([
                'and',
                ['=', 'fid', $goodClass->id],
                ['=', 'is_del', 0],
                ['=', 'is_show', 0]
            ])->orderBy('sort asc, id asc')->all();
            $goodGcIds = arrayGroupsAction($gcFilters, function ($goodClass) {
                return $goodClass->id;
            });
            $goodGcIds or $goodGcIds = [0];
        } else {
            $goodGc = 0;
            $topGc = 0;

            $goodClasses = GoodsClass::find()->where([
                'and',
                ['in', 'fid', [1, 2, 3]],
                ['=', 'is_del', 0],
                ['=', 'is_show', 0]
            ])->orderBy('sort asc, id asc')->all();
            $goodClassIds = arrayGroupsAction($goodClasses, function ($goodClass) {
                return $goodClass->id;
            });
//            $topGc = reset($goodClassIds);

            $defaultTopGc = 0;
            $gcFilters = GoodsClass::find()->where([
                'and',
                ['in', 'fid', $goodClassIds],
                ['=', 'is_del', 0],
                ['=', 'is_show', 0]
            ])->orderBy('sort asc, id asc')->all();
            $goodGcIds = arrayGroupsAction($gcFilters, function ($goodClass) {
                return $goodClass->id;
            });
        }

        $searchKeyword = '';
        if (isset($get['keyword']) and strlen($get['keyword'])) {
            $searchKeyword = $get['keyword'];
            $filter[] = ['like', 'goods_name', $get['keyword']];

            $secondGcs = GoodsClass::find()->where([
                'and',
                ['in', 'fid', [1, 2, 3]],
                ['=', 'is_del', 0],
                ['=', 'is_show', 0]
            ])->orderBy('sort asc, id asc')->all();

            $topGc = isset($defaultTopGc) ? $defaultTopGc : $topGc;
            $filter[] = ['in', 'gc_id', $goodGcIds];
        } else {
            // 二级筛选条件
            $secIds = arrayGroupsAction($goodClasses, function ($gcFilter) {
                return $gcFilter->id;
            });

            $secondGcs = GoodsClass::find()->where([
//                'fid' => $secGcNow->fid,
                'is_del' => 0,
                'is_show'=>0
            ])->andFilterWhere(['in', 'id', $secIds])->orderBy('sort asc, id asc')->all();

            $filter[] = ['in', 'gc_id', $goodGcIds];
        }

        if (isset($get['brand_id']) and strlen($get['brand_id'])) {
            $get['brand_id'] = arrayGroupsAction(explode('_', $get['brand_id']), function ($d) {
                return (int)trim($d);
            });
            $get['brand_id'] or $get['brand_id'] = [0];
            $filter[] = ['in', 'brand_id', $get['brand_id']];
        } else {
            $get['brand_id'] = [];
        }

        isset($get['start_price']) and $filter[] = ['>=' , 'goods_price', $get['start_price']];
        isset($get['end_price']) and $filter[] = ['<' , 'goods_price', $get['end_price']];

        count($filter) and $filter = array_merge(['and'], $filter);

        // 排序规则
        isset($get['s']) or $get['s'] = 1;
        switch ($get['s']) {
            case 2 :
                $sort = 'admin_score desc, goods_salenum desc';
                break;
            case 3 :
                $sort = 'admin_score desc,id desc';
                break;
            case 4 :
                $sort = 'admin_score desc, evaluation_count desc';
                break;
            case 5 :
                $sort = 'admin_score desc, goods_price desc';
                break;
            case 1 :
            default :
                $sort = 'admin_score desc, goods_collect desc';
        }

        $limit = 16;
        $p = (int)Yii::$app->request->get('p', 1);
        $amount = Goods::find()->where($filter)->count();
        $goods = Goods::find()->where($filter)->orderBy($sort)
            ->offset(($p - 1) * $limit)->limit($limit)->all();

        $page = new Page($amount, $limit);
        $pageHtml = $page->show();

        return $this->render('lists', [
            'get' => $get,
            'searchKeyword' => $searchKeyword,
            'keywords' => isset($get['keyword']) ? $get['keyword'] : '',
            'secondGcs' => $secondGcs,
            'goods' => $goods,
            'goodGc' => $goodGc,
            'topGc' => $topGc,
            'gcFilters' => $gcFilters,
            'pageHtml' => $pageHtml
        ]);
    }

    /**
     * 商品详情
     */
    public function actionDetail()
    {
        $id = (int)Yii::$app->request->get('id', 0);
        $id or stopFlow("商品信息未找到");

        try {
            $goodDetail = Goods::findOne($id);
            $goodDetail or stopFlow("商品已下架");
            $goodDetail->goods_image = strlen($goodDetail->goods_image) > 4 ? arrayGroupsAction(json_decode($goodDetail->goods_image, true), function ($detail) {
                return (object)$detail;
            }) : [];


            if (strlen($goodDetail->goods_name)) {
	            global $title;
	            $title = $goodDetail->goods_name;
	        }
	        if (strlen($goodDetail->keywords)) {
	            global $keywords;
	            $keywords = $goodDetail->keywords;
	        }
	        if (strlen($goodDetail->description)) {
	            global $description;
	            $description = $goodDetail->description;
	        }
	        
            $store = Store::init()->info($goodDetail->user_id);
            $store or stopFlow("该商品已下架");

            // 相关分类
            // $rlCategorys =  Goods::find()->where([
            //     'gc_id'=>$goodDetail->gc_id,
            //     'isdelete' => 0,
            //     'goods_state' => 1
            // ])->limit(6)->all();
            $cityId = $this->getRequestCity();
            $rlCategorys =  Goods::find()->alias('g')->leftJoin(['u' => User::tableName()], 'u.id=g.user_id')->where([
                'g.gc_id'=>$goodDetail->gc_id,
                'g.isdelete' => 0,
                'g.goods_state' => 1,
                'u.provinces_id' => $cityId
            ])->limit(6)->all();

            //查询用户领取过的店铺优惠劵   如果打开失败请检查 member_id的参数
            $hasStoreCoupon = [];
            if ($this->memberInfo) {
                $hasStoreCoupon = MemberStoreCoupon::find()->andFilterWhere([
                    'member_id'=> $this->memberInfo->id,
                    'store_id'=>$goodDetail->user_id,
                    'is_expire'=>0,
                    'is_use'=>0,
                    'is_delete'=>0
                ])->all();
            }

            $hasArr=[];
            foreach ($hasStoreCoupon as $k=>$v){
                $hasArr[]=$v->coupon_id;
            }

            // 更新商品信息点击量
            if ($this->memberInfo) {
                $history = new History();
                $history->member_id = $this->memberInfo->id;
                $history->goods_id = $goodDetail->id;
                $history->add_time = date('Y-m-d');
                $history->create_time = date('Y-m-d H:i:s');
                $history->save();
            }
            $gcId = $this->_getGoodsTopGc($goodDetail->gc_id);
            $storeGcScore = StoreGcScore::findOne([
                'user_id' => $goodDetail->user_id,
//                'provinces_id' => $this->getRequestCity(),
                'gc_id' => $gcId
            ]);
            if ($storeGcScore) {
                $storeGcScore->score += 1;
                $storeGcScore->save();
            }

            // 商品是否为预定商品
            $joinTicketGoods = Store::init()->goodsJoinZH($goodDetail->user_id);
            $joinType = []; // 该商品参加何种展会
            if ($joinTicketGoods) {
                foreach ($joinTicketGoods AS $joinTicketGood) {
                    if ($joinTicketGood->good_id == $goodDetail->id) {
                        $joinType[$joinTicketGood->type] = $joinTicketGood->ticket->type;
                    }
                }
            }
            $isCollection=0;  //未收藏
            if (!empty($this->memberInfo)){
                  $collect=Collect::find()->andFilterWhere(['member_id'=>$this->memberInfo->id,'type'=>Collect::TYPE_GOOD,'goods_id'=>$id])->one();
                  if (!empty($collect)){
                      $isCollection=1;
                  }
            }


            $this->render('detail', [
                'goodDetail' => $goodDetail,
                'joinType' => $joinType,
                'store' => $store,
                'rlCategorys' => $rlCategorys,
                'hasArr'=>$hasArr,
                'isCollection'=>$isCollection
            ]);
        } catch (\Exception $e) {

           stopFlow($e->getMessage());
        }
    }

    public function actionTicket()
    {
        $useUserIds = Store::init()->usefulStoreIds();

        $cityId= $this->getRequestCity();
        $ticketType = Yii::$app->request->get('type', false);

        $tickets = CityTicket::init($cityId)->currentFactionTickets();
        $ticketIds = $tickets ? array_keys($tickets) : [0];

        $nFilter = [
            'and',
            ['in', 'ticket_id', $ticketIds]
        ];

        $nFilter and $nFilter[] = ['=', 'type', $ticketType];
        $goodInfos = TicketApplyGoods::find()->where($nFilter)->all();
        $goodIds = $goodInfos ? arrayGroupsAction($goodInfos, function ($goodInfo) {
            return $goodInfo->good_id;
        }) : [0];


        $filter = [
            ['in', 'type', [1, 2]],
            ['in', 'id', $goodIds],
            ['=', 'isdelete', 0],
            ['=', 'goods_state', 1],
            ['in', 'user_id', $useUserIds],
        ];

        // 栏目筛选条件
        $get = Yii::$app->request->get();

        $goodClasses = GoodsClass::find()->where([
            'and',
            ['in', 'fid', [1, 2, 3]],
            ['=', 'is_del', 0],
            ['=', 'is_show', 0]
        ])->orderBy('sort asc, id asc')->all();

        if (isset($get['gc'])) {
            $goodGc = $get['gc'];
            $goodClass = GoodsClass::findOne($get['gc']);
            // 三级筛选条件
            $gcFilters = GoodsClass::findAll([
                'fid' => $goodClass->fid,
                'is_del' => 0,
                'is_show' => 0
            ]);
            $gcFilters or stopFlow("未找到有效栏目");
            $topGc = $goodClass->fid;
            $goodGcIds = [$get['gc']];

        } elseif (isset($get['gc_id']) and $get['gc_id']) {
            $topGc = $get['gc_id'];
            $goodGc = 0;

            $gcFilters = GoodsClass::find()->where([
                'and',
                ['=', 'fid', $get['gc_id']],
                ['=', 'is_del', 0],
                ['=', 'is_show', 0]
            ])->orderBy('sort asc, id asc')->all();
            $goodGcIds = arrayGroupsAction($gcFilters, function ($goodClass) {
                return $goodClass->id;
            });

        } else {
            $goodGc = 0;
            $topGc = 0;
            $goodClassIds = arrayGroupsAction($goodClasses, function ($goodClass) {
                return $goodClass->id;
            });

//            $topGc = reset($goodClassIds);
            $gcFilters = GoodsClass::find()->where([
                'and',
                ['in', 'fid', $goodClassIds],
                ['=', 'is_del', 0],
                ['=', 'is_show', 0]
            ])->orderBy('sort asc, id asc')->all();
            $goodGcIds = arrayGroupsAction($gcFilters, function ($goodClass) {
                return $goodClass->id;
            });


        }

        $filter[] = ['in', 'gc_id', $goodGcIds];

        $searchKeyword = '';
        if (isset($get['keyword'])) {
            $searchKeyword = $get['keyword'];
            $filter[] = ['like', 'goods_name', $get['keyword']];
        }

        if (isset($get['brand_id']) and strlen($get['brand_id'])) {
            $get['brand_id'] = arrayGroupsAction(explode('_', $get['brand_id']), function ($d) {
                return (int)trim($d);
            });
            $get['brand_id'] or $get['brand_id'] = [0];
            $filter[] = ['in', 'brand_id', $get['brand_id']];
        } else {
            $get['brand_id'] = [];
        }

        isset($get['start_price']) and $filter[] = ['>=' , 'goods_price', $get['start_price']];
        isset($get['end_price']) and $filter[] = ['<' , 'goods_price', $get['end_price']];

        count($filter) and $filter = array_merge(['and'], $filter);

        // 排序规则
        isset($get['s']) or $get['s'] = 1;
        switch ($get['s']) {
            case 2 :
                $sort = 'goods_salenum desc';
                break;
            case 3 :
                $sort = 'id desc';
                break;
            case 4 :
                $sort = 'evaluation_count desc';
                break;
            case 5 :
                $sort = 'goods_price desc';
                break;
            case 1 :
            default :
                $sort = 'goods_collect desc';
        }

        $limit = 16;
        $p = (int)Yii::$app->request->get('p', 1);
        $amount = Goods::find()->where($filter)->count();
        $goods = Goods::find()->where($filter)->orderBy($sort)
            ->offset(($p - 1) * $limit)->limit($limit)->all();

        $page = new Page($amount, $limit);
        $pageHtml = $page->show();
        $goodsDics = yii\helpers\ArrayHelper::map($goodInfos, 'good_id', 'type');

        return $this->render('ticket', [
            'get' => $get,
            'goodClasses' => $goodClasses,
            'searchKeyword' => $searchKeyword,
            'keywords' => isset($get['keyword']) ? $get['keyword'] : '',
            'goods' => $goods,
            'goodGc' => $goodGc,
            'topGc' => $topGc,
            'gcFilters' => $gcFilters,
            'ticketType' => $ticketType,
            'pageHtml' => $pageHtml,
            'goodsDics' =>  $goodsDics

        ]);
    }

    private function _getGoodsTopGc($gcId, $sonId=0)
    {
        $gc = GoodsClass::findOne($gcId);
        if (!$gc) {
            throw new \Exception('栏目信息异常');
        }

        if ($gc->fid) {
            return $this->_getGoodsTopGc($gc->fid, $gc->id);
        }

        return $sonId ? : $gcId;
    }
}
