<?php
namespace index\controllers;


use common\models\IntegralLog;
use common\models\IntergralTreasure;
use common\models\IntergralTreasureLog;
use common\models\Ticket;
use common\search\IntergralTreasureSearch;

use DL\vendor\ConfigService;
use DL\vendor\Page;


/**
 * Class IntegralTreasureController
 * @package index\controllers
 *  积分夺宝
 */
class IntegralTreasureController extends BaseController
{
    /**
     * 积分夺宝
     */
    public function actionIndex()
    {
        $request = \Yii::$app->request->get();
        $request['IntergralTreasureSearch']['goods_state'] = 1;
        $request['IntergralTreasureSearch']['isdelete'] = 0;

        $intergralTreasureSearch = new IntergralTreasureSearch();
        $search = $intergralTreasureSearch->search($request);

        $limit = 12;
        $amount = $search->query->count();
        $page = new Page($amount, $limit);

        $treasures = $search->query->offset($page->firstRow)->limit($page->listRows)->all();

        return $this->render('index', [
            'treasures' => $treasures,
            'page' => $page,

            'pageIndexKey'=>2
        ]);
    }

    /**
     * 最新揭晓
     */
    public  function actionNewRecord()
    {
        $records = IntergralTreasure::find()->where([
            'and',
            ['=', 'goods_state', 0],
            ['=', 'isdelete', '0']
        ])->orderBy('end_time desc')->limit('10')->all();

        return $this->render('new-record', [
            'records' => $records,
            'pageIndexKey' => 2
        ]);
    }

    public function actionGetTreasure()
    {
        $id = \Yii::$app->request->get('id', 0);
        $amount = (int)\Yii::$app->request->get('amount', 0);

        $amount or apiSendError("请输入领票的数量");

        $member = \Yii::$app->user->identity;
        $member or apiSendError("您尚未登录");

        $treasure = IntergralTreasure::findOne([
            'id' => $id,
            'goods_state' => 1,
            'isdelete' => 0,
        ]);
        $treasure or apiSendError("活动已结束");

        $totalIntegral = $treasure->goods_integral * $amount;
        $oldIntegral = $member->integral;
        if ($oldIntegral < $totalIntegral) {
            apiSendError("积分不足");
        }
        $member->integral = $oldIntegral - $totalIntegral;
        $member->save() or apiSendError("网络异常");

        $applyGroupKey = 'db' . date('YmdH:i:s') . '_' . $member->id . '_' . $treasure->id;

        $integralLog = new IntegralLog();
        $integralLog->orderId = $applyGroupKey;
        $integralLog->memberId = $member->id;
        $integralLog->old_integral = $oldIntegral;
        $integralLog->now_integral = $member->integral;
        $integralLog->pay_integral = $totalIntegral;
        $integralLog->create_time = date('Y-m-d H:i:s');
        $integralLog->expand = "参加积分夺宝活动[{$applyGroupKey}]";
        $integralLog->save();

        for ($i=1; $i<=$amount; $i++) {
            $log = new IntergralTreasureLog();
            $log->member_id = (int)$member->id;
            $log->treasure_id = (int)$treasure->id;
            $log->integral = $treasure->goods_integral;
            $log->key = $log->getCreateKey();
            $log->create_time = date('Y-m-d H:i:s');
            $log->apply_group = $applyGroupKey;

            $log->save();
        }

        $treasure->goods_salenum = IntergralTreasureLog::find()->where([
            'treasure_id' => $treasure->id
        ])->count();
        $treasure->save();

        $treasure->goods_salenum >= $treasure->goods_storage and $this->_setTreasure($treasure->id);

        apiSendSuccess("夺宝成功");
    }

    /**
     * 设置获奖人员
     */
    private function _setTreasure($iTreasureId)
    {
        // 获奖人数
        $number = 1;
        $iTreasure = IntergralTreasure::findOne([
                'id' => $iTreasureId,
                'goods_state' => 1
       ]);

        $iTreasure or stopFlow("系统异常");

        $unsetIds = [0];
        $treasure = [];
        for ($i=1; $i<= $number; $i++) {
            $query = IntergralTreasureLog::find()->where([
                'and',
                ['not in', 'id', $unsetIds],
                ['=', 'treasure_id', $iTreasureId]
            ]);

            $amount = $query->count();
            $key = rand(0, $amount-1);
            $ticket = $query->offset($key)->limit(1)->one();
            $treasure[] = $ticket->id;
            $unsetIds[] = $ticket->id;
        }

        $iTreasure->treasure_log_ids = json_encode($treasure);
        $iTreasure->goods_state = 0;
        $iTreasure->end_time = date('Y-m-d H:i:s');
        $iTreasure->save();

        if ($iTreasure->is_virtual) {
            $data = $iTreasure->attributes;

            unset($data['id'], $data['goods_salenum'], $data['goods_collect'], $data['treasure_log_id']);
            $data['goods_addtime'] = date('Y-m-d H:i:s');
            $data['goods_state'] = 1;
            $data['from_id'] >0 or $data['from_id'] = $iTreasure->id;

            $newTreasure = new IntergralTreasure();
            $newTreasure->setAttributes($data);
            $newTreasure->save();
        }
    }

    /**
     * 我的夺宝记录
     */
    public  function actionMyRecord()
    {
        $member = \Yii::$app->user->identity;

        $records = [];
        $pageHtml = '';
        if ($member) {
            $amount = IntergralTreasureLog::find()->where([
                'member_id' => $member->id,
            ])->groupBy('apply_group')->orderBy('id desc')->count();
            $page = new Page($amount, 10);
            $records = IntergralTreasureLog::findBySql("select member_id, count(*) as times, create_time from `intergral_treasure_log` where `member_id`='{$member->id}' group by `member_id` limit {$page->firstRow}, {$page->listRows};")->all();

            $pageHtml = $page->show();
        }

        return $this->render('my-record',[
            'records'=> $records,
            'pageHtml'=> $pageHtml,

            'pageIndexKey'=>2
        ]);
    }

    /**
     * 夺宝规则
     */
    public  function actionTreasureRule()
    {
        $content = ConfigService::init('INDEX_INTERGRAL_SHOP')->get('int_rules');
        return $this->render('treasure-rule', [
            'content' => $content,
            'pageIndexKey'=>2
        ]);
    }

}
