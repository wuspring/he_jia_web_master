<?php
namespace index\controllers;
use Yii;
use common\models\Channel;
use common\models\IpMemory;
use common\models\Member;
use common\models\Provinces;
use common\models\SelectCity;
use common\models\Ticket;
use common\models\TicketAsk;
use common\models\User;
use DL\Base\Config;
use DL\Project\CityExpand;
use DL\service\CacheService;
use DL\vendor\ConfigService;
use dlExpand\Position;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use common\models\JsApiPay;
use common\models\MemberForm;


abstract class BaseController extends Controller
{
    protected $isWechatLogin = true;
    protected $memberInfo;

    public $footIndex = 0;
    public $globalTitle;

    protected $webBrowserKey;

    protected $usefulStore = [];

    public function beforeAction($action){
        
        $route=\yii::$app->session->get('routedata');
        $act=Yii::$app->controller->getRoute();

        if (empty($route)) {
            $route=array();
            $route[0]=array(
                'route'=>Yii::$app->controller->getRoute(),
                'params'=>Yii::$app->request->getQueryParams(),
            );
            $route[1]=array(
                'route'=>Yii::$app->controller->getRoute(),
                'params'=>Yii::$app->request->getQueryParams(),
            );
        }else{
            if (count($route[0])!=2){
                $route=array();
                $route[0]=array(
                    'route'=>Yii::$app->controller->getRoute(),
                    'params'=>Yii::$app->request->getQueryParams(),
                );
                $route[1]=array(
                    'route'=>Yii::$app->controller->getRoute(),
                    'params'=>Yii::$app->request->getQueryParams(),
                );
            }else{
                if ($act=='index/set-city'){
                    $route[0]=$route[1];
                }else{
                    $route[0]=$route[1];
                    $route[1]=array(
                        'route'=>Yii::$app->controller->getRoute(),
                        'params'=>Yii::$app->request->getQueryParams(),
                    );
                }

            }

        }
        \yii::$app->session->set('routedata',$route);

    
        return parent::beforeAction($action);

    }

    public function init()
    {
    

        $this->memberInfo = \Yii::$app->user->identity;
        // if (empty($this->memberInfo)) {
        //     $this->isWechatLogin and $this->memberInfo = $this->_init();

        //     // 网络延迟注册失败
        //     if ($this->isWechatLogin and empty($this->memberInfo)) {
        //         $request = getServerHost() . $_SERVER["REQUEST_URI"];
        //         header('Location: ' . $request);
                // die();
        //     }
        // }
        if (!empty($_GET['channel'])){
            \yii::$app->session->set('channel',$_GET['channel']);

            // 统计渠道来源
            $channel = new Channel();
            $channel->channel = trim($_GET['channel']);
            $channel->create_time = date('Y-m-d H:i:s');
            $channel->save();
        }



        $this->getRequestCity();
        $this->invalidTimeOutTicket();

        $this->refreshInvalidUser();
          parent::init();
    }

    protected function refreshInvalidUser()
    {
        $stores = User::find()->where([
            'and',
            ['=', 'assignment', User::ASSIGNMENT_HOU_TAI],
            ['=', 'user_type', User::USER_TYPE_LIMBO],
            ['=', 'status', 1],
            ['<', 'end_time', date('Y-m-d')]
        ])->all();

        arrayGroupsAction($stores, function ($store) {
            $store->status = 0;
            $store->save();
        });

    }

    protected function invalidTimeOutTicket()
    {
        $time = date('Y-m-d', strtotime('+0 day'));
        $tickets = Ticket::find()->where([
            'and',
            ['<', 'end_date', $time],
            ['=', 'status', 1]
        ])->all();

        arrayGroupsAction($tickets, function ($ticket) {
            $ticket->status = 0;
            $ticket->save();
        });
    }
    /**
     * 获取当前城市信息
     *
     * @return int provinces id
     */
    protected function getRequestCity()
    {
        session_status()==2 or session_start();
        $this->webBrowserKey = '__CITY_INFO__' . session_id();

        $secretCityId = \Yii::$app->request->get('SecretCityId', 0);

        if ($secretCityId) {
        	$provinces = Provinces::findOne(['pinyin'=>$secretCityId]);
         	$secretCityId=$provinces->id;
        }

        $cityId = $secretCityId ? $secretCityId : CacheService::init()->get($this->webBrowserKey);

        if (!$cityId) {
            // 默认城市
            $cityId = ConfigService::init(ConfigService::EXPAND)->get('default_city');
            $ip = $_SERVER['REMOTE_ADDR'];
            if (!strlen($ip) and isset($_SERVER['  HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['  HTTP_X_FORWARDED_FOR'];
            }

            $ipRelation = (array)CacheService::init()->get('__IP_RELATION_INFO__');
            if (isset($ipRelation[$ip])) {
                $requestId = $ipRelation[$ip];
            } else {
                $uniqueKey = md5(trim($ip));

                $ipInfo = IpMemory::findOne([
                    'unique_key' => $uniqueKey
                ]);

                if ($ipInfo) {
                    $requestId = $ipInfo->provinces_id;
                } else {
                    $info = file_get_contents("http://ip.taobao.com/service/getIpInfo.php?ip={$ip}");
                    $info = json_decode($info, true);

                    if ($info) {
                        if (preg_match('/^\d+$/',$info['data']['city_id'])) {
                            $provinces = Provinces::findOne($info['data']['city_id']);
                            $requestId = $provinces ? $provinces->id : $cityId;
                        };
                    }

                    $ipInfo = new IpMemory();
                    $ipInfo->ip = $ip;
                    $ipInfo->provinces_id = $requestId;
                    $ipInfo->unique_key = $uniqueKey;
                    $ipInfo->save();
                    $ipRelation[$ip] = $requestId;
                    CacheService::init()->set('__IP_RELATION_INFO__', $ipRelation);
                }

            }

            $selectCitys = SelectCity::find()->where([])->all();
            $selectCityIds = ArrayHelper::getColumn($selectCitys, 'provinces_id');

            in_array($requestId, $selectCityIds) and $cityId = $requestId;
            CacheService::init()->set($this->webBrowserKey, $cityId);
        }

        return $cityId;
    }

    protected function _init()
    {
        include('./lib/emoji.php');
        $session = \Yii::$app->session;
        $guanzhuurl= \Yii::$app->request->hostInfo."/index.php?r=weixin/qrcode";
        $tools = new JsApiPay();

        if (!$session->isActive){
            $session->open();
        }
        $_SESSION['accessurl']= \Yii::$app->request->hostInfo . \Yii::$app->request->getUrl();
        if (isset($_GET['pid'])) {
            $session->set('userpid',$_GET['pid']);
        }
        $openId =null;
        if (!isset($_SESSION['wxopenid'])) {
            //获取用户openid
            $openId = $tools->GetOpenid();
            $session->set('wxopenid',$openId);
        }else{
            $openId=$session->get('wxopenid');
        }

        $userinfo = Member::find()->where(['wxopenid'=>$openId])->one();

        if (!empty($userinfo)) {

            $wxuserinfo= $tools->getUserInfo($openId);
            if ($wxuserinfo['subscribe']==1) {
                $userinfo->sex=$wxuserinfo['sex'];
                $userinfo->avatar=$wxuserinfo['headimgurl'];
                $userinfo->nickname=emoji_unified_to_docomo(strtolower($wxuserinfo['nickname']));
                if (empty($userinfo->qrodeUrl)) {
                    $qrode=$tools->getQrode($openId);
                    $userinfo->ticket=$qrode['ticket'];
                    $userinfo->qrodeUrl=$qrode['url'];
                }
                if (isset($_SESSION['userpid'])) {
                    $pid=intval($_SESSION['userpid']); //$session->get('userpid');
                    if ($pid!=$userinfo->id&&$userinfo->pid<1) {
                        $userinfo->pid = $pid;
                    }

                }
            }else{
                return  $this->redirect($guanzhuurl);
                \yii::$app->end();
            }
            $userinfo->last_visit =date('Y-m-d H:i:s');
            $userinfo->save();

            if (\Yii::$app->user->isGuest) {

                $model = new MemberForm();
                $model->username =$userinfo->username;

                $model->wxlogin($userinfo->id);
                //Yii::$app->getRequest->redirect(Yii::$app->user->getReturnUrl());
                // $this->goBack();
            }

            // if ($userinfo->status =='0') {
            //   // $this->geturl();
            //      //print_r(3333);exit;
            //     $geturl=yii::$app->request->hostInfo."/index.php?r=site/order";

            //     $this->redirect($geturl);
            // }
            $this->memberInfo = $userinfo;
        } else {

            // if (!isset($_SESSION['wxuser'])) {
            $wxuserinfo= $tools->getUserInfo($openId);
            $user = new Member();
            $user->username ="yc".rand(1,100) .Member::find()->count().rand(100,999);
            $user->password = \Yii::$app->security->generatePasswordHash("123456");
            $user->role=2;
            $user->status='1';
            if ($wxuserinfo['subscribe']==1) {
                $user->sex=$wxuserinfo['sex'];
                $user->avatar=$wxuserinfo['headimgurl'];

                $user->nickname=emoji_unified_to_docomo(strtolower($wxuserinfo['nickname']));//str_replace(array("'","\\"),array(''),$wxuserinfo['nickname']);//$this->jsonName();
                $user->wxnick = emoji_unified_to_docomo(strtolower($wxuserinfo['nickname']));//str_replace(array("'","\\"),array(''),$wxuserinfo['nickname']);//$this->jsonName();

                $qrode=$tools->getQrode($openId);
                $user->ticket=$qrode['ticket'];
                $user->qrodeUrl=$qrode['url'];

            }
            $user->last_visit=date('Y-m-d H:i:s');
            $user->createTime =$user->last_visit;
            $user->modifyTime=$user->createTime;
            $user->userType = 1;
            $user->wxopenid = $openId;

            if (!isset($_SESSION['userpid'])) {
                $user->pid=0;
            } else {
                $user->pid=intval($_SESSION['userpid']); //$session->get('userpid');
            }
            if ($user->save()) {
                if (\Yii::$app->user->isGuest) {
                    $model = new MemberForm();
                    $model->username =$user->username;
                    $model->wxlogin($user->id);
                }
            }
            if ($wxuserinfo['subscribe']==0) {
                return  $this->redirect($guanzhuurl);
                \yii::$app->end();
            }

            $this->memberInfo = $user;
            // }
        }
    }

    public function render($view, $params = [])
    {
        $cityId = $this->getRequestCity();
        // 通用页脚信息
        $pageFootData = [];
        $pageFootData['pc_xssl'] = ConfigService::init(ConfigService::EXPAND)->get('pc_xssl');
        $pageFootData['pc_gwzn'] = ConfigService::init(ConfigService::EXPAND)->get('pc_gwzn');
        $pageFootData['pc_fwbz'] = ConfigService::init(ConfigService::EXPAND)->get('pc_fwbz');
        $pageFootData['pc_hyzx'] = ConfigService::init(ConfigService::EXPAND)->get('pc_hyzx');
        $pageFootData['pc_lxwm'] = ConfigService::init(ConfigService::EXPAND)->get('pc_lxwm');
        $pageFootData['pc_yjdb'] = ConfigService::init(ConfigService::EXPAND)->get('pc_yjdb');

        // select group citys
        $defaultCity = Provinces::findOne($cityId);
        $selectCitys = SelectCity::find()->where([])->all();
        $cityIds = ArrayHelper::getColumn($selectCitys, 'provinces_id');
        $cityIds or $cityIds = [0];
        $groupCitys = Provinces::find()->where([
            'and',
            ['=', 'level', 2],
            ['in', 'id', $cityIds]
        ])->all();

        // 用户信息
        $userInfo = null;
        if (\Yii::$app->user->identity) {
            $userInfo = Member::findOne(\Yii::$app->user->id);
        }

        $cityConfig = CityExpand::init()->get($cityId, CityExpand::TYPE_CITY_EXPAND);
        $phone = ConfigService::init('system')->get('telephone');

        $get = \Yii::$app->request->get();
        $params = array_merge(['get' => $get], $params, $pageFootData, [
            'userInfo' => $userInfo,
            'defaultCity' => $defaultCity,
            'groupCitys' => $groupCitys,
        ]);



        // 网页TITLE
        if (isset($params['title'])) {
            global $title;
            $title = $params['title'];
        }

        global $userCurrentCity, $cityPhone;
        $userCurrentCity = $defaultCity;
        $cityPhone = $cityConfig ? $cityConfig->tel : $phone;
        die(parent::render($view, $params));
    }
}