<?php
namespace index\controllers;

use common\models\Goods;
use common\models\GoodsClass;
use common\models\GoodsCoupon;
use common\models\StoreGcScore;
use common\models\StoreInfo;
use common\models\Ticket;
use common\models\TicketApply;
use common\models\TicketApplyGoods;
use common\models\TicketAsk;
use common\models\TicketInfo;
use DL\Project\AskTicket;
use DL\Project\FloorConfig;
use DL\Project\Store;
use DL\service\CacheService;
use DL\vendor\ConfigService;
use DL\Project\Page;
use yii\helpers\ArrayHelper;

class JieHunController extends BaseTicketController
{
    protected $formType = Ticket::TYPE_JIE_HUN;

    public function actionIndex($ticketId = 0)
    {
        $cityId = $this->getRequestCity();

        $amount = 0;

        $ticketId or $ticketId = $this->getTicketId();
        $ticket = false;
        $joinTicketStoreIds = [0];
        if ($ticketId) {
            $amount = $this->ticketAmountKey($ticketId);
            $ticket = Ticket::findOne($ticketId);

	        if (strlen($ticket->name)) {
	            global $title;
	            $title = $ticket->name;
	        }
	        if (strlen($ticket->keywords)) {
	            global $keywords;
	            $keywords = $ticket->keywords;
	        }
	        if (strlen($ticket->description)) {
	            global $description;
	            $description = $ticket->description;
	        }
	        
            $storeApplys = TicketApply::findAll([
                'ticket_id' => $ticket->id,
                'status' => 1
            ]);

            if ($storeApplys) {
                $uStoreIds = Store::init()->usefulStoreIds();
                $joinTicketStoreIds = arrayGroupsAction($storeApplys, function ($storeApply) {
                    return $storeApply->user_id;
                });

                $joinTicketStoreIds = array_intersect($joinTicketStoreIds, $uStoreIds);
                $joinTicketStoreIds or $joinTicketStoreIds = [0];
            }
        }

        $uKey = $this->getSessionKey($ticketId);
        $formData = CacheService::init()->get($uKey);
        $formData or $formData = [];

        $formData = array_merge($formData, [
            'amount' => $amount,
            'ticketId' => $ticketId,
            'cityId' => $cityId
        ]);

        $banner = AskTicket::init()->get($this->getRequestCity(), Ticket::TYPE_JIE_HUN)->banner;
        $zhanhuiPic = AskTicket::init()->get($this->getRequestCity(), Ticket::TYPE_JIE_HUN)->zhanhui_pic;

        //参展商品
        $goods = TicketApplyGoods::find()->andFilterWhere(['ticket_id'=>$ticketId])->all();
        $goodsIds=ArrayHelper::getColumn($goods,'good_id');
        //爆款预约
        $goods_appointment = TicketApplyGoods::find()->andFilterWhere(['ticket_id'=>$ticketId,'type'=>TicketApplyGoods::TYPE_COUPON])->all();
        $goods_appointmentIds=ArrayHelper::getColumn($goods_appointment,'good_id');
        //预存 享特价
        $goods_special = TicketApplyGoods::find()->andFilterWhere(['ticket_id'=>$ticketId,'type'=>TicketApplyGoods::TYPE_ORDER])->all();
        $goods_specialIds=ArrayHelper::getColumn($goods_special,'good_id');
        if (empty($goodsIds)){
            $goodsIds = [0];
        }

        $goodCoupon = [];
        if ($ticket) {
            //品牌优惠劵
            $goodCoupon=GoodsCoupon::find()->andFilterWhere([
                'and',
                ['in', 'user_id', $joinTicketStoreIds],
                ['>', 'num', 0],
//                ['=', 'is_show', 1],
                ['=', 'is_del', 0],
            ])->limit($ticket->couple_nums)->all();
        }

        //特价
        $teJia_good=Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 'g.id=t.good_id')->andFilterWhere(['t.ticket_id'=>$ticketId,'t.type'=>TicketApplyGoods::TYPE_ORDER, 'is_zhanhui' => 1])->select(['g.*', 't.good_amount'])->orderBy(' t.sort desc')->all();

        //爆款预约
        $baoKuan_good=Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 'g.id=t.good_id')->andFilterWhere(['t.ticket_id'=>$ticketId,'t.type'=>TicketApplyGoods::TYPE_COUPON, 'is_zhanhui' => 1])->select(['g.*', 't.good_amount'])->orderBy(' t.sort desc')->all();

        //结婚展类型
        $goodClass=GoodsClass::find()->andFilterWhere(['fid'=>2])->limit(7)->orderBy("id ASC")->all();
          //获取合作品牌
          $jieHunType=ArrayHelper::getColumn($goodClass,'id');
          $joinStore_gc=[];
          $storeGcScore=StoreGcScore::find()->select('*, `score` * `admin_score` as `a_score`')->andFilterWhere([
              'and',
              ['in','gc_id',$jieHunType],
              ['=', 'provinces_id',$cityId],
              ['in', 'user_id', $joinTicketStoreIds]
          ])->orderBy('a_score desc')->all();
            foreach ($storeGcScore as $store){
                $joinStore_gc[$store->gc_id][] = Store::init()->info($store->user_id);
            }
        $configIndex = [
            'banner' => Page::init()->get($cityId, Page::PAGE_CAI_GOU_JIEHUN)->banner,
            'huodongs' => Page::init()->get($cityId, Page::PAGE_CAI_GOU_JIEHUN)->huodongs
        ];

        $this->render('index', [
            'indexIndex' => 5,
            'showPromise' => 1,
            'configIndex' => $configIndex,
            'zhanhuiPic' => $zhanhuiPic,
            'formData' => $formData,
            'name' => '婚博会',
            'ticket' => $ticket,
            'banner' => $banner,
            'formType'=>$this->formType,
            'goodCoupon'=>$goodCoupon,
            'teJia_good'=>$teJia_good,
            'baoKuan_good'=>$baoKuan_good,
            'goodClass1'=>$goodClass,
            'joinStore_gc'=>$joinStore_gc
        ]);
    }


    public function actionIndex2($ticketId = 0)
    {
        global $cityId;
        $cityId = $this->getRequestCity();
        $configIndex = [
            'banner' => Page::init()->get($cityId, Page::PAGE_CAI_GOU_JIEHUN)->banner,
            'huodongs' => Page::init()->get($cityId, Page::PAGE_CAI_GOU_JIEHUN)->huodongs
        ];

        $gcs = GoodsClass::findAll([
            'fid' => 2
        ]);

        $gcDatas = arrayGroupsAction($gcs, function (GoodsClass $gc) {
            global $cityId;
            $commendStores = StoreGcScore::find()->where([
                'provinces_id' => $cityId,
                'gc_id' => $gc->id
            ])->orderBy('score desc')->limit(6)->all();

            $pics = FloorConfig::init()->get($cityId, $gc->id)->zh_img;
            $zhFirstPics = FloorConfig::init()->get($cityId, $gc->id)->zh_first_imgs;

            return [
                'info' => $gc,
                'commends' => $commendStores,
                'pics' => $pics,
                'zhFirstPics' => $zhFirstPics
            ];
        });

        $ticketId or $ticketId = $this->getTicketId();

        $uKey = $this->getSessionKey($ticketId);
        $formData = CacheService::init()->get($uKey);
        $formData or $formData = [];
        $amount = $this->ticketAmountKey($ticketId);

        $formData = array_merge($formData, [
            'amount' => $amount,
            'ticketId' => $ticketId,
            'cityId' => $cityId
        ]);

        $this->render('index2', [
            'configIndex' => $configIndex,
            'cityId' => $cityId,
            'gcDatas' => $gcDatas,
            'formData' => $formData,
            'basePathData' => ['path' => 'jie-hun', 'info' => '结婚采购'],
            'indexIndex' => 3
        ]);
    }
}
