<?php
namespace index\controllers;


use common\models\IntergralLuckDraw;
use common\models\IntergralPrize;
use common\models\Member;
use yii\helpers\ArrayHelper;
use common\models\IntergralTurntable;
use common\models\IntegralLog;
/**
 * Class IntegralLuckdrawController
 * @package index\controllers
 *  积分抽奖
 */
class IntegralLuckdrawController extends BaseController
{

    /**
     * 积分抽奖
     */
    public function actionIndex()
    {
        $cityId = $this->getRequestCity();
         if (\Yii::$app->user->isGuest){
             $playNum=0;
         }else{
             $playNum=($this->memberInfo->integral)/10;
         }
         //当前期
         $now_term=IntergralPrize::find()->andFilterWhere(['pid_terms'=>0])->orderBy('prize_terms DESC')->limit(1)->one();
         //当前期奖品
         $prize=IntergralPrize::find()->andFilterWhere(['pid_terms'=>$now_term->id])->orderBy('sort DESC')->limit(5)->all();

         //当前获奖名单
         $prizeArr=[1,2,3,5,6,8,9];         //获奖id
         $getPizeList=IntergralLuckDraw::find()->andFilterWhere(['prize_terms'=>$now_term->prize_terms])->andFilterWhere(['in','draw_id',$prizeArr])->all();

        return $this->render('index',[
            'pageIndexKey'=>3,
            'playNum'=>$playNum,
            'prize'=>$prize,
            'getPizeList'=>$getPizeList
        ]);
    }

    /**
     * 积分大转盘__点击后可以旋转
     */
    public  function  actionDoaward()
    {
        $result=array('code'=>0,'msg'=>'存储异常');

        $now_prize=IntergralPrize::find()->andFilterWhere(['pid_terms'=>0])->orderBy('prize_terms DESC')->limit(1)->one();
        $now_time=date('Y-m-d H:i:s');
        if (\Yii::$app->user->isGuest){
            $result=array('code'=>0,'msg'=>'请先登录');
        }else{
           if ($now_prize->start_time<=$now_time && $now_time<=$now_prize->end_time){
               $member=Member::findOne(\Yii::$app->user->id);
               $member_interal=intval($member->integral);
               if ($member_interal>=10){
                   //更新积分
                   $member->integral-=10;
                   $member->save();

                   //控制转盘
                   $prize_arr =IntergralTurntable::find()->asArray()->all();
                   foreach ($prize_arr as $key => $val) {
                       $arr[$val['id']] = $val['v'];
                   }
                   $rid = $this->getRand($arr);   //根据概率获取奖项id
                   $res = $prize_arr[$rid - 1];  //中奖项

                   //积分数据变更记录
                   $this->InteralRelationData($res);

                   $result['code'] =1; //状态
                   $result['playNum'] =($member_interal-10)>=10?($member_interal-10)/10:0; //次数
                   $result['angle'] =floatval($res['angle']); //随机生成一个角度
                   $result['prize'] = $res['prize'];
                   $result['msg'] ='抽奖成功';

               }else{
                   $result=array('code'=>0,'msg'=>'积分不足');
               }
           }else{
               $result=array('code'=>0,'msg'=>'本期已经结束');
           }

        }
        return json_encode($result);
    }

    /**
     * 积分相关表的数据变更
     */
    public  function InteralRelationData($res){

        $member=Member::findOne($this->memberInfo->id);
        //当前期数
        $now_prize=IntergralPrize::find()->orderBy('prize_terms DESC')->limit(1)->one();
        //点击抽奖记录
        $intergralLuckDraw=new IntergralLuckDraw();
         $orderNumber=$intergralLuckDraw->newOrderNumber;
        $intergralLuckDraw->member_id=$this->memberInfo->id;
        $intergralLuckDraw->prize_terms=$now_prize->prize_terms;      //获奖期限
        $intergralLuckDraw->draw_id=$res['id'];             //获奖id
        $intergralLuckDraw->create_time=date('Y-m-d H:i:s');
        $intergralLuckDraw->number=$orderNumber;
        $intergralLuckDraw->save();
        //更改获奖数据
        switch ($res['id']){
              case 1:  //特等奖
                  IntergralTurntable::updateAll(['v'=>0],['id'=>1]);
                  break;
               case 2;    //三等奖
                   IntergralTurntable::updateAll(['v'=>0],['in','id',[2,8]]);
               break;
              case 3;
                  IntergralTurntable::updateAll(['v'=>0],['in','id',[3,9]]);
                  break;
              case 5;    //一等奖
                  IntergralTurntable::updateAll(['v'=>0],['id'=>5]);
                  break;
              case 6;  //二等奖
                  IntergralTurntable::updateAll(['v'=>0],['id'=>6]);
                  break;
              case 8;    //三等奖
                  IntergralTurntable::updateAll(['v'=>0],['in','id',[2,8]]);
                  break;
              case 9;
                  IntergralTurntable::updateAll(['v'=>0],['in','id',[3,9]]);
                  break;
              default:
                  break;
          }
        //积分变更记录
        $integralLog=new IntegralLog();
        $integralLog->memberId=$this->memberInfo->id;
        $integralLog->orderId=$orderNumber;
        $integralLog->pay_integral=10;
        $integralLog->old_integral=intval($member->integral)+10;
        $integralLog->now_integral=$member->integral;
        $integralLog->expand='积分抽奖';
        $integralLog->create_time=date('Y-m-d H:i:s');
        $integralLog->save();

    }


    /**
     * @param $proArr
     * @return int|string
     *  中奖概率计算
     */
   public  function getRand($proArr) {
        $result = '';
        //概率数组的总概率精度
        $proSum = array_sum($proArr);
        //概率数组循环
        foreach ($proArr as $key => $proCur) {
            $randNum = mt_rand(1, $proSum);
            if ($randNum <= $proCur) {
                $result = $key;
                break;
            } else {
                $proSum -= $proCur;
            }
        }
        unset ($proArr);
        return $result;
    }

}
