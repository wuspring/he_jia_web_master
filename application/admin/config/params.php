<?php
return [
//    'adminEmail' => 'admin@example.com',
     'imageDirectoryBasePath'=>'/wamp/www/yii2',
//      'domain' => 'http://quanmin.shangdandan.com',
		'webuploader' => [
	        // 后端处理图片的地址，value 是相对的地址
	        'uploadUrl' => 'blog/upload',
	        // 多文件分隔符
	        'delimiter' => ',',
	        // 基本配置
	        'baseConfig' => [
	            'defaultImage' => '/public/images/nopicture.png',
	            'disableGlobalDnd' => true,
	            'accept' => [
	                'title' => 'Images',
	                'extensions' => 'gif,jpg,jpeg,bmp,png',
	                'mimeTypes' => 'image/*',
	            ],
                'islink'=>true,
	            'pick' => [
	                'multiple' => false,
	            ],
	        ],
	    ],
];
