<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'pkkolTMANp7vb0cXSdUkxH4E6xZVMUcn',
        ],
        'session' => [
            'name' => 'BACKSESSID',//可以自定义
            'savePath' => RUNTIME_PATH . '/session/',//手工在backend目录下新建文件夹TMP
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';
}


    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
return $config;
