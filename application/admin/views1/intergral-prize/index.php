<?php

use yii\helpers\Html;
use yii\grid\GridView;


$this->title = '积分奖品';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .a_margin{
        margin-left: 10px;
    }
</style>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">
        <?= Html::a('添加奖品', ['create','id'=>$id], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'prize_name',
                array(
                    'format' => 'raw',
                    'attribute' => 'cover_pic',
                    'label' => '奖品图片',
                    'value' => function ($filterModel) {
                        return Html::img(yii::$app->request->hostInfo . $filterModel->cover_pic, ['width' => 30]);
                    },
                ),
                'prize_grade',
                'prize_price',
                array(
                    'attribute' => 'sort',
                    'label' => '排序',
                ),
                // 'prize_describe:ntext',
                // 'create_time',
                // 'is_del',
                // 'is_show',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>'操作',
                    'template' => '{update}{view}{delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-globe"></span>', $url, ['title' => '查看', 'data-toggle' => 'modal','class'=>'a_margin']);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('<span class="fa fa-pencil"></span>', $url, ['title' => '更新', 'data-toggle' => 'modal','class'=>'a_margin']);
                        },
                        'delete' => function ($url, $model, $key) {
                          return Html::a('<span class="fa fa-bitbucket"></span>', $url, ['title' => '删除', 'data-toggle' => 'modal','class'=>'a_margin']);
                        },
                    ],

                ],
            ],
        ]); ?>
    </div>

</div>
