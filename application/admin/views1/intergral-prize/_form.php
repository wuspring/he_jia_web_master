<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\IntergralPrize */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="intergral-prize-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
        'fieldConfig' => [
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions'=>['class'=>'col-sm-2 control-label'],
            'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
    ]); ?>

    <?= $form->field($model, 'prize_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cover_pic')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => false,
            ],
            'server' => Url::toRoute(['intergral-prize/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ])->label("奖品主图"); ?>
    <?= $form->field($model, 'prize_grade')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prize_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sort')->textInput(['placeholder' =>'奖品按照从大到小排序'])->label('排序') ?>

    <?= $form->field($model, 'prize_describe')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '发布') : Yii::t('app', '保存修改'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
