<?php

use yii\helpers\Html;
use DL\service\UrlService;

/* @var $this yii\web\View */
/* @var $model common\models\StoreInfo */

$this->title = "编辑店铺";
$this->params['breadcrumbs'][] = ['label' => 'Store Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <ul class="nav nav-tabs dl_tab">
        <li class="col-lg-2 tab_title active"><a href="javascript:void(0);">基础信息</a></li>
        <li class="col-lg-2 tab_title"><a  href="<?=UrlService::build(['store-shop/index','storeId' => $model->user_id]);?>">门店列表</a></li>
    </ul>
    <?= $this->render('_form', [
        'model' => $model,
        'userModel' => $userModel,
    ]) ?>
</div>
