<?php

use yii\helpers\Html;
use yii\grid\GridView;
use DL\service\UrlService;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use \common\models\GoodsClass;

/* @var $this yii\web\View */
/* @var $searchModel app\search\StoreInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '商铺详情';
$this->params['breadcrumbs'][] = $this->title;

$index = 0;
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
    .goods-pic {
        width: 5.5rem;
        height: 5.5rem;
        display: inline-block;
        background-color: #ddd;
        background-size: cover;
        background-position: center;
        margin-right: 1rem;
    }
</style>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php //if (Yii::$app->user->id == $store->user_id) :?>
    <div class="panel-body">
         <?= Html::a('编辑', ['update', 'id' => $store->id], ['class' => 'btn btn-primary']) ?>
    </div>
    <?php //endif; ?>

    <ul class="nav nav-tabs dl_tab">
        <li class="col-lg-2 tab_title active" data-id="group" data-val="1"><a  href="javascript:void(0)">基础信息</a></li>
        <li class="col-lg-2 tab_title" data-id="group" data-val="2"><a  href="javascript:void(0)">品牌介绍</a></li>
        <li class="col-lg-2 tab_title" data-id="group" data-val="3"><a  href="javascript:void(0)">门店列表</a></li>
        <?php if ($joinTicket) :?>
<!--        <li class="col-lg-2 tab_title" data-id="group" data-val="4"><a  href="javascript:void(0)">活动设置</a></li>-->
        <?php endif; ?>
    </ul>

    <div class="panel-body" data-id="panel-body" data-val="1">
        <?php
        function analysisInfo (Array $data)
        {
            $infos = arrayGroupsAction($data, function (GoodsClass $s) {
                return $s->name;
            });

            return implode('、', $infos);
        }

        echo DetailView::widget([
            'model' => $store,
            'attributes' => [
                [
                    'attribute' => 'id',
                    'label' => '商户账户',
                    'value' => $store->user->username,
                ],
                [
                    'attribute' => 'nickname',
                    'label' => '商户名称',
                    'value' => $store->user->nickname,
                ],
                [
                    'attribute' => 'provinces_id',
                    'label' => '所在城市',
                    'value' => $store->user->cityInfo,
                ],
                [
                    'attribute' => 'store_amount',
                    'label' => '商铺数量',
                ],
                [
                    'attribute' => 'score',
                    'label' => '评价分数',
                ],
                [
                    'attribute' => 'id',
                    'label' => '所属栏目',
                    'value' => analysisInfo($store->user->gcInfo)
                ],
            ],
        ]) ?>


        <div class="panel-body">
        <?php $tabData = json_decode($store->tab_text, true);if ($tabData) :?>
            <?php
                $tabDatas = arrayGroupsAction($tabData, function ($str) {
                    list($tab, $info) = explode('@', $str);

                    return ['tab' => $tab, 'info' => $info];
                });

            ?>

            <table class="table">
                <thead>
                    标签信息
                </thead>
                <tbody>
                <tr>
                    <td class="col-md-3"><b>标签</b></td>
                    <td class="col-md-3"><b>内容</b></td>
                </tr>
                <?php foreach ($tabDatas as $val):?>
                    <tr>
                        <td class="col-md-3"><?= $val['tab']; ?></td>
                        <td class="col-md-9"><?= $val['info']; ?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>

            <table class="table">
                <thead>
                默认门店
                </thead>
                <tbody>
                <tr>
                    <th class="col-md-3"><b>店铺名称</b></th>
                    <th class="col-md-3"><b>联系电话</b></th>
                    <th class="col-md-3"><b>工作日</b></th>
                    <th class="col-md-3"><b>工作时间</b></th>
                </tr>
                <?php if ($store->defaultShop) : $defaultShop = $store->defaultShop;?>
                    <tr>
                        <td class="col-md-3"><?= $defaultShop->store_name; ?></td>
                        <td class="col-md-3"><?= $defaultShop->mobile; ?></td>
                        <td class="col-md-3">
                            <?= implode(' ', $defaultShop->workDaysInfo);?></td>
                        <td class="col-md-3"><?= $defaultShop->work_times; ?></td>
                    </tr>
                <?php else :?>
                    <tr><td colspan="4">门店未设置</td></tr>
                <?php endif; ?>
                </tbody>
            </table>

        <?php else :?>
        <div class="col-lg-12" style="text-align: center;height: 60px;line-height: 60px;">
            未设置标签信息
        </div>
        <?php endif; ?>
        </div>
    </div>

    <div class="panel-body" data-id="panel-body" data-val="2" style="display: none">
        <?= $store->describe; ?>
    </div>
    <div class="panel-body" data-id="panel-body" data-val="3" style="display: none">
        <table class="table">
            <thead>
            门店列表
            </thead>
            <tbody>
            <tr>
                <td class="col-md-3"><b>名称</b></td>
                <td class="col-md-3"><b>工作日期</b></td>
                <td class="col-md-3"><b>工作时间</b></td>
                <td class="col-md-3"><b>地址</b></td>
            </tr>
            <?php if ($storeShops) :?>
            <?php foreach ($storeShops as $shop):?>
                <tr>
                    <td class="col-md-3"><?= $shop->store_name; ?></td>
                    <td class="col-md-3"><?= $shop->workDaysinfo; ?></td>
                    <td class="col-md-3"><?= $shop->work_times; ?></td>
                    <td class="col-md-3"><?= $shop->address; ?></td>
                </tr>
            <?php endforeach;?>
            <?php else :?>
                <tr>
                    <td colspan="4" class="col-md-12" style="text-align: center">未添加门店信息</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>

    <div class="panel-body" data-id="panel-body" data-val="4" style="display: none">
        <?php
        $form = ActiveForm::begin([
            'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
            'fieldConfig' => [
                'inputOptions' => ['class' => 'form-control'],
                'labelOptions'=>['class'=>'col-sm-2 control-label'],
                'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
        ]);

        $shopGoods = \common\models\Goods::findAll([
                'user_id' => $store->user_id,
                'goods_state' => 1,
                'isdelete' => 0
        ]);
        $shopGoodsData = \yii\helpers\ArrayHelper::map($shopGoods, 'id', 'goods_name');

        $shopGoodsDataArray = [];
        if ($shopGoods) {
            foreach ($shopGoods AS $shopGood) {
                $shopGoodsDataArray[$shopGood->id] = $shopGood;
            }
        }

        $goodCoupons = \DL\Project\Store::init()->coupons($store->user_id);

//        $couponsData = \yii\helpers\ArrayHelper::map($goodCoupons, 'id', 'describe');
        $couponsData = [];
        if ($goodCoupons) {
            foreach ($goodCoupons AS $shopGood) {
                $couponsData[$shopGood->id] = $shopGood;
            }
        }
        ?>

        <?= $form->field($store, 'advance_goods_id')->dropDownList($shopGoodsData)->label('爆款预约') ?>
        <?php $form->field($store, 'order_goods_id')->checkboxList($shopGoodsDataArray,
            [
                'item' => function($index, $label, $name, $checked, $value) {
                    $checked= $checked ? "checked" : "";
                    $value = $label->id;

                   return <<<HTML
<div class="order-item col-sm-6" style="">
                <table class="table table-bordered bg-white">
                    <tbody>
                    <tr>
                     <td class="order-tab-2" style="display:table-cell; vertical-align:middle">
                        <input type="checkbox" id="{$name}{$value}" name="{$name}" value="{$value}" class="md-checkbox" {$checked}>
                        </td>
                        <td class="order-tab-1">
                             <div style="clear: both;">
                                    <div class="fs-0 col-md-2">
                                        <div class="goods-pic" style="height:40px;background-image: url('{$label->goods_pic}')"></div>
                                    </div>
                                    <div class="goods-info col-md-10">
                                        <div class="goods-name">{$label->goods_name}</div>
                                    </div>
                                </div>
                         </td>
                    </tr>
                </tbody>
                </table>
            </div>
HTML;
                }
            ]
        )->label('预存 享特价'); ?>
        <?= $form->field($store, 'store_coupon_id')->checkboxList($couponsData, [
            'item' => function($index, $label, $name, $checked, $value) {
                $checked= $checked ? "checked" : "";
                $value = $label->id;

                return <<<HTML
<div class="order-item col-sm-6" style="">
                <table class="table table-bordered bg-white">
                    <tbody>
                    <tr>
                     <td class="order-tab-2" style="display:table-cell; vertical-align:middle">
                        <input type="checkbox" id="{$name}{$value}" name="{$name}" value="{$value}" class="md-checkbox" {$checked}>
                        </td>
                        <td class="order-tab-1">
                             <div style="clear: both;">
                                    <div class="fs-0 col-md-2">
                                        <div class="goods-pic" style="height:40px;background-image: url('{$label->goods->goods_pic}')"></div>
                                    </div>
                                    <div class="goods-info col-md-10">
                                        <div class="goods-name"><b>{$label->describe}</b></div>
                                        <div class="goods-name">优惠金额：{$label->money}</div>
                                    </div>
                                </div>
                         </td>
                    </tr>
                </tbody>
                </table>
            </div>
HTML;
            }
        ])->label('店铺优惠劵') ?>
        <?= $form->field($store, 'sid', ['options' => ['style' => 'display:none; ']])->hiddenInput(['value' => $store->id])->label('商户名称'); ?>

        <div class="form-group" style="text-align: center;">
            <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
<div style="text-align:center;padding-bottom:40px;">
    <?= Html::Button('返回' , ['class' => 'btn btn-default', 'data-id' => 'return']) ?>
</div>
</div>
<script>
    $('[data-id="group"]').click(function () {
        var that = $(this);
        that.addClass('active').siblings().removeClass('active');
        $('[data-id="panel-body"]').each(function () {
            if ($(this).attr('data-val') == that.attr('data-val')) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    })


    $('[data-id="return"]').click(function(){
        window.history.go(-1)
    });
</script>
