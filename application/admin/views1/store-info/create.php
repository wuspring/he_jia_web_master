<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreInfo */

$this->title = 'Create Store Info';
$this->params['breadcrumbs'][] = ['label' => 'Store Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
