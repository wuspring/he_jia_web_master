<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\search\NewsclassifySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '文章分类';
$this->params['breadcrumbs'][] = $this->title;
?>
<link href="<?php echo Yii::$app->request->hostInfo;?>/public/css/tree.css" rel="stylesheet" type="text/css">
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <div class="panel-body">
        <?= Html::a('添加分类', Url::toRoute('/newsclassify/create'), ['class' => 'btn btn-success']) ?>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="panel-body">
        <div class="tree well">
        <ul>
            <?php foreach ($tree as $key => $value):?>
            <li>
                <span><i class="<?php echo !empty($value['tree'])? 'glyphicon glyphicon-chevron-up':'glyphicon glyphicon-chevron-down';?>"></i></span>
                <a href="javascript:void(0);"><?php echo $value['name'];?></a>
                <a  href="<?php echo Url::toRoute(['/newsclassify/create','cid'=>$value['id']]);?>"><i class="fa fa-plus"></i></a>
                <a href="<?php echo Url::toRoute(['/newsclassify/update','id'=>$value['id']]);?>"><i class="fa fa-pencil"></i></a>
<!--                 <a href="javascript:void(0);" class="delete"  ><i data="--><?php //echo $value['id'];?><!--" class="fa fa-trash-o"></i></a>-->
                <?php if (!empty($value['tree'])): ?>
                    <ul>
                    <?php foreach ($value['tree'] as $k1 => $v1):?>
                        <li style="display: none;">
                            <span><i class="<?php echo !empty($v1['tree'])? 'fa fa-plus-square':'fa fa-leaf';?>"></i></span>
                            <a href="javascript:void(0);"><?php echo $v1['name'];?></a>
                            <a style="display: none" href="<?php echo Url::toRoute(['/newsclassify/create','cid'=>$v1['id']]);?>"><i class="fa fa-plus"></i></a>
                            <a href="<?php echo Url::toRoute(['/newsclassify/update','id'=>$v1['id']]);?>"><i class="fa fa-pencil"></i></a>
<!--                              <a href="javascript:void(0);" class="delete" ><i  data="--><?php //echo $v1['id'];?><!--" class="fa fa-trash-o"></i></a>-->
                            <?php if (!empty($value['tree'])): ?>
                                <ul>
                                 <?php foreach ($v1['tree'] as $k2 => $v2):?>
                                    <li style="display: none;"><span><i class="<?php echo !empty($v2['tree'])? 'fa fa-minus-square':'fa fa-leaf';?>"></i></span>
                                    <a href="javascript:void(0);" value="<?php echo $v2['id'];?>"><?php echo $v2['name'];?></a>
                                    <a href="<?php echo Url::toRoute(['/newsclassify/update','id'=>$v2['id']]);?>"><i class="fa fa-pencil"></i></a>
                                    <a href="javascript:void(0);" class="delete" ><i data="<?php echo $v2['id'];?>" class="fa fa-trash-o"></i></a>
                                    </li>
                                  <?php endforeach?>
                                </ul>
                            <?php endif ?>
                        </li>
                    <?php endforeach?>
                    </ul>
                 <?php endif ?>
            </li>
        <?php endforeach?>
        </ul>
        </div>
        <input type="hidden" value="<?php echo Yii::$app->getRequest()->getCsrfToken();?>" name="_csrf" id="_csrf">
    </div>
<script type="text/javascript">
     init.push(function () {  
        $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', '展开分类');

        $('.tree li.parent_li > span').on('click', function (e) {
            var children = $(this).parent('li.parent_li').find(' > ul > li');
            if (children.is(":visible")) {
                children.hide();
                $(this).attr('title', '展开分类').find(' > i').addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-down');
            } else {
                children.show();
                $(this).attr('title', '折叠分类').find(' > i').addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
            }
            e.stopPropagation();
        }); 

        $(".fa-trash-o").click(function(){
            
            var del=$(this);
            var _csrf =$("#_csrf").val();
            $.post("<?php echo Url::toRoute('/newsclassify/delete');?>"+"&id="+$(del).attr('data'),{_csrf:_csrf},function(data){
                if (data.status) {
                    $(del).parent().parent().remove();
                    alert(data.mg);
                }else{
                    alert(data.mg);
                }

            },'json');
        });
    });

</script>