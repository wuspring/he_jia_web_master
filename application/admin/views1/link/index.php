<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\LinkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '友情链接';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="panel-body">
        <?= Html::a('添加友情链接', ['create'], ['class' => 'btn btn-success']) ?>
    </div>

	<div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',
            'url:url',
//            'imgval',
            array(
                'format' => 'raw',
                'attribute' => 'imgval',
                'label'=>'图片',
                'value'=>function($filterModel){
                    if ($filterModel->type==1){
                        return '无图片';
                    }else{
                        return Html::img(yii::$app->request->hostInfo.$filterModel->imgval, ['width' =>30]);
                    }

                },
            ),
            array(
                'format' => 'raw',
                'attribute' => 'type',
                'label'=>'类型',
                'value'=>function($filterModel){
                   if ($filterModel->type==1){
                       return '文字类型';
                   }else{
                       return '图片类型';
                   }
                },
            ),
//            'type',
            // 'createTime',
            // 'modifyTime',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '操作',
                'template' => '{view}{update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('查看', $url, ['title' => '查看', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('更新', $url, ['title' => '更新', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                    },
                    'delete' => function ($url, $model, $key) {
                         return Html::a('删除', $url, ['title' => '删除', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                    },
                ],

            ],
        ],
    ]); ?>
</div>
</div>
