<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Goods */
/* @var $form yii\widgets\ActiveForm */

?>

<script src="<?php echo yii::$app->request->hostInfo; ?>/public/ueditor/ueditor.config.js"></script>
<script src="<?php echo yii::$app->request->hostInfo; ?>/public/ueditor/ueditor.all.min.js"></script>
<script src="<?php echo yii::$app->request->hostInfo; ?>/public/ueditor/lang/zh-cn/zh-cn.js"></script>


<style>
    .inline .radio, .inline .checkbox {
        display: inline-block;
        margin: 0 5px;
    }

     #intergralgoods-goods_body, #intergralgoods-goods_jingle, #intergralgoods-mobile_body {
        margin-top: 20px;
        padding: 0;
        margin: 20px 0;
        width: 100%;
        height: auto;
        border: none;
    }


    .attr-group {
        border: 1px solid #eee;
        padding: .5rem .75rem;
        margin-bottom: .5rem;
        border-radius: .15rem;
    }

    .attr-group-delete {
        display: inline-block;
        background: #eee;
        color: #fff;
        width: 1rem;
        height: 1rem;
        text-align: center;
        line-height: 1rem;
        border-radius: 999px;
    }

    .attr-group-delete:hover {
        background: #ff4544;
        color: #fff;
        text-decoration: none;
    }

    .attr-list > div {
        vertical-align: top;
    }

    .attr-item {
        display: inline-block;
        background: #eee;
        margin-right: 1rem;
        margin-top: .5rem;
        overflow: hidden;
    }

    .attr-item .attr-name {
        padding: .15rem .75rem;
        display: inline-block;
    }

    .attr-item .attr-delete {
        padding: .35rem .75rem;
        background: #d4cece;
        color: #fff;
        font-size: 1rem;
        font-weight: bold;
    }

    .attr-item .attr-delete:hover {
        text-decoration: none;
        color: #fff;
        background: #ff4544;
    }

    .btn {
        line-height: inherit !important;
        border: 1px solid #d5d5d5 !important;
    }

    /*表格样式*/
    table#process {
        font-size: 11px;
        color: #333333;
        border-width: 1px;
        border-color: #666666;
        border-collapse: collapse;
    }

    table#process th {
        border-width: 1px;
        padding: 8px;
        border-style: solid;
        border-color: #666666;
        background-color: #dedede;
    }

    table#process td {
        border-width: 1px;
        padding: 8px;
        border-style: solid;
        border-color: #666666;
        background-color: #ffffff;
    }


</style>
<div class="panel-body form-controls-demo">
    <?php
    $form = ActiveForm::begin([
        'id' => 'goodsfrom',
        'options' => [
                'class' => 'form-horizontal',
//            'enctype' => 'multipart/form-data'
        ],
        'fieldConfig' => [
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions' => ['class' => 'col-sm-2 control-label '],
            'template' => "{label}<div class='col-sm-8'>{input}{hint}{error}</div>",],
    ]);
    $categorys = \common\models\Category::find()->where(['>', 'id', 22])->all();
    $categorys = \yii\helpers\ArrayHelper::map($categorys, 'id', 'title');

    ?>
    <?= $form->field($model, 'goods_name')->textInput(['maxlength' => true])->label('商品名称') ?>
        <?= $form->field($model, 'keywords')->textarea(['rows' => 6])->label('关键字') ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6])->label('描述') ?>
    

    <?= $form->field($model, 'gc_id')->dropDownList($model->getIntergralGoodsClass(), ['prompt' => '请选择商品分类'])->label('商品分类') ?>

    <?= $form->field($model, 'goods_integral')->textInput(['maxlength' => true, 'type' => 'number'])->label('积分价格') ?>

    <?= $form->field($model, 'goods_marketprice')->textInput(['maxlength' => true, 'type' => 'double'])->label('商品原价'); ?>

    <?= $form->field($model, 'goods_pic')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => false,
            ],
            'server' => Url::toRoute(['intergral-goods/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ])->label("商品主图"); ?>

    <?php $model->goods_image = json_decode($model->goods_image); echo $form->field($model, 'goods_image')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => true,
            ],
            'server' => Url::toRoute(['intergral-goods/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ])->label("商品相册"); ?>

    <?= $form->field($model, 'goods_storage')->textInput(['type' => 'number'])->label('商品数量') ?>

<!--    --><?php //$form->field($model, 'goods_weight')->textInput(['maxlength' => true, 'type' => 'number']) ?>

    <?php $form->field($model, 'goods_storage_alarm')->textInput() ?>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="intergralgoods-goods_storage_alarm"></label>
        <div class="col-sm-7">
            <?= $form->field($model, 'use_attr')->checkbox([], false)->label('启用规格') ?>
        </div>
    </div>

    <div class="form-group" id="sku" style="<?php echo $model->use_attr ? '' : 'display: none;'; ?>">
        <label class="col-sm-2 control-label" for="intergralgoods-goods_storage_alarm">规格组</label>
        <div class="col-sm-8">
            <div class="input-group">
                <span class="input-group-addon">规格组</span>
                <input type="text" class="form-control" id="attrgroupname" placeholder="如颜色、尺码、套餐">
                <span class="input-group-btn">
                        <button class="btn" type="button" id="skugroup">添加</button>
                    </span>
            </div>
            <div class="panel-body" id="group">


            </div>
            <div class="panel-body" id="guigezu">

            </div>

        </div>
    </div>

    <div style="display: none">
        <?= $form->field($model, 'attr')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'goods_spec')->textarea(['rows' => 6]) ?>
    </div>

    <script>
        var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
        var attrgroup = [];
        var goodssku = [];
        init.push(function () {
            $(function () {
                var attr = $("#intergralgoods-attr").val();
                var group = $("#intergralgoods-goods_spec").val();
                attrgroup = group.length ? JSON.parse(group) : [];
                goodssku = attr.length ? JSON.parse(attr) : [];

                $(goodssku).each(function (key, item) {
                    goodssku[key].attr = JSON.parse(item.attr.toString());
                });

                <?php if($model->use_attr == 1):?>
                $(attrgroup).each(function (key, val) {
                    $("#group").append(addgrouplist(val, key))
                });

                caratTable(attrgroup);
                <?php endif?>
            });
            $("#skugroup").click(function () {
                var index = $("#group .attr-group").length;
                // alert(index);

                if (attrgroup.length == 3 || attrgroup.length > 3) {
                    alert("最多添加3个规格组")
                    return;
                }

                var attrgroupname = $("#attrgroupname").val();
                if (attrgroupname.length == 0) {
                    return;
                }
                var isgroup = false;
                $(attrgroup).each(function (key, val) {
                    if (attrgroupname == val.group_name) {
                        isgroup = true;
                        return;
                    }
                });
                if (isgroup) {
                    alert("该规格组已添加");
                    return;
                }

                $.post('<?php echo Url::toRoute(['intergral-goods/attr-group-add']);?>', {
                    _csrf: _csrf,
                    name: attrgroupname
                }, function (res) {
                    if (res.state == 1) {
                        attrgroup.push(res.model);
                        $("#group").append(addgroup(res.model, index));

                    } else {
                        alert(res.msg);
                    }
                }, 'json');
            });

            $(document).on('click', ".attr-group-delete", function () {

                var deletebut = this;
                $(attrgroup).each(function (key, val) {

                    var id = $(deletebut).attr('data-id');
                    if (id == val.id) {
                        attrgroup.splice(key, 1);

                        $(deletebut).parent().parent().remove();
                    }
                })
            });

            $(document).on('click', ".add-attr-btn", function () {
                var groupid = $(this).attr('data-id');
                var addbut = this;
                var name = $(this).parent().parent().find('.add-attr-input').val();
                var isattr = false;
                $(attrgroup).each(function (key, val) {
                    if (groupid == val.id) {
                        $(val.attr).each(function (k, item) {
                            if (name == item.attr_name) {
                                isattr = true;
                                return;
                            }
                        });
                    }
                });
                if (isattr) {
                    alert("该规格已存在");
                    return;
                }

                $.post('<?php echo Url::toRoute(['intergral-goods/attr-add']);?>', {
                    _csrf: _csrf,
                    groupid: groupid,
                    name: name
                }, function (res) {
                    if (res.state == 1) {
                        $(attrgroup).each(function (key, val) {

                            if (groupid == val.id) {
                                attrgroup[key].attr.push(res.model);
                                $(addbut).parent().parent().parent().before(addAttr(res.model));

                                caratTable(attrgroup);
                            }
                        });

                    } else {
                        alert(res.msg);
                    }
                }, 'json');


            });
            $(document).on('click', '.attr-delete', function () {
                var groupid = $(this).attr('group-id');
                var attrid = $(this).attr("data-id");
                var deletebut = this;

                $(attrgroup).each(function (key, val) {
                    if (groupid == val.id) {
                        $(val.attr).each(function (k, item) {
                            if (attrid == item.id) {
                                attrgroup[key].attr.splice(k, 1);
                                var skuddd = [];
                                $(goodssku).each(function (i, sku) {
                                    var isonsku = true;
                                    $(sku.attr).each(function (v, attr) {
                                        if (attr == attrid) {
                                            isonsku = false;
                                        }
                                    });
                                    if (isonsku) {
                                        skuddd.push(sku);
                                    }
                                });
                                goodssku = skuddd;
                                $(deletebut).parent().remove();
                                caratTable(attrgroup);
                                console.log(goodssku)
                            }
                        });
                    }
                })
            });

            $("#guigezu").on("change", "input[name='sku_integral']", function () {
                var integral = $(this).val();
                var attr = $(this).attr("data-attr");
                $(goodssku).each(function (key, item) {

                    if (JSON.stringify(item.attr) == attr) {
                        goodssku[key].integral = integral;
                    }
                });
                console.log(goodssku);
            });
            $("#guigezu").on("change", "input[name='sku_num']", function () {
                var sku_num = $(this).val();
                var attr = $(this).attr("data-attr");
                $(goodssku).each(function (key, item) {
                    if (JSON.stringify(item.attr) == attr) {
                        goodssku[key].num = sku_num;
                    }
                });
            });
            $("#guigezu").on("change", "input[name='sku_code']", function () {
                var sku_code = $(this).val();
                var attr = $(this).attr("data-attr");
                $(goodssku).each(function (key, item) {
                    if (JSON.stringify(item.attr) == attr) {
                        goodssku[key].goods_code = sku_code;
                    }
                });
            });
            $("#guigezu").on("change", "input[name='sku_picimg']", function () {
                var sku_picimg = $(this).val();
                var attr = $(this).attr("data-attr");
                $(goodssku).each(function (key, item) {
                    if (JSON.stringify(item.attr) == attr) {
                        goodssku[key].picimg = sku_picimg;
                    }
                });
            });
            $("#goodsfrom").submit(function () {
                var isgroup = false;
                $(attrgroup).each(function (key, item) {
                    if (item.attr.length == 0) {
                        isgroup = true;
                    }
                });
                if (isgroup) {
                    alert("请在空的规格组中添加规格值")
                    return false;
                }

                $("#intergralgoods-goods_spec").val(JSON.stringify(attrgroup));
                $("#intergralgoods-attr").val(JSON.stringify(goodssku));

                return true;
            });

            $(document).on('click', '.upload-attr-pic', function () {
                var file = $(this).parent().parent().find("input[type='file']");
                $(file).click();
                var skuimg = this;
                $(file).change(function () {
                    var formData = new FormData();
                    formData.append('_csrf', _csrf);
                    formData.append("file", this.files[0]);
                    $.ajax({
                        type: "POST", // 数据提交类型
                        url: "<?php echo Url::toRoute(['intergral-goods/upload']);?>", // 发送地址
                        data: formData, //发送数据
                        async: true, // 是否异步
                        dataType: 'json',
                        processData: false, //processData 默认为false，当设置为true的时候,jquery ajax 提交的时候不会序列化 data，而是直接使用data
                        contentType: false, //
                        success: function (re) {
                            if (re.code == 0) {
                                var skuimgpic = $(skuimg).parent().parent().find("input[name='sku_picimg']");
                                skuimgpic.val(re.attachment);
                                var sku_picimg = $(skuimgpic).val();

                                var attr = $(skuimgpic).attr("data-attr");
                                $(goodssku).each(function (key, item) {
                                    if (JSON.stringify(item.attr) == attr) {
                                        goodssku[key].picimg = sku_picimg;
                                    }
                                });
                            }
                        },
                        error: function () {
                            alert("上传失败")

                        },

                    });
                });
            });

            $(document).on('click', '.delete-attr-pic', function () {
                var skuimgpic = $(this).parent().parent().find("input[name='sku_picimg']");
                skuimgpic.val('');

                var attr = $(skuimgpic).attr("data-attr");
                $(goodssku).each(function (key, item) {
                    if (JSON.stringify(item.attr) == attr) {
                        goodssku[key].picimg = '';
                    }
                });
            });
        });


    </script>
    <script src="<?php echo yii::$app->request->hostInfo; ?>/public/js/setsuk.js"></script>
    <?= $form->field($model, 'goods_body')->textarea(['rows' => 6])->label('商品详情') ?>
    <?= $form->field($model, 'goods_jingle')->textarea(['rows' => 3])->label('商品规格') ?>

    <?= $form->field($model, 'mobile_body')->textarea(['rows' => 6])->label('售后保障') ?>

    <?php echo $form->field($model, 'goods_hot')->radioList([
        '1' => '是',
        '0' => '否',
    ])->label('积分首页展示') ?>
    <?php $form->field($model, 'is_virtual')->radioList(['0' => '否', '1' => '是'])->label('是否为虚拟商品') ?>
    <?php $form->field($model, 'is_appoint')->radioList(['0' => '否', '1' => '是'])->label('是否是预约商品') ?>
    <?php $form->field($model, 'is_presell')->radioList(['0' => '否', '1' => '是'])->label('是否热销') ?>
    <?= $form->field($model, 'goods_state')->radioList(['0' => '下架', '1' => '在售'])->label('商品状态') ?>
    <?php $form->field($model, 'goods_vat')->radioList(['0' => '否', '1' => '是'])->label('是否开具增值税发票') ?>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-3">
            <?= Html::submitButton($model->isNewRecord ? '添加' : '保存', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <a href="<?php echo Url::toRoute(['intergral-goods/index']); ?>" class="btn btn-w-m btn-default">取消</a>
        </div>
    </div>


    <?php ActiveForm::end(); ?>
</div>
<script type="text/javascript">
    // $(function() {
        var setIntegralLabel = function(val) {
            var text;
            val = parseInt(val);
            console.log(val);
            switch (val) {
                case 1 :
                    text = '赠送积分';
                    break;
                case 2 :
                    text = '积分价格';
                    break;
            }
            $('label[for="intergralgoods-integral"]').text(text);
        };
       $('input[name="Goods[type]"]').click(function () {
           var val = $('input[name="Goods[type]"]:checked').val();
           setIntegralLabel(val);
       });
        $('input[name="Goods[type]"]:checked').click();
    // });
    init.push(function () {
        $('#intergralgoods-use_attr').switcher({on_state_content: '开启', off_state_content: '关闭'});
        var ue = UE.getEditor('intergralgoods-goods_body', {
            'zIndex': 9,
        });
        var ue = UE.getEditor('intergralgoods-goods_jingle', {
            'zIndex': 9,
        });
        var ue = UE.getEditor('intergralgoods-mobile_body', {
            'zIndex': 9,
        });

        $("#intergralgoods-use_attr").click(function () {
            var isto = $(this).is(':checked');
            if (isto) {
                $("#sku").show();
                $(this).val(1);
                $("#intergralgoods-goods_serial").attr("disabled", "disabled");
                $("#intergralgoods-goods_storage").attr("disabled", "disabled");
            } else {
                $("#sku").hide();
                $(this).val(0);
                $("#intergralgoods-goods_serial").removeAttr("disabled");
                $("#intergralgoods-goods_storage").removeAttr("disabled");
            }
        });
    });


</script>