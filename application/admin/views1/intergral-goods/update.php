<?php

use yii\helpers\Html;
use DL\service\UrlService;
/* @var $this yii\web\View */
/* @var $model common\models\Goods */

$this->title =($defaultTitle==0)?'编辑商品':'添加商品';
$this->params['breadcrumbs'][] = ['label' => 'Goods', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <ul class="nav nav-tabs dl_tab">
        <li class="col-lg-2 tab_title <?=('goods'==$active)?'active':''?>"><a  href="<?=UrlService::build(['goods/update','id'=>$model->id])?>">商品</a></li>
        <li class="col-lg-2 tab_title <?=('coupon'==$active)?'active':''?>"><a  href="<?=UrlService::build(['goods/coupon-list','goodId'=>$model->id])?>">优惠劵</a></li>
    </ul>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>