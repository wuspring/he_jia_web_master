<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Brand */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .inline .radio,.inline .checkbox{display: inline-block;margin: 0 5px;}
    .btn{
        line-height: inherit!important;
        border: 1px solid #d5d5d5!important;
    }
</style>
<div class="panel-body form-controls-demo">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
        'fieldConfig' => [
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions'=>['class'=>'col-sm-2 control-label '],
            'template' => "{label}<div class='col-sm-8'>{input}{hint}{error}</div>",],
    ]); ?>

    <?= $form->field($model, 'brand_name')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'brand_initial')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'brand_pic')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => false,
            ],
            'server' => Url::toRoute(['brand/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ])->label("品牌Logo"); ?>

    <?php $form->field($model, 'brand_sort')->textInput() ?>

    <?php $form->field($model, 'brand_recommend')->radioList([0=>'否',1=>'是']) ?>

    <?php // $form->field($model, 'class_id')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'show_type')->radioList([0=>'图片',1=>'文字']) ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
