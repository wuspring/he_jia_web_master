<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FitupProblem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fitup-problem-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'is_show')->dropDownList([
            '1' => '通过',
            '0' => '未通过'
    ]) ?>

    <?= $form->field($model, 'sort')->textInput()->label('排序') ?>
    <div class="form-group">
        <?= Html::submitButton('保存', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
