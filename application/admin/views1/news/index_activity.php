<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '品牌活动';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .a_margin{margin-left: 10px}
</style>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="panel-body">
        <?= Html::a('添加活动文章', ['create-activity'], ['class' => 'btn btn-success']) ?>
    </div>

    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            array(
                'format' => 'raw',
                'attribute' => 'themeImg',
                'label' => '图像',
                'value' => function ($filterModel) {
                    return Html::img(yii::$app->request->hostInfo . $filterModel->themeImg, ['width' => 30]);
                },
            ),
            [
                'attribute' => 'id',
                'label' => 'ID',
            ],
            'title',
            [
                'attribute' => 'type',
                'label' => '类型',
                'value' => function ($filterModel) {
                    return $filterModel->typeInfo;
                },
            ],
            // 'themeImg',
            // 'themeImgTm',
            // 'brief',
            // 'content:ntext',
            // 'seokey',
            // 'seoDepict',
            [
                'attribute' => 'isShow',
                'label' => '是否显示',
                'value'=>function($filterModel){
                    return ($filterModel->isShow==0)?'不显示':'显示';
                }
            ],
            // 'isRmd',
//            'clickRate',
//            'writer',
            // 'uid',
            [
                'attribute' => 'provinces_id',
                'label' => '选择城市',
                'value'=>function($filterModel){
                        return $filterModel->cityName;
                }
            ],
            [
                'attribute' => 'createTime',
                'label' => '添加时间',
            ],



//            array(
//                'attribute' => 'isRmd',
//                'label' => '推荐',
//                'format' => 'raw',
//                'value' => function ($filterModel) {
//                    $url = "javascript:void(0)";
//                    if ($filterModel->isRmd == 0) {
//                        return Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', $url, ['title' => '是/否推荐', 'data-toggle' => $filterModel->id, 'data-status' => $filterModel->isRmd, 'id' => 'data-recommend']);
//                    } else {
//                        return Html::a('<span class="glyphicon glyphicon-ok-sign"></span>', $url, ['title' => '是/否推荐', 'data-toggle' => $filterModel->id, 'data-status' => $filterModel->isRmd, 'id' => 'data-recommend']);
//                    }
//                }
//
//            ),

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '操作',
                'template' => '{update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = Yii::$app->request->hostInfo . "/admin.php?r=news/view&id=" . $model->id;
                        return Html::a('查看', $url, ['title' => '查看', 'data-toggle' => 'modal','class'=>'a_margin']);
                    },
                    'update' => function ($url, $model, $key) {
                        $url = Yii::$app->request->hostInfo . "/admin.php?r=news/update-activity&id=" . $model->id;
                        return Html::a('更新', $url, ['title' => '更新', 'data-toggle' => 'modal','class'=>'a_margin']);
                    },
                    'delete' => function ($url, $model, $key) {
                        $unDeleteArray = [1];
                        if (!in_array($model->id,$unDeleteArray)) {
                            return Html::a('删除', $url, ['title' => '删除', 'data-toggle' => 'modal','class'=>'a_margin']);
                        }
                    },
                ],

            ],
        ],
    ]); ?>
    </div>
</div>
<script type="text/javascript">
    init.push(function () {
        var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
        $("#data-recommend").live('click', function () {
            var th = $(this);
            var id = $(this).attr('data-toggle');
            var status = $(this).attr('data-status');
            if (status == 0) {
                status = 1;
            } else {
                status = 0;
            }
            $.post("<?php echo Url::toRoute(['/news/recommend']);?>", {
                _csrf: _csrf,
                id: id,
                status: status
            }, function (data) {
                if (data == 0) {
                    $(th).find('span').attr('class', 'glyphicon glyphicon-ban-circle');
                    $(th).attr('data-status', 0);
                } else {
                    $(th).find('span').attr('class', 'glyphicon glyphicon-ok-sign');
                    $(th).attr('data-status', 1);
                }
            });
        });

        $("#data-isShow").live('click', function () {
            var th = $(this);
            var id = $(this).attr('data-toggle');
            var status = $(this).attr('data-status');
            if (status == 0) {
                status = 1;
            } else {
                status = 0;
            }
            $.post("<?php echo Url::toRoute(['/news/isshow']);?>", {
                _csrf: _csrf,
                id: id,
                status: status
            }, function (data) {
                if (data == 0) {
                    $(th).find('span').attr('class', 'glyphicon glyphicon-eye-close');
                    $(th).attr('data-status', 0);
                } else {
                    $(th).find('span').attr('class', 'glyphicon glyphicon-eye-open');
                    $(th).attr('data-status', 1);
                }
            });
        });
    });
</script>