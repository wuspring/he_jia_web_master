<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FitupAnswer */

$this->title = 'Create Fitup Answer';
$this->params['breadcrumbs'][] = ['label' => 'Fitup Answers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fitup-answer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
