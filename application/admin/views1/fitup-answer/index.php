<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\search\FitupAnswerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fitup Answers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fitup-answer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Fitup Answer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'member_id',
            'pid',
            'content:ntext',
            'create_time',
            // 'is_del',
            // 'is_show',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
