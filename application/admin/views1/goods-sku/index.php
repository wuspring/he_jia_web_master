<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\GoodsSkuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Goods Skus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goods-sku-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Goods Sku', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'goods_id',
            'attr:ntext',
            'num',
            'price',
            // 'integral',
            // 'goods_code',
            // 'picimg',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
