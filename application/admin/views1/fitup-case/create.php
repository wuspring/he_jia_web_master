<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FitupCase */

$this->title = '添加案例';
$this->params['breadcrumbs'][] = ['label' => 'Fitup Cases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'strHtml'=>$strHtml,
        'drowList'=>$drowList
    ]) ?>

</div>
