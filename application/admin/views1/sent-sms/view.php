<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SentSms */

$this->title = '查看';
$this->params['breadcrumbs'][] = ['label' => 'Sent Sms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <div class="panel-title"><?= Html::encode($this->title) ?></div>
    </div>


    <div class="panel-body">
        <?= Html::a('列表页', ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
//            'access_key_id',
//            'access_key_secret',
            'sms_code',
            'sms_name',
            'type',
            'content',
//            'is_show',
//            'is_del',
//            'is_access_keys',
            'create_time',
        ],
    ]) ?>

</div>
