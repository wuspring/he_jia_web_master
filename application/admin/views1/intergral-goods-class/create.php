<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Goodsclassify */

$this->title = '添加商品分类';
$this->params['breadcrumbs'][] = ['label' => 'Goodsclassifies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'parent'=>$parent,
    ]) ?>

</div>