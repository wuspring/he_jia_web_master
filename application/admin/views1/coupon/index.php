<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\CouponSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '优惠券列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="panel-body">
        <?= Html::a('添加优惠券', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'store_id',
            [
                'format'=>'raw',
                'attribute'=>'discount_type',
                'value'=>function($model){
                    switch ($model->discount_type){
                        case 1:
                            $status_txt = '满减';break;
                        case 2:
                            $status_txt = '折扣';break;
                        default:
                            $status_txt = '满减';
                    }
                    return $status_txt;
                }
            ],
            'name',
            'desc',
//            'pic_url:url',
//            'discount_type',
             'min_price',
             'sub_price',
            // 'discount',
            // 'expire_type',
            // 'expire_day',
            // 'begin_time',
            // 'end_time',

            // 'is_delete',
             'total_count',
            [
                'attribute' => 'addtime',
                'label' => '添加时间',
            ],
            // 'is_join',
//             'sort',
            // 'is_integral',
            // 'integral',
            // 'price',
            // 'total_num',
            // 'type',
            // 'user_num',
            // 'goods_id_list',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
