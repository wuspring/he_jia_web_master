<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\search\AccountLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '消费记录';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'member_id',
                'label' => '用户名',
                'value' => function($model) {
                    return $model->member->wxnick;
                }
            ],
            [
                'attribute' => 'type',
                'value' => function($model) {
                    return $model->typeInfo;
                }
            ],
            'money',
            'integral',
             'remark',
            // 'member_id',
             'create_time',

//            [
//                'class' => 'yii\grid\ActionColumn',
//                'header' => '操作',
//                'template' => '',
//                'buttons' => [
//                        'view' => function($url, $model, $key) {
//                            return Html::a(
//                                    '<span class="glyphicon glyphicon-eye-open"></span>',
//                                    Url::toRoute(['order/view', 'id' => $model->orderid]),
//                                    ['title' => '查看', 'data-toggle' => 'modal']
//                            );
//                        },
//                ]
//            ],
        ],
    ]); ?>
    </div>
</div>
