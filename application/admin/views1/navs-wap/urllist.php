<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\NavsWapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '手机端地址说明';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <div class="panel-body">
        <p>参数部分请查询相应id填上</p>
        <?php foreach ($list as $item):?>
        <p>
            <?php echo $item['name'].":".$item['url'];?>
        </p>
        <?php endforeach;?>

    </div>
</div>
