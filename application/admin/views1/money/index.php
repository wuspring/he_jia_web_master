<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MoneySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '财务管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <div class="panel-body">
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            array('attribute'=>'member_id',
                'label'=> '姓名',
                'value' =>function ($filterModel){
                    return $filterModel->member->wxnick;
                }),
            array('attribute'=>'type',
                'label'=> '	账户类型',
                'value' =>function ($filterModel){
                    return $filterModel->typeInfo;
                }),
            'money',
            array('attribute'=>'status',
                'label'=> '申请状态',
                'value' =>function ($filterModel){
                    return $filterModel->statusInfo;
                }),
             'create_time',

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'审核',
                'template' => '{update}'],
        ],
    ]); ?>
    </div>
</div>
<script src="/public/js/jquery-1.8.3.min.js"></script>
<script>
    $(function () {
        $('#w0 .form-group').addClass('col-sm-2').last().css('margin-top', '25px');
        $('#w0 .form-group').removeClass('form-group');
        $('#w0').addClass('form-group');
    })
</script>