<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\GroupOrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="group-order-search">

    <?php
    $moneyModel = new \common\models\Money();

    $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


    <?= $form->field($model, 'type')->dropDownList($moneyModel->getTypeDic(), ['prompt'=>'请选择']) ?>
    <?= $form->field($model, 'status')->dropDownList($moneyModel->getStatusDic(), ['prompt'=>'请选择']) ?>

    <div class="form-group">
        <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('清除', '/admin.php?r=money/index',['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
