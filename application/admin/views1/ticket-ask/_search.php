<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\search\TicketAskSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ticket-ask-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'name')->label('姓名') ?>

    <?php  echo $form->field($model, 'mobile')->label('手机号') ?>

    <?= $form->field($model, 'start_time')->label('起始时间') ?>

    <?= $form->field($model, 'end_time')->label('截止时间')?>

    <input type="hidden" name="id" value="<?= $_GET['id']; ?>" />
    <div class="form-group">
        <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('重置', \DL\service\UrlService::build(['ticket-ask/index', 'id' => $_GET['id']]), ['class' => 'btn btn-default']) ?>
        <?= Html::a('导出', \DL\service\UrlService::build(array_merge(['ticket-ask/loader'], $_GET)), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script src="<?php echo Yii::$app->request->hostInfo;?>/public/My97DatePicker/WdatePicker.js"></script>
<script>
    //点击出来日历
    $("#ticketasksearch-start_time").click(function(){
        WdatePicker({dateFmt:'yyyy-MM-dd'})
    });
    $("#ticketasksearch-end_time").click(function(){
        WdatePicker({dateFmt:'yyyy-MM-dd'})
    });
</script>