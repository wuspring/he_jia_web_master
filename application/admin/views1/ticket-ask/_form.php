<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TicketAsk */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="panel-body">
    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>姓名</th>
            <td><?=$model->name?></td>
        </tr>
        <tr>
            <th>城市</th>
            <td><?=$model->cityName->cname?></td>
        </tr>
        <tr>
            <th>票据</th>
            <td><?=$model->ticket->name?></td>
        </tr>
        <tr>
            <th>数量</th>
            <td><?=$model->ticket_amount?></td>
        </tr>
        <tr>
            <th>渠道</th>
            <td><?=$model->resource?></td>
        </tr>

        <tr>
            <th>时间</th>
            <td><?=$model->create_time?></td>
        </tr>
        </tbody>
    </table>
</div>


<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"> 更改数据</span>
    </div>
    <div class="panel-body">

        <?php $form = ActiveForm::begin([
            'id' => 'goodsfrom',
            'options' => [
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data'
            ],
            'fieldConfig' => [
                'inputOptions' => ['class' => 'form-control'],
                'labelOptions' => ['class' => 'col-sm-2 control-label '],
                'template' => "{label}<div class='col-sm-8'>{input}{hint}{error}</div>",],
        ]); ?>


        <?= $form->field($model, 'mobile')->textInput(['maxlength' => true])->label('手机号') ?>

        <?= $form->field($model, 'address')->textarea(['rows' => 6])->label('地址') ?>

        <?= $form->field($model, 'shipping_code')->textInput(['maxlength' => true])->label('快递号') ?>

        <?= $form->field($model, 'status')->dropDownList($model->statusDic)->label('状态'); ?>

        <div class="form-group">
            <label class="col-sm-2 control-label "></label>
            <div class="col-sm-8">
                <?= Html::submitButton($model->isNewRecord ? '提交' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>








