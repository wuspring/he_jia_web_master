<?php

use common\models\User;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = '修改: ' . ' ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="panel">

   	<div class="panel-heading">
		<span class="panel-title"><?= Html::encode($this->title) ?></span>
	</div>

    <?php if ($type==User::ASSIGNMENT_HOU_TAI):?>
        <?= $this->render('_form', [
            'model' => $model,
            'type'=>$type
        ]) ?>
    <?php else:?>
        <?= $this->render('_form_admin', [
            'model' => $model,
            'type'=>$type
        ]) ?>
    <?php endif?>

</div>