<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FloorIndex */

$this->title = "楼层设置";
$this->params['breadcrumbs'][] = ['label' => 'Floor Indices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
