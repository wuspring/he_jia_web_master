<?php

use yii\helpers\Html;
use yii\grid\GridView;


$this->title = '积分文章';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
 .a_margin{
     margin-left: 10px;
 }
</style>
<div class="panel">

    <div class="panel-heading">
        <div class="panel-title"><?= Html::encode($this->title) ?></div>
    </div>
     <div class="panel-body">
         <?= Html::a('添加文章', ['create'], ['class' => 'btn btn-success']) ?>
     </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'title',
                [
                    'attribute' => 'read_num',
                    'label' => '阅读数量',

                ],
                [
                    'attribute' => 'is_recommend',
                    'label' => '是否推荐',
                    'value'=>function($fildModel){
                        return ($fildModel->is_recommend==0)?'不推荐':'推荐';
                    }
                ],
                [
                    'attribute' => 'create_time',
                    'label' => '添加时间',

                ],
                [
                    'attribute' => 'is_show',
                    'label' => '是否显示',
                    'value'=>function($fildModel){
                        return ($fildModel->is_show==0)?'不显示':'显示';
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => '操作',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('查看', $url, ['title' => '查看', 'data-toggle' => 'modal','class'=>'a_margin']);
                        },
                        'update' => function ($url, $model, $key) {

                            return Html::a('更新', $url, ['title' => '更新', 'data-toggle' => 'modal','class'=>'a_margin']);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('删除', $url, ['title' => '删除', 'data-toggle' => 'modal','class'=>'a_margin']);
                        },
                    ],

                ],
            ],
        ]); ?>
    </div>

</div>
