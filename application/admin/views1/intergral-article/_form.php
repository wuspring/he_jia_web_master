<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\IntergralArticle */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/js/uploadPreview.js');
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/ueditor/ueditor.config.js');
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/ueditor/ueditor.all.min.js');
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/ueditor/lang/zh-cn/zh-cn.js');
?>
<style>
    #intergralarticle-content {margin-top: 20px;padding:0;margin:20px 0;width:100%;height:auto;border: none;}
</style>
<div class="intergral-article-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
        'fieldConfig' => [
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions'=>['class'=>'col-sm-2 control-label'],
            'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'seokey')->textarea(['rows' => 6])->label('关键字') ?>
    <?= $form->field($model, 'seoDepict')->textarea(['rows' => 6])->label('描述') ?>
    
    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_recommend')->radioList(['0'=>'不推荐','1'=>'推荐']) ?>

    <?= $form->field($model, 'is_show')->radioList(['0'=>'不显示','1'=>'显示']) ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '发布') : Yii::t('app', '保存修改'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    init.push(function () {
        var ue = UE.getEditor('intergralarticle-content',{
            'zIndex':9,
        });
    });
</script>