<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\IntergralArticle */

$this->title = '添加文章';
$this->params['breadcrumbs'][] = ['label' => 'Intergral Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

     <div class="panel-heading">
          <div class="panel-title"><?= Html::encode($this->title) ?></div>
     </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
