<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HelpType */

$this->title = '添加种类';
$this->params['breadcrumbs'][] = ['label' => 'Help Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
   <div class="panel-heading">
       <span class="panel-title"><?= Html::encode($this->title) ?></span>
   </div>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
