<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TicketApply */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel-body form-controls-demo">
<div class="ticket-apply-form">


    <?php
        $form = ActiveForm::begin();

        $goodDatas = [];

        call_user_func(function ($datas, $result) {

            foreach ($datas AS $data) {

                $result['data'][$data->id] = "<img style='width: 80px; height: 60px;' src='{$data->goods_pic}'>$data->goods_name";

            }
        }, $goods, ['data' => &$goodDatas]);

        function newCheckbox($index, $label, $name, $checked, $value){
            $checkStr = $checked ? "checked" : "";
            return '<label><input type="checkbox" style="margin:10px" name="' . $name . '" value="' . $value . '" ' . $checkStr . ' class="class' . $index . '">' . $label . '</label>';
    }
    ?>

    <?= $form->field($model, 'good_ids')->checkboxList($goodDatas, ['item'=>'newCheckbox'])->label('参展商品') ?>
    <?= $form->field($model, 'id', ['options' => ['style' => 'display:none']])->hiddenInput(['value' => $model->id]) ?>
    <?= $form->field($model, 'user_id', ['options' => ['style' => 'display:none']])->hiddenInput(['value' => $model->user_id]) ?>
    <?= $form->field($model, 'user_id', ['options' => ['style' => 'display:none']])->hiddenInput(['value' => $model->user_id]) ?>
    <?= $form->field($model, 'user_id', ['options' => ['style' => 'display:none']])->hiddenInput(['value' => $model->user_id]) ?>
    <?= $form->field($model, 'user_id', ['options' => ['style' => 'display:none']])->hiddenInput(['value' => $model->user_id]) ?>


    <div class="form-group">
        <?= Html::submitButton('提交', ['class' =>  'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
