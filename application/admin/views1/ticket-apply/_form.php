<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TicketApply */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel-body form-controls-demo">
<div class="ticket-apply-form">


    <?php
        $form = ActiveForm::begin();

    ?>

    <?= $form->field($model, 'id', ['options' => ['style' => 'display:none']])->hiddenInput(['value' => $model->id]) ?>
    <?= $form->field($model, 'user_id', ['options' => ['style' => 'display:none']])->hiddenInput(['value' => $model->user_id]) ?>

    <?php if ($model->status != 1) :?>
    <div class="form-group" style="text-align: center">
        <?= Html::submitButton('提交', ['class' =>  'btn btn-primary']) ?>
    </div>
    <?php endif; ?>

    <?php ActiveForm::end(); ?>

</div>
</div>
