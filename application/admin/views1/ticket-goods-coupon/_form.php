<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TicketGoodsCoupon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ticket-goods-coupon-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ticket_id', ['options' => ['style' => 'display:none']])->textInput(['value' => $ticketId]) ?>

    <?php
    
    echo $form->field($model, 'goods_coupon_id')->dropDownList([]) ?>

    <?= $form->field($model, 'sort')->textInput(['maxlength' => true])->label('权重') ?>

    <div class="form-group">
        <?= Html::submitButton('保存', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
