<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\Request;
/* @var $this yii\web\View */
/* @var $model app\Models\Config */

$this->title = "拓展设置"

?>

<!-- 5. $JQUERY_VALIDATION =========================================================================

				jQuery Validation
-->
				<!-- Javascript -->
				<script>
					init.push(function () {
						$("#jq-validation-phone").mask("(999) 999-9999");
						$('#jq-validation-select2').select2({ allowClear: true, placeholder: 'Select a country...' }).change(function(){
							$(this).valid();
						});
						$('#jq-validation-select2-multi').select2({ placeholder: 'Select gear...' }).change(function(){
							$(this).valid();
						});

						// Add phone validator
						$.validator.addMethod(
							"phone_format",
							function(value, element) {
								var check = false;
								return this.optional(element) || /^\(\d{3}\)[ ]\d{3}\-\d{4}$/.test(value);
							},
							"Invalid phone number."
						);

						// Setup validation
						$("#jq-validation-form").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: false,
							rules: {
								'jq-validation-email': {
								  required: true,
								  email: true
								},
								'jq-validation-password': {
									required: true,
									minlength: 6,
									maxlength: 20
								},
								'jq-validation-password-confirmation': {
									required: true,
									minlength: 6,
									equalTo: "#jq-validation-password"
								},
								'jq-validation-required': {
									required: true
								},
								'jq-validation-url': {
									required: true,
									url: true
								},
								'jq-validation-phone': {
									required: true,
									phone_format: true
								},
								'jq-validation-select': {
									required: true
								},
								'jq-validation-multiselect': {
									required: true,
									minlength: 2
								},
								'jq-validation-select2': {
									required: true
								},
								'jq-validation-select2-multi': {
									required: true,
									minlength: 2
								},
								'jq-validation-text': {
									required: true
								},
								'jq-validation-simple-error': {
									required: true
								},
								'jq-validation-dark-error': {
									required: true
								},
								'jq-validation-radios': {
									required: true
								},
								'jq-validation-checkbox1': {
									require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
								},
								'jq-validation-checkbox2': {
									require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
								},
								'jq-validation-policy': {
									required: true
								}
							},
							messages: {
								'jq-validation-policy': 'You must check it!'
							}
						});
					});
				</script>
				<!-- / Javascript -->

<style>
    .alert_window {
        min-width: 600px;
        width: 50%;
        height: 150px;
        padding-top: 30px;
        z-index: 100;
        position: absolute;
        margin: 0 auto;
        top: 30%;
        left: 25%;
        background: #fff;
    }
    .shadow {
        width: 100%;
        height: 100%;
        z-index: 99;
        background: #9e9e9e;
        position: absolute;
        top: 0;
        left: 0;
    }
    .hide {
        display: none;
    }
</style>
<div data-bind="shadow" class="shadow hide"></div>
<div data-bind="shadow" class="alert_window hide">
    <div class="form-group">
    <div>
        <label class="col-sm-3 control-label">跳转地址</label>
    </div>
    <div class="col-sm-9">
        <input class="form-control" type="text" name="url_path" placeholder="请输入点击图片跳转的地址">
        格式 ： '/pages/detail/detail?id=' + 商品ID
    </div>
    </div>
    <div class="form-group">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
<!--        <button type="button" class="btn btn-primary"  data-id="continueUpload">上传图片</button>-->
<!--        <button type="button" style="margin-left:15px" class="btn btn-default" data-id="stop">取消</button>-->
    </div>
    </div>
</div>
<?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],]); ?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title">拓展设置</span>
    </div>
    <div class="panel-body">

        <div class="form-group" style="display: none">
            <label for="jq-validation-name" class="col-sm-3 control-label">欢迎语句</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="tel_welcome_words" name="welcome_words" value="<?=$model->welcome_words;?>" placeholder="首次登录欢迎语句">
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">腾讯地图秘钥</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="times" name="map_key" value="<?=$model->map_key;?>" placeholder="腾讯地图秘钥">
            </div>
        </div>

        <div class="form-group" style="display: none">
            <label for="jq-validation-name" class="col-sm-3 control-label">退货地址:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="send_position" name="send_position" value="<?=$model->send_position;?>" placeholder="退货收货地址">
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">默认城市:</label>
            <div class="col-sm-9">
            <select class="form-control" name="default_city">

                    <?php if (empty($seletCity)):?>
                        <option value="1" <?=(1==$model->default_city) ? "selected" : "" ?>>全国</option>
                    <?php else:?>

                        <?php if (1==$model->default_city):?>
                               <option value="1" selected >全国</option>
                                <?php  foreach ($seletCity as $k=>$v):?>
                                    <option value="<?=$v->provinces_id?>" <?=($v->provinces_id==$model->default_city) ? "selected" : "" ?>><?=$v->cityName?></option>
                                <?php endforeach;?>
                        <?php else:?>
                            <?php  foreach ($seletCity as $k=>$v):?>
                                <option value="<?=$v->provinces_id?>" <?=($v->provinces_id==$model->default_city) ? "selected" : "" ?>><?=$v->cityName?></option>
                            <?php endforeach;?>
                        <?php endif;?>

                    <?php endif;?>

            </select>
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">索票页了解渠道</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="resource" name="resource" value="<?= isset($model->resource) ? $model->resource : '';?>" placeholder="索票页了解渠道">
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">门票说明</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="tickets_notes" name="tickets_notes" value="<?= isset($model->tickets_notes) ? $model->tickets_notes : '';?>" placeholder="门票说明">
            </div>
        </div>
    </div>
</div>


<div class="panel">

    <div class="panel-heading">
        <span class="panel-title">页脚设置</span>
    </div>
    <div class="panel-body">
        <div class="panel" style="display: none">

            <div class="panel-heading">
                <span class="panel-title">导航链接</span>
            </div>
            <div class="form-group">
                <label for="jq-validation-name" class="col-sm-3 control-label">新手上路</label>
                <div class="col-sm-9">
                    <textarea type="text" class="form-control" id="tel_pc_xssl" name="pc_xssl" placeholder="请输入新手上路链接(以“|”分隔)"><?= $model->pc_xssl;?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="jq-validation-name" class="col-sm-3 control-label">购物指南</label>
                <div class="col-sm-9">
                    <textarea type="text" class="form-control" id="tel_pc_gwzn" name="pc_gwzn" placeholder="请输入购物指南链接(以“|”分隔)"><?= $model->pc_gwzn;?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="jq-validation-name" class="col-sm-3 control-label">服务保证</label>
                <div class="col-sm-9">
                    <textarea type="text" class="form-control" id="tel_pc_fwbz" name="pc_fwbz" placeholder="请输入服务保证链接(以“|”分隔)"><?= $model->pc_fwbz;?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="jq-validation-name" class="col-sm-3 control-label">会员中心</label>
                <div class="col-sm-9">
                    <textarea type="text" class="form-control" id="tel_pc_hyzx" name="pc_hyzx" placeholder="请输入会员中心链接(以“|”分隔)"><?= $model->pc_hyzx;?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="jq-validation-name" class="col-sm-3 control-label">联系我们</label>
                <div class="col-sm-9">
                    <textarea type="text" class="form-control" id="tel_pc_lxwm" name="pc_lxwm" placeholder="请输入联系我们链接(以“|”分隔)"><?= $model->pc_lxwm;?></textarea>
                </div>
            </div>
        </div>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->hostInfo;?>/public/css/jquery.tagsinput.css" />
        <script type="text/javascript" src="<?php echo Yii::$app->request->hostInfo;?>/public/js/jquery.tagsinput.min.js"></script>
        <div class="panel">

            <div class="panel-heading">
                <span class="panel-title">分类搜索</span>
            </div>

            <div class="form-group">
                <label for="jq-validation-name" class="col-sm-3 control-label">分类搜索</label>
                <div class="col-sm-9">
                    <?php
//                    $model->pc_ssdh = $model->pc_ssdh ? json_decode($model->pc_ssdh, true) : [];
                        $goodGlasses = \common\models\GoodsClass::find()->where(['=', 'fid', 0])->all();
                    ?>
                    <?php foreach ($goodGlasses AS $goodGlass) :?>
                        <?php
                        $sonGcs = $goodGlass->sonCategory;
                        $data = \yii\helpers\ArrayHelper::map($sonGcs, 'id', 'name');
                        ?>
                        <div  class="row">
                            <label><?=$goodGlass->name?></label>
                             <?php foreach ($data as $k=>$v):?>
                                <?php if (in_array($k,$seletSsdh)):?>
                                     <label><input id="config-pc_ssdh<?=$k?>" name="pc_ssdh[]" type="checkbox" checked value="<?=$k?>"/><?=$v?></label>
                                 <?php else:?>
                                     <label><input id="config-pc_ssdh<?=$k?>" name="pc_ssdh[]" type="checkbox"  value="<?=$k?>"/><?=$v?></label>
                                <?php endif;?>


                             <?php endforeach;?>
                        </div>
                    <?php endforeach;?>

                </div>
            </div>
        </div>



        <div class="panel" style="display: none;">

            <div class="panel-heading">
                <span class="panel-title">页脚链接</span>
            </div>

            <div class="form-group">
                <label for="jq-validation-name" class="col-sm-3 control-label">页脚底部</label>
                <div class="col-sm-9">
                    <textarea type="text" class="form-control" id="tel_welcome_words" name="pc_yjdb" placeholder="请输入页脚底部网页"><?=$model->pc_yjdb;?></textarea>
                </div>
            </div>
        </div>

    </div>
</div>



<div class="panel">
    <div class="panel-heading">
        <span class="panel-title">短信参数配置</span>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">Access Key ID</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="config-access_key_id" name="access_key_id" value="<?=$model->access_key_id;?>" placeholder="Access Key ID">
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">Access Key Secret</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="config-access_key_secret" name="access_key_secret" value="<?=$model->access_key_secret;?>" placeholder="Access Key Secret">
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">签名CODE</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="config-sms_code" name="sms_code" value="<?=$model->sms_code;?>" placeholder="签名CODE">
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary">保存</button>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
<div data-id="build" style="display:none;"></div>
<script src="/public/js/jquery.min.js"></script>
<script src="/public/js/base_upload.js"></script>
<script src="/public/js/upload-hooks/preview.js"></script>
<script src="/public/js/jquery.tagsinput.min.js"></script>
<script>
    var uploadPath = '/upload/img?more=true';
    $(function() {
        // config = [
        //     {category: 'config', id: 'logo', type: 'logo'},
        // ];
        // previewCtrl(config);



        $('#resource').tagsInput({width:'auto',defaultText:'添加渠道'});

    });
</script>
