<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\Request;
/* @var $this yii\web\View */
/* @var $model app\Models\Config */

$this->title = "小程序基础设置"

?>

<!-- 5. $JQUERY_VALIDATION =========================================================================

				jQuery Validation
-->
				<!-- Javascript -->
				<script>
					init.push(function () {
						$("#jq-validation-phone").mask("(999) 999-9999");
						$('#jq-validation-select2').select2({ allowClear: true, placeholder: 'Select a country...' }).change(function(){
							$(this).valid();
						});
						$('#jq-validation-select2-multi').select2({ placeholder: 'Select gear...' }).change(function(){
							$(this).valid();
						});

						// Add phone validator
						$.validator.addMethod(
							"phone_format",
							function(value, element) {
								var check = false;
								return this.optional(element) || /^\(\d{3}\)[ ]\d{3}\-\d{4}$/.test(value);
							},
							"Invalid phone number."
						);

						// Setup validation
						$("#jq-validation-form").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: false,
							rules: {
								'jq-validation-email': {
								  required: true,
								  email: true
								},
								'jq-validation-password': {
									required: true,
									minlength: 6,
									maxlength: 20
								},
								'jq-validation-password-confirmation': {
									required: true,
									minlength: 6,
									equalTo: "#jq-validation-password"
								},
								'jq-validation-required': {
									required: true
								},
								'jq-validation-url': {
									required: true,
									url: true
								},
								'jq-validation-phone': {
									required: true,
									phone_format: true
								},
								'jq-validation-select': {
									required: true
								},
								'jq-validation-multiselect': {
									required: true,
									minlength: 2
								},
								'jq-validation-select2': {
									required: true
								},
								'jq-validation-select2-multi': {
									required: true,
									minlength: 2
								},
								'jq-validation-text': {
									required: true
								},
								'jq-validation-simple-error': {
									required: true
								},
								'jq-validation-dark-error': {
									required: true
								},
								'jq-validation-radios': {
									required: true
								},
								'jq-validation-checkbox1': {
									require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
								},
								'jq-validation-checkbox2': {
									require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
								},
								'jq-validation-policy': {
									required: true
								}
							},
							messages: {
								'jq-validation-policy': 'You must check it!'
							}
						});
					});
				</script>
				<!-- / Javascript -->

<style>
    .alert_window {
        min-width: 600px;
        width: 50%;
        height: 150px;
        padding-top: 30px;
        z-index: 100;
        position: absolute;
        margin: 0 auto;
        top: 30%;
        left: 25%;
        background: #fff;
    }
    .shadow {
        width: 100%;
        height: 100%;
        z-index: 99;
        background: #9e9e9e;
        position: absolute;
        top: 0;
        left: 0;
    }
    .hide {
        display: none;
    }
</style>
<div data-bind="shadow" class="shadow hide"></div>
<div data-bind="shadow" class="alert_window hide">
    <div class="form-group">
    <div>
        <label class="col-sm-3 control-label">跳转地址</label>
    </div>
    <div class="col-sm-9">
        <input class="form-control" type="text" name="url_path" placeholder="请输入点击图片跳转的地址">
        格式 ： '/pages/detail/detail?id=' + 商品ID
    </div>
    </div>
    <div class="form-group">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
<!--        <button type="button" class="btn btn-primary"  data-id="continueUpload">上传图片</button>-->
<!--        <button type="button" style="margin-left:15px" class="btn btn-default" data-id="stop">取消</button>-->
    </div>
    </div>
</div>
<?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],]); ?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title">积分设置</span>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">标题</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="title" name="title" value="<?=$model->title;?>" placeholder="标题">
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-wxid" class="col-sm-3 control-label">签到说明:</label>
            <div class="col-sm-9">
                <textarea id="explain" class="form-control"  name="explain"><?=$model->explain;?></textarea>

            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-wxnumber" class="col-sm-3 control-label">规则说明:</label>
            <div class="col-sm-9">
                <textarea id="rule" class="form-control"  name="rule"><?=$model->rule;?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-appid" class="col-sm-3 control-label">签到获得积分:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="sign_integral" name="sign_integral" value="<?=$model->sign_integral;?>" type="number" placeholder="签到获得积分">
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-AppSecret" class="col-sm-3 control-label">续签到的天数:</label>
            <div class="col-sm-9">
                <input class="form-control" name="continuation" id="continuation" value="<?=$model->continuation;?>" type="number" placeholder="续签到的天数"/>
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-token" class="col-sm-3 control-label">续签到奖励积分</label>
            <div class="col-sm-9">
                <input class="form-control" name="reward" id="reward" value="<?=$model->reward;?>" type="number" placeholder="续签到奖励积分"/>
            </div>
        </div>

        <div class="form-group" style="display:none;">
            <label for="jq-validation-name" class="col-sm-3 control-label">充值积分</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="config-logo" name="charge_integral" value="<?=$model->charge_integral;?>" placeholder="小程序LOGO">
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">购物返积分方式</label>
            <div class="col-sm-9">
                <?php
                    strlen($model->buy_goods_type) or $model->buy_goods_type = 1;

                    foreach ($buyGoodsTypeDic AS $value => $mean) {
                        echo sprintf(
                                '<span style="margin-right: 10px;"><input type="radio" id="config-logo" name="buy_goods_type" value="%s" '. ($model->buy_goods_type == $value ? ' checked ' : '') . '/>%s</span>',
                            $value,
                            $mean
                        );
                    }
                ?>
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">购物返积分</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="config-logo" name="buy_goods_integral" value="<?=$model->buy_goods_integral;?>" placeholder="请输入购物获得的积分额或消费金额的百分比数值">
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary">保存</button>
            </div>
        </div>

    </div>
</div>
<?php ActiveForm::end(); ?>

