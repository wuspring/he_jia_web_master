<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cart */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cart-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'buyerId')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goodsId')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goodsName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goodsPrice')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goodsNum')->textInput() ?>

    <?= $form->field($model, 'attr')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sku_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goods_image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ispay')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
