<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Postmethod */

$this->title = '添加运费模板';
$this->params['breadcrumbs'][] = ['label' => 'Postmethods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'provinces'=>$provinces
    ]) ?>

</div>
