<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Postmethod */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-body form-controls-demo">

    <?php
    $model->send_area or $model->send_area = [];
    $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
        'fieldConfig' => [
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions'=>['class'=>'col-sm-2 control-label'],
            'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?php // $form->field($model, 'expressid')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'code')->hiddenInput(['maxlength' => true])->label("") ?>

    <?= $form->field($model, 'type')->radioList(['1'=>'按重收费','2'=>'按件收费'])->label('计费类型') ?>

    <?= $form->field($model, 'first_condition')->textInput(['type'=>'number']) ?>

    <?= $form->field($model, 'first_money')->textInput(['type'=>'number']) ?>

    <?= $form->field($model, 'other_condition')->textInput(['type'=>'number']) ?>

    <?= $form->field($model, 'other_money')->textInput(['type'=>'number']) ?>

    <?= $form->field($model, 'status')->radioList(['1'=>'启用','0'=>'禁用']) ?>

    <?= $form->field($model, 'is_default')->radioList(['1'=>'是','0'=>'否']) ?>
    <div class="form-group field-postmethod-is_default">
        <label class="col-sm-8 control-label" for="postmethod-is_default" style="color: #ef0e06;">说明:当选择包邮时上面的运费针对的是选择的不包邮区域进行计算运费,当选择不包邮时,运费计算是针对未选择的不包邮区域进行计算</label>
    </div>
    <div class="form-group field-postmethod-send_area">
        <label class="col-sm-2 control-label" id="xianshi" for="postmethod-send_area">包邮区域</label>
        <div class="col-sm-10">
        <ul>
            <?php if(!empty($provinces)): ?>
                <?php foreach ($provinces as $key=>$val): ?>
                <li>
                    <input type="checkbox" onclick="selectCity(this)" name="Postmethod[send_area][]" value="<?php echo $val['id'];?>" <?php if(in_array($val['id'], $model->send_area)): ?>checked<?php endif;?>><?php echo $val['cname'];?> <a href="javascript:;" onclick="showCity(this)">+</a>
                    <ul class="city_div" style="display: none">
                        <?php foreach ($val['child'] as $k=>$v): ?>
                        <li>
                            <input type="checkbox" name="Postmethod[send_area][]" value="<?php echo $v['id'];?>" <?php if(in_array($v['id'],$model->send_area)): ?>checked<?php endif;?> /><?php echo $v['cname'];?>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </li>

                <?php endforeach; ?>
            <?php endif;?>
        </ul>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '创建') : Yii::t('app', '保存修改'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    .field-postmethod-send_area li{list-style: none;display: inline-block;width: 32%;}
</style>

<script>
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    init.push(function () {
        $('.field-postmethod-type input[type=radio]').change(function() {
            showTypeDesc();
        });
        showTypeDesc();
        function showTypeDesc()
        {
            var type = $('.field-postmethod-type input[type=radio]:checked').val();
            if (type == 1) {
                $(".field-postmethod-first_condition .control-label").html("首重(kg)");
                $(".field-postmethod-first_money .control-label").html("首费(元)");
                $(".field-postmethod-other_condition .control-label").html("续重(kg)");
                $(".field-postmethod-other_money .control-label").html("续费(元)");
            }
            else if (type == 2) {
                $(".field-postmethod-first_condition .control-label").html("首件");
                $(".field-postmethod-first_money .control-label").html("首费(元)");
                $(".field-postmethod-other_condition .control-label").html("续件");
                $(".field-postmethod-other_money .control-label").html("续费(元)");
            }
        }

            $("#postmethod-expressid").select2({
                placeholder:"输入物流公司名称",//文本框的提示信息
                minimumInputLength:1,   //至少输入n个字符，才去加载数据
                allowClear: true,  //是否允许用户清除文本信息
                ajax:{
                    url:'<?php echo Url::toRoute(['express/list'])?>',   //地址
                    dataType:'text',    //接收的数据类型
                    //contentType:'application/json',
                    data: function (term, pageNo) {     //在查询时向服务器端传输的数据
                        term = $.trim(term);
                        return {
                            _csrf:_csrf ,
                            val: term,    //联动查询的字符
                            //pageSize: 15,    //一次性加载的数据条数
                            //pageNo:pageNo,    //页码
                            //time:new Date()//测试
                        }
                    },
                    results:function(data,pageNo){

                        if(data.length>0){   //如果没有查询到数据，将会返回空串
                            var dataObj =eval("("+data+")");  //将接收到的JSON格式的字符串转换成JSON数据
                            var more = (pageNo*15)<dataObj.total; //用来判断是否还有更多数据可以加载
                            return {
                                results:dataObj.result,more:more
                            };
                        }else{
                            return {results:data};
                        }
                    }
                },
                initSelection:function(element,callback){           //初始化，其中doName是自定义的一个属性，用来存放text的值
                    var id=$(element).val();

                    var name="<?php echo  $model->isNewRecord ? '请选择物流公司':$model->express->name;?>"

                    //var text=$(element).attr("doName");
                    if(id!=''&&name!=""){
                        callback({id:id,name:name});
                    }

                },
                formatResult: formatAsText, //渲染查询结果项
                formatSelection: movieFormatSelection,  // omitted for brevity, see the source of this page
                dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
                escapeMarkup: function (m) { return m; }
            });

        })


    //格式化查询结果,将查询回来的id跟name放在两个div里并同行显示，后一个div靠右浮动
    function formatAsText(item){
        console.log(item)
        var itemFmt = "<div style='display:inline;'>" + item.name + "</div>";//<div style='float:right;color:#4F4F4F;display:inline'>"+item.name+"</div>
        return itemFmt;
    }
    function movieFormatSelection(movie) {
        return movie.name;
    }
    function showCity(obj) {
        var city_div =  $(obj).parent().find('.city_div');
        city_div.toggle();
    }
    function selectCity(obj) {
        if($(obj).prop('checked')){
            $(obj).prop('checked',true);
            $(obj).parent().find('input[name="Postmethod[send_area][]"]').prop('checked',true);
        }else{
            $(obj).prop('checked',false);
            $(obj).parent().find('input[name="Postmethod[send_area][]"]').prop('checked',false);
        }
    }
</script>