<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PostmethodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '运费设置';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">
        <?= Html::a('添加运费模板', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('快递列表', \yii\helpers\Url::toRoute('express/index'), ['class' => 'btn btn-default']) ?>
    </div>


    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
//            'code',
            'type',
            'first_condition',
             'first_money',
             'other_condition',
             'other_money',
             'status',
            // 'createTime',
            // 'send_area:ntext',
            // 'not_area:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
