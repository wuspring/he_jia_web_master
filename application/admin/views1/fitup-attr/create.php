<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FitupAttr */

$this->title = '添加属性';
$this->params['breadcrumbs'][] = ['label' => 'Fitup Attrs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'parent'=>$parent,
    ]) ?>

</div>
