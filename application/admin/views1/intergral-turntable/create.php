<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\IntergralTurntable */

$this->title = 'Create Intergral Turntable';
$this->params['breadcrumbs'][] = ['label' => 'Intergral Turntables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intergral-turntable-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
