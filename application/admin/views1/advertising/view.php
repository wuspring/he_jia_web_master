<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Advertising */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Advertisings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advertising-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('继续添加', ['create', 'adid' => $model->adid], ['class' => 'btn btn-primary']) ?>
       
        <?= Html::a('修改', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('删除', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
         <?= Html::a('返回列表', ['index'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'adid',
            'title',
            'pictureTm',
            [
                'attribute'=>'pictureTm',
                'value'=>$model->pictureTm,
                'format' => ['image',['width'=>'50','height'=>'50']],
            ],
            // 'pictureTm',
            'url:url',
            'isShow',
            'clickRate',
            'sort',
            'createTime',
            'modifyTime',
        ],
    ]) ?>

</div>
