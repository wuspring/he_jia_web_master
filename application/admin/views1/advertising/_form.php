<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Advertising */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/js/uploadPreview.js');

$classes = \common\models\GoodsClass::find()->where([
        'fid' => 0,
        'is_del' => 0
])->all();
$classesArray = \yii\helpers\ArrayHelper::map($classes, 'id', 'name');
$cid = $model->adid ? (int)$model->adid : Yii::$app->request->get('cid', 0);
?>
<script language="javascript" type="text/javascript" src="<?php echo yii::$app->request->hostInfo;?>/public/My97DatePicker/WdatePicker.js"></script>
<div class="panel-body form-controls-demo">

    <?php $form = ActiveForm::begin([
     'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
        'fieldConfig' => [
        'inputOptions' => ['class' => 'form-control'],
        'labelOptions'=>['class'=>'col-sm-2 control-label'],
        'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'picture')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => false,
            ],
            'server' => Url::toRoute(['advertising/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ]); ?>
    <?= $form->field($model, 'adid')->dropDownList($adsense) ?>
    <?= $form->field($model, 'province_id')->dropDownList(\common\models\SelectCity::getAllCityName(),['prompt'=>'请选择']) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'describe')->textarea(['rows' => 3]) ?>
    <?= $form->field($model, 'isShow')->radioList(['1'=>'显示','0'=>'不显示']) ?>
    <?php //echo $form->field($model, 'isTime')->radioList(['1'=>'开启','0'=>'关闭']) ?>
    <?php // $form->field($model, 'startTime')->textInput() ?>
    <?php //$form->field($model, 'endTime')->textInput() ?>
    <?php // $form->field($model, 'clickRate')->textInput() ?>
    <?php echo $form->field($model, 'sort')->textInput() ?>

   <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '添加') : Yii::t('app', '保存修改'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    init.push(function () {
        $("#advertising-starttime").click(function(){
            WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'advertising-endtime\')}'})
        });
        $("#advertising-endtime").click(function(){
            WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'advertising-starttime\')}'})
        });
    });
</script>