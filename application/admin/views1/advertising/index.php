<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\search\AdvertisingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '广告列表';
$this->params['breadcrumbs'][] = $this->title;
global $cid;
$cid = Yii::$app->request->get('id', 0);
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <div class="panel-body" >
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="panel-body" >
        <?= Html::a('添加广告', Url::toRoute(['advertising/create', 'adid' => $cid]), ['class' => 'btn btn-success']) ?>
    </div>


    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            array(
                'format' => 'raw',
                'attribute' => 'pictureTm',
                'label'=>'主题缩略图',
                'value'=>function($filterModel){
                    return Html::img(yii::$app->request->hostInfo.$filterModel->pictureTm, ['width' =>30]);
                },
            ),
            // 'adid',
            'title',
            array(
                'format' => 'raw',
                'attribute'=>'adid',
                'label'=>'广告位',
                'value'=>function($filterModel){
                    return empty($filterModel->adsense->name)?"-":$filterModel->adsense->name;
                },
            ),
            // 'picture',
            // 'pictureTm',
            // 'url:url',
            // 'isShow',
            //  'clickRate',
            'sort',
            'createTime',
//            array(
//                'attribute'=>'adid',
//
//                ),
            // 'picture',
            // 'pictureTm',

            // 'url:url',
             // 'isShow',

           //  'clickRate',
//             'sort',
//             'createTime',
            // 'modifyTime',
//            array(
//                'attribute' => 'isShow',
//                'label'=>'是/否显示',
//                'format' => 'raw',
//                'value'=>function($filterModel){
//                    $url="javascript:void(0)";
//                    if ($filterModel->isShow == 0) {
//                        return  Html::a('<span class="glyphicon glyphicon-eye-close"></span>', $url, ['title' => '是/否显示','data-toggle'=>$filterModel->id,'data-status'=>$filterModel->isShow,'id'=>'data-isShow'] ) ;
//                    }else{
//                        return  Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => '是/否显示','data-toggle'=>$filterModel->id,'data-status'=>$filterModel->isShow,'id'=>'data-isShow'] ) ;
//                    }
//
//                },
//            ),
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'操作',
                'template' => '{update} {delete}',
                'buttons'=>[
                    'view'=>function($url, $model, $key){
                        return  Html::a('查看', $url, ['title' => '查看','data-toggle'=>'modal','class'=>'a_margin'] ) ;
                    },
                    'update' => function ($url, $model, $key) {
                        return  Html::a('更新', $url, ['title' => '更新','data-toggle'=>'modal','class'=>'a_margin'] ) ;
                    },
                    'delete'=> function ($url, $model, $key) {
                        return  Html::a('删除', $url, ['title' => '删除','class'=>'a_margin','data' => [
                            'confirm' => '确认删除，删除后不可恢复?',
                            'method' => 'post',
                        ]]) ;
                    },
                ],
            ],
        ],
    ]); ?>
    </div>
</div>
<script type="text/javascript">
init.push(function () {
    var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>";

    $("#data-isShow").on('click', function () {
        var th = $(this);
        var id = $(this).attr('data-toggle');
        var status = $(this).attr('data-status');
        if (status == 0) {
            status = 1;
        } else {
            status = 0;
        }
        $.post("<?php echo Url::toRoute(['/advertising/isshow']);?>", {
            _csrf: _csrf,
            id: id,
            status: status
        }, function (data) {
            if (data == 0) {
                $(th).find('span').attr('class', 'glyphicon glyphicon-eye-close');
                $(th).attr('data-status', 0);
            } else {
                $(th).find('span').attr('class', 'glyphicon glyphicon-eye-open');
                $(th).attr('data-status', 1);
            }
        });
    });
});
</script>