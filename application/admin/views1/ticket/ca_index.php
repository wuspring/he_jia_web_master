<?php

use yii\helpers\Html;
use yii\grid\GridView;
use DL\service\UrlService;
use yii\widgets\LinkPager;
use common\models\Ticket;

/* @var $this yii\web\View */
/* @var $searchModel admin\search\TicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '采购设置';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <ul class="nav nav-tabs dl_tab">
        <li class="col-lg-2 tab_title <?=($active=='JIE_HUN')?'active':''?>"><a  href="<?=UrlService::build(['ticket/ca-index','tab'=>'JIE_HUN']);?>">婚庆采购</a></li>
        <li class="col-lg-2 tab_title <?=($active=='JIA_ZHUANG')?'active':''?>"><a  href="<?=UrlService::build(['ticket/ca-index','tab'=>'JIA_ZHUANG']);?>">家装采购</a></li>
        <li class="col-lg-2 tab_title <?=($active=='YUN_YING')?'active':''?>"><a  href="<?=UrlService::build(['ticket/ca-index','tab'=>'YUN_YING']);?>">孕婴采购</a></li>
    </ul>

    <div class="panel-body">
        <?php
        $ticketModel = new Ticket();
        if (in_array($active, array_keys($ticketModel->getTypeDic()))) {
            $href = UrlService::build(['page/index', 'type' => 'PAGE_CAI_GOU_' . str_replace('_', '', $active)]);
            echo Html::a('页面设置', $href, ['class' => 'btn btn-primary']);
        }

        ?>
        <?php Html::a('添加门票', ['create', 'ca_type' => Ticket::CA_TYPE_CAI_GOU], ['class' => 'btn btn-success']) ?>
    </div>

    <div class="panel-body">
        <div class="panel">
            <?php if ($list) :?>
               <?php foreach ($list  as $k => $v):?>
              <div class="panel-heading">
                   <span class="panel-title">
                       <?= $v->name; ?>
                       <span style="float: right">
                           <a class="btn btn-default" href="<?= UrlService::build(['ticket/update', 'id'=>$v->id]); ?>">编辑</a>
                       </span>
                   </span>
               </div>
              <div class="panel-body">
                  <table class="table table-striped table-bordered detail-view">
                      <tbody>
                          <tr><th class="col-lg-3">类型</th><td><?= $v->typeInfo; ?></td></tr>
                          <tr><th>状态</th><td><?= $v->statusInfo; ?></td></tr>
                          <tr><th>预约总量</th><td><?= $v->amount; ?></td></tr>
                          <tr><th>地址</th><td><?= $v->address; ?></td></tr>
                          <tr><th>添加时间</th><td><?= $v->create_time; ?></td></tr>
                      </tbody>
                  </table>
                  <table class="table">
                      <thead>
                            投放详情
                      </thead>
                      <tbody>
                      <tr>
                          <td class="col-md-3">投放城市</td>
                          <td class="col-md-3">城市编号</td>
                          <td class="col-md-3">预约数量</td>
                      </tr>
                      <?php foreach ($v->ticketInfor as $tk=>$tv):?>
                          <tr>
                              <td class="col-md-3"><?=$tv->cityName?></td>
                              <td class="col-md-3"><?=$tv->provinces_id?></td>
                              <td class="col-md-3"><?=$tv->ticket_amount?></td>
                          </tr>
                      <?php endforeach;?>
                      </tbody>
                  </table>

              </div>
               <?php endforeach;?>
               <?php else :?>
                <div class="col-lg-12" style="text-align: center;height: 60px;line-height: 60px;">
                    暂无发行票据
                </div>
               <?php endif; ?>
        </div>

        <?= LinkPager::widget(['pagination' => $pagination,]) ?>
    </div>

</div>
