<?php

use yii\helpers\Html;
use yii\grid\GridView;
use DL\service\UrlService;
use \common\models\Ticket;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MoneySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '展会列表';
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
    .joinCoupon{ margin-left: 5px}
</style>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>


    <ul class="nav nav-tabs dl_tab">
        <li class="col-lg-2 tab_title <?=($active=='JIA_ZHUANG')?'active':''?>"><a  href="<?=UrlService::build(['ticket/list','tab'=>'JIA_ZHUANG']);?>">家装展</a></li>
        <li class="col-lg-2 tab_title <?=($active=='JIE_HUN')?'active':''?>"><a  href="<?=UrlService::build(['ticket/list','tab'=>'JIE_HUN']);?>">婚庆展</a></li>
        <li class="col-lg-2 tab_title <?=($active=='YUN_YING')?'active':''?>"><a  href="<?=UrlService::build(['ticket/list','tab'=>'YUN_YING']);?>">孕婴展</a></li>
    </ul>

    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'id',
            array('attribute'=>'name',
                'label'=> '展会名称',
                'value' =>function ($filterModel){
                    return $filterModel->name;
                }),
            array(
                    'attribute'=>'open_date',
                    'label'=> '举办日期'
                ),
            array(
                'attribute'=>'end_date',
                'label'=> '结束日期'
            ),
            array('attribute'=>'name',
                'label'=> '预定数量',
                'value' =>function ($filterModel){
                    return $filterModel->ticketInfo->ticket_amount;
                }),
            array(
                    'attribute'=>'order_price',
                    'label'=> '预定金额'
                ),
            array(
                    'attribute'=>'status',
                    'label'=> '展会状态',
                    'value' => function ($model) {
                        return $model->statusInfo;
                    }
                ),

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'操作',
                'template' => '{view} {join} {joincoupon} {ticketcoupon}',
                'buttons' => [
                        'view' => function ($url, $model, $key) {
                            if ($model->status) {
                                return Html::a('查看', ['ticket/info', 'id' => $model->id], ['title' => '查看', 'class'=>"btn btn-info btn-xs "]);
                            } else {
                                return Html::a('已结束', ['ticket/info', 'id' => $model->id], ['title' => '查看', 'class'=>"btn btn-info btn-xs "]);
                            }
                        },
                        'join' => function ($url, $model, $key) {
                            return Html::a('定金订单兑换记录', ['coupon-verify/record', 'id' => $model->id], ['title' => '查看', 'class'=>"btn btn-info btn-xs "]);
                        },
                       'joincoupon' => function ($url, $model, $key) {
                        return Html::a('参展优惠劵', ['ticket/join-coupon', 'ticketId' => $model->id], ['title' => '参展优惠劵', 'class'=>"btn btn-info btn-xs joinCoupon"]);
                      },
                    'ticketcoupon' => function ($url, $model, $key) {
                        return Html::a('店铺展会优惠劵', ['ticket-store-coupon/index-store', 'ticketId' => $model->id], ['title' => '店铺展会通用优惠券', 'class'=>"btn btn-info btn-xs joinCoupon"]);
                    }
                ]
            ]
        ],
    ]); ?>
    </div>
</div>
<script src="/public/js/jquery-1.8.3.min.js"></script>
<script>
    $(function () {
        $('#w0 .form-group').addClass('col-sm-2').last().css('margin-top', '25px');
        $('#w0 .form-group').removeClass('form-group');
        $('#w0').addClass('form-group');
    })
</script>