<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model common\models\Jingxiaoshang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jingxiaoshang-form">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'member_id',
                'label' => '申请人',
                'value' => $model->member->wxnick
            ],
            [
                'attribute' => 'name',
                'label' => '申请人',
            ],
            [
                'attribute' => 'phone',
                'label' => '联系方式',
            ],
            [
                'attribute' => 'create_time',
                'label' => '申请时间',
            ]
        ],
    ]) ?>

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'status')->dropDownList([
        \common\models\Jingxiaoshang::STATUS_APPLY => '同意',
        \common\models\Jingxiaoshang::STATUS_REFUSE => '拒绝',
    ])->label('审核') ?>

    <div class="form-group">
        <?= Html::submitButton('保存', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
