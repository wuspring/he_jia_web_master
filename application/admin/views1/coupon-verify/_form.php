<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile(Yii::$app->request->hostInfo . '/public/js/uploadPreview.js');
$this->registerJsFile(Yii::$app->request->hostInfo . '/public/ueditor/ueditor.config.js');
$this->registerJsFile(Yii::$app->request->hostInfo . '/public/ueditor/ueditor.all.min.js');
$this->registerJsFile(Yii::$app->request->hostInfo . '/public/ueditor/lang/zh-cn/zh-cn.js');
?>
<style>
    .inline .radio, .inline .checkbox {
        display: inline-block;
        margin: 0 5px;
    }

    #news-content {
        margin-top: 20px;
        padding: 0;
        margin: 20px 0;
        width: 100%;
        height: auto;
        border: none;
    }

</style>

<div class="panel-body form-controls-demo">
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions' => ['class' => 'col-sm-2 control-label'],
            'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <div class="form-group field-news-cid has-success">
        <label class="control-label col-sm-2" for="news-cid">分类</label>
        <div class="col-sm-10">
            <?php echo $parent; ?>
            <div class="help-block"></div>
        </div>
    </div>

    <?= $form->field($model, 'themeImg')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => false,
            ],
            'server' => Url::toRoute(['site/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ]); ?>

    <?= $form->field($model, 'brief')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seokey')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'seoDepict')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'isShow')->radioList(['0' => '不显示', '1' => '显示']) ?>

    <?= $form->field($model, 'isRmd')->radioList(['0' => '不推荐', '1' => '推荐']) ?>

    <?= $form->field($model, 'clickRate')->textInput() ?>

    <?= $form->field($model, 'writer')->textInput(['maxlength' => 255]) ?>


    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '发布') : Yii::t('app', '保存修改'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    init.push(function () {
        new uploadPreview({UpBtn: "news-themeimg", DivShow: "imgdiv", ImgShow: "imgShow"});
        var ue = UE.getEditor('news-content', {
            'zIndex': 9,
        });
    });
</script>