-- -------------------------------------------
START TRANSACTION;
SET SQL_QUOTE_SHOW_CREATE = 1;
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
-- -------------------------------------------
-- -------------------------------------------
-- START BACKUP
-- -------------------------------------------
-- -------------------------------------------
-- TABLE `account_log`
-- -------------------------------------------
DROP TABLE IF EXISTS `account_log`;
CREATE TABLE IF NOT EXISTS `account_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL COMMENT '类型 money消费记录',
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '变动金额',
  `integral` int(10) DEFAULT '0' COMMENT '积分',
  `remark` varchar(250) DEFAULT NULL COMMENT '说明',
  `member_id` int(11) DEFAULT NULL COMMENT '会员ID',
  `create_time` datetime DEFAULT NULL COMMENT '变动时间',
  `sn` varchar(255) DEFAULT NULL COMMENT '变动记录号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='账户余额与积分记录';

-- -------------------------------------------
-- TABLE `address`
-- -------------------------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT '0' COMMENT '会员id',
  `name` varchar(255) DEFAULT NULL COMMENT '收货人姓名',
  `province` varchar(255) DEFAULT NULL COMMENT '省',
  `city` varchar(255) DEFAULT NULL COMMENT '市',
  `region` varchar(255) DEFAULT '0' COMMENT '地区',
  `address` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `tel` varchar(255) DEFAULT NULL COMMENT '座机',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机号',
  `isDefault` int(11) DEFAULT '0' COMMENT '是否默认地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `adsense`
-- -------------------------------------------
DROP TABLE IF EXISTS `adsense`;
CREATE TABLE IF NOT EXISTS `adsense` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '广告位名称',
  `state` int(11) DEFAULT '1' COMMENT '状态:1有效 0无效',
  `createTime` datetime DEFAULT NULL,
  `modifyTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `advertising`
-- -------------------------------------------
DROP TABLE IF EXISTS `advertising`;
CREATE TABLE IF NOT EXISTS `advertising` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adid` bigint(20) DEFAULT NULL COMMENT '广告位id',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `picture` varchar(255) DEFAULT NULL COMMENT '图片',
  `pictureTm` varchar(255) DEFAULT NULL COMMENT '缩略图',
  `url` varchar(255) DEFAULT NULL COMMENT '跳转地址',
  `isShow` int(11) DEFAULT '1' COMMENT '是否显示1显示0为不显示',
  `describe` varchar(255) DEFAULT NULL COMMENT '描述',
  `clickRate` int(11) DEFAULT '0' COMMENT '点击量',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `isTime` int(11) DEFAULT '0' COMMENT '是否是时间广告',
  `startTime` datetime DEFAULT NULL COMMENT '开始时间',
  `endTime` datetime DEFAULT NULL COMMENT '结束时间',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `modifyTime` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `auth_assignment`
-- -------------------------------------------
DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `created_at` (`created_at`) USING BTREE,
  KEY `item_name` (`item_name`) USING BTREE,
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员授权表';

-- -------------------------------------------
-- TABLE `auth_item`
-- -------------------------------------------
DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE,
  KEY `rule_name` (`rule_name`) USING BTREE,
  KEY `type` (`type`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `created_at` (`created_at`) USING BTREE,
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理权权限条目';

-- -------------------------------------------
-- TABLE `auth_item_child`
-- -------------------------------------------
DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`) USING BTREE,
  KEY `child` (`child`) USING BTREE,
  KEY `parent` (`parent`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员权限关系表';

-- -------------------------------------------
-- TABLE `auth_rule`
-- -------------------------------------------
DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `created_at` (`created_at`) USING BTREE,
  KEY `updated_at` (`updated_at`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员权限规则表';

-- -------------------------------------------
-- TABLE `brand`
-- -------------------------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE IF NOT EXISTS `brand` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '索引ID',
  `brand_name` varchar(100) DEFAULT NULL COMMENT '品牌名称',
  `brand_initial` varchar(1) DEFAULT NULL COMMENT '品牌首字母',
  `brand_pic` varchar(100) DEFAULT NULL COMMENT '图片',
  `brand_sort` tinyint(3) unsigned DEFAULT '0' COMMENT '排序',
  `brand_recommend` tinyint(1) DEFAULT '0' COMMENT '推荐，0为否，1为是，默认为0',
  `class_id` int(10) unsigned DEFAULT '0' COMMENT '所属分类id',
  `show_type` tinyint(1) DEFAULT '0' COMMENT '品牌展示类型 0表示图片 1表示文字 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='品牌表';

-- -------------------------------------------
-- TABLE `cart`
-- -------------------------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `buyerId` bigint(20) DEFAULT '0' COMMENT '买家id',
  `goodsId` bigint(20) DEFAULT '0' COMMENT '商品id',
  `goodsName` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `goodsPrice` decimal(10,2) DEFAULT '0.00' COMMENT '商品价格',
  `goodsIntegral` bigint(20) unsigned DEFAULT NULL COMMENT '商品积分',
  `goodsNum` int(11) DEFAULT '1' COMMENT '购买商品数量',
  `attr` text COMMENT '规格',
  `attrInfo` text COMMENT '规格详情',
  `sku_id` bigint(20) DEFAULT NULL COMMENT 'sku编号',
  `goods_image` varchar(255) DEFAULT NULL COMMENT '商品图片',
  `integral` decimal(12,2) unsigned DEFAULT '0.00' COMMENT '商品积分',
  `ispay` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='购物车';

-- -------------------------------------------
-- TABLE `category`
-- -------------------------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(12) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` char(255) NOT NULL COMMENT '栏目标题',
  `keyword` varchar(255) DEFAULT NULL COMMENT '关键字',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `parent_id` int(12) unsigned DEFAULT NULL COMMENT '上级目录',
  `is_index` tinyint(5) DEFAULT '0',
  `icon` varchar(255) DEFAULT NULL,
  `rank` int(2) unsigned NOT NULL DEFAULT '0' COMMENT '栏目权重',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `rank` (`rank`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='栏目分类表';

-- -------------------------------------------
-- TABLE `collect`
-- -------------------------------------------
DROP TABLE IF EXISTS `collect`;
CREATE TABLE IF NOT EXISTS `collect` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_id` int(11) DEFAULT NULL COMMENT '会员ID',
  `goods_id` int(11) DEFAULT NULL COMMENT '商品ID',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='收藏表';

-- -------------------------------------------
-- TABLE `comment`
-- -------------------------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT NULL,
  `goods_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL COMMENT 'good 好评 mid  中评 low 差评',
  `content` text COMMENT '内容',
  `imgs` text COMMENT '上传图片',
  `member_id` int(11) DEFAULT '0' COMMENT '会员ID',
  `create_time` datetime DEFAULT NULL COMMENT '评论时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='评论表';

-- -------------------------------------------
-- TABLE `commission_log`
-- -------------------------------------------
DROP TABLE IF EXISTS `commission_log`;
CREATE TABLE IF NOT EXISTS `commission_log` (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `memberId` int(11) DEFAULT '0' COMMENT '会员',
  `orderId` bigint(20) DEFAULT '0',
  `amount` decimal(10,2) DEFAULT '0.00' COMMENT '订单金额',
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '分成金额',
  `status` int(1) DEFAULT '0' COMMENT '领取状态 0表示未领取 1表示已分配',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分销商分成表';

-- -------------------------------------------
-- TABLE `config`
-- -------------------------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `cKey` varchar(255) DEFAULT NULL COMMENT '键值',
  `cValue` text COMMENT '值',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `modifyTime` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `coupon`
-- -------------------------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE IF NOT EXISTS `coupon` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `store_id` bigint(11) DEFAULT '0' COMMENT '店铺编号',
  `name` varchar(255) NOT NULL COMMENT '优惠券名称',
  `desc` varchar(2000) NOT NULL DEFAULT '' COMMENT '优惠券介绍',
  `pic_url` varchar(2048) DEFAULT NULL COMMENT '图片',
  `discount_type` smallint(6) NOT NULL DEFAULT '1' COMMENT '优惠券类型：1=满减，2=折扣',
  `min_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '最低消费金额',
  `sub_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '优惠金额',
  `discount` decimal(3,1) NOT NULL DEFAULT '10.0' COMMENT '折扣率',
  `expire_type` smallint(1) NOT NULL DEFAULT '1' COMMENT '到期类型：1=领取后N天过期，2=指定有效期',
  `expire_day` int(11) NOT NULL DEFAULT '0' COMMENT '有效天数，expire_type=1时',
  `begin_time` datetime DEFAULT NULL COMMENT '有效期开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '有效期结束时间',
  `addtime` datetime NOT NULL,
  `is_delete` int(6) NOT NULL DEFAULT '0',
  `total_count` int(11) NOT NULL DEFAULT '-1' COMMENT '发放总数量',
  `is_join` smallint(6) NOT NULL DEFAULT '1' COMMENT '是否加入领券中心 1--不加入领券中心 2--加入领券中心',
  `sort` int(11) DEFAULT '100' COMMENT '排序按升序排列',
  `is_integral` smallint(6) NOT NULL DEFAULT '1' COMMENT '是否加入积分商城 1--不加入 2--加入',
  `integral` int(11) NOT NULL DEFAULT '0' COMMENT '兑换需要积分数量',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '售价',
  `total_num` int(11) NOT NULL DEFAULT '0' COMMENT '积分商城发放总数',
  `type` int(11) DEFAULT '1' COMMENT '券类型1店铺券 2 商品券,3,外部投放券 4通用券',
  `user_num` int(11) NOT NULL DEFAULT '0' COMMENT '每人限制兑换数量',
  `goods_id_list` varchar(255) DEFAULT NULL COMMENT '商品',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `store_id` (`store_id`) USING BTREE,
  KEY `is_delete` (`is_delete`) USING BTREE,
  KEY `is_join` (`is_join`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='优惠券';

-- -------------------------------------------
-- TABLE `coupon_auto_send`
-- -------------------------------------------
DROP TABLE IF EXISTS `coupon_auto_send`;
CREATE TABLE IF NOT EXISTS `coupon_auto_send` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `event` int(11) NOT NULL DEFAULT '1' COMMENT '触发事件：1=分享，2=购买并付款',
  `send_times` int(11) NOT NULL DEFAULT '1' COMMENT '最多发放次数，0表示不限制',
  `addtime` int(11) NOT NULL DEFAULT '0',
  `is_delete` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `is_delete` (`is_delete`) USING BTREE,
  KEY `store_id` (`store_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='优惠券自动发放';

-- -------------------------------------------
-- TABLE `express`
-- -------------------------------------------
DROP TABLE IF EXISTS `express`;
CREATE TABLE IF NOT EXISTS `express` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '物流公司名称',
  `status` tinyint(1) DEFAULT '1' COMMENT '显示',
  `type` varchar(255) DEFAULT NULL COMMENT '公司编号',
  `createTime` datetime DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `fallintolog`
-- -------------------------------------------
DROP TABLE IF EXISTS `fallintolog`;
CREATE TABLE IF NOT EXISTS `fallintolog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderId` bigint(20) DEFAULT NULL COMMENT '订单编号',
  `memberId` bigint(20) DEFAULT NULL COMMENT '会员id',
  `goodsId` bigint(20) DEFAULT NULL COMMENT '商品id',
  `fallIntoMoney` decimal(10,2) DEFAULT NULL COMMENT '分成金额',
  `actualMoney` decimal(10,2) DEFAULT NULL COMMENT '实际分成',
  `createTime` datetime DEFAULT NULL,
  `modifyTime` datetime DEFAULT NULL,
  `state` int(11) DEFAULT '0' COMMENT '领取状态 0表示未领取 1表示已分配',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `fenxiaoshang`
-- -------------------------------------------
DROP TABLE IF EXISTS `fenxiaoshang`;
CREATE TABLE IF NOT EXISTS `fenxiaoshang` (
  `id` int(12) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_id` int(12) NOT NULL COMMENT 'Member ID',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `phone` char(32) DEFAULT NULL COMMENT '手机号码',
  `recommend` varchar(255) DEFAULT NULL COMMENT '推荐人信息',
  `status` char(32) DEFAULT NULL COMMENT '申请状态',
  `create_time` datetime DEFAULT NULL COMMENT '申请时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `member_id` (`member_id`) USING BTREE,
  KEY `phone` (`phone`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分销商申请表';

-- -------------------------------------------
-- TABLE `goods`
-- -------------------------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE IF NOT EXISTS `goods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `goods_name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `goods_jingle` varchar(255) DEFAULT NULL COMMENT '商品广告词',
  `gc_id` bigint(20) DEFAULT NULL COMMENT '商品分类id',
  `egc_id` bigint(20) unsigned DEFAULT NULL COMMENT '拓展分类ID',
  `use_attr` int(11) DEFAULT '0' COMMENT '是否使用规格',
  `brand_id` bigint(20) DEFAULT NULL COMMENT '品牌id',
  `goods_price` decimal(10,2) DEFAULT '0.00' COMMENT '商品价格',
  `goods_promotion_price` decimal(10,2) DEFAULT '0.00' COMMENT '商品促销价格',
  `goods_promotion_type` int(11) DEFAULT '0' COMMENT '促销类型 0无促销，1团购，2限时折扣',
  `goods_marketprice` decimal(10,2) DEFAULT '0.00' COMMENT '市场价',
  `goods_serial` varchar(255) DEFAULT NULL COMMENT '商家编号',
  `goods_storage_alarm` int(11) DEFAULT '0' COMMENT '库存报警值',
  `goods_click` int(11) DEFAULT '0' COMMENT '商品点击数量',
  `goods_salenum` int(11) DEFAULT '0' COMMENT '销售数量',
  `goods_collect` int(11) DEFAULT '0' COMMENT '收藏数量',
  `attr` longtext COMMENT '规格属性',
  `goods_spec` text COMMENT '商品规格序列化',
  `goods_body` longtext COMMENT '商品详情',
  `mobile_body` longtext COMMENT '手机端详情',
  `goods_storage` int(11) DEFAULT '0' COMMENT '商品库存',
  `goods_pic` varchar(255) DEFAULT NULL COMMENT '商品主图',
  `goods_image` text COMMENT '商品相册',
  `goods_state` int(11) DEFAULT '0' COMMENT '商品状态 0下架，1正常，10违规（禁售）',
  `goods_addtime` datetime DEFAULT NULL COMMENT '商品添加时间',
  `goods_edittime` datetime DEFAULT NULL COMMENT '商品编辑时间',
  `goods_vat` int(11) DEFAULT '0' COMMENT '是否开具增值税发票 1是，0否',
  `evaluation_good_star` float DEFAULT '5' COMMENT '好评星级',
  `evaluation_count` int(11) DEFAULT '0' COMMENT '评价数',
  `is_virtual` int(11) DEFAULT '0' COMMENT '是否为虚拟商品 1是，0否',
  `is_appoint` int(11) DEFAULT '0' COMMENT '是否是预约商品 1是，0否',
  `is_presell` int(11) DEFAULT '0' COMMENT '是否是预售商品 1是，0否',
  `have_gift` int(11) DEFAULT '0' COMMENT '是否拥有赠品',
  `isdelete` int(11) DEFAULT '0' COMMENT '是否删除',
  `type` int(11) DEFAULT '1' COMMENT '商品类型:1现金商品 2积分商品 3混合商品',
  `integral` int(11) DEFAULT '0' COMMENT '积分',
  `goods_weight` decimal(10,1) DEFAULT '0.0' COMMENT '重量 单位KG',
  `is_second_kill` tinyint(1) unsigned DEFAULT NULL COMMENT '是否秒杀',
  `second_kill_groups` text COMMENT '秒杀时段',
  `second_price` decimal(12,2) DEFAULT NULL COMMENT '秒杀价格',
  `second_limit` int(12) DEFAULT NULL COMMENT '秒杀限制',
  `goods_commend` tinyint(1) DEFAULT '0' COMMENT '商品推荐 1是，0否 默认0',
  `goods_hot` tinyint(1) unsigned DEFAULT NULL COMMENT '热销产品',
  `third_code` char(50) DEFAULT NULL COMMENT '商品代号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品表';

-- -------------------------------------------
-- TABLE `goods_attr`
-- -------------------------------------------
DROP TABLE IF EXISTS `goods_attr`;
CREATE TABLE IF NOT EXISTS `goods_attr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) DEFAULT NULL,
  `attr_name` varchar(255) DEFAULT NULL COMMENT '属性名称',
  `is_delete` int(11) DEFAULT '0' COMMENT '是否删除',
  `is_default` int(11) DEFAULT '0' COMMENT '是否默认属性',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='属性值列表';

-- -------------------------------------------
-- TABLE `goods_attr_group`
-- -------------------------------------------
DROP TABLE IF EXISTS `goods_attr_group`;
CREATE TABLE IF NOT EXISTS `goods_attr_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '属性组编号',
  `group_name` varchar(255) DEFAULT NULL COMMENT '属性组名称',
  `is_delete` int(11) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='商品属性组';

-- -------------------------------------------
-- TABLE `goods_class`
-- -------------------------------------------
DROP TABLE IF EXISTS `goods_class`;
CREATE TABLE IF NOT EXISTS `goods_class` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `fid` bigint(20) DEFAULT '0' COMMENT '父级',
  `icoImg` varchar(255) DEFAULT NULL COMMENT '图标',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `createTime` datetime DEFAULT NULL,
  `modifyTime` datetime DEFAULT NULL,
  `is_del` tinyint(1) DEFAULT '0' COMMENT '1是删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品分类表';

-- -------------------------------------------
-- TABLE `goods_comment`
-- -------------------------------------------
DROP TABLE IF EXISTS `goods_comment`;
CREATE TABLE IF NOT EXISTS `goods_comment` (
  `id` int(12) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `goods_id` int(12) unsigned NOT NULL COMMENT '商品ID',
  `member_id` int(12) unsigned NOT NULL COMMENT '用户ID',
  `type` char(32) NOT NULL COMMENT '评论类型 good: 好评,medium: 中评,bad:差评 ',
  `number` char(32) NOT NULL COMMENT '订单编号',
  `score` decimal(10,1) NOT NULL DEFAULT '0.0' COMMENT '评分',
  `content` text COMMENT '评论内容',
  `image` text COMMENT '上传图片',
  `create_time` date DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `detail_id` (`goods_id`) USING BTREE,
  KEY `user_id` (`member_id`) USING BTREE,
  KEY `type` (`type`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='商品评论表';

-- -------------------------------------------
-- TABLE `goods_sku`
-- -------------------------------------------
DROP TABLE IF EXISTS `goods_sku`;
CREATE TABLE IF NOT EXISTS `goods_sku` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'sku编码',
  `goods_id` bigint(20) DEFAULT NULL COMMENT '商品编号',
  `attr` text COMMENT '属性组合',
  `num` int(11) DEFAULT '0' COMMENT '库存',
  `price` decimal(10,0) DEFAULT '0' COMMENT '价格',
  `integral` int(11) DEFAULT '0' COMMENT '积分',
  `goods_code` varchar(255) DEFAULT NULL COMMENT '商品编码',
  `picimg` varchar(255) DEFAULT NULL COMMENT '图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='商品sku表';

-- -------------------------------------------
-- TABLE `history`
-- -------------------------------------------
DROP TABLE IF EXISTS `history`;
CREATE TABLE IF NOT EXISTS `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_id` int(11) DEFAULT NULL COMMENT '会员ID',
  `goods_id` int(11) DEFAULT NULL COMMENT '商品ID',
  `add_time` date DEFAULT NULL COMMENT '日期',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='我的足迹';

-- -------------------------------------------
-- TABLE `integral_log`
-- -------------------------------------------
DROP TABLE IF EXISTS `integral_log`;
CREATE TABLE IF NOT EXISTS `integral_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `memberId` int(11) DEFAULT '0' COMMENT '会员',
  `orderId` char(32) DEFAULT '0',
  `pay_integral` int(11) DEFAULT '0' COMMENT '消耗积分',
  `old_integral` int(11) DEFAULT '0' COMMENT '原有积分',
  `now_integral` int(11) DEFAULT '0' COMMENT '现有积分',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `jingxiaoshang`
-- -------------------------------------------
DROP TABLE IF EXISTS `jingxiaoshang`;
CREATE TABLE IF NOT EXISTS `jingxiaoshang` (
  `id` int(12) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_id` int(12) NOT NULL COMMENT 'Member ID',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `phone` char(32) DEFAULT NULL COMMENT '手机号码',
  `status` char(32) NOT NULL COMMENT '申请状态',
  `province` int(12) unsigned DEFAULT NULL COMMENT '经销商省编码',
  `city` int(12) unsigned DEFAULT NULL COMMENT '经销商市编码',
  `region` int(12) unsigned DEFAULT NULL COMMENT '经销商区编码',
  `level` varchar(255) DEFAULT NULL COMMENT '经销商等级',
  `good_ids` text COMMENT '代理商品',
  `create_time` datetime DEFAULT NULL COMMENT '申请时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `member_id` (`member_id`) USING BTREE,
  KEY `phone` (`phone`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='经销商申请表';

-- -------------------------------------------
-- TABLE `level`
-- -------------------------------------------
DROP TABLE IF EXISTS `level`;
CREATE TABLE IF NOT EXISTS `level` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '等级名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `score` int(11) DEFAULT NULL COMMENT '升级分值',
  `createTime` datetime DEFAULT NULL,
  `modifyTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `member`
-- -------------------------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE IF NOT EXISTS `member` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL COMMENT '帐号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `nickname` varchar(255) DEFAULT NULL COMMENT '昵称',
  `role` int(11) DEFAULT NULL COMMENT '角色名称',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机号',
  `sex` int(11) DEFAULT NULL COMMENT '性别',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `status` varchar(255) DEFAULT NULL COMMENT '状态',
  `avatarTm` varchar(255) DEFAULT NULL COMMENT '头像缩略图',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `modifyTime` datetime DEFAULT NULL COMMENT '修改时间',
  `expiryTime` datetime DEFAULT NULL COMMENT '会员过期时间',
  `isText` int(11) DEFAULT '1' COMMENT '是否签署协议',
  `last_visit` datetime DEFAULT NULL COMMENT '最后登录时间',
  `userType` int(11) DEFAULT NULL COMMENT '用户类别',
  `authkey` varchar(255) DEFAULT NULL COMMENT '授权',
  `accessToken` varchar(255) DEFAULT NULL,
  `remainder` decimal(10,2) DEFAULT '0.00' COMMENT '余额',
  `integral` int(11) DEFAULT '0' COMMENT '积分',
  `rank` int(11) DEFAULT '0' COMMENT '等级分数',
  `brokerage` decimal(10,0) DEFAULT '0' COMMENT '分销商佣金',
  `proxyBrokerage` decimal(10,0) unsigned DEFAULT NULL COMMENT '经销商佣金',
  `pid` bigint(20) DEFAULT '0' COMMENT '上一级',
  `wxopenid` varchar(255) DEFAULT NULL COMMENT '微信openid',
  `wxnick` varchar(255) DEFAULT NULL COMMENT '微信昵称',
  `ticket` varchar(255) DEFAULT NULL,
  `qrodeUrl` varchar(255) DEFAULT NULL COMMENT '推荐二维码',
  `is_fenxiao` tinyint(1) unsigned DEFAULT '0' COMMENT '是否是分销商',
  `is_daili` tinyint(1) unsigned DEFAULT '0' COMMENT '是否是代理商',
  `origin` text COMMENT '所在区域',
  `cost_amount` decimal(12,2) unsigned DEFAULT NULL COMMENT '消费总金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `member_coupon`
-- -------------------------------------------
DROP TABLE IF EXISTS `member_coupon`;
CREATE TABLE IF NOT EXISTS `member_coupon` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` bigint(20) DEFAULT NULL COMMENT '店铺编号',
  `member_id` bigint(20) DEFAULT NULL COMMENT '会员编号',
  `coupon_id` bigint(20) DEFAULT NULL COMMENT '优惠券编号',
  `coupon_auto_send_id` bigint(20) DEFAULT '0' COMMENT '自动发放id',
  `begin_time` datetime DEFAULT NULL COMMENT '有效期开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '有效期结束时间',
  `is_expire` int(11) DEFAULT '0' COMMENT '是否已过期：0=未过期，1=已过期',
  `is_use` int(11) DEFAULT '0' COMMENT '是否已使用：0=未使用，1=已使用',
  `is_delete` int(11) DEFAULT '0' COMMENT '是否删除',
  `addtime` datetime DEFAULT NULL COMMENT '添加时间',
  `type` int(11) DEFAULT '2' COMMENT '领取类型 0--平台发放 1--自动发放 2--领取',
  `integral` int(11) DEFAULT '0' COMMENT '兑换支付积分数量',
  `price` decimal(10,0) DEFAULT '0' COMMENT '兑换支付价格',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户-优惠券关系';

-- -------------------------------------------
-- TABLE `member_level`
-- -------------------------------------------
DROP TABLE IF EXISTS `member_level`;
CREATE TABLE IF NOT EXISTS `member_level` (
  `id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT '等级名称',
  `score` int(20) DEFAULT NULL COMMENT '等级分数',
  `sale` decimal(5,2) DEFAULT NULL COMMENT '等级折扣率',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `menu`
-- -------------------------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(256) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `parent` (`parent`) USING BTREE,
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `message`
-- -------------------------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL COMMENT '会员id',
  `info` text COMMENT '消息类容',
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='网站留言';

-- -------------------------------------------
-- TABLE `migration`
-- -------------------------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `money`
-- -------------------------------------------
DROP TABLE IF EXISTS `money`;
CREATE TABLE IF NOT EXISTS `money` (
  `id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(12) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '提现人姓名',
  `type` char(255) DEFAULT NULL COMMENT '提现类型',
  `money` decimal(12,2) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '体现状态 0：审核中， 1：已完成',
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `news`
-- -------------------------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '文章编号',
  `title` varchar(255) DEFAULT NULL COMMENT '文章标题',
  `cid` bigint(20) DEFAULT NULL COMMENT '分类id',
  `themeImg` varchar(255) DEFAULT NULL COMMENT '主题图片',
  `themeImgTm` varchar(255) DEFAULT NULL COMMENT '主题缩略图',
  `brief` varchar(255) DEFAULT NULL COMMENT '简介',
  `content` text COMMENT '描述',
  `seokey` varchar(255) DEFAULT NULL COMMENT 'seo关键词',
  `seoDepict` varchar(255) DEFAULT '' COMMENT 'seo描述',
  `isShow` int(11) DEFAULT '1' COMMENT '是否显示1显示 0不显示',
  `isRmd` int(11) DEFAULT '0' COMMENT '是否推荐1推荐 0 不推荐',
  `clickRate` int(11) DEFAULT '0' COMMENT '点击量',
  `writer` varchar(255) DEFAULT NULL COMMENT '作者',
  `uid` bigint(20) DEFAULT NULL COMMENT '发布人id',
  `createTime` datetime DEFAULT NULL,
  `modifyTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `newsclassify`
-- -------------------------------------------
DROP TABLE IF EXISTS `newsclassify`;
CREATE TABLE IF NOT EXISTS `newsclassify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `fid` bigint(20) DEFAULT '0' COMMENT '父级',
  `isShow` int(11) DEFAULT '1' COMMENT '是否显示',
  `isRdm` int(11) DEFAULT '0' COMMENT '是否推荐',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `createTime` datetime DEFAULT NULL,
  `modifyTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

