<?php

namespace admin\controllers;

use common\models\UploadForm;
use Yii;
use common\models\IntergralPrize;
use app\search\IntergralPrizeSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * IntergralPrizeController implements the CRUD actions for IntergralPrize model.
 */
class IntergralPrizeController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndexList()
    {
        $query=IntergralPrize::find()->andFilterWhere(['pid_terms'=>0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('indexlist', [

            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Lists all IntergralPrize models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new IntergralPrizeSearch();
        $params=Yii::$app->request->queryParams;
        $pidIntergral=IntergralPrize::findOne($id);
        $params['IntergralPrizeSearch']['prize_terms']=$pidIntergral->prize_terms;
        $params['IntergralPrizeSearch']['pid_terms']=$pidIntergral->id;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id'=>$id
        ]);
    }

    /**
     * Displays a single IntergralPrize model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IntergralPrize model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $data=Yii::$app->request->get();

        $termIntergral=IntergralPrize::findOne($data['id']);

        $model = new IntergralPrize();

        if ($model->load(Yii::$app->request->post())) {
            $model->pid_terms=$termIntergral->id;
            $model->prize_terms=$termIntergral->prize_terms;
            $model->create_time=date('Y-m-d H:i:s');
            $model->save();
            return $this->redirect(['index','id'=>$data['id']]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    /**
     *  创建本期积分活动
     */
    public  function actionPrizeActivity(){

        $model = new IntergralPrize();
        $model->create_time=date('Y-m-d H:i:s');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index-list']);
        } else {
            return $this->render('_form_terms', [
                'model' => $model,
            ]);
        }
    }

    /**
     *  编辑本期积分活动
     */
    public  function actionEditPrizeActivity($id){

        $model = IntergralPrize::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index-list']);
        } else {
            return $this->render('_form_terms', [
                'model' => $model,
            ]);
        }
    }



    /**
     * Updates an existing IntergralPrize model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $pid=$model->pid_terms;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index','id'=>$pid]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing IntergralPrize model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        $model->is_del=1;
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the IntergralPrize model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return IntergralPrize the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IntergralPrize::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionUpload(){
        header("Access-Control-Allow-Origin:*");
        $json=array('state'=>0,'error'=>'参数错误');
        if (Yii::$app->request->post()){

            $post=Yii::$app->request->post();

            $uploadedFile = new UploadForm();

            $uploadedFile->file = UploadedFile::getInstanceByName("file");

            if ($uploadedFile->file && $uploadedFile->validate()) {

                $pieces = explode("/",$uploadedFile->file->type);

//                $filename=$uploadedFile->createImagePathWithExtension($uploadedFile->file->extension,'/uploads/goods/');
                $filename=$uploadedFile->createImagePathMD5($uploadedFile->file->extension,'/uploads/prize/',$uploadedFile->file->tempName);
                $uploadedFile->saveImage($uploadedFile->file,$filename);

                $json=array(
                    'code'=>0,
                    'url'=>yii::$app->request->hostInfo.$filename,
                    'attachment'=>$filename
                );

            }

        }else{

            $json=array('code'=>1,'msg'=>"上传失败");
        }

        echo json_encode($json);yii::$app->end();
    }
}
