<?php

namespace admin\controllers;

use common\models\Provinces;
use Yii;
use common\models\Postmethod;
use app\search\PostmethodSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PostmethodController implements the CRUD actions for Postmethod model.
 */
class PostmethodController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Postmethod models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostmethodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Postmethod model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Postmethod model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Postmethod();
        $model->type=1;
        if ($model->load(Yii::$app->request->post())) {
            if(!empty($model->send_area)){
                $model->send_area = implode(',',$model->send_area);
            }
            $model->createTime = date('Y-m-d H:i:s');
            if($model->save()){
                return $this->redirect(['index']);
            }

        } else {
            $provinces = Provinces::provinceCity();

            return $this->render('create', [
                'model' => $model,
                'provinces'=>$provinces
            ]);
        }
    }

    /**
     * Updates an existing Postmethod model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {
            if(!is_array($model->send_area)){
                $model->send_area = '';
            }
            if(!empty($model->send_area)){
                $model->send_area = implode(',',$model->send_area);
            }

            if($model->save()){
                return $this->redirect(['index']);
            }
        } else {
            $model->send_area = explode(',',$model->send_area);

            $provinces = Provinces::provinceCity();

            //print_r($model->send_area);exit;
            return $this->render('update', [
                'model' => $model,
                'provinces'=>$provinces
            ]);
        }
    }

    /**
     * Deletes an existing Postmethod model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Postmethod model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Postmethod the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Postmethod::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
