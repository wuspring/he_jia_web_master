<?php

namespace admin\controllers;

use common\models\Goods;
use common\models\TicketApplyGoodsData;
use DL\Project\Store;
use Faker\Provider\Base;
use Yii;
use common\models\TicketApplyGoods;
use app\search\TicketApplyGoodsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TicketApplyGoodsController implements the CRUD actions for TicketApplyGoods model.
 */
class TicketApplyGoodsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TicketApplyGoods models.
     * @return mixed
     */
    public function actionIndex($ticketId=0)
    {

        $searchModel = new TicketApplyGoodsSearch();
        $request = Yii::$app->request->queryParams;

        isset($request['TicketApplyGoodsSearch']['ticket_id'])
            and $ticketId = $request['TicketApplyGoodsSearch']['ticket_id'];

        $request['TicketApplyGoodsSearch']['ticket_id'] = $ticketId;
        $dataProvider = $searchModel->search($request);

        return $this->render('index', [
            'ticketId' => $ticketId,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TicketApplyGoods model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TicketApplyGoods model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($ticketId=0)
    {
        $user = Yii::$app->user->identity;
        $model = new TicketApplyGoodsData();

        if ($model->load(Yii::$app->request->post())) {
            $goodInfo = Goods::findOne($model->good_id);
            if (!strlen((string)$model->good_price)) {
                $model->good_price = $goodInfo->goods_price;
            }

            if (!strlen((string)$model->good_pic)) {
                $model->good_pic = $goodInfo->goods_pic;
            }

            if (!strlen((string)$model->good_amount)) {
                $model->good_amount = $goodInfo->goods_storage;
            }

            if ($model->save()) {
                return $this->redirect(['ticket-apply/join', 'ticketId' => $model->ticket_id]);
            }
        }

        $model->ticket_id = $ticketId;
        $model->user_id = $user->id;

        $applyGoods = TicketApplyGoodsData::findAll([
            'ticket_id' => $model->ticket_id,
            'user_id' => $model->user_id,
        ]);
        $notIds = $applyGoods ? arrayGroupsAction($applyGoods, function ($applyGood) {
            return $applyGood->good_id;
        }) : [0];
        $goods = Store::init()->goods($model->user_id, ['ids_no' => $notIds]);
        return $this->render('create', [
            'model' => $model,
            'goods' => $goods,
        ]);
    }

    public function actionSetIndex($id=0)
    {
        $apply = TicketApplyGoodsData::findOne($id);

        if ($apply) {
            $apply->is_index = (int)(!$apply->is_index);
            $apply->save() and apiSendSuccess("设置成功");

        }

        apiSendError("设置失败");
    }

    public function actionSetZhanhui($id=0)
    {
        $apply = TicketApplyGoodsData::findOne($id);

        if ($apply) {
            $apply->is_zhanhui = (int)(!$apply->is_zhanhui);
            $apply->save() and apiSendSuccess("设置成功");

        }

        apiSendError("设置失败");
    }


    /**
     * Updates an existing TicketApplyGoods model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $goods = Store::init()->goods($model->user_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {


        if (yii::$app->user->identity->assignment == \common\models\User::ASSIGNMENT_HOU_TAI) {
           return $this->redirect(['ticket-apply/join', 'ticketId' => $model->ticket_id]);
        } else {
				return $this->redirect(['ticket-apply-goods/index', 'ticketId' => $model->ticket_id]);
        
        }
            
        } else {

            return $this->render('update', [
                'model' => $model,
                'goods' => $goods,
            ]);
        }
    }

    /**
     * Deletes an existing TicketApplyGoods model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TicketApplyGoods model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TicketApplyGoods the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TicketApplyGoodsData::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
