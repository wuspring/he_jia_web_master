<?php
namespace admin\controllers;

use common\models\UploadForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use yii\captcha\CaptchaValidator;
use yii\captcha\Captcha;
use common\models\User;
use common\models\CityArea;
use common\models\Provinces;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','captcha','diqu','upload'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','upload'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'logout' => ['post'],
            //     ],
            // ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        	'captcha' => [
        			'class' => 'yii\captcha\CaptchaAction',
        			'backColor'=>0xFFFFFF,  //背景颜色
        			'minLength'=>4,  //最短为4位
        			'maxLength'=>5,   //是长为4位
        			'transparent'=>true,  //显示为透明
        	]
        ];
    }

    public function actionIndex()
    {
            
        return $this->render('index');
    }


    public function actionDiqu(){
        // $data=$this->data(0,1);
        $data=Provinces::find()->All();
        $list=array();
        foreach ($data as $key => $value) {
            $list[]=$value->attributes;
          
        }
         echo json_encode($list); yii::$app->end();
    }

    public function data($pid=0,$level){
        $data=CityArea::find()->where(['pid'=>$pid])->all();

        foreach ($data as $key => $value) {
            $model=new Provinces();
            $model->id=$value->id;
            $model->cname=$value->name;
            $model->upid=$pid;
            $model->level=$level;
            $model->save();
            $data=$this->data($value->id,$level+1);
        }
      
    }
    public function actionLogin()
    {
              $this->layout="@app/views/layouts/login.php";
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionUpload()
    {
        header("Access-Control-Allow-Origin:*");
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $uploadedFile = new UploadForm();
            $uploadedFile->file = UploadedFile::getInstanceByName("file");
            if ($uploadedFile->file && $uploadedFile->validate()) {
                $filename = $uploadedFile->createImagePathMD5($uploadedFile->file->extension, '/uploads/picture/', $uploadedFile->file->tempName);
                $uploadedFile->saveImage($uploadedFile->file, $filename);
                $json = [
                    'code' => 0,
                    'url' => yii::$app->request->hostInfo . $filename,
                    'attachment' => $filename
                ];
            } else {
                $json = ['code' => 1, 'msg' => "验证失败", 'data' => $uploadedFile->validate()];
            }
        } else {
            $json = ['code' => 1, 'msg' => "上传失败"];
        }
        echo json_encode($json);yii::$app->end();
    }
}
