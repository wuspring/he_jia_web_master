<?php

namespace admin\controllers;

use common\models\UploadForm;
use Yii;
use common\models\Brand;
use app\search\BrandSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BrandController implements the CRUD actions for Brand model.
 */
class BrandController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Brand models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BrandSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Brand model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Brand model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Brand();
        $model->brand_sort = 99;
        $model->brand_recommend = 0;
        $model->show_type = 1;
        $model->user_id = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Brand model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Brand model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Brand model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Brand the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Brand::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionUpload(){
        header("Access-Control-Allow-Origin:*");
        $json=array('state'=>0,'error'=>'参数错误');
        if (Yii::$app->request->post()){

            $post=Yii::$app->request->post();

            $uploadedFile = new UploadForm();

            $uploadedFile->file = UploadedFile::getInstanceByName("file");

            if ($uploadedFile->file && $uploadedFile->validate()) {

                $pieces = explode("/",$uploadedFile->file->type);

                $filename=$uploadedFile->createImagePathMD5($uploadedFile->file->extension,'/uploads/brand/',$uploadedFile->file->tempName);
                $uploadedFile->saveImage($uploadedFile->file,$filename);

                $json=array(
                    'code'=>0,
                    'url'=>yii::$app->request->hostInfo.$filename,
                    'attachment'=>$filename
                );

            }

        }else{

            $json=array('code'=>1,'msg'=>"上传失败");
        }

        echo json_encode($json);yii::$app->end();
    }
}
