<?php

namespace admin\controllers;

use app\search\GoodsClassSearch;
use common\models\GoodsClass;
use common\models\SelectCity;
use common\models\StoreGcScore;
use common\models\User;
use DL\Project\FloorConfig;
use DL\Project\Store;
use Yii;
use common\models\FloorIndex;
use app\search\FloorIndex as FloorIndexSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FloorIndexController implements the CRUD actions for FloorIndex model.
 */
class FloorIndexController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FloorIndex models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = GoodsClass::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $searchModel,
        ]);
        $searchModel->where(['is_del'=>0]);
        $searchModel->andFilterWhere([
            '<>', 'fid', 0
        ])->orderBy('fid asc');

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStoreInfo()
    {
        $ids = Yii::$app->request->get('storeIds');
        $ids or $ids = [0];
        $users = User::find()->where([
            'and',
            ['in', 'id', $ids],
            ['=', 'assignment', User::ASSIGNMENT_HOU_TAI],
            ['=', 'status', 1],
        ])->all();

        $users = $users ? arrayGroupsAction($users, function ($user) {
            return [
                'storeId' => (int)$user->id,
                'avatarTm' => (string)$user->avatarTm,
                'nickname' => (string)$user->nickname,
            ];
        }) : [];

        apiSendSuccess("OK", $users);
    }

    public function actionStore()
    {
        $key = Yii::$app->request->get('key', '###');

        $users = User::find()->where([
            'and',
            ['like', 'nickname', $key],
            ['=', 'assignment', User::ASSIGNMENT_HOU_TAI],
            ['=', 'status', 1],
        ])->all();
        $users = $users ? arrayGroupsAction($users, function ($user) {
            return [
              'storeId' => (int)$user->id,
              'avatarTm' => (string)$user->avatarTm,
              'nickname' => (string)$user->nickname,
            ];
        }) : [];

        apiSendSuccess("OK", $users);
    }

    public function actionInfo($cid)
    {
        $currentCity = $this->getCurrentCity();

        $model = FloorConfig::init()->get($currentCity->id, $cid);
        return $this->render('info', [
            'model' => $model,
            'cid' => $cid
        ]);
    }

    public function actionSetAdminScore()
    {
        if (Yii::$app->request->isPost) {

            $id = Yii::$app->request->post("id", 0);
            $val = Yii::$app->request->post("val", 0);

            $storeGcScore = StoreGcScore::findOne($id);
            $storeGcScore or apiSendError("未找到有效信息");

            $storeGcScore->admin_score = $val;
            if ($storeGcScore->save()) {
                $store = User::findOne($storeGcScore->user_id);
                $store->modifyTime = date('Y-m-d H:i:s');
                $store->save() and apiSendSuccess();
            }
        }

        apiSendError("请求失败");
    }

    /**
     * Displays a single FloorIndex model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FloorIndex model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FloorIndex();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FloorIndex model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            //---- 去除排序中断问题 -- wufeng -- start
            $imgs = json_decode($model->img,true);
            $n = 1;
            $arr = [];
            if(!empty($imgs)){
                foreach ($imgs as $key=>$item){
                    $arr['pic_'.$n] = $item;
                    $n++;
                }
            }
            $model->img = empty($arr)?'':json_encode($arr);
            //---- 去除排序中断问题 -- wufeng -- end
            $model->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FloorIndex model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FloorIndex model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return FloorIndex the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FloorIndex::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
