<?php

namespace admin\controllers;

use app\search\GoodsCouponSearch;
use common\models\IntergralGoodsAttr;
use common\models\IntergralGoodsAttrGroup;
use common\models\GoodsCoupon;
use common\models\IntergralGoodsSku;
use common\models\IntergralTreasure;
use common\models\UploadForm;
use common\search\IntergralTreasureSearch;
use Yii;
use common\models\IntergralGoods;
use common\search\IntergralGoodsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * GoodsController implements the CRUD actions for Goods model.
 */
class IntergralTreasureController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Goods models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request->queryParams;
        $request['IntergralTreasureSearch']['isdelete'] = 0;

        $searchModel = new IntergralTreasureSearch();
        $dataProvider = $searchModel->search($request);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Goods model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Goods model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new IntergralTreasure();

        if ($model->load(Yii::$app->request->post())) {
            $model->use_attr = 0;
            $model->goods_image = json_encode($model->goods_image);
            $model->goods_addtime = date('Y-m-d H:i:s');

            if($model->save()){
                return $this->redirect(['index']);
            }
        }
        $model->goods_state = 1;
        $model->goods_hot = 0;
        $model->use_attr = 0;
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Goods model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id,$defaultTitle=0)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->goods_image=json_encode($model->goods_image);
            $model->goods_edittime=date('Y-m-d H:i:s');
            if ($model->save()){
                $sku=json_decode($model->attr,true);

                $list=array();
                $skulist= IntergralGoodsSku::find()->where(['goods_id'=>$model->id])->all();

                if (count($sku)>count($skulist)){
                    foreach ($skulist as $key=>$item){
                        $item->goods_id=$model->id;
                        $item->attr=json_encode($sku[$key]['attr']);
                        $item->num=$sku[$key]['num'];
                        $item->price=$sku[$key]['price'];
                        $item->integral=$sku[$key]['integral'];
                        $item->goods_code=$sku[$key]['goods_code'];
                        $item->picimg=$sku[$key]['picimg'];
                        if ($item->save()){
                            $list[]=$item->attributes;
                        }
                    }
                    for ($i=count($skulist);$i<count($sku);$i++){
                        $skumodel=new IntergralGoodsSku();
                        $skumodel->goods_id=$model->id;
                        $skumodel->attr=json_encode($sku[$i]['attr']);
                        $skumodel->num=$sku[$i]['num'];
                        $skumodel->price=$sku[$i]['price'];
                        $skumodel->integral=$sku[$i]['integral'];
                        $skumodel->goods_code=$sku[$i]['goods_code'];
                        $skumodel->picimg=$sku[$i]['picimg'];
                        if ($skumodel->save()){
                            $list[]=$skumodel->attributes;
                        }
                    }
                }else{
                    foreach ($skulist as $key=>$item){
                        if ($key<count($sku)){
                            $item->goods_id=$model->id;
                            $item->attr=json_encode($sku[$key]['attr']);
                            $item->num=$sku[$key]['num'];
                            $item->price=$sku[$key]['price'];
                            $item->integral=$sku[$key]['integral'];
                            $item->goods_code=$sku[$key]['goods_code'];
                            $item->picimg=$sku[$key]['picimg'];
                            if ($item->save()){
                                $list[]=$item->attributes;
                            }
                        }else{
                            $item->delete();
                        }

                    }
                }

                $model->attr=json_encode($list);
                if($model->save()){
                    return $this->redirect(['index']);
                }

            }else{
                $model->goods_image= json_decode($model->goods_image);
            }

        }


        return $this->render('update', [
            'model' => $model,
            'active'=>'goods',
            'defaultTitle'=>$defaultTitle
        ]);
    }



    /**
     * Deletes an existing Goods model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model =IntergralTreasure::findOne(['id'=>$id]);
        $model->isdelete = 1;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Goods model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Goods the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IntergralTreasure::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpgoods(){
        $json=array('code'=>0,'mg'=>'参数错误');
        if (yii::$app->request->isPost){
            $post=yii::$app->request->post();
            $model = IntergralTreasure::findOne(['id'=>$post['id']]);
            if (!empty($model)){
                $model->goods_state=1;
                if ($model->save()){
                    $json=array('code'=>1,'mg'=>'上架成功');
                }else{
                    $json=array('code'=>0,'mg'=>'上架失败');
                }
            }else{
                $json=array('code'=>0,'mg'=>'参数错误');
            }
        }

        echo json_encode($json);yii::$app->end();
    }

    public function actionDngoods(){
        $json=array('code'=>0,'mg'=>'参数错误');
        if (yii::$app->request->isPost){
            $post=yii::$app->request->post();
            $model=IntergralTreasure::findOne(['id'=>$post['id']]);

            if (!empty($model)){
                $model->goods_state=0;
                if ($model->save()){
                    $json=array('code'=>1,'mg'=>'下架成功');
                }else{
                    $json=array('code'=>0,'mg'=>'下架失败');
                }
            }else{
                $json=array('code'=>0,'mg'=>'参数错误');
            }
        }

        echo json_encode($json);yii::$app->end();
    }

    public function actionUpload(){
        header("Access-Control-Allow-Origin:*");
        $json=array('state'=>0,'error'=>'参数错误');
        if (Yii::$app->request->post()){

            $post=Yii::$app->request->post();

            $uploadedFile = new UploadForm();

            $uploadedFile->file = UploadedFile::getInstanceByName("file");

            if ($uploadedFile->file && $uploadedFile->validate()) {

                $pieces = explode("/",$uploadedFile->file->type);

//                $filename=$uploadedFile->createImagePathWithExtension($uploadedFile->file->extension,'/uploads/goods/');
            $filename=$uploadedFile->createImagePathMD5($uploadedFile->file->extension,'/uploads/goods/',$uploadedFile->file->tempName);
            $uploadedFile->saveImage($uploadedFile->file,$filename);

                $json=array(
                    'code'=>0,
                    'url'=>yii::$app->request->hostInfo.$filename,
                    'attachment'=>$filename
                );

            }

        }else{

            $json=array('code'=>1,'msg'=>"上传失败");
        }

        echo json_encode($json);yii::$app->end();
    }

    public function actionUpload2(){
        header("Access-Control-Allow-Origin:*");
        $json=array('state'=>0,'error'=>'参数错误');
        if (Yii::$app->request->post()){

            $post=Yii::$app->request->post();

            $uploadedFile = new UploadForm();

            $uploadedFile->file = UploadedFile::getInstanceByName("file");

            if ($uploadedFile->file && $uploadedFile->validate()) {

                $pieces = explode("/",$uploadedFile->file->type);

//                $filename=$uploadedFile->createImagePathWithExtension($uploadedFile->file->extension,'/uploads/goods/');
                $filename=$uploadedFile->createImagePathMD5($uploadedFile->file->extension,'/uploads/floor/',$uploadedFile->file->tempName);
                $uploadedFile->saveImage($uploadedFile->file,$filename);

                $json=array(
                    'code'=>0,
                    'url'=>yii::$app->request->hostInfo.$filename,
                    'attachment'=>$filename
                );

            }

        }else{

            $json=array('code'=>1,'msg'=>"上传失败");
        }

        echo json_encode($json);yii::$app->end();
    }
    public function actionAttrGroupAdd(){
        $json=array('state'=>0,'mg'=>'参数错误!');
        if (yii::$app->request->isPost){
            $post=yii::$app->request->post();
            $model=IntergralGoodsAttrGroup::findOne(['group_name'=>$post['name']]);
            if (empty($model)){
                $model = new IntergralGoodsAttrGroup();
                $model->group_name=$post['name'];
                $model->is_delete=0;
                if ($model->save()){
                    $data=$model->attributes;
                    $data['attr']=array();
                    $json=array('state'=>1,'mg'=>'添加成!','model'=>$data);
                }else{
                    $json=array('state'=>0,'mg'=>'添加失败');
                }
            }else{
                $data=$model->attributes;
                $data['attr']=array();
                $json=array('state'=>1,'mg'=>'添加成!','model'=>$data);
            }

        }

        echo json_encode($json);yii::$app->end();
    }

    public function actionAttrAdd(){
        $json=array('state'=>0,'mg'=>'参数错误!');
        if (yii::$app->request->isPost){
            $post=yii::$app->request->post();
            $model=IntergralGoodsAttr::findOne(['attr_name'=>$post['name'],'group_id'=>$post['groupid']]);
            if (empty($model)){
                $model = new IntergralGoodsAttr();
                $model->attr_name=$post['name'];
                $model->group_id=$post['groupid'];
                $model->is_delete=0;
                $model->is_default=0;
                if ($model->save()){
                    $json=array('state'=>1,'mg'=>'添加成!','model'=>$model->attributes);
                }else{
                    $json=array('state'=>0,'mg'=>'添加失败');
                }
            }else{
                $json=array('state'=>1,'mg'=>'添加成!','model'=>$model->attributes);
            }

        }
        echo json_encode($json);yii::$app->end();
    }
}
