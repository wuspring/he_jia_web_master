<?php

namespace admin\controllers;

use app\search\GoodsSearch;
use common\models\Goods;
use common\models\GoodsCoupon;
use common\models\Member;
use common\models\MemberStoreCoupon;
use common\models\Order;
use common\models\SystemCoupon;
use common\models\SystemCouponLog;
use common\models\Ticket;
use common\models\TicketApplyGoods;
use common\models\TicketInfo;
use common\models\User;
use DL\Project\CityTicket;
use DL\Project\Store;
use DL\service\OrderService;
use Yii;
use common\models\TicketApply;
use app\search\TicketApplySearch;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TicketApplyController implements the CRUD actions for TicketApply model.
 */
class ExpandController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionRefresh()
    {
        $cacheDir = ROOT_PATH . '/runtimes/data';

        $deleteDir = ROOT_PATH . '/runtimes/delete';
        is_dir($deleteDir) or @mkdir($deleteDir, 777);
        if (is_dir($cacheDir)) {
            $moveDir = $deleteDir . '/' . time();
            @rename($cacheDir, $moveDir);

            apiSendSuccess("刷新成功");
        }

        apiSendSuccess("缓存已刷新");
    }
}
