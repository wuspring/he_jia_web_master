<?php

namespace admin\controllers;

use common\models\IntergralPrize;
use Yii;
use common\models\IntergralLuckDraw;
use app\search\IntergralLuckDrawSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IntergralLuckDrawController implements the CRUD actions for IntergralLuckDraw model.
 */
class IntergralLuckDrawController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all IntergralLuckDraw models.
     * @return mixed
     */
    public function actionIndex($prize_termsId)
    {
        $params=Yii::$app->request->queryParams;

        $pidIntergral=IntergralPrize::findOne($prize_termsId);
        $searchModel = new IntergralLuckDrawSearch();
        $params['IntergralLuckDrawSearch']['prize_terms']=$pidIntergral->prize_terms;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $prize_termsId
     * @return string
     *  获奖记录
     */

    public function actionLuckPerson($prize_termsId)
    {
        //获奖id
        $prizeArr=[1,2,3,5,6,8,9];
        $pidIntergral=IntergralPrize::findOne($prize_termsId);
        $query=IntergralLuckDraw::find()->andFilterWhere(['prize_terms'=>$pidIntergral->prize_terms])->andFilterWhere(['in','draw_id',$prizeArr]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('luck-person', [
            'dataProvider' => $dataProvider,
        ]);
    }



    /**
     * Displays a single IntergralLuckDraw model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IntergralLuckDraw model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new IntergralLuckDraw();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing IntergralLuckDraw model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing IntergralLuckDraw model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IntergralLuckDraw model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return IntergralLuckDraw the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IntergralLuckDraw::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
