<?php

namespace admin\controllers;

use Yii;
use common\models\Payment;
use app\search\PaymentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\UploadForm;

/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class PaymentController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Payment model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Payment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Payment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionWechat($id){
        $text=  $this->findModel($id);

        if (empty($text->payment_config)) {
            $text->payment_config=json_encode($model=array('mchid'=>'','key'=>'','apiclient_cert'=>'','apiclient_key'=>'','rootca'=>''));
            $model = json_decode($text->payment_config);
        }else{
            $model = json_decode($text->payment_config);
        }

        if (Yii::$app->request->post()) {

            $model->mchid = $_POST['mchid'];
            $model->key = $_POST['key'];
            $apiclientCert = new UploadForm();
            $apiclientCert->allfile = UploadedFile::getInstanceByName('apiclient_cert');
            if ($apiclientCert->allfile && $apiclientCert->validate()) {

                $apiclient_cert='/uploads/pay/wx/'.$apiclientCert->allfile->name;
                $oldapiclient_cert=$model->apiclient_cert;
                $model->apiclient_cert = $apiclient_cert;
                $apiclientCert->deleteImage($oldapiclient_cert);
            }
            $apiclientKey = new UploadForm();
            $apiclientKey->allfile = UploadedFile::getInstanceByName('apiclient_key');
            if ($apiclientKey->allfile && $apiclientKey->validate()) {
                $apiclient_key='/uploads/pay/wx/'.$apiclientKey->allfile->name;
                $oldapiclient_key=$model->apiclient_key;
                $model->apiclient_key = $apiclient_key;
                $apiclientKey->deleteImage($oldapiclient_key);
            }
            $rootcaFile = new UploadForm();
            $rootcaFile->allfile = UploadedFile::getInstanceByName('rootca');
            if ($rootcaFile->allfile && $rootcaFile->validate()) {
                $rootca='/uploads/pay/wx/'.$rootcaFile->allfile->name;
                $oldrootca=$model->rootca;
                $model->rootca = $rootca;
                $rootcaFile->deleteImage($oldrootca);
            }

            $text->payment_config = json_encode($model);
            if ($text->save()) {
                if ($apiclientCert->allfile && $apiclientCert->validate()) {
                    $apiclientCert->saveImage($apiclientCert->allfile,$apiclient_cert);
                }
                if ($apiclientKey->allfile && $apiclientKey->validate()) {
                    $apiclientKey->saveImage($apiclientKey->allfile,$apiclient_key);
                }
                if ($rootcaFile->allfile && $rootcaFile->validate()) {
                    $rootcaFile->saveImage($rootcaFile->allfile,$rootca);
                }
            }
        }

        return $this->render('wxpay', [
            'model' => $model,
        ]);
    }

    /**
     * 支付宝支付配置 - app
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAlipay($id){
        $text=  $this->findModel($id);

        if (empty($text->payment_config)) {
            $text->payment_config=json_encode([
                'app_id'=>'',
                'merchant_private_key'=>'',
                'alipay_public_key'=>'',
                'sign_type'=>'RSA2'
            ]);
            $model = json_decode($text->payment_config);
        }else{
            $model = json_decode($text->payment_config);
        }

        if (Yii::$app->request->post()) {
            foreach ($_POST as $key=>$item){
                $model->$key = $item;
            }

            $text->payment_config = json_encode($model);
            if ($text->save()) {

            }
            return $this->redirect(['index']);
        }

        return $this->render('app_alipay', [
            'model' => $model
        ]);
    }
    /**
     * Updates an existing Payment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionIsstate(){
        $id=$_POST['id'];
        $state=$_POST['status'];
        $model = $this->findModel($id);
        $model->state=$state;
        if($model->save()){
            echo $model->state;
        }
    }

    /**
     * Deletes an existing Payment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Payment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Payment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
