<?php

namespace admin\controllers;

use common\models\Address;
use common\models\Config;
use common\models\Express;
use common\models\Member;
use common\models\OrderGoods;
use common\models\OrderReback;
use common\models\Payment;
use common\models\Ticket;
use common\models\User;
use DL\WechatPay;
use DL\WechatRefund;
use Yii;
use common\models\Order;
use app\search\OrderSearch;
use yii\base\Module;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends BaseController
{
    public function __construct($id, Module $module, array $config = [])
    {
//        die('stop');
        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $get = Yii::$app->request->get();
        $filter = [];
        $user = Yii::$app->user->identity;
        if ($user and $user->assignment != User::ASSIGNMENT_GUAN_LI_YUAN) {
            $filter['user_id'] = $user->id;
        }

        $filter['type'] = Order::TYPE_ZHAN_HUI;

        $data = Order::find()->where($filter);
        //展会ID筛选
        if(isset($get['id']) && !empty($get['id'])){
            $data->andFilterWhere(['ticket_id'=>intval($get['id'])]);
        }else{
            $cityId = $this->getCurrentCity();
            $ticketIds = Ticket::find()->where(['citys'=>$cityId])->select('id')->column();
            $data->andFilterWhere(['in','ticket_id', $ticketIds]);
        }

        if(isset($get['keyword_1'])){
            if($get['keyword_1']==1){
                if(!empty($get['keyword'])){
                    $data->andFilterWhere(['like','orderid',trim($get['keyword'])]);
                }
            }else if($get['keyword_1']==2){
                if(!empty($get['keyword'])){
                    $data->andFilterWhere(['like','buyer_name',trim($get['keyword'])]);
                }
            }else if($get['keyword_1']==3){
                if(!empty($get['keyword'])){
                    $data->andFilterWhere(['like','receiver_name',trim($get['keyword'])]);
                }
            }
            if(!empty($get['date_start'])){
                $data->andFilterWhere(['>=','add_time',trim($get['date_start'])]);
            }

            if(!empty($get['date_end'])){
                $end_time = trim($get['date_end']).' 23:59:59';
                $data->andFilterWhere(['<','add_time', $end_time]);
            }
        }

        $filterStatus = false;
        if(!empty($get['order_state'])){
            $filterStatus = $get['order_state'];
            $data->andFilterWhere(['=', 'order_state', $get['order_state']]);
        }

        $sort = 'add_time DESC';
        $model = $this->getPagedRows($data, ['order' => $sort, 'pageSize' => '6', 'rows' => 'models']);
        $list = array();
        foreach ($model['models'] as $key => $value) {
            $list[] = array(
                'orderid' => $value->orderid,
                'buyer_name' => $value->buyer_name,
                'order_amount' => $value->order_amount,
                'integral_amount' => $value->integral_amount,
                'type' => $value->ticketTypeInfo,
                'freight' => $value->freight,
                'evaluation_state' => $value->evaluation_state,
                'order_state' =>$value->order_state,
                'state_txt'=>$value->getOrderState(),
                'receiver' => $value->receiver,
                'add_time' => $value->add_time,
                'receiver_name' => $value->receiver_name,
                'receiver_state' => $value->receiver_state,
                'receiver_address' => $value->receiver_address,
                'receiver_mobile' => $value->receiver_mobile,
                'receiver_zip' => $value->receiver_zip,
                'shipping' => $value->shipping,
                'shipping_code' => $value->shipping_code,
                'deliveryTime' => $value->deliveryTime,
                'payment_time' => $value->payment_time,
                'finnshed_time' => $value->finnshed_time,
                'fallinto_state' => $value->fallinto_state,
                'pay_amount' => $value->pay_amount,
                'ticket_type' => $value->ticket_type,
                'goods_list'=> $value->getOrderGoodsList($value->orderid)
            );
        }

        //        运费公司
        $express = Express::find()->where(['status'=>1])->asArray()->all();
        $express_arr = ArrayHelper::map($express,'id','name');

        return $this->render('index', [
            'list' => $list,
            'type' => $filter['type'],
            'filterStatus' => $filterStatus,
            'pagination' => $model['pages'],
            'express' => $express_arr,
        ]);
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionStoreIndex()
    {
        $get = Yii::$app->request->get();
        $filter = [];
        $user = Yii::$app->user->identity;
        if ($user and $user->assignment != User::ASSIGNMENT_GUAN_LI_YUAN) {
            $filter['user_id'] = $user->id;
        }

        $filter['type'] = Order::STORE_TYPE_COUPON;

        $data = Order::find()->where($filter);

        if(isset($get['keyword_1'])){
            if($get['keyword_1']==1){
                if(!empty($get['keyword'])){
                    $data->andFilterWhere(['like','orderid',trim($get['keyword'])]);
                }
            }else if($get['keyword_1']==2){
                if(!empty($get['keyword'])){
                    $data->andFilterWhere(['like','buyer_name',trim($get['keyword'])]);
                }
            }else if($get['keyword_1']==3){
                if(!empty($get['keyword'])){
                    $data->andFilterWhere(['like','receiver_name',trim($get['keyword'])]);
                }
            }
            if(!empty($get['date_start'])){
                $data->andFilterWhere(['>=','add_time',trim($get['date_start'])]);
            }

            if(!empty($get['date_end'])){
                $end_time = trim($get['date_end']).' 23:59:59';
                $data->andFilterWhere(['<','add_time', $end_time]);
            }
        }

        $filterStatus = false;
        if(!empty($get['order_state'])){
            $filterStatus = $get['order_state'];
            $data->andFilterWhere(['=', 'order_state', $get['order_state']]);
        }

        $sort = 'add_time DESC';
        $model = $this->getPagedRows($data, ['order' => $sort, 'pageSize' => '6', 'rows' => 'models']);
        $list = array();
        foreach ($model['models'] as $key => $value) {
            $list[] = array(
                'orderid' => $value->orderid,
                'buyer_name' => $value->buyer_name,
                'order_amount' => $value->order_amount,
                'integral_amount' => $value->integral_amount,
                'type' => $value->ticketTypeInfo,
                'freight' => $value->freight,
                'evaluation_state' => $value->evaluation_state,
                'order_state' =>$value->order_state,
                'state_txt'=>$value->getOrderState(),
                'receiver' => $value->receiver,
                'add_time' => $value->add_time,
                'receiver_name' => $value->receiver_name,
                'receiver_state' => $value->receiver_state,
                'receiver_address' => $value->receiver_address,
                'receiver_mobile' => $value->receiver_mobile,
                'receiver_zip' => $value->receiver_zip,
                'shipping' => $value->shipping,
                'shipping_code' => $value->shipping_code,
                'deliveryTime' => $value->deliveryTime,
                'payment_time' => $value->payment_time,
                'finnshed_time' => $value->finnshed_time,
                'fallinto_state' => $value->fallinto_state,
                'pay_amount' => $value->pay_amount,
                'ticket_type' => $value->ticket_type,
                'goods_list'=> $value->getOrderGoodsList($value->orderid)
            );
        }

        //        运费公司
        $express = Express::find()->where(['status'=>1])->asArray()->all();
        $express_arr = ArrayHelper::map($express,'id','name');

        return $this->render('index1', [
            'list' => $list,
            'type' => $filter['type'],
            'filterStatus' => $filterStatus,
            'pagination' => $model['pages'],
            'express' => $express_arr,
        ]);
    }

    public function actionLoader()
    {
        $get = Yii::$app->request->get();

        $filter = [];
        $user = Yii::$app->user->identity;
        if ($user and $user->assignment != User::ASSIGNMENT_GUAN_LI_YUAN) {
            $filter['user_id'] = $user->id;
        }

        $data = Order::find()->where($filter);

        if (isset($get['keyword_1'])) {
            if($get['keyword_1']==1){
                if(!empty($get['keyword'])){
                    $data->andFilterWhere(['like','orderid',trim($get['keyword'])]);
                }
            }else if($get['keyword_1']==2){
                if(!empty($get['keyword'])){
                    $data->andFilterWhere(['like','buyer_name',trim($get['keyword'])]);
                }
            }else if($get['keyword_1']==3){
                if(!empty($get['keyword'])){
                    $data->andFilterWhere(['like','receiver_name',trim($get['keyword'])]);
                }
            }
            if(!empty($get['date_start'])){
                $data->andFilterWhere(['>=','add_time',trim($get['date_start'])]);
            }

            if(!empty($get['date_end'])){
                $end_time = trim($get['date_end']).' 23:59:59';
                $data->andFilterWhere(['<','add_time', $end_time]);
            }
        }

        if(isset($get['id']) && !empty($get['id'])){
            $data->andWhere(['ticket_id'=>intval($get['id'])]);
        }
        if(!isset($get['type']) || $get['type']!='STORE_COUPON'){
            $cityId = $this->getCurrentCity();
            $ticketIds = Ticket::find()->where(['citys'=>$cityId])->select('id')->column();
            $data->andFilterWhere(['in','ticket_id', $ticketIds]);
        }

        if(!empty($get['type'])){
            $data->andFilterWhere(['=','type', $get['type']]);
        }

        $filterStatus = false;
        if(!empty($get['order_state'])){
            $filterStatus = $get['order_state'];
            $data->andFilterWhere(['=', 'order_state', $get['order_state']]);
        }

//        $model = $this->getPagedRows($data, ['order' => $sort, 'pageSize' => '6', 'rows' => 'models']);
        $models = $data->orderBy('add_time DESC')->all();

        $list = array();

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()                                               //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")                         //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")                  //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )            //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )          //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")                 //设置标记
            ->setCategory( "Test resultfile");                       //设置类别


        $objPHPExcel->setActiveSheetIndex(0)//表头的信息
        ->setCellValue('A1', "订单编号")
            ->setCellValue('B1', "买家姓名")
            ->setCellValue('C1', "展会名称")
            ->setCellValue('D1', "店铺名称")
            ->setCellValue('E1', "商品名称")
            ->setCellValue('F1', "商品数量")
            ->setCellValue('G1', "商品总价格")
            ->setCellValue('H1', "实际支付")
            ->setCellValue('I1', "剩余支付")
            ->setCellValue('J1', "订单类型")
            ->setCellValue('K1', "订单状态")
            ->setCellValue('L1', "联系方式")
            ->setCellValue('M1', "创建时间");
        $i=2;
        foreach ($models as $key => $value) {
//            $value['order_state'] = $value->orderState;
//            $value['typeInfo'] = $value->type;
            $payMethodInfo = $value->payMethodInfo;

            $goodLists = $value->getOrderGoodsList();
            $goodAmount = count($goodLists);
            $firstGood = $value->getFirstGoods();

            $ticketTypeInfo = $value->ticketTypeInfo;
            if ($value->type != $value::TYPE_ZHAN_HUI) {
                $ticketTypeInfo = '店铺订单';
            }

            $restMoney = $value['pay_amount'] - $value['order_amount'];
            $restMoney > 0 or $restMoney = 0;
            $objPHPExcel->getActiveSheet()
            ->setCellValue( "A{$i}",  $value['orderid'])
                ->setCellValue( "B{$i}",  $value['buyer_name'])
                ->setCellValue( "C{$i}",  $value->ticket->name)
                ->setCellValue( "D{$i}",  $firstGood->goods->user->nickname)
                ->setCellValue( "E{$i}",  $goodAmount > 1 ? "{$firstGood->goods_name}等共{$goodAmount}件商品": $firstGood->goods_name)
                ->setCellValue( "F{$i}",  $goodAmount > 1 ? "多件商品" : $firstGood->goods_num)
                ->setCellValue( "G{$i}",  $value->pay_amount)
                ->setCellValue( "H{$i}",  $value['order_amount'])
                ->setCellValue( "I{$i}",  $restMoney)
                ->setCellValue( "J{$i}",  $ticketTypeInfo)
                ->setCellValue( "K{$i}",  $value->getOrderState())
                ->setCellValue( "L{$i}",  $value['receiver_mobile'])
                ->setCellValue( "M{$i}",  $value['add_time']);
            $i++;
        }

        $files = RUNTIME_PATH .  '/order' . createRandKey(16) . '.xlsx';
        $objActSheet = $objPHPExcel->getActiveSheet();

        $objActSheet->setTitle('订单列表');

        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($files);
        header('Content-Type:application/vnd.ms-excel');
        header('Content-Disposition:attachment;filename="订单信息.xlsx"');
        header('Cache-Control:max-age=0');

        die(readfile($files));


    }

    public function actionReback()
    {
        $get = Yii::$app->request->get();
        $data = Order::find()->where(true)->andWhere(['=', 'reback_status', 1]);
        if(isset($get['keyword_1'])){
            if($get['keyword_1']==1){
                if(!empty($get['keyword'])){
                    $data->andFilterWhere(['like','orderid',trim($get['keyword'])]);
                }
            }else if($get['keyword_1']==2){
                if(!empty($get['keyword'])){
                    $data->andFilterWhere(['like','buyer_name',trim($get['keyword'])]);
                }
            }else if($get['keyword_1']==3){
                if(!empty($get['keyword'])){
                    $data->andFilterWhere(['like','receiver_name',trim($get['keyword'])]);
                }
            }
            if(!empty($get['date_start'])){
                $data->andFilterWhere(['>=','add_time',trim($get['date_start'])]);
            }
            if(!empty($get['date_end'])){
                $end_time = trim($get['date_end']).' 23:59:59';
                $data->andFilterWhere(['<=','add_time',$end_time]);
            }
        }

        $sort = 'add_time DESC';
        $model = $this->getPagedRows($data, ['order' => $sort, 'pageSize' => '6', 'rows' => 'models']);
        $list = array();
        foreach ($model['models'] as $key => $value) {
            $list[] = array(
                'orderid' => $value->orderid,
                'buyer_name' => $value->buyer_name,
                'order_amount' => $value->order_amount,
                'integral_amount' => $value->integral_amount,
                'freight' => $value->freight,
                'type' => $value->type,
                'evaluation_state' => $value->evaluation_state,
                'order_state' =>$value->order_state,
                'state_txt'=>$value->getOrderState(),
                'receiver' => $value->receiver,
                'add_time' => $value->add_time,
                'receiver_name' => $value->receiver_name,
                'receiver_state' => $value->receiver_state,
                'receiver_address' => $value->receiver_address,
                'receiver_mobile' => $value->receiver_mobile,
                'receiver_zip' => $value->receiver_zip,
                'shipping' => $value->shipping,
                'shipping_code' => $value->shipping_code,
                'deliveryTime' => $value->deliveryTime,
                'payment_time' => $value->payment_time,
                'finnshed_time' => $value->finnshed_time,
                'fallinto_state' => $value->fallinto_state,
                'goods_list'=> $value->getOrderGoodsList($value->orderid)
            );
        }

        // 运费公司
        $express = Express::find()->where(['status'=>1])->asArray()->all();
        $express_arr = ArrayHelper::map($express,'id','name');

        return $this->render('reback', [
            'list' => $list,
            'pagination' => $model['pages'],
            'express' => $express_arr,
        ]);
    }

    public function actionCheckReback()
    {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $orderId = $post['order_id'];
            $order = Order::findOne([
                'orderid' => $orderId
            ]);

            $order->reback_status = $post['express_id'];
            switch ($order->reback_status) {
                case 2 :
                    $order->order_state = Order::STATUS_CLOSE;
                    $order->finnshed_time = date('Y-m-d H:i:s');

                    $config= Config::find()->where(["cKey"=>'weixin'])->one();
                    $model = json_decode($config->cValue);
                    $payment = Payment::find()->where(['code' => 'wechat'])->one();
                    $paymentInfo = json_decode($payment->payment_config);

                    // todo
                    if ($order->pay_method == $order::PAY_MECHOD_WECHAT) {
                        $reReback = new OrderReback();
                        $rebackLog = $reReback::findOne([
                            'orderid' => $order->orderid,
                            'status' => 0
                        ]);

                        if (!$rebackLog) {
                            $reReback->orderid = $order->orderid;
                            $reReback->pay_sn = $order->pay_sn;
                            $reReback->reback_sn = $reReback->getNewRepaySn();
                            $reReback->status = 0;
                            $reReback->save();

                            $rebackLog = $reReback;
                        }

                        $rebackLog->create_time = date('Y-m-d H:i:s');
                        if ($rebackLog->save()) {
                            // repay money
                            $repayMoney = $order->goods_amount;
                            $wechatRefund = new WechatRefund($model->appid, $paymentInfo->mchid, $paymentInfo->key, $model->notifyUrl);
                            $wechatRefund->refund($reReback->pay_sn, $reReback->reback_sn, $order->order_amount * 100, $repayMoney * 100);
                        }
                    } else {
                        $member = Member::findOne($order->buyer_id);
                        $member->remainder = $member->remainder + $order->goods_amount;
                        $member->save();
                    }


                    break;
                case 3 :
                    $order->order_state = Order::STATUS_SEND;
                    break;
            }

            $order->save();

            apiSendSuccess("订单已退款");

        }
    }

    /**
     * 分页方法
     * @param $query
     * @param array $config
     * @return array
     */
    public function getPagedRows($query,$config=[])
    {
        $countQuery = clone $query;

        $pages=new Pagination(['totalCount' => $countQuery->count()]);

        if(isset($config['pageSize']))
        {
            $pages->setPageSize($config['pageSize'],true);
        }

        $rows = $query->offset($pages->offset)->limit($pages->limit);
        if(isset($config['order']))
        {
            $rows = $rows->orderBy($config['order']);
        }
        $rows = $rows->all();

        $rowsLable='rows';
        $pagesLable='pages';

        if(isset($config['rows']))
        {
            $rowsLable=$config['rows'];
        }
        if(isset($config['pages']))
        {
            $pagesLable=$config['pages'];
        }

        $ret=[];
        $ret[$rowsLable]=$rows;
        $ret[$pagesLable]=$pages;

        return $ret;
    }

    /**
     * Displays a single Order model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {

        $orderGoods = OrderGoods::findAll([
            'order_id' => $id
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'orderGoods' => $orderGoods
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->orderid]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->orderid]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSendOrder($id)
    {
        $order = $this->findModel($id);
        if(Yii::$app->request->isPost){

        }
        $expresses = Express::find()->where([])->all();
        return $this->render('send-order', [
            'order' => $order,
            'expresses' => $expresses
        ]);
    }

    /**
     * 发货
     */
    public function actionSendGoods()
    {
        $json = ['code'=>1,'msg'=>'参数错误'];

        if(Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model = Order::findOne($post['order_id']);
            $express = Express::findOne($post['express_id']);
            $model->shipping = $express->id;
            $model->shipping_code = $post['express_no'];
            $model->order_state = Order::STATUS_SEND;
            $model->deliveryTime = date('Y-m-d H:i:s');

            if($model->save()){
                $json = ['code'=>0,'msg'=>'发货成功'];
            }else{
                $json = ['code'=>1,'msg'=>'提交失败','err'=>$model->getErrors()];
            }
        }

        die(json_encode($json));
    }

    /**
     * 导出excel
     */
    public function actionExport()
    {
        $filter = [];
        $get = Yii::$app->request->get('q');

        if (isset($get['date_start']) and strlen($get['date_start'])) {
            $filter[] = ['>=', 'add_time', date('Y-m-d', strtotime($get['date_start']))];
        }
        if (isset($get['date_end']) and strlen($get['date_end'])) {
            $filter[] = ['<', 'add_time', date('Y-m-d 23:59:59', strtotime($get['date_end']))];
        }
        if (isset($get['keyword_1'])) {
            switch ((int)$get['keyword_1']) {
                case '1' :
                    if (isset($get['keyword']) and strlen($get['keyword'])) {
                        $filter[] = ['=', 'orderid', $get['keyword']];
                    }
                    break;
                case '2' :
                    if (isset($get['keyword']) and strlen($get['keyword'])) {
                        $filter[] = ['=', 'buyer_name', $get['keyword']];
                    }
                    break;
                case '3' :
                    if (isset($get['keyword']) and strlen($get['keyword'])) {
                        $filter[] = ['=', 'receiver_name', $get['keyword']];
                    }
                    break;
            }
        }

        isset($get['reback']) and $get['order_state'] = 5;

        if (isset($get['order_state'])) {
            $filter[] = ['=', 'order_state', (int)$get['order_state']];
        } else {
            $filter[] = ['=', 'order_state', Order::STATUS_PAY];
        }

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()                                               //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")                         //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")                  //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )            //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )          //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")                 //设置标记
            ->setCategory( "Test resultfile");                       //设置类别


        if ($filter) {
            $filter = array_merge(['and'], $filter);
        }

        $data = Order::find()->where($filter)->all();//数据库取出数据

        $objPHPExcel->setActiveSheetIndex(0)//表头的信息
        ->setCellValue('A1', "订单编号")
        ->setCellValue('B1', "支付单号")
        ->setCellValue('C1', "买家姓名")
        ->setCellValue('D1', "商品总价格")
        ->setCellValue('E1', "订单总价格")
        ->setCellValue('F1', "运费")
        ->setCellValue('G1', "订单状态")
        ->setCellValue('H1', "收货人")
        ->setCellValue('I1', "联系方式")
        ->setCellValue('J1', "收货地址")
        ->setCellValue('K1', "物流单号")
        ->setCellValue('L1', "支付方式")
        ->setCellValue('M1', "订单类型");
        $i=2;
        foreach ($data as $key => $value) {
            $value['order_state'] = $value->orderState;
            $value['typeInfo'] = $value->type;
            $payMethodInfo = $value->payMethodInfo;
            $objPHPExcel->getActiveSheet()             //     设置第一个内置表（一个xls文件里可以有多个表）为活动的
            ->setCellValue( "A{$i}",  $value['orderid'])
            ->setCellValue( "B{$i}",  $value['pay_sn'])
            ->setCellValue( "C{$i}",  $value['buyer_name'])
            ->setCellValue( "D{$i}",  $value['goods_amount'])
            ->setCellValue( "E{$i}",  $value['order_amount'])
            ->setCellValue( "F{$i}",  $value['freight'])
            ->setCellValue( "G{$i}",  $value['order_state'])
            ->setCellValue( "H{$i}",  $value['receiver_name'])
            ->setCellValue( "I{$i}",  $value['receiver_mobile'])
            ->setCellValue( "J{$i}",  $value['receiver_address'])
            ->setCellValue( "K{$i}",  $value['shipping_code'])
            ->setCellValue( "L{$i}",  $payMethodInfo)
            ->setCellValue( "M{$i}",  $value['typeInfo']);
            $i++;
        }

        $objActSheet = $objPHPExcel->getActiveSheet();

        $objActSheet->setTitle('订单列表');

        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('order.xlsx');
        header('Content-Type:application/vnd.ms-excel');
        header('Content-Disposition:attachment;filename="order.xlsx"');
        header('Cache-Control:max-age=0');

        die(readfile("order.xlsx"));
    }

    public function actionHexiaoOrder()
    {
        $orderId = Yii::$app->request->post('number', '###');

        $order = Order::find()->where([
            'and',
            ['=', 'orderid', $orderId],
            ['in', 'order_state', [Order::STATUS_WAIT, Order::STATUS_PAY]]
        ])->one();

        $order or apiSendError("订单信息未找到");

        switch ($order->ticket_type) {
            case Order::TICKET_TYPE_COUPON :
                break;
            case Order::TICKET_TYPE_ORDER :
                if ($order->order_state = Order::STATUS_WAIT) {
                    apiSendError("订单未生效！请确认您已支付定金");
                }
                break;
        }

        $order->order_state = Order::STATUS_RECEIVED;
        if ($order->save()) {
            $member = $order->member;
            $member->integral = $member->integral + $order->integral_amount;

            $member->save() and apiSendSuccess("核销成功");
        }

        apiSendError("核销失败");
    }
}
