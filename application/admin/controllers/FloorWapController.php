<?php

namespace admin\controllers;

use wap\models\Ticket;
use Yii;
use common\models\FloorWap;
use app\search\FloorWapSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FloorWapController implements the CRUD actions for FloorWap model.
 */
class FloorWapController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FloorWap models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FloorWapSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FloorWap model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FloorWap model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FloorWap();
        $model->type = FloorWap::TYPE_JIA_ZHUANG;
        $model->sort = 99;
        $model->status = 1;
        if ($model->load(Yii::$app->request->post())) {
            $model->topimg = !empty($model->topimg)?json_encode($model->topimg):'';
            $model->nextimg = !empty($model->nextimg)?json_encode($model->nextimg):'';
            $model->create_time = date('Y-m-d H:i:s');
            $model->modify_time = date('Y-m-d H:i:s');
            if($model->save()){
                return $this->redirect(['index']);
            }
        } else {
            $model->topimg = json_decode($model->topimg);
            $model->nextimg = json_decode($model->nextimg);
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FloorWap model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->topimg = !empty($model->topimg)?json_encode($model->topimg):'';
            $model->nextimg = !empty($model->nextimg)?json_encode($model->nextimg):'';
            $model->modify_time = date('Y-m-d H:i:s');
            if($model->save()){
                return $this->redirect(['index']);
            }
        } else {
            $model->topimg = json_decode($model->topimg);
            $model->nextimg = json_decode($model->nextimg);
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionTicket($type){
        $list = Ticket::find()->where(['status'=>1,'type'=>$type])->orderBy(['open_date'=>SORT_DESC])->all();
        $ids = ArrayHelper::map($list,'id','name');
        $options = '<option value="0">请选择</option>';
        if(!empty($ids)){
            foreach ($ids as $key=>$item){
                $options .= '<option value="'.$key.'">'.$item.'</option>';
            }
        }
        apiSendSuccess('ok',$options);
    }

    /**
     * Deletes an existing FloorWap model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FloorWap model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FloorWap the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FloorWap::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
