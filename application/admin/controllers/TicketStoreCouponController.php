<?php

namespace admin\controllers;

use app\search\GoodsCouponSearch;
use common\models\Goods;
use common\models\Ticket;
use common\models\User;
use Yii;
use common\models\GoodsCoupon;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GoodsCouponController implements the CRUD actions for GoodsCoupon model.
 */
class TicketStoreCouponController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all GoodsCoupon models.
     * @return mixed
     */
    public function actionIndex()
    {
        $params=Yii::$app->request->queryParams;
        $searchModel = new GoodsCouponSearch();
        $params['GoodsCouponSearch']['goods_id']=$params['goodId'];
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'goodId'=>$params['goodId'],
            'active'=>'coupon'
        ]);
    }
    /**
     * 店铺优惠劵
     */
    public function actionIndexStore($ticketId=0)
    {
        $user = Yii::$app->user->identity;
        if (!$user) {
            stopFlow("用户未登录", 'javascript:window.history.go(-1)');
        }

        $ticket = Ticket::findOne($ticketId);
        $ticket or stopFlow("未找到有效展会", 'javascript:window.history.go(-1)');
        $ticket->status or stopFlow("展会已结束", 'javascript:window.history.go(-1)');

        $params=Yii::$app->request->queryParams;
        $searchModel = new GoodsCouponSearch();

        $params['GoodsCouponSearch']['user_id'] = $user->id;
        $params['GoodsCouponSearch']['goods_id'] = -1;
        $params['GoodsCouponSearch']['type'] = 'ticket';
        $params['GoodsCouponSearch']['ticket_id'] = $ticketId;

        $dataProvider = $searchModel->search($params);

        return $this->render('index_store', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ticket' => $ticket,
            'active'=>'coupon'
        ]);
    }

    /**
     * Displays a single GoodsCoupon model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GoodsCoupon model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
         $model = new GoodsCoupon();
         $goodId=$_GET['goodId'];
         $goods=Goods::findOne($goodId);
        if ($model->load(Yii::$app->request->post())) {
            User::updateAll(['modifyTime'=>date('Y-m-d H:i:s')],['id'=>$goods->user_id]);
            $model->goods_id=$goodId;
            $model->user_id=$goods->user_id;
            $model->create_time=date('Y-m-d H:i:s');
            $model->type= 'ticket';
            if ($model->save()){
                return $this->redirect(['index', 'goodId' =>$goodId]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * @return string|\yii\web\Response
     *  添加店铺优惠劵
     */

    public function actionCreateStore($ticketId=0)
    {
        $model = new GoodsCoupon();

        if ($model->load(Yii::$app->request->post())) {
            User::updateAll(['modifyTime'=>date('Y-m-d H:i:s')],['id'=>Yii::$app->user->id]);
            $model->goods_id=-1;
            $model->user_id=Yii::$app->user->id;
            $model->create_time=date('Y-m-d H:i:s');
            $model->type= 'ticket';
            if ($model->save()){
                $user = User::findOne($model->user_id);
                if ($user) {
                    $user->modifyTime = date('Y-m-d H:i:s');
                    $user->save();
                }
                return $this->redirect(['index-store', 'ticketId' => $model->ticket_id]);
            }
        } else {

            $ticket = Ticket::findOne($ticketId);
            $ticket or stopFlow("未找到有效展会", 'javascript:window.history.go(-1)');
            $ticket->status or stopFlow("展会已结束", 'javascript:window.history.go(-1)');

            return $this->render('create_store', [
                'model' => $model,
                'ticket' => $ticket,
            ]);
        }
    }

    /**
     * Updates an existing GoodsCoupon model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            User::updateAll(['modifyTime'=>date('Y-m-d H:i:s')],['id'=>$model->user_id]);
            return $this->redirect(['index', 'goodId' =>$model->goods_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    public function actionUpdateStore($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            User::updateAll(['modifyTime'=>date('Y-m-d H:i:s')],['id'=>Yii::$app->user->id]);
            return $this->redirect(['index-store', 'ticketId' => $model->ticket_id]);
        } else {
            $ticket = Ticket::findOne($id);
            return $this->render('update_store', [
                'model' => $model,
                'ticket' => $ticket
            ]);
        }
    }

    /**
     * Deletes an existing GoodsCoupon model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=GoodsCoupon::findOne($id);
        $model->is_del=1;
        $model->save();
        return $this->redirect(['index','goodId'=>$model->goods_id]);
    }

    /**
     * Finds the GoodsCoupon model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return GoodsCoupon the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GoodsCoupon::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionUpcoupon(){
        $json=array('code'=>0,'mg'=>'参数错误');
        if (yii::$app->request->isPost){
            $post=yii::$app->request->post();
            $model=GoodsCoupon::findOne(['id'=>$post['id']]);
            if (!empty($model)){
                $model->is_show=1;
                if ($model->save()){
                    $json=array('code'=>1,'mg'=>'上架成功');
                }else{
                    $json=array('code'=>0,'mg'=>'上架失败');
                }
            }else{
                $json=array('code'=>0,'mg'=>'参数错误');
            }
        }

        echo json_encode($json);yii::$app->end();
    }

    public function actionDncoupon(){
        $json=array('code'=>0,'mg'=>'参数错误');
        if (yii::$app->request->isPost){
            $post=yii::$app->request->post();
            $model=GoodsCoupon::findOne(['id'=>$post['id']]);

            if (!empty($model)){
                $model->is_show=1;
                if ($model->save()){
                    $json=array('code'=>1,'mg'=>'下架成功');
                }else{
                    $json=array('code'=>0,'mg'=>'下架失败');
                }
            }else{
                $json=array('code'=>0,'mg'=>'参数错误');
            }
        }

        echo json_encode($json);yii::$app->end();
    }
}
