<?php

namespace admin\controllers;

use Yii;
use common\models\FitupAppointment;
use app\search\FitupAppointmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FitupAppointmentController implements the CRUD actions for FitupAppointment model.
 */
class FitupAppointmentController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                   // 'delete' => ['get'],
                ],
            ],
        ];
    }

    /**
     * Lists all FitupAppointment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request->queryParams;

        $searchModel = new FitupAppointmentSearch();

        isset($request['date_start']) and $searchModel->startTime = $request['date_start'];
        isset($request['date_end']) and $searchModel->endTime = $request['date_end'];

        $dataProvider = $searchModel->search($request);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FitupAppointment model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['index']);
        } else {
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    public function actionLoader()
    {
        $request = Yii::$app->request->queryParams;

        $searchModel = new FitupAppointmentSearch();

        isset($request['date_start']) and $searchModel->startTime = $request['date_start'];
        isset($request['date_end']) and $searchModel->endTime = $request['date_end'];

        $dataProvider = $searchModel->search($request);
        $datas = $dataProvider->query->orderBy('id desc')->all();


        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()                                               //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")                         //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")                  //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )            //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )          //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")                 //设置标记
            ->setCategory( "Test resultfile");                       //设置类别


        $objPHPExcel->setActiveSheetIndex(0)//表头的信息
        ->setCellValue('A1', "会员名称")
            ->setCellValue('B1', "名字")
            ->setCellValue('C1', "手机号")
            ->setCellValue('D1', "时间")
            ->setCellValue('E1', "是否处理")
            ->setCellValue('F1', "备注");


        $i=2;
        foreach ($datas as $key => $value) {
            $objPHPExcel->getActiveSheet()
                ->setCellValue( "A{$i}",  $value->member->nickname)
                ->setCellValue( "B{$i}",  $value->name)
                ->setCellValue( "C{$i}",  $value->mobile)
                ->setCellValue( "D{$i}",  $value->create_time)
                ->setCellValue( "E{$i}",  ($value->is_deal==1)?'已经处理':'未处理')
                ->setCellValue( "F{$i}",  $value->remarks);
            $i++;
        }

        $files = RUNTIME_PATH .  '/appointment' . createRandKey(16) . '.xlsx';
        $objActSheet = $objPHPExcel->getActiveSheet();

        $objActSheet->setTitle('装修预约列表');

        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($files);
        header('Content-Type:application/vnd.ms-excel');
        header('Content-Disposition:attachment;filename="装修预约列表.xlsx"');
        header('Cache-Control:max-age=0');

        die(readfile($files));
    }

    /**
     * Creates a new FitupAppointment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FitupAppointment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FitupAppointment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FitupAppointment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FitupAppointment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return FitupAppointment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FitupAppointment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
