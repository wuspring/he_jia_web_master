<?php

namespace admin\controllers;

use Yii;
use common\models\MTicketConfig;
use app\search\MTicketConfigSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MTicketConfigController implements the CRUD actions for MTicketConfig model.
 */
class MTicketConfigController extends BaseController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MTicketConfig models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MTicketConfigSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MTicketConfig model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MTicketConfig model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($ticketId)
    {
        $model = new MTicketConfig();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['page/wap-index', 'tab' => $model->ticket->type]);
        } else {
            $model->ads = json_encode([
                'ad_1' => '@',
                'ad_2' => '@',
                'ad_3' => '@',
                'ad_4' => '@',
            ]);
            $model->create_time = date('Y-m-d H:i:s');

            return $this->render('create', [
                'model' => $model,
                'ticketId' => $ticketId
            ]);
        }
    }

    /**
     * Updates an existing MTicketConfig model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($ticketId)
    {
        $model = $this->findModel(['ticket_id' => $ticketId]);

        if (!$model) {
            $model = new \wap\models\MTicketConfig();
            $model->ticket_id = $ticketId;
            $model->save();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['page/wap-index', 'tab' => $model->ticket->type]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'ticketId' => $ticketId,
            ]);
        }
    }

    /**
     * Deletes an existing MTicketConfig model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MTicketConfig model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MTicketConfig the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
//        if (($model = MTicketConfig::findOne($id)) !== null) {
//            return $model;
//        } else {
//            throw new NotFoundHttpException('The requested page does not exist.');
//        }

        return MTicketConfig::findOne($id);
    }
}
