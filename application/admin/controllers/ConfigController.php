<?php

namespace admin\controllers;

use common\models\SelectCity;
use DL\Project\CityExpand;
use DL\service\CacheService;
use Yii;
use common\models\Config;
use app\search\ConfigSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\UploadForm;

/**
 * ConfigController implements the CRUD actions for Config model.
 */
class ConfigController extends BaseController
{
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Config models.
     * @return mixed
     */
    public function actionIndex()
    {

        $config = require(__DIR__ . '/../../common/config/params.php');

        $config['system'] = $this->conReplace("erewwr11110000011we");

        $this->save_config(__DIR__ . '/../../common/config/params.php', $config);
        $searchModel = new ConfigSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCity()
    {
        $city = $this->getCurrentCity();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $post = (object)$post;
            CityExpand::init()->get($city->id, CityExpand::TYPE_CITY_EXPAND, false, false)->setData($post);
        }

        $config = CityExpand::init()->get($city->id, CityExpand::TYPE_CITY_EXPAND, false);

        return $this->render('city', [
            'config' => $config
        ]);
    }

    public function actionSystem()
    {
        $text = Config::find()->where(['cKey' => 'system'])->one();
        $model = json_decode($text->cValue);
        if (Yii::$app->request->post()) {
            $oldlogo = $model->logo;
            $oldwaplogo = $model->waplogo;
            $olderweima = $model->erweima;
            $model->name = $_POST['name'];
            $model->urladdress = $_POST['urladdress'];
            $model->keywords = $_POST['keywords'];
            $model->description = $_POST['description'];
            $model->telephone = $_POST['telephone'];
            $model->companyAddress = $_POST['companyAddress'];
            $model->icp = $_POST['icp'];
            $model->notice = $_POST['notice'];
            $model->company = $_POST['company'];
            $model->qq = $_POST['QQ'];
            $model->send_coupon = !empty($_POST['send_coupon']) ? json_encode($_POST['send_coupon']) : '';
            $model->recharge_integral = trim($_POST['recharge_integral']);

            $model->coupon_desc = $_POST['coupon_desc'];
            $model->mobileUrl = $_POST['mobileUrl'];

            $text->cValue = json_encode($model);
            $text->modifyTime = date('Y-m-d H:i:s');
            $picture = new UploadForm();
            $picture->file = UploadedFile::getInstanceByName('logo');
            if ($picture->file && $picture->validate()) {
                $model->logo = $picture->createImagePathWithExtension($picture->file->extension, '/uploads/config/logo/');
                $picture->deleteImage($oldlogo);
            } else {
                $model->logo = $oldlogo;
            }
            $picture2 = new UploadForm();
            $picture2->file = UploadedFile::getInstanceByName('waplogo');
            if ($picture2->file && $picture2->validate()) {
                $model->waplogo = $picture->createImagePathWithExtension($picture2->file->extension, '/uploads/config/waplogo/');
                $picture2->deleteImage($oldwaplogo);
            } else {
                $model->waplogo = $oldwaplogo;
            }
            $picture3 = new UploadForm();
            $picture3->file = UploadedFile::getInstanceByName('erweima');
            if ($picture3->file && $picture3->validate()) {
                $model->erweima = $picture3->createImagePathWithExtension($picture3->file->extension, '/uploads/config/erweima/');
                $picture3->deleteImage($olderweima);
            } else {
                $model->erweima = $olderweima;
            }
            $text->cValue = json_encode($model);
            $text->modifyTime = date('Y-m-d H:i:s');
            if ($text->save()) {
                if ($picture->file && $picture->validate()) {
                    $picture->saveImage($picture->file, $model->logo);
                }
                if ($picture2->file && $picture2->validate()) {
                    $picture2->saveImage($picture2->file, $model->waplogo);
                }
                if ($picture3->file && $picture3->validate()) {
                    $picture3->saveImage($picture3->file, $model->erweima);
                }
                CacheService::init()->flush();
            }

//            $config = require(__DIR__ . '/../../common/config/params.php');
//
//            $system=array();
//            foreach ($model as $key => $value) {
//                if($key=='send_coupon'){
//                    $system[$key]=json_decode($value);
//                }else{
//                    $system[$key]=$value;
//                }
//
//            }
//             $config['system']=$system;
//
//             $this->save_config(__DIR__ . '/../../common/config/params.php', $config);
        }
        $model->send_coupon = json_decode($model->send_coupon);
        return $this->render('system', [
            'model' => $model
        ]);
    }

    //微信参数配置
    public function actionWeixin()
    {

        $text = Config::find()->where(['cKey' => 'weixin'])->one();
        $model = json_decode($text->cValue);

        if (Yii::$app->request->post()) {
            $model->name = $_POST['name'];
            $model->wxid = $_POST['wxid'];
            $model->wxnumber = $_POST['wxnumber'];
            $model->appid = $_POST['appid'];
            $model->token = $_POST['token'];
            // $model->mchid = $_POST['mchid'];
            $model->notifyUrl = $_POST['notifyUrl'];

            $model->appsecret = $_POST['appsecret'];
            $model->aesencodingkey = $_POST['aesencodingkey'];
            $text->cValue = json_encode($model);
            $text->save();
            CacheService::init()->flush();

            /*$config = require(__DIR__ . '/../../common/config/params.php');

            $weixin=array();
            foreach ($model as $key => $value) {
                $weixin[$key]=$value;
            }
            $config['weixin']=$weixin;

            $this->save_config(__DIR__ . '/../../common/config/params.php', $config);*/

        }
        return $this->render('weixin', [
            'model' => $model
        ]);
    }

//修改配置参数
    function conReplace($value)
    {
        if ($value == 'true') return true;
        if ($value == 'false') return false;
        if (preg_match("/^\d*$/", $value) && strlen($value) < 10 && !empty($value)) return intval($value);
        return $value;
    }

//保存配置
    function save_config($app, $new_config = array())
    {
        if (!is_file($app)) {
            $file = __DIR__ . '/../../common/config/params.php';
        } else {
            $file = $app;
        }

        if (is_file($file)) {
            $config = require($file);
            $config = array_merge($config, $new_config);
        } else {
            $config = $new_config;
        }
        $content = var_export($config, true);
        $content = str_replace("_PATH' => '" . addslashes(__DIR__), "_PATH' => BASE_PATH . '", $content);

        if (file_put_contents($file, "<?php \r\nreturn " . $content . ';')) {
            return true;
        }
        return false;
    }

    /**
     * 积分设置
     */
    public function actionIntegral()
    {
        $buyGoodsTypeDic = [
            1 => '固定积分',
            2 => '按比例支付(百分比%)'
        ];

        $text = Config::find()->where(['cKey' => 'INDEX_INTEGRAL'])->one();
        if (!$text) {
            $text = new Config();
            $text->cKey = 'INDEX_INTEGRAL';
            $text->cValue = json_encode([
                'charge_integral' => 0,
                'buy_goods_type' => '',
                'buy_goods_integral' => 0,
                'title' => '',
                'explain' => '',
                'rule' => '',
                'continuation' => 10,
                'reward' => 0,
                'sign_integral' => 0
            ]);
            $text->save();
        }
        $model = json_decode($text->cValue);

        if (Yii::$app->request->post()) {
            $model->charge_integral = $_POST['charge_integral'];
            $model->buy_goods_type = $_POST['buy_goods_type'];
            $model->buy_goods_integral = (int)$_POST['buy_goods_integral'];
            $model->sign_integral = (int)$_POST['sign_integral'];

            // valid
            switch ($model->buy_goods_type) {
                case 2 :
                case 1 :
                    if ($model->buy_goods_integral < 0) {
                        stopFlow("请输入有效的积分", 'config/integral');
                    }

                    if (!preg_match('/^\d+$/', $model->buy_goods_integral)) {
                        stopFlow("单次固定积分必须为整数", 'config/integral');
                    }
                    break;
            }

            $model->title = $_POST['title'];
            $model->explain = $_POST['explain'];
            $model->rule = $_POST['rule'];
            $model->continuation = (int)$_POST['continuation'];
            $model->reward = (int)$_POST['reward'];

            $text->cValue = json_encode($model);

            $result = $text->save();
            CacheService::init()->flush();
            if (!$result) {
                var_dump($text->getErrors());
                die;
            }
        }

        return $this->render('integral', [
            'model' => $model,
            'buyGoodsTypeDic' => $buyGoodsTypeDic
        ]);
    }


    /**
     * Displays a single Config model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Config model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Config();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Config model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Config model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    //经销分成参数设置
    public function actionJingxiao()
    {

        $text = Config::find()->where(['cKey' => 'jingxiao'])->one();
        $model = json_decode($text->cValue);
        if (Yii::$app->request->post()) {
            $act = isset($_POST['act']) ? $_POST['act'] : '123456';
            switch ($act) {
                case 'add':
                    $this->fxadd($text, $model);
                    break;
                case 'update':
                    $this->fxupdate($text, $model);
                    break;
                case 'del':
                    $this->fxdel($text, $model);
                    break;
            }
            $text->cValue = json_encode($model);
            $text->save();
        }

        return $this->render('jingxiao', [
            'model' => $model
        ]);
    }

    //分成参数设置
    public function actionJxFencheng()
    {

        $text = Config::find()->where(['cKey' => 'jingxiao'])->one();
        $model = json_decode($text->cValue);
        if (Yii::$app->request->post()) {
            $act = $_POST['act'];
            switch ($act) {
                case 'add':
                    $this->fxadd($text, $model);
                    break;
                case 'update':
                    $this->fxupdate($text, $model);
                    break;
                case 'del':
                    $this->fxdel($text, $model);
                    break;
            }
            $text->cValue = json_encode($model);
            $text->save();
        }

        return $this->render('fenxiao', [
            'model' => $model
        ]);
    }


    //分销参数配置
    public function actionFenxiao()
    {

        $text = Config::find()->where(['cKey' => 'fenxiao'])->one();
        $model = json_decode($text->cValue);
        if (Yii::$app->request->post()) {

            if (isset($_POST['state'])) {
                $model->state = $_POST['state'];
            } else {
                $model->state = "off";
            }

            $text->cValue = json_encode($model);
            $text->save();
        }

        return $this->render('fenxiao', [
            'model' => $model
        ]);
    }


    //分成参数设置
    public function actionFencheng()
    {

        $text = Config::find()->where(['cKey' => 'fenxiao'])->one();
        $model = json_decode($text->cValue);
        if (Yii::$app->request->post()) {
            $act = $_POST['act'];
            switch ($act) {
                case 'add':
                    $this->fxadd($text, $model);
                    break;
                case 'update':
                    $this->fxupdate($text, $model);
                    break;
                case 'del':
                    $this->fxdel($text, $model);
                    break;
            }
            $text->cValue = json_encode($model);
            $text->save();
        }

        return $this->render('fenxiao', [
            'model' => $model
        ]);
    }


    //更新分成参数
    public function fxupdate($text, $model)
    {
        $id = $_POST['id'];
        $type = $_POST['type'];
        $val = $_POST['val'];
        // $mod = $model->lists[$id];
        $model->lists[$id]->$type = $val;
        $text->cValue = json_encode($model);
        $text->save();
        echo $val;
        Yii::$app->end();
    }

    //添加分成
    public function fxadd($text, $model)
    {
        $money = $_POST['money'];
        $percent = $_POST['percent'];
        $model->lists[] = array(
            'money' => $money,
            'percent' => $percent
        );
        $text->cValue = json_encode($model);
        $text->save();
        echo json_encode($model->lists);
        Yii::$app->end();
    }

    //删除分成
    public function fxdel($text, $model)
    {
        $id = $_POST['id'];
        unset($model->lists[$id]);
        $list = array();
        foreach ($model->lists as $key => $value) {
            $list[] = array(
                'money' => $value->money,
                'percent' => $value->percent,
            );
        }
        $model->lists = $list;
        $text->cValue = json_encode($model);
        $text->save();
        echo json_encode($model->lists);
        exit;
    }

    public function actionExpand()
    {
        $text = Config::find()->where(['cKey' => 'INDEX_EXPAND'])->one();
        $seletCity = SelectCity::find()->all();
        if (!$text) {
            $text = new Config();
            $text->cKey = 'INDEX_EXPAND';
            $text->cValue = json_encode([
                'logo' => '',
//                'tel_phone' => '',
                'times' => '',
                'map_key' => '',
                'welcome_words' => '',
                'seller_code' => '',
                'device_api' => '',
                'device_key' => '',
                'device_secret' => '',
                'access_key_id' => '',
                'access_key_secret' => '',
                'sms_code' => '',

                'pc_xssl' => '',
                'pc_gwzn' => '',
                'pc_fwbz' => '',
                'pc_hyzx' => '',
                'pc_lxwm' => '',
                'pc_yjdb' => '',

                'pc_ssdh' => '',

                'send_position' => '',
                'default_city' => '',
                'resource' => '',
                'tickets_notes' => '',
            ]);
            $text->save();
        }

        $model = json_decode($text->cValue);

        if (Yii::$app->request->post()) {
//            $model->logo = $_POST['logo'];
            //  $model->times = $_POST['times'];
            $model->map_key = $_POST['map_key'];
//            $model->tel_phone = $_POST['tel_phone'];
            $model->welcome_words = $_POST['welcome_words'];


            $model->access_key_id = $_POST['access_key_id'];
            $model->access_key_secret = $_POST['access_key_secret'];
            $model->sms_code = $_POST['sms_code'];

            $model->pc_xssl = $_POST['pc_xssl'];
            $model->pc_gwzn = $_POST['pc_gwzn'];
            $model->pc_fwbz = $_POST['pc_fwbz'];
            $model->pc_hyzx = $_POST['pc_hyzx'];
            $model->pc_lxwm = $_POST['pc_lxwm'];
            $model->pc_yjdb = $_POST['pc_yjdb'];
            $model->resource = isset($_POST['resource']) ? $_POST['resource'] : '';
            $model->tickets_notes = isset($_POST['tickets_notes']) ? $_POST['tickets_notes'] : '';
            $model->pc_ssdh = json_encode($_POST['pc_ssdh']);
            $model->send_position = $_POST['send_position'];
            $model->default_city = $_POST['default_city'];
            $text->cValue = json_encode($model);

            $result = $text->save();
            CacheService::init()->flush();
            if (!$result) {
                var_dump($text->getErrors());
                die;
            }
        }
        $seletSsdh = json_decode($model->pc_ssdh);

        return $this->render('expand', [
            'model' => $model,
            'seletCity' => $seletCity,
            'seletSsdh' => $seletSsdh
        ]);
    }

    public function actionIntergralShop()
    {
        $text = Config::find()->where(['cKey' => 'INDEX_INTERGRAL_SHOP'])->one();
        if (!$text) {
            $text = new Config();
            $text->cKey = 'INDEX_INTERGRAL_SHOP';
            $text->cValue = json_encode([
                'int_banner' => '',
                'int_score_filter' => '',
                'int_rules' => '',
                'lottery_rules' => '',
            ]);
            $text->save();
        }

        $model = json_decode($text->cValue);

        if (Yii::$app->request->post()) {
            $model->int_banner = $_POST['int_banner'];
            $model->int_score_filter = $_POST['int_score_filter'];
            $model->int_rules = $_POST['int_rules'];
            $model->lottery_rules = $_POST['lottery_rules'];

            $text->cValue = json_encode($model);

            $result = $text->save();
            CacheService::init()->flush();
            if (!$result) {
                var_dump($text->getErrors());
                die;
            }
        }
        return $this->render('intergral-shop', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Config model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Config the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Config::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
