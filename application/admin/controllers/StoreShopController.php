<?php

namespace admin\controllers;

use chenkby\region\RegionAction;
use common\models\Provinces;
use common\models\StoreInfo;
use common\models\User;
use Yii;
use common\models\StoreShop;
use app\search\StoreShopSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StoreShopController implements the CRUD actions for StoreShop model.
 */
class StoreShopController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actions()
    {
        $actions = parent::actions();
        $actions['get-region'] = [
            'class' => RegionAction::className(),
            'model' => Provinces::className()
        ];
        return $actions;
    }

    /**
     * Lists all StoreShop models.
     * @return mixed
     */
    public function actionIndex($storeId=0)
    {
        $request = Yii::$app->request->queryParams;
        $storeId or $storeId = Yii::$app->user->id;

        $userInfo  = User::findOne([
            'id' => $storeId,
            'assignment' => User::ASSIGNMENT_HOU_TAI
        ]);
        $userInfo or stopFlow('店铺信息无效');

        $request['StoreShopSearch']['user_id'] = $userInfo->id;


        $searchModel = new StoreShopSearch();
        $dataProvider = $searchModel->search($request);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'userInfo' => $userInfo
        ]);
    }

    /**
     * Displays a single StoreShop model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StoreShop model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StoreShop();

        if ($model->load(Yii::$app->request->post())) {
            $model->user_id=Yii::$app->user->id;
            User::updateAll(['modifyTime'=>date('Y-m-d H:i:s')],['id'=>Yii::$app->user->id]);
            $model->work_days=json_encode($model->work_days);
            if ($model->save()){
                $amount = $model::find()->where([
                    'user_id' => $model->user_id
                ])->count();

                StoreInfo::updateAll([
                    'store_amount' => $amount
                ], ['user_id' => $model->user_id]);

                return $this->redirect(['index', 'storeId' => $model->user_id]);
            }else{
               print_r($model->getErrors());die();
            }

        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StoreShop model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            User::updateAll(['modifyTime'=>date('Y-m-d H:i:s')],['id'=>Yii::$app->user->id]);
            $model->work_days=json_encode($model->work_days);
            if ($model->save()){
                return $this->redirect(['index', 'storeId' => $model->user_id]);
            }

        } else {
            //省市县
            $province_sheng = Provinces::findOne($model->provinces_id);
            $model->province_id=$province_sheng->upid;
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing StoreShop model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        $amount = $model::find()->where([
            'user_id' => $model->user_id
        ])->count();

        StoreInfo::updateAll([
            'store_amount' => $amount
        ], ['user_id' => $model->user_id]);


        return $this->redirect(['index']);
    }

    /**
     * Finds the StoreShop model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return StoreShop the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StoreShop::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
