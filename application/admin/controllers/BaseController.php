<?php

namespace admin\controllers;

use common\models\Provinces;
use common\models\SelectCity;
use common\models\User;
use DL\service\CacheService;
use DL\vendor\ConfigService;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * AddressController implements the CRUD actions for Address model.
 */
class BaseController extends Controller
{
    public $defaultCity;
    public $cityDatas = [];

    protected $defaultCityKey = 'ADMIN_SELECT_CITY_ID';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * 判断管理员权限
     * @return bool
     */
    public function checkManager()
    {
        $check = false;
        if (\Yii::$app->user->identity) {
            $user = User::findOne(\Yii::$app->user->id);
            if ($user->assignment == $user::ASSIGNMENT_GUAN_LI_YUAN) {
                $check = true;
            }
        }

        return $check;
    }

    protected function getCurrentCity()
    {
        $currentCityId = ConfigService::init(ConfigService::EXPAND)->get('default_city');

        if (\Yii::$app->user->identity) {
            $key = $this->defaultCityKey . \Yii::$app->user->id;
            $cacheCityId = CacheService::init()->get($key);

            if ($cacheCityId === false) {
                $user = User::findOne(\Yii::$app->user->id);
                if (!$this->checkManager() || !$user->change_city) {
                    $currentCityId = $user->provinces_id;
                }
            } else {
                $currentCityId = $cacheCityId;
            }

            CacheService::init()->set($key, $currentCityId);
        }

        return Provinces::findOne($currentCityId);
    }

    public function render($view, $params = [])
    {
        $defaultCity = $this->getCurrentCity();

        $citys = SelectCity::find()->where([])->all();
        $cityDatas = \yii\helpers\ArrayHelper::map($citys, 'provinces_id', 'name');

        $this->defaultCity = $defaultCity;
        $this->cityDatas = $cityDatas;

        return parent::render($view, $params);
    }
}
