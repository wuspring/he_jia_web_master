<?php

namespace admin\controllers;

use common\models\Member;
use Yii;
use common\models\Fenxiaoshang;
use app\search\FenxiaoshangSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FenxiaoshangController implements the CRUD actions for Fenxiaoshang model.
 */
class FenxiaoshangController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Fenxiaoshang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FenxiaoshangSearch();
        $request = Yii::$app->request->queryParams;
        $request = array_merge(['FenxiaoshangSearch' => ['status' => Fenxiaoshang::STATUS_APPLY]], $request);
        $dataProvider = $searchModel->search($request);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApply()
    {
        $searchModel = new FenxiaoshangSearch();
        $request = Yii::$app->request->queryParams;
        $request = array_merge(['FenxiaoshangSearch' => ['status' => Fenxiaoshang::STATUS_WAIT]], $request);
        $dataProvider = $searchModel->search($request);

        return $this->render('apply', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Fenxiaoshang model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Fenxiaoshang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Fenxiaoshang();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Fenxiaoshang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->status == $model::STATUS_APPLY) {
                $member = Member::findOne($model->member_id);
                $member->is_fenxiao = 1;
                $member->save();
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Fenxiaoshang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Fenxiaoshang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Fenxiaoshang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Fenxiaoshang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
