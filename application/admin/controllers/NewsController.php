<?php

namespace admin\controllers;

use Imagine\Exception\NotSupportedException;
use Yii;
use common\models\News;
use app\search\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Newsclassify;
use yii\web\UploadedFile;
use common\models\UploadForm;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends BaseController
{
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['get'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex($cid=0)
    {
        $searchModel = new NewsSearch();
        if ($cid>0) {
           $searchModel->cid = $cid;
        }else{
            $searchModel->cid = -1;
        }
        $defaultCity = $this->getCurrentCity();
        if(!empty($defaultCity)){
            $searchModel->likeCity = $defaultCity->id;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($cid=0)
    {
       $model = new News();
       $model->isShow=1;
       $model->isRmd=0;
        $str = "<select id='news-cid' class='form-control' name='News[cid]' > <option value='0'>请选择栏目</option>";
        $parent = Newsclassify::displayLists(0,$cid,$str);

        if ($model->load(Yii::$app->request->post())) {
            $model->createTime=date('Y-m-d H:i:s');
            $model->modifyTime=$model->createTime;
            $model->provinces_id=json_encode($model->provinces_id);
            $model->uid=yii::$app->user->identity->id;
           // $model->provinces_id=$this->getCurrentCity()->id;

            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'parent'=>$parent,
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $str = "<select id='news-cid' class='form-control' name='News[cid]' > <option value='0'>请选择栏目</option>";
        $parent = Newsclassify::displayLists(0, $model->cid,$str);
        if ($model->load(Yii::$app->request->post())) {
              $model->provinces_id=json_encode($model->provinces_id);
              $model->uid=yii::$app->user->identity->id;
              $model->modifyTime=date('Y-m-d H:i:s');
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'parent'=>$parent,
        ]);
    }


    /**
     *       品牌活动
     */
    public function actionIndexActivity()
    {
        $params=Yii::$app->request->queryParams;
        $searchModel = new NewsSearch();
        $params['NewsSearch']['cid']=0;
        $dataProvider = $searchModel->search($params);

        return $this->render('index_activity', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     *  品牌活动
     */
    public function actionCreateActivity($cid=0)
    {
        $model = new News();
        $model->isShow=1;
        $model->isRmd=0;
        if ($model->load(Yii::$app->request->post())) {
            $model->createTime=date('Y-m-d H:i:s');
            $model->modifyTime=$model->createTime;
            $model->provinces_id=json_encode($model->provinces_id);
            if ($model->save()) {
                return $this->redirect(['index-activity']);
            }
        }
        return $this->render('create_activity', [
            'model' => $model,
        ]);
    }

    /**
     *  品牌活动
     */

    public function actionUpdateActivity($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->provinces_id=json_encode($model->provinces_id);
            $model->uid=yii::$app->user->identity->id;
            $model->modifyTime=date('Y-m-d H:i:s');
            if ($model->save()) {
                return $this->redirect(['index-activity']);
            }
        }

        return $this->render('update_activity', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       $model=$this->findModel($id);
       if ($model->cid==0){
           $model->delete();
           return $this->redirect(['index-activity']);
       }else{
           $model->delete();
           return $this->redirect(['index']);
       }

    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
     public function actionRecommend(){
        $id=$_POST['id'];
        $state=$_POST['status'];
        $model = $this->findModel($id);
        $model->isRmd=$state;
        if($model->save()){
            echo $model->isRmd;
        }
    }
    public function actionIsshow(){
        $id=$_POST['id'];
        $state=$_POST['status'];
        $model = $this->findModel($id);
        $model->isShow=$state;
        if($model->save()){
            echo $model->isShow;
        }
    }
    public function actionUpload(){
        header("Access-Control-Allow-Origin:*");
        $json=array('state'=>0,'error'=>'参数错误');
        if (Yii::$app->request->post()){

            $post=Yii::$app->request->post();

            $uploadedFile = new UploadForm();

            $uploadedFile->file = UploadedFile::getInstanceByName("file");

            if ($uploadedFile->file && $uploadedFile->validate()) {

                $pieces = explode("/",$uploadedFile->file->type);

//                $filename=$uploadedFile->createImagePathWithExtension($uploadedFile->file->extension,'/uploads/goods/');
                $filename=$uploadedFile->createImagePathMD5($uploadedFile->file->extension,'/uploads/news/',$uploadedFile->file->tempName);
                $uploadedFile->saveImage($uploadedFile->file,$filename);

                $json=array(
                    'code'=>0,
                    'url'=>yii::$app->request->hostInfo.$filename,
                    'attachment'=>$filename
                );

            }

        }else{

            $json=array('code'=>1,'msg'=>"上传失败");
        }

        echo json_encode($json);yii::$app->end();
    }
}
