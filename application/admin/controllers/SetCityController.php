<?php

namespace admin\controllers;

use common\models\Provinces;
use common\models\SelectCity;
use common\models\User;
use DL\service\CacheService;
use DL\service\UrlService;
use Yii;
use common\models\Address;
use app\search\AddressSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AddressController implements the CRUD actions for Address model.
 */
class SetCityController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Address models.
     * @return mixed
     */
    public function actionIndex($cityId)
    {
        Yii::$app->user->isGuest and stopFlow("您尚未登录");
        $user = Yii::$app->user->identity;
        switch ($user->assignment) {
            case User::ASSIGNMENT_HOU_TAI :
                $cityId == $user->provinces_id or stopFlow("暂无权限");
                break;
        }

        $city = SelectCity::findOne([
            'provinces_id' => (int)$cityId
        ]);

        $city or stopFlow("未开放该城市");

        $key = $this->defaultCityKey . $user->id;
        CacheService::init()->set($key, $city->provinces_id);

        header('Location: /admin.php');
        die;
    }
}
