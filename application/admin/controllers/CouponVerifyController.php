<?php

namespace admin\controllers;

use app\search\GoodsSearch;
use common\models\CouponGoodsCart;
use common\models\Goods;
use common\models\GoodsCoupon;
use common\models\Member;
use common\models\MemberStoreCoupon;
use common\models\Order;
use common\models\SystemCoupon;
use common\models\SystemCouponLog;
use common\models\Ticket;
use common\models\TicketApplyGoods;
use common\models\TicketInfo;
use common\models\User;
use DL\Project\CityTicket;
use DL\Project\Store;
use DL\service\OrderService;
use Yii;
use common\models\TicketApply;
use app\search\TicketApplySearch;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TicketApplyController implements the CRUD actions for TicketApply model.
 */
class CouponVerifyController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * 普通店铺核销核销
     * Lists all TicketApply models.
     * @return mixed
     */
    public function actionIndex()
    {
        try {
            $user = Yii::$app->user->identity;
            $isManage = $user->assignment == User::ASSIGNMENT_GUAN_LI_YUAN;
            $joinZH = Store::init()->checkJoinZH(Yii::$app->user->id);

            if (Yii::$app->request->isPost) {
                $code = Yii::$app->request->post('name', '###');
                $coupon = SystemCoupon::findOne([
                    'code' => $code,
                    'status' => SystemCoupon::STATUS_TRUE
                ]);
                $coupon or stopFlow("验证码错误或已被使用", false);
                $orderData = [];
                $goods = [];

                // 票据有效的
                if ($coupon) {
                    $order = $coupon->order;
                    $ticketInfo = TicketInfo::findOne([
                        'ticket_id' => $order->ticket_id
                    ]);
                    $good = $order->getOrderGoodsList($order->orderid);
                    $orderData = array(
                        'orderid' => $order->orderid,
                        'buyer_name' => $order->buyer_name,
                        'order_amount' => $order->order_amount,
                        'integral_amount' => $order->integral_amount,
                        'ticket' => $ticketInfo->ticket_name,
                        'ticket_city' => $ticketInfo->cityName,
                        'freight' => $order->freight,
                        'evaluation_state' => $order->evaluation_state,
                        'order_state' =>$order->order_state,
                        'state_txt'=>$order->getOrderState(),
                        'receiver' => $order->receiver,
                        'add_time' => $order->add_time,
                        'receiver_name' => $order->receiver_name,
                        'receiver_state' => $order->receiver_state,
                        'receiver_address' => $order->receiver_address,
                        'receiver_mobile' => $order->receiver_mobile,
                        'receiver_zip' => $order->receiver_zip,
                        'shipping' => $order->shipping,
                        'shipping_code' => $order->shipping_code,
                        'deliveryTime' => $order->deliveryTime,
                        'payment_time' => $order->payment_time,
                        'finnshed_time' => $order->finnshed_time,
                        'fallinto_state' => $order->fallinto_state,
                        'goods_item'=> reset($good)
                    );

                    $currentCity = $this->getCurrentCity();

                    $filterUser = $isManage ? 0 : $user->id;
                    $goodIds = CityTicket::init($currentCity->id)->currentGoodsIds($filterUser);

                    $goodIds or $goodIds = [0];
                    $goodsFilter = [
                        'and',
                        ['in', 'good_id', $goodIds],
                        ['=', 'type', TicketApplyGoods::TYPE_ORDER],
                    ];
                    $isManage or $goodsFilter = array_merge($goodsFilter, [['=', 'user_id', $user->id]]);
//                    $goods = Goods::find()->where($goodsFilter)->all();
                    $applyInfos = TicketApplyGoods::find()->where($goodsFilter)->all();
                    $goods = arrayGroupsAction($applyInfos, function ($applyInfo) {
                        $good = $applyInfo->goods;
                        $good->goods_price = $applyInfo->good_price;
                        $good->goods_pic = $applyInfo->good_pic;
                        $good->goods_storage = $applyInfo->good_amount;

                        return $good;
                    });
                }

                return $this->render('select', [
                    'coupon' => $coupon,
                    'order_item' => $orderData,
                    'goods' => $goods,
                    'showSysCouponTab' => (bool)($isManage or $joinZH),
                    'active' => 'system'
                ]);
            }
        } catch (\Exception $e) {
            stopFlow($e->getMessage(), false);
        }

        return $this->render('index', [
            'active' => 'system',
            'showSysCouponTab' => (bool)($isManage or $joinZH),
        ]);
    }

    /**
     * 管理员核销
     */
    public function actionAdminCheck()
    {
        return $this->actionIndex();
    }

    /**
     * 使用并注销优惠券
     */
    public function actionUseCoupon()
    {
        $code = Yii::$app->request->post('code', '###');
        $goodId = Yii::$app->request->post('goodId', '0');
        $good = Goods::findOne([
            'id' => $goodId,
            'goods_state' => 1,
            'isdelete' => 0
        ]);
        $good or stopFlow("商品已下架");

        $user = Yii::$app->user->identity;
        $isManage = $user->assignment == User::ASSIGNMENT_GUAN_LI_YUAN;
        if (!$isManage) {
            Store::init()->checkJoinZH(Yii::$app->user->id) or stopFlow("您并未参加本次活动");
        }

        $coupon = SystemCoupon::findOne([
            'code' => $code,
            'status' => SystemCoupon::STATUS_TRUE
        ]);
        $coupon or apiSendError("验证码错误或已被使用");

        $order = Order::findOne(['orderid' => $coupon->order->orderid]);
        $goods = $coupon->order->orderGoodsList;
        $goodIds = arrayGroupsAction($goods, function ($g) {
            return $g['goods_id'];
        });

        $currentCity = $this->getCurrentCity();
        $usefullGoodIds = CityTicket::init($currentCity->id)->currentGoodsIds();

        in_array($good->id, $usefullGoodIds) or stopFlow("当前商品并未参加活动");

        $systemCouponLog = new SystemCouponLog();
        $systemCouponLog->user_id = $user->id;
        $systemCouponLog->coupon_id = $coupon->id;
        $systemCouponLog->goods_id = $good->id;
        $systemCouponLog->type = in_array($good->id, $goodIds) ? SystemCouponLog::TYPE_NORMAL : SystemCouponLog::TYPE_CHANGE;
        $systemCouponLog->create_time = date('Y-m-d');

        if ($systemCouponLog->save()) {
            $coupon->status = $coupon::STATUS_USED;
            $coupon->save();

            switch ($systemCouponLog->type) {
                case SystemCouponLog::TYPE_NORMAL :
                    $order->order_state = $order::STATUS_RECEIVED;
                    break;
                case SystemCouponLog::TYPE_CHANGE :
                    $order->order_state = $order::STATUS_CLOSE;

                    // clone 新订单信息
                    $orderService = OrderService::init();
                    $newOrder = $orderService->createNewEmptyOrder($order->buyer_id, $good->user_id,$order->type, $order->ticket_type);
                    $orderService->addOrderGoodToOrder($newOrder->orderid, $good->id, 1, $coupon->ticket_id);
                    $orderkeys = array_keys($order->attributes);
                    foreach ($orderkeys AS $key) {
                        if (in_array($key, ['orderid', 'user_id', 'goods_amount', 'order_amount', 'integral_amount', 'pd_amount', 'pay_amount'])) {
                            continue;
                        }
                        $newOrder->$key = $order->$key;
                    }
                    $newOrder->order_state = $newOrder::STATUS_RECEIVED;
                    $newOrder->save();

                    break;
            }

            $order->save() and apiSendSuccess("使用成功");
        }

        apiSendError("使用失败");
    }

    /**
     *  预约商品使用订单号查询 __qin
     */
    public function actionReserveVerify(){
        $joinZH = false;
        $user = Yii::$app->user->identity;
        $isManage = $user->assignment == User::ASSIGNMENT_GUAN_LI_YUAN;
        if (!$isManage) {
            $joinZH = Store::init()->checkJoinZH($user->id);
        }

        if (Yii::$app->request->isPost) {
            $code = Yii::$app->request->post('name', '###');
            $order = Order::findOne([
                'orderid' => $code,
                'order_state' =>Order::STATUS_WAIT
            ]);
            $order or stopFlow("验证码错误或已被使用", false);

            return $this->render('reserve-select', [
                'order'=>$order,
                'active' => 'reserve',
                'showSysCouponTab' =>(bool)($isManage or $joinZH),
            ]);
        }

        return $this->render('reserve-record', [
            'active' => 'reserve',
            'showSysCouponTab' =>(bool)($isManage or $joinZH),
        ]);
    }

    /**
     *  预约订单 使用订单号核销 _qin
     */
    public  function  actionOrderidVerify(){
        $json=array('msg'=>'核销失败');
        $code = Yii::$app->request->get('code','##');

        $order = Order::findOne([
            'orderid' => $code,
        ]);
        $order->order_state=Order::STATUS_CHECKOUT;
        if ($order->save()){
            $json=array('status'=>1,'msg'=>'核销成功');
        }else{
            print_r($order->getErrors());
        }
        return json_encode($json);
    }



    /**
     * 店铺核销商品
     * @return string
     */
    public function actionStore()
    {
        $user = Yii::$app->user->identity;
        $user or stopFlow("您尚未登录");
        $joinZH = false;
        $isManage = $user->assignment == User::ASSIGNMENT_GUAN_LI_YUAN;
        if (!$isManage) {
            $joinZH = Store::init()->checkJoinZH($user->id);
        }

        if (Yii::$app->request->isPost) {
            $code = Yii::$app->request->post('name', '###');
            $coupon = MemberStoreCoupon::findOne([
                'code' => $code,
                'is_use' => 0,
                'is_delete' => 0,
                'store_id' => Yii::$app->user->id
            ]);

//            $goods = [];
//            $member = false;

            $coupon or stopFlow("券码不在存在",'coupon-verify/store');
            // 票据有效的

            if (strtotime($coupon->begin_time) > time() || strtotime($coupon->end_time) < time()) {

                stopFlow("券码不在有效期",'coupon-verify/store');
            }

            $ticketInfo = GoodsCoupon::findOne($coupon->coupon_id);
            $ticketInfo or stopFlow("券码无效");

            $goods = $ticketInfo->goods;
            $member = $coupon->member;


            return $this->render('store-select', [
                'coupon' => $coupon,
                'goods' => $goods,
                'member' => $member,
                'ticketInfo' => $ticketInfo,
                'active' => 'store',
                'showSysCouponTab' => (bool)($isManage or $joinZH),
            ]);
        }

        return $this->render('index', [
            'active' => 'store',
            'showSysCouponTab' => (bool)($isManage or $joinZH),
        ]);
    }

    public function actionStoreCoupon()
    {
        $user = Yii::$app->user->identity;
        $user or stopFlow("您尚未登录",'coupon-verify/store');
        $joinZH = false;
        $isManage = $user->assignment == User::ASSIGNMENT_GUAN_LI_YUAN;
        if (!$isManage) {
            $joinZH = Store::init()->checkJoinZH($user->id);
        }

        $code = Yii::$app->request->post('name', '###');
        $coupon = MemberStoreCoupon::findOne([
            'code' => $code,
            'is_use' => 0,
            'is_delete' => 0,
            'store_id' => Yii::$app->user->id
        ]);
        $coupon or stopFlow("券码不存在",'coupon-verify/store');
        // 票据有效的
        if (strtotime($coupon->begin_time) > time() || strtotime($coupon->end_time) < time()) {
            stopFlow("券码不在有效期",'coupon-verify/store');
        }

        $ticketInfo = GoodsCoupon::findOne($coupon->coupon_id);

        $ticketInfo or stopFlow("券码无效");

        $ticketInfo->goods_id>0 and $query['GoodsSearch']['ids'] = [$ticketInfo->goods_id];
        $query['GoodsSearch']['user_id'] = $ticketInfo->user_id;
        $query['GoodsSearch']['isdelete'] = 0;
        $query['GoodsSearch']['goods_state'] = 1;

        $goodSearch = new GoodsSearch();
        $search = $goodSearch->search($query);

        return $this->render('store-coupon', [
            'coupon' => $coupon,
            'search' => $search,
            'ticketInfo' => $ticketInfo,
            'active' => 'store',
            'showSysCouponTab' => (bool)($isManage or $joinZH),
        ]);
    }

    public function actionUseStoreCoupon()
    {
        $code = Yii::$app->request->get('code', '###');
        $coupon = MemberStoreCoupon::findOne([
            'code' => $code,
            'is_use' => 0,
            'is_delete' => 0,
        ]);

        $coupon or apiSendError("优惠券无效");
        Yii::$app->user->id==$coupon->store_id or apiSendError("你无权使用其他店铺的优惠券",'coupon-verify/store');

        $coupon or stopFlow("券码不存在",'coupon-verify/store');
        // 票据有效的
        if (strtotime($coupon->begin_time) > time() || strtotime($coupon->end_time) < time()) {
            stopFlow("券码不在有效期",'coupon-verify/store');
        }

        $ticketInfo = GoodsCoupon::findOne($coupon->coupon_id);
        $ticketInfo or stopFlow("券码无效",'coupon-verify/store');

        $goodId = (int)Yii::$app->request->get('id', 0);
        $good = Goods::findOne([
            'id' => $goodId,
            'user_id' => $coupon->store_id,
            'goods_state' => 1,
            'isdelete' => 0,
        ]);
        $good or stopFlow("商品信息有误,请确认后重试",'coupon-verify/store' );

        if ($ticketInfo->goods_id >0) {
            $good->id != $ticketInfo->goods_id and stopFlow("该商品与优惠券不符",'coupon-verify/store' );
        }

        $coupon->is_use = 1;
        if ($coupon->save()) {
            $orderService = OrderService::init();

            $order = $orderService->createNewEmptyOrder($coupon->member_id, $good->user_id, Order::STORE_TYPE_COUPON);
            $orderService->addGoodsToOrder($order->orderid, $good->id, $coupon->coupon_id);

            $order->pay_method = $order::PAY_MECHOD_ACCOUNT;
            $order->order_state = $order::STATUS_RECEIVED;

            $order->save() and apiSendSuccess("使用成功");
        }

        apiSendError("网络异常");
    }

    /**
     * 系统购物券使用记录
     */
    public function actionRecord()
    {
        return $this->actionAdminRecord(false);
    }

    /**
     *  店铺优惠劵领取记录
     */
    public  function actionReceiveRecord(){

        $query=MemberStoreCoupon::find()->andFilterWhere([
            'is_use' => 0,
            'is_delete' => 0,
            'store_id' => Yii::$app->user->id
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('receive-record',['dataProvider'=>$dataProvider]);

    }

    public function actionAdminRecord($isManage=true)
    {
        $id = Yii::$app->request->get('id', 0);

        $filter = [
            '`sc`.`ticket_id`' => $id
        ];
        $isManage or $filter['user_id'] = Yii::$app->user->id;

        $query = SystemCouponLog::find()->leftJoin('`system_coupon` as `sc` on `system_coupon_log`.`coupon_id`=`sc`.`id`')
           ->where($filter);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        return $this->render('record', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAddGroupStroreCoupons()
    {
        $mCouponId = Yii::$app->request->get('mCouponId', 0);
        $goodId = Yii::$app->request->get('goodId', 0);

        $memberStoreCoupon = MemberStoreCoupon::findOne($mCouponId);
        $memberStoreCoupon or apiSendError("并未领取该优惠券");

        Yii::$app->user->id==$memberStoreCoupon->store_id or apiSendError("你无权使用其他店铺的优惠券");

        // 票据有效的
        if (strtotime($memberStoreCoupon->begin_time) > time() || strtotime($memberStoreCoupon->end_time) < time()) {
            apiSendError("券码不在有效期");
        }

        $memberStoreCoupon->is_use and apiSendError("优惠券已使用");

        $ticketInfo = GoodsCoupon::findOne($memberStoreCoupon->coupon_id);
        $ticketInfo or apiSendError("券码无效");

        if ($ticketInfo->goods_id > 0) {
            apiSendError("优惠券类型不符");
        }

        $good = Goods::findOne([
            'id' => $goodId,
            'user_id' => $memberStoreCoupon->store_id,
            'goods_state' => 1,
            'isdelete' => 0,
        ]);
        $good or apiSendError("商品信息有误,请确认后重试");

        $couponGoodsCart = new CouponGoodsCart();
        $cart = $couponGoodsCart::findOne([
            'mcoupon_id' => $memberStoreCoupon->id,
            'good_id' => $good->id
        ]);

        $cart and apiSendSuccess("您已添加");

        $couponGoodsCart->coupon_id = $ticketInfo->id;
        $couponGoodsCart->mcoupon_id = $memberStoreCoupon->id;
        $couponGoodsCart->good_id = $good->id;
        $couponGoodsCart->good_price = $good->goods_price;

        $couponGoodsCart->save() and apiSendSuccess("添加成功");

        apiSendError("添加失败");
    }

    public function actionRemoveGroupStroreCoupons()
    {
        $mCouponId = Yii::$app->request->get('mCouponId', 0);
        $goodId = Yii::$app->request->get('goodId', 0);

        $couponGoodsCart = new CouponGoodsCart();
        $cart = $couponGoodsCart::findOne([
            'mcoupon_id' => $mCouponId,
            'good_id' => $goodId
        ]);

        if ($cart) {
            $cart->delete() or apiSendError("移除失败");
        }

        apiSendSuccess("移除成功");
    }


    public function actionUseGroupStoreCoupon()
    {
        $code = Yii::$app->request->get('code', '###');
        $coupon = MemberStoreCoupon::findOne([
            'code' => $code,
            'is_use' => 0,
            'is_delete' => 0,
        ]);

        $coupon or apiSendError("优惠券无效");
        Yii::$app->user->id==$coupon->store_id or apiSendError("你无权使用其他店铺的优惠券",'coupon-verify/store');

        $coupon or stopFlow("券码不存在",'coupon-verify/store');
        // 票据有效的
        if (strtotime($coupon->begin_time) > time() || strtotime($coupon->end_time) < time()) {
            stopFlow("券码不在有效期",'coupon-verify/store');
        }

        $ticketInfo = GoodsCoupon::findOne($coupon->coupon_id);
        $ticketInfo or stopFlow("券码无效",'coupon-verify/store');

        $couponGoodsCart = new CouponGoodsCart();
        $carts = $couponGoodsCart::findAll([
            'mcoupon_id' => $coupon->id,
        ]);

        $carts or stopFlow("商品信息有误,请确认后重试",'coupon-verify/store' );

        if ($ticketInfo->goods_id >0) {
            stopFlow("优惠券类型有误",'coupon-verify/store' );
        } else {
            $payPrice = CouponGoodsCart::find()->where([
                'mcoupon_id' => $coupon->id,
            ])->sum('good_price');

            $payPrice < $ticketInfo->condition and stopFlow('商品未满足使用条件','coupon-verify/store' );
        }

        $coupon->is_use = 1;
        if ($coupon->save()) {
            $orderService = OrderService::init();
            $order = $orderService->createNewEmptyOrder($coupon->member_id, $ticketInfo->user_id, Order::STORE_TYPE_COUPON);
            foreach ($carts AS $index => $cart) {
                $orderService->addGoodsToOrder($order->orderid, $cart->good_id, $coupon->coupon_id);
            }

            $order->pay_method = $order::PAY_MECHOD_ACCOUNT;
            $order->order_state = $order::STATUS_RECEIVED;

            $order->save() and stopFlow("使用成功",'coupon-verify/store', true);
        }

        stopFlow("网络异常",'coupon-verify/store');
    }
}
