<?php

namespace admin\controllers;

use app\search\GoodsCouponSearch;
use common\models\GoodsAttr;
use common\models\GoodsAttrGroup;
use common\models\GoodsCoupon;
use common\models\GoodsSku;
use common\models\TicketApply;
use common\models\TicketApplyGoodsData;
use common\models\UploadForm;
use common\models\User;
use wap\models\StoreInfo;
use Yii;
use common\models\Goods;
use app\search\GoodsSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * GoodsController implements the CRUD actions for Goods model.
 */
class GoodsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Goods models.
     * @return mixed
     */
    public function actionIndex($shopid = 0)
    {
        $user = Yii::$app->user;
        $user or stopFlow("您尚未登录");

        // 清除无效数据
        $deleteDatas = Goods::find()->where([
            'and',
            ['=', 'user_id', $user->id],
            ['=', 'isdelete', 1],
            ['<', 'goods_addtime', date('Y-m-d')],
        ])->all();
         foreach ($deleteDatas AS $deleteData) {
             $deleteData->delete();
         }

        $request = Yii::$app->request->queryParams;
        $searchModel = new GoodsSearch();

        $assignment = $this->checkManager();

        if ($assignment) {         //系统管理员
            if ($shopid > 0) {
                $request['GoodsSearch']['user_id'] = $shopid;
            }
        } else {
            $request['GoodsSearch']['user_id'] = $user->id;
        }
        $request['GoodsSearch']['assignment'] = $assignment;

        $dataProvider = $searchModel->search($request);

        if ($assignment and $shopid == 0) {         //系统管理员
            $city = $this->getCurrentCity();
            $dataProvider->query->alias('g')->leftJoin(['s' => StoreInfo::tableName()], 's.user_id=g.user_id')
                ->andFilterWhere(['=', 's.provinces_id', $city->id]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'assignment' => $assignment,
            'shopid' => $shopid,
        ]);
    }

    /**
     * Displays a single Goods model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Goods model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($shopid = 0)
    {
        $model = new Goods();
        $model->isdelete = 1;
        if (yii::$app->user->identity->assignment == \common\models\User::ASSIGNMENT_HOU_TAI) {
            $model->user_id = Yii::$app->user->id;
        } else {
            $model->user_id = $shopid;
        }
        $model->goods_state = 0;
        if ($model->save()) {
            if ($shopid > 0) {
                return $this->redirect(['update', 'id' => $model->id, 'defaultTitle' => 1, 'shopid' => $shopid]);
            } else {
                return $this->redirect(['update', 'id' => $model->id, 'defaultTitle' => 1]);
            }

        }
    }


    /**
     *  当前商品的优惠劵列表
     */
    public function actionCouponList($goodId)
    {

        return $this->redirect(['goods-coupon/index', 'goodId' => $goodId]);
    }

    /**
     * Updates an existing Goods model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id, $defaultTitle = 0, $shopid = 0)
    {
		$ncity = $this->getCurrentCity();

        $model = $this->findModel($id);
        $activitygoods = TicketApplyGoodsData::findOne(['good_id' => $model->id, 'user_id' => $model->user_id]);
        if (empty($activitygoods)) {
            $activitygoods = new TicketApplyGoodsData();
        }

        if (empty($model)) {
            $model = new Goods();
            $model->goods_image = '[]';
        } else {
            $model->goods_image = json_decode($model->goods_image);
        }
        if ($model->user_id == 0 && yii::$app->user->identity->assignment == \common\models\User::ASSIGNMENT_GUAN_LI_YUAN) {
            if ($shopid > 0) {
                $model->user_id = $shopid;
            } else {
                $model->user_id = null;
            }

        }

        if ($model->load(Yii::$app->request->post())) {
            $model->goods_image = json_encode($model->goods_image);
            $model->goods_edittime = date('Y-m-d H:i:s');
            $model->isdelete = 0;
            $model->second_kill_groups = json_encode($model->second_kill_groups);
            if ($model->save()) {
                // 下架参展商品、刷新店铺信息
                if ($model->goods_state != 1) {
                    $ticketGoodsInfos = TicketApplyGoodsData::findAll([
                        'good_id' => $model->id
                    ]);

                    if ($ticketGoodsInfos) {
                        arrayGroupsAction($ticketGoodsInfos, function ($ticketGoodsInfo) {
                            $ticketGoodsInfo->status = 0;
                            $ticketGoodsInfo->save();
                        });

                        $store = User::findOne([
                            'id' => $model->user_id
                        ]);

                        if ($store) {
                            $store->modifyTime = date('Y-m-d H:i:s');
                            $store->save();
                        }
                    }
                }



                $sku = json_decode($model->attr, true);

                $list = array();
                $skulist = GoodsSku::find()->where(['goods_id' => $model->id])->all();

                if (count($sku) > count($skulist)) {
                    foreach ($skulist as $key => $item) {
                        $item->goods_id = $model->id;
                        $item->attr = json_encode($sku[$key]['attr']);
                        $item->num = $sku[$key]['num'];
                        $item->price = $sku[$key]['price'];
                        $item->integral = $sku[$key]['integral'];
                        $item->goods_code = $sku[$key]['goods_code'];
                        $item->picimg = $sku[$key]['picimg'];
                        if ($item->save()) {
                            $list[] = $item->attributes;
                        }
                    }
                    for ($i = count($skulist); $i < count($sku); $i++) {
                        $skumodel = new GoodsSku();
                        $skumodel->goods_id = $model->id;
                        $skumodel->attr = json_encode($sku[$i]['attr']);
                        $skumodel->num = $sku[$i]['num'];
                        $skumodel->price = $sku[$i]['price'];
                        $skumodel->integral = $sku[$i]['integral'];
                        $skumodel->goods_code = $sku[$i]['goods_code'];
                        $skumodel->picimg = $sku[$i]['picimg'];
                        if ($skumodel->save()) {
                            $list[] = $skumodel->attributes;
                        }
                    }
                } else {
                    foreach ($skulist as $key => $item) {
                        if ($key < count($sku)) {
                            $item->goods_id = $model->id;
                            $item->attr = json_encode($sku[$key]['attr']);
                            $item->num = $sku[$key]['num'];
                            $item->price = $sku[$key]['price'];
                            $item->integral = $sku[$key]['integral'];
                            $item->goods_code = $sku[$key]['goods_code'];
                            $item->picimg = $sku[$key]['picimg'];
                            if ($item->save()) {
                                $list[] = $item->attributes;
                            }
                        } else {
                            $item->delete();
                        }

                    }
                }

                $model->attr = json_encode($list);

                if (yii::$app->user->identity->assignment == \common\models\User::ASSIGNMENT_GUAN_LI_YUAN) {
                    if ($model->save()) {
                        $post = Yii::$app->request->post();
                        if ($post['isactivity'] == 1) {
                            if ($activitygoods->load(Yii::$app->request->post())) {
                                $activitygoods->user_id = $model->user_id;
                                $activitygoods->good_amount = $model->goods_storage;
                                $activitygoods->good_id = $model->id;
                                $ticketapply = TicketApply::findOne(['ticket_id' => $activitygoods->ticket_id, 'user_id' => $model->user_id]);
                                if (empty($ticketapply)) {
                                    $ticketapply = new TicketApply();
                                    $ticketapply->good_ids = '[]';
                                    $ticketapply->user_id = $model->user_id;
                                    $ticketapply->ticket_id = $activitygoods->ticket_id;
                                    $ticketapply->status = 1;
                                }
                                $goodsidlist = json_decode($ticketapply->good_ids, true);

                                if (!in_array($model->id, $goodsidlist)) {
                                    $goodsidlist[] = $model->id;
                                }

                                $ticketapply->good_ids = json_encode($goodsidlist);


                                if ($ticketapply->create1()) {
                                    if ($activitygoods->save()) {
                                        return $this->redirect(['index', 'shopid' => $model->user_id]);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if ($model->save()) {
                        return $this->redirect(['index', 'shopid' => $model->user_id]);
                    }
                }

                $model->goods_image = json_decode($model->goods_image);
            } else {
                $model->goods_image = json_decode($model->goods_image);
            }

        }

        $model->second_kill_groups = json_decode($model->second_kill_groups, true);

        if (yii::$app->user->identity->assignment == \common\models\User::ASSIGNMENT_GUAN_LI_YUAN) {

            $activitygoods->good_id = $model->id;
            return $this->render('update', [
                'model' => $model,
                'ncity' => $ncity,
                'active' => 'goods',
                'defaultTitle' => $defaultTitle,
                'activitygoods' => $activitygoods
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'active' => 'goods',
                'defaultTitle' => $defaultTitle
            ]);
        }


    }


    /**
     * Deletes an existing Goods model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete()
    {
        $data = Yii::$app->request->post();
        $model = Goods::findOne(['id' => $data['id']]);
        $model->isdelete = 1;
        if ($model->save()) {
            $json = array('code' => 1, 'msg' => '删除成功');
        } else {
            $json = array('code' => 0, 'msg' => '删除失败');
        }

        return json_encode($json);
    }

    /**
     * Finds the Goods model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Goods the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Goods::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpgoods()
    {
        $json = array('code' => 0, 'mg' => '参数错误');
        if (yii::$app->request->isPost) {
            $post = yii::$app->request->post();
            $model = Goods::findOne(['id' => $post['id']]);
            if (!empty($model)) {
                $model->goods_state = 1;
                if ($model->save()) {


                        $store = User::findOne([
                            'id' => $model->user_id
                        ]);

                        if ($store) {
                            $store->modifyTime = date('Y-m-d H:i:s');
                            $store->save();
                        }


                    $json = array('code' => 1, 'mg' => '上架成功');
                } else {
                    $json = array('code' => 0, 'mg' => '上架失败');
                }
            } else {
                $json = array('code' => 0, 'mg' => '参数错误');
            }
        }

        echo json_encode($json);
        yii::$app->end();
    }

    public function actionDngoods()
    {
        $json = array('code' => 0, 'mg' => '参数错误');
        if (yii::$app->request->isPost) {
            $post = yii::$app->request->post();
            $model = Goods::findOne(['id' => $post['id']]);

            if (!empty($model)) {
                $model->goods_state = 0;
                if ($model->save()) {

                    $ticketGoodsInfos = TicketApplyGoodsData::findAll([
                        'good_id' => $model->id
                    ]);

                    if ($ticketGoodsInfos) {
                        arrayGroupsAction($ticketGoodsInfos, function ($ticketGoodsInfo) {
                            $ticketGoodsInfo->status = 0;
                            $ticketGoodsInfo->save();
                        });

                        $store = User::findOne([
                            'id' => $model->user_id
                        ]);

                        if ($store) {
                            $store->modifyTime = date('Y-m-d H:i:s');
                            $store->save();
                        }
                    }


                    $json = array('code' => 1, 'mg' => '下架成功');
                } else {
                    $json = array('code' => 0, 'mg' => '下架失败');
                }
            } else {
                $json = array('code' => 0, 'mg' => '参数错误');
            }
        }

        echo json_encode($json);
        yii::$app->end();
    }

    public function actionUpload()
    {
        header("Access-Control-Allow-Origin:*");
        $json = array('state' => 0, 'error' => '参数错误');
        if (Yii::$app->request->post()) {

            $post = Yii::$app->request->post();

            $uploadedFile = new UploadForm();

            $uploadedFile->file = UploadedFile::getInstanceByName("file");

            if ($uploadedFile->file && $uploadedFile->validate()) {

                $pieces = explode("/", $uploadedFile->file->type);

//                $filename=$uploadedFile->createImagePathWithExtension($uploadedFile->file->extension,'/uploads/goods/');
                $filename = $uploadedFile->createImagePathMD5($uploadedFile->file->extension, '/uploads/goods/', $uploadedFile->file->tempName);
                $uploadedFile->saveImage($uploadedFile->file, $filename);

                $json = array(
                    'code' => 0,
                    'url' => yii::$app->request->hostInfo . $filename,
                    'attachment' => $filename
                );

            }

        } else {

            $json = array('code' => 1, 'msg' => "上传失败");
        }

        echo json_encode($json);
        yii::$app->end();
    }

    public function actionUpload2()
    {
        header("Access-Control-Allow-Origin:*");
        $json = array('state' => 0, 'error' => '参数错误');
        if (Yii::$app->request->post()) {

            $post = Yii::$app->request->post();

            $uploadedFile = new UploadForm();

            $uploadedFile->file = UploadedFile::getInstanceByName("file");

            if ($uploadedFile->file && $uploadedFile->validate()) {

                $pieces = explode("/", $uploadedFile->file->type);

//                $filename=$uploadedFile->createImagePathWithExtension($uploadedFile->file->extension,'/uploads/goods/');
                $filename = $uploadedFile->createImagePathMD5($uploadedFile->file->extension, '/uploads/floor/', $uploadedFile->file->tempName);
                $uploadedFile->saveImage($uploadedFile->file, $filename);

                $json = array(
                    'code' => 0,
                    'url' => yii::$app->request->hostInfo . $filename,
                    'attachment' => $filename
                );

            }

        } else {

            $json = array('code' => 1, 'msg' => "上传失败");
        }

        echo json_encode($json);
        yii::$app->end();
    }

    public function actionAttrGroupAdd()
    {
        $json = array('state' => 0, 'mg' => '参数错误!');
        if (yii::$app->request->isPost) {
            $post = yii::$app->request->post();
            $model = GoodsAttrGroup::findOne(['group_name' => $post['name']]);
            if (empty($model)) {
                $model = new GoodsAttrGroup();
                $model->group_name = $post['name'];
                $model->is_delete = 0;
                if ($model->save()) {
                    $data = $model->attributes;
                    $data['attr'] = array();
                    $json = array('state' => 1, 'mg' => '添加成!', 'model' => $data);
                } else {
                    $json = array('state' => 0, 'mg' => '添加失败');
                }
            } else {
                $data = $model->attributes;
                $data['attr'] = array();
                $json = array('state' => 1, 'mg' => '添加成!', 'model' => $data);
            }

        }

        echo json_encode($json);
        yii::$app->end();
    }

    public function actionAttrAdd()
    {
        $json = array('state' => 0, 'mg' => '参数错误!');
        if (yii::$app->request->isPost) {
            $post = yii::$app->request->post();
            $model = GoodsAttr::findOne(['attr_name' => $post['name'], 'group_id' => $post['groupid']]);
            if (empty($model)) {
                $model = new GoodsAttr();
                $model->attr_name = $post['name'];
                $model->group_id = $post['groupid'];
                $model->is_delete = 0;
                $model->is_default = 0;
                if ($model->save()) {
                    $json = array('state' => 1, 'mg' => '添加成!', 'model' => $model->attributes);
                } else {
                    $json = array('state' => 0, 'mg' => '添加失败');
                }
            } else {
                $json = array('state' => 1, 'mg' => '添加成!', 'model' => $model->attributes);
            }

        }
        echo json_encode($json);
        yii::$app->end();
    }
}
