<?php

namespace admin\controllers;

use Yii;
use common\models\FitupAttr;
use app\search\FitupAttrSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FitupAttrController implements the CRUD actions for FitupAttr model.
 */
class FitupAttrController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FitupAttr models.
     * @return mixed
     */
    public function actionIndex()
    {
        $cl = new FitupAttr();
        $tree = $cl->getParent(0);
        return $this->render('index', [
            'tree' => $tree,
        ]);
    }

    /**
     * Displays a single FitupAttr model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FitupAttr model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($cid = 0)
    {
        $model = new FitupAttr();

        $str = "<select id='fitupAttr-fid' class='form-control' name='FitupAttr[pid]' > <option value='0'>顶级栏目</option>";
        $parent = FitupAttr::displayListsselt(0, $cid, $str);
        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {
                return $this->redirect(['index']);
            }
            print_r($model->errors);exit;
        }

        return $this->render('create', [
            'model' => $model,
            'parent' => $parent,
        ]);

    }

    /**
     * Updates an existing FitupAttr model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $str = "<select id='fitupAttr-fid' class='form-control' name='FitupAttr[pid]' > <option value='0'>顶级栏目</option>";
        $parent = FitupAttr::displayListsselt(0, $model->pid, $str);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'parent' => $parent,
            ]);
        }
    }

    /**
     * Deletes an existing FitupAttr model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FitupAttr model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return FitupAttr the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FitupAttr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
