<?php

namespace admin\controllers;

use common\models\Member;
use Yii;
use common\models\Jingxiaoshang;
use app\search\JingxiaoshangSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * JingxiaoshangController implements the CRUD actions for Jingxiaoshang model.
 */
class JingxiaoshangController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Jingxiaoshang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JingxiaoshangSearch();
        $request = Yii::$app->request->queryParams;
        $request = array_merge(['JingxiaoshangSearch' => ['status' => Jingxiaoshang::STATUS_APPLY]], $request);
        $dataProvider = $searchModel->search($request);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApply()
    {
        $searchModel = new JingxiaoshangSearch();
        $request = Yii::$app->request->queryParams;
        $request = array_merge(['JingxiaoshangSearch' => ['status' => Jingxiaoshang::STATUS_WAIT]], $request);
        $dataProvider = $searchModel->search($request);

        return $this->render('apply', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Jingxiaoshang model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Jingxiaoshang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Jingxiaoshang();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Jingxiaoshang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->status == $model::STATUS_APPLY) {
                $member = Member::findOne($model->member_id);
                $member->is_daili = 1;
                $member->save();
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionSetting($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();
        if(isset($post['Jingxiaoshang']) and isset($post['Jingxiaoshang']['good_ids'])) {
            $post['Jingxiaoshang']['good_ids'] = implode(',', $post['Jingxiaoshang']['good_ids']);
        }
        if ($model->load($post) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            $model->good_ids = explode(',', $model->good_ids );

            return $this->render('setting', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Jingxiaoshang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $member = $model->member;
        $member->is_daili = 0;
        $member->save() and $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Jingxiaoshang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Jingxiaoshang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Jingxiaoshang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
