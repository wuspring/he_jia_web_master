<?php

namespace admin\controllers;

use common\models\Provinces;
use Yii;
use common\models\SelectCity;
use admin\search\SelectCitySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SelectCityController implements the CRUD actions for SelectCity model.
 */
class SelectCityController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SelectCity models.
     * @return mixed
     */
    public function actionIndex()
    {

        $cityObj=SelectCity::find()->all();
        $cityArr=[];
         foreach ($cityObj as $ck=>$cv){
                $cityArr[]=$cv->provinces_id;
         }

         if (Yii::$app->request->post()){
             $data=Yii::$app->request->post();
             SelectCity::deleteAll();
             foreach($data as $k=>$v)
             {
                 if ($k!="r" && $k!="_csrf"){
                     $provinces=Provinces::findOne($v);
                     $model = new SelectCity();
                     $model->provinces_id=$v;
                     $model->name=$provinces->cname;
                     $model->create_time=date('Y-m-d H:i:s');
                     $model->save();
                 }

             }
             $cityObj2=SelectCity::find()->all();
             $cityArr2=[];
             foreach ($cityObj2 as $ck2=>$cv2){
                 $cityArr2[]=$cv2->provinces_id;
             }
             return $this->render('index',[
                 'cityArr'=>$cityArr2
             ]);
         }
        return $this->render('index',[
            'cityArr'=>$cityArr
        ]);
    }

    /**
     * Displays a single SelectCity model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SelectCity model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SelectCity();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SelectCity model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SelectCity model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SelectCity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SelectCity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SelectCity::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
