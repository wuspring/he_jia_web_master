<?php

namespace admin\controllers;

use common\models\Ticket;
use common\models\User;
use Yii;
use common\models\Cart;
use app\search\CartSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use DL\Project\Page;

/**
 * CartController implements the CRUD actions for Cart model.
 */
class PageController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cart models.
     * @return mixed
     */
    public function actionIndex($type=Page::PAGE_INDEX)
    {
        in_array($type, [
            Page::PAGE_INDEX,
            Page::PAGE_CAI_GOU_JIAZHUANG,
            Page::PAGE_CAI_GOU_JIEHUN,
            Page::PAGE_CAI_GOU_YUNYING,
        ]) or stopFlow("参数无效", 'page/index');

        $typeDics = Page::init()->dictionarys();
        $currentCity = $this->getCurrentCity();
        $banner = Page::init()->get($currentCity->id, $type, false)->banner;
        $huodongs = Page::init()->get($currentCity->id, $type, false)->huodongs;
        $expand =[];
        $promise = '';
        $smap = '';
        if ($type == Page::PAGE_INDEX) {
            $data = Page::init()->get($currentCity->id, $type, false);
            $promise = isset($data->promise) ? $data->promise : '';
            $smap = isset($data->smap) ? $data->smap : '';
        }

        return $this->render('index', [
            'type' => $type,
            'typeDics' => $typeDics,
            'expand' => $expand,
            'promise' => $promise,
            'smap' => $smap,
            'banner' => $banner,
            'huodongs' => $huodongs,
        ]);
    }

    public function actionSetting($type=Page::PAGE_INDEX)
    {
        in_array($type, [
            Page::PAGE_INDEX,
            Page::PAGE_CAI_GOU_JIAZHUANG,
            Page::PAGE_CAI_GOU_JIEHUN,
            Page::PAGE_CAI_GOU_YUNYING,
        ]) or stopFlow("参数无效", 'page/index');

        $typeDics = Page::init()->dictionarys();
        $currentCity = $this->getCurrentCity();
        $model = Page::init()->get($currentCity->id, $type);

        if (Yii::$app->request->post()) {
            $model->banner = isset($_POST['Config']['banner']) ? json_decode($_POST['Config']['banner'], true) : [];
            $model->huodongs = isset($_POST['Config']['huodongs']) ? json_decode($_POST['Config']['huodongs'], true) : [];
            isset($_POST['Config']['expand']) && strlen($_POST['Config']['expand']) and $model->expand =  json_decode($_POST['Config']['expand'], true);

            isset($_POST['Config']['promise']) && strlen($_POST['Config']['promise']) and $model->promise = $_POST['Config']['promise'];
            isset($_POST['Config']['smap']) && strlen($_POST['Config']['smap']) and $model->smap = $_POST['Config']['smap'];

            Page::init()->get($currentCity->id, $type, false, false)->setData($model);
        }

        return $this->render('setting', [
            'title' => "<b>{$currentCity->cname}</b> | {$typeDics[$type]}设置",
            'model' => $model,
            'type' => $type
        ]);
    }

    public function actionWapSetting($type=Page::PAGE_INDEX)
    {
        in_array($type, [
            Page::PAGE_INDEX,
            Page::PAGE_CAI_GOU_JIAZHUANG,
            Page::PAGE_CAI_GOU_JIEHUN,
            Page::PAGE_CAI_GOU_YUNYING,
        ]) or stopFlow("参数无效", 'page/index');

        $typeDics = Page::init()->dictionarys();
        $currentCity = $this->getCurrentCity();
        $model = Page::init()->get($currentCity->id, $type);

        if (Yii::$app->request->post()) {
            $model->banner = isset($_POST['Config']['banner']) ? json_decode($_POST['Config']['banner'], true) : [];
            $model->huodongs = isset($_POST['Config']['huodongs']) ? json_decode($_POST['Config']['huodongs'], true) : [];
            isset($_POST['Config']['expand']) && strlen($_POST['Config']['expand']) and $model->expand =  json_decode($_POST['Config']['expand'], true);

            isset($_POST['Config']['promise']) && strlen($_POST['Config']['promise']) and $model->promise = $_POST['Config']['promise'];
            isset($_POST['Config']['smap']) && strlen($_POST['Config']['smap']) and $model->smap = $_POST['Config']['smap'];


            $model->icon = isset($_POST['Config']['icon']) ? $_POST['Config']['icon'] : '';
            $model->icon1 = isset($_POST['Config']['icon1']) ? $_POST['Config']['icon1'] : '';
            Page::init()->get($currentCity->id, $type, false, false)->setData($model);
        }

        $k = $type == Page::PAGE_INDEX ? '详情页' : $typeDics[$type];
        return $this->render('setting1', [
            'title' => "<b>{$currentCity->cname}</b> | {$k}设置",
            'model' => $model,
            'type' => $type
        ]);
    }

    public function actionWapIndex()
    {
        $user = Yii::$app->user->identity;

        $params = Yii::$app->request->queryParams;
        $active = isset($params['tab'])? $params['tab'] : Ticket::TYPE_JIA_ZHUANG;
        $sql = Ticket::find()->where([
            'citys' => $this->getCurrentCity(),
            'ca_type' => Ticket::CA_TYPE_ZHAN_HUI
        ]);
        $sql->andFilterWhere(['=', 'type', $active])->orderBy('status desc');
        $model = new ActiveDataProvider([
            'query' => $sql,
        ]);

        switch ($user->assignment) {
            case User::ASSIGNMENT_HOU_TAI :
                stopFlow("暂无权限");
                break;
            case User::ASSIGNMENT_GUAN_LI_YUAN :
                $tmp = 'lists_admin1';
                $isManage = true;
                break;
        }

        return $this->render($tmp, [
            'active' => $active,
            'isManage' => $isManage,
            'dataProvider' => $model,
        ]);
    }

}
