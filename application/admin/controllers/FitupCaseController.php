<?php

namespace admin\controllers;

use common\models\UploadForm;
use Yii;
use common\models\FitupCase;
use app\search\FitupCaseSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * FitupCaseController implements the CRUD actions for FitupCase model.
 */
class FitupCaseController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FitupCase models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FitupCaseSearch();
        /*$defaultCity = $this->getCurrentCity();
        if (!empty($defaultCity)) {
            $searchModel->likeCity = $defaultCity->id;
        }*/
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FitupCase model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FitupCase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FitupCase();

        $drowList=$model->getFitupChilder(3);
        $strHtml='<option value="">请选择</option>';
        foreach ($drowList as $k=>$v){
            $strHtml.=('<option value="'.$k.'">'.$v.'</option>');
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->provinces_id=json_encode($model->provinces_id);
            $tab_text_obj=$model->tab_text;
            if ($tab_text_obj){
                $tabKey=ArrayHelper::getColumn($tab_text_obj,'title');
                $model->tab=json_encode($tabKey);
                $model->tab_text=json_encode($tab_text_obj);
            }

            $model->create_time=date('Y-m-d H:i:s');
            $model->save();
            return $this->redirect(['index']);
        } else {
            $model->tab_text=json_decode($model->tab_text);

            return $this->render('create', [
                'model' => $model,
                'strHtml'=>$strHtml,
                'drowList'=>$drowList
            ]);
        }
    }

    /**
     * Updates an existing FitupCase model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $drowList=$model->getFitupChilder(3);
        $strHtml='<option value="">请选择</option>';
        foreach ($drowList as $k=>$v){
            $strHtml.=('<option value="'.$k.'">'.$v.'</option>');
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->provinces_id=json_encode($model->provinces_id);
            $tab_text_obj=$model->tab_text;
            if ($tab_text_obj){
                $tabKey=ArrayHelper::getColumn($tab_text_obj,'title');
                $model->tab=json_encode($tabKey);
                $model->tab_text=json_encode($tab_text_obj);
            }
            $model->tab_text=json_encode($tab_text_obj);
            $model->save();
            return $this->redirect(['index']);
        } else {
            $model->tab_text=json_decode($model->tab_text);
            return $this->render('update', [
                'model' => $model,
                'strHtml'=>$strHtml,
                'drowList'=>$drowList
            ]);
        }
    }

    /**
     * Deletes an existing FitupCase model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FitupCase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return FitupCase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FitupCase::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionUpload(){
        header("Access-Control-Allow-Origin:*");
        $json=array('state'=>0,'error'=>'参数错误');
        if (Yii::$app->request->post()){

            $post=Yii::$app->request->post();

            $uploadedFile = new UploadForm();

            $uploadedFile->file = UploadedFile::getInstanceByName("file");

            if ($uploadedFile->file && $uploadedFile->validate()) {

                $pieces = explode("/",$uploadedFile->file->type);

//                $filename=$uploadedFile->createImagePathWithExtension($uploadedFile->file->extension,'/uploads/goods/');
                $filename=$uploadedFile->createImagePathMD5($uploadedFile->file->extension,'/uploads/fitup/',$uploadedFile->file->tempName);
                $uploadedFile->saveImage($uploadedFile->file,$filename);

                $json=array(
                    'code'=>0,
                    'url'=>yii::$app->request->hostInfo.$filename,
                    'attachment'=>$filename
                );

            }

        }else{

            $json=array('code'=>1,'msg'=>"上传失败");
        }

        echo json_encode($json);yii::$app->end();
    }
}
