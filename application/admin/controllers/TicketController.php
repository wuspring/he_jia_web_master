<?php

namespace admin\controllers;

use app\search\UserSearch;
use common\models\GoodsCoupon;
use common\models\Provinces;
use common\models\SelectCity;
use common\models\TicketApply;
use common\models\TicketApplyGoods;
use common\models\User;
use DL\Project\Store;
use Yii;
use common\models\Ticket;
use admin\search\TicketSearch;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TicketController implements the CRUD actions for Ticket model.
 */
class TicketController extends BaseController
{
    protected $defaultModule;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();

        $this->defaultModule = Ticket::TYPE_JIA_ZHUANG;
    }

    /**
     * 展会列表 (管理员使用，仅用作区分导航)
     */
    public function actionManage()
    {
        return $this->actionList();
    }

    /**
     * 展会列表
     */
    public function actionList($isAdmin=false)
    {
        $user = Yii::$app->user->identity;

        $params = Yii::$app->request->queryParams;
        $active = isset($params['tab'])? $params['tab'] : $this->defaultModule;
        $sql = Ticket::find()->where([
            'citys' => $this->getCurrentCity(),
            'ca_type' => Ticket::CA_TYPE_ZHAN_HUI
        ]);
        $sql->andFilterWhere(['=', 'type', $active])->orderBy('status desc');
        $model = new ActiveDataProvider([
            'query' => $sql,
            'sort'=> ['defaultOrder'=>['id'=>SORT_DESC]]
        ]);
        switch ($user->assignment) {
            case User::ASSIGNMENT_HOU_TAI :
                    $tmp = 'lists';
                    $isManage = false;
                break;
            case User::ASSIGNMENT_GUAN_LI_YUAN :
                    $tmp = 'lists_admin';
                    $isManage = true;
                break;
        }

        return $this->render($tmp, [
            'active' => $active,
            'isManage' => $isManage,
            'dataProvider' => $model,
        ]);
    }

    /**
     * admin settiong
     *
     * Lists all Ticket models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->actionInfo(true);
    }

    /***
     * 展会信息
     */
    public function actionInfo($adminSecret=false)
    {
        $tmp = $adminSecret ? 'index' : 'info';

        $params = Yii::$app->request->queryParams;
        $params['id'] = isset($params['id']) ? $params['id'] : 0;
        $sql = Ticket::find()->where([
            'id' => $params['id']
        ]);
        $sort = 'create_time DESC';
        $model = $this->getPagedRows($sql, ['order' => $sort, 'rows' => 'list']);

        return $this->render($tmp, [
            'list' => $model['list'],
            'pagination' => $model['pages'],
        ]);
    }

    /**
     * Lists all Ticket models.
     * @return mixed
     */
    public function actionCaIndex()
    {
        $params = Yii::$app->request->queryParams;
        $active = isset($params['tab'])?$params['tab']: $this->defaultModule;
        $sql = Ticket::find()->where([
            'citys' => $this->getCurrentCity(),
            'status' => 1,
            'ca_type' => Ticket::CA_TYPE_CAI_GOU
        ]);
        $sql->andFilterWhere(['like', 'type', $active]);
        $sort = 'create_time DESC';
        $model = $this->getPagedRows($sql, ['order' => $sort, 'pageSize' => '2', 'rows' => 'list']);

        return $this->render('ca_index', [
            'active' => $active,
            'list' => $model['list'],
            'pagination' => $model['pages'],
        ]);
    }

    public function actionJoin()
    {
        if (Yii::$app->user->identity) {
            $key = Yii::$app->request->post('key', 0);

            $ticket = Ticket::findOne([
                'id' => $key,
                'status' => 1,
                'citys' => Yii::$app->user->identity->provinces_id
            ]);

            $ticket or apiSendError("暂未获得参加资格");

            $apply = TicketApply::findOne([
                'user_id' => Yii::$app->user->id,
                'ticket_id' => $ticket->id
            ]);

            $apply and apiSendError("请不要重复申请");

            $apply = new TicketApply();
            $apply->user_id = Yii::$app->user->id;
            $apply->ticket_id = $ticket->id;
            $apply->status = 0;
            $apply->create_time = date('Y-m-d H:i:s');

            $apply->create() and apiSendSuccess("参加成功");
        }

        apiSendError("网络异常，请稍后重试");
    }


    /**
     *  店铺查看参展店铺优惠劵
     *
     */
     public function actionJoinCoupon($ticketId){

         $joinCoupon=TicketApplyGoods::findAll(['ticket_id'=>$ticketId,'user_id'=>Yii::$app->user->id]);
         //查询绑定商品的优惠劵
         $bindGoodCoupon=ArrayHelper::getColumn($joinCoupon,'good_id');
         $bindGoodCoupon[]=-1;

         $sql=GoodsCoupon::find()->andFilterWhere(['user_id'=>Yii::$app->user->id,'is_del'=>0,'is_show'=>0])->andFilterWhere(['in','goods_id',$bindGoodCoupon]);
         $dataProvider = new ActiveDataProvider([
             'query' => $sql,
         ]);

         return $this->render('join_coupon',['dataProvider'=>$dataProvider]);
     }
    /**
     * Displays a single Ticket model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ticket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ticket();
        if ($model->load(Yii::$app->request->post())) {
            $model->create_time = date('Y-m-d H:i:s');
            $model->save();

            switch ($model->ca_type) {
                case Ticket::CA_TYPE_CAI_GOU :
                    $redirect = ['ca-index', 'tab' => $model->type];
                    break;
                default :
                    $redirect = ['manage', 'tab' => $model->type];
            }

            return $this->redirect($redirect);
        }
        $model->citys = $this->getCurrentCity();
        $model->ca_type = Yii::$app->request->get('ca_type', Ticket::CA_TYPE_ZHAN_HUI);
        $model->type = Yii::$app->request->get('type', Ticket::TYPE_JIA_ZHUANG);
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ticket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            switch ($model->ca_type) {
                case Ticket::CA_TYPE_CAI_GOU :
                    $redirect = ['ca-index', 'tab' => $model->type];
                    break;
                default :
                    $redirect = ['manage', 'tab' => $model->type];
            }

            return $this->redirect($redirect);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Ticket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ticket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ticket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ticket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * 分页方法
     * @param $query
     * @param array $config
     * @return array
     */
    public function getPagedRows($query,$config=[])
    {
        $countQuery = clone $query;

        $pages=new Pagination(['totalCount' => $countQuery->count()]);

        if(isset($config['pageSize']))
        {
            $pages->setPageSize($config['pageSize'],true);
        }

        $rows = $query->offset($pages->offset)->limit($pages->limit);
        if(isset($config['order']))
        {
            $rows = $rows->orderBy($config['order']);
        }
        $rows = $rows->all();

        $rowsLable='rows';
        $pagesLable='pages';

        if(isset($config['rows']))
        {
            $rowsLable=$config['rows'];
        }
        if(isset($config['pages']))
        {
            $pagesLable=$config['pages'];
        }

        $ret=[];
        $ret[$rowsLable]=$rows;
        $ret[$pagesLable]=$pages;

        return $ret;
    }


}
