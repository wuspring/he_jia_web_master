<?php

namespace admin\controllers;

use common\models\UploadForm;
use Yii;
use common\models\Newsclassify;
use app\search\NewsclassifySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\News;
use yii\web\UploadedFile;

/**
 * NewsclassifyController implements the CRUD actions for Newsclassify model.
 */
class NewsclassifyController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Newsclassify models.
     * @return mixed
     */
    public function actionIndex()
    {
        $cl = new Newsclassify();
        $tree = $cl->getParent(0);
        return $this->render('index', [
            'tree'=>$tree,
        ]);
    }

    /**
     * Displays a single Newsclassify model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Newsclassify model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($cid=0)
    {
        $model = new Newsclassify();

       $str = "<select id='newsclassify-fid' class='form-control' name='Newsclassify[fid]' > <option value='0'>顶级栏目</option>";
        $parent = Newsclassify::displayListsselt(0,$cid,$str);
        $model->createTime=date('Y-m-d H:i:s');
        $model->modifyTime=date('Y-m-d H:i:s');
        $model->isShow=1;
        $model->isRdm=0;
        $model->sort=99;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'parent'=>$parent,
            ]);
        }
    }

    /**
     * Updates an existing Newsclassify model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldimages = $model->icoImg;
        $str = "<select id='newsclassify-fid' class='form-control' name='Newsclassify[fid]' > <option value='0'>顶级栏目</option>";
        $parent = Newsclassify::displayListsselt(0,$model->fid,$str);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'parent'=>$parent,
            ]);
        }
    }

    /**
     * Deletes an existing Newsclassify model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $count = Newsclassify::find()->where(['fid' => $id])->count();
         $goodscount = News::find()->where(['cid' => $id])->count();
         if ($goodscount>0) {
              echo json_encode(array('status'=>0,'mg'=>'该分类下有文章存在!!'));
           Yii::$app->end();
         }
        $model =  $this->findModel($id);
        if ($count ==0) {
         

          if (!empty($model)) {
             $model->delete();
             echo json_encode(array('status'=>1,'mg'=>'删除成功!!'));
           Yii::$app->end();
          }else{
            echo json_encode(array('status'=>0,'mg'=>'删除的栏目不存在!!'));
           Yii::$app->end();
          }

        }

         echo json_encode(array('status'=>0,'mg'=>'删除失败!!,请检查是否存在子类!!'));
          Yii::$app->end();
    }

    /**
     * Finds the Newsclassify model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Newsclassify the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Newsclassify::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @throws \yii\base\ExitException
     */
    public function actionUpload(){
        header("Access-Control-Allow-Origin:*");
        $json=array('code'=>0,'error'=>'参数错误');
        if (Yii::$app->request->post()){
            $post=Yii::$app->request->post();
            $uploadedFile = new UploadForm();
            $uploadedFile->file = UploadedFile::getInstanceByName("file");
            if ($uploadedFile->file && $uploadedFile->validate()) {
                $pieces = explode("/",$uploadedFile->file->type);
                $filename=$uploadedFile->createImagePathMD5($pieces[1],'/uploads/icoImg/picture/',$uploadedFile->file->tempName);
            }
            if ($uploadedFile->file && $uploadedFile->validate()){
                $uploadedFile->saveImage($uploadedFile->file,$filename);
                $json=array(
                    'code'=>0,
                    'url'=>yii::$app->request->hostInfo.$filename,
                    'attachment'=>$filename
                );
            }else{
                $json=array('code'=>1,'msg'=>'图片保存失败');
            }
        }else{
            $json=array('code'=>1,'msg'=>"上传失败");
        }
        echo json_encode($json);yii::$app->end();
    }

}
