<?php

namespace admin\controllers;

use common\models\Goods;
use common\models\Provinces;
use common\models\Ticket;
use common\models\TicketApplyGoods;
use common\models\TicketApplyGoodsData;
use common\models\User;
use DL\Project\CityTicket;
use DL\Project\Store;
use DL\service\UrlService;
use Yii;
use common\models\TicketApply;
use app\search\TicketApplySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TicketApplyController implements the CRUD actions for TicketApply model.
 */
class TicketApplyController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TicketApply models.
     * @return mixed
     */
    public function actionIndex($id=0)
    {
        $active = Yii::$app->request->get('tab', Ticket::TYPE_JIE_HUN);
//        $currentCity = $this->getCurrentCity();
//
//        $ticket = CityTicket::init($currentCity->id)->getTicket($active);
//        $ticketIds = $ticket ? [$id] : [0];
        $ticketIds = [$id];
        $request = Yii::$app->request->queryParams;
        $request['TicketApplySearch']['ticketIds'] = $ticketIds;

        $searchModel = new TicketApplySearch();
        $dataProvider = $searchModel->search($request);

        $dataProvider->query->orderBy('status asc, id desc');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'active' => $active,
        ]);
    }

    /**
     * Displays a single TicketApply model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TicketApply model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TicketApply();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionJoin($ticketId=0)
    {
        Yii::$app->user->isGuest and stopFlow("您尚未登录");
        $user = Yii::$app->user->identity;

        $ticketApply = TicketApply::findOne([
            'ticket_id' => $ticketId,
            'user_id' => $user->id,
        ]);

        if (!$ticketApply) {
            $model = new TicketApply();
            $model->user_id = Yii::$app->user->id;
            $model->ticket_id = $ticketId;
            $model->good_ids = '[]';
            $model->status = 0;
        } else {
            $model = $ticketApply;
        }

        if ($model->load(Yii::$app->request->post())) {

            $orderGoodsAmount = TicketApplyGoodsData::find()->where([
                'ticket_id' => $model->ticket_id,
                'user_id' => $user->id,
                'type' => TicketApplyGoodsData::TYPE_ORDER,
            ])->count();
            $couponGoods = TicketApplyGoodsData::find()->where([
                'ticket_id' => $model->ticket_id,
                'user_id' => $user->id,
                'type' => TicketApplyGoodsData::TYPE_COUPON,
            ])->count();

         //   if (!$orderGoodsAmount and !$couponGoods) {
         //       stopFlow("请完善参加展会的商品信息", ['ticket/info', 'id' => $model->ticket_id]);
         //   }

            if ($model->create()) {
                return $this->redirect(UrlService::build(['ticket/info', 'id' => $model->ticket_id]));
            }

            stopFlow("网络异常", ['ticket/info', 'id' => $model->ticket_id]);
        } else {
            $orderGoods = TicketApplyGoodsData::findAll([
                'ticket_id' => $ticketId,
                'user_id' => $user->id,
                'type' => TicketApplyGoodsData::TYPE_ORDER,
            ]);

            $couponGoods = TicketApplyGoodsData::findAll([
                'ticket_id' => $ticketId,
                'user_id' => $user->id,
                'type' => TicketApplyGoodsData::TYPE_COUPON,
            ]);

            return $this->render('join_new', [
                'model' => $model,
                'orderGoods' => $orderGoods,
                'couponGoods' => $couponGoods,
            ]);
        }
    }

    /**
     * 商户报名
     * @return string|\yii\web\Response
     */
    public function actionJoin_delete()
    {
        Yii::$app->user->isGuest and stopFlow("您尚未登录");

        if ($this->checkManager()) {
            $currentCity = $this->getCurrentCity();
        } else {
            $user = User::findOne(Yii::$app->user->id);
            $currentCity = Provinces::findOne($user->provinces_id);
        }

        $active = Yii::$app->request->get('tab', Ticket::TYPE_JIE_HUN);
        $ticket = CityTicket::init($currentCity->id)->getTicket($active);

        $ticket or stopFlow("活动尚未开始，敬请期待");

        $ticketApply = TicketApply::findOne([
            'ticket_id' => $ticket->id,
            'user_id' => Yii::$app->user->id,
        ]);

        if (!$ticketApply) {
            $model = new TicketApply();
            $model->user_id = Yii::$app->user->id;
            $model->ticket_id = $ticket->id;
            $model->good_ids = '[]';
            $model->status = 0;
        } else {
            $model = $ticketApply;
        }

        $goods = Store::init()->goods(Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post()) && $model->create()) {
            return $this->redirect(['ticket/info', 'tab' => $active]);
        } else {
            $model->good_ids = json_decode($model->good_ids, true);

            return $this->render('join', [
                'model' => $model,
                'goods' => $goods
            ]);
        }
    }

    /**
     * Updates an existing TicketApply model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionCheck($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $ticketGoods = TicketApplyGoodsData::findAll([
                'ticket_id' => $model->ticket_id,
                'user_id' => $model->user_id,
            ]);

            switch ((int)$model->status) {
                // 同意
                case 1 :
                    if ($ticketGoods) {
                        arrayGroupsAction($ticketGoods, function ($ticketGoods) {
                            $ticketGoods->status = 1;
                            $ticketGoods->save();
                        });
                    }

                    break;
                    // 拒绝
                default :
                    if ($ticketGoods) {
                        arrayGroupsAction($ticketGoods, function ($ticketGoods) {
                            $ticketGoods->status = 0;
                            $ticketGoods->save();
                        });
                    }
            }
            $ticket = $model->getTicket();
            $store = User::findOne($model->user_id);
            $store->modifyTime = date('Y-m-d H:i:s');
            $store->save();
            return $this->redirect(['index', 'id' => $ticket->id]);
        } else {

            $orderGoods = TicketApplyGoodsData::findAll([
                'ticket_id' => $model->ticket_id,
                'user_id' => $model->user_id,
                'type' => TicketApplyGoodsData::TYPE_ORDER,
            ]);

            $couponGoods = TicketApplyGoodsData::findAll([
                'ticket_id' => $model->ticket_id,
                'user_id' => $model->user_id,
                'type' => TicketApplyGoodsData::TYPE_COUPON,
            ]);

            return $this->render('check', [
                'model' => $model,
                'orderGoods' => $orderGoods,
                'couponGoods' => $couponGoods,
            ]);
        }
    }

    /**
     * Deletes an existing TicketApply model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TicketApply model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TicketApply the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TicketApply::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
