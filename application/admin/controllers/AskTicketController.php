<?php

namespace admin\controllers;

use common\models\ConfigAskTicket;
use common\models\SelectCity;
use common\models\Ticket;
use DL\Project\AskTicket;
use DL\service\CacheService;
use Yii;
use common\models\Config;
use app\search\ConfigSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\UploadForm;

class AskTicketController extends ConfigController
{
    private $_title;
    private $_configKey;

    public function init()
    {
        parent::init();
    }

    /**
     * 家装索票
     *
     * @return string
     */
    public function actionJiaZhuang()
    {
        $this->_title = '家装展设置';
        $this->_configKey = Ticket::TYPE_JIA_ZHUANG;
        return $this->common();
    }

    /**
     * 家装索票
     *
     * @return string
     */
    public function actionJieHun()
    {
        $this->_title = '婚庆展设置';
        $this->_configKey = Ticket::TYPE_JIE_HUN;
        return $this->common();
    }

    /**
     * 孕婴索票
     *
     * @return string
     */
    public function actionYunYing()
    {
        $this->_title = '孕婴展设置';
        $this->_configKey = Ticket::TYPE_YUN_YING;
        return $this->common();
    }

    protected function common()
    {
        $currentCity = $this->getCurrentCity();


        $model = AskTicket::init()->get($currentCity->id, $this->_configKey);

        if (Yii::$app->request->post()) {
            $model->banner = isset($_POST['Config']['banner']) ? $_POST['Config']['banner'] : '';
            $model->index_top = isset($_POST['Config']['index_top']) ? $_POST['Config']['index_top'] : '';
            $model->index_footer = isset($_POST['Config']['index_footer']) ? $_POST['Config']['index_footer'] : '';
            $model->zhanhui_pic = isset($_POST['Config']['zhanhui_pic']) ? $_POST['Config']['zhanhui_pic'] : '';

            AskTicket::init()->get($currentCity->id, $this->_configKey, false, false)->setData($model);
        }

        return $this->render('common', [
            'title' => "<b>{$currentCity->cname}</b> |　{$this->_title}",
            'model' => $model,
            'configKey' => $this->_configKey
        ]);
    }
}
