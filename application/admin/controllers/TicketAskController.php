<?php

namespace admin\controllers;

use common\models\Provinces;
use common\models\Ticket;
use common\models\TicketInfo;
use Yii;
use common\models\TicketAsk;
use admin\search\TicketAskSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TicketAskController implements the CRUD actions for TicketAsk model.
 */
class TicketAskController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TicketAsk models.
     * @return mixed
     */
    public function actionIndex($caTypeInfo=false, $id)
    {
        $params=Yii::$app->request->queryParams;
        $active=isset($params['tab'])?$params['tab']:'JIE_HUN';

        // 区分 展会索票、 展览索票
//        $caType = $caTypeInfo ? Ticket::CA_TYPE_CAI_GOU : Ticket::CA_TYPE_ZHAN_HUI;
//        $ticketObj = Ticket::find()->where(['type'=>$active, 'ca_type' => $caType])->all();
        $ticketArr = [$id];
//         foreach ($ticketObj as $k=>$v){
//             $ticketArr[]=$v->id;
//         }
        $params['TicketAskSearch']['ticket_type']=$ticketArr;

        $city = $this->getCurrentCity();
        $params['TicketAskSearch']['provinces_id'] = $city->id;
        $searchModel = new TicketAskSearch();
        $dataProvider = $searchModel->search($params);

        //获取可以筛序的城市
         $tacket_infor=TicketInfo::find()->andFilterWhere(['type'=>$active])->all();
          $cityArr=[];
          foreach ($tacket_infor as $tk=>$tv){
                 $prov=Provinces::findOne($tv->provinces_id);
                 $cityArr[$prov->id]=$prov->cname;
          }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'active'=>$active,
            'cityArr'=>$cityArr,
            'caTypeInfo' => $caTypeInfo,
            'city'=>$city
        ]);
    }

    public function actionCaIndex()
    {
        return $this->actionIndex(true);
    }

    /**
     * Displays a single TicketAsk model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TicketAsk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TicketAsk();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TicketAsk model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index','id'=>$model->ticket_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TicketAsk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TicketAsk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TicketAsk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TicketAsk::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionLoader($caTypeInfo=false, $id)
    {
        $params=Yii::$app->request->queryParams;
        $active=isset($params['tab'])?$params['tab']:'JIE_HUN';

        // 区分 展会索票、 展览索票
//        $caType = $caTypeInfo ? Ticket::CA_TYPE_CAI_GOU : Ticket::CA_TYPE_ZHAN_HUI;
//        $ticketObj = Ticket::find()->where(['type'=>$active, 'ca_type' => $caType])->all();
        $ticketArr = [$id];
//         foreach ($ticketObj as $k=>$v){
//             $ticketArr[]=$v->id;
//         }
        $params['TicketAskSearch']['ticket_type']=$ticketArr;

        $city = $this->getCurrentCity();
        $params['TicketAskSearch']['provinces_id'] = $city->id;
        $searchModel = new TicketAskSearch();
        $dataProvider = $searchModel->search($params);

        //获取可以筛序的城市
        $tacket_infor=TicketInfo::find()->andFilterWhere(['type'=>$active])->all();
        $cityArr=[];
        foreach ($tacket_infor as $tk=>$tv){
            $prov=Provinces::findOne($tv->provinces_id);
            $cityArr[$prov->id]=$prov->cname;
        }

        $datas = $dataProvider->query->all();
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel
            ->getProperties()                                               //获得文件属性对象，给下文提供设置资源
            ->setCreator( "MaartenBalliauw")                         //设置文件的创建者
            ->setLastModifiedBy( "MaartenBalliauw")                  //设置最后修改者
            ->setTitle( "Office2007 XLSX Test Document" )            //设置标题
            ->setSubject( "Office2007 XLSX Test Document" )          //设置主题
            ->setDescription( "Test document for Office2007 XLSX, generated using PHP classes.") //设置备注
            ->setKeywords( "office 2007 openxmlphp")                 //设置标记
            ->setCategory( "Test resultfile");                       //设置类别

        $objPHPExcel->setActiveSheetIndex(0)//表头的信息
        ->setCellValue('A1', "姓名")
            ->setCellValue('B1', "城市")
            ->setCellValue('C1', "门票")
            ->setCellValue('D1', "手机号")
            ->setCellValue('E1', "地址")
            ->setCellValue('F1', "状态")
            ->setCellValue('G1', "数量")
            ->setCellValue('H1', "渠道")
            ->setCellValue('I1', "时间")
            ->setCellValue('J1', "快递号");
        $i=2;

        foreach ($datas as $key => $value) {

        $objPHPExcel->getActiveSheet()
            ->setCellValue( "A{$i}",  $value->name)
            ->setCellValue( "B{$i}",  $value->cityName->cname)
            ->setCellValue( "C{$i}",  $value->ticket->name)
            ->setCellValue( "D{$i}",  $value->mobile)
            ->setCellValue( "E{$i}",  $value->address)
            ->setCellValue( "F{$i}",  $value->statusInfo)
            ->setCellValue( "G{$i}",  $value->ticket_amount)
            ->setCellValue( "H{$i}",  $value->resource)
            ->setCellValue( "I{$i}",  $value->create_time)
            ->setCellValue( "J{$i}",  $value->shipping_code);
        $i++;
    }

        $files = RUNTIME_PATH .  '/ask-ticket' . createRandKey(16) . '.xlsx';
        $objActSheet = $objPHPExcel->getActiveSheet();

        $objActSheet->setTitle('索票信息');

        $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($files);
        header('Content-Type:application/vnd.ms-excel');
        header('Content-Disposition:attachment;filename="索票信息.xlsx"');
        header('Cache-Control:max-age=0');

        die(readfile($files));
    }
}
