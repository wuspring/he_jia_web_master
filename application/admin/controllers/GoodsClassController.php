<?php

namespace admin\controllers;

use common\models\FloorIndex;
use common\models\UploadForm;
use wap\models\FloorWap;
use Yii;
use common\models\GoodsClass;
use app\search\GoodsClassSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * GoodsClassController implements the CRUD actions for GoodsClass model.
 */
class GoodsClassController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all GoodsClass models.
     * @return mixed
     */
    public function actionIndex()
    {

        $cl = new GoodsClass();
        $tree = $cl->getParent(0);
        return $this->render('index', [
            'tree' => $tree,
        ]);
    }

    /**
     * Displays a single GoodsClass model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GoodsClass model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($cid = 0)
    {
        $model = new GoodsClass();

        $str = "<select id='goodsclass-fid' class='form-control' name='GoodsClass[fid]' > <option value='0'>顶级栏目</option>";
        $parent = GoodsClass::displayListsselt(0, $cid, $str);

        if ($model->load(Yii::$app->request->post())) {

            $uploadedFile = new UploadForm();
            $uploadedFile->file = UploadedFile::getInstance($model, 'iconImg');
            if ($uploadedFile->file && $uploadedFile->validate()) {
                $filename = $uploadedFile->createImagePathWithExtension($uploadedFile->file->extension, '/uploads/goodclass/images/');
                $model->iconImg = $filename;
            }
            if ($model->save()) {
                if ($uploadedFile->file && $uploadedFile->validate()) {
                    $uploadedFile->saveImage($uploadedFile->file, $filename);
                }
                return $this->redirect(['index']);
            }
            print_r($model->errors);exit;
        }

        return $this->render('create', [
            'model' => $model,
            'parent' => $parent,
        ]);

    }

    /**
     * Updates an existing GoodsClass model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldimages = $model->icoImg;

        $str = "<select id='goodsclass-fid' class='form-control' name='GoodsClass[fid]' > <option value='0'>顶级栏目</option>";
        $parent = GoodsClass::displayListsselt(0, $model->fid, $str);

        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'parent' => $parent,
            ]);
        }
    }

    public function actionSearchlist($val){

        $list=GoodsClass::find()->where(['like', 'name', $val])->all();

        $data=array('result'=>array(),'total'=>1);
        $data['result'][]=array(
            'id'=>'0',
            'text'=>"无",
            'name'=>"无"
        );
        foreach ($list as $key => $value) {
            $data['result'][]=array(
                'id'=>$value->id,
                'text'=>$value->name,
                'name'=>$value->name
            );
        }

        echo json_encode($data); yii::$app->end();
    }
    /**
     * Deletes an existing GoodsClass model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
         $model=$this->findModel($id);
         $model->is_del=1;
         if($model->save()){
             FloorIndex::deleteAll(['good_class_id'=>$id]);
         }
         return $this->redirect(['index']);
    }

    /**
     * Finds the GoodsClass model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return GoodsClass the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GoodsClass::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetlist($val){
        if (is_numeric($val)) {
            $list=GoodsClass::find()->where(['id'=> $val])->all();
        }else{
            $list=GoodsClass::find()->where(['like', 'name', $val])->all();
        }

        $data=array('result'=>array(),'total'=>1);
        $data['result'][]=array(
            'id'=>'0',
            'text'=>"无",
            'name'=>"无"
        );
        foreach ($list as $key => $value) {
            $data['result'][]=array(
                'id'=>$value->id,
                'text'=>$value->name,
                'name'=>$value->name
            );
        }

        echo json_encode($data); yii::$app->end();
    }
}
