<?php

namespace admin\controllers;

use Yii;
use common\models\Link;
use app\search\LinkSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\UploadForm;
/**
 * LinkController implements the CRUD actions for Link model.
 */
class LinkController extends BaseController
{
    public function behaviors()
    {
        return [
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'delete' => ['post'],
            //     ],
            // ],
        ];
    }

    /**
     * Lists all Link models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LinkSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Link model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Link model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Link();
        $model->type=1;
        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile = new UploadForm();
            $uploadedFile->file = UploadedFile::getInstance($model, 'imgval');
            if ($uploadedFile->file && $uploadedFile->validate()) {
                $filename=$uploadedFile->createImagePathWithExtension($uploadedFile->file->extension,'/uploads/link/');
                $model->imgval = $filename;
            }
            $model->createTime=date('Y-m-d H:i:s');
            $model->modifyTime=$model->createTime;  
            if ($model->save()) {
                  if ($uploadedFile->file && $uploadedFile->validate()){
                    $uploadedFile->saveImage($uploadedFile->file,$filename); 
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
            
        } 
            return $this->render('create', [
                'model' => $model,
            ]);
       
    } 

    /**
     * Updates an existing Link model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldimgval=$model->imgval;
        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile = new UploadForm();
            $uploadedFile->file = UploadedFile::getInstance($model, 'imgval');
            if ($uploadedFile->file && $uploadedFile->validate()) {
                $filename=$uploadedFile->createImagePathWithExtension($uploadedFile->file->extension,'/uploads/link/');
                $model->imgval = $filename;
                $uploadedFile->deleteImage($oldimgval);
            }else{
                $model->imgval = $oldimgval;
            }
       
            $model->modifyTime=date('Y-m-d H:i:s');  
            if ($model->save()) {
                if ($uploadedFile->file && $uploadedFile->validate()){
                    $uploadedFile->saveImage($uploadedFile->file,$filename); 
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
            
        } 
            return $this->render('update', [
                'model' => $model,
            ]);
       
    }

    /**
     * Deletes an existing Link model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Link model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Link the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Link::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
