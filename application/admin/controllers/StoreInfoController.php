<?php

namespace admin\controllers;

use common\models\Goods;
use common\models\GoodsClass;
use common\models\StoreGcScore;
use common\models\StoreShop;
use common\models\UploadForm;
use common\models\User;
use DL\Project\Store;
use Yii;
use common\models\StoreInfo;
use app\search\StoreInfoSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * StoreInfoController implements the CRUD actions for StoreInfo model.
 */
class StoreInfoController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * 列表
     * @param int $id
     * @return mixed
     */
    public function actionLists($id=0)
    {
        return $this->actionIndex($id);
    }

    /**
     * Lists all StoreInfo models.
     * @return mixed
     */
    public function actionIndex($id=0)
    {
        if (Yii::$app->request->isPost) {
            $storeInfo = Yii::$app->request->post('StoreInfo');
            if (isset($storeInfo['sid'])) {
                return $this->actionUpdate($storeInfo['sid']);
                die;
            }
        }

        $user = Yii::$app->user->identity;
        ($user->assignment != User::ASSIGNMENT_GUAN_LI_YUAN) and $id = Yii::$app->user->id;


        $storeUser = User::findOne([
            'id' => (int)$id,
            'assignment' => User::ASSIGNMENT_HOU_TAI
        ]);

        $storeUser or stopFlow("无访问权限");

        $store = StoreInfo::findOne([
            'user_id' => $storeUser->id
        ]);
        if ($store) {
            $store->order_goods_id = json_decode($store->order_goods_id, true);
            $store->store_coupon_id = json_decode($store->store_coupon_id, true);
        }

        $storeShops = StoreShop::findAll([
            'user_id' => $store->user_id
        ]);

        $joinTicket = Store::init()->checkJoinZH($storeUser->id);

        return $this->render('index', [
            'userInfo' => $storeUser,
            'store' => $store,
            'joinTicket' => $joinTicket,
            'storeShops' => $storeShops
        ]);
    }

    /**
     * 查看店铺信息
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionInfo($id=0)
    {
        return $this->actionIndex($id);
    }

    /**
     * Displays a single StoreInfo model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StoreInfo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StoreInfo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            User::updateAll(['modifyTime'=>date('Y-m-d H:i:s')],['id'=>Yii::$app->user->id]);
            return $this->redirect(['index', 'id' => $model->user_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StoreInfo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $userModel = User::findOne($model->user_id);

        $request = Yii::$app->request->post();
        $request['StoreInfo']['order_goods_id'] = isset($request['StoreInfo']['order_goods_id'])
            ? json_encode($request['StoreInfo']['order_goods_id']) : '[]';
        $request['StoreInfo']['store_coupon_id'] = isset($request['StoreInfo']['store_coupon_id'])
            ? json_encode($request['StoreInfo']['store_coupon_id']) : '[]';

        if (Yii::$app->request->isPost and $model->load($request) && $model->save()) {
            User::updateAll(['modifyTime'=>date('Y-m-d H:i:s')],['id'=>Yii::$app->user->id]);
             if ($userModel->load($request)){
                 $userModel->avatarTm=$this->imgTm($userModel->avatar);
                 $userModel->save();
             }

            return $this->redirect(['index', 'id' => $model->user_id]);
        } else {
            $model->order_goods_id = json_decode($model->order_goods_id, true);
            $model->store_coupon_id = json_decode($model->store_coupon_id, true);

            return $this->render('update', [
                'model' => $model,
                'userModel' => $userModel
            ]);
        }
    }

    /**
     * Deletes an existing StoreInfo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StoreInfo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return StoreInfo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StoreInfo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * 图片压缩
     */
    public function imgTm($avatar){

        if (empty($avatar)){
            return "";
        }

        $uploadedFile = new UploadForm();
        $size_src=getimagesize(Yii::$app->request->hostInfo.$avatar);

         $imgFomat=substr(strrchr($avatar, '.'), 1);
         $fileThumbnailName=$uploadedFile->createImagePathWithExtension($imgFomat,'/uploads/user/headPicThumbnail/');
        $w=$size_src['0'];
        $h=$size_src['1'];
        $max=160;
        if($w > $h){
            $w=$max;
            $h=$h*($max/$size_src['0']);
        }else{
            $h=$max;
            $w=$w*($max/$size_src['1']);
        }
        $w=number_format($w, 0);
        $h=number_format($h, 0);
        $uploadedFile->createThumbnail($avatar,$fileThumbnailName,$w,$h);
        return $fileThumbnailName;
    }

    public function actionUpload(){
        header("Access-Control-Allow-Origin:*");
        $json=array('state'=>0,'error'=>'参数错误');
        if (Yii::$app->request->post()){

            $post=Yii::$app->request->post();

            $uploadedFile = new UploadForm();

            $uploadedFile->file = UploadedFile::getInstanceByName("file");

            if ($uploadedFile->file && $uploadedFile->validate()) {

                $pieces = explode("/",$uploadedFile->file->type);

//                $filename=$uploadedFile->createImagePathWithExtension($uploadedFile->file->extension,'/uploads/goods/');
                $filename=$uploadedFile->createImagePathMD5($uploadedFile->file->extension,'/uploads/user/headPic/',$uploadedFile->file->tempName);
                $uploadedFile->saveImage($uploadedFile->file,$filename);

                $json=array(
                    'code'=>0,
                    'url'=>yii::$app->request->hostInfo.$filename,
                    'attachment'=>$filename,
                );

            }

        }else{

            $json=array('code'=>1,'msg'=>"上传失败");
        }

        echo json_encode($json);yii::$app->end();
    }


    public function actionGcs()
    {
        $storeId = Yii::$app->request->get('storeId', 0);

        $gcs = StoreGcScore::findAll([
            'user_id' => $storeId
        ]);

        $ds = [];
        $ngc = new GoodsClass();
        foreach ($gcs AS $gc) {
           $c = $ngc->getSonCategoryByDG($gc->gc_id);
            $ds = array_merge($ds, $c);
        }


        $gcsDatas = ArrayHelper::getColumn($ds, 'id');

        apiSendSuccess("success", $gcsDatas);
    }


}
