<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\IntergralLuckDraw */

$this->title ='详情';
$this->params['breadcrumbs'][] = ['label' => 'Intergral Luck Draws', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

   <div class="panel-body">
       <?= DetailView::widget([
           'model' => $model,
           'attributes' => [
               [
                   'attribute'=>'member_id',
                   'label'=>'会员手机号',
                   'value'=>$model->member->mobile,
               ],
               [
                   'attribute'=>'prize_terms',
                   'label'=>'第几期'
               ],
               [
                   'attribute'=>'draw_id',
                   'label'=>'获奖等级',
                   'value'=>$model->intergralTurntable->prize,
               ],
               [
                   'attribute'=>'create_time',
                   'label'=>'获奖时间',
               ],
           ],
       ]) ?>

   </div>

</div>
