<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\GoodsAttrSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Goods Attrs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goods-attr-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Goods Attr', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'group_id',
            'attr_name',
            'is_delete',
            'is_default',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
