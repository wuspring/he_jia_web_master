<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\BrandSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '品牌管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="panel-body">
        <?= Html::a('添加品牌', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'brand_name',
//            'brand_initial',
//            'brand_pic',
            array(
                'format' => 'raw',
                'attribute' => 'brand_pic',
                'label'=>'品牌Logo',
                'value'=>function($filterModel){
                    return Html::img($filterModel->brand_pic, ['width' =>30]);
                },
            ),
//            'brand_sort',
            // 'brand_recommend',
            // 'class_id',
            // 'show_type',

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'操作',
                'template' => '{update} {delete}',
                'buttons'=>[

                ],

            ],
        ],
    ]); ?>
    </div>
</div>
