<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HelpType */

$this->title = '添加种类';
$this->params['breadcrumbs'][] = ['label' => '帮助中心', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '分类信息', 'url' => ['help-type/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="panel">
   <div class="panel-heading">
       <span class="panel-title"><?= Html::encode($this->title) ?></span>
   </div>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
