<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Goods */

$this->title = $model->goods_name;
$this->params['breadcrumbs'][] = ['label' => 'Goods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$statusDic = [
    0 => '下架',
    1 => '正常',
    10 => '违规（禁售）',
];

function formatJsonImg ($jsonString) {
    $imgs = json_decode($jsonString, true);

    $result = '';
    if ($imgs) {
        foreach ($imgs AS $img) {
            $result .= sprintf('<img src="%s" alt="%s" >', $img['imgurl'], '相册');
        }
    }

    return $result;
}
?>
<div class="goods-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'goods_name',
            'goods_jingle',
            [
                'attribute' => 'gc_id',
                'label' => '商品分类',
                'value' => $model->gclass->name,
            ],
//            'use_attr',
            [
                'attribute' => 'brand_id',
                'label' => '品牌',
                'value' => $model->branddata->brand_name,
            ],
            'goods_price',
            'goods_marketprice',
//            'goods_promotion_price',
//            'goods_promotion_type',
            'goods_serial',
//            'goods_storage_alarm',
            'goods_click',
            'goods_salenum',
            'goods_collect',

//            'goods_spec:ntext',
            [
                'attribute' => 'goods_body',
                'label' => '商品详情	',
                'format' => 'raw',
            ],
//            'mobile_body:ntext',
            'goods_storage',
            [
                'attribute' => 'goods_image',
                'label' => '商品相册',
                'format' => 'raw',
                'value' => formatJsonImg($model->goods_image)
            ],
            [
                'attribute' => 'goods_state',
                'label' => '商品状态	',
                'format' => 'raw',
                'value' => isset($statusDic[$model->goods_state]) ?$statusDic[$model->goods_state]: '-'
            ],
//            'goods_addtime',
//            'goods_edittime',
//            'goods_vat',
//            'goods_commend',
            'evaluation_good_star',
            'evaluation_count',
//            'is_virtual',
//            'is_appoint',
//            'is_presell',
//            'have_gift',
            [
                'attribute' => 'isdelete',
                'label' => '是否删除',
                'value' => (int)$model->isdelete ? '是' : '否'
            ],
            [
                'attribute' => 'type',
                'label' => '商品类型',
                'value' => $model->typeInfo
            ],
            'integral',
        ],
    ]) ?>

</div>
