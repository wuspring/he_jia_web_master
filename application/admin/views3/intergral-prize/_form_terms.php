<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\IntergralPrize */
/* @var $form yii\widgets\ActiveForm */

$this->title = '编辑';
$this->params['breadcrumbs'][] = ['label' => '积分商城', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '积分奖品', 'url' => ['intergral-prize/index-list']];
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="<?php echo Yii::$app->request->hostInfo;?>/public/My97DatePicker/WdatePicker.js"></script>
<div class="panel">
    <div class="panel-heading">
        <div class="panel-title">
             增加新的积分活动
        </div>
    </div>

    <div class="panel-body">
        <div class="intergral-prize-form">

            <?php $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
                'fieldConfig' => [
                    'inputOptions' => ['class' => 'form-control'],
                    'labelOptions'=>['class'=>'col-sm-2 control-label'],
                    'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
            ]); ?>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10" style="text-align: right">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '添加') : Yii::t('app', '保存修改'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>

            <?= $form->field($model, 'prize_terms')->textInput() ?>


            <?= $form->field($model, 'start_time')->textInput(['readonly'=>'readonly'])->label('开始时间') ?>


            <?= $form->field($model, 'end_time')->textInput(['readonly'=>'readonly'])->label('结束时间') ?>
            <script>
                //点击出来日历
                $("#intergralprize-start_time").click(function(){
                    WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})
                });
                $("#intergralprize-end_time").click(function(){
                    WdatePicker({dateFmt:'yyyy-MM-dd'})
                });

            </script>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>


