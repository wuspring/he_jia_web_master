<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GoodsSku */

$this->title = 'Create Goods Sku';
$this->params['breadcrumbs'][] = ['label' => 'Goods Skus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goods-sku-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
