<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'orderid') ?>

    <?= $form->field($model, 'pay_sn') ?>

    <?= $form->field($model, 'buyer_id') ?>

    <?= $form->field($model, 'buyer_name') ?>

    <?= $form->field($model, 'goods_amount') ?>

    <?php // echo $form->field($model, 'order_amount') ?>

    <?php // echo $form->field($model, 'integral_amount') ?>

    <?php // echo $form->field($model, 'pd_amount') ?>

    <?php // echo $form->field($model, 'freight') ?>

    <?php // echo $form->field($model, 'evaluation_state') ?>

    <?php // echo $form->field($model, 'order_state') ?>

    <?php // echo $form->field($model, 'receiver') ?>

    <?php // echo $form->field($model, 'receiver_name') ?>

    <?php // echo $form->field($model, 'receiver_state') ?>

    <?php // echo $form->field($model, 'receiver_address') ?>

    <?php // echo $form->field($model, 'receiver_mobile') ?>

    <?php // echo $form->field($model, 'receiver_zip') ?>

    <?php // echo $form->field($model, 'shipping') ?>

    <?php // echo $form->field($model, 'shipping_code') ?>

    <?php // echo $form->field($model, 'deliveryTime') ?>

    <?php // echo $form->field($model, 'add_time') ?>

    <?php // echo $form->field($model, 'payment_time') ?>

    <?php // echo $form->field($model, 'finnshed_time') ?>

    <?php // echo $form->field($model, 'fallinto_state') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
