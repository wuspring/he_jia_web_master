                   <?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use \common\models\Order;

/* @var $this yii\web\View */
/* @var $searchModel app\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '订单列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<script language="javascript" type="text/javascript" src="<?php echo yii::$app->request->hostInfo;?>/public/My97DatePicker/WdatePicker.js"></script>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <style>
        .order-item {
            border: 1px solid transparent;
            margin-bottom: 1rem;
        }

        .order-item table {
            margin: 0;
        }

        .order-item:hover {
            border: 1px solid #3c8ee5;
        }

        .goods-item {
            margin-bottom: .75rem;
        }

        .goods-item:last-child {
            margin-bottom: 0;
        }

        .goods-pic {
            width: 5.5rem;
            height: 5.5rem;
            display: inline-block;
            background-color: #ddd;
            background-size: cover;
            background-position: center;
            margin-right: 1rem;
        }

        .goods-name {
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }

        .order-tab-1 {
            width: 40%;
        }

        .order-tab-2 {
            width: 10%;
            text-align: center;
        }

        .order-tab-3 {
            width: 10%;
            text-align: center;
        }

        .order-tab-4 {
            width: 20%;
            text-align: center;
        }

        .order-tab-5 {
            width: 10%;
            text-align: center;
        }

        .order-tab-6 {
            width: 10%;
            text-align: center;
        }
        .status-item.active {
            color: inherit;
        }
    </style>
    <div class="panel-body">
        <div class="mb-3 clearfix">
            <div class="p-4 bg-shaixuan">
                <form method="get">
                    <?php $_s = ['keyword', 'keyword_1', 'date_start', 'date_end', 'page', 'per-page'] ?>
                    <?php foreach ($_GET as $_gi => $_gv):if (in_array($_gi, $_s)) continue; ?>
                        <input type="hidden" name="<?= $_gi ?>" value="<?= $_gv ?>">
                    <?php endforeach; ?>


                    <div class="col-md-12">
                        <div class="mr-4">
                            <div class="form-group col-md-4">
                                <div class="col-5 col-md-4">
                                    <select class="form-control" name="keyword_1">
                                        <?php if (isset($_GET['keyword_1'])): ?>
                                            <option value="1" <?= $_GET['keyword_1'] == 1 ? "selected" : "" ?>>订单号</option>
                                            <option value="2" <?= $_GET['keyword_1'] == 2 ? "selected" : "" ?>>用户</option>
                                            <option value="3" <?= $_GET['keyword_1'] == 3 ? "selected" : "" ?>>收货人</option>
                                        <?php else:?>
                                            <option value="1">订单号</option>
                                            <option value="2">用户</option>
                                            <option value="3">收货人</option>
                                        <?php endif;?>
                                    </select>
                                </div>
                                <div class="col-7 col-md-8">
                                    <input class="form-control"
                                           name="keyword"
                                           autocomplete="off"
                                           value="<?= isset($_GET['keyword']) ? trim($_GET['keyword']) : null ?>">
                                </div>
                            </div>
                            <div class="form-group row col-md-8">
                                <div class="col-md-8">
                                    <div class="input-group col-md-12">
                                        <label class="col-md-2">下单时间：</label>
                                        <input class="form-inline col-md-3" id="date_start" name="date_start"
                                               autocomplete="off"
                                               value="<?= isset($_GET['date_start']) ? trim($_GET['date_start']) : '' ?>">
                                        <span class="text-center col-md-1" style="padding:0 4px">至</span>
                                        <input class="form-inline col-md-3" id="date_end" name="date_end"
                                               autocomplete="off"
                                               value="<?= isset($_GET['date_end']) ? trim($_GET['date_end']) : ''  ?>">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="mr-4">
                            <div class="form-group col-md-offset-1">
                                <button class="btn btn-primary mr-4">筛选</button>
                                <a class="btn btn-secondary export-btn"
                                   href="<?=Url::toRoute(['order/export','q'=>array_merge($_GET, ['reback'=>1])])?>" target="_blank">批量导出</a>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <table class="table table-bordered bg-white">
            <tr>
                <th class="order-tab-1">商品信息</th>
                <th class="order-tab-6">订单类型</th>
                <th class="order-tab-2">金额</th>
                <th class="order-tab-3">实际付款</th>
                <th class="order-tab-4">订单状态</th>
                <th class="order-tab-5">操作</th>
            </tr>
        </table>
        <?php foreach ($list as $order_item): ?>
            <div class="order-item" style="">
                <table class="table table-bordered bg-white">
                    <tr>
                        <td colspan="6">
                            <span class="mr-5"><?= $order_item['add_time'] ?></span>
                            <span class="mr-5">订单号：<?= $order_item['orderid'] ?></span>
                            <span>用户：<?= $order_item['buyer_name'] ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-tab-1">
                            <?php foreach ($order_item['goods_list'] as $goods_item): ?>
                                <div class="goods-item" flex="dir:left box:first">
                                    <div class="fs-0 col-md-2">
                                        <div class="goods-pic"
                                             style="background-image: url('<?= $goods_item['goods_pic'] ?>')"></div>
                                    </div>
                                    <div class="goods-info col-md-10">
                                        <div class="goods-name"><?= $goods_item['goods_name'] ?></div>
                                        <div class="fs-sm">
                                            规格：
                                            <span class="text-danger">
                                            <?php $attr_list = json_decode($goods_item['attr']); ?>
                                                <?php if (is_array($attr_list)):foreach ($attr_list as $attr): ?>
                                                    <span class="mr-3"><?= $attr->attr_group_name ?>
                                                        :<?= $attr->attr_name ?></span>
                                                <?php endforeach;;endif; ?>
                                        </span>
                                        </div>
                                        <div class="fs-sm">数量：
                                            <span class="text-danger"><?= $goods_item['goods_num'] ?></span>
                                        </div>
                                        <div class="fs-sm">小计：
                                            <span class="text-danger">￥<?= $goods_item['goods_price'] * $goods_item['goods_num'] ?>
                                                </span></div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </td>
                        <td class="order-tab-6">
                            <div><?= $order_item['type'] ?></div>
                        </td>
                        <td class="order-tab-2">
                            <div>运费：<?= $order_item['freight'] ?>元</div>
                        </td>
                        <td class="order-tab-3">
                            <div>总金额：<?= $order_item['order_amount'] ?>元（含运费）</div>
                            <div>总积分：<?= $order_item['integral_amount'] ?></div>
                        </td>
                        <td class="order-tab-4">
                            <div class="label label-success">
                                <?= $order_item['state_txt'] ?>
                            </div>

                            <?php if (in_array($order_item['order_state'], [Order::STATUS_SEND, Order::STATUS_RECEIVED, Order::STATUS_JUDGE])): ?>

                                <div>快递单号：<a href="https://www.baidu.com/s?wd=<?= $order_item['shipping_code'] ?>"
                                             target="_blank"><?= $order_item['shipping_code'] ?></a></div>
                                <div>快递公司：<?= isset($express[$order_item['shipping']]) ? $express[$order_item['shipping']] : $order_item['shipping'] ?></div>

                            <?php endif; ?>

                        </td>
                        <td class="order-tab-5">
                            <a class="btn btn-sm btn-primary"
                               href="<?= Url::toRoute(['order/view', 'id' => $order_item['orderid']]) ?>">详情</a>
                            <?php if ($order_item['order_state'] == Order::STATUS_REFUSE): ?>
                                <a class="btn btn-sm btn-primary fahuo" href="javascript:"
                                   data-id="<?= $order_item['orderid'] ?>">审核</a>
                            <?php endif; ?>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div>
                                <span class="mr-3">收货人：<?= $order_item['receiver_name'] ?></span>
                                <span class="mr-3">电话：<?= $order_item['receiver_mobile'] ?></span>
                                <span>地址：<?= $order_item['receiver_address'] ?></span>
                            </div>

                        </td>
                    </tr>
                </table>
            </div>
        <?php endforeach; ?>
        <div class="text-center">
            <?php if (!empty($pagination)): ?>
                <?= LinkPager::widget(['pagination' => $pagination,]) ?>
            <?php endif; ?>
            <div class="text-muted"><?= $pagination->totalCount ?>条数据</div>
        </div>
    </div>

</div>

                   <div id="fahuoModal" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
                       <div class="modal-dialog">
                           <div class="modal-content">
                               <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                   <h4 class="modal-title" id="myModalLabel">审核退款请求</h4>
                               </div>
                               <div class="modal-body">
                                   <input type="hidden" value="" id="ordercode">
                                   <p>审核：</p>
                                   <select class="form-control" id="express_id">
                                           <option value="2">同意</option>
                                           <option value="3">拒绝</option>
                                   </select>
                               </div>
                               <div class="modal-footer">
                                   <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                                   <button type="button" class="btn btn-primary sendGoods">审核</button>
                               </div>
                           </div>
                       </div>
                   </div>
<script>
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    init.push(function () {

        $(document).on("click",".fahuo",function () {
            $("#ordercode").val($(this).attr('data-id'));

            $("#fahuoModal").modal();
        });

        $(document).on('click','.sendGoods',function () {
            var pbody = $(this).parents('.modal-content');
            var order_id = pbody.find('#ordercode').val();
            var express_id = pbody.find('#express_id').val();
            var express_no = pbody.find('#express_no').val();

            $.post('<?php echo Url::toRoute(['order/check-reback'])?>',{order_id:order_id,express_id:express_id},function (res) {
                if (res.status==1) {
                    location.reload();
                    alert(res.msg);
                } else {
                    alert(res.msg);
                }
            },'json');
        });

        $("#date_start").click(function() {
            WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d'})
        });
        $("#date_end").click(function() {
            WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d',minDate:'#F{$dp.$D(\'date_start\')}'})
        });

    });

</script>