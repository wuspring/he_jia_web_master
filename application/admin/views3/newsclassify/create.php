<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Newsclassify */

$this->title = '添加资讯分类';
$this->params['breadcrumbs'][] = ['label' => '资讯管理', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '资讯分类', 'url' => ['newsclassify/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

   	<div class="panel-heading">
		<span class="panel-title"><?= Html::encode($this->title) ?></span>
	</div>

    <?= $this->render('_form', [
        'model' => $model,
        'parent'=>$parent,
    ]) ?>

</div>
