<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MTicketConfig */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel-body form-controls-demo">
<div class="mticket-config-form">

    <?php $form = ActiveForm::begin(); ?>

    <input name="_csrf" type="hidden" id="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
    <?= $form->field($model, 'ticket_id', ['options' => ['style' => 'display:none']])->textInput(['value' => $ticketId]) ?>


    <div data-id="tab" data-val="zh">
        <div class="form-group" style="text-align: right;">
            <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
        </div>
        <?= $form->field($model, 'colors', ['options' => ['style' => 'width:50%;']])->textInput(['maxlength' => true])->label('背景色') ?>
        <?= $form->field($model, 'banners')->textInput()->label('Banner图')?>
        <?= $form->field($model, 'top_guiders')->textInput(['data-prefix' => 'tGuiders_'])->label('顶部导航') ?>
        <?= $form->field($model, 'guiders')->textInput(['data-prefix' => 'guider_'])->label('底部导航') ?>
        <?= $form->field($model, 'ad_1')->textInput(['data-prefix' => 'ad1_'])->label('广告位1') ?>
        <?= $form->field($model, 'ad_2')->textInput(['data-prefix' => 'ad2_'])->label('广告位2') ?>
        <?= $form->field($model, 'ad_3')->textInput(['data-prefix' => 'ad3_'])->label('广告位3') ?>
        <?= $form->field($model, 'ad_4')->textInput(['data-prefix' => 'ad4_'])->label('广告位4') ?>
        <?= $form->field($model, 'ad_ticket_address')->textInput(['data-prefix' => 'adTa_'])->label('索票地址页广告位') ?>

    </div>
    <div data-id="tab" data-val="sp" style="display: none">
        <div class="form-group" style="text-align: right;">
            <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
        </div>
        <?= $form->field($model, 'spbg_color', ['options' => ['style' => 'width:50%;']])->textInput()->label('索票背景色') ?>
        <?= $form->field($model, 'spbanners')->textInput()->label('自助索票Banner图')?>
        <?= $form->field($model, 'star')->textInput(['data-prefix' => 'star_'])->label('展会亮点') ?>
        <?= $form->field($model, 'spbg_1')->textInput()->label('自助索票背景图1') ?>
        <?= $form->field($model, 'spbg_2')->textInput()->label('自助索票背景图2') ?>
        <?= $form->field($model, 'spbg_3')->textInput()->label('自助索票背景图3') ?>
        <?= $form->field($model, 'hot_pics')->textInput()->label('展会热图') ?>
        <?= $form->field($model, 'guider_pic')->textInput()->label('路线导航图') ?>

        <?= $form->field($model, 'spad_1')->textInput(['data-prefix' => 'spad1_'])->label('自助索票广告图') ?>

        <?= $form->field($model, 'ico_limit')->textInput(['maxlength' => true,'placeholder' => '免费索票页合作品牌展示数量(0:不限制)'])->label('参展商展示组数量') ?>

    </div>


    <?= $form->field($model, 'create_time', ['options' => ['style' => 'display:none']])->textInput() ?>





    <?php ActiveForm::end(); ?>
</div>
</div>
<script src="/public/js/base_upload.js"></script>
<script src="/public/js/grids/grid_upload.js"></script>
<script src="/public/js/grids/grid_upload_ico.js"></script>
<script src="/public/js/grids/grid_upload_expand.js"></script>
<script src="/public/js/upload-hooks/preview.js"></script>

<script>
    var uploadPath = "<?= DL_DEBUG ? \DL\service\UrlService::build(['/upload/img', 'more' => 1], 'index.php') : '/upload/img?more=true'; ?>";
    var MORE_FILES = true;

    GridUpload($('#mticketconfig-banners'));
    GridUpload($('#mticketconfig-spbanners'));
    GridUpload($('#mticketconfig-ad_1'));
    GridUpload($('#mticketconfig-ad_2'));
    GridUpload($('#mticketconfig-ad_3'));
    GridUpload($('#mticketconfig-ad_4'));
    GridUpload($('#mticketconfig-spad_1'));
    GridUpload($('#mticketconfig-ad_ticket_address'));
    GridUploadIco($('#mticketconfig-top_guiders'));
    GridUploadIco($('#mticketconfig-guiders'));

    GridUploadExpand($('#mticketconfig-star'), true, {title3 : '标题'});

    previewCtrl([
        {category : 'mticketconfig', id : 'hot_pics', type : 'hot_pics'},
        {category : 'mticketconfig', id : 'spbg_1', type : 'spbg_1'},
        {category : 'mticketconfig', id : 'spbg_2', type : 'spbg_2'},
        {category : 'mticketconfig', id : 'spbg_3', type : 'spbg_3'},
        {category : 'mticketconfig', id : 'guider_pic', type : 'guider_pic'},
    ]);
</script>