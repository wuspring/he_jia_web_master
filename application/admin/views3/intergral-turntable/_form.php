<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\IntergralTurntable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="intergral-turntable-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
        'fieldConfig' => [
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions'=>['class'=>'col-sm-2 control-label'],
            'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
    ]); ?>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10" style="text-align: right">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '添加') : Yii::t('app', '保存修改'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>


    <?= $form->field($model, 'angle')->textInput(['readonly'=>'readonly']) ?>

    <?= $form->field($model, 'prize')->textInput(['maxlength' => true,'readonly'=>'readonly']) ?>

    <?= $form->field($model, 'v')->textInput(['type'=>'number']) ?>

    <?php ActiveForm::end(); ?>

</div>
