<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\IntergralTurntable */

$this->title = '编辑奖项';
$this->params['breadcrumbs'][] = ['label' => '积分商城', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '抽奖转盘', 'url' => ['intergral-turntable/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <div class="panel-title">
            <?= Html::encode($this->title) ?>
        </div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
