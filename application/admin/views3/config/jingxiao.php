<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\web\Request;
/* @var $this yii\web\View */
/* @var $model app\Models\Config */

$this->title = "经销商分成设置"

?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title">经销商分成设置</span>
    </div>
    <div class="panel-body">

        <?php $form = ActiveForm::begin([
            'options' => ['class' => 'form-horizontal'],]); ?>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9" style="text-align: right">
                <button type="submit" class="btn btn-primary">编辑</button>
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">分销状态</label>
            <div class="col-sm-9">
                <input type="checkbox" id="state" name="state" <?php if($model->state =='on')  :?> checked="checked" <?php endif;?> />
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">分成方式</label>
            <div class="col-sm-9">
                <label><input type="radio" name="fashion" value="1" <?php if($model->fashion ==1) :?> checked="checked" <?php endif;?>><span class="lbl">按金额分成</span></label>
                &nbsp;&nbsp;&nbsp;
                <label><input type="radio" name="fashion" value="2" <?php if($model->fashion ==2) :?> checked="checked" <?php endif;?>><span class="lbl">按百分比分成</span></label>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">分成设置</span>
            </div>
            <div class="panel-body">
                <table class="table table-bordered" id="fenxiao">
                    <thead>
                    <tr>
                        <th>分成级别</th>
                        <th>金额分成比例</th>
                        <th>比例分成比例(100%)</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $step = [
                                '省',
                                '市',
                                '区'
                        ];
                    foreach ($model->lists as $key => $value): ?>
                        <?php if ($key < 3) : ?>
                        <tr>
                            <td><?php echo $step[$key];?></td>
                            <td class="fx-money" data-id="<?php echo $key;?>"><span><?php echo $value->money;?></span></td>
                            <td class="fx-percent" data-id="<?php echo $key;?>"><span><?php echo $value->percent;?></span></td>
                            <td><a href="Javascript:void(0);" data-id="<?php echo $key;?>" class="fa fa-bitbucket"></a></td>
                        </tr>
                    <?php endif; ?>
                    <?php endforeach ?>
                    <tr>
                        <td>#</td>
                        <td><input id="p-money"/></td>
                        <td><input id="p-percent"   onafterpaste="this.value=this.value.replace(/\D/g,'')" ></td>
                        <td><a href="Javascript:void(0);" class="fa fa-plus"></a></td>
                    </tr>
                    </tbody>
                </table>
                <input type="hidden" value="<?php echo Yii::$app->getRequest()->getCsrfToken(); ?>" name="YII_CSRF_TOKEN" id="_csrf"/>
            </div>
        </div>
    </div>
</div>
<!-- Javascript -->
<script>
    init.push(function () {
        // Colors
        var _csrf = '<?php echo Yii::$app->getRequest()->getCsrfToken(); ?>';
        $('#state').switcher();
        $('#cash').switcher();
        $('#auditing').switcher();
        $("#fenxiao .fa-plus").on('click',function(){
            var money =$("#p-money").val();
            var percent=$("#p-percent").val();
            $.post("<?php echo Url::toRoute('/config/jx-fencheng');?>",{_csrf:_csrf,act:'add',money:money,percent:percent},function(data){
                fx_tab(data);
            },'json');
        });
        $(".fx-money").on('click',function(){
            $(this).removeClass("fx-money");
            var old = $(this).find('span').html();
            var inputhtml = "<input class='fx-input-money' value='"+$.trim(old)+"'/>";
            $(this).html(inputhtml);
        });
        $(".fx-percent").on('click',function(){
            $(this).removeClass("fx-percent");
            var old = $(this).find('span').html();
            var inputhtml = "<input class='fx-input-percent'  value='"+$.trim(old)+"'/>";
            $(this).html(inputhtml);
        });
        $(".fx-input-percent").on('focusout',function(){
            var old = $(this).parent();

            var val = $(this).val();
            var id = old.attr("data-id");
            $.post("<?php echo Url::toRoute('/config/jx-fencheng');?>",{_csrf:_csrf,act:'update',type:'percent',val:val,id:id},function(data){
                var fxhtml	="<span>"+data+"</span>"
                $(old).html(fxhtml);
                old.addClass("fx-percent");
            });
        });
        $(".fx-input-money").on('focusout',function(){
            var old = $(this).parent();

            var val = $(this).val();
            var id = old.attr("data-id");
            $.post("<?php echo Url::toRoute('/config/jx-fencheng');?>",{_csrf:_csrf,act:'update',type:'money',val:val,id:id},function(data){
                var fxhtml	="<span>"+data+"</span>"
                $(old).html(fxhtml);
                old.addClass("fx-money");
            });

        });
        $("#fenxiao .fa-bitbucket").on('click',function(){
            var id = $(this).attr('data-id');
            $.post("<?php echo Url::toRoute('/config/jx-fencheng');?>",{_csrf:_csrf,act:'del',id:id},function(data){
                fx_tab(data);
            },'json');
        });

        $("#p-money,#standard,.fx-input-percent,.fx-input-mmoney").on('keyup',function(){
            $(this).val($(this).val().replace(/\D/g,''));
        });
        function fx_tab(data){
            var f_html ="";
            $(data).each(function(key,val){
                f_html += "	<tr>";
                f_html += "<td>"+(key+1)+"</td>";
                f_html+="<td class='fx-money' data-id='"+key+"'>"+val.money+"</td>";
                f_html+="<td class='fx-percent' data-id='"+key+"'>"+val.percent+"</td>";
                f_html+="<td><a href='Javascript:void(0);' data-id='"+key+"' class='fa fa-bitbucket'></a></td>";
                f_html+= "</tr>";
            });
            f_html+="<tr><td>#</td><td><input id='p-money'/></td><td><input id='p-percent' /></td><td><a href='Javascript:void(0);' class='fa fa-plus'></a></td></tr>";
            $("#fenxiao tbody").html(f_html);
        }
    });
</script>
<!-- / Javascript -->