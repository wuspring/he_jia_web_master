<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Config */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="config-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
    </div>

    <?= $form->field($model, 'cKey')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cValue')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'createTime')->textInput() ?>

    <?= $form->field($model, 'modifyTime')->textInput() ?>

    <?php ActiveForm::end(); ?>

</div>
