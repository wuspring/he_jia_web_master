<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\Request;
/* @var $this yii\web\View */
/* @var $model app\Models\Config */

$this->title = "积分商城设置";
$this->params['breadcrumbs'][] = ['label' => '积分商城', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="<?php echo yii::$app->request->hostInfo; ?>/public/ueditor/ueditor.config.js"></script>
<script src="<?php echo yii::$app->request->hostInfo; ?>/public/ueditor/ueditor.all.min.js"></script>
<script src="<?php echo yii::$app->request->hostInfo; ?>/public/ueditor/lang/zh-cn/zh-cn.js"></script>
<!-- 5. $JQUERY_VALIDATION =========================================================================

				jQuery Validation
-->
				<!-- Javascript -->
				<script>
					init.push(function () {
						$("#jq-validation-phone").mask("(999) 999-9999");
						$('#jq-validation-select2').select2({ allowClear: true, placeholder: 'Select a country...' }).change(function(){
							$(this).valid();
						});
						$('#jq-validation-select2-multi').select2({ placeholder: 'Select gear...' }).change(function(){
							$(this).valid();
						});

						// Add phone validator
						$.validator.addMethod(
							"phone_format",
							function(value, element) {
								var check = false;
								return this.optional(element) || /^\(\d{3}\)[ ]\d{3}\-\d{4}$/.test(value);
							},
							"Invalid phone number."
						);

						// Setup validation
						$("#jq-validation-form").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: false,
							rules: {
								'jq-validation-email': {
								  required: true,
								  email: true
								},
								'jq-validation-password': {
									required: true,
									minlength: 6,
									maxlength: 20
								},
								'jq-validation-password-confirmation': {
									required: true,
									minlength: 6,
									equalTo: "#jq-validation-password"
								},
								'jq-validation-required': {
									required: true
								},
								'jq-validation-url': {
									required: true,
									url: true
								},
								'jq-validation-phone': {
									required: true,
									phone_format: true
								},
								'jq-validation-select': {
									required: true
								},
								'jq-validation-multiselect': {
									required: true,
									minlength: 2
								},
								'jq-validation-select2': {
									required: true
								},
								'jq-validation-select2-multi': {
									required: true,
									minlength: 2
								},
								'jq-validation-text': {
									required: true
								},
								'jq-validation-simple-error': {
									required: true
								},
								'jq-validation-dark-error': {
									required: true
								},
								'jq-validation-radios': {
									required: true
								},
								'jq-validation-checkbox1': {
									require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
								},
								'jq-validation-checkbox2': {
									require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
								},
								'jq-validation-policy': {
									required: true
								}
							},
							messages: {
								'jq-validation-policy': 'You must check it!'
							}
						});
					});
				</script>
				<!-- / Javascript -->

<style>
    .alert_window {
        min-width: 600px;
        width: 50%;
        height: 150px;
        padding-top: 30px;
        z-index: 100;
        position: absolute;
        margin: 0 auto;
        top: 30%;
        left: 25%;
        background: #fff;
    }
    .shadow {
        width: 100%;
        height: 100%;
        z-index: 99;
        background: #9e9e9e;
        position: absolute;
        top: 0;
        left: 0;
    }
    .hide {
        display: none;
    }
    #int-int_rules {
        margin-top: 20px;
        padding: 0;
        margin: 20px 0;
        width: 100%;
        height: auto;
        border: none;
    }
    #int-lottery_rules {
        margin-top: 20px;
        padding: 0;
        margin: 20px 0;
        width: 100%;
        height: auto;
        border: none;
    }
</style>
<div data-bind="shadow" class="shadow hide"></div>

<?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],]); ?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title">积分商城设置</span>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9" style="text-align: right">
                <button type="submit" class="btn btn-primary">保存</button>
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">导航图片</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="int-int_banner" name="int_banner" value='<?=$model->int_banner;?>' placeholder="首次登录欢迎语句">
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">积分筛选设置</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="int-int_score_filter" name="int_score_filter" value='<?=$model->int_score_filter;?>' placeholder="积分筛选设置">
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">夺宝规则</label>
            <div class="col-sm-9">
                <textarea type="text" class="form-control" id="int-int_rules" name="int_rules" placeholder="夺宝规则"><?=$model->int_rules;?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">积分抽奖规则</label>
            <div class="col-sm-9">
                <textarea type="text" class="form-control" id="int-lottery_rules" name="lottery_rules" placeholder="积分抽奖规则"><?=isset($model->lottery_rules)?$model->lottery_rules:'';?></textarea>
            </div>
        </div>

    </div>
</div>


<?php ActiveForm::end(); ?>
<div data-id="build" style="display:none;"></div>
<script src="/public/js/jquery.min.js"></script>
<script src="/public/js/base_upload.js"></script>
<script src="/public/js/upload-hooks/preview.js"></script>
<script src="/public/js/grids/grid_input.js"></script>
<script>
    var uploadPath = '/upload/img?more=true',
        MORE_FILES = true,
        label_1 = '起始积分', label_2 = '截止积分';
    $(function() {
        config = [
            {category: 'int', id: 'int_banner', type: 'int_banner'},
        ];
        previewCtrl(config);

        GridInput($('#int-int_score_filter'));
    });

    init.push(function () {
        var ue = UE.getEditor('int-int_rules', {
            'zIndex': 9,
        });
        var ue2 = UE.getEditor('int-lottery_rules', {
            'zIndex': 9,
        });
    })
</script>
