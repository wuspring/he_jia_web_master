<?php

use yii\helpers\Html;
use yii\grid\GridView;
use DL\service\UrlService;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '后台管理';
$this->params['breadcrumbs'][] = ['label' => '用户管理', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = $this->title;

$findContent=['username'=>'账号','nickname'=>'商户名称'];
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }

    .btn1 {
        font-size: 6px;
        padding: 3px 5px;
        margin-left: 15px;
    }
</style>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <ul class="nav nav-tabs dl_tab">
        <li class="col-lg-2 tab_title <?= $type == User::ASSIGNMENT_HOU_TAI ? 'active' : ''; ?>"><a  href="<?=UrlService::build(['user/index','type'=>'store']);?>">商铺管理</a></li>
        <li class="col-lg-2 tab_title <?= $type == User::ASSIGNMENT_GUAN_LI_YUAN ? 'active' : ''; ?>"><a  href="<?=UrlService::build(['user/index','type'=>'per']);?>">人员列表</a></li>
    </ul>

    <div class="panel-body">
        <?php if ($type==User::ASSIGNMENT_HOU_TAI):?>
        <div class="row" style="margin-left: 18px">
            <?= Html::a('添加店铺', ['create','type'=>$type], ['class' => 'btn btn-success']) ?>
        </div>

        <div class="row" style="margin-top: 20px">
            <form method="get">
                <input type="hidden" name="r" value="user/index">
                <input type="hidden" name="type" value="store">
                <div class="form-group col-md-12">
                    <div class="col-md-2">
                        <select class="form-control col-md-6" name="content" style="display: inline-block;">
                            <option value="">请选择</option>
                            <?php
                            foreach ($findContent AS $status => $name) {
                                echo "<option value='{$status}'" . ($filterStatus == $status ? 'selected' : '') . " >{$name}</option>";
                            };
                            ?>
                        </select>
                    </div>


                    <?php $userStatuses = [
                    	'1' => '正常',
                    	'0' => '禁用'
                    ];

                    ?>
                     <div class="col-md-1" style="width: 130px;">
                        <select class="form-control col-md-6" name="user_status" style="display: inline-block;">
                        	<optgroup label="用户状态">
                            <?php
                            foreach ($userStatuses AS $status => $name) {
                                echo "<option value='{$status}'" . ($userStatus == $status ? 'selected' : '') . " >{$name}</option>";
                            };
                            ?>
                        	</optgroup>
                        </select>
                    </div>


                    <div class="col-md-3">
                        <input type="text" class="form-control" name="keyword"  value="<?=($keyword)?$keyword:''?>" placeholder="请输入">
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-primary">筛选</button>
                    </div>
                </div>
            </form>
        </div>

        <?php else:?>
            <?= Html::a('添加管理', ['create','type'=>$type], ['class' => 'btn btn-success']) ?>
        <?php endif;?>
    </div>

    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
             ['class' => 'yii\grid\SerialColumn'],

//            'id',

            [
                'attribute' => 'username',
                'label' => '登录账户',
            ],
            [
                'attribute' => 'nickname',
                'label' =>($type==User::ASSIGNMENT_HOU_TAI)?'商户名称':'管理员名称',
            ],
            [
                'attribute' => 'id',
                'label' => '商户ID',
            ],
            array(
                'format' => 'raw',
                'attribute' => 'avatarTm',
                'label' =>($type==User::ASSIGNMENT_HOU_TAI)?'商家Logo':'管理员Logo',
                'value'=>function($filterModel){
                    return Html::img(yii::$app->request->hostInfo.$filterModel->avatarTm, ['width' =>30]);
                },
            ),
            // 'role',
//            'sex',
            // 'birthday',
            [
                'attribute' => '状态',
                'label' => '状态',
                'value' => function ($model) {
                    return $model->statusInfo;
                }
            ],
            // 'avatarTm',
            // 'avatar',
            // 'email:email',
            // 'createTime',
            // 'modifyTime',
//             'last_visit',
            // 'userType',
            // 'authkey',
            // 'accessToken',
            // 'remainder',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => call_user_func(function($info, $type) {
                    switch ($type) {
                        case User::ASSIGNMENT_GUAN_LI_YUAN :
                        return '{update}';
                        case User::ASSIGNMENT_HOU_TAI :
                            return '{setting} {update}';
                    }
                }, $this, $type),
                'buttons' => [
                    'setting' => function ($url, $model, $key) {
                        $url = UrlService::build(['store-info/info', 'id' => $model->id]);
                        return Html::a('查看', $url, ['class'=> 'btn btn-success btn1', 'title' => '查看信息']);
                    },
                    'update' => function ($url, $model, $key) {
                        $url = UrlService::build(['user/update', 'id' => $model->id]);

                       // if($type==User::ASSIGNMENT_GUAN_LI_YUAN){
                        	 $url1 = UrlService::build(['goods/index', 'shopid' => $model->id]);
								return Html::a('编辑', $url, ['class'=> 'btn btn-primary btn1', 'title' => '编辑信息']).' '. Html::a('商品列表', $url1, ['class'=> 'btn btn-primary btn1', 'title' => '商品列表']);
                       /// }else{
                        	return Html::a('编辑', $url, ['class'=> 'btn btn-primary btn1', 'title' => '编辑信息']);
                       // }
                        
                    },
                ]
            ],
        ],
    ]); ?>
    </div>
</div>
