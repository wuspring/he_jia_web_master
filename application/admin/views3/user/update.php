<?php

use common\models\User;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = '编辑';
$this->params['breadcrumbs'][] = ['label' => '用户管理', 'url' => ['user/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

   	<div class="panel-heading">
		<span class="panel-title"><?= Html::encode($this->title) ?></span>
	</div>

    <?php if ($type==User::ASSIGNMENT_HOU_TAI):?>
        <?= $this->render('_form', [
            'model' => $model,
            'type'=>$type
        ]) ?>
    <?php else:?>
        <?= $this->render('_form_admin', [
            'model' => $model,
            'type'=>$type
        ]) ?>
    <?php endif?>

</div>