<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\NavsWapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '首页导航';
$this->params['breadcrumbs'][] = ['label' => '手机端', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <div class="panel-body">
        <p>
            <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>
        </p>
        <p>
            <?= Html::a('添加导航', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            array(
                'attribute' => 'provinces_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getCityName();
                }
            ),
//            'type',
            'name',
            array(
                'attribute' => 'icon',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img($model->icon,['width'=>50]);
                }
            ),
            // 'url:url',
            array(
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->status==1?'正常':'隐藏';
                }
            ),
             'sort',
             'create_time',
            // 'modify_time',
            [
                    'class' => 'yii\grid\ActionColumn',
                'header'=>'操作',
                'template'=>'{update} {delete}',
                'buttons'=>[
                    'view'=>function($url, $model, $key){
                        return  Html::a('查看', $url, ['title' => '查看','data-toggle'=>'modal','class'=>'a_margin']) ;
                    },
                    'update' => function ($url, $model, $key) {
                        return  Html::a('更新', $url, ['title' => '更新','data-toggle'=>'modal','class'=>'a_margin']) ;
                    },
                    'delete'=> function ($url, $model, $key) {
                        return  Html::a('删除', $url, ['title' => '删除','class'=>'a_margin','data' => [
                            'confirm' => '确认删除，删除后不可恢复?',
                            'method' => 'post',
                        ]]) ;
                    },
                ],
            ],
        ],
    ]); ?>
    </div>
</div>
