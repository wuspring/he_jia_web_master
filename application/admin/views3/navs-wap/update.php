<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NavsWap */

$this->title = '编辑';
$this->params['breadcrumbs'][] = ['label' => '手机端', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '首页导航', 'url' => ['navs-wap/wap-index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
