<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\TicketApplyGoodsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ticket-apply-goods-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php $form->field($model, 'id') ?>

    <?= $form->field($model, 'title')->textInput()->label('商品名称') ?>

    <?= $form->field($model, 'ticket_id', ['options' =>['style' => 'display:none;']])->hiddenInput(['value' => $ticketId]); ?>

    <?php  echo $form->field($model, 'type')->dropDownList($model->getTypeDic(),['prompt'=>'请选择'])->label('类型'); ?>

    <?php // echo $form->field($model, 'create_time') ?>

    <div class="form-group">
        <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('重置', \DL\service\UrlService::build(['ticket-apply-goods/index', 'ticketId'=>$ticketId]), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $(function () {
        $('#w0 .form-group').addClass('col-sm-2').last().css('margin-top', '25px');
        $('#w0 .form-group').removeClass('form-group');
        $('#w0').addClass('form-group');
    })
</script>
