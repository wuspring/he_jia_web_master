<?php

use yii\helpers\Html;
use yii\grid\GridView;
use DL\service\UrlService;
use \common\models\Ticket;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MoneySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '页面设置';
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>


    <ul class="nav nav-tabs dl_tab">
        <li class="col-lg-2 tab_title <?=($active=='JIA_ZHUANG')?'active':''?>"><a  href="<?=UrlService::build(['ticket/manage','tab'=>'JIA_ZHUANG']);?>">家装展</a></li>
        <li class="col-lg-2 tab_title <?=($active=='JIE_HUN')?'active':''?>"><a  href="<?=UrlService::build(['ticket/manage','tab'=>'JIE_HUN']);?>">婚庆展</a></li>
        <li class="col-lg-2 tab_title <?=($active=='YUN_YING')?'active':''?>"><a  href="<?=UrlService::build(['ticket/manage','tab'=>'YUN_YING']);?>">孕婴展</a></li>
    </ul>

    <div class="panel-body">
        <?= Html::a('添加展会', ['create', 'ca_type' => Ticket::CA_TYPE_ZHAN_HUI], ['class' => 'btn btn-success']) ?>
    </div>

    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            array('attribute'=>'name',
                'label'=> '展会名称',
                'value' =>function ($filterModel){
                    return $filterModel->name;
                }),
            array(
                    'attribute'=>'open_date',
                    'label'=> '举办日期'
                ),
            array(
                'attribute'=>'end_date',
                'label'=> '结束日期'
            ),
            array(
                'attribute'=>'ask_ticket_start_date',
                'label'=> '票据发行日期',
                'value' => function ($model) {
                    $start = strlen($model->ask_ticket_start_date) ? $model->ask_ticket_start_date : '(未设置)';
                    $end = strlen($model->ask_ticket_end_date) ? $model->ask_ticket_end_date : '(未设置)';
                    return "{$start} ~ {$end}";
                }
            ),
            array('attribute'=>'name',
                'label'=> '预定数量',
                'value' =>function ($filterModel){
                    return $filterModel->ticketInfo->ticket_amount;
                }),
            array(
                    'attribute'=>'order_price',
                    'label'=> '预定金额'
                ),
            array(
                    'attribute'=>'status',
                    'label'=> '状态',
                    'value' => function ($model) {
                        return $model->statusInfo;
                    }
                ),

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'操作',
                'template' => '{setting}',
                'buttons' => [
                    'setting' => function ($url, $model, $key) {
                        $url = \DL\service\UrlService::build(['m-ticket-config/update', 'ticketId' => $model->id]);
                        return Html::a('页面设置', $url, ['title' => '页面设置', 'class'=>"btn btn-info btn-xs ",'data-toggle' => 'modal']);
                    },
                ]
            ]
        ],
    ]); ?>
    </div>
</div>
<script src="/public/js/jquery-1.8.3.min.js"></script>
<script>
    $(function () {
        $('#w0 .form-group').addClass('col-sm-2').last().css('margin-top', '25px');
        $('#w0 .form-group').removeClass('form-group');
        $('#w0').addClass('form-group');
    });
</script>