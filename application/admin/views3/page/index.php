<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\Request;
use \DL\service\UrlService;

/* @var $this yii\web\View */
/* @var $model app\Models\Config */

$this->title = "页面设置";
$this->params['breadcrumbs'][] = ['label' => '城市管理', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = $this->title;

function getBanner($pics)
{
    $pics = (array)$pics;
    return arrayGroupsAction($pics, function ($pic) {
        list($href, $img) = explode('@', $pic);
        return ['url' => $href, 'img' => $img];
    });
}
?>

<!-- 5. $JQUERY_VALIDATION =========================================================================

				jQuery Validation
-->
				<!-- Javascript -->
				<script>
					init.push(function () {
						$("#jq-validation-phone").mask("(999) 999-9999");
						$('#jq-validation-select2').select2({ allowClear: true, placeholder: 'Select a country...' }).change(function(){
							$(this).valid();
						});
						$('#jq-validation-select2-multi').select2({ placeholder: 'Select gear...' }).change(function(){
							$(this).valid();
						});

						// Add phone validator
						$.validator.addMethod(
							"phone_format",
							function(value, element) {
								var check = false;
								return this.optional(element) || /^\(\d{3}\)[ ]\d{3}\-\d{4}$/.test(value);
							},
							"Invalid phone number."
						);

						// Setup validation
						$("#jq-validation-form").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: false,
							rules: {
								'jq-validation-email': {
								  required: true,
								  email: true
								},
								'jq-validation-password': {
									required: true,
									minlength: 6,
									maxlength: 20
								},
								'jq-validation-password-confirmation': {
									required: true,
									minlength: 6,
									equalTo: "#jq-validation-password"
								},
								'jq-validation-required': {
									required: true
								},
								'jq-validation-url': {
									required: true,
									url: true
								},
								'jq-validation-phone': {
									required: true,
									phone_format: true
								},
								'jq-validation-select': {
									required: true
								},
								'jq-validation-multiselect': {
									required: true,
									minlength: 2
								},
								'jq-validation-select2': {
									required: true
								},
								'jq-validation-select2-multi': {
									required: true,
									minlength: 2
								},
								'jq-validation-text': {
									required: true
								},
								'jq-validation-simple-error': {
									required: true
								},
								'jq-validation-dark-error': {
									required: true
								},
								'jq-validation-radios': {
									required: true
								},
								'jq-validation-checkbox1': {
									require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
								},
								'jq-validation-checkbox2': {
									require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
								},
								'jq-validation-policy': {
									required: true
								}
							},
							messages: {
								'jq-validation-policy': 'You must check it!'
							}
						});
					});
				</script>
				<!-- / Javascript -->

<style>
    .alert_window {
        min-width: 600px;
        width: 50%;
        height: 150px;
        padding-top: 30px;
        z-index: 100;
        position: absolute;
        margin: 0 auto;
        top: 30%;
        left: 25%;
        background: #fff;
    }
    .shadow {
        width: 100%;
        height: 100%;
        z-index: 99;
        background: #9e9e9e;
        position: absolute;
        top: 0;
        left: 0;
    }
    .hide {
        display: none;
    }
</style>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title">页面设置</span>
    </div>

    <ul class="nav nav-tabs dl_tab">
        <?php if ($typeDics) : foreach ($typeDics AS $index => $typeDic) :?>
        <li class="col-lg-2 tab_title <?= $type==$index ? 'active' : ''; ?>"><a href="<?= UrlService::build(['page/index', 'type' => $index]); ?>"><?= "{$typeDic}设置"; ?></a></li>
        <?php endforeach; endif;?>
    </ul>
    <div class="panel-body" style="text-align: right">
        <?= Html::a('设置', ['setting', 'type' => $type], ['class' => 'btn btn-primary']) ?>
    </div>
    <div class="panel-body">
            <table class="table">
                <thead>
                <tr>
                    <th class="col-md-3">Banner列表</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php if ($banner) :?>
                            <td>

                                <div id="myCarousel" class="carousel slide">
                                    <!-- 轮播（Carousel）指标 -->

                                    <ol class="carousel-indicators">
                                        <?php  foreach (getBanner($banner) AS $k=> $ban) :?>
                                            <li data-target="#myCarousel" data-slide-to="<?php echo $k;?>"  class="<?php echo $k==0?'active':'';?>"></li>
                                        <?php endforeach; ?>
                                    </ol>
                                    <!-- 轮播（Carousel）项目 -->
                                    <div class="carousel-inner">
                                        <?php  foreach (getBanner($banner) AS $k=> $ban) :?>
                                        <div class="item <?php echo $k==0?'active':'';?>">
                                            <img src="<?=$ban['img']?>" alt="First slide">
                                        </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <!-- 轮播（Carousel）导航 -->
                                    <!-- 轮播（Carousel）导航 -->
                                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>


<!--                                <img src="--><?//= $ban; ?><!--" title="图片链接：(未设置)" style="max-height: 240px" alt="">-->

                            </td>
                        <?php else :?>
                            <td>
                                未设置
                            </td>
                        <?php endif; ?>
                    </tr>
                </tbody>
            </table>

        <?php if ($expand) :?>

        <table class="table">
            <thead>
            <tr>
                <th class="col-md-1">类型</th>
                <th class="col-md-1">优选品牌1</th>
                <th class="col-md-1">优选品牌2</th>
                <th class="col-md-1">优选品牌3</th>
                <th class="col-md-1">优选品牌4</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="col-md-1">优选品牌</td>

                <?php arrayGroupsAction($expand, function ($huodong) {
                    strpos('@', $huodong) > -1 or $huodong .= '@';
                    list($href, $img) = explode('@', $huodong);
                    echo sprintf(
                        '<td><a href="%s"><img src="%s" title="图片链接：(未设置)" style="max-height: 140px" alt=""></a></td>',
                        $href, $img
                    );
                })
                ?>
            </tr>
            </tbody>
        </table>
    <?php endif; ?>

            <table class="table">
                <thead>
                <tr>
                    <th class="col-md-1">类型</th>
                    <th class="col-md-1">活动图片1</th>
                    <th class="col-md-1">活动图片2</th>
                    <th class="col-md-1">活动图片3</th>
                    <th class="col-md-1">活动图片4</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="col-md-1">活动图</td>

                        <?php arrayGroupsAction($huodongs, function ($huodong) {
                            strpos('@', $huodong) > -1 or $huodong .= '@';
                            list($href, $img) = explode('@', $huodong);
                            echo sprintf(
                                    '<td><a href="%s"><img src="%s" title="图片链接：(未设置)" style="max-height: 140px" alt=""></a></td>',
                                $href, $img
                            );
                        })
                        ?>
                </tr>
                </tbody>
            </table>

        <?php if ($promise) :?>
            <table class="table">
                <thead>
                <tr>
                    <th class="col-md-3">承诺图片</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="col-md-1">
                        <div class="carousel slide">
                            <img src="<?= $promise; ?>" alt="">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        <?php endif; ?>
        <?php if ($smap) :?>
            <table class="table">
                <thead>
                <tr>
                    <th class="col-md-3">小地图</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="col-md-1">
                        <div class="carousel slide">
                            <img src="<?= $smap; ?>" alt="">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
</div>

