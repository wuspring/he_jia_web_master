<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FitupAppointment */

$this->title ='详情';
$this->params['breadcrumbs'][] = ['label' => '装修学堂', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '预约列表', 'url' => ['fitup-appointment/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

   <div class="panel-heading">
       <div class="panel-title"><?= Html::encode($this->title) ?></div>
   </div>
    <div class="panel-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
//                'id',
                [
                    'attribute' => 'member_id',
                    'label'=>'会员名称',
                    'value'=>$model->member->nickname
                ],
                'name',
                'mobile',
                'create_time',
//                'is_del',
                [
                    'attribute' => 'is_deal',
                    'label'=>'是否处理',
                    'value'=>($model->is_deal==1)?'已经处理':'未处理'
                ],
                [
                    'attribute' => 'remarks',
                    'label'=>'备注',
                ],
              //  'dispatch_people',
            ],
        ]) ?>
       <?php if ($model->is_deal==0):?>
           <?php $form = ActiveForm::begin([
               'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
               'fieldConfig' => [
                   'inputOptions' => ['class' => 'form-control'],
                   'labelOptions'=>['class'=>'col-sm-2 control-label'],
                   'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
           ]); ?>

           <?= $form->field($model, 'remarks')->textarea(['rows'=>6])->label('添加备注') ?>
           <?= $form->field($model, 'is_deal')->radioList(['0'=>'不处理','1'=>'处理'])->label('是否处理') ?>
           <div class="form-group">
               <div class="col-sm-offset-2 col-sm-10">
                   <?=Html::submitButton('提交',['class' => 'btn btn-success']) ?>
               </div>
           </div>

           <?php ActiveForm::end(); ?>
        <?php endif;?>


    </div>

</div>
