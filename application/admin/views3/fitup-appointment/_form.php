<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FitupAppointment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fitup-appointment-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
        'fieldConfig' => [
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions'=>['class'=>'col-sm-2 control-label'],
            'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
    ]); ?>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10" style="text-align: right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '发布') : Yii::t('app', '保存修改'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'create_time')->textInput()->label('添加时间') ?>

    <?php ActiveForm::end(); ?>

</div>
