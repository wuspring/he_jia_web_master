<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \common\models\Order;
use \common\models\TicketAsk;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = '订单详情';
$this->params['breadcrumbs'][] = ['label' => '订单管理', 'url' => 'javascript:void(0)'];
$this->params['breadcrumbs'][] = $this->title;

function secretName ($model) {
    if (!in_array($model->order_state, [Order::STATUS_RECEIVED, Order::STATUS_JUDGE])) {

        return secretString ($model->buyer_name);
    }
}

function secretString ($string)
{
    mb_internal_encoding('UTF-8');
    $length = mb_strlen($string);

    if ($length > 3) {
        return mb_substr($string, 0, 1) . "*****" . mb_substr($string, -1, 1);
    } else if ($length > 1) {
        return mb_substr($string, 1, 1) . "***";
    } else {
        return '*';
    }
}

function getAskTicket ($module)
{
    $model = TicketAsk::findOne(['ticket_id' => $module->ticket_id, 'member_id' => $module->buyer_id]);

    return $model ?:(new TicketAsk());
}
?>

<div class="panel" style="display: none">
    <div class="panel-heading">
        <span class="panel-title">收货人信息</span>
    </div>
    <div class="panel-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'receiver_name',
                'receiver_address',
                'receiver_mobile',
                'receiver_zip',
            ],
        ]) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">
        <span class="panel-title">订单商品信息</span>
    </div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>订单编号</th>
                <th>商品名称</th>
                <th>商品规格</th>
                <th>单价</th>
                <th>数量</th>
                <th>小计</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($orderGoods as $orderGood): ?>
                <tr>
                    <td><?= $model->orderid ?></td>
                    <td><?= $orderGood->goods_name ?></td>
                    <td><?= $orderGood->attr ?></td>
                    <td><?= $orderGood->goods_pay_price ?></td>
                    <td><?= $orderGood->goods_num ?></td>
                    <td><?= ($orderGood->goods_pay_price * $orderGood->goods_num) ?></td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>

    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">订单信息</span>
        </div>
        <div class="panel-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'orderid',
//            'pay_sn',
            [
                'attribute' => 'ticket_type',
                'label' => '订单类型',
                'value' => $model->ticketTypeInfo
            ],
            [
                'attribute' => 'pay_amount',
                'label' => '订单价格',
            ],
            'order_amount',
//            'integral_amount',
//            'pd_amount',
//            'freight',
            [
                'attribute'=>'order_state',
                'label'=>'订单状态',
                'value' => $model->orderState
            ],
            [
                'attribute'=>'evaluation_state',
                'label'=>'评价状态',
                'value' => $model->judgeState
            ],
//            'shipping',
//            'shipping_code',
//            'deliveryTime',
            'add_time',
//            'payment_time',
//            'finnshed_time',
//            [
//                'attribute' =>'fallinto_state',
//                'value' => call_user_func(function ($model) {
//                    $dic = [
//                        '0' => '待分成',
//                        '1' => '已分成'
//                    ];
//                    return isset($dic[$model->fallinto_state]) ? $dic[$model->fallinto_state] : $model->fallinto_state;
//                }, $model)
//            ]
        ],
    ]) ?>
        </div>
    </div>

        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">买家信息</span>
            </div>
            <div class="panel-body">
    <?php if ($model->ticket_id) :
             echo DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                'attribute'=>'buyer_name',
                                'value' => secretName($model)
                            ],
                            [
                                'attribute'=>'buyer_name',
                                'label' => '预约电话',
                                'value' => secretString(getAskTicket($model)->mobile)
                            ],
                            [
                                'attribute'=>'buyer_name',
                                'label' => '索票地址',
                                'value' => secretString(getAskTicket($model)->address)
                            ],
                        ],
                    ]);
               else :
                echo   DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute'=>'buyer_name',
                            'value' => secretName($model)
                        ],
                    ],
                ]);

                endif;
                ?>
            </div>
        </div>


