                   <?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use \common\models\Order;

/* @var $this yii\web\View */
/* @var $searchModel app\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '订单列表';
$this->params['breadcrumbs'][] = ['label' => '订单管理', 'url' => 'javascript:void(0)'];
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
.sml {
    margin-right: 20px;
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    padding: 1px 5px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
}
</style>
<script language="javascript" type="text/javascript" src="<?php echo yii::$app->request->hostInfo;?>/public/My97DatePicker/WdatePicker.js"></script>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <style>
        .order-item {
            border: 1px solid transparent;
            margin-bottom: 1rem;
        }

        .order-item table {
            margin: 0;
        }

        .order-item:hover {
            border: 1px solid #3c8ee5;
        }

        .goods-item {
            margin-bottom: .75rem;
        }

        .goods-item:last-child {
            margin-bottom: 0;
        }

        .goods-pic {
            width: 5.5rem;
            height: 5.5rem;
            display: inline-block;
            background-color: #ddd;
            background-size: cover;
            background-position: center;
            margin-right: 1rem;
        }

        .goods-name {
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }

        .order-tab-1 {
            width: 40%;
        }

        .order-tab-2 {
            width: 10%;
            text-align: center;
        }

        .order-tab-3 {
            width: 10%;
            text-align: center;
        }

        .order-tab-4 {
            width: 20%;
            text-align: center;
        }

        .order-tab-5 {
            width: 10%;
            text-align: center;
        }
        .order-tab-6 {
            width: 10%;
            text-align: center;
        }

        .status-item.active {
            color: inherit;
        }
        .label1 {
            height: 34px;
            line-height: 34px;
        }
        .inblock {
            display: inline-block;
        }
    </style>
    <div class="panel-body">
        <div class="mb-3 clearfix">
            <div class="p-4 bg-shaixuan">
                <form method="get">
                    <?php $_s = ['keyword', 'keyword_1', 'date_start', 'date_end', 'page', 'per-page'] ?>
                    <?php foreach ($_GET as $_gi => $_gv):if (in_array($_gi, $_s)) continue; ?>
                        <input type="hidden" name="<?= $_gi ?>" value="<?= $_gv ?>">
                    <?php endforeach; ?>

                    <div class="col-md-12">
                        <div class="mr-4">
                            <div class="col-5 col-md-3" style="width: 20%;padding: 0">
                                <div class="col-md-4" style="padding: 0">
                                    <label class="label1">订单类型：</label>
                                </div>
                                <select class="form-control inblock col-md-8" name="order_state" style="width: 66.66%">
                                    <?php
                                    $orderTypeDic = new Order();
                                    $orderTypeDic->ticket_type = Order::TICKET_TYPE_ORDER;
                                    foreach ($orderTypeDic->getOrderStatusDic() AS $status => $name ) {
                                        echo "<option value='{$status}'" . ($filterStatus==$status ? 'selected' : '') . " >{$name}</option>";
                                    };
                                    ?>
                                </select>
                            </div>
                            <div class="col-5 col-md-3">
                                <div class="col-md-3" style="padding: 0">
                                    <label class="label1">下单时间：</label>
                                </div>
                                <div class="col-md-9" style="padding: 0 0 0 5px;">
                                        <input class="form-inline form-control col-md-3 " style="display: inline-block;width: 40%;" id="date_start" name="date_start"
                                               autocomplete="off"
                                               value="<?= isset($_GET['date_start']) ? trim($_GET['date_start']) : '' ?>">

                                        <label class="form-inline col-md-2 label1" style="text-align: center;padding: 0 10px;">至</label>

                                        <input class="form-inline form-control col-md-3 " style="display: inline-block;width: 40%;" id="date_end" name="date_end"
                                               autocomplete="off"
                                               value="<?= isset($_GET['date_end']) ? trim($_GET['date_end']) : ''  ?>">

                                </div>
                            </div>

                            <div class="form-group row col-md-5">
                                <div class="col-5 col-md-4" style="padding: 0">
                                    <select class="form-control" name="keyword_1">
                                        <?php if (isset($_GET['keyword_1'])): ?>
                                            <option value="1" <?= $_GET['keyword_1'] == 1 ? "selected" : "" ?>>订单号</option>
                                            <option value="2" <?= $_GET['keyword_1'] == 2 ? "selected" : "" ?>>用户</option>
                                            <option value="3" <?= $_GET['keyword_1'] == 3 ? "selected" : "" ?>>收货人</option>
                                        <?php else:?>
                                            <option value="1">订单号</option>
                                            <option value="2">用户</option>
                                            <option value="3">收货人</option>
                                        <?php endif;?>
                                    </select>
                                </div>
                                <div class="col-7 col-md-8">
                                    <input class="form-control"
                                           name="keyword"
                                           autocomplete="off"
                                           value="<?= isset($_GET['keyword']) ? trim($_GET['keyword']) : null ?>">
                                </div>
                            </div>

                            <div class="form-group col-md-offset-1">
                                <button class="btn btn-primary mr-4">筛选</button>
                                <a style="" class="btn btn-secondary mr-4"
                                   href="<?php $q = $_GET; $q['type'] = $type; unset($q['r']); echo Url::toRoute(array_merge(['order/loader'], $q)); ?>" target="_blank">批量导出</a>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <table class="table table-bordered bg-white">
            <tr>
                <th class="order-tab-1">商品信息</th>
                <th class="order-tab-6">订单类型</th>
                <th class="order-tab-2">金额</th>
                <th class="order-tab-4">实际付款</th>
                <th class="order-tab-3">订单状态</th>
                <th class="order-tab-5">操作</th>
            </tr>
        </table>
        <?php foreach ($list as $order_item): ?>
            <div class="order-item" style="">
                <table class="table table-bordered bg-white">
                    <tr>
                        <td colspan="6">
                            <span class="mr-5">订单号：<?= $order_item['orderid'] ?></span>
                            <span>用户：<?= $order_item['buyer_name'] ?></span>

                        </td>
                    </tr>
                    <tr>
                        <td class="order-tab-1">
                            <?php foreach ($order_item['goods_list'] as $goods_item): ?>
                                <div class="goods-item" flex="dir:left box:first">
                                    <div class="fs-0 col-md-2">
                                        <div class="goods-pic"
                                             style="background-image: url('<?= $goods_item['goods_pic'] ?>')"></div>
                                    </div>
                                    <div class="goods-info col-md-10">
                                        <div class="goods-name"><?= $goods_item['goods_name'] ?></div>
                                        <div class="fs-sm">
                                            规格：
                                            <span class="text-danger">
                                            <?php $attr_list = json_decode($goods_item['attr']); ?>
                                                <?php if (is_array($attr_list)):foreach ($attr_list as $attr): ?>
                                                    <span class="mr-3"><?= $attr->attr_group_name ?>
                                                        :<?= $attr->attr_name ?></span>
                                                <?php endforeach;endif; ?>
                                        </span>
                                        </div>
                                        <div class="fs-sm">数量：
                                            <span class="text-danger"><?= $goods_item['goods_num'] ?></span>
                                        </div>
                                        <div class="fs-sm">小计：
                                            <span class="text-danger">￥<?= $goods_item['goods_price'] * $goods_item['goods_num'] ?>
                                                </span></div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </td>
                        <td class="order-tab-6">
                            <div><?= $order_item['type'] ?></div>
                        </td>
                        <td class="order-tab-2">
                            <div><?= $order_item['pay_amount'] ?>元</div>
                            <div>积分：<?= $order_item['integral_amount'] ?></div>

                        </td>
                        <td class="order-tab-4">
                            <div>实际支付：<?= $order_item['order_amount'] ?>元</div>
                            <div>剩余支付：<?= $order_item['pay_amount'] - $order_item['order_amount'] ?>元</div>
                            <?php if ($order_item['ticket_type'] == Order::TICKET_TYPE_COUPON ): ?>
                                <?php if (in_array($order_item['order_state'], [Order::STATUS_SEND, Order::STATUS_RECEIVED, Order::STATUS_JUDGE])): ?>
                                    <div>
                                        <b>线下核销</b>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td class="order-tab-3">
                            <?php if (trim($order_item['state_txt'])=='待支付') :?>
                            <div class="btn btn-sm btn-success sml" style="background: red;border: red">
                                <?= $order_item['state_txt'] ?>
                            </div>
                            <?php else:?>
                            <div class="btn btn-sm btn-success sml">
                                <?= $order_item['state_txt'] ?>
                            </div>
                            <?php endif; ?>

                        </td>
                        <td class="order-tab-5">
                            <a class="btn btn-sm btn-primary"
                               href="<?= Url::toRoute(['order/view', 'id' => $order_item['orderid']]) ?>">详情</a>
                            <?php if ($order_item['ticket_type'] == Order::TICKET_TYPE_COUPON  and false): ?>
                                <?php if ($order_item['order_state'] == Order::STATUS_WAIT): ?>
                                <a class="btn btn-sm btn-primary hexiao" href="javascript:"
                                   data-id="<?= $order_item['orderid'] ?>">确认</a>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <span class="mr-5"><?= $order_item['add_time'] ?></span>
                        </td>
                    </tr>
                </table>
            </div>
        <?php endforeach; ?>
        <div class="text-center">
            <?php if (!empty($pagination)): ?>
                <?= LinkPager::widget(['pagination' => $pagination,]) ?>
            <?php endif; ?>
            <div class="text-muted"><?= $pagination->totalCount ?>条数据</div>
        </div>
    </div>

</div>
                   <div id="hexiaoModal" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
                       <div class="modal-dialog">
                           <div class="modal-content">
                               <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                   <h4 class="modal-title" id="myModalLabel">确认</h4>
                               </div>
                               <div class="modal-body">
                                    是否完成该订单？
                               </div>
                               <div class="modal-footer">
                                   <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                                   <button type="button" class="btn btn-primary hexiao"  id="hexiaocode">确认</button>
                               </div>
                           </div>
                       </div>
                   </div>

<div id="fahuoModal" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">订单发货</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" id="ordercode">
                <p>快递公司：</p>
                <select class="form-control" id="express_id">
                    <?php foreach ($express as $key=>$op):?>
                        <option value="<?=$key?>"><?=$op?></option>
                    <?php endforeach;?>
                </select>
                <p>快递单号：</p>
                <input type="text" id="express_no" class="form-control" placeholder="快递单号">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary sendGoods">确认发货</button>
            </div>
        </div>
    </div>
</div>
<script>
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    init.push(function () {

        $(document).on("click",".fahuo",function () {
            $("#ordercode").val($(this).attr('data-id'));

            $("#fahuoModal").modal();
        });

        $(document).on('click','.sendGoods',function () {
            var pbody = $(this).parents('.modal-content');
            var order_id = pbody.find('#ordercode').val();
            var express_id = pbody.find('#express_id').val();
            var express_no = pbody.find('#express_no').val();

            if (!/^[\d|\w]+$/.test(express_no)) {
                alert('请输入正确的快递单号');
                $(this).val('');
                return false;
            }

            $.post('<?php echo Url::toRoute(['order/send-goods'])?>', {order_id:order_id,express_id:express_id,express_no:express_no},function (res) {
                if(res.code==0){
                    location.reload();
                    alert(res.msg);
                }else{
                    alert(res.msg);
                }
            },'json');
        });

        $("#date_start").click(function(){
            WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d'})
        });
        $("#date_end").click(function(){
            WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d',minDate:'#F{$dp.$D(\'date_start\')}'})
        });

        var saveVal = '';
        $('input[id="express_no"]').bind('input', function () {
            var val = $(this).val();
            if (!/^[\d|\w]+$/.test(val)) {
                alert('请输入正确的快递单号');
                // $(this).val(saveVal);
                return false;
            }

            saveVal = val;
        })

    });


    $(document).on("click",".hexiao",function () {
        $("#hexiaocode").attr('data-val', $(this).attr('data-id'));

        $("#hexiaoModal").modal();
    });

    $(document).on('click','.hexiao', function () {
        var number = $(this).attr('data-val'),
            data = {};

        if (number.length < 0) {
            alert("订单信息有误");
            return false;
        }
        data.number = number;

        $.post('<?php echo \DL\service\UrlService::build(['order/hexiao-order'])?>', data, function (res) {
            if(res.status){
                alert(res.msg);
                window.location.reload();
                return false;
            }

            alert(res.msg);
            $("#hexiaoModal").modal('hide');
        },'json');
    });

</script>