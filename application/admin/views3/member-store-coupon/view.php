<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MemberCoupon */

$this->title ='详情';
$this->params['breadcrumbs'][] = ['label' => 'Member Coupons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <div class="panel-title">
            <?= Html::encode($this->title) ?>
        </div>
    </div>
    <div class="panel-body">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
              //  'id',

                [
                    'attribute' => 'member_id',
                    'label' => '会员名称',
                    'value'=>(!empty($model->member))?$model->member->nickname:' '
                ],
                [
                    'attribute' => 'coupon_id',
                    'label' => '优惠劵描述',
                    'value'=>(!empty($model->goodsCoupon))?$model->goodsCoupon->describe:' '
                ],
                [
                    'attribute' => 'code',
                    'label' => '核销码',
                    'value'=>($model->is_use==1)?$model->code:'-'
                ],
                'begin_time',
                'end_time',
                [
                    'attribute' => 'is_use',
                    'label' => '是否使用',
                    'value'=>($model->is_use==1)?'已经使用':'未使用'
                ],
                [
                    'attribute' => 'addtime',
                    'label' => '领取时间',
                ],
            ],
        ]) ?>

    </div>

</div>
