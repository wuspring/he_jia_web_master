<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SentSms */

$this->title = '添加模板';
$this->params['breadcrumbs'][] = ['label' => 'Sent Sms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading" style="margin-bottom: 20px">
         <div class="panel-title"><?= Html::encode($this->title) ?></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
