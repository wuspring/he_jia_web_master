<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SentSms */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sent-sms-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
        'fieldConfig' => [
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions'=>['class'=>'col-sm-2 control-label'],
            'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
    ]); ?>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10" style="text-align: right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '创建') : Yii::t('app', '保存修改'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?= $form->field($model, 'sms_name')->textInput(['maxlength' => true,'placeholder'=>'中细软']) ?>

    <?= $form->field($model, 'sms_code')->textInput(['maxlength' => true,'placeholder'=>'SMS_162198405']) ?>


    <?= $form->field($model, 'type')->dropDownList($model->getTypeDic(),['prompt'=>'请选择类型'])->label('模板类型') ?>

    <?= $form->field($model, 'content')->textInput(['maxlength' => true,'placeholder'=>'一些通知信息'])->label('模板信息') ?>


    <?php ActiveForm::end(); ?>

</div>
