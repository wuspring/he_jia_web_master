<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Provinces */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="panel-body form-controls-demo">

    <?php $form = ActiveForm::begin();?>
    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
    </div>

    <?php $form->field($pModel, 'id')->textInput(['maxlength' => true]); ?>
    <?php  $form->field($pModel, 'cname')->textInput(['maxlength' => true]) ?>
    <?= $form->field($pModel, 'pinyin')->textInput(['maxlength' => true]) ?>


    <?= $form->field($pModel, 'title')->textInput() ?>
    <?= $form->field($pModel, 'keywords')->textarea(['rows' => 6]) ?>
    <?= $form->field($pModel, 'description')->textarea(['rows' => 6]) ?>

    <?php ActiveForm::end(); ?>

</div>
