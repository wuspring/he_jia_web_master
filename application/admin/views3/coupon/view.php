<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Coupon */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Coupons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coupon-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
//            'store_id',
            'name',
            'desc',
//            'pic_url:url',
//            'discount_type',
            'min_price',
            'sub_price',
//            'discount',
//            'expire_type',
//            'expire_day',
            'begin_time',
            'end_time',
            [
                 'attribute' => 'addtime',
                 'label' => '创建时间',
            ],
//            'is_delete',
            'total_count',
//            'is_join',
//            'sort',
//            'is_integral',
//            'integral',
//            'price',
//            'total_num',
//            'type',
//            'user_num',
//            'goods_id_list',
        ],
    ]) ?>

</div>
