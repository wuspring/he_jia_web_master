<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Advertising */

$this->title = '添加广告';
$this->params['breadcrumbs'][] = ['label' => '手机端', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '广告列表', 'url' => ['advertising/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

   	<div class="panel-heading">
		<span class="panel-title"><?= Html::encode($this->title) ?></span>
	</div>

    <?= $this->render('_form', [
        'model' => $model,
        'adsense'=>$adsense,
    ]) ?>

</div>