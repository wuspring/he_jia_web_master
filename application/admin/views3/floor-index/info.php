<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FloorIndex */

$this->title = '楼层设置';
$this->params['breadcrumbs'][] = ['label' => '城市管理', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '楼层管理', 'url' => ['floor-index/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
        <span style="float: right">
            <?= Html::a('编辑', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        </span>
    </div>

    <div class="panel-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'provinces_id',
                'label' => '城市',
                'value' => $model->provinces->cname
            ],
            [
                'attribute' => 'good_class_id',
                'label' => '栏目',
                'value' => $model->goodClass->name
            ],
        ],
    ]) ?>
    </div>

    <div class="panel-body">
        <div class="panel-heading">
            <span class="panel-title">首页设置</span>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th class="col-md-1">位置</th>
                <?php foreach ($model->img as $pos => $pic): ?>
                    <th class="col-md-1"><?= $pos; ?></th>
                <?php endforeach;?>
            </tr>
            </thead>
            <tbody>
            <tr>
             <td class="col-md-1">详情</td>
            <?php foreach ($model->img as $pos => $pic): ?>
                    <?php if (strlen($pic['src'])) :?>
                    <td class="col-md-1"><img src="<?= $pic['src']; ?>" title="图片链接：<?= strlen($pic['href']) ? $pic['href'] : '(未设置)';?>" style="max-height: 240px" alt=""></td>
                    <?php else :?>
                    <td class="col-md-1">(空)</td>
                    <?php endif; ?>
            <?php endforeach;?>
            </tr>
            </tbody>
        </table>

        <table class="table">
            <thead>
            <tr>
                <th class="col-md-3">推荐商铺</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="col-md-3">
            <?php foreach ($model->brand as $brand): ?>
                <div  class="col-md-3" style="height: 75px; overflow: hidden;margin-bottom: 10px">
                    <img src="<?= $brand->avatarTm; ?>" alt="">
                    <span style="display:  inline-block;width: 35%;float: right;margin-right: 5%;" title="<?= $brand->nickname; ?>">
                        <?= \yii\helpers\StringHelper::truncate($brand->nickname, 4); ?>
                        <?php if (isset($brand->StoreGcScore[$cid])) : $scoreInfo = $brand->StoreGcScore[$cid]; ?>
                       <b title="当前店铺评分"> (<?= $scoreInfo->score;?>)</b>
                    <br> 管理员评分：
                    <input type="text" title="分值与店铺评分相乘越高排名越前" data-id="scoreInfo" data-val="<?= $scoreInfo->id;?>" value="<?= $scoreInfo->admin_score; ?>" style="display:  block;width:  100%;">
                    <?php endif; ?>
                        </span>
                </div>
            <?php endforeach;?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <script>
        $("[data-id=\"scoreInfo\"]").change(function () {
            var id = $(this).attr('data-val'),
                val = $(this).val();

            $.ajax({
                type: "POST",
                url: "<?php echo \DL\service\UrlService::build(['set-admin-score']);?>",
                data: {id : id, val : val},
                async: true,
                dataType: 'json',

                success: function (res) {
                    if (res.status) {
                        window.location.reload();
                        return false;
                    }
                    alert("更新失败");
                },
            });
        })
    </script>

    <div class="panel-body">
        <div class="panel-heading">
            <span class="panel-title">采购设置</span>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th class="col-md-1">位置</th>
                    <th class="col-md-1">pic_1</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="col-md-2">详情</td>
                        <td class="col-md-10">
                            <?php if (count($model->zh_first_imgs)) : foreach ($model->zh_first_imgs AS $src) :?>
                                <img src="<?= $src; ?>" class="col-md-3" style="display:inline-block; max-height: 240px" alt="">
                            <?php endforeach; else :?>
                            (空)
                            <?php endif; ?>
                        </td>
            </tr>
            </tbody>
        </table>
        <table class="table">
            <thead>
            <tr>
                <th class="col-md-1">位置</th>
                <?php foreach ($model->zh_img as $pos => $pic): ?>
                    <th class="col-md-1"><?= $pos; ?></th>
                <?php endforeach;?>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="col-md-1">详情</td>
                <?php foreach ($model->zh_img as $pos => $pic): ?>
                    <?php if (count($pic['src'])) :?>
                        <td class="col-md-1">
                            <?php foreach ($pic['src'] AS $src) :?>
                            <img src="<?= $src; ?>" title="图片链接：<?= strlen($pic['href']) ? $pic['href'] : '(未设置)';?>" style="max-height: 240px" alt="">
                            <?php endforeach; ?>
                        </td>
                    <?php else :?>
                        <td class="col-md-1">(空)</td>
                    <?php endif; ?>
                <?php endforeach;?>
            </tr>
            </tbody>
        </table>
    </div>
</div>




