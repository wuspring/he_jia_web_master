<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = '修改文章: ' . ' ' . $model->title;

?>
<div class="panel">

   	<div class="panel-heading">
		<span class="panel-title"><?= Html::encode($this->title) ?></span>
	</div>
    <?= $this->render('_form_activity', [
        'model' => $model,
    ]) ?>

</div>
