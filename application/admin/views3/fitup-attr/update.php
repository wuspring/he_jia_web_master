<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FitupAttr */

$this->title = 'Update Fitup Attr: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fitup Attrs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fitup-attr-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
