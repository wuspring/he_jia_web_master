<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Adsense */

$this->title = '添加广告位';
$this->params['breadcrumbs'][] = ['label' => 'Adsenses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

   	<div class="panel-heading">
		<span class="panel-title"><?= Html::encode($this->title) ?></span>
	</div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>