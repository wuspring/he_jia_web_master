<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\search\AdsenseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '广告位';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <div class="panel-body">
        <p>
            <?= Html::a('添加广告位', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
//            'id',
            'name',
            // 'state',
//            array(
//                'attribute' => 'state',
//                'label'=>'是/否显示',
//                'format' => 'raw',
//                'value'=>function($filterModel){
//                    $url="javascript:void(0)";
//                    if ($filterModel->state == 0) {
//                        return  Html::a('<span class="glyphicon glyphicon-eye-close"></span>', $url, ['title' => '是/否显示','data-toggle'=>$filterModel->id,'data-status'=>$filterModel->state,'id'=>'data-state'] ) ;
//                    }else{
//                        return  Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => '是/否显示','data-toggle'=>$filterModel->id,'data-status'=>$filterModel->state,'id'=>'data-state'] ) ;
//                    }
//
//                },
//            ),
//            'createTime',
//            'modifyTime',
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'操作',
                'template' => '{view}',
                'buttons'=>[
                    'add'=>function($url, $model, $key){
                         $url=Url::toRoute(['/advertising/create','cid'=>$model->id]) ;
                         return  Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, ['title' => '添加广告','data-toggle'=>'modal'] ) ;
                    },
                    'view'=>function($url, $model, $key){
                        $url=Url::toRoute(['/advertising/index','id'=>$model->id]) ;
                         return  Html::a('<span class="glyphicon glyphicon-globe"></span>', $url, ['title' => '查看','data-toggle'=>'modal'] ) ;
                    },
                    'update' => function ($url, $model, $key) {
                         return  Html::a('<span class="fa fa-pencil"></span>', $url, ['title' => '更新','data-toggle'=>'modal'] ) ;
                     },
                     'delete'=> function ($url, $model, $key) {
                         return  Html::a('<span class="fa fa-bitbucket"></span>', $url, ['title' => '删除','data-toggle'=>'modal'] ) ;
                     },
                ],

            ],
        ],
    ]); ?>
    </div>
</div>
<script type="text/javascript">
init.push(function () {
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
      $("#data-state").on('click',function(){
        var th =$(this);
        var id=$(this).attr('data-toggle');
        var status= $(this).attr('data-status');
        if (status == 0) {
            status=1;
        }else{
           status=0; 
        }
        $.post("<?php echo Url::toRoute(['/adsense/isshow']);?>",{_csrf:_csrf,id:id,status:status},function(data){
            if (data == 0) {
                $(th).find('span').attr('class','glyphicon glyphicon-eye-close');
                $(th).attr('data-status',0);
            }else{
                $(th).find('span').attr('class','glyphicon glyphicon-eye-open');
                $(th).attr('data-status',1);
            }
        });
    });
});
</script>