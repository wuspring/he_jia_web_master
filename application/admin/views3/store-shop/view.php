<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StoreShop */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Store Shops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <p>
        <?= Html::a('更新', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('删除', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
     <div class="panel-body">
         <?= DetailView::widget([
             'model' => $model,
             'attributes' => [
                 'id',
                 'user_id',
                 'provinces_id',
                 'store_name',
                 'description:ntext',
                 'address:ntext',
                 'work_days',
                 'work_times',
                 'notice:ntext',
                 'picitures:ntext',
             ],
         ]) ?>
     </div>


</div>
