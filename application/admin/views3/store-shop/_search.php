<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\StoreShopSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-shop-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'provinces_id') ?>

    <?= $form->field($model, 'store_name') ?>

    <?= $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'work_days') ?>

    <?php // echo $form->field($model, 'work_times') ?>

    <?php // echo $form->field($model, 'notice') ?>

    <?php // echo $form->field($model, 'picitures') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
