<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TicketApply */

$this->title = 'Create Ticket Apply';
$this->params['breadcrumbs'][] = ['label' => 'Ticket Applies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-apply-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
