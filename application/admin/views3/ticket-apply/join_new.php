<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TicketApply */

$this->title = '申请参加';
$this->params['breadcrumbs'][] = ['label' => 'Ticket Applies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">
        <?php
            $url = \DL\service\UrlService::build(['ticket-apply-goods/create', 'ticketId' => $model->ticket_id]);

            echo Html::a('添加商品', $url, ['class' => 'btn btn-primary']);
        ?>
    </div>

    <div class="panel-body">
        <table class="table">
            <thead>
                爆款预约
            </thead>
            <tbody>
            <tr>
                <td class="col-md-3"><b>商品名称</b></td>
                <td class="col-md-3"><b>封面图</b></td>
                <td class="col-md-3"><b>活动价格</b></td>
                <td class="col-md-3"><b>库存</b></td>
                <td class="col-md-3"><b>操作</b></td>
            </tr>
            <?php if ($couponGoods) :?>
                <?php foreach ($couponGoods as $shop):?>
                    <tr>
                        <td class="col-md-3"><?= $shop->goods->goods_name; ?></td>
                        <td class="col-md-3"><img src="<?= $shop->good_pic; ?>" style="max-width: 300px" alt=""></td>
                        <td class="col-md-3"><?= $shop->good_price; ?></td>
                        <td class="col-md-3"><?= $shop->good_amount; ?></td>
                        <td class="col-md-3"><?php
                            $url = \DL\service\UrlService::build(['ticket-apply-goods/update', 'id' => $shop->id]);
                            echo Html::a('编辑', $url, ['class' => 'btn btn-primary'])
                            ?>
                        </td>
                    </tr>
                <?php endforeach;?>
            <?php else :?>
                <tr>
                    <td colspan="5" class="col-md-12" style="text-align: center">暂未添加</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>

    <div class="panel-body">
        <table class="table">
            <thead>
            预存 享特价
            </thead>
            <tbody>
            <tr>
                <td class="col-md-3"><b>商品名称</b></td>
                <td class="col-md-3"><b>封面图</b></td>
                <td class="col-md-3"><b>活动价格</b></td>
                <td class="col-md-3"><b>库存</b></td>
                <td class="col-md-3"><b>操作</b></td>
            </tr>
            <?php if ($orderGoods) :?>
                <?php foreach ($orderGoods as $shop):?>
                    <tr>
                        <td class="col-md-3"><?= $shop->goods->goods_name; ?></td>
                        <td class="col-md-3"><img src="<?= $shop->good_pic; ?>" style="max-width: 300px" alt=""></td>
                        <td class="col-md-3"><?= $shop->good_price; ?></td>
                        <td class="col-md-3"><?= $shop->good_amount; ?></td>
                        <td class="col-md-3"><?php
                            $url = \DL\service\UrlService::build(['ticket-apply-goods/update', 'id' => $shop->id]);
                            echo Html::a('编辑', $url, ['class' => 'btn btn-primary'])
                            ?>
                        </td>
                    </tr>
                <?php endforeach;?>
            <?php else :?>
                <tr>
                    <td colspan="5" class="col-md-12" style="text-align: center">暂未添加</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


</div>
