<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Link */

$this->title = '添加';
$this->params['breadcrumbs'][] = ['label' => '系统管理', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '友情链接', 'url' => ['link/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

   	<div class="panel-heading">
		<span class="panel-title"><?= Html::encode($this->title) ?></span>
	</div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
