<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FitupProblem */

$this->title = \yii\helpers\StringHelper::truncate($model->problem, 10);
$this->params['breadcrumbs'][] = ['label' => '装修学堂', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '装修问答', 'url' => ['fitup-problem/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            [
                 'attribute' => 'problem',
                 'label' => '问题',
            ],
            [
                'attribute' => 'member_id',
                'label' => '会员信息',
                'value' => $model->member->nickname
            ],
            'create_time',
        ],
    ]) ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    </div>
</div>
