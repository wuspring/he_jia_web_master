<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use \common\models\Provinces;
/* @var $this yii\web\View */
/* @var $model common\models\Jingxiaoshang */

$this->title = '代理商审核权限分配 ： ' . $model->name;

$provincesData = [];
$provincesDataInfo = Provinces::find()->where(['not in', 'upid', [0, 1]])->all();
$provincesDataInfo = formatObjLists($provincesDataInfo);
foreach ($provincesDataInfo AS $info) {
    $provincesData[$info['upid']][] = $info;
}
$provinces = Provinces::find()->where(['upid' => 1])->all();
$provinces = formatObjLists($provinces);
?>
<script>
    var provincesData = <?= json_encode($provincesData); ?>;
</script>
<div class="jingxiaoshang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="jingxiaoshang-form">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'member_id',
                    'label' => '申请人',
                    'value' => $model->member->wxnick
                ],
                [
                    'attribute' => 'name',
                    'label' => '申请人',
                ],
                [
                    'attribute' => 'phone',
                    'label' => '联系方式',
                ],
                [
                    'attribute' => 'create_time',
                    'label' => '申请时间',
                ]
            ],
        ]) ?>

        <?php
            $form = ActiveForm::begin();

            $goods = \common\models\Goods::find()->where([])->all();
            $goodsInfo = \yii\helpers\ArrayHelper::map($goods, 'id', 'goods_name');
            $address = new \common\models\Address();

        ?>

        <div class="form-group field-jingxiaoshang-level" data-id="origin" data-val="province">
            <label class="control-label" for="jingxiaoshang-level">经销省区域</label>
            <select name="Jingxiaoshang[province]" class="form-control" data-title="请选择省份">
                <?php if ($model->province) : ?>
                    <option value="<?= $model->province ?>" selected><?= $address->getProvince($model->province); ?></option>
                <?php else : ?>
                    <option value="0">请选择省份</option>
                <?php endif; ?>
                <?php foreach ($provinces AS $province) : ?>
                    <option value="<?= $province['id'] ?>"><?= $province['cname'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group field-jingxiaoshang-level" data-id="origin" data-val="city">
            <label class="control-label" for="jingxiaoshang-level">经销市区域</label>
            <select name="Jingxiaoshang[city]" class="form-control" data-title="请选择城市">
                <?php if ($model->city) : ?>
                    <option value="<?= $address->city ?>" selected><?= $address->getProvince($model->city); ?></option>
                <?php else : ?>
                    <option value="0">请选择城市</option>
                <?php endif; ?>
            </select>
        </div>
        <div class="form-group field-jingxiaoshang-level" data-id="origin" data-val="region">
            <label class="control-label" for="jingxiaoshang-level">经销区县</label>
            <select name="Jingxiaoshang[region]" class="form-control" data-title="请选择区县">
                <?php if ($model->region) : ?>
                    <option value="<?= $model->region ?>" selected><?= $address->getProvince($model->region); ?></option>
                <?php else : ?>
                    <option value="0">请选择区县</option>
                <?php endif; ?>
            </select>
        </div>

        <?= $form->field($model, 'level')->dropDownList($model->getLevelDic())->label('权限分配'); ?>
        <?= $form->field($model, 'good_ids')->checkBoxList($goodsInfo)->label('代理商品'); ?>


        <div class="form-group">
            <?= Html::submitButton('保存', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
<script>
    var resetSelect = function(value, dom) {
        var data = provincesData[value];
        dom.empty().val('').append($('<option>').attr({value : 0}).text(dom.attr('data-title')));

        if (value) {
            for (var i=0; i<data.length; i++) {
                dom.append(
                    $('<option>').attr({value : data[i].id}).text(data[i].cname)
                );
            }
        }
    }
    $('select').change(function () {
        var name = $(this).attr('name'),
            that = $(this);

        switch (name) {
            case 'Jingxiaoshang[province]' :
                resetSelect(that.val(), $('select[name="Jingxiaoshang[city]"]'));
                resetSelect(0, $('select[name="Jingxiaoshang[region]"]'));
                break;
            case 'Jingxiaoshang[city]' :
                resetSelect(that.val(), $('select[name="Jingxiaoshang[region]"]'));
                break
        }
    });

    $('select[name="Jingxiaoshang[level]"]').click(function () {
        $('div[data-id="origin"]').hide();
        switch ($(this).val()) {
            default :
                $('div[data-id="origin"][data-val="region"]').show();
            case 'CITY' :
                $('div[data-id="origin"][data-val="city"]').show();
            case 'PROVINCE' :
                $('div[data-id="origin"][data-val="province"]').show();

        }
    });
    $('select[name="Jingxiaoshang[level]"]').click();
</script>