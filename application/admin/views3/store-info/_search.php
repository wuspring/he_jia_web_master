<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\StoreInfoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-info-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'tab_text') ?>

    <?= $form->field($model, 'describe') ?>

    <?= $form->field($model, 'store_amount') ?>

    <?php // echo $form->field($model, 'store_info') ?>

    <?php // echo $form->field($model, 'judge_amount') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
