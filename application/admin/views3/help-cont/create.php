<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HelpCont */

$this->title = '添加内容';
$this->params['breadcrumbs'][] = ['label' => '帮助中心', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '信息列表', 'url' => ['help-cont/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="panel">
    <div class="panel-heading">
      <div class="panel-title">
          <?= Html::encode($this->title) ?>
      </div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
