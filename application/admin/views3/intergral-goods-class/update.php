<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Goodsclassify */

$this->title = '修改商品分类: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => '积分商城', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '商品分类', 'url' =>['intergral-goods-class/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'parent'=>$parent,
    ]) ?>

</div>