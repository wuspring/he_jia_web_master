<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '网站留言';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'member_id',
            array(
                'format' => 'raw',
                'attribute' => 'member_id',
                'label'=>'会员账号',
                'value'=>function($filterModel){
                    return $filterModel->member->username;
                },
            ),
            'info:ntext',
            'createTime',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
