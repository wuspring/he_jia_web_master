<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\IntergralArticle */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Intergral Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intergral-article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'content:ntext',
            'read_num',
            'is_recommend',
            'create_time',
            'is_del',
            'is_show',
        ],
    ]) ?>

</div>
