<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FloorWap */

$this->title = '添加';
$this->params['breadcrumbs'][] = ['label' => '手机端', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '手机端楼层管理', 'url' => ['floor-wap/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
