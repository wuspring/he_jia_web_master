<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '城市设置';
$this->params['breadcrumbs'][] = ['label' => '城市管理', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">
        <form action="" method="post">
            <div class="panel-body">
                <div class="row" style="text-align: right">
                    <button type="submit" style="float: right;" class="btn btn-primary center-block"> 提交</button>
                </div>
            </div>

            <input type="text" hidden name="r" value="select-city/index">
            <input type="hidden" name="_csrf" id='csrf' value="<?= Yii::$app->request->csrfToken ?>">
            <?php  foreach (\common\models\Provinces::provinceCity() as $k=>$v):?>
         <div>
             <div class="panel">
                 <div class="panel-heading">
                     <span class="panel-title"><?=$v['cname']?></span>
                 </div>
                 <div class="panel-body">
                  <?php foreach ($v['child'] as $k2=>$v2):?>

                     <?php  if (in_array($v2['id'],$cityArr)):?>
                          <label><input  name="city<?=$v2['id']?>" type="checkbox" checked="checked" value="<?=$v2['id']?>" /><?=$v2['cname']?> </label>
                      <?php else:?>
                          <label><input  name="city<?=$v2['id']?>" type="checkbox" value="<?=$v2['id']?>" /><?=$v2['cname']?> </label>
                     <?php endif;?>

                  <?php endforeach;?>
                 </div>
         </div>
        <?php endforeach;?>


       </form>

    </div>



</div>
