<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\CartSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cart-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'buyerId') ?>

    <?= $form->field($model, 'goodsId') ?>

    <?= $form->field($model, 'goodsName') ?>

    <?= $form->field($model, 'goodsPrice') ?>

    <?php // echo $form->field($model, 'goodsNum') ?>

    <?php // echo $form->field($model, 'attr') ?>

    <?php // echo $form->field($model, 'sku_id') ?>

    <?php // echo $form->field($model, 'goods_image') ?>

    <?php // echo $form->field($model, 'ispay') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
