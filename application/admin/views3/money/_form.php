<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Money */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="money-form">


    <?php
    $moneyModel = new \common\models\Money();
    $form = ActiveForm::begin(); ?>
    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton('审核' , ['class' => 'btn btn-primary']) ?>
    </div>

    <?= $form->field($model, 'status')->dropDownList($moneyModel->getStatusDic()) ?>

    <?php ActiveForm::end(); ?>

</div>
