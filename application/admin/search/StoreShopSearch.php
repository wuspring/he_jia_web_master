<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoreShop;

/**
 * StoreShopSearch represents the model behind the search form about `common\models\StoreShop`.
 */
class StoreShopSearch extends StoreShop
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'provinces_id'], 'integer'],
            [['store_name', 'description', 'address', 'work_days', 'work_times', 'notice', 'picitures'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreShop::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'provinces_id' => $this->provinces_id,
        ]);

        $query->andFilterWhere(['like', 'store_name', $this->store_name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'work_days', $this->work_days])
            ->andFilterWhere(['like', 'work_times', $this->work_times])
            ->andFilterWhere(['like', 'notice', $this->notice])
            ->andFilterWhere(['like', 'picitures', $this->picitures]);

        return $dataProvider;
    }
}
