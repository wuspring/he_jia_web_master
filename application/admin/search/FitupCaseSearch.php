<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FitupCase;

/**
 * FitupCaseSearch represents the model behind the search form about `common\models\FitupCase`.
 */
class FitupCaseSearch extends FitupCase
{
    public $likeCity;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'house_type', 'design_style', 'is_del'], 'integer'],
            [['fitup_company', 'address', 'design', 'area', 'budget', 'tab_text', 'tab', 'create_time','likeCity'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FitupCase::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'house_type' => $this->house_type,
            'design_style' => $this->design_style,
            'create_time' => $this->create_time,
            'is_del' => $this->is_del,
        ]);

        $query->andFilterWhere(['like', 'fitup_company', $this->fitup_company])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'design', $this->design])
            ->andFilterWhere(['like', 'area', $this->area])
            ->andFilterWhere(['like', 'budget', $this->budget])
            ->andFilterWhere(['like', 'tab_text', $this->tab_text])
            ->andFilterWhere(['like', 'tab', $this->tab]);

        return $dataProvider;
    }
}
