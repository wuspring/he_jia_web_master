<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TicketApply as TicketApplyModel;

/**
 * TicketApply represents the model behind the search form about `common\models\TicketApply`.
 */
class TicketApplySearch extends TicketApplyModel
{
    public $ticketIds;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'ticket_id', 'status'], 'integer'],
            [['zw_pos', 'reason', 'good_ids', 'create_time', 'ticketIds'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TicketApplyModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'ticket_id' => $this->ticket_id,
            'status' => $this->status,
            'create_time' => $this->create_time,
        ]);

        $query->andFilterWhere(['like', 'zw_pos', $this->zw_pos])
            ->andFilterWhere(['like', 'reason', $this->reason])
            ->andFilterWhere(['in', 'ticket_id', $this->ticketIds])
            ->andFilterWhere(['like', 'good_ids', $this->good_ids]);

        return $dataProvider;
    }
}
