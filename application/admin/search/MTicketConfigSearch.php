<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MTicketConfig;

/**
 * MTicketConfigSearch represents the model behind the search form about `common\models\MTicketConfig`.
 */
class MTicketConfigSearch extends MTicketConfig
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ticket_id'], 'integer'],
            [['banners', 'colors', 'guiders', 'top_guiders', 'ad_1', 'ad_2', 'ad_3', 'ad_4', 'create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MTicketConfig::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ticket_id' => $this->ticket_id,
            'create_time' => $this->create_time,
        ]);

        $query->andFilterWhere(['like', 'banners', $this->banners])
            ->andFilterWhere(['like', 'colors', $this->colors])
            ->andFilterWhere(['like', 'top_guiders', $this->top_guiders])
            ->andFilterWhere(['like', 'guiders', $this->guiders])
            ->andFilterWhere(['like', 'ad_1', $this->ad_1])
            ->andFilterWhere(['like', 'ad_2', $this->ad_2])
            ->andFilterWhere(['like', 'ad_3', $this->ad_3])
            ->andFilterWhere(['like', 'ad_4', $this->ad_4]);

        return $dataProvider;
    }
}
