<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FitupAppointment;

/**
 * FitupAppointmentSearch represents the model behind the search form about `common\models\FitupAppointment`.
 */
class FitupAppointmentSearch extends FitupAppointment
{
    public $startTime;
    public $endTime;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'member_id', 'is_del', 'is_deal'], 'integer'],
            [['name', 'mobile', 'create_time', 'dispatch_people'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FitupAppointment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'member_id' => $this->member_id,
            'create_time' => $this->create_time,
            'is_del' => $this->is_del,
            'is_deal' => $this->is_deal,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'dispatch_people', $this->dispatch_people])
            ->andFilterWhere(['>=', 'create_time', $this->startTime])
            ->andFilterWhere(['<', 'create_time', $this->endTime]);

        return $dataProvider;
    }
}
