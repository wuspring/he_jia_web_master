<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Postmethod;

/**
 * PostmethodSearch represents the model behind the search form about `common\models\Postmethod`.
 */
class PostmethodSearch extends Postmethod
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'first_condition', 'first_money', 'other_condition', 'other_money', 'status'], 'integer'],
            [['name', 'code', 'createTime', 'send_area', 'not_area'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Postmethod::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'first_condition' => $this->first_condition,
            'first_money' => $this->first_money,
            'other_condition' => $this->other_condition,
            'other_money' => $this->other_money,
            'status' => $this->status,
            'createTime' => $this->createTime,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'send_area', $this->send_area])
            ->andFilterWhere(['like', 'not_area', $this->not_area]);

        return $dataProvider;
    }
}
