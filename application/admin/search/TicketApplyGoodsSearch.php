<?php

namespace app\search;

use common\models\Goods;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TicketApplyGoods;

/**
 * TicketApplyGoodsSearch represents the model behind the search form about `common\models\TicketApplyGoods`.
 */
class TicketApplyGoodsSearch extends TicketApplyGoods
{
    public $title;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ticket_id', 'user_id', 'good_id','sort'], 'integer'],
            [['good_pic', 'type', 'create_time', 'title'], 'safe'],
            [['good_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TicketApplyGoods::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $goodIds = [];
        if ($this->good_id) {
            $goodIds = [$this->good_id];
        } else {
            if ($this->title) {
                $goods = Goods::find()->where([
                    'and',
                    ['like', 'goods_name', $this->title],
                    ['=', 'isdelete', 0],
                    ['=', 'goods_state', 1],
                ])->all();

                if ($goods) {
                    $goodIds = arrayGroupsAction($goods, function ($good) {
                        return $good->id;
                    });
                }
            }
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ticket_id' => $this->ticket_id,
            'user_id' => $this->user_id,
//            'good_id' => $this->good_id,
            'good_price' => $this->good_price,
            'create_time' => $this->create_time,
            'sort'=>$this->sort,
        ]);

        $query->andFilterWhere(['like', 'good_pic', $this->good_pic])
            ->andFilterWhere(['in', 'good_id', $goodIds])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
