<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'role', 'sex', 'status', 'userType', 'user_type'], 'integer'],
            [['username', 'password', 'nickname','birthday', 'avatarTm', 'avatar', 'email', 'createTime', 'modifyTime', 'last_visit', 'authkey', 'accessToken', 'assignment', 'end_time', 'provinces_id'], 'safe'],
            [['remainder'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'role' => $this->role,
            'sex' => $this->sex,
            'birthday' => $this->birthday,
            'status' => $this->status,
            'createTime' => $this->createTime,
            'modifyTime' => $this->modifyTime,
            'last_visit' => $this->last_visit,
            'userType' => $this->userType,
            'remainder' => $this->remainder,
            'user_type' => $this->user_type,
            'assignment' => $this->assignment,
            'provinces_id' => $this->provinces_id,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'nickname', $this->nickname])
            ->andFilterWhere(['like', 'avatarTm', $this->avatarTm])
            ->andFilterWhere(['like', 'avatar', $this->avatar])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'authkey', $this->authkey])
            ->andFilterWhere(['like', 'accessToken', $this->accessToken]);

        return $dataProvider;
    }
}
