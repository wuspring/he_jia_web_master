<?php

namespace admin\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TicketInfo;

/**
 * TicketInfoSearch represents the model behind the search form about `common\models\TicketInfo`.
 */
class TicketInfoSearch extends TicketInfo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id', 'provinces_id', 'ticket_amount'], 'integer'],
            [['ticket_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TicketInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ticket_id' => $this->ticket_id,
            'provinces_id' => $this->provinces_id,
            'ticket_amount' => $this->ticket_amount,
        ]);

        $query->andFilterWhere(['like', 'ticket_name', $this->ticket_name]);

        return $dataProvider;
    }
}
