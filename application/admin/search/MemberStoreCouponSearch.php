<?php

namespace app\search;

use common\models\MemberStoreCoupon;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MemberCoupon;

/**
 * MemberCouponSearch represents the model behind the search form about `common\models\MemberCoupon`.
 */
class MemberStoreCouponSearch extends MemberCoupon
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_id', 'member_id', 'coupon_id', 'coupon_auto_send_id', 'is_expire', 'is_use', 'is_delete', 'type', 'integral'], 'integer'],
            [['begin_time', 'end_time', 'addtime'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MemberStoreCoupon::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'store_id' => $this->store_id,
            'member_id' => $this->member_id,
            'coupon_id' => $this->coupon_id,
            'coupon_auto_send_id' => $this->coupon_auto_send_id,
            'begin_time' => $this->begin_time,
            'end_time' => $this->end_time,
            'is_expire' => $this->is_expire,
            'is_use' => $this->is_use,
            'is_delete' => $this->is_delete,
            'addtime' => $this->addtime,
            'type' => $this->type,
            'integral' => $this->integral,
            'price' => $this->price,
        ]);

        return $dataProvider;
    }
}
