<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\GoodsSku;

/**
 * GoodsSkuSearch represents the model behind the search form about `common\models\GoodsSku`.
 */
class GoodsSkuSearch extends GoodsSku
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'goods_id', 'num', 'integral'], 'integer'],
            [['attr', 'goods_code', 'picimg'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GoodsSku::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'goods_id' => $this->goods_id,
            'num' => $this->num,
            'price' => $this->price,
            'integral' => $this->integral,
        ]);

        $query->andFilterWhere(['like', 'attr', $this->attr])
            ->andFilterWhere(['like', 'goods_code', $this->goods_code])
            ->andFilterWhere(['like', 'picimg', $this->picimg]);

        return $dataProvider;
    }
}
