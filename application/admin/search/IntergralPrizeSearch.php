<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\IntergralPrize;

/**
 * IntergralPrizeSearch represents the model behind the search form about `common\models\IntergralPrize`.
 */
class IntergralPrizeSearch extends IntergralPrize
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'prize_terms', 'is_del', 'is_show','pid_terms'], 'integer'],
            [['prize_name', 'prize_grade', 'prize_describe', 'create_time'], 'safe'],
            [['prize_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IntergralPrize::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pid_terms'=>$this->pid_terms,
            'prize_terms' => $this->prize_terms,
            'prize_price' => $this->prize_price,
            'create_time' => $this->create_time,
            'is_del' => 0,
            'is_show' => $this->is_show,
        ]);

        $query->andFilterWhere(['like', 'prize_name', $this->prize_name])
            ->andFilterWhere(['like', 'prize_grade', $this->prize_grade])
            ->andFilterWhere(['like', 'prize_describe', $this->prize_describe]);

        return $dataProvider;
    }
}
