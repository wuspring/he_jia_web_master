<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Newsclassify;

/**
 * NewsclassifySearch represents the model behind the search form about `common\models\Newsclassify`.
 */
class NewsclassifySearch extends Newsclassify
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fid', 'isShow', 'isRdm', 'sort'], 'integer'],
            [['name', 'createTime', 'modifyTime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Newsclassify::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'fid' => $this->fid,
            'isShow' => $this->isShow,
            'isRdm' => $this->isRdm,
            'sort' => $this->sort,
            'createTime' => $this->createTime,
            'modifyTime' => $this->modifyTime,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
