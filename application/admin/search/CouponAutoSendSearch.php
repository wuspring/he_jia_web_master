<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CouponAutoSend;

/**
 * CouponAutoSendSearch represents the model behind the search form about `common\models\CouponAutoSend`.
 */
class CouponAutoSendSearch extends CouponAutoSend
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_id', 'coupon_id', 'event', 'send_times', 'addtime', 'is_delete'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CouponAutoSend::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'store_id' => $this->store_id,
            'coupon_id' => $this->coupon_id,
            'event' => $this->event,
            'send_times' => $this->send_times,
            'addtime' => $this->addtime,
            'is_delete' => $this->is_delete,
        ]);

        return $dataProvider;
    }
}
