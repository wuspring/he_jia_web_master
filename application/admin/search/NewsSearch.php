<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\News;

/**
 * NewsSearch represents the model behind the search form about `common\models\News`.
 */
class NewsSearch extends News
{
    public $likeCity;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cid', 'isShow', 'isRmd', 'clickRate', 'uid', 'provinces_id', 'sort'], 'integer'],
            [['title', 'themeImg', 'themeImgTm', 'brief', 'content', 'seokey', 'seoDepict', 'writer', 'createTime', 'modifyTime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'sort' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {

            return $dataProvider;
        }
        if ($this->cid==-1){
            $query->andFilterWhere(['>','cid',0]);
        }else{
            $query->andFilterWhere(['cid' => $this->cid]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'isShow' => $this->isShow,
            'isRmd' => $this->isRmd,
            'clickRate' => $this->clickRate,
            'uid' => $this->uid,
            'createTime' => $this->createTime,
            'modifyTime' => $this->modifyTime,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'themeImg', $this->themeImg])
            ->andFilterWhere(['like', 'provinces_id', $this->likeCity])
            ->andFilterWhere(['like', 'themeImgTm', $this->themeImgTm])
            ->andFilterWhere(['like', 'brief', $this->brief])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'seokey', $this->seokey])
            ->andFilterWhere(['like', 'seoDepict', $this->seoDepict])
            ->andFilterWhere(['like', 'writer', $this->writer]);

        return $dataProvider;
    }
}
