<?php

namespace app\search;

use wap\models\GoodsClass;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Goods;

/**
 * GoodsSearch represents the model behind the search form about `common\models\Goods`.
 */
class GoodsSearch extends Goods
{
    public $ids;
    //判断是 管理员还是后台操作人员
    public  $assignment;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gc_id', 'user_id', 'use_attr', 'brand_id', 'goods_promotion_type', 'goods_storage_alarm', 'goods_click', 'goods_salenum', 'goods_collect', 'goods_storage', 'goods_state', 'goods_vat', 'goods_commend', 'evaluation_count', 'is_virtual', 'is_appoint', 'is_presell', 'have_gift', 'isdelete', 'type', 'integral'], 'integer'],
            [['goods_name', 'goods_jingle', 'goods_serial', 'attr', 'goods_spec', 'goods_body', 'mobile_body', 'goods_image', 'goods_addtime', 'goods_edittime','user_id', 'ids'], 'safe'],
            [['goods_price', 'goods_promotion_price', 'goods_marketprice', 'evaluation_good_star'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Goods::find()->alias('g');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'g.id' => $this->id,
//            'gc_id' => $this->gc_id,
            'g.user_id' => $this->user_id,
            'g.use_attr' => $this->use_attr,
            'g.brand_id' => $this->brand_id,
            'g.goods_price' => $this->goods_price,
            'g.goods_promotion_price' => $this->goods_promotion_price,
            'g.goods_promotion_type' => $this->goods_promotion_type,
            'g.goods_marketprice' => $this->goods_marketprice,
            'g.goods_storage_alarm' => $this->goods_storage_alarm,
            'g.goods_click' => $this->goods_click,
            'g.goods_salenum' => $this->goods_salenum,
            'g.goods_collect' => $this->goods_collect,
            'g.goods_storage' => $this->goods_storage,
            'g.goods_state' => $this->goods_state,
            'g.goods_addtime' => $this->goods_addtime,
            'g.goods_edittime' => $this->goods_edittime,
            'g.goods_vat' => $this->goods_vat,
            'g.goods_commend' => $this->goods_commend,
            'g.evaluation_good_star' => $this->evaluation_good_star,
            'g.evaluation_count' => $this->evaluation_count,
            'g.is_virtual' => $this->is_virtual,
            'g.is_appoint' => $this->is_appoint,
            'g.is_presell' => $this->is_presell,
            'g.have_gift' => $this->have_gift,
            'g.type' => $this->type,
            'g.integral' => $this->integral,
            'g.isdelete' => 0,
        ]);

        $this->ids and $query->andFilterWhere(['in', 'g.id', $this->ids]);

        if ($this->gc_id) {
            $lists = [$this->gc_id];
             $this->getGcLists($this->gc_id, $lists);

            $query->andFilterWhere(['in', 'g.gc_id', $lists]);
        }

        $query->andFilterWhere(['like', 'g.goods_name', $this->goods_name])
            ->andFilterWhere(['like', 'g.goods_jingle', $this->goods_jingle])
            ->andFilterWhere(['like', 'g.goods_serial', $this->goods_serial])
            ->andFilterWhere(['like', 'g.attr', $this->attr])
            ->andFilterWhere(['like', 'g.goods_spec', $this->goods_spec])
            ->andFilterWhere(['like', 'g.goods_body', $this->goods_body])
            ->andFilterWhere(['like', 'g.mobile_body', $this->mobile_body])
            ->andFilterWhere(['like', 'g.goods_image', $this->goods_image]);

        return $dataProvider;
    }

    public function getGcLists($pid = 0, &$lists = array(), $deep = 1)
    {
        $parentData = GoodsClass::find()->where(['fid'=>$pid])->orderBy('sort asc')->all();
        if (!empty($parentData)) {

            foreach ($parentData as $key => $value) {
                $id = $value->id;
                $lists[] = $id;
                $this->getGcLists($id, $lists, ++$deep); //进入子类之前深度+1
                --$deep; //从子类退出之后深度-1
            }
        }
        return $lists;
    }
}
