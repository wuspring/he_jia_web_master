<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Advertising;

/**
 * AdvertisingSearch represents the model behind the search form about `common\models\Advertising`.
 */
class AdvertisingSearch extends Advertising
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'adid', 'isShow', 'clickRate', 'sort', 'isTime'], 'integer'],
            [['title', 'picture', 'pictureTm', 'url', 'describe', 'startTime', 'endTime', 'createTime', 'modifyTime','province_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Advertising::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>['defaultOrder'=>['adid'=>SORT_ASC,'sort'=>SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'adid' => $this->adid,
            'isShow' => $this->isShow,
            'clickRate' => $this->clickRate,
            'sort' => $this->sort,
            'isTime' => $this->isTime,
            'startTime' => $this->startTime,
            'endTime' => $this->endTime,
            'createTime' => $this->createTime,
            'modifyTime' => $this->modifyTime,
            'province_id' => $this->province_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'picture', $this->picture])
            ->andFilterWhere(['like', 'pictureTm', $this->pictureTm])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'describe', $this->describe]);

        return $dataProvider;
    }
}
