<?php

namespace admin\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TicketAsk;

/**
 * TicketAskSearch represents the model behind the search form about `common\models\TicketAsk`.
 */
class TicketAskSearch extends TicketAsk
{
     public   $ticket_type;

     public $start_time;
     public $end_time;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'provinces_id', 'ticket_id', 'ticket_amount', 'status'], 'integer'],
            [['name', 'mobile', 'address', 'resource', 'create_time','ticket_type', 'start_time', 'end_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TicketAsk::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['<','status',2]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'provinces_id' => $this->provinces_id,
//            'ticket_id' => $this->ticket_id,
            'ticket_amount' => $this->ticket_amount,
            'create_time' => $this->create_time,
        ]);

         //如果 ticket_type值为空   ticket_ask中是不可能存在符合的ticket_type
        if (empty($this->ticket_type)){
            $query->andFilterWhere(['ticket_id' =>0]);
        }else{
            $query->andFilterWhere(['in', 'ticket_id', $this->ticket_type]);
         }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['>=', 'create_time', $this->start_time])
            ->andFilterWhere(['<', 'create_time', $this->end_time])
            ->andFilterWhere(['like', 'resource', $this->resource]);


        return $dataProvider;
    }
}
