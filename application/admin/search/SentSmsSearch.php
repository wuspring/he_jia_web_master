<?php

namespace admin\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SentSms;

/**
 * SentSmsSearch represents the model behind the search form about `common\models\SentSms`.
 */
class SentSmsSearch extends SentSms
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_show', 'is_del', 'is_access_keys'], 'integer'],
            [['access_key_id', 'access_key_secret', 'sms_code', 'sms_name', 'type', 'create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SentSms::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_show' => $this->is_show,
            'is_del' =>0,
            'is_access_keys' => $this->is_access_keys,
            'create_time' => $this->create_time,
        ]);

        $query->andFilterWhere(['like', 'access_key_id', $this->access_key_id])
            ->andFilterWhere(['like', 'access_key_secret', $this->access_key_secret])
            ->andFilterWhere(['like', 'sms_code', $this->sms_code])
            ->andFilterWhere(['like', 'sms_name', $this->sms_name])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
