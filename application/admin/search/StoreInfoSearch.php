<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoreInfo;

/**
 * StoreInfoSearch represents the model behind the search form about `common\models\StoreInfo`.
 */
class StoreInfoSearch extends StoreInfo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'store_amount', 'judge_amount'], 'integer'],
            [['tab_text', 'describe', 'store_info'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'store_amount' => $this->store_amount,
            'judge_amount' => $this->judge_amount,
        ]);

        $query->andFilterWhere(['like', 'tab_text', $this->tab_text])
            ->andFilterWhere(['like', 'describe', $this->describe])
            ->andFilterWhere(['like', 'store_info', $this->store_info]);

        return $dataProvider;
    }
}
