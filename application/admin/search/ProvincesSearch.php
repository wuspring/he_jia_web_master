<?php

namespace app\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Provinces;

/**
 * ProvincesSearch represents the model behind the search form of `app\models\Provinces`.
 */
class ProvincesSearch extends Provinces
{
    public $in_level;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'upid', 'level'], 'integer'],
            [['cname', 'ename', 'pinyin'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Provinces::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'upid' => $this->upid,
            'level' => $this->level,
        ]);

        $query->andFilterWhere(['like', 'cname', $this->cname])
            ->andFilterWhere(['in', 'in_level', $this->in_level])
            ->andFilterWhere(['like', 'ename', $this->ename])
            ->andFilterWhere(['like', 'pinyin', $this->pinyin]);

        return $dataProvider;
    }
}
