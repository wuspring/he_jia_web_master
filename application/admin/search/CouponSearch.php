<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Coupon;

/**
 * CouponSearch represents the model behind the search form about `common\models\Coupon`.
 */
class CouponSearch extends Coupon
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_id', 'discount_type', 'expire_type', 'expire_day', 'is_delete', 'total_count', 'is_join', 'sort', 'is_integral', 'integral', 'total_num', 'type', 'user_num'], 'integer'],
            [['name', 'desc', 'pic_url', 'begin_time', 'end_time', 'addtime', 'goods_id_list'], 'safe'],
            [['min_price', 'sub_price', 'discount', 'price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Coupon::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->orderBy('sort ASC,addtime DESC');
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'store_id' => $this->store_id,
            'discount_type' => $this->discount_type,
            'min_price' => $this->min_price,
            'sub_price' => $this->sub_price,
            'discount' => $this->discount,
            'expire_type' => $this->expire_type,
            'expire_day' => $this->expire_day,
            'begin_time' => $this->begin_time,
            'end_time' => $this->end_time,
            'addtime' => $this->addtime,
            'is_delete' => 0,
            'total_count' => $this->total_count,
            'is_join' => $this->is_join,
            'sort' => $this->sort,
            'is_integral' => $this->is_integral,
            'integral' => $this->integral,
            'price' => $this->price,
            'total_num' => $this->total_num,
            'type' => $this->type,
            'user_num' => $this->user_num,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'pic_url', $this->pic_url])
            ->andFilterWhere(['like', 'goods_id_list', $this->goods_id_list]);

        return $dataProvider;
    }
}
