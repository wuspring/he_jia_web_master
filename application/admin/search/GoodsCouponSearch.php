<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\GoodsCoupon;

/**
 * GoodsCouponSearch represents the model behind the search form about `common\models\GoodsCoupon`.
 */
class GoodsCouponSearch extends GoodsCoupon
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'goods_id', 'user_id', 'ticket_id'], 'integer'],
            [['money', 'condition'], 'number'],
            [['create_time', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GoodsCoupon::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>['defaultOrder'=>['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'goods_id' => $this->goods_id,
            'money' => $this->money,
            'condition' => $this->condition,
            'create_time' => $this->create_time,
            'type' => $this->type,
            'is_del'=>0,
        ]);

        return $dataProvider;
    }
}
