<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '栏目专区';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="panel-body">
        <?= Html::a('添加栏目', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            [
//                    'attribute' => 'title',
//                    'value' => buildArticleList($filterModel)
//            ],
            [
                'attribute' => 'title',
            ],
//            [
//                'attribute' => 'parent_id',
//                'value' => function ($filterModel) {
//                   return $filterModel->parent_id ? $filterModel->parentCategory->title : '-';
//                },
//            ],
//            'keyword',
//            'description',
//            [
//                'attribute' => 'icon',
//                'format' => 'raw',
//                'value' => function ($filterModel) {
//                    return Html::img($filterModel->icon, ['width' => '60px']);
//                }
//            ],
//             'rank',

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'操作',
                'template' => '{update}'
            ],
        ],
    ]); ?>
    </div>
</div>

<script src="/public/js/jquery-1.8.3.min.js"></script>
<script>
    $(function () {
        $('#w0 .form-group').addClass('col-sm-2').last().css('margin-top', '25px');
        $('#w0 .form-group').removeClass('form-group');
        $('#w0').addClass('form-group');
    })
</script>