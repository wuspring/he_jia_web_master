<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
    </div>

    <?php
    $categorys = \common\models\Category::find()->where([
        'parent_id' => 0
    ])->orderBy('parent_id asc, id asc')->all();
    $listData = \yii\helpers\ArrayHelper::map($categorys,'id','title');

    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id', ['options' => ['style' => 'display:none']])->hiddenInput(['value' => 0]) ?>
    <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>

    <?php ActiveForm::end(); ?>

</div>
<script>
    var uploadPath = '<?php echo ADMIN_URL ?>' + '/upload/img';
</script>

<script src="/public/js/jquery.min.js"></script>
<script src="/public/js/base_upload.js"></script>
<script src="/public/js/upload-hooks/preview.js"></script>
<script>
    $(function(){
        var Config  = [
            {category : 'category', id : 'icon', type : 'pictures'}
        ];
        previewCtrl(Config);
    });
</script>