<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Newsclassify */

$this->title = '添加文章分类';
$this->params['breadcrumbs'][] = ['label' => 'Newsclassifies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

   	<div class="panel-heading">
		<span class="panel-title"><?= Html::encode($this->title) ?></span>
	</div>

    <?= $this->render('_form', [
        'model' => $model,
        'parent'=>$parent,
    ]) ?>

</div>
