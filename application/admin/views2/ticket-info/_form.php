<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TicketInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ticket-info-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $form->field($model, 'ticket_id')->textInput() ?>

    <?= $form->field($model, 'provinces_id')->textInput() ?>

    <?= $form->field($model, 'ticket_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ticket_amount')->textInput(['maxlength' => true]) ?>

    <?php ActiveForm::end(); ?>

</div>
