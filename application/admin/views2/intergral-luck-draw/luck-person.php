<?php

use yii\helpers\Html;
use yii\grid\GridView;



$this->title = '获奖者';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <div class="panel-title"><?= Html::encode($this->title) ?></div>
    </div>

     <div class="panel-body">
         <?= GridView::widget([
             'dataProvider' => $dataProvider,
             'columns' => [
                 ['class' => 'yii\grid\SerialColumn'],

                 [
                     'attribute'=>'member_id',
                     'label'=>'会员',
                     'value'=>function($fildmodel){
                         return $fildmodel->member->mobile;
                     }
                 ],
                 [
                     'attribute'=>'prize_terms',
                     'label'=>'第几期'
                 ],
                 [
                     'attribute'=>'draw_id',
                     'label'=>'获奖等级',
                     'value'=>function($fildmodel){
                         return $fildmodel->intergralTurntable->prize;
                     }
                 ],
                 [
                     'attribute'=>'create_time',
                     'label'=>'获奖时间',

                 ],

                 [
                     'class' => 'yii\grid\ActionColumn',
                     'header'=>'操作',
                     'template' => '{view}',
                 ],
             ],
         ]); ?>
     </div>

</div>
