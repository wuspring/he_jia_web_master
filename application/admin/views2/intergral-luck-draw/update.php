<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\IntergralLuckDraw */

$this->title = 'Update Intergral Luck Draw: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Intergral Luck Draws', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="intergral-luck-draw-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
