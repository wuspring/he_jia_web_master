<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\search\GoodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '商品列表';
$this->params['breadcrumbs'][] = $this->title;

//if ($assignment){
    $findContent=['goods_name'=>'商品名称','user_id'=>'店铺名称'];
//}else{
//    $findContent=['goods_name'=>'商品名称'];
//}

global $nowCoupon;
$nowCoupon = $coupon;
?>
<style>
    #form .col-md-12,.col-md-1,.col-md-3{padding-left: 0px}
    .a_margin{margin-left: 10px}
</style>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>
<script>
    var addCouponGoodIds = <?php
        $carts = \common\models\CouponGoodsCart::findAll([
                'mcoupon_id' => $nowCoupon->id
        ]);

        $cartIds = $carts ? arrayGroupsAction($carts, function ($cart) {
            return $cart->good_id;
        }) : [];
        echo json_encode($cartIds);
    ?>;
</script>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php include __DIR__ . '/header.php'; ?>
    <div class="panel-body">
        <?php if ($nowCoupon->goodsCoupon->type == 'ticket') :?>
        <div class="panel-body" style="text-align: right">
            <?= Html::a('结算', ['use-group-store-coupon', 'code' => $nowCoupon->code], ['class' => 'btn btn-success']) ?>
        </div>
        <?php endif; ?>
        <?= GridView::widget([
            'dataProvider' => $search,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                array(
                    'format' => 'raw',
                    'attribute' => 'goods_image',
                    'label'=>'缩略图',
                    'value'=>function($filterModel){
                        $itm=$filterModel->goods_pic;
                        if ($itm){
                            return Html::img(yii::$app->request->hostInfo.$itm, ['width' =>30]);
                        }else{
                            return '无';
                        }

                    },
                ),
                array(
                    'attribute' => 'goods_name',
                    'label'=>'商品名称',
                    'value'=>function($filterModel){
                        return StringHelper::truncate($filterModel->goods_name, 20);

                    },
                ),
                array(
                    'attribute' => 'user_id',
                    'label'=>'店铺名称',
                    'value'=>function($filterModel){
                        return $filterModel->user->nickname;

                    },
                ),
                array(
                    'attribute' => 'gc_id',
                    'label'=>'商品分类',
                    'value'=>function($filterModel){
                        return $filterModel->gclass->name;

                    },
                ),
//                array(
//                    'format' => 'raw',
//                    'attribute' => 'brand_id',
//                    'label'=>'品牌',
//                    'value'=>function($filterModel){
//                        if ($filterModel->branddata->show_type==0){
//                            return Html::img(yii::$app->request->hostInfo.$filterModel->branddata->brand_pic, ['width' =>30]);
//                        }else{
//                            return $filterModel->branddata->brand_name;
//                        }
//                    },
//                ),
                'goods_price',
//                'goods_click',
//                'goods_salenum',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>'操作',
                    'template' => '{action} {add}',
                    'buttons'=>[
                        'action'=> function ($url, $model, $key) {
                            global $nowCoupon;
                            if ($nowCoupon->goodsCoupon->goods_id > 0) {
                                return  Html::a('<span class="btn btn-info btn-xs">使用</span>', 'javascript:void(0)', ['title' => '使用','data-toggle'=>'modal', 'data-code'=>$nowCoupon->code, 'data-val'=> $model->id, 'data-id' => 'use','class'=>'a_margin'] ) ;
                            }

                        },

                        'add' => function ($url, $model, $key) {
                            global $nowCoupon;
                            if ($nowCoupon->goodsCoupon->goods_id < 0) {
                                return Html::a('<span class="btn btn-info btn-xs">添加</span>', 'javascript:void(0)', ['title' => '使用', 'data-id' => 'useButton', 'data-type' => 'add', 'data-coupon' => $nowCoupon->id, 'data-good' => $model->id, 'class' => 'a_margin']);
                            }
                        }
                    ],

                ],
            ],
        ]); ?>
    </div>

</div>
<script>
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    init.push(function () {

        $(document).on("click",".fahuo",function () {
            $("#ordercode").val($(this).attr('data-id'));

            $("#fahuoModal").modal();
        });

        var useCoupon = function(code, goodId) {
            var data = {
                code : code,
                id : goodId
            };

            if (confirm("确认使用优惠券？")) {console.log(data);
                $.get('<?php echo Url::toRoute(['coupon-verify/use-store-coupon'])?>', data, function (res) {
                    if(res.status) {
                        alert(res.msg);
                        window.location.href = '<?= \DL\service\UrlService::build('coupon-verify/store'); ?>';
                    }else{
                        alert(res.msg);
                    }
                },'json');
            }
        };

        $('[data-id="use"]').click(function () {
            var that = $(this);
            useCoupon(that.attr('data-code'), that.attr('data-val'));
        });

        $('[data-id=\'useButton\']').click(function () {
            var that = $(this),
                type = $(this).attr('data-type'),
                couponId = $(this).attr('data-coupon'),
                goodId = $(this).attr('data-good'),
                href, text, state;

            switch (type) {
                case 'add' :
                    href = '<?= \DL\service\UrlService::build('coupon-verify/add-group-strore-coupons');?>';
                    text = '移除';
                    state = 'remove';
                    break;
                case 'remove' :
                    href = '<?= \DL\service\UrlService::build('coupon-verify/remove-group-strore-coupons');?>';
                    text = '添加';
                    state = 'add';
                    break;
            }

            $.get(href, {mCouponId : couponId, goodId : goodId}, function (res) {
                res = JSON.parse(res);

                if (res.status) {
                    that.find('span').eq(0).text(text);
                    that.attr('data-type', state);

                    return false;
                }

                alert(res.msg)
            })
        });

        $(function () {
            $('[data-id=\'useButton\']').each(function () {
                var goodId = $(this).attr('data-good');
                goodId = parseInt(goodId);
                if (addCouponGoodIds.indexOf(goodId) > -1) {
                    $(this).find('span').eq(0).text('移除');
                    $(this).attr('data-type', 'remove');
                }
            })

        })
    });

</script>