<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = '预定优惠券使用记录';
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                array(
//                    'format' => 'raw',
//                    'attribute' => 'themeImg',
//                    'label' => '图像',
//                    'value' => function ($filterModel) {
//                        return Html::img(yii::$app->request->hostInfo . $filterModel->themeImg, ['width' => 30]);
//                    },
//                ),
                [
                    'attribute' => 'user_id',
                    'label' => '店铺',
                    'value' => function ($model) {
                        return $model->store->nickname;
                    }
                ],
                [
                    'attribute' => 'user_id',
                    'label' => '使用人',
                    'value' => function ($model) {
                        return $model->coupon->member->nickname;
                    }
                ],
                [
                    'attribute' => 'user_id',
                    'label' => '联系方式',
                    'value' => function ($model) {
                        return $model->coupon->member->mobile;
                    }
                ],
                [
                    'attribute' => 'code',
                    'label' => '使用券',
                    'value' => function ($model) {
                        return $model->coupon->code;
                    }
                ],
                [
                    'attribute' => 'code',
                    'label' => '使用商品',
                    'value' => function ($model) {
                        return $model->goods->goods_name;
                    }
                ],


//                [
//                    'class' => 'yii\grid\ActionColumn',
//                    'header' => '操作',
//                    'template' => '',
//                    'buttons' => [
//                        'view' => function ($url, $model, $key) {
//                            $url = Yii::$app->request->hostInfo . "/index.php?r=news/view&id=" . $model->id;
//                            return Html::a('<span class="glyphicon glyphicon-globe"></span>', $url, ['title' => '查看', 'data-toggle' => 'modal']);
//                        },
//                        'update' => function ($url, $model, $key) {
//                            return Html::a('<span class="fa fa-pencil"></span>', $url, ['title' => '更新', 'data-toggle' => 'modal']);
//                        },
//                        'delete' => function ($url, $model, $key) {
//                            $unDeleteArray = [1];
//                            if (!in_array($model->id,$unDeleteArray)) {
//                                return Html::a('<span class="fa fa-bitbucket"></span>', $url, ['title' => '删除', 'data-toggle' => 'modal']);
//                            }
//                        },
//                    ],

//                ],
            ],
        ]); ?>
    </div>
</div>