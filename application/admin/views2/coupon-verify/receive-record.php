<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = '优惠劵领取记录';
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute'=>'member_id',
                    'label'=>'会员账号',
                    'value'=>function($model){
                       return substr_cut($model->member->username);
                    }
                ],
                [
                    'attribute'=>'begin_time',
                    'label'=>'有效期开始时间',
                ],

                [
                    'attribute'=>'end_time',
                    'label'=>'有效期结束时间',
                ],
                [
                    'attribute'=>'addtime',
                    'label'=>'领取时间',
                ],

                [
                    'attribute'=>'good_id',
                    'label'=>'商品名称',
                    'value'=>function($model){
                        return $model->goodsCoupon->goods->goods_name;
                    }
                ],

            ],
        ]); ?>
    </div>

</div>
