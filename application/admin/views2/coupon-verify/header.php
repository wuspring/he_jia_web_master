<?php

use DL\service\UrlService;

$showSysCouponTab = isset($showSysCouponTab) ? $showSysCouponTab : false;
?>

<ul class="nav nav-tabs dl_tab">
    <li class="col-lg-2 tab_title <?=($active=='store')?'active':''?>"><a  href="<?=UrlService::build(['coupon-verify/store']);?>">店铺券</a></li>

    <?php if ($showSysCouponTab) :?>
    <li class="col-lg-2 tab_title <?=($active=='system')?'active':''?>"><a  href="<?=UrlService::build(['coupon-verify/index']);?>">定金券</a></li>
    <?php endif; ?>

    <li class="col-lg-2 tab_title <?=($active=='reserve')?'active':''?>"><a  href="<?=UrlService::build(['coupon-verify/reserve-verify']);?>">预约商品兑换</a></li>

</ul>

