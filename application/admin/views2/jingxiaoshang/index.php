<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\JingxiaoshangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '经销商列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'member_id',
                'label'=>'会员名称',
                'value'=>function($filterModel){
                    return $filterModel->member->wxnick;
                },
            ],
            [
                'attribute' => 'phone',
                'label'=>'联系方式',
            ],
            [
                'attribute' => 'name',
                'label'=>'申请人',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '操作',
                'template' => '{setting} {delete}',
                'buttons' => [
                    'setting' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::toRoute(['jingxiaoshang/setting', 'id' => $model->id]);
                        return Html::a('<span class=" fa  fa-cog"></span>', $url, ['title' => '设置', 'data-toggle' => 'modal']);
                    },
                ]
            ],
        ],
    ]); ?>
    </div>
</div>
