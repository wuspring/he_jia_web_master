<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\search\HelpTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '帮助中心类型';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .a_margin{
        margin-left: 10px;
    }
</style>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">
        <?= Html::a('添加类型', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'type',
                'sort',
                'create_time',
              //  'is_del',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => '操作',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {

                            return Html::a('查看', $url, ['title' => '查看', 'data-toggle' => 'modal','class'=>'a_margin']);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('更新', $url, ['title' => '更新', 'data-toggle' => 'modal','class'=>'a_margin']);
                        },
                        'delete' => function ($url, $model, $key) {

                            return Html::a('删除', $url, ['title' => '删除', 'data-toggle' => 'modal','class'=>'a_margin']);
                        },
                    ],

                ],
            ],
        ]); ?>
    </div>

</div>
