<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\search\FitupProblemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '装修问答';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            'problem:ntext',
            [
                'attribute' => 'member_id',
                'label' => '会员信息',
                'value' => function ($model) {
                    return $model->member->nickname;
                }
            ],
            'create_time',
//            'is_del',
            [
                'attribute' => 'is_show',
                'label' => '审核',
                'value' => function ($model) {
                    return (int)$model->is_show ? '通过' : '未通过';
                }
            ],
            [
                'attribute' => 'sort',
                'label' => '排序'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '操作',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url = '/fitup-school/answer-detail.html?id=' . $model->id;
                        return Html::a('查看', $url, ['title' => '查看', 'data-toggle' => 'modal','class'=>'a_margin']);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('审核', $url, ['title' => '审核', 'data-toggle' => 'modal','class'=>'a_margin']);
                    },
                    'delete' => function ($url, $model, $key) {
                        $unDeleteArray = [1];
                        if (!in_array($model->id,$unDeleteArray)) {
                            return Html::a('删除', $url, ['title' => '删除', 'data-toggle' => 'modal','class'=>'a_margin']);
                        }
                    },
                ],

            ],
        ],
    ]); ?>
    </div>
</div>
