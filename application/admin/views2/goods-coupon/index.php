<?php

use yii\helpers\Html;
use yii\grid\GridView;
use DL\service\UrlService;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '优惠券列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>

<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?=Html::encode($this->title)?></span>
    </div>

    <ul class="nav nav-tabs dl_tab">
        <li class="col-lg-2 tab_title <?=('goods'==$active)?'active':''?>"><a  href="<?=UrlService::build(['goods/update','id'=>$goodId])?>">商品</a></li>
        <li class="col-lg-2 tab_title <?=('coupon'==$active)?'active':''?>"><a  href="<?=UrlService::build(['goods/coupon-list','goodId'=>$goodId])?>">优惠劵</a></li>
    </ul>
    <div class="panel-body">
        <?= Html::a('添加优惠劵', ['create','goodId'=>$goodId], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'money',
//                [
//                       'attribute'=>'condition',
//                       'label'=>'最低使用额度'
//                ],
                'describe',
                [
                    'attribute'=>'valid_day',
                     'label'=>'有效期天数'
                ],
                'create_time',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => '操作',
                    'template' => '{view}{update}{delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('查看', $url, ['title' => '查看', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('更新', $url, ['title' => '更新', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('删除', $url, ['title' => '删除', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                        },
                    ],

                ],
            ],
        ]); ?>

    </div>

</div>












