<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TicketApply */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel-body form-controls-demo">
<div class="ticket-apply-form">


    <?php
        $form = ActiveForm::begin();

        $dic = $model->getStatusDic();
        if (isset($dic[0])) {
            unset($dic[0]);
        }
    ?>
    <div class="form-group" style="text-align: right">
        <?= Html::submitButton('提交', ['class' =>  'btn btn-primary']) ?>
    </div>

    <?= $form->field($model, 'id', ['options' => ['style' => 'display:none']])->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'user_id', ['options' => ['style' => 'display:none']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ticket_id', ['options' => ['style' => 'display:none']])->textInput() ?>

    <?php $form->field($model, 'zw_pos', [])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList($dic, ['prompt' => '请选择'])->label('审核') ?>

    <?= $form->field($model, 'reason')->textarea(['rows' => 6])->label('拒绝理由') ?>


    <div class="form-group">
        <?= Html::submitButton('提交', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
