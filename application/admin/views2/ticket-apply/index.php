<?php

use yii\helpers\Html;
use yii\grid\GridView;
use DL\service\UrlService;

/* @var $this yii\web\View */
/* @var $searchModel app\search\TicketApply */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '展会申请';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <ul class="nav nav-tabs dl_tab hide">
        <li class="col-lg-2 tab_title <?=($active=='JIE_HUN')?'active':''?>"><a  href="<?=UrlService::build(['ticket-apply/index','tab'=>'JIE_HUN']);?>">婚庆展</a></li>
        <li class="col-lg-2 tab_title <?=($active=='JIA_ZHUANG')?'active':''?>"><a  href="<?=UrlService::build(['ticket-apply/index','tab'=>'JIA_ZHUANG']);?>">家装展</a></li>
        <li class="col-lg-2 tab_title <?=($active=='YUN_YING')?'active':''?>"><a  href="<?=UrlService::build(['ticket-apply/index','tab'=>'YUN_YING']);?>">孕婴展</a></li>
    </ul>
    <div class="panel-body hide">
        <?= Html::a('展会详情', ['ticket/index', 'tab' => $active], ['class' => 'btn btn-primary']) ?>
    </div>
    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'user_id',
                'label' => '店铺名称',
                'value' => function($model) {
                    try {
                        $store = \DL\Project\Store::init()->info($model->user_id, false);
                    } catch (\Exception $e) {
                        $store = new \common\models\User();
                        $store->nickname = '商铺状态异常';
                    }
                    return $store->nickname;
                }
            ],
            [
                'attribute' => 'ticket_id',
                'label' => '展会名称',
                'value' => function($model) {
                    $ticket = \common\models\Ticket::findOne($model->ticket_id);
                    return $ticket->name;
                }
            ],
            [
                'attribute' => 'statusInfo',
                'label' => '申请状态',
            ],
            [
                'attribute' => 'create_time',
                'label' => '申请时间',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                 'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $url = UrlService::build(['ticket-apply/check', 'id' => $model->id]);
                        switch ($model->status){
                            case 0:
                                return Html::a('审核', $url, ['class'=> 'btn btn-default', 'title' => '审核']);
                                break;
                            case 1:
                                return Html::a('查看信息', $url, ['class'=> 'btn btn-default', 'title' => '查看信息']);
                                break;
                            case 2:
                                return Html::a('查看信息', $url, ['class'=> 'btn btn-default', 'title' => '查看信息']);
                                break;
                        }

                    },
                ]
            ]
        ],
    ]); ?>
    </div>
</div>
