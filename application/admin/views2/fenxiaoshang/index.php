<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\FenxiaoshangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '分销商列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'member_id',
                'label'=>'会员名称',
                'value'=>function($filterModel){
                    return $filterModel->member->wxnick;
                },
            ],
            [
                'attribute' => 'phone',
                'label'=>'联系方式',
            ],
            [
                'attribute' => 'name',
                'label'=>'申请人',
            ],
            [
                'attribute' => 'recommend',
                'label'=>'推荐人',
            ],
        ],
    ]); ?>
    </div>
</div>
