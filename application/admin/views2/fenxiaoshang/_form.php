<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model common\models\Fenxiaoshang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fenxiaoshang-form">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
               'attribute' => 'member_id',
               'label' => '申请人',
               'value' => $model->member->wxnick
            ],
            [
                'attribute' => 'name',
                'label' => '申请人',
            ],
            [
                'attribute' => 'phone',
                'label' => '联系方式',
            ],
            [
                'attribute' => 'recommend',
                'label' => '推荐人',
            ],
            [
                'attribute' => 'create_time',
                'label' => '申请时间',
            ]
        ],
    ]) ?>
    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
    </div>

    <?= $form->field($model, 'status')->dropDownList([
            \common\models\Fenxiaoshang::STATUS_APPLY => '同意',
            \common\models\Fenxiaoshang::STATUS_REFUSE => '拒绝',
    ])->label('审核') ?>

    <?php ActiveForm::end(); ?>

</div>
