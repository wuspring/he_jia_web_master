<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\Request;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\Models\Config */

$this->title = "基本设置"

?>

<!-- 5. $JQUERY_VALIDATION =========================================================================

				jQuery Validation
-->
<!-- Javascript -->
<script>
    init.push(function () {

        // Setup validation
        $("#jq-validation-form").validate({
            ignore: '.ignore, .select2-input',
            focusInvalid: false,
            rules: {
                'jq-validation-email': {
                    required: true,
                    email: true
                },
                'jq-validation-password': {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                'jq-validation-password-confirmation': {
                    required: true,
                    minlength: 6,
                    equalTo: "#jq-validation-password"
                },
                'jq-validation-required': {
                    required: true
                },
                'jq-validation-url': {
                    required: true,
                    url: true
                },
                'jq-validation-phone': {
                    required: true,
                    phone_format: true
                },
                'jq-validation-select': {
                    required: true
                },
                'jq-validation-multiselect': {
                    required: true,
                    minlength: 2
                },
                'daynum': {
                    required: true
                },

            },
            messages: {
                'jq-validation-policy': 'You must check it!'
            }
        });
    });
</script>

<script src="<?php echo Yii::$app->request->hostInfo;?>/public/js/uploadPreview.js"></script>
<!-- / Javascript -->

<div class="panel">
    <div class="panel-heading">
        <span class="panel-title">基础设置</span>
    </div>
    <div class="panel-body">

        <?php $form = ActiveForm::begin([
            'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],]); ?>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9" style="text-align: right">
                <button type="submit" class="btn btn-primary">保存</button>
            </div>
        </div>

     <div class="form-group ">
            <div class="col-sm-offset-3 col-sm-10" id="imgdiv">
                <img id="imgShow" height="100"
                     src="<?= empty($model->logo) ? Yii::$app->request->hostInfo . '/public/images/defaultlogo.png' : Url::to(Yii::$app->request->hostInfo . $model->logo); ?>"/>
            </div>
        </div>
        <div class="form-group simple">
            <label for="jq-validation-simple-icp" class="col-sm-3 control-label">logo</label>
            <div class="col-sm-9">
                <input type="file"  id="logo" name="logo" placeholder="">
            </div>
        </div>

        <div class="form-group ">
            <div class="col-sm-offset-3 col-sm-10" id="imgdiv2">
                <img id="imgShow2" height="100"
                     src="<?= empty($model->waplogo) ? Yii::$app->request->hostInfo . '/public/images/defaultlogo.png' : Url::to(Yii::$app->request->hostInfo . $model->waplogo); ?>"/>
            </div>
        </div>
        <div class="form-group simple">
            <label for="jq-validation-simple-icp" class="col-sm-3 control-label">手机版logo</label>
            <div class="col-sm-9">
                <input type="file"  id="waplogo" name="waplogo" placeholder="">
            </div>
        </div>

        <div class="form-group ">
            <div class="col-sm-offset-3 col-sm-10" id="imgdiv1">
                <img id="imgShow1" height="100"
                     src="<?= empty($model->erweima) ? Yii::$app->request->hostInfo . '/public/images/defaultlogo.png' : Url::to(Yii::$app->request->hostInfo . $model->erweima); ?>"/>
            </div>
        </div>
        <div class="form-group simple">
            <label for="jq-validation-simple-icp" class="col-sm-3 control-label">二维码</label>
            <div class="col-sm-9">
                <input type="file"  id="erweima" name="erweima" placeholder="">
            </div>
        </div>            
        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">站点名称</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="name" name="name" value="<?=$model->name;?>" placeholder="站点名称">
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-urladdress" class="col-sm-3 control-label">网站地址</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="urladdress" name="urladdress" value="<?=$model->urladdress;?>" placeholder="http://www.xxx.com">
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-keywords" class="col-sm-3 control-label">网站关键词</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="keywords" name="keywords" value="<?=$model->keywords;?>"  placeholder="输入网站关键词">
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-description" class="col-sm-3 control-label">网站描述</label>
            <div class="col-sm-9">
                <textarea class="form-control" name="description" id="description" placeholder="输入网站描述" ><?=$model->description;?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-telephone" class="col-sm-3 control-label">联系电话:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="telephone" name="telephone" value="<?=$model->telephone;?>" placeholder="联系电话">
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-telephone" class="col-sm-3 control-label">QQ:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="QQ" name="QQ" value="<?=$model->qq;?>" placeholder="QQ">
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-companyAddress" class="col-sm-3 control-label">公司名称</label>
            <div class="col-sm-9">
                <input class="form-control" name="company" id="company" value="<?=$model->company;?>" placeholder="公司名称"/>
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-companyAddress" class="col-sm-3 control-label">公司地址</label>
            <div class="col-sm-9">
                <input class="form-control" name="companyAddress" id="companyAddress" value="<?=$model->companyAddress;?>" placeholder="公司地址"/>
            </div>
        </div>
        <div class="form-group simple">
            <label for="jq-validation-simple-icp" class="col-sm-3 control-label">ICP备案号</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="icp" name="icp" value="<?=$model->icp;?>" placeholder="ICP备案号">
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-description" class="col-sm-3 control-label">优惠券说明</label>
            <div class="col-sm-9">
                <textarea class="form-control" name="coupon_desc" id="coupon_desc" placeholder="输入优惠券说明" ><?=isset($model->coupon_desc)?$model->coupon_desc:'';?></textarea>
            </div>
        </div>

        <div class="form-group simple" style="display: none">
            <label for="jq-validation-simple-icp" class="col-sm-3 control-label">公告</label>
            <div class="col-sm-9">
                <textarea type="text" class="form-control" id="notice" name="notice" value="<?=$model->notice;?>" placeholder="公告"></textarea>
            </div>
        </div>

        <div class="form-group simple" style="display: none">
            <label for="jq-validation-simple-icp" class="col-sm-3 control-label">购物送优惠券</label>
            <div class="col-sm-9">
                <label class="checkbox-inline">
                    <input type="checkbox" value="1" name="send_coupon[]" <?php if($model->send_coupon && in_array(1,$model->send_coupon)):?>checked<?php endif;?> class="px">
                    <span class="lbl">现金购买</span>
                </label>

                <label class="checkbox-inline">
                    <input type="checkbox" value="2" name="send_coupon[]" <?php if($model->send_coupon && in_array(2,$model->send_coupon)):?>checked<?php endif;?> class="px">
                    <span class="lbl">现金+积分</span>
                </label>
            </div>
        </div>
        <div class="form-group simple" style="display: none">
            <label for="jq-validation-simple-icp" class="col-sm-3 control-label">充值赠送积分比例(1:N)</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="recharge_integral" name="recharge_integral" value="<?=$model->recharge_integral;?>" placeholder="比例为：充值1元赠送n积分">
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<script type="text/javascript">
    init.push(function () {

        new uploadPreview({ UpBtn: "logo", DivShow: "imgdiv", ImgShow: "imgShow" });
        // new uploadPreview({ UpBtn: "waplogo", DivShow: "imgdiv2", ImgShow: "imgShow2" });
        new uploadPreview({ UpBtn: "erweima", DivShow: "imgdiv1", ImgShow: "imgShow1" });


    });
</script>

<!-- /5. $JQUERY_VALIDATION -->