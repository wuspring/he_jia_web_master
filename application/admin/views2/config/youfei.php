<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\web\Request;
/* @var $this yii\web\View */
/* @var $model app\Models\Config */

$this->title = "经销商分成设置"

?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title">快递设置</span>
    </div>
    <div class="panel-body">

        <?php $form = ActiveForm::begin([
            'options' => ['class' => 'form-horizontal'],]); ?>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9" style="text-align: right">
                <button type="submit" class="btn btn-primary">保存</button>
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">邮费金额(元)</label>
            <div class="col-sm-9">
                <input type="number" class="form-control" id="youfei" name="youfei" value="<?= $model->youfei; ?>" />
            </div>
        </div>


        <?php ActiveForm::end(); ?>
    </div>
</div>
