<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\Request;
/* @var $this yii\web\View */
/* @var $model common\models\Config */

$this->title = "微信基础设置"

?>

<!-- 5. $JQUERY_VALIDATION =========================================================================

    jQuery Validation
-->
    <!-- Javascript -->
    <script>
        init.push(function () {
            $("#jq-validation-phone").mask("(999) 999-9999");
            $('#jq-validation-select2').select2({ allowClear: true, placeholder: 'Select a country...' }).change(function(){
                $(this).valid();
            });
            $('#jq-validation-select2-multi').select2({ placeholder: 'Select gear...' }).change(function(){
                $(this).valid();
            });

            // Add phone validator
            $.validator.addMethod(
                "phone_format",
                function(value, element) {
                    var check = false;
                    return this.optional(element) || /^\(\d{3}\)[ ]\d{3}\-\d{4}$/.test(value);
                },
                "Invalid phone number."
            );

            // Setup validation
            $("#jq-validation-form").validate({
                ignore: '.ignore, .select2-input',
                focusInvalid: false,
                rules: {
                    'jq-validation-email': {
                      required: true,
                      email: true
                    },
                    'jq-validation-password': {
                        required: true,
                        minlength: 6,
                        maxlength: 20
                    },
                    'jq-validation-password-confirmation': {
                        required: true,
                        minlength: 6,
                        equalTo: "#jq-validation-password"
                    },
                    'jq-validation-required': {
                        required: true
                    },
                    'jq-validation-url': {
                        required: true,
                        url: true
                    },
                    'jq-validation-phone': {
                        required: true,
                        phone_format: true
                    },
                    'jq-validation-select': {
                        required: true
                    },
                    'jq-validation-multiselect': {
                        required: true,
                        minlength: 2
                    },
                    'jq-validation-select2': {
                        required: true
                    },
                    'jq-validation-select2-multi': {
                        required: true,
                        minlength: 2
                    },
                    'jq-validation-text': {
                        required: true
                    },
                    'jq-validation-simple-error': {
                        required: true
                    },
                    'jq-validation-dark-error': {
                        required: true
                    },
                    'jq-validation-radios': {
                        required: true
                    },
                    'jq-validation-checkbox1': {
                        require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
                    },
                    'jq-validation-checkbox2': {
                        require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
                    },
                    'jq-validation-policy': {
                        required: true
                    }
                },
                messages: {
                    'jq-validation-policy': 'You must check it!'
                }
            });
        });
    </script>
    <!-- / Javascript -->

    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">微信基础设置</span>
        </div>
        <div class="panel-body">

             <?php $form = ActiveForm::begin([
             'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],]); ?>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9" style="text-align: right">
                    <button type="submit" class="btn btn-primary">编辑</button>
                </div>
            </div>

                <div class="form-group">
                    <label for="jq-validation-name" class="col-sm-3 control-label">公众号名称</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" value="<?=$model->name;?>" placeholder="微信公众号名称">
                    </div>
                </div>

                <div class="form-group">
                    <label for="jq-validation-wxid" class="col-sm-3 control-label">公众号原始id</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="wxid" name="wxid" value="<?=$model->wxid;?>" placeholder="公众号原始id">
                    </div>
                </div>

                <div class="form-group">
                    <label for="jq-validation-wxnumber" class="col-sm-3 control-label">微信号</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="wxnumber" name="wxnumber" value="<?=$model->wxnumber;?>"  placeholder="微信号">
                    </div>
                </div>

                <div class="form-group">
                    <label for="jq-validation-appid" class="col-sm-3 control-label">AppID:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="appid" name="appid" value="<?=$model->appid;?>" placeholder="AppID">
                    </div>
                </div>
                <div class="form-group">
                    <label for="jq-validation-AppSecret" class="col-sm-3 control-label">AppSecret</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="appsecret" id="appsecret" value="<?=$model->appsecret;?>" placeholder="AppSecret"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="jq-validation-token" class="col-sm-3 control-label">Token</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="token" id="token" value="<?=$model->token;?>" placeholder="Token"/>
                    </div>
                </div>
                <div class="form-group simple">
                    <label for="jq-validation-simple-aesencodingkey" class="col-sm-3 control-label">AesEncodingKey</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="aesencodingkey" name="aesencodingkey" value="<?=$model->aesencodingkey;?>" placeholder="AesEncodingKey">
                    </div>
                </div>
                <div class="form-group simple">
                    <label for="jq-validation-simple-notifyUrl" class="col-sm-3 control-label">异步回调地址</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="notifyUrl" name="notifyUrl" value="<?=$model->notifyUrl;?>" placeholder="异步回调地址">
                    </div>
                </div>

             <?php ActiveForm::end(); ?>
        </div>
    </div>
<!-- /5. $JQUERY_VALIDATION -->