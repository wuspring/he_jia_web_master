<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\Request;
/* @var $this yii\web\View */
/* @var $model app\Models\Config */

$this->title = "拓展设置";
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/js/grids/grid_input.js');
?>

<!-- 5. $JQUERY_VALIDATION =========================================================================

				jQuery Validation
-->
				<!-- Javascript -->
				<script>
					init.push(function () {
						$("#jq-validation-phone").mask("(999) 999-9999");
						$('#jq-validation-select2').select2({ allowClear: true, placeholder: 'Select a country...' }).change(function(){
							$(this).valid();
						});
						$('#jq-validation-select2-multi').select2({ placeholder: 'Select gear...' }).change(function(){
							$(this).valid();
						});

						// Add phone validator
						$.validator.addMethod(
							"phone_format",
							function(value, element) {
								var check = false;
								return this.optional(element) || /^\(\d{3}\)[ ]\d{3}\-\d{4}$/.test(value);
							},
							"Invalid phone number."
						);

						// Setup validation
						$("#jq-validation-form").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: false,
							rules: {
								'jq-validation-email': {
								  required: true,
								  email: true
								},
								'jq-validation-password': {
									required: true,
									minlength: 6,
									maxlength: 20
								},
								'jq-validation-password-confirmation': {
									required: true,
									minlength: 6,
									equalTo: "#jq-validation-password"
								},
								'jq-validation-required': {
									required: true
								},
								'jq-validation-url': {
									required: true,
									url: true
								},
								'jq-validation-phone': {
									required: true,
									phone_format: true
								},
								'jq-validation-select': {
									required: true
								},
								'jq-validation-multiselect': {
									required: true,
									minlength: 2
								},
								'jq-validation-select2': {
									required: true
								},
								'jq-validation-select2-multi': {
									required: true,
									minlength: 2
								},
								'jq-validation-text': {
									required: true
								},
								'jq-validation-simple-error': {
									required: true
								},
								'jq-validation-dark-error': {
									required: true
								},
								'jq-validation-radios': {
									required: true
								},
								'jq-validation-checkbox1': {
									require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
								},
								'jq-validation-checkbox2': {
									require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
								},
								'jq-validation-policy': {
									required: true
								}
							},
							messages: {
								'jq-validation-policy': 'You must check it!'
							}
						});
					});
				</script>
				<!-- / Javascript -->

<style>
    .alert_window {
        min-width: 600px;
        width: 50%;
        height: 150px;
        padding-top: 30px;
        z-index: 100;
        position: absolute;
        margin: 0 auto;
        top: 30%;
        left: 25%;
        background: #fff;
    }
    .shadow {
        width: 100%;
        height: 100%;
        z-index: 99;
        background: #9e9e9e;
        position: absolute;
        top: 0;
        left: 0;
    }
    .hide {
        display: none;
    }
</style>
<div data-bind="shadow" class="shadow hide"></div>
<div data-bind="shadow" class="alert_window hide">
    <div class="form-group">
    <div>
        <label class="col-sm-3 control-label">跳转地址</label>
    </div>
    <div class="col-sm-9">
        <input class="form-control" type="text" name="url_path" placeholder="请输入点击图片跳转的地址">
        格式 ： '/pages/detail/detail?id=' + 商品ID
    </div>
    </div>
    <div class="form-group">
    <div class="col-sm-3"></div>
    <div class="col-sm-9">
<!--        <button type="button" class="btn btn-primary"  data-id="continueUpload">上传图片</button>-->
<!--        <button type="button" style="margin-left:15px" class="btn btn-default" data-id="stop">取消</button>-->
    </div>
    </div>
</div>
<?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],]); ?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title">拓展设置</span>
    </div>
    <div class="panel-body">

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9" style="text-align: right">
                <button type="submit" class="btn btn-primary">保存</button>
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">客服电话</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="tel_tel" name="tel" value="<?=$config->tel;?>" placeholder="请输入当前城市客服电话">
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-name" class="col-sm-3 control-label">店铺默认标签</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="tel_tag" name="tag" value='<?= isset($config->tag) ? $config->tag : '{}';?>' placeholder="请输入当前城市店铺默认标签">
            </div>
        </div>
        </div>

    </div>
</div>



<?php ActiveForm::end(); ?>
<div data-id="build" style="display:none;"></div>
<script src="/public/js/jquery.min.js"></script>
<script src="/public/js/base_upload.js"></script>
<script src="/public/js/upload-hooks/preview.js"></script>
<script src="/public/js/jquery.tagsinput.min.js"></script>
<script>
    var uploadPath = '/upload/img?more=true';
    $(function() {
        // config = [
        //     {category: 'config', id: 'logo', type: 'logo'},
        // ];
        // previewCtrl(config);



        $('#resource').tagsInput({width:'auto',defaultText:'添加渠道'});

    });

    $(function () {
        GridInput($('#tel_tag'), true);
    })
</script>
