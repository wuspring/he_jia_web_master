<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\Request;
/* @var $this yii\web\View */
/* @var $model common\models\Config */

$this->title = "小程序支付"

?>

<!-- 5. $JQUERY_VALIDATION =========================================================================

				jQuery Validation
-->
<!-- Javascript -->
<script>
    init.push(function () {
        $("#jq-validation-phone").mask("(999) 999-9999");
        $('#jq-validation-select2').select2({ allowClear: true, placeholder: 'Select a country...' }).change(function(){
            $(this).valid();
        });
        $('#jq-validation-select2-multi').select2({ placeholder: 'Select gear...' }).change(function(){
            $(this).valid();
        });

        // Add phone validator
        $.validator.addMethod(
            "phone_format",
            function(value, element) {
                var check = false;
                return this.optional(element) || /^\(\d{3}\)[ ]\d{3}\-\d{4}$/.test(value);
            },
            "Invalid phone number."
        );

        // Setup validation
        $("#jq-validation-form").validate({
            ignore: '.ignore, .select2-input',
            focusInvalid: false,
            rules: {
                'mchid': {
                    required: true,
                },
                'key': {
                    required: true,
                    // minlength: 6,
                    // maxlength: 32
                },


            },
            messages: {
                'jq-validation-policy': 'You must check it!'
            }
        });
    });
</script>
<!-- / Javascript -->

<div class="panel">
    <div class="panel-heading">
        <span class="panel-title">小程序支付设置</span>
    </div>
    <div class="panel-body">

        <?php $form = ActiveForm::begin([
            'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],]); ?>
        <div class="form-group">
            <label for="jq-validation-mchid" class="col-sm-3 control-label">支付帐号</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="mchid" name="mchid" value="<?=$model->mchid;?>" placeholder="支付帐号">
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-key" class="col-sm-3 control-label">支付密钥</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="key" name="key" value="<?=$model->key;?>" placeholder="支付密钥">
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-apiclient_cert" class="col-sm-3 control-label">apiclient_cert私钥文件</label>
            <div class="col-sm-9">
                <input type="text" class="form-control " style="width:30%; float: left;" id="pay_cert" name="pay_cert" value="<?=$model->apiclient_cert;?>" placeholder="私钥文件">
                <input type="file" class="form-control" style="width:70%;float: left;border:0px" id="apiclient_cert" name="apiclient_cert" value="<?=$model->apiclient_cert;?>" placeholder="私钥文件">
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-apiclient_key" class="col-sm-3 control-label">apiclient_key公钥文件</label>
            <div class="col-sm-9">
                <input type="text" class="form-control " style="width:30%; float: left;" id="pay_key" name="pay_key" value="<?=$model->apiclient_key;?>" placeholder="公钥文件">
                <input type="file" class="form-control" style="width:70%;float: left;border:0px" id="apiclient_key" name="apiclient_key" value="<?=$model->apiclient_key;?>" placeholder="公钥文件">
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-rootca" class="col-sm-3 control-label">根证书文件</label>
            <div class="col-sm-9">
                <input type="text" class="form-control " style="width:30%; float: left;" id="pay_rootca" name="pay_rootca" value="<?=$model->rootca;?>" placeholder="公钥文件">
                <input type="file" class="form-control" style="width:70%;float: left;border:0px" id="rootca" name="rootca" value="<?=$model->rootca;?>" placeholder="公钥文件">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary">编辑</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<!-- /5. $JQUERY_VALIDATION -->