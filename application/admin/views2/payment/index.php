<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\search\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '支付管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            'code',
            // 'payment_config:ntext',
            // 'state',

            array(
                'attribute' => 'state',
                'label'=>'状态',
                'format' => 'raw',
                'value'=>function($filterModel){
                    $url="javascript:void(0)";
                    if ($filterModel->state == 0) {
                        return  Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', $url, ['title' => '状态','data-toggle'=>$filterModel->id,'data-status'=>$filterModel->state,'id'=>'data-state'] ) ;
                    }else{
                        return  Html::a('<span class="glyphicon glyphicon-ok-sign"></span>', $url, ['title' => '状态','data-toggle'=>$filterModel->id,'data-status'=>$filterModel->state,'id'=>'data-state'] ) ;
                    }
                }
                       
            ),
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '操作',
                'template' => '{setup} {update}',
                'buttons' => [
                    'setup' => function ($url, $model, $key) {
                        $url=Url::toRoute(['payment/'.$model->code,'id'=>$model->id]);
                        return Html::a('设置', $url, ['title' => '查看', 'data-toggle' => 'modal','class'=>'a_margin']);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('更新', $url, ['title' => '更新', 'data-toggle' => 'modal','class'=>'a_margin']);
                    },
                ],

            ],
        ],
    ]); ?>
    </div>
</div>
<script type="text/javascript">
init.push(function () {
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    $("#data-state").on('click',function(){
        var th =$(this);
        var id=$(this).attr('data-toggle');
        var status= $(this).attr('data-status');
        if (status == 0) {
            status=1;
        }else{
           status=0; 
        }
        $.post("<?php echo Url::toRoute(['/payment/isstate']);?>",{_csrf:_csrf,id:id,status:status},function(data){
            if (data == 0) {
                $(th).find('span').attr('class','glyphicon glyphicon-ban-circle');
                $(th).attr('data-status',0);
            }else{
                $(th).find('span').attr('class','glyphicon glyphicon-ok-sign');
                $(th).attr('data-status',1);
            }
        });
    });

});
</script>