<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MTicketConfig */

$this->title = '页面设置：' . $model->ticket->name;
$this->params['breadcrumbs'][] = ['label' => 'Mticket Configs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <ul class="nav nav-tabs dl_tab">
        <li class="col-lg-2 tab_title active" data-id="tab-t" data-val="zh"><a href="javascript:void(0)">展会页</a></li>
        <li class="col-lg-2 tab_title" data-id="tab-t" data-val="sp"><a href="javascript:void(0)">自助索票</a></li>
    </ul>

    <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
            'ticketId' => $ticketId,
        ]) ?>
    </div>

</div>
<script>
    $('[data-id="tab-t"]').click(function () {
        $(this).addClass('active').siblings().removeClass('active');

        var v = $(this).attr('data-val');
        $('[data-id="tab"][data-val="' + v + '"]').show().siblings().hide();
    })
</script>
