<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\ServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '承诺服务';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="panel-body">
    <p>
        <?= Html::a('添加服务', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            array(
                'format' => 'raw',
                'attribute' => 'pos',
                'label'=>'位置',
                'value'=>function($filterModel){
                    return $filterModel->getPosDesc();
                },
            ),
            array(
                'format' => 'raw',
                'attribute' => 'icon',
                'label'=>'图标',
                'value'=>function($filterModel){
                    return Html::img($filterModel->icon, ['width' =>30]);
                },
            ),
            'name',
            'desc',
            'sort',
//            'provinces_id',
             'create_time',
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'操作',
                'template' => '{update} {delete}',
                'buttons'=>[
                    'view'=>function($url, $model, $key){
                        return  Html::a('查看', $url, ['title' => '查看','data-toggle'=>'modal','class'=>'a_margin']);
                    },
                    'update' => function ($url, $model, $key) {
                        return  Html::a('更新', $url, ['title' => '更新','data-toggle'=>'modal','class'=>'a_margin'] ) ;
                    },
                    'delete'=> function ($url, $model, $key) {
                        return  Html::a('删除', $url, ['title' => '删除','class'=>'a_margin','data' => [
                            'confirm' => '确认删除，删除后不可恢复?',
                            'method' => 'post',
                        ]]);
                    },
                ],
            ],
        ],
    ]); ?>
    </div>
</div>
