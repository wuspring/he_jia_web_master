<?php

use yii\helpers\Html;
use yii\grid\GridView;
use DL\service\UrlService;

/* @var $this yii\web\View */
/* @var $searchModel app\search\StoreShopSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '编辑店铺';
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <ul class="nav nav-tabs dl_tab">
        <li class="col-lg-2 tab_title"><a href="<?php
            $storeInfo = \common\models\StoreInfo::findOne([
                    'user_id' => $userInfo->id,
            ]);

            if ($storeInfo) {
                echo UrlService::build(['store-info/update', 'id'=> $storeInfo->id]);
            } else {
                echo 'javascript:void(0);';
            }
             ?>">基础信息</a></li>
        <li class="col-lg-2 tab_title active"><a href="javascript:void(0);">门店列表</a></li>
    </ul>

    <div class="panel-body">
        <?= Html::a('添加门店', ['create'], ['class' => 'btn btn-primary']) ?>
    </div>

    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'user_id',

            [
                'attribute' => 'provinces_id',
                'label'=>'城市',
                'value'=>function($filetModel){
                    return  $filetModel->provincesInfo->cname;
                }
            ],
            [
                'attribute' => 'store_name',
                'label'=>'店铺名字',
            ],
            [
                'attribute' => 'description',
                'label'=>'描述',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '操作',
                'template' => '{update}{delete}'
            ],
        ],
    ]); ?>
    </div>
</div>
