<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreShop */

$this->title = '添加门店';
$this->params['breadcrumbs'][] = ['label' => 'Store Shops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
   <div class="panel-heading">
         <div class="panel-title"><?= Html::encode($this->title) ?></div>
   </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
