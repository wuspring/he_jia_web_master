<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \DL\service\UrlService;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '装修预约列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .a_margin{
        margin-left: 10px;
    }
</style>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title">
           <?= Html::encode($this->title) ?>
        </span>
    </div>

    <div class="panel-body">
        <div class="mb-3 clearfix">
            <div class="p-4 bg-shaixuan">
                <form method="get">
                    <div class="col-md-12">
                        <input type="hidden" name="r" value="fitup-appointment/index">

                        <div class="form-group row col-md-4">
                            <div class="col-md-12">
                                <div class="input-group col-md-12">
                                    <div class="col-md-5">
                                        <label class="col-md-12 label1">是否处理：</label>
                                    </div>

                                    <div class="col-md-7">
                                        <div class="col-md-12">
                                            <select class="form-control " name="FitupAppointmentSearch[is_deal]" style="display: inline-block;">
                                                <option value="0" >未选择</option>
                                                <option value="0" <?= isset($_GET['FitupAppointmentSearch']['is_deal']) && $_GET['FitupAppointmentSearch']['is_deal'] ==0 ? 'selected' : ''; ?> >未处理</option>
                                                <option value="1" <?= isset($_GET['FitupAppointmentSearch']['is_deal']) && $_GET['FitupAppointmentSearch']['is_deal'] ==1 ? 'selected' : ''; ?>>已处理</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row col-md-6">
                            <div class="col-md-12">
                                <div class="input-group col-md-12">
                                    <div class="col-md-3">
                                        <label class="col-md-12 label1">下单时间：</label>
                                    </div>

                                    <div class="col-md-9">
                                        <div class="col-md-5">
                                            <input class="form-inline col-md-12 form-control" id="date_start" name="date_start"
                                                   autocomplete="off"
                                                   value="<?= isset($_GET['date_start']) ? trim($_GET['date_start']) : '' ?>">
                                        </div>
                                        <div class="col-md-1 label1" style="text-align: center">
                                            至
                                        </div>
                                        <div class="col-md-5">
                                            <input class="form-inline col-md-12 form-control" id="date_end" name="date_end"
                                                   autocomplete="off"
                                                   value="<?= isset($_GET['date_end']) ? trim($_GET['date_end']) : ''  ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-offset-1">
                            <button class="btn btn-primary mr-4">筛选</button>
                            <a style="" class="btn btn-secondary mr-4"
                               href="<?php $q = $_GET; unset($q['r']); echo UrlService::build(array_merge(['fitup-appointment/loader'], $q)); ?>" target="_blank">批量导出</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'member_id',
                    'label'=>'会员名称',
                    'value'=>function($filterModel){
                        return $filterModel->member->nickname;
                    },
                ],
                [
                    'attribute' => 'name',
                    'label'=>'名字',
                ],
                'mobile',
                'create_time',
                [
                    'attribute' => 'is_deal',
                    'label'=>'是否处理',
                    'value'=>function($fileModel){
                      return  ($fileModel->is_deal==1)?'已经处理':'未处理';
                    }
                ],
                // 'is_del',
                // 'is_deal',
                // 'dispatch_people',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => '操作',
                    'template' => '{view} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('查看', $url, ['title' => '查看', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('更新', $url, ['title' => '更新', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('删除', $url, ['title' => '删除', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                        },
                    ],

                ],
            ],
        ]); ?>
    </div>

</div>
<script language="javascript" type="text/javascript" src="<?php echo yii::$app->request->hostInfo;?>/public/My97DatePicker/WdatePicker.js"></script>
<script>
    $("#date_start").click(function(){
        WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d'})
    });
    $("#date_end").click(function(){
        WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d',minDate:'#F{$dp.$D(\'date_start\')}'})
    });
</script>