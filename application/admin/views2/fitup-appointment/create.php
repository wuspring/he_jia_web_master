<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FitupAppointment */

$this->title = 'Create Fitup Appointment';
$this->params['breadcrumbs'][] = ['label' => 'Fitup Appointments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fitup-appointment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
