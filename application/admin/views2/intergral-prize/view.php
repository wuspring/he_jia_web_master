<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\IntergralPrize */

$this->title ='详情';
$this->params['breadcrumbs'][] = ['label' => 'Intergral Prizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <div class="panel-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

                'prize_name',
                'prize_grade',
                'prize_price',
                'prize_describe',
                'create_time',
            ],
        ]) ?>
    </div>



</div>
