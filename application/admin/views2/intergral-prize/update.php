<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\IntergralPrize */

$this->title = '编辑';
$this->params['breadcrumbs'][] = ['label' => 'Intergral Prizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
