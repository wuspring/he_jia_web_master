<?php

use yii\helpers\Html;
use yii\grid\GridView;


$this->title = '积分奖品列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .caoZuo{
        margin-left: 10px;
    }
</style>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">
        <?= Html::a('添加新的一期', ['prize-activity'], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],


                [
                    'attribute'=>'prize_terms',
                    'label'=>'积分抽奖期数'
                ],
                [
                    'attribute'=>'start_time',
                    'label'=>'开始时间'
                ],
                [
                    'attribute'=>'end_time',
                    'label'=>'结束时间'
                ],
                 [
                         'attribute'=>'create_time',
                        'label'=>'添加时间'
                 ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>'操作',
                    'template' => '{update}{view}{record}{prize}',
                    'buttons'=>[
                        'update'=>function($url, $model, $key){
                            $url=yii::$app->request->hostInfo.'/admin.php?r=intergral-prize/edit-prize-activity&id='.$model->id ;
                            return Html::a('编辑', $url, ['title' => '编辑', 'class'=>"btn btn-info btn-xs caoZuo",'data-toggle' => 'modal']);
                        },
                        'view'=>function($url, $model, $key){
                            $url=yii::$app->request->hostInfo.'/admin.php?r=intergral-prize/index&id='.$model->id ;
                            return Html::a('本期奖品', $url, ['title' => '本期奖品', 'class'=>"btn btn-info btn-xs caoZuo",'data-toggle' => 'modal']);
                        },
                        'record'=>function($url, $model, $key){
                            $url = \DL\service\UrlService::build(['intergral-luck-draw/index', 'prize_termsId' =>$model->id]);
                            return Html::a('抽奖记录', $url, ['title' => '抽奖记录', 'class'=>"btn btn-info btn-xs caoZuo",'data-toggle' => 'modal']);
                        },
                        'prize'=>function($url, $model, $key){
                            $url = \DL\service\UrlService::build(['intergral-luck-draw/luck-person', 'prize_termsId' =>$model->id]);
                            return Html::a('获奖者', $url, ['title' => '获奖者', 'class'=>"btn btn-info btn-xs caoZuo",'data-toggle' => 'modal']);
                        },

                    ],

                ],
            ],
        ]); ?>
    </div>

</div>
