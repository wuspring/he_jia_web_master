<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\search\IntergralPrizeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="intergral-prize-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'prize_terms') ?>

    <?= $form->field($model, 'prize_name') ?>

    <?= $form->field($model, 'prize_grade') ?>

    <?= $form->field($model, 'prize_price') ?>

    <?php // echo $form->field($model, 'prize_describe') ?>

    <?php // echo $form->field($model, 'create_time') ?>

    <?php // echo $form->field($model, 'is_del') ?>

    <?php // echo $form->field($model, 'is_show') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
