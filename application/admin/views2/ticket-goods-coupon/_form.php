<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TicketGoodsCoupon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ticket-goods-coupon-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group" style="text-align: right">
        <?= Html::submitButton('保存', ['class' => 'btn btn-primary']) ?>
    </div>

    <?= $form->field($model, 'ticket_id', ['options' => ['style' => 'display:none']])->textInput(['value' => $ticketId]) ?>

    <?php
    
    echo $form->field($model, 'goods_coupon_id')->dropDownList([]) ?>

    <?= $form->field($model, 'sort')->textInput(['maxlength' => true])->label('权重') ?>



    <?php ActiveForm::end(); ?>

</div>
