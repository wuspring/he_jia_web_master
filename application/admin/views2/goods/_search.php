<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\GoodsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-body">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'form-inline'],
    ]); ?>

    <?= $form->field($model, 'goods_name')->label('商品名称') ?>


    <?php  if ($assignment){ echo $form->field($model, 'user_id')->label('店铺名称');}   ?>

    <?= $form->field($model, 'gc_id')->label('商品分类') ?>

    <div class="form-group">
        <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('重置', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>

    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";

    init.push(function () {
        //删除类为 help-block的div
        $(".help-block").remove();


        $("#goodssearch-user_id").select2({
            placeholder:"输入店铺名称",//文本框的提示信息
            minimumInputLength:1,   //至少输入n个字符，才去加载数据
            allowClear: true,     //是否允许用户清除文本信息
            ajax:{
                url:'<?php echo Url::toRoute(['user/getlist'])?>',   //地址
                dataType:'text',    //接收的数据类型
                //contentType:'application/json',
                data: function (term, pageNo) {     //在查询时向服务器端传输的数据
                    term = $.trim(term);
                    return {
                        _csrf:_csrf ,
                        val: term,    //联动查询的字符
                        //pageSize: 15,    //一次性加载的数据条数
                        //pageNo:pageNo,    //页码
                        //time:new Date()//测试
                    }
                },
                results:function(data,pageNo){
                    if(data.length>0){         //如果没有查询到数据，将会返回空串
                        var dataObj =eval("("+data+")");  //将接收到的JSON格式的字符串转换成JSON数据
                        var more = (pageNo*15)<dataObj.total; //用来判断是否还有更多数据可以加载
                        return {
                            results:dataObj.result,more:more
                        };
                    }else{
                        return {results:data};
                    }
                }
            },

            initSelection:function(element,callback){             //初始化，其中doName是自定义的一个属性，用来存放text的值
                var id=$(element).val();

                var text="<?php echo $model->user->nickname;?>";

                //var text=$(element).attr("doName");
                if(id!=''&&text!=""){
                    callback({id:id,text:text});
                }
            },
            formatResult: formatAsText //渲染查询结果项
        });

        $("#goodssearch-gc_id").select2({
            placeholder:"输入分类",//文本框的提示信息
            minimumInputLength:1,   //至少输入n个字符，才去加载数据
            allowClear: true,     //是否允许用户清除文本信息
            ajax:{
                url:'<?php echo Url::toRoute(['goods-class/getlist'])?>',   //地址
                dataType:'text',    //接收的数据类型
                //contentType:'application/json',
                data: function (term, pageNo) {     //在查询时向服务器端传输的数据
                    term = $.trim(term);
                    return {
                        _csrf:_csrf ,
                        val: term,    //联动查询的字符
                        //pageSize: 15,    //一次性加载的数据条数
                        //pageNo:pageNo,    //页码
                        //time:new Date()//测试
                    }
                },
                results:function(data,pageNo){
                    if(data.length>0){         //如果没有查询到数据，将会返回空串
                        var dataObj =eval("("+data+")");  //将接收到的JSON格式的字符串转换成JSON数据
                        var more = (pageNo*15)<dataObj.total; //用来判断是否还有更多数据可以加载
                        return {
                            results:dataObj.result,more:more
                        };
                    }else{
                        return {results:data};
                    }
                }
            },

            initSelection:function(element,callback){             //初始化，其中doName是自定义的一个属性，用来存放text的值
                var id=$(element).val();

                var text="<?php echo $model->gclass->name;?>";

                //var text=$(element).attr("doName");
                if(id!=''&&text!=""){
                    callback({id:id,text:text});
                }
            },
            formatResult: formatAsText //渲染查询结果项
        });
    });

    //格式化查询结果,将查询回来的id跟name放在两个div里并同行显示，后一个div靠右浮动
    function formatAsText(item){
        var itemFmt = "<div style='display:inline;'>" + item.text + "</div>";//<div style='float:right;color:#4F4F4F;display:inline'>"+item.name+"</div>
        return itemFmt;
    }

</script>
