<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FloorIndex */

$this->title = 'Create Floor Index';
$this->params['breadcrumbs'][] = ['label' => 'Floor Indices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="floor-index-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
