<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\PostmethodSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="postmethod-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'code') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'first_condition') ?>

    <?php // echo $form->field($model, 'first_money') ?>

    <?php // echo $form->field($model, 'other_condition') ?>

    <?php // echo $form->field($model, 'other_money') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'createTime') ?>

    <?php // echo $form->field($model, 'send_area') ?>

    <?php // echo $form->field($model, 'not_area') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
