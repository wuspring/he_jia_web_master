<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\GoodsSku */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="goods-sku-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
    </div>

    <?= $form->field($model, 'goods_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'attr')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'num')->textInput() ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'integral')->textInput() ?>

    <?= $form->field($model, 'goods_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'picimg')->textInput(['maxlength' => true]) ?>

    <?php ActiveForm::end(); ?>

</div>
