<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\HelpCont */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Help Conts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <div class="panel-title"><?= Html::encode($this->title) ?></div>
    </div>

   <div class="panel-body">
       <?= Html::a('返回', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
   </div>
  <div class="panel-body">

      <?= DetailView::widget([
          'model' => $model,
          'attributes' => [
              'id',
              'title',
              'type_id',
              'sort',
              'content:ntext',
              'create_time',
              'is_del',
              'is_show',
          ],
      ]) ?>
  </div>


</div>
