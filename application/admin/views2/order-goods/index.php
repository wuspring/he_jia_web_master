<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\OrderGoodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order Goods';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-goods-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Order Goods', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'order_id',
            'goods_id',
            'goods_name',
            'goods_price',
            // 'goods_integral',
            // 'fallinto_state',
            // 'goods_num',
            // 'fallInto',
            // 'goods_pay_price',
            // 'buyer_id',
            // 'createTime',
            // 'modifyTime',
            // 'sku_id',
            // 'attr:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
