<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CouponAutoSend */

$this->title = 'Create Coupon Auto Send';
$this->params['breadcrumbs'][] = ['label' => 'Coupon Auto Sends', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coupon-auto-send-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
