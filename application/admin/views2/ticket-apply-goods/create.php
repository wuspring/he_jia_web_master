<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TicketApplyGoods */

$this->title = '添加展会商品';
$this->params['breadcrumbs'][] = ['label' => 'Ticket Apply Goods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'goods' => $goods,
    ]) ?>

</div>
