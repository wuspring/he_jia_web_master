<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Member */

$this->title ='用户信息';
$this->params['breadcrumbs'][] = ['label' => 'Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <div class="panel-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute'=>'avatarTm',
                    'value'=>$model->avatarTm,
                    'format' => ['image',['width'=>'50','height'=>'50']],
                ],
                'username',
                // 'password',
                'nickname',
                array(
                    'attribute' => 'sex',
                    'label'=>'头像',
                    'value'=>($model->sex==1)?'男':'女',
                ),
                // 'birthday',
                // 'status',
                // 'avatarTm',
                // 'avatar',
                // 'email:email',
                'createTime',
                'modifyTime',
                'last_visit',
                //'userType',
                //'authkey',
                //'accessToken',
                'remainder',
                'integral',
                // 'rank',
            ],
        ]) ?>
    </div>


</div>
