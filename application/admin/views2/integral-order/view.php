<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = '订单详情';
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel">
    <div class="panel-heading">
        <span class="panel-title">收货人信息</span>
    </div>
    <div class="panel-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'receiver_name',
                'receiver_address',
                'receiver_mobile',
//                'receiver_zip',
            ],
        ]) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">
        <span class="panel-title">订单商品信息</span>
    </div>
    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>订单编号</th>
                <th>商品名称</th>
                <th>商品规格</th>
                <th>单价</th>
                <th>数量</th>
                <th>¥ 小计</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($orderGoods as $orderGood): ?>
                <tr>
                    <td><?= $model->orderid ?></td>
                    <td><?= $orderGood->goods_name ?></td>
                    <td><?= $orderGood->attr ?></td>
                    <td><?= $orderGood->goods_price ?>积分</td>
                    <td><?= $orderGood->goods_num ?></td>
                    <td><?= ($orderGood->goods_pay_price) ?>积分</td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>

    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">订单信息</span>
        </div>
        <div class="panel-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'orderid',
            'pay_sn',
//            [
//                'attribute' => 'type',
//                'label' => '订单类型',
//            ],
//            'goods_amount',
            'order_amount',
//            'integral_amount',
//            'pd_amount',
//            'freight',
            [
                'attribute'=>'order_state',
                'label'=>'订单状态',
                'value' => $model->orderState
            ],
//            'shipping',
//            'shipping_code',
//            'deliveryTime',
            'add_time',
            'payment_time',
            'finnshed_time',
//            [
//                'attribute' =>'fallinto_state',
//                'value' => call_user_func(function ($model) {
//                    $dic = [
//                        '0' => '待分成',
//                        '1' => '已分成'
//                    ];
//                    return isset($dic[$model->fallinto_state]) ? $dic[$model->fallinto_state] : $model->fallinto_state;
//                }, $model)
//            ]
        ],
    ]) ?>
        </div>
    </div>

        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">买家信息</span>
            </div>
            <div class="panel-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'buyer_name',
        ],
    ]) ?>
            </div>
        </div>


