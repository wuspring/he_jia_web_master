<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Money */

$this->title = '数据分析';
$this->params['breadcrumbs'][] = ['label' => 'Moneys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$date = date('Y-m-01');
$orderAmount = \common\models\Order::find()->where([
        'and',
        ['>=', 'add_time', $date],
        ['not in', 'order_state', ['5', '9']]
])->count();
$waitOrderAmount = \common\models\Order::find()->where([
    'and',
    ['>=', 'add_time', $date],
    ['=', 'order_state', '0']
])->count();
$payOrderAmount = \common\models\Order::find()->where([
    'and',
    ['>=', 'add_time', $date],
    ['in', 'order_state', ['1', '2', '3', '4']]
])->count();
$rebackOrderAmount = \common\models\Order::find()->where([
    'and',
    ['>=', 'add_time', $date],
    ['in', 'order_state', ['5']]
])->count();

$payOrder = \common\models\Order::find()->where([
    'and',
    ['>=', 'add_time', $date],
    ['in', 'order_state', ['1', '2', '3', '4']]
])->all();
$orderIds = \yii\helpers\ArrayHelper::getColumn($payOrder, 'orderid');
$sellGoods = \common\models\OrderGoods::find()->where([
        'and',
    ['in', 'order_id', $orderIds]
])->select('goods_id, sum(`goods_num`) as goods_num')->groupBy('goods_id')->orderby('goods_num desc')->limit(5)
->all();


$sellGoods = \common\models\OrderGoods::find()->where([
    'and',
    ['in', 'order_id', $orderIds]
])->select('goods_id, sum(`goods_num`) as goods_num')->groupBy('goods_id')->orderby('goods_num desc')->limit(5)
    ->all();

$sellMembers = \common\models\OrderGoods::find()->where([
    'and',
    ['in', 'order_id', $orderIds]
])->select('buyer_id, sum(`goods_num` * `goods_pay_price`) as goods_num')->groupBy('goods_id')->orderby('goods_num desc')->limit(5)
    ->all();
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">
            <table class="table table-striped table-bordered">
                <thead>
                    <th colspan="2">
                        销售统计
                    </th>
                </thead>
                <tbody>
                <tr>
                    <td><b>本月订单</b></td>
                    <td><?= $orderAmount; ?></td>
                </tr>
                <tr>
                    <td><b>待支付订单</b></td>
                    <td><?= $waitOrderAmount; ?></td>
                </tr>
                <tr>
                    <td><b>已支付订单</b></td>
                    <td><?= $payOrderAmount; ?></td>
                </tr>
                <tr>
                    <td><b>退换货订单</b></td>
                    <td><?= $rebackOrderAmount; ?></td>
                </tr>
                <tr>
                    <td><b>下单结算率</b></td>
                    <td><?php
                        if ($orderAmount) {
                            echo round($payOrderAmount  * 100 / $orderAmount, 2) . '%';
                        } else {
                            echo 0;
                        }
                    ?></td>
                </tr>
                <tr>
                    <td><b>订单退款率</b></td>
                    <td><?php
                        if ($orderAmount + $rebackOrderAmount) {
                            echo round($rebackOrderAmount  * 100 / ($orderAmount + $rebackOrderAmount), 2) . '%';
                        } else {
                            echo 0;
                        }

                    ?></td>
                </tr>
                </tbody>
            </table>

        <table class="table table-striped table-bordered">
            <thead>
            <th colspan="2">
                商品统计
            </th>
            </thead>
            <tbody>
            <tr>
                <td><b>商品销量排名</b></td>
                <td>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th>商品名称</th>
                            <th>销量</th>
                        </thead>
                        <?php if($sellGoods) :?>
                            <?php foreach ($sellGoods AS $sellGood) :?>
                            <tr>
                                <td><?= $sellGood->goods->goods_name; ?></td>
                                <td><?= $sellGood->goods_num; ?></td>
                            </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
        <table class="table table-striped table-bordered">
            <thead>
            <th colspan="2">
                消费统计
            </th>
            </thead>
            <tbody>
            <tr>
                <td><b>消费排行</b></td>
                <td>
                    <table class="table table-striped table-bordered">
                        <thead>
                        <th>用户</th>
                        <th>消费金额</th>
                        </thead>
                        <?php if($sellMembers) :?>
                            <?php foreach ($sellMembers AS $sellGood) :?>
                                <tr>
                                    <td><?= $sellGood->member->wxnick; ?></td>
                                    <td><?= $sellGood->goods_num; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>