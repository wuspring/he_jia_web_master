<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NavsWap */

$this->title = '编辑导航: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Navs Waps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
