<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SentSms */

$this->title = '修改模板';
$this->params['breadcrumbs'][] = ['label' => 'Sent Sms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="panel">

    <div class="panel-heading" style="margin-bottom: 20px">
        <div class="panel-title"><?= Html::encode($this->title) ?></div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
