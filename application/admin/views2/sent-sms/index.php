<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '短信模板';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="panel-body">
        <?= Html::a('添加短信模板', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
   <div class="panel-body">
       <?= GridView::widget([
           'dataProvider' => $dataProvider,
          // 'filterModel' => $searchModel,
           'columns' => [
               ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'sms_name',
                    'label' => '模板名称',
                ],
               [
                   'attribute' => 'sms_code',
                   'label' => '模板CODE',
               ],
               [
                   'attribute' => 'type',
                   'label' => '模板类型',
                   'value' => function ($filterModel) {
                       return $filterModel->typeInfo;
                   }
               ],
               [
                   'attribute' => 'content',
                   'label' => '模板信息',
               ],
//                'is_show',
//                'is_del',
//                'is_access_keys',
                'create_time',
               ['class' => 'yii\grid\ActionColumn'],
           ],
       ]); ?>
   </div>

</div>
