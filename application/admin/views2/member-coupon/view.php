<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MemberCoupon */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Member Coupons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-coupon-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'store_id',
            'member_id',
            'coupon_id',
            'coupon_auto_send_id',
            'begin_time',
            'end_time',
            'is_expire',
            'is_use',
            'is_delete',
            'addtime',
            'type',
            'integral',
            'price',
        ],
    ]) ?>

</div>
