<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Advertising */

$this->title = '添加广告';
$this->params['breadcrumbs'][] = ['label' => 'Advertisings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

   	<div class="panel-heading">
		<span class="panel-title"><?= Html::encode($this->title) ?></span>
	</div>

    <?= $this->render('_form', [
        'model' => $model,
        'adsense'=>$adsense,
    ]) ?>

</div>