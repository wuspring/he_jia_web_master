<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Link */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel-body form-controls-demo">
 <script src="<?php echo Yii::$app->request->hostInfo;?>/public/js/uploadPreview.js"></script>
    <?php $form = ActiveForm::begin([
     'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
        'fieldConfig' => [
        'inputOptions' => ['class' => 'form-control'],
        'labelOptions'=>['class'=>'col-sm-2 control-label'],
        'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
    ]); ?>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10" style="text-align: right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '添加') : Yii::t('app', '更新'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>


    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <div class="form-group field-curriculum-coverUrl">
        <div class="col-sm-offset-2 col-sm-10" id="imgdiv">
            <img id="imgShow" height="100" src="<?= empty($model->imgval)? Yii::$app->request->hostInfo.'/public/images/default_image.png': Url::to(Yii::$app->request->hostInfo.$model->imgval);?>" />
        </div>
    </div>

    <?= $form->field($model, 'imgval')->fileInput() ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'type')->radioList(['1'=>'文字','2'=>'图文'])?>



    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    init.push(function () {
        var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
        new uploadPreview({ UpBtn: "link-imgval", DivShow: "imgdiv", ImgShow: "imgShow" });

    });
</script>