<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Goodsclassify */

$this->title = '修改商品分类: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Goodsclassifies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'parent'=>$parent,
    ]) ?>

</div>