<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\SelectCity;
use common\models\User;
use \kartik\datetime\DateTimePicker;
/* @var $this yii\web\View */
/* @var $model common\models\Member */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .cus-img{
        min-width: 100px;
        width: auto;
    }
</style>
<script src="<?php echo Yii::$app->request->hostInfo;?>/public/js/uploadPreview.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo yii::$app->request->hostInfo;?>/public/My97DatePicker/WdatePicker.js"></script>
<div class="panel-body form-controls-demo">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
        'fieldConfig' => [
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions'=>['class'=>'col-sm-2 control-label'],
            'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
    ]); ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10" style="text-align: right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '创建') : Yii::t('app', '保存修改'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php if ($model->isNewRecord) :?>
        <?= $form->field($model, 'username')->textInput(['maxlength' => 255])->label('登录账户') ?>
        <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255])->label('登录密码') ?>
    <?php else:?>
        <div class="form-group field-user-username has-success">
            <label class="col-sm-2 control-label" for="user-username">登录账户</label>
            <div class="col-sm-10">
                <label><?php echo $model->username;?></label><a href="javascript:void(0);" id="updatepw" data-pw="<?php echo $model->password;?>" data-val="1">修改密码</a>
            </div>
        </div>
        <div id="password" style="display: none;">
            <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255]) ?>
        </div>
    <?php endif?>

        <?= $form->field($model, 'avatar')->widget('manks\FileInput', [
            'clientOptions' => [
                'pick' => [
                    'multiple' => false,
                ],
                'server' => Url::toRoute(['user/upload']),
                // 'accept' => [
                // 	'extensions' => 'png',
                // ],
            ],
        ])->label("管理员头像"); ?>


    <?php $form->field($model, 'avatar')->fileInput()->label('缩略图') ?>


    <?php $form->field($model, 'sex')->radioList(['1'=>'男','2'=>'女']) ?>

    <?php  $form->field($model, 'birthday')->textInput() ?>

    <?php
    is_null($model->status) and $model->status = 1;
    echo $form->field($model, 'status')->radioList($model->statusDic);
    ?>

    <?php $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
    <?php
      $ops = $model->isNewRecord ? [] :  ['disabled'=>true];
      echo $form->field($model, 'assignment')->dropDownList($model->getAdminAssignmentDic(), $ops)->label('类型');
    ?>

    <?= $form->field($model, 'provinces_id')->dropDownList(SelectCity::getAllCityName(), array_merge($ops, ['prompt' => '请选择']))->label('所在城市') ?>

    <?= $form->field($model, 'change_city')->dropDownList([
            '1' => '是',
            '0' => '否'
    ], ['prompt' => '请选择'])->label('管理多城市') ?>
    <?php
        $model->gc_ids = $model->gc_ids ? json_decode($model->gc_ids, true) : [];
        $goodGlasses = \common\models\GoodsClass::find()->where(['=', 'fid', 0])->all();
    ?>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    init.push(function () {
       // new uploadPreview({ UpBtn: "member-avatar", DivShow: "imgdiv", ImgShow: "imgShow" });

        $("#member-birthday").click(function(){
            WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})
        });

        $("#updatepw").click(function(){
            var val=$(this).attr("data-val");
            if (val==1) {
                $(this).html("取消密码修改");
                $(this).attr("data-val","2");
                $('#member-password').val("");
                $("#password").show();
            }else{
                $(this).html("密码修改");
                $(this).attr("data-val","1");
                $("#password").hide();
                $('#member-password').val($(this).attr("data-pw"));
            }
        });
    });
    $('input[name="User[gc_ids]"]').remove();
</script>