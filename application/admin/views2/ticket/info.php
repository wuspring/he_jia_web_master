<?php

use yii\helpers\Html;
use yii\grid\GridView;
use DL\service\UrlService;
use yii\widgets\LinkPager;
use common\models\Ticket;
use common\models\TicketApplyGoodsData;

/* @var $this yii\web\View */
/* @var $searchModel admin\search\TicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '展会设置';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>


    <div class="panel-body">
        <div class="panel">
            <?php if ($list) :?>
               <?php foreach ($list  as $k => $v):?>
              <div class="panel-heading">
                   <span class="panel-title">
                       <?= $v->name; ?>
                   </span>
               </div>
                <?php if ($v->status) :?>
                        <?php
                        $user = Yii::$app->user->identity;

                        $apply = \common\models\TicketApply::findOne([
                            'ticket_id' => $v->id,
                            'user_id' => $user->id
                        ]);

                        if (!$apply) {
                            $ticketModel = new Ticket();
                            if (in_array($v->type, array_keys($ticketModel->getTypeDic()))) {
                                ?>
                            <div class="panel-body">

                                <?= Html::a('报名参展', ['ticket-apply/join', 'ticketId' => $v->id], ['class' => 'btn btn-primary', 'data-key' => $v->id, 'data-button' => 1]); ?>
            </div>
                                <?php
                            }
                        }
                        ?>
                <?php endif;?>
              <div class="panel-body">
                  <table class="table table-striped table-bordered detail-view">
                      <tbody>
                          <tr><th class="col-lg-3">类型</th><td><?= $v->typeInfo; ?></td></tr>
                          <tr><th class="col-lg-3">预定金额</th><td><?= $v->order_price; ?></td></tr>
                          <tr><th>状态</th><td><?= $v->statusInfo; ?></td></tr>
                          <tr><th>参展数量</th><td><?= (int)$v->amount; ?></td></tr>
                          <tr><th>地址</th><td><?= $v->address; ?></td></tr>
                          <tr><th>添加时间</th><td><?= $v->create_time; ?></td></tr>
                          <?php if(isset($apply) and $apply) :?>
                          <?php switch ($apply->status) :case '0' :?>
                          <tr><th>申请状态</th><td>
                                <span style="display: inline-block;width: 90%"><?= '审核中'; ?></span>
                                  <span style="float: right;display: inline-block;max-width: 10%">
                                      <?= Html::a('查看', ['ticket-apply/join', 'ticketId' => $v->id], ['class' => 'btn btn-default', 'data-key' => $v->id, 'data-button' => 1]); ?>
                                  </span>
                                  </td></tr>
                          <?php break; case '1' :?>
                          <tr><th>申请状态</th><td><?= '已通过'; ?></td></tr>
                          <tr><th>展位号</th><td><?= $apply->zw_pos; ?></td></tr>
                          <?php break; case '2' :?>
                          <tr><th>申请状态</th><td>
                                  <span style="display: inline-block;width: 90%"><?= '已拒绝'; ?></span>
                                      <span style="float: right;display: inline-block;max-width: 10%"><?= Html::a('修改协议', ['ticket-apply/join', 'ticketId' => $v->id], ['class' => 'btn btn-primary', 'data-key' => $v->id, 'data-button' => 1]); ?></span>
                              </td></tr>
                          <tr><th>拒绝原因</th><td><?= $apply->reason; ?></td></tr>
                          <?php break; endswitch; ?>
                          <tr><th>申请时间</th><td><?= $apply->create_time; ?></td></tr>
                          <?php endif; ?>
                      </tbody>
                  </table>
                  <table class="table">
                      <thead>
                            投放详情
                      </thead>
                      <tbody>
                      <tr>
                          <td class="col-md-3">投放城市</td>
                          <td class="col-md-3">城市编号</td>
                          <td class="col-md-3">预约数量</td>
                      </tr>
                      <?php foreach ($v->ticketInfor as $tk=>$tv):?>
                          <tr>
                              <td class="col-md-3"><?=$tv->cityName?></td>
                              <td class="col-md-3"><?=$tv->provinces_id?></td>
                              <td class="col-md-3"><?=$tv->ticket_amount?></td>
                          </tr>
                      <?php endforeach;?>
                      </tbody>
                  </table>

                  <?php if(isset($apply) and $apply) :

                      $orderGoods = TicketApplyGoodsData::findAll([
                          'ticket_id' => $v->id,
                          'user_id' => $user->id,
                          'type' => TicketApplyGoodsData::TYPE_ORDER,
                      ]);

                      $couponGoods = TicketApplyGoodsData::findAll([
                          'ticket_id' => $v->id,
                          'user_id' => $user->id,
                          'type' => TicketApplyGoodsData::TYPE_COUPON,
                      ]);

                      ?>

                  <span class="panel-title">
                       参展商品
                   </span>
                  <div class="panel-body">
                      <table class="table">
                          <thead>
                          爆款预约
                          </thead>
                          <tbody>
                          <tr>
                              <td class="col-md-3"><b>商品名称</b></td>
                              <td class="col-md-3"><b>封面图</b></td>
                              <td class="col-md-3"><b>活动价格</b></td>
                              <td class="col-md-3"><b>库存</b></td>
                          </tr>
                          <?php if ($couponGoods) :?>
                              <?php foreach ($couponGoods as $shop):?>
                                  <tr>
                                      <td class="col-md-3"><?= $shop->goods->goods_name; ?></td>
                                      <td class="col-md-3"><img src="<?= $shop->good_pic; ?>" style="max-width: 300px" alt=""></td>
                                      <td class="col-md-3"><?= $shop->good_price; ?></td>
                                      <td class="col-md-3"><?= $shop->good_amount; ?></td>
                                  </tr>
                              <?php endforeach;?>
                          <?php else :?>
                              <tr>
                                  <td colspan="4" class="col-md-12" style="text-align: center">暂未添加</td>
                              </tr>
                          <?php endif; ?>
                          </tbody>
                      </table>
                  </div>

                  <div class="panel-body">
                      <table class="table">
                          <thead>
                          预存 享特价
                          </thead>
                          <tbody>
                          <tr>
                              <td class="col-md-3"><b>商品名称</b></td>
                              <td class="col-md-3"><b>封面图</b></td>
                              <td class="col-md-3"><b>活动价格</b></td>
                              <td class="col-md-3"><b>库存</b></td>
                          </tr>
                          <?php if ($orderGoods) :?>
                              <?php foreach ($orderGoods as $shop):?>
                                  <tr>
                                      <td class="col-md-3"><?= $shop->goods->goods_name; ?></td>
                                      <td class="col-md-3"><img src="<?= $shop->good_pic; ?>" style="max-width: 300px" alt=""></td>
                                      <td class="col-md-3"><?= $shop->good_price; ?></td>
                                      <td class="col-md-3"><?= $shop->good_amount; ?></td>
                                  </tr>
                              <?php endforeach;?>
                          <?php else :?>
                              <tr>
                                  <td colspan="4" class="col-md-12" style="text-align: center">暂未添加</td>
                              </tr>
                          <?php endif; ?>
                          </tbody>
                      </table>
                <?php endif; ?>
              </div>
               <?php endforeach;?>
               <?php else :?>
                <div class="col-lg-12" style="text-align: center;height: 60px;line-height: 60px;">
                    （暂无 敬请期待）
                </div>
               <?php endif; ?>
        </div>

        <?= LinkPager::widget(['pagination' => $pagination,]) ?>
    </div>
</div>
<script>
    var data = {
        'store_id' : <?= Yii::$app->user->id; ?>,
        'type' : '<?= 2 ; ?>',
        'r' : 'ticket/join'
    };

    //$('a[data-button]').click(function () {
    //    data.key = $(this).attr('data-key');
    //    $.post('<?//= UrlService::build("ticket/join"); ?>//', data, function(res) {
    //        console.log(res);
    //        if (res.status) {
    //            window.location.reload();
    //            return false;
    //        }
    //
    //        alert(res.msg);
    //    });
    //});


</script>
