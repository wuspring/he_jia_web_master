<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\GoodsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="goods-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'goods_name') ?>

    <?= $form->field($model, 'goods_jingle') ?>

    <?= $form->field($model, 'gc_id') ?>

    <?= $form->field($model, 'use_attr') ?>

    <?php // echo $form->field($model, 'brand_id') ?>

    <?php // echo $form->field($model, 'goods_price') ?>

    <?php // echo $form->field($model, 'goods_promotion_price') ?>

    <?php // echo $form->field($model, 'goods_promotion_type') ?>

    <?php // echo $form->field($model, 'goods_marketprice') ?>

    <?php // echo $form->field($model, 'goods_serial') ?>

    <?php // echo $form->field($model, 'goods_storage_alarm') ?>

    <?php // echo $form->field($model, 'goods_click') ?>

    <?php // echo $form->field($model, 'goods_salenum') ?>

    <?php // echo $form->field($model, 'goods_collect') ?>

    <?php // echo $form->field($model, 'attr') ?>

    <?php // echo $form->field($model, 'goods_spec') ?>

    <?php // echo $form->field($model, 'goods_body') ?>

    <?php // echo $form->field($model, 'mobile_body') ?>

    <?php // echo $form->field($model, 'goods_storage') ?>

    <?php // echo $form->field($model, 'goods_image') ?>

    <?php // echo $form->field($model, 'goods_state') ?>

    <?php // echo $form->field($model, 'goods_addtime') ?>

    <?php // echo $form->field($model, 'goods_edittime') ?>

    <?php // echo $form->field($model, 'goods_vat') ?>

    <?php // echo $form->field($model, 'goods_commend') ?>

    <?php // echo $form->field($model, 'evaluation_good_star') ?>

    <?php // echo $form->field($model, 'evaluation_count') ?>

    <?php // echo $form->field($model, 'is_virtual') ?>

    <?php // echo $form->field($model, 'is_appoint') ?>

    <?php // echo $form->field($model, 'is_presell') ?>

    <?php // echo $form->field($model, 'have_gift') ?>

    <?php // echo $form->field($model, 'isdelete') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'integral') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
