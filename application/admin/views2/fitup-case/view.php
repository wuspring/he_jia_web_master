<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FitupCase */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fitup Cases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fitup-case-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fitup_company',
            'address',
            'design',
            'area',
            'house_type',
            'design_style',
            'budget',
            'tab_text:ntext',
            'tab',
            'create_time',
            'is_del',
        ],
    ]) ?>

</div>
