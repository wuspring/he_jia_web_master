<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\search\FitupCaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fitup-case-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'fitup_company') ?>

    <?= $form->field($model, 'address') ?>

    <?= $form->field($model, 'design') ?>

    <?= $form->field($model, 'area') ?>

    <?php // echo $form->field($model, 'house_type') ?>

    <?php // echo $form->field($model, 'design_stype') ?>

    <?php // echo $form->field($model, 'budget') ?>

    <?php // echo $form->field($model, 'tab_text') ?>

    <?php // echo $form->field($model, 'tab') ?>

    <?php // echo $form->field($model, 'create_time') ?>

    <?php // echo $form->field($model, 'is_del') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
