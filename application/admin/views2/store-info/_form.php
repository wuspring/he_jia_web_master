<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StoreInfo */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/js/grids/grid_input.js');
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/ueditor/ueditor.config.js');
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/ueditor/ueditor.all.min.js');
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/ueditor/lang/zh-cn/zh-cn.js');
?>
<style>
    .inline .radio,.inline .checkbox{display: inline-block;margin: 0 5px;}
    #info-describe {margin-top: 20px;padding:0;margin:20px 0;width:100%;height:auto;border: none;}

</style>

<div class="panel-body store-info-form">
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
        'fieldConfig' => [
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions'=>['class'=>'col-sm-2 control-label'],
            'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
    ]);


    $shops = \common\models\StoreShop::findAll([
            'user_id' => $userModel->id
    ]);
    $shopNotice = '暂未添加门店';
    $shopDatas = [];
    if ($shops) {
        $shopNotice = '请选择默认门店';
        $shopDatas = \yii\helpers\ArrayHelper::map($shops, 'id', 'store_name');
    }

    ?>
    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
    </div>

    <?= $form->field($userModel, 'nickname')->textInput(['maxlength' => 255])->label('商户名称') ?>
    <?php $form->field($userModel, 'avatar')->fileInput()->label('缩略图') ?>

    <?= $form->field($userModel, 'avatar')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => false,
            ],
            'server' => Url::toRoute(['store-info/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ])->label("缩略图"); ?>


    <?= $form->field($model, 'tab_text')->textInput()->label('标签设置') ?>
    <?= $form->field($model, 'default_shop')->dropDownList($shopDatas, ['prompt'=>$shopNotice])->label('默认门店') ?>

    <?= $form->field($model, 'describe')->textarea(['rows' => 6, 'class'=>''])->label('品牌介绍'); ?>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    init.push(function () {
        var ue = UE.getEditor('storeinfo-describe', {
            'zIndex':9,
        });
    });

    $(function () {
        GridInput($('#storeinfo-tab_text'), true);
    })
</script>
