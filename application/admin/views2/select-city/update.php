<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SelectCity */

$this->title = 'Update Select City: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Select Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="select-city-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
