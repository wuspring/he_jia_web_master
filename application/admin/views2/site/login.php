<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\web\Request;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
$this->title = '和家网';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login">
    <div class="row">
    <div class="logo">
       <h1>网站后台登录系统</h1>
       <p>BACKGROUND MANAGEMENT SYSTEM</p>
    </div>
        <div class="frame" style="display: flex;flex-direction: row;">
            <div class="loginleft">
                <img src="/public/images/form_ico.png">
            </div>
             <div class="formdiv">
                 <?php $form = ActiveForm::begin([
                     'id' =>'login-form',
                     'layout' => 'horizontal',
                     'fieldConfig' => [
                         'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                         'horizontalCssClasses' => [
                             'label' => '',
                             'offset' => '',
                             'wrapper' => '',
                             'error' => '',
                             'hint' => '',
                         ],
                     ],
                 ]); ?>
                 <?= $form->field($model, 'username',['template'=>'<img class="ico_img" src="/public/images/yhm.png">{input}{error}']) ?>
                 <?= $form->field($model, 'password',['template'=>'<img  class="ico_img" src="/public/images/mm.png">{input}{error}'])->passwordInput() ?>
                 <?= $form->field($model, 'verifyCode',['template'=>'<img class="ico_img" src="/public/images/verifycode.png">{input}{error}'])->widget(Captcha::className(), [
                     'template' => '<div class="col-sm-7 captcha">{input}</div><div class="col-sm-5 logincaptcha">{image}</div>',
                 ]) ?>
                 <?= $form->field($model, 'rememberMe')->checkbox() ?>
                 <div class="form-group">

                     <?= Html::submitButton('立即登录', ['class' => 'btn ', 'name' => 'login-button']) ?>

                 </div>
                 <?php ActiveForm::end(); ?>
             </div>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>          