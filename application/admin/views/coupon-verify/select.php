                   <?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use \common\models\Order;

/* @var $this yii\web\View */
/* @var $searchModel app\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '查询优惠券';
$this->params['breadcrumbs'][] = $this->title;
?>
<script language="javascript" type="text/javascript" src="<?php echo yii::$app->request->hostInfo;?>/public/My97DatePicker/WdatePicker.js"></script>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <style>
        .order-item {
            border: 1px solid transparent;
            margin-bottom: 1rem;
        }

        .order-item table {
            margin: 0;
        }

        .order-item:hover {
            border: 1px solid #3c8ee5;
        }

        .goods-item {
            margin-bottom: .75rem;
        }

        .goods-item:last-child {
            margin-bottom: 0;
        }

        .goods-pic {
            width: 5.5rem;
            height: 5.5rem;
            display: inline-block;
            background-color: #ddd;
            background-size: cover;
            background-position: center;
            margin-right: 1rem;
        }

        .goods-name {
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }

        .order-tab-1 {
            width: 40%;
        }

        .order-tab-2 {
            width: 10%;
            text-align: center;
        }

        .order-tab-3 {
            width: 10%;
            text-align: center;
        }

        .order-tab-4 {
            width: 20%;
            text-align: center;
        }

        .order-tab-5 {
            width: 10%;
            text-align: center;
        }
        .order-tab-6 {
            width: 10%;
            text-align: center;
        }

        .status-item.active {
            color: inherit;
        }
        .label1 {
            height: 34px;
            line-height: 34px;
        }

        .tab_title, .tab_title a {
            margin: 0;
            padding: 0;
            text-align: center;
        }

        .col-lg-2.tab_title.active>a {
            color: #fff!important;
            background-color: #1a7ab9;
        }
    </style>
    <div class="panel-body">

        <?php include __DIR__ . '/header.php'; ?>

        <div class="order-item" style="text-align: center">
        <?php if ($coupon) :?>
            <table class="table table-bordered bg-white">
                    <tr>
                        <td colspan="6" style="text-align: left;">
                            <span class="mr-5">订单号：<?= $order_item['orderid'] ?></span>
                            <span class="mr-5" style="margin-left: 15px;"><?= $order_item['add_time'] ?></span>

                        </td>
                    </tr>
                    <tr>
                        <td class="order-tab-6">
                            <div><?= $order_item['ticket'] ?></div>
                        </td>
                        <td class="order-tab-2">
                            <div>所在城市：<br><?= $order_item['ticket_city'] ?></div>

                        </td>
                        <td class="order-tab-1">
                                <div class="goods-item" flex="dir:left box:first">
                                    <div class="fs-0 col-md-2">
                                        <div class="goods-pic"
                                             style="background-image: url('<?= $order_item['goods_item']['goods_pic'] ?>')"></div>
                                    </div>
                                    <div class="goods-info col-md-10">
                                        <div class="goods-name"><?= $order_item['goods_item']['goods_name'] ?></div>
                                        <div class="fs-sm">数量：
                                            <span class="text-danger"><?= $order_item['goods_item']['goods_num'] ?></span>
                                        </div>
                                        <div class="fs-sm">商品总价：
                                            <span class="text-danger">￥<?= $order_item['goods_item']['goods_price'] * $order_item['goods_item']['goods_num'] ?>
                                                </span></div>
                                    </div>
                                </div>
                        </td>
                        <td class="order-tab-4">
                            <?php if (trim($order_item['state_txt'])=='待支付') :?>
                            <div class="label label-success" style="background: red;">
                                <?= $order_item['state_txt'] ?>
                            </div>
                            <?php else:?>
                            <div class="label label-success">
                                <?= $order_item['state_txt'] ?>
                            </div>
                            <?php endif; ?>
                        </td>
                        <td class="order-tab-5">

                            <?php if ($order_item['goods_item']['store_id'] == Yii::$app->user->id) :?>
                            <!-- 当前店铺为商品发布者 -->
                            <a class="btn btn-sm btn-primary" data-id="use" data-val="<?=$order_item['goods_item']['goods_id'] ;?>" data-code="<?= $coupon->code; ?>" href="javascript:void(0)">使用</a>
                            <?php endif; ?>

                            <?php if ($order_item['order_state'] == Order::STATUS_PAY): ?>
                                <a class="btn btn-sm btn-primary fahuo" href="javascript:"
                                   data-id="<?= $order_item['orderid'] ?>">改签</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6"  style="text-align: left;">
                                <span>用户：<?= $order_item['buyer_name'] ?></span>
                                <span class="mr-3">电话：<?= $order_item['receiver_mobile'] ?></span>
                        </td>
                    </tr>
                </table>
        <?php else :?>
            验证码错误或已被使用
        <?php endif; ?>
        </div>

    </div>

</div>

<div id="fahuoModal" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">改签商品</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="code" value="<?= $coupon->code; ?>" />
                <p>请选择改签的商品：</p>
                <div style="clear: both">
                    <?php foreach ($goods AS $good) :?>
                    <div class="order-item col-sm-6" style="">
                        <table class="table table-bordered bg-white">
                            <tbody>
                            <tr>
                                <td class="order-tab-12" style="display:table-cell; vertical-align:middle">
                                    <input type="radio" name="goodId" value="<?= $good->id; ?>" class="md-checkbox">
                                </td>
                                <td class="order-tab-12">
                                    <div style="clear: both;">
                                        <div class="fs-0 col-md-4" style="height:40px;background-image: url('<?= $good->goods_pic; ?>')">
                                        </div>
                                        <div class="goods-info col-md-8" style="text-align: right">
                                            <div class="goods-name"><?= $good->goods_name; ?></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div style="clear: both"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary sendGoods">确认</button>
            </div>
        </div>
    </div>
</div>
<script>
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    init.push(function () {

        $(document).on("click",".fahuo",function () {
            $("#ordercode").val($(this).attr('data-id'));

            $("#fahuoModal").modal();
        });

        var useCoupon = function(code, goodId) {
            var data = {
                code : code,
                goodId : goodId
            };

            if (confirm("确认使用优惠券？")) {console.log(data);
                $.post('<?php echo Url::toRoute(['coupon-verify/use-coupon'])?>', data,function (res) {
                    if(res.status) {
                        alert(res.msg);
                        window.location.href = window.location.href;
                    }else{
                        alert(res.msg);
                    }
                },'json');
            }

        };

        $('[data-id="use"]').click(function () {
            var that = $(this);

            if (confirm("确认使用优惠券？")) {
                useCoupon(that.attr('data-code'), that.attr('data-val'));
            }
        });

        $(document).on('click','.sendGoods',function () {
            var goodId = $('input[name="goodId"]:checked').val(),
                code = $('input[name="code"]').val();

            useCoupon(code, goodId);
            $('#fahuoModal').modal('hide');
        });

        $("#date_start").click(function(){
            WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d'})
        });
        $("#date_end").click(function(){
            WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d',minDate:'#F{$dp.$D(\'date_start\')}'})
        });



    });

</script>