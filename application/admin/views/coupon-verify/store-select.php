                   <?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use \common\models\Order;

/* @var $this yii\web\View */
/* @var $searchModel app\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '查询优惠券';
$this->params['breadcrumbs'][] = $this->title;
?>
<script language="javascript" type="text/javascript" src="<?php echo yii::$app->request->hostInfo;?>/public/My97DatePicker/WdatePicker.js"></script>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <style>
        .order-item {
            border: 1px solid transparent;
            margin-bottom: 1rem;
        }

        .order-item table {
            margin: 0;
        }

        .order-item:hover {
            border: 1px solid #3c8ee5;
        }

        .goods-item {
            margin-bottom: .75rem;
        }

        .goods-item:last-child {
            margin-bottom: 0;
        }

        .goods-pic {
            width: 5.5rem;
            height: 5.5rem;
            display: inline-block;
            background-color: #ddd;
            background-size: cover;
            background-position: center;
            margin-right: 1rem;
        }

        .goods-name {
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }

        .order-tab-1 {
            width: 40%;
        }

        .order-tab-2 {
            width: 10%;
            text-align: center;
        }

        .order-tab-3 {
            width: 10%;
            text-align: center;
        }

        .order-tab-4 {
            width: 20%;
            text-align: center;
        }

        .order-tab-5 {
            width: 10%;
            text-align: center;
        }
        .order-tab-6 {
            width: 10%;
            text-align: center;
        }

        .status-item.active {
            color: inherit;
        }
        .label1 {
            height: 34px;
            line-height: 34px;
        }

        .tab_title, .tab_title a {
            margin: 0;
            padding: 0;
            text-align: center;
        }

        .col-lg-2.tab_title.active>a {
            color: #fff!important;
            background-color: #1a7ab9;
        }
    </style>
    <div class="panel-body">

        <?php include __DIR__ . '/header.php'; ?>

        <div class="order-item" style="text-align: center">
        <?php if ($coupon) :?>
            <table class="table table-bordered bg-white">
                    <tr>
                        <td colspan="6" style="text-align: left;">
                            <span class="mr-5">订单号：<?= $member->nickname ?></span>
                            <span class="mr-5" style="margin-left: 15px;">联系方式：<?= $member->mobile ?></span>

                        </td>
                    </tr>
                    <tr>
                        <td class="order-tab-12">
                            <div class="goods-item" flex="dir:left box:first">
                                <div class="fs-0 col-md-2">
                                    <div class="goods-pic"
                                         style="background-image: url('<?= $goods->goods_pic ?>')"></div>
                                </div>
                                <div class="goods-info col-md-10">
                                    <div class="goods-name"><?= $goods->goods_name ?></div>
                                    <div class="fs-sm">商品价格：
                                        <span class="text-danger">￥<?= $goods->goods_price ?>
                                                </span></div>
                                    <div class="fs-sm">优惠金额：
                                        <span class="text-danger">￥<?= $ticketInfo->money ?></span>
                                    </div>

                                </div>
                            </div>
                        </td>
                        <td class="order-tab-5">
                            <a class="btn btn-sm btn-primary" data-id="use" data-code="<?= $coupon->code; ?>" href="javascript:void(0)">使用</a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6"  style="text-align: left;">
                            <span class="mr-5" style="margin-left: 15px;">领取时间<?= $coupon->addtime ?></span>
                        </td>
                    </tr>
                </table>
        <?php else :?>
            <div class="col-lg-12" style="text-align: center;height: 150px;line-height: 150px;">
                未找到有效的店铺优惠券信息, 请确认店铺与优惠券是否匹配
            </div>
        <?php endif; ?>
        </div>

    </div>

</div>


<script>
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    init.push(function () {

        $(document).on("click",".fahuo",function () {
            $("#ordercode").val($(this).attr('data-id'));

            $("#fahuoModal").modal();
        });

        var useCoupon = function(code, goodId) {
            var data = {
                code : code
            };

            if (confirm("确认使用优惠券？")) {console.log(data);
                $.get('<?php echo Url::toRoute(['coupon-verify/use-store-coupon'])?>', data,function (res) {
                    if(res.status) {
                        alert(res.msg);
                        window.location.href = window.location.href;
                    }else{
                        alert(res.msg);
                    }
                },'json');
            }
        };

        $('[data-id="use"]').click(function () {
            var that = $(this);
                useCoupon(that.attr('data-code'), that.attr('data-val'));
        });

    });

</script>