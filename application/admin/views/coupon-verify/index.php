<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = '查询优惠券';
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>

<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?php include __DIR__ . '/header.php'; ?>

    <div class="panel-body form-controls-demo">
        <?php $form = ActiveForm::begin([
            'action' => ['coupon-verify/store-coupon'],
            'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
            'fieldConfig' => [
                'inputOptions' => ['class' => 'form-control'],
                'labelOptions' => ['class' => 'col-sm-2 control-label'],
                'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
        ]); ?>

        <div class="form-group field-ticket-name">
            <label class="col-sm-2 control-label " for="ticket-name">优惠码</label>
            <div class="col-sm-8">
                <input type="text" id="ticket-name" class="form-control" name="name" maxlength="255" placeholder="请输入优惠码">
                <div class="help-block"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-xs-8">
                <?= Html::submitButton('查询', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
