<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Jingxiaoshang */

$this->title = '代理商审核 ： ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Jingxiaoshangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jingxiaoshang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
