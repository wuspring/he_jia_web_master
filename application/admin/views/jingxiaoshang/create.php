<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Jingxiaoshang */

$this->title = 'Create Jingxiaoshang';
$this->params['breadcrumbs'][] = ['label' => 'Jingxiaoshangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jingxiaoshang-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
