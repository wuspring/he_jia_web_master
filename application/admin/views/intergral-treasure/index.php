<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\search\GoodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '积分夺宝';
$this->params['breadcrumbs'][] = ['label' => '积分商城', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
        <div style="float: right;text-align: right;">
            <?= Html::a('添加夺宝', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>




    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                array(
                    'attribute' => 'goods_name',
                    'label'=>'商品名称',
                    'value'=>function($filterModel){
                        return StringHelper::truncate($filterModel->goods_name, 20);

                    },
                ),
                array(
                    'format' => 'raw',
                    'attribute' => 'goods_image',
                    'label'=>'缩略图',
                    'value'=>function($filterModel){
                        $itm=$filterModel->goods_pic;
                        if ($itm){
                            return Html::img(yii::$app->request->hostInfo.$itm, ['width' =>30]);
                        }else{
                            return '无';
                        }

                    },
                ),
                [
                    'attribute' => 'goods_integral',
                    'label'=>'单次夺宝积分',
                ],
                [
                    'attribute' => 'goods_storage',
                    'label'=>'开奖所需人次',
                ],
                [
                    'attribute' => 'goods_salenum',
                    'label'=>'已参加人次',
                ],
//                'goods_click',
//                'goods_salenum',
                array(
                    'format' => 'raw',
                    'attribute' => 'goods_state',
                    'label'=>'状态',
                    'value'=>function($filterModel){
                        switch ($filterModel->goods_state){
                            case 10:
                                return '<span class="label label-danger">商品违规</span>';
                                break;
                            case 0:
                                return '<button type="button" class="btn btn-default btn-xs upgoods" data-id="'.$filterModel->id.'">上架</button>';
                                break;
                            case 1:
                                return '<button type="button" class="btn btn-success btn-xs dngoods" data-id="'.$filterModel->id.'">下架</button>';
                                break;
                        }
                    },
                ),

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>'操作',
                    'template' => '{view} {update} {delete}',
                    'buttons'=>[
                        'view'=>function($url, $model, $key){
                            $url=yii::$app->request->hostInfo.'/admin.php?r=intergral-treasure/view&id='.$model->id ;
                            return  Html::a('查看', $url, ['title' => '查看','data-toggle'=>'modal'] ) ;
                        },
                        'update' => function ($url, $model, $key) {
                            return  Html::a('更新', $url, ['title' => '更新','data-toggle'=>'modal'] ) ;
                        },
                        'delete'=> function ($url, $model, $key) {
                            return  Html::a('删除', 'javascript:void(0)', ['title' => '删除','data-toggle'=>'modal', 'data-id'=>$model->id] ) ;
                        },
                    ],

                ],
            ],
        ]); ?>
    </div>

</div>
<script type="text/javascript">
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";

    $(document).on("click",'.upgoods',function () {
        var id=$(this).attr('data-id');
        var upbut=this;

        $.post("<?php echo Url::toRoute(['intergral-treasure/upgoods']);?>",{_csrf:_csrf,id:id},function (res) {
            if (res.code==1){
                var html='<button type="button" class="btn btn-success btn-xs dngoods" data-id="'+id+'">下架</button>'
                $(upbut).parent().html(html);
            }else{
                alert(res.mg);
            }
        },'json');

    });

    $(document).on("click",'.dngoods',function () {
        var id=$(this).attr('data-id');
        var dnbut=this;

        $.post("<?php echo Url::toRoute(['intergral-treasure/dngoods']);?>",{_csrf:_csrf,id:id},function (res) {
            if (res.code==1){
                var html='<button type="button" class="btn btn-default btn-xs upgoods" data-id="'+id+'">上架</button>'
                $(dnbut).parent().html(html);
            }else{
                alert(res.mg);
            }
        },'json');

    });

    $(".fa-bitbucket").click(function(){
        if (confirm("确定要删除该分类么？")) {
            var del=$(this).parent('a');
            var _csrf =$("#_csrf").val();
            $.post("<?php echo Url::toRoute('intergral-treasure/delete');?>"+"&id="+del.attr('data-id'),{_csrf:_csrf},function(data){
                if (data.status) {
                    window.location.reload();
                }else{
                    alert(data.msg);
                }

            },'json');
        }
    });
</script>