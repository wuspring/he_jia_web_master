<?php

use yii\helpers\Html;
use DL\service\UrlService;
/* @var $this yii\web\View */
/* @var $model common\models\Goods */

$this->title =($defaultTitle==0)?'编辑商品':'添加商品';
$this->params['breadcrumbs'][] = ['label' => '积分商城', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '积分夺宝', 'url' =>['intergral-treasure/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>
<div class="panel">
    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>