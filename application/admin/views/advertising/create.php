<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Advertising */

$this->title = '添加广告';
$this->params['breadcrumbs'][] = ['label' => '手机端', 'url' => ['advertising/index']];
$this->params['breadcrumbs'][] = ['label' => '广告列表', 'url' => ['advertising/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
        'adsense'=>$adsense,
    ]) ?>

</div>