<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\AdvertisingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advertising-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'form-inline'],
    ]); ?>

    <?= $form->field($model, 'adid')->dropDownList($model->getAdsenseList(),['prompt'=>'请选择'])->label('广告位')->error(false) ?>

    <?php // echo $form->field($model, 'isShow') ?>

    <?php // echo $form->field($model, 'clickRate') ?>

    <?php // echo $form->field($model, 'sort') ?>

    <?php // echo $form->field($model, 'createTime') ?>

    <?php // echo $form->field($model, 'modifyTime') ?>

    <div class="form-group">
        <?= Html::submitButton('筛选', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
