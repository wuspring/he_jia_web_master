<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\search\AdvertisingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '广告列表';
$this->params['breadcrumbs'][] = ['label' => '手机端', 'url' => ['advertising/index']];
$this->params['breadcrumbs'][] = $this->title;

global $cid;
$cid = Yii::$app->request->get('id', 0);
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active > a {
        color: #fff !important;
        background-color: #1a7ab9;
    }

    .joinCoupon {
        margin-left: 5px
    }

    .theme-default .nav-tabs > li.active > a, .theme-default .nav-tabs > li.active > a:focus, .theme-default .nav-tabs > li.active > a:hover {
        background: #1d89cf;
        border-bottom: 2px solid #1a7ab9;
    }

    .col-lg-2.tab_title.active > a {
        color: #fff !important;
        background-color: #1a7ab9;
    }

    .nav-tabs > li {
        width: 10%;
        height: 40px;
        line-height: 40px;
        overflow: hidden;
    }

    .nav-tabs > li a {
        width: 100%;
        height: 40px;
        line-height: 40px;
        overflow: hidden;
        display: block;
        padding: 0;
    }
</style>

<div class="panel">
    <div class="panel-heading" style="margin-bottom: 10px;">
        <div class="panel-title">
            <?= Html::encode($this->title) ?>
            <span style="float: right;text-align: right;">
            <?= Html::a('添加广告', Url::toRoute(['advertising/create', 'adid' => $cid]), ['class' => 'btn btn-success']) ?>
            </span>
        </div>
    </div>
    <div style="display: none;">
        <ul class="nav nav-tabs dl_tab">
            <?php foreach ($lists AS $i => $list) : ?>
            <li class="col-lg-2 tab_title <?= ($list->id == $key) ? 'active' : '' ?>" title="<?= $list->name; ?>"
                style="width: 14.2%;"><a
                        href="<?= \DL\service\UrlService::build(['advertising/index', 'id' => $list->id]); ?>"><?= $list->name; ?></a>
            </li>
            <?php if ($i == 6) : ?>
        </ul>
        <ul class="nav nav-tabs dl_tab">
            <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>

    <div class="panel-body">
        <p>
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                // 'id',
                array(
                    'format' => 'raw',
                    'attribute' => 'pictureTm',
                    'label' => '主题缩略图',
                    'value' => function ($filterModel) {
                        return Html::img(yii::$app->request->hostInfo . $filterModel->pictureTm, ['width' => 30]);
                    },
                ),
                // 'adid',
                'title',
                array(
                    'format' => 'raw',
                    'attribute' => 'adid',
                    'label' => '广告位',
                    'value' => function ($filterModel) {
                        return empty($filterModel->adsense->name) ? "-" : $filterModel->adsense->name;
                    },
                ),
                // 'picture',
                // 'pictureTm',
                // 'url:url',
                // 'isShow',
                //  'clickRate',
                'sort',
//            'createTime',
//            array(
//                'attribute'=>'adid',
//
//                ),
                // 'picture',
                // 'pictureTm',

                // 'url:url',
                // 'isShow',

                //  'clickRate',
//             'sort',
//             'createTime',
                // 'modifyTime',
//            array(
//                'attribute' => 'isShow',
//                'label'=>'是/否显示',
//                'format' => 'raw',
//                'value'=>function($filterModel){
//                    $url="javascript:void(0)";
//                    if ($filterModel->isShow == 0) {
//                        return  Html::a('<span class="glyphicon glyphicon-eye-close"></span>', $url, ['title' => '是/否显示','data-toggle'=>$filterModel->id,'data-status'=>$filterModel->isShow,'id'=>'data-isShow'] ) ;
//                    }else{
//                        return  Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => '是/否显示','data-toggle'=>$filterModel->id,'data-status'=>$filterModel->isShow,'id'=>'data-isShow'] ) ;
//                    }
//
//                },
//            ),
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => '操作',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('查看', $url, ['title' => '查看', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('更新', $url, ['title' => '更新', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('删除', $url, ['title' => '删除', 'class' => 'a_margin', 'data' => [
                                'confirm' => '确认删除，删除后不可恢复?',
                                'method' => 'post',
                            ]]);
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
<script type="text/javascript">
    init.push(function () {
        var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>";

        $("#data-isShow").on('click', function () {
            var th = $(this);
            var id = $(this).attr('data-toggle');
            var status = $(this).attr('data-status');
            if (status == 0) {
                status = 1;
            } else {
                status = 0;
            }
            $.post("<?php echo Url::toRoute(['/advertising/isshow']);?>", {
                _csrf: _csrf,
                id: id,
                status: status
            }, function (data) {
                if (data == 0) {
                    $(th).find('span').attr('class', 'glyphicon glyphicon-eye-close');
                    $(th).attr('data-status', 0);
                } else {
                    $(th).find('span').attr('class', 'glyphicon glyphicon-eye-open');
                    $(th).attr('data-status', 1);
                }
            });
        });
    });
</script>