<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FloorWap */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="panel-heading">
    <span class="panel-title"><?= Html::encode($this->title) ?>
        <span class="form-group" style="float: right;text-align: right">
            <?= Html::submitButton($model->isNewRecord ? '添加' : '保存', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </span>

    </span>
</div>

<div class="panel-body">




    <?= $form->field($model, 'type')->dropDownList($model->getTypeDic()) ?>

    <?= $form->field($model, 'ticket_id')->dropDownList($model->getTicketList()) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'topimg')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => true,
            ],
            'server' => \yii\helpers\Url::toRoute(['site/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ]); ?>

    <?= $form->field($model, 'nextimg')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => true,
            ],
            'server' => \yii\helpers\Url::toRoute(['site/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ]); ?>


    <?= $form->field($model, 'status')->radioList(['1'=>'显示','0'=>'隐藏']) ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <?php ActiveForm::end(); ?>
</div>
<script>
    init.push(function () {
        $('#floorwap-type').change(function () {
            var type = $(this).val();
            var href = "<?=\yii\helpers\Url::to(['floor-wap/ticket'])?>";
            $.get(href,{type : type},function(res){
                $('#floorwap-ticket_id').html(res.data);
            },'json');
        })
    });
</script>
