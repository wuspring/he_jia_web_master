<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FloorWap */

$this->title = '编辑';
$this->params['breadcrumbs'][] = ['label' => '手机端', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '手机端楼层管理', 'url' => ['floor-wap/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
