<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/js/uploadPreview.js');
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/ueditor/ueditor.config.js');
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/ueditor/ueditor.all.min.js');
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/ueditor/lang/zh-cn/zh-cn.js');
?>
<style>

    #news-content {margin-top: 20px;padding:0;margin:20px 0;width:100%;height:auto;border: none;}
</style>
<?php $form = ActiveForm::begin([
    'options' => ['enctype' =>'multipart/form-data'],
    'fieldConfig' => [
        'inputOptions' => ['class' => 'form-control'],
        'labelOptions'=>['class'=>'col-sm-2 control-label'],
        'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
]); ?>
<div class="panel-heading">
    <span class="panel-title"><?= Html::encode($this->title) ?></span>
    <div style="float: right;text-align: right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '发布') : Yii::t('app', '保存修改'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<div class="panel-body form-controls-demo">

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'seokey')->textarea(['rows' => 6])->label('关键字') ?>
    <?= $form->field($model, 'seoDepict')->textarea(['rows' => 6])->label('描述') ?>

    <?=$form->field($model, 'type')->dropDownList($model->getTypeDic())->label("活动类型") ?>

    <?= $form->field($model, 'themeImg')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => false,
            ],
            'server' => Url::toRoute(['news/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ])->label("商品主图"); ?>



    <?= $form->field($model, 'content')->textarea(['rows' => 6])->label("内容") ?>


    <?php $form->field($model, 'seokey')->textInput(['maxlength' => 255]) ?>

    <?php $form->field($model, 'seoDepict')->textInput(['maxlength' => 255]) ?>


    <?= $form->field($model, 'isShow')->radioList(['0'=>'不显示','1'=>'显示']) ?>

    <?= $form->field($model, 'isRmd')->radioList(['0'=>'不是热门','1'=>'是热门']) ?>

    <?php $form->field($model, 'clickRate')->textInput() ?>

    <?= $form->field($model, 'writer')->textInput(['maxlength' => 255]) ?>
    <?php $model->provinces_id =json_decode($model->provinces_id) ;?>
    <?= $form->field($model, 'provinces_id')->checkboxList($model->getSelectCity())->label("选择城市") ?>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
        init.push(function () {
              var ue = UE.getEditor('news-content',{
                'zIndex':9,
            });
    });
</script>