<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = '修改文章: ' . ' ' . $model->title;

?>
<div class="panel">

    <?= $this->render('_form_activity', [
        'model' => $model,
    ]) ?>

</div>
