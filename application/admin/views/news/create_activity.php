<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = '添加文章';
$this->params['breadcrumbs'][] = ['label' => '资讯管理', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '品牌活动', 'url' => ['news/index-activity']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <?= $this->render('_form_activity', [
        'model' => $model,
    ]) ?>

</div>