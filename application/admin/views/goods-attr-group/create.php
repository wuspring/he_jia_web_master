<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GoodsAttrGroup */

$this->title = 'Create Goods Attr Group';
$this->params['breadcrumbs'][] = ['label' => 'Goods Attr Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goods-attr-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
