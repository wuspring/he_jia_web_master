<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\GoodsAttr */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="goods-attr-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $form->field($model, 'group_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'attr_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_delete')->textInput() ?>

    <?= $form->field($model, 'is_default')->textInput() ?>


    <?php ActiveForm::end(); ?>

</div>
