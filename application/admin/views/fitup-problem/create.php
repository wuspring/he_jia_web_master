<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FitupProblem */

$this->title = 'Create Fitup Problem';
$this->params['breadcrumbs'][] = ['label' => 'Fitup Problems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fitup-problem-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
