<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TicketApplyGoods */

$this->title = '编辑';

?>
<div class="panel">
    <?= $this->render('_form', [ 'etitle' => $this->title,
        'goods' => $goods,
        'model' => $model,
    ]) ?>

</div>
