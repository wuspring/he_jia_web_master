<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TicketApplyGoods */
/* @var $form yii\widgets\ActiveForm */
?>



    <?php
        $form = ActiveForm::begin();

        $goodDatas = \yii\helpers\ArrayHelper::map($goods, 'id', 'goods_name');
    ?>
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
        <div style="float: right;text-align: right">
            <?= Html::submitButton('保存', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<div class="panel-body form-controls-demo">
    <div class="ticket-apply-goods-form">
    <div style="display: none">
        <?= $form->field($model, 'ticket_id')->textInput() ?>
        <?= $form->field($model, 'user_id')->textInput() ?>
        <?= $form->field($model, 'create_time')->textInput() ?>
    </div>

    <?= $form->field($model, 'good_id')->dropDownList($goodDatas, ['prompt' => '请选择'])->label('商品名称') ?>
    <?= $form->field($model, 'good_pic')->textInput()->label('商品封面') ?>
    <?= $form->field($model, 'good_price')->textInput(['maxlength' => true])->label('活动价格') ?>
    <?= $form->field($model, 'type')->dropDownList($model->getTypeDic(), ['prompt' => '请选择', 'required' => true])->label('类型') ?>
    <?= $form->field($model, 'good_amount')->textInput()->label('库存') ?>
     <?= $form->field($model, 'sort')->textInput()->label('排序') ?>
    
    <?php if (yii::$app->user->identity->assignment==\common\models\User::ASSIGNMENT_GUAN_LI_YUAN):?>
        <?= $form->field($model, 'status')->dropDownList(['0'=>'下架','1'=>'上架'])->label('状态') ?>
    <?php endif?>

    <?php ActiveForm::end(); ?>
</div>
</div>
<script src="/public/js/jquery.min.js"></script>
<script src="/public/js/base_upload.js"></script>
<script src="/public/js/upload-hooks/preview.js"></script>
<script>
    var uploadPath = '/upload/img?more=true';
    var MORE_FILES = true;
    $(function() {
        var config = [
            {category: 'ticketapplygoodsdata', id: 'good_pic', type: 'good_pic'},
        ];
        previewCtrl(config);
    });
</script>