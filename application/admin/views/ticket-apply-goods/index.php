<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\TicketApplyGoodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '参展商品';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <div class="panel-body">
        <?php echo $this->render('_search', ['model' => $searchModel, 'ticketId'=>$ticketId]); ?>
    </div>
    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'ticket_id',
            [
                'attribute' => 'user_id',
                'label' => '店铺名称',
                'value' => function ($model) {
                    return  $model->user->nickname;
                },
            ],
            [
                'attribute' => 'good_id',
                'label' => '商品名称',
                'value' => function ($model) {
                    return  $model->goods->goods_name;
                },
            ],
            [
                'attribute' => 'good_pic',
                'label' => '商品图片',
                'format' => 'raw',
                'value' => function ($model) {
                    return  sprintf("<img src='%s' width='80px;'/>", $model->good_pic);
                },
            ],
            [
                'attribute' => 'type',
                'label' => '商品类型',
                'value' => function ($model) {
                    return  $model->typeInfo;
                },
            ],

            [
                'attribute' => 'sort',
                'label' => '排序',
                'value' => function ($model) {
                    return  $model->sort;
                },
            ],
            // 'create_time',
           	[
                'attribute' => 'is_index',
                'label' => '是否置顶',
                'format' => 'raw',
                'value' => function ($model) {
                      $status = false;
                    if ($model->is_index) {
                        $status = true;
                    }
                     $html = Html::a('取消置顶', 'javascript:void(0)', ['class'=>"btn btn-info btn-xs " . ($status ? '' : 'hide'), 'data-id'=>'action', 'data-val' => $model->id]);
                        $html .= Html::a('置顶', 'javascript:void(0)', ['class'=>"btn btn-info btn-xs " . ($status ? 'hide' : ''), 'data-id'=>'action', 'data-val' => $model->id]);
                         return $html;
                },
            ],
            [
                'attribute' => 'is_zhanhui',
                'label' => '展会置顶',
                'format' => 'raw',
                'value' => function ($model) {
                    $status = false;
                    if ($model->is_zhanhui) {
                        $status = true;
                    }
                    $html = Html::a('取消置顶', 'javascript:void(0)', ['class'=>"btn btn-info btn-xs " . ($status ? '' : 'hide'), 'data-id'=>'action1', 'data-val' => $model->id]);
                    $html .= Html::a('置顶', 'javascript:void(0)', ['class'=>"btn btn-info btn-xs " . ($status ? 'hide' : ''), 'data-id'=>'action1', 'data-val' => $model->id]);
                    return $html;
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '操作',
                'template' => '{setting}',
                'buttons'=>[
                    'setting'=>function($url, $model, $key){
                        $status = false;
                        if ($model->is_index) {
                            $status = true;
                        }
                        $url=\yii\helpers\Url::toRoute(['ticket-apply-goods/update','id'=>$model->id]);
                       // $html = Html::a('取消置顶', 'javascript:void(0)', ['class'=>"btn btn-info btn-xs " . ($status ? '' : 'hide'), 'data-id'=>'action', 'data-val' => $model->id]);
                       // $html .= Html::a('置顶', 'javascript:void(0)', ['class'=>"btn btn-info btn-xs " . ($status ? 'hide' : ''), 'data-id'=>'action', 'data-val' => $model->id]);
                        $html = ' '. Html::a('编辑', $url, ['class'=>"btn btn-info btn-xs " ,]);
                        return $html;
                    },
                ]
            ],
        ],
    ]); ?>
    </div>
</div>
<script>
    $('a[data-id="action"]').click(function () {
        var that = $(this),
            id = that.attr('data-val');
        $.get('<?= \DL\service\UrlService::build("ticket-apply-goods/set-index"); ?>', {id : id}, function (res) {
            res = JSON.parse(res);

            if (res.status) {
                that.addClass('hide').siblings().removeClass('hide');
                return false;
            }

            alert(res.msg);
        })
    })
    $('a[data-id="action1"]').click(function () {
        var that = $(this),
            id = that.attr('data-val');
        $.get('<?= \DL\service\UrlService::build("ticket-apply-goods/set-zhanhui"); ?>', {id : id}, function (res) {
            res = JSON.parse(res);

            if (res.status) {
                that.addClass('hide').siblings().removeClass('hide');
                return false;
            }

            alert(res.msg);
        })
    })
</script>
