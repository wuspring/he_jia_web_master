<?php

use yii\helpers\Html;
use DL\service\UrlService;
/* @var $this yii\web\View */
/* @var $model common\models\Goods */

$this->title =($defaultTitle==0)?'编辑商品':'添加商品';
$this->params['breadcrumbs'][] = ['label' => '商品管理', 'url' => 'javascript:void(0)'];
$this->params['breadcrumbs'][] = ['label' => '商品列表', 'url' => ['goods/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>
<div class="panel">

    <?php if (yii::$app->user->identity->assignment==\common\models\User::ASSIGNMENT_GUAN_LI_YUAN):?>
    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
        'ncity' => $ncity,
        'activitygoods'=>$activitygoods,
        'active' => $active
    ]) ?>
    <?php else:?>
        <?= $this->render('_form', [ 'etitle' => $this->title,
            'model' => $model,
            'active' => $active
        ]) ?>
    <?php endif;?>
</div>