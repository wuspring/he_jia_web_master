<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Goods */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="goods-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'goods_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goods_jingle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gc_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'use_attr')->textInput() ?>

    <?= $form->field($model, 'brand_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goods_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goods_promotion_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goods_promotion_type')->textInput() ?>

    <?= $form->field($model, 'goods_marketprice')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goods_serial')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goods_storage_alarm')->textInput() ?>

    <?= $form->field($model, 'goods_click')->textInput() ?>

    <?= $form->field($model, 'goods_salenum')->textInput() ?>

    <?= $form->field($model, 'goods_collect')->textInput() ?>

    <?= $form->field($model, 'attr')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'goods_spec')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'goods_body')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'mobile_body')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'goods_storage')->textInput() ?>

    <?= $form->field($model, 'goods_image')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'goods_state')->textInput() ?>

    <?= $form->field($model, 'goods_addtime')->textInput() ?>

    <?= $form->field($model, 'goods_edittime')->textInput() ?>

    <?= $form->field($model, 'goods_vat')->textInput() ?>

    <?= $form->field($model, 'goods_commend')->textInput() ?>

    <?= $form->field($model, 'evaluation_good_star')->textInput() ?>

    <?= $form->field($model, 'evaluation_count')->textInput() ?>

    <?= $form->field($model, 'is_virtual')->textInput() ?>

    <?= $form->field($model, 'is_appoint')->textInput() ?>

    <?= $form->field($model, 'is_presell')->textInput() ?>

    <?= $form->field($model, 'have_gift')->textInput() ?>

    <?= $form->field($model, 'isdelete')->textInput() ?>

    <?= $form->field($model, 'type')->textInput() ?>

    <?= $form->field($model, 'integral')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
