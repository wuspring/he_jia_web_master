<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use wap\models\GoodsClass;

/* @var $this yii\web\View */
/* @var $model common\models\Goods */
/* @var $form yii\widgets\ActiveForm */

?>

<script src="<?php echo yii::$app->request->hostInfo; ?>/public/ueditor/ueditor.config.js"></script>
<script src="<?php echo yii::$app->request->hostInfo; ?>/public/ueditor/ueditor.all.min.js"></script>
<script src="<?php echo yii::$app->request->hostInfo; ?>/public/ueditor/lang/zh-cn/zh-cn.js"></script>


<style>
    .inline .radio, .inline .checkbox {
        display: inline-block;
        margin: 0 5px;
    }

     #goods-goods_body, #goods-goods_jingle {
        margin-top: 20px;
        padding: 0;
        margin: 20px 0;
        width: 100%;
        height: auto;
        border: none;
    }

    <?php if ($model->goods_promotion_type==0):?>
    .field-goods-goods_promotion_price {
        display: none;
    }

    <?php endif?>

    .attr-group {
        border: 1px solid #eee;
        padding: .5rem .75rem;
        margin-bottom: .5rem;
        border-radius: .15rem;
    }

    .attr-group-delete {
        display: inline-block;
        background: #eee;
        color: #fff;
        width: 1rem;
        height: 1rem;
        text-align: center;
        line-height: 1rem;
        border-radius: 999px;
    }

    .attr-group-delete:hover {
        background: #ff4544;
        color: #fff;
        text-decoration: none;
    }

    .attr-list > div {
        vertical-align: top;
    }

    .attr-item {
        display: inline-block;
        background: #eee;
        margin-right: 1rem;
        margin-top: .5rem;
        overflow: hidden;
    }

    .attr-item .attr-name {
        padding: .15rem .75rem;
        display: inline-block;
    }

    .attr-item .attr-delete {
        padding: .35rem .75rem;
        background: #d4cece;
        color: #fff;
        font-size: 1rem;
        font-weight: bold;
    }

    .attr-item .attr-delete:hover {
        text-decoration: none;
        color: #fff;
        background: #ff4544;
    }

    .btn {
        line-height: inherit !important;
        border: 1px solid #d5d5d5 !important;
    }

    /*表格样式*/
    table#process {
        font-size: 11px;
        color: #333333;
        border-width: 1px;
        border-color: #666666;
        border-collapse: collapse;
    }

    table#process th {
        border-width: 1px;
        padding: 8px;
        border-style: solid;
        border-color: #666666;
        background-color: #dedede;
    }

    table#process td {
        border-width: 1px;
        padding: 8px;
        border-style: solid;
        border-color: #666666;
        background-color: #ffffff;
    }

    .title-fixed{position: fixed;top: 20px;right: 20px;}
</style>
<?php $form = ActiveForm::begin([
    'id' => 'goodsfrom',
    'options' => [
        'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
        'inputOptions' => ['class' => 'form-control'],
        'labelOptions' => ['class' => 'col-sm-2 control-label '],
        'template' => "{label}<div class='col-sm-8'>{input}{hint}{error}</div>",],
]);
$categorys = \common\models\Category::find()->where(['>', 'id', 22])->all();
$categorys = \yii\helpers\ArrayHelper::map($categorys, 'id', 'title');

empty($model->second_limit) and $model->second_limit = 0;


function hasGcs ($gc = [0],$list=array()) {
    is_array($gc) or $gc = json_decode($gc, true);

    if ($gc === false) {
        return [];
    }
    $query = GoodsClass::find();
    $gc and $query->andFilterWhere(['in', 'fid', $gc]);
    $calsslist = $query->orderBy(" sort desc")->all();

    if(!empty($calsslist)){
        foreach ($calsslist as $key=>$item){
            $count= GoodsClass::find()->where(['fid'=>$item->id])->count();
            if ($count>0){
                $list[$item->name]= hasGcs([$item->id]);
            }else{
                $list[$item->id]='|-'.$item->name;
            }
        }
    }
    return $list;
}

$nowUser = Yii::$app->user->identity;
$shopUserId = Yii::$app->request->get('shopid', 0);
$shopUserId or $shopUserId = $nowUser->id;

$useStore = \wap\models\User::findOne($shopUserId);
$useStore or stopFlow("未找到有效店铺");

$userGc = json_decode($useStore->gc_ids, true);

$userGc or $userGc = [0];
$selectClassList = hasGcs($userGc);
?>

<script>
    var checkStore = function (storeId) {

        $.get('<?= \DL\service\UrlService::build('store-info/gcs'); ?>', {storeId : storeId}, function (res) {
            if (res.status) {
                var ds = res.data;

                $('#goods-gc_id option').each(function () {
                    var val = $(this).attr('value');
                    // console.log(val);
                    if (ds.indexOf(val) > -1) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                })
            }
        }, 'json');
    }
</script>
<div class="panel-heading">
    <span class="panel-title"><?= Html::encode($this->title) ?></span>
    <div class="form-group" style="float:right;text-align: right;" data-fixed>
        <?= Html::submitButton($model->isNewRecord ? '添加' : '保存', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <a href="<?php echo Url::toRoute(['goods/index']); ?>" class="btn btn-w-m btn-default">取消</a>
    </div>
</div>
<ul class="nav nav-tabs dl_tab">
    <li class="col-lg-2 tab_title <?=('goods'==$active)?'active':''?>"><a  href="<?=\DL\service\UrlService::build(['goods/update','id'=>$model->id])?>">商品</a></li>
    <li class="col-lg-2 tab_title <?=('coupon'==$active)?'active':''?>"><a  href="<?=\DL\service\UrlService::build(['goods/coupon-list','goodId'=>$model->id])?>">优惠劵</a></li>
</ul>
<div class="panel-body form-controls-demo">

    <?= $form->field($model, 'goods_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'keywords')->textarea(['rows' => 6])->label('关键字') ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6])->label('描述') ?>
    
    <?= $form->field($model, 'mobile_body')->textInput(['rows' => 6])->label('商品简介') ?>

 
    <?php $form->field($model, 'brand_id')->dropDownList($model->getBrand(), ['prompt' => '请选择商品品牌'])->label('品牌选择') ?>
    <?php if (yii::$app->user->identity->assignment==\common\models\User::ASSIGNMENT_GUAN_LI_YUAN):?>
        <?php
        $st = ['maxlength' => true];
        if ($model->user_id) {
            $st['disabled'] = true;
        }
        echo $form->field($model, 'user_id')->textInput($st)->label('所属店铺') ?>

        <script type="text/javascript">
            var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
            init.push(function () {
                $("#goods-user_id").select2({
                    placeholder:"输入店铺名称",//文本框的提示信息
                    minimumInputLength:1,   //至少输入n个字符，才去加载数据
                    allowClear: true,  //是否允许用户清除文本信息
                    ajax:{
                        url:'<?php echo Url::toRoute(['user/getlist'])?>',   //地址
                        dataType:'text',    //接收的数据类型
                        //contentType:'application/json',
                        data: function (term, pageNo) {     //在查询时向服务器端传输的数据
                            term = $.trim(term);
                            return {
                                _csrf:_csrf ,
                                val: term,    //联动查询的字符
                                //pageSize: 15,    //一次性加载的数据条数
                                //pageNo:pageNo,    //页码
                                //time:new Date()//测试
                            }
                        },
                        results:function(data,pageNo){

                            if(data.length>0){   //如果没有查询到数据，将会返回空串
                                var dataObj =eval("("+data+")");  //将接收到的JSON格式的字符串转换成JSON数据
                                var more = (pageNo*15)<dataObj.total; //用来判断是否还有更多数据可以加载
                                return {
                                    results:dataObj.result,more:more
                                };
                            }else{
                                return {results:data};
                            }
                        }
                    },
                    initSelection:function(element,callback){           //初始化，其中doName是自定义的一个属性，用来存放text的值
                        var id=$(element).val();

                        var text="<?php echo  $model->isNewRecord ? '':$model->user->nickname;?>"

                        //var text=$(element).attr("doName");
                        if(id!=''&&text!=""){
                            callback({id:id,text:text});
                        }

                    },
                    formatResult: formatAsText //渲染查询结果项
                });
            });

            $("#goods-user_id").on("select2-selecting", function(e) {
                checkStore(e.object.id);
            });
            //格式化查询结果,将查询回来的id跟name放在两个div里并同行显示，后一个div靠右浮动
            function formatAsText(item){
                var itemFmt = "<div style='display:inline;'>" + item.text + "</div>";//<div style='float:right;color:#4F4F4F;display:inline'>"+item.name+"</div>
                return itemFmt;
            }
        </script>
    <?php endif;?>

    <?= $form->field($model, 'gc_id')->dropDownList($selectClassList, ['prompt' => '请选择商品分类'])->label('商品分类') ?>

    <?php $form->field($model, 'egc_id')->dropDownList($categorys, ['prompt' => '默认栏目']) ?>

    <?php $form->field($model, 'goods_promotion_type')->hiddenInput(['value'=>0]) ?>

    <?= $form->field($model, 'type', ['options'=>['style'=>'display:none']])->hiddenInput(['value' => '1']);?>

    <?= $form->field($model, 'goods_marketprice')->textInput(['maxlength' => true, 'type' => 'double'])->label('商品原价'); ?>
    <?= $form->field($model, 'goods_price')->textInput(['maxlength' => true, 'type' => 'double']); ?>
    <?= $form->field($model, 'unit')->textInput(['maxlength' => true,'placeholder'=>'例如:套，件，户'])->label('商品单位'); ?>

    <?php $form->field($model, 'integral')->textInput(['maxlength' => true, 'type' => 'number']) ?>
    <?php

        $data = \DL\Project\SecondKill::init()->limbos();
        $secondGroups = [];

        foreach ($data AS $key => $dt) {
            $secondGroups[] = "{$dt[0]}~{$dt[1]}";
        }

        empty($model->is_second_kill) and $model->is_second_kill = 0;
    ?>
    <!-- 秒杀设置 -->
    <?php $form->field($model, 'is_second_kill')->radioList(['0' => '关闭', '1' => '开启'])->label('秒杀商品') ?>
    <?php $form->field($model, 'second_price')->textInput(['type' => 'double'])->label('秒杀价格') ?>
    <?php $form->field($model, 'second_kill_groups')->checkboxList($secondGroups)->label('秒杀时段') ?>
    <?php  $form->field($model, 'second_limit')->textInput()->label('秒杀时段数量') ?>


    <?= $form->field($model, 'goods_promotion_price')->textInput(['maxlength' => true, 'type' => 'double']) ?>

    <?= $form->field($model, 'goods_pic')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => false,
            ],
            'server' => Url::toRoute(['goods/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ])->label("商品主图"); ?>

    <?= $form->field($model, 'goods_image')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => true,
            ],
            'server' => Url::toRoute(['goods/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ])->label("商品相册"); ?>

    <?= $form->field($model, 'goods_storage')->textInput(['type' => 'number']) ?>

<!--    --><?php //$form->field($model, 'goods_weight')->textInput(['maxlength' => true, 'type' => 'number']) ?>

    <?php $form->field($model, 'goods_storage_alarm')->textInput() ?>
    <?php //$form->field($model, 'goods_serial')->textInput(['maxlength' => true]) ?>
    <?php $form->field($model, 'third_code')->textInput(['maxlength' => true])->label('第三方商品编号') ?>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="goods-goods_storage_alarm"></label>
        <div class="col-sm-7">
            <?php $form->field($model, 'use_attr')->checkbox([], false) ?>

        </div>
    </div>

    <div class="form-group" id="sku" style="<?php echo $model->use_attr ? '' : 'display: none;'; ?>">
        <label class="col-sm-2 control-label" for="goods-goods_storage_alarm">规格组</label>
        <div class="col-sm-8">
            <div class="input-group">
                <span class="input-group-addon">规格组</span>
                <input type="text" class="form-control" id="attrgroupname" placeholder="如颜色、尺码、套餐">
                <span class="input-group-btn">
                        <button class="btn" type="button" id="skugroup">添加</button>
                    </span>
            </div>
            <div class="panel-body" id="group">


            </div>
            <div class="panel-body" id="guigezu">

            </div>

        </div>
    </div>

    <div style="display: none">
        <?= $form->field($model, 'attr')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'goods_spec')->textarea(['rows' => 6]) ?>
    </div>

    <script>
        var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
        var attrgroup = [];
        var goodssku = [];
        init.push(function () {
            $(function () {
                var attr = $("#goods-attr").val();
                var group = $("#goods-goods_spec").val();
                attrgroup = group.length ? JSON.parse(group) : [];
                goodssku = attr.length ? JSON.parse(attr) : [];

                $(goodssku).each(function (key, item) {
                    goodssku[key].attr = JSON.parse(item.attr.toString());
                });

                <?php if($model->use_attr == 1):?>
                $(attrgroup).each(function (key, val) {
                    $("#group").append(addgrouplist(val, key))
                });

                caratTable(attrgroup);
                <?php endif?>
            });



            $("#skugroup").click(function () {
                var index = $("#group .attr-group").length;
                // alert(index);


                if (attrgroup.length == 3 || attrgroup.length > 3) {
                    alert("最多添加3个规格组")
                    return;
                }

                var attrgroupname = $("#attrgroupname").val();
                if (attrgroupname.length == 0) {
                    return;
                }
                var isgroup = false;
                $(attrgroup).each(function (key, val) {
                    if (attrgroupname == val.group_name) {
                        isgroup = true;
                        return;
                    }
                });
                if (isgroup) {
                    alert("该规格组已添加")
                    return;
                }

                $.post('<?php echo Url::toRoute(['goods/attr-group-add']);?>', {
                    _csrf: _csrf,
                    name: attrgroupname
                }, function (res) {
                    if (res.state == 1) {
                        attrgroup.push(res.model);
                        $("#group").append(addgroup(res.model, index));

                    } else {
                        alert(res.msg);
                    }
                }, 'json');
            });

            $(document).on('click', ".attr-group-delete", function () {

                var deletebut = this;
                $(attrgroup).each(function (key, val) {

                    var id = $(deletebut).attr('data-id');
                    if (id == val.id) {
                        attrgroup.splice(key, 1);

                        $(deletebut).parent().parent().remove();
                    }
                })
            });

            $(document).on('click', ".add-attr-btn", function () {
                var groupid = $(this).attr('data-id');
                var addbut = this;
                var name = $(this).parent().parent().find('.add-attr-input').val();
                var isattr = false;
                $(attrgroup).each(function (key, val) {
                    if (groupid == val.id) {
                        $(val.attr).each(function (k, item) {
                            if (name == item.attr_name) {
                                isattr = true;
                                return;
                            }
                        });
                    }
                });
                if (isattr) {
                    alert("该规格已存在");
                    return;
                }

                $.post('<?php echo Url::toRoute(['goods/attr-add']);?>', {
                    _csrf: _csrf,
                    groupid: groupid,
                    name: name
                }, function (res) {
                    if (res.state == 1) {
                        $(attrgroup).each(function (key, val) {

                            if (groupid == val.id) {
                                attrgroup[key].attr.push(res.model);
                                $(addbut).parent().parent().parent().before(addAttr(res.model));

                                caratTable(attrgroup);
                            }
                        });

                    } else {
                        alert(res.msg);
                    }
                }, 'json');


            });
            $(document).on('click', '.attr-delete', function () {
                var groupid = $(this).attr('group-id');
                var attrid = $(this).attr("data-id");
                var deletebut = this;

                $(attrgroup).each(function (key, val) {
                    if (groupid == val.id) {
                        $(val.attr).each(function (k, item) {
                            if (attrid == item.id) {
                                attrgroup[key].attr.splice(k, 1);
                                var skuddd = [];
                                $(goodssku).each(function (i, sku) {
                                    var isonsku = true;
                                    $(sku.attr).each(function (v, attr) {
                                        if (attr == attrid) {
                                            isonsku = false;
                                        }
                                    });
                                    if (isonsku) {
                                        skuddd.push(sku);
                                    }
                                });
                                goodssku = skuddd;
                                $(deletebut).parent().remove();
                                caratTable(attrgroup);
                                console.log(goodssku)
                            }
                        });


                    }
                })
            });

            $("#guigezu").on("change", "input[name='sku_price']", function () {
                var price = $(this).val();
                var attr = $(this).attr("data-attr");
                $(goodssku).each(function (key, item) {

                    if (JSON.stringify(item.attr) == attr) {
                        goodssku[key].price = price;
                    }
                });
                console.log(goodssku);
            });
            $("#guigezu").on("change", "input[name='sku_num']", function () {
                var sku_num = $(this).val();
                var attr = $(this).attr("data-attr");
                $(goodssku).each(function (key, item) {
                    if (JSON.stringify(item.attr) == attr) {
                        goodssku[key].num = sku_num;
                    }
                });
            });
            $("#guigezu").on("change", "input[name='sku_code']", function () {
                var sku_code = $(this).val();
                var attr = $(this).attr("data-attr");
                $(goodssku).each(function (key, item) {
                    if (JSON.stringify(item.attr) == attr) {
                        goodssku[key].goods_code = sku_code;
                    }
                });
            });
            $("#guigezu").on("change", "input[name='sku_picimg']", function () {
                var sku_picimg = $(this).val();
                var attr = $(this).attr("data-attr");
                $(goodssku).each(function (key, item) {
                    if (JSON.stringify(item.attr) == attr) {
                        goodssku[key].picimg = sku_picimg;
                    }
                });
            });
            $("#goodsfrom").submit(function () {
                var isgroup = false;
                $(attrgroup).each(function (key, item) {
                    if (item.attr.length == 0) {
                        isgroup = true;
                    }
                });
                if (isgroup) {
                    alert("请在空的规格组中添加规格值")
                    return false;
                }

                $("#goods-goods_spec").val(JSON.stringify(attrgroup));
                $("#goods-attr").val(JSON.stringify(goodssku));

                return true;
            });

            $(document).on('click', '.upload-attr-pic', function () {
                var file = $(this).parent().parent().find("input[type='file']");
                $(file).click();
                var skuimg = this;
                $(file).change(function () {
                    var formData = new FormData();
                    formData.append('_csrf', _csrf);
                    formData.append("file", this.files[0]);
                    $.ajax({
                        type: "POST", // 数据提交类型
                        url: "<?php echo Url::toRoute(['goods/upload']);?>", // 发送地址
                        data: formData, //发送数据
                        async: true, // 是否异步
                        dataType: 'json',
                        processData: false, //processData 默认为false，当设置为true的时候,jquery ajax 提交的时候不会序列化 data，而是直接使用data
                        contentType: false, //
                        success: function (re) {
                            if (re.code == 0) {
                                var skuimgpic = $(skuimg).parent().parent().find("input[name='sku_picimg']");
                                skuimgpic.val(re.attachment);
                                var sku_picimg = $(skuimgpic).val();

                                var attr = $(skuimgpic).attr("data-attr");
                                $(goodssku).each(function (key, item) {
                                    if (JSON.stringify(item.attr) == attr) {
                                        goodssku[key].picimg = sku_picimg;
                                    }
                                });
                            }
                        },
                        error: function () {
                            alert("上传失败")

                        },

                    });
                });
            });

            $(document).on('click', '.delete-attr-pic', function () {
                var skuimgpic = $(this).parent().parent().find("input[name='sku_picimg']");
                skuimgpic.val('');

                var attr = $(skuimgpic).attr("data-attr");
                $(goodssku).each(function (key, item) {
                    if (JSON.stringify(item.attr) == attr) {
                        goodssku[key].picimg = '';
                    }
                });
            });
        });

        $(function () {
            var sVal = $('#goods-user_id');
            if (sVal) {
                var iv = sVal.val();
                if (iv != '') {
                    checkStore(iv);
                }
            }
        })

    </script>
    <script src="<?php echo yii::$app->request->hostInfo; ?>/public/js/setsuk.js"></script>

    <?= $form->field($model, 'goods_jingle')->textarea(['rows' => 3])->label('商品参数') ?>

    <?= $form->field($model, 'goods_body')->textarea(['rows' => 6]) ?>
    <?php  $form->field($model, 'goods_commend')->radioList([
        '1' => '是',
        '0' => '否',
    ])->label('是否推荐') ?>
    <?php  $form->field($model, 'goods_hot')->radioList([
        '1' => '是',
        '0' => '否',
    ])->label('是否热销') ?>
    <?php $form->field($model, 'is_virtual')->radioList(['0' => '否', '1' => '是'])->label('是否为虚拟商品') ?>
    <?php $form->field($model, 'is_appoint')->radioList(['0' => '否', '1' => '是'])->label('是否是预约商品') ?>
    <?php $form->field($model, 'is_presell')->radioList(['0' => '否', '1' => '是'])->label('是否热销') ?>
    <?= $form->field($model, 'goods_state')->radioList(['0' => '下架', '1' => '在售'])->label('商品状态') ?>
    <?php $storeUser = Yii::$app->user->identity; if ($storeUser->assignment == \common\models\User::ASSIGNMENT_GUAN_LI_YUAN) :?>
    <?= $form->field($model, 'admin_score')->textInput()->label('管理员打分') ?>
    <?php endif; ?>
    <?php $form->field($model, 'goods_vat')->radioList(['0' => '否', '1' => '是'])->label('是否开具增值税发票') ?>
    <?php if (yii::$app->user->identity->assignment==\common\models\User::ASSIGNMENT_GUAN_LI_YUAN):?>
    <div class="form-group field-goods-goods_state">
        <label class="col-sm-2 control-label " for="goods-goods_state">是否同步展会</label>
        <div class="col-sm-8">
            <div id="isactivity">
                <label><input type="radio" name="isactivity" value="0">不同步展会</label>
                <label><input type="radio" name="isactivity" value="1"  checked="checked">同步展会</label>
            </div>
            <div class="help-block"></div>
        </div>
    </div>
    <div id="activity" style="" class="panel ">
        <?= $form->field($activitygoods, 'ticket_id')->dropDownList($activitygoods->getStartTicketlist($ncity->id), ['prompt' => '请选择展会'])->label('请选择展会') ?>

        <?= $form->field($activitygoods, 'good_pic')->widget('manks\FileInput', [
            'clientOptions' => [
                'pick' => [
                    'multiple' => false,
                ],
                'server' => Url::toRoute(['goods/upload']),
            ],
        ])->label("活动主图"); ?>

        <?= $form->field($activitygoods, 'good_price')->textInput(['maxlength' => true])->label('活动价格') ?>
        <?= $form->field($activitygoods, 'type')->dropDownList($activitygoods->getTypeDic(), ['prompt' => '请选择', 'required' => true])->label('类型') ?>
        <?= $form->field($activitygoods, 'good_amount')->textInput()->label('库存') ?>
        <?= $form->field($activitygoods, 'status')->dropDownList(['0'=>'下架','1'=>'上架'])->label('状态') ?>
    </div>

    <script>
        init.push(function () {
            $("#isactivity :radio").click(function () {
               var m=$(this).val();
               if (m==0){
                   $("#activity").hide()
               }else{
                   $("#activity").show()
               }
            });

        });
    </script>
    <?php endif?>




    <?php ActiveForm::end(); ?>
</div>

<script type="text/javascript">
    // $(function() {
        var setIntegralLabel = function(val) {
            var text;
            val = parseInt(val);
            console.log(val);
            switch (val) {
                case 1 :
                    text = '赠送积分';
                    break;
                case 2 :
                    text = '积分价格';
                    break;
            }
            $('label[for="goods-integral"]').text(text);
        };
       $('input[name="Goods[type]"]').click(function () {
           var val = $('input[name="Goods[type]"]:checked').val();
           setIntegralLabel(val);
       });
        $('input[name="Goods[type]"]:checked').click();
    // });
    init.push(function () {
        $('#goods-use_attr').switcher({on_state_content: '开启', off_state_content: '关闭'});

        var ue = UE.getEditor('goods-goods_body', {
            'zIndex': 9,
        });
        var ue = UE.getEditor('goods-goods_jingle', {
            'zIndex': 9,
            //serverUrl:'<?php //echo Url::toRoute([])?>//'
            toolbars: [
                [
                  //  'anchor', //锚点
                  //  'undo', //撤销
                   // 'redo', //重做
                    'bold', //加粗
                   // 'indent', //首行缩进
                    //'snapscreen', //截图
                   // 'italic', //斜体
                  //  'underline', //下划线
                  //  'strikethrough', //删除线
                   // 'subscript', //下标
                   // 'fontborder', //字符边框
                  //  'superscript', //上标
                  //  'formatmatch', //格式刷
                  //  'source', //源代码
                  //  'blockquote', //引用
                   // 'pasteplain', //纯文本粘贴模式
                   // 'selectall', //全选
                  //  'preview', //预览
                  //  'horizontal', //分隔线
                  //  'removeformat', //清除格式
                  //  'time', //时间
                  //  'date', //日期
                  //  'unlink', //取消链接
                  //  'inserttitle', //插入标题
                   // 'cleardoc', //清空文档
                    // 'insertparagraphbeforetable', //"表格前插入行"
                    // 'insertcode', //代码语言
                  //  'fontfamily', //字体
                  //  'fontsize', //字号
                   // 'paragraph', //段落格式
                   // 'simpleupload', //单图上传
                   // 'insertimage', //多图上传
                   // 'link', //超链接
                    // 'emotion', //表情
                   // 'spechars', //特殊字符
                   // 'searchreplace', //查询替换
                   // 'map', //Baidu地图
                    //'justifyleft', //居左对齐
                   // 'justifyright', //居右对齐
                  //  'justifycenter', //居中对齐
                    //'justifyjustify', //两端对齐
                   // 'forecolor', //字体颜色
                    //'backcolor', //背景色
                   // 'insertorderedlist', //有序列表
                   // 'insertunorderedlist', //无序列表
                   // 'directionalityltr', //从左向右输入
                   // 'directionalityrtl', //从右向左输入
                   // 'rowspacingtop', //段前距
                   // 'rowspacingbottom', //段后距
                   // 'pagebreak', //分页
                  //  'imagenone', //默认
                   // 'imageleft', //左浮动
                  //  'imageright', //右浮动
                  //  'attachment', //附件
                   // 'imagecenter', //居中
                   // 'wordimage', //图片转存
                   // 'lineheight', //行间距
                  //  'edittip ', //编辑提示
                  //  'customstyle', //自定义标题
                   // 'autotypeset', //自动排版
                    //'touppercase', //字母大写
                   // 'tolowercase', //字母小写
                   // 'background', //背景
                    // 'template', //模板
                    // 'scrawl', //涂鸦
                   // 'drafts', // 从草稿箱加载

                ]
            ],
            autoHeightEnabled: true,

            height: 400,
        });


        $("#goods-goods_promotion_type input[type=radio]").change(function () {
            var val = $(this).val();

            if (val == 1) {
                $(".field-goods-goods_promotion_price").show();
            } else {
                $(".field-goods-goods_promotion_price").hide();
            }
        })
        $("#goods-use_attr").click(function () {
            var isto = $(this).is(':checked');
            if (isto) {
                $("#sku").show();
                $(this).val(1);
                $("#goods-goods_serial").attr("disabled", "disabled");
                $("#goods-goods_storage").attr("disabled", "disabled");
            } else {
                $("#sku").hide();
                $(this).val(0);
                $("#goods-goods_serial").removeAttr("disabled");
                $("#goods-goods_storage").removeAttr("disabled");
            }
        });

        $(window).scroll(function() {
            //为了保证兼容性，这里取两个值，哪个有值取哪一个
            //scrollTop就是触发滚轮事件时滚轮的高度
            var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
            if(scrollTop> 100){
                $('[data-fixed]').addClass('title-fixed');
            }else{
                $('[data-fixed]').removeClass('title-fixed');
            }
        })
    });


</script>