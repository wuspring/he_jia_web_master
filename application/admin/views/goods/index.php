<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\search\GoodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '商品列表';
$this->params['breadcrumbs'][] = ['label' => '商品管理', 'url' => 'javascript:void(0)'];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #form .col-md-12,.col-md-1,.col-md-3{padding-left: 0px}
    .a_margin{margin-left: 10px}
</style>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?>
            <span style="float: right;text-align: right;">
    	<?php if($shopid>0):?>
            <?= Html::a('发布商品', ['create','shopid'=>$shopid], ['class' => 'btn btn-success']) ?>
        <?php else:?>
            <?= Html::a('发布商品', ['create'], ['class' => 'btn btn-success']) ?>
        <?php endif;?>

    </span>



        </span>
    </div>



    <?php  echo $this->render('_search', ['model' => $searchModel,'assignment'=>$assignment]); ?>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                array(
                    'format' => 'raw',
                    'attribute' => 'goods_image',
                    'label'=>'缩略图',
                    'value'=>function($filterModel){
                        $itm=$filterModel->goods_pic;
                        if ($itm){
                            return Html::img(yii::$app->request->hostInfo.$itm, ['width' =>30]);
                        }else{
                            return '无';
                        }

                    },
                ),
                array(
                    'attribute' => 'goods_name',
                    'label'=>'商品名称',
                    'value'=>function($filterModel){
                        return StringHelper::truncate($filterModel->goods_name, 20);

                    },
                ),
                array(
                    'attribute' => 'id',
                    'label'=>'商品ID',
                ),
                array(
                    'attribute' => 'user_id',
                    'label'=>'店铺名称',
                    'value'=>function($filterModel){
                        return $filterModel->user->nickname;

                    },
                ),
                array(
                    'attribute' => 'gc_id',
                    'label'=>'商品分类',
                    'value'=>function($filterModel){
                        return $filterModel->gclass->name;

                    },
                ),
//                array(
//                    'format' => 'raw',
//                    'attribute' => 'brand_id',
//                    'label'=>'品牌',
//                    'value'=>function($filterModel){
//                        if ($filterModel->branddata->show_type==0){
//                            return Html::img(yii::$app->request->hostInfo.$filterModel->branddata->brand_pic, ['width' =>30]);
//                        }else{
//                            return $filterModel->branddata->brand_name;
//                        }
//                    },
//                ),
                'goods_price',
//                'goods_click',
//                'goods_salenum',
                array(
                    'format' => 'raw',
                    'attribute' => 'goods_state',
                    'label'=>'状态',
                    'value'=>function($filterModel){

                        switch ($filterModel->goods_state){
                            case 10:
                                return '<span class="label label-danger">商品违规</span>';
                                break;
                            case 0:
                                return '<span class="label">已下架</span> | <button type="button" class="btn  btn-success btn-xs upgoods" data-id="'.$filterModel->id.'">上架</button>';
                                break;
                            case 1:
                                return '<span class="label label-success">已上架</span> | <button type="button" class="btn btn-default btn-xs dngoods" data-id="'.$filterModel->id.'">下架</button>';
                                break;
                        }

                    },
                ),
                array(
                    'attribute' => 'admin_score',
                    'label'=>'排序',
                ),
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>'操作',
                    'template' => '{view} {update}',
                    'buttons'=>[
                        'view'=>function($url, $model, $key){
                            $url=yii::$app->request->hostInfo.'/admin.php?r=goods/view&id='.$model->id ;
                            return  Html::a('查看', $url, ['title' => '查看','data-toggle'=>'modal','class'=>'a_margin'] ) ;
                        },
                        'update' => function ($url, $model, $key) {
                            $url = ['goods/update','id'=>$model->id,'shopid'=>$model->user_id];
                            return  Html::a('更新', $url, ['title' => '更新','data-toggle'=>'modal','class'=>'a_margin'] ) ;
                        },
                        'delete'=> function ($url, $model, $key) {
                            return  Html::a('删除', 'javascript:void(0)', ['title' => '删除','data-toggle'=>'modal', 'data-id'=>$model->id,'class'=>'a_margin'] ) ;
                        },
                    ],

                ],
            ],
        ]); ?>
    </div>

</div>
<script type="text/javascript">
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";

    $(document).on("click",'.upgoods',function () {
        var id=$(this).attr('data-id');
        var upbut=this;

        $.post("<?php echo Url::toRoute(['goods/upgoods']);?>",{_csrf:_csrf,id:id},function (res) {
            if (res.code==1){
                var html='<span class="label label-success">已上架</span> | <button type="button" class="btn btn-default btn-xs dngoods" data-id="'+id+'">下架</button>'
                $(upbut).parent().html(html);
            }else{
                alert(res.mg);
            }
        },'json');

    });

    $(document).on("click",'.dngoods',function () {
        var id=$(this).attr('data-id');
        var dnbut=this;

        $.post("<?php echo Url::toRoute(['goods/dngoods']);?>",{_csrf:_csrf,id:id},function (res) {
            if (res.code==1){
                var html='<span class="label">已下架</span> | <button type="button" class="btn  btn-success btn-xs upgoods" data-id="'+id+'">上架</button>'
                $(dnbut).parent().html(html);
            }else{
                alert(res.mg);
            }
        },'json');

    });

    $(".fa-bitbucket").click(function(){
        if (confirm("确定要删除该商品？")) {
            var del=$(this).parent('a');
            var _csrf =$("#_csrf").val();
            var id=del.attr('data-id');
            $.post("<?php echo Url::toRoute('goods/delete');?>",{_csrf:_csrf,id:id},function(data){
                alert(data.msg);
                window.location.reload();
            },'json');
        }
    });
</script>