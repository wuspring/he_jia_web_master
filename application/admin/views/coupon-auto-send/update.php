<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CouponAutoSend */

$this->title = 'Update Coupon Auto Send: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Coupon Auto Sends', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="coupon-auto-send-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
