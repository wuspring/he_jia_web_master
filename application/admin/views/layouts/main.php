<?php

use common\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use mdm\admin\components\MenuHelper;
use common\models\SelectCity;
use DL\service\UrlService;
use \common\models\Provinces;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$contextData = $this->context;
$defaultCity = isset($contextData->defaultCity) ? $contextData->defaultCity : (new Provinces());
$cityDatas = isset($contextData->cityDatas) ? $contextData->cityDatas :[];

?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
     <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <!-- 后台框架样式 -->
    <link href="<?php echo Yii::$app->request->hostInfo;?>/public/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo Yii::$app->request->hostInfo;?>/public/css/pixel-admin.css" rel="stylesheet" type="text/css">
    <link href="<?php echo Yii::$app->request->hostInfo;?>/public/css/widgets.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo Yii::$app->request->hostInfo;?>/public/css/rtl.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo Yii::$app->request->hostInfo;?>/public/css/themes.min.css" rel="stylesheet" type="text/css">
    <?php $this->head() ?>
    <!--[if lt IE 9]>
        <script src="assets/javascripts/ie.min.js"></script>
    <![endif]-->

    <!-- Get jQuery from Google CDN -->
    <!--[if !IE]> -->
    <script type="text/javascript"> window.jQuery || document.write('<script src="<?php echo Yii::$app->request->hostInfo;?>/public/js/jquery-2.1.1.min.js">'+"<"+"/script>"); </script>
    <!-- <![endif]-->
    <!--[if lte IE 9]>
    <script type="text/javascript"> window.jQuery || document.write('<script src="<?php echo Yii::$app->request->hostInfo;?>/public/js/jquery-2.1.1.min.js">'+"<"+"/script>"); </script>
    <![endif]-->
    <style type="text/css">
        #main-navbar .navbar-brand div img{
            width: auto;
            height: auto;
            padding-bottom: 5px;
        }
        .dl-mbx {
            padding: 0;
            margin: 0;
        }

        #w0 > .form-group {
            margin-right: 40px;
        }
    </style>
</head>


<!-- 1. $BODY ======================================================================================
    
    Body

    Classes:
    * 'theme-{THEME NAME}'
    * 'right-to-left'      - Sets text direction to right-to-left
    * 'main-menu-right'    - Places the main menu on the right side
    * 'no-main-menu'       - Hides the main menu
    * 'main-navbar-fixed'  - Fixes the main navigation
    * 'main-menu-fixed'    - Fixes the main menu
    * 'main-menu-animated' - Animate main menu
-->
<body class="theme-default main-menu-animated">
 <?php $this->beginBody() ?>
<script>var init = [];</script>

<div id="main-wrapper">


<!-- 2. $MAIN_NAVIGATION ===========================================================================

    Main navigation
-->
    <div id="main-navbar" class="navbar navbar-inverse" role="navigation">
        <!-- Main menu toggle -->
        <button type="button" id="main-menu-toggle"><i class="navbar-icon fa fa-bars icon"></i><span class="hide-menu-text">菜单</span></button>
        
        <div class="navbar-inner">
            <!-- Main navbar header -->
            <div class="navbar-header">

                <!-- Logo -->
                <a href="javascript:void(0);" class="navbar-brand">
                    <div style="background: #fff"><img alt="网站后台管理" src="<?php echo Yii::$app->request->hostInfo;?>/public/images/adminlogo.png"></div>

                </a>

                <!-- Main navbar toggle -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar-collapse"><i class="navbar-icon fa fa-bars"></i></button>

            </div> <!-- / .navbar-header -->

            <div id="main-navbar-collapse" class="collapse navbar-collapse main-navbar-collapse">
                <div>
                    <ul class="nav navbar-nav">
<!--                        <li>-->
<!--                            <a href="--><?php //echo Yii::$app->request->hostInfo;?><!--/admin.php">后端首页</a>-->
<!--                        </li>-->
                       <li class="dropdown">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">当前区域：
                                <b><?= $defaultCity->cname; ?></b></a>
                           <?php
                                $user = Yii::$app->user;
                                if ($user) :
                                    $userInfo = $user->identity;
                                    if ($userInfo) :
                                        if($userInfo->assignment == $userInfo::ASSIGNMENT_GUAN_LI_YUAN and (int)$userInfo->change_city) :
                           ?>
                            <ul class="dropdown-menu">
                                <?php foreach ($cityDatas AS $cityId => $cname) :?>
                                <li><a href="<?= UrlService::build(['set-city/index', 'cityId' => $cityId]); ?>"><?= $cname; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                                <?php endif;endif;endif; ?>
                        </li>
                    </ul> <!-- / .navbar-nav -->

                    <div class="right clearfix">
                        <ul class="nav navbar-nav pull-right right-navbar-nav">
                            <?php if ($userInfo and $userInfo->assignment == $userInfo::ASSIGNMENT_GUAN_LI_YUAN) : ?>
                            <li class="dropdown">
                                <a href="#" data-id="refreshSystemCache" data-href="<?= UrlService::build('expand/refresh'); ?>" class="dropdown-toggle user-menu" data-toggle="dropdown">
                                    清除缓存
                                </a>
                            </li>
                            <?php endif; ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle user-menu" data-toggle="dropdown">
                                    <img src="<?php echo yii::$app->user->identity ? yii::$app->user->identity->avatarTm : '/public/images/user_admin.jpg';?>">
                                    <span><?php echo yii::$app->user->identity ? yii::$app->user->identity->username : '';?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo Url::toRoute(['user/myview']);?>">帐号管理</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?php echo Url::toRoute('site/logout')?>"><i class="dropdown-icon fa fa-power-off"></i>&nbsp;&nbsp;退出</a></li>
                                </ul>
                            </li>
                        </ul> <!-- / .navbar-nav -->
                    </div> <!-- / .right -->
                </div>
            </div> <!-- / #main-navbar-collapse -->
        </div> <!-- / .navbar-inner -->
    </div> <!-- / #main-navbar -->
<!-- /2. $END_MAIN_NAVIGATION -->


<!-- 4. $MAIN_MENU =================================================================================-->
    <div id="main-menu" role="navigation">
        <div id="main-menu-inner">
            <?php $menu=MenuHelper::getAssignedMenu(Yii::$app->user->id);?>
            <ul class="navigation">
                <?php foreach ($menu as $key => $value) :?>
                    <?php
                        if (!YII_DEBUG and $value['label']=='权限管理') {
                            continue;
                        }
                    ?>
                    <li <?php if(isset($value['items'])){echo "class='mm-dropdown'";}?>>
                        <a href="<?php echo isset($value['items'])? "javascript:void(0);":Url::toRoute($value['url']) ;?>"><?php echo $value['data']; ?><span class="mm-text"><?php echo $value['label'];?></span></a>
                        <?php if (isset($value['items'])): ?>
                            <ul>
                                <?php foreach ($value['items'] as $key => $val) :?>
                                    <?php
                                        // 根据是否开启分销功能过滤分销链接
                                        if (in_array(trim($val['url'][0], '/'), ['config/jingxiao', 'config/fenxiao'])) {
                                            if (!IS_FENXIAO) {
                                                continue;
                                            }
                                        }
                                    ?>
                                <li>
                                    <a tabindex="-1" href="<?php echo Url::toRoute($val['url'])?>"><span class="mm-text"><?php echo $val['label'];?></span></a>
                                </li>
                               <?php endforeach;?>
                            </ul>
                        <?php endif ?>
                    </li>
                <?php endforeach;?>
             
            </ul> <!-- / .navigation -->
            <div class="menu-content">
                <!-- <a href="pages-invoice.html" class="btn btn-primary btn-block btn-outline dark">Create Invoice</a> -->
            </div>
        </div> <!-- / #main-menu-inner -->
    </div> <!-- / #main-menu -->
<!-- /4. $MAIN_MENU -->


    <div id="content-wrapper">
        <?php if (isset($_GET['r']) and $_GET['r'] != 'site/index') :?>
        <div class="panel">
            <div class="panel-heading">
                <?= Breadcrumbs::widget([
                    // 'homeLink'=>[
                    //    'label'=>'后台首页>>', 修改默认的Home
                    //    'url'=>Url::to(['index/index']), 修改默认的Home指向的url地址
                    // ],
                    'homeLink' => ['label' => '首页', 'url' => ['site/index']],
//            'homeLink'=>false, // 若设置false 则 可以隐藏Home按钮
                    'itemTemplate'=>"<span class='panel-title'>{link}</span> > ",
                    'activeItemTemplate'=>"<span class='panel-title active'>{link}</span>",
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'options' => [
                        'class' => 'dl-mbx'
                    ]
                ]) ?>
            </div>
        </div>
    <?php endif; ?>

     <?= $content ?>

    </div> <!-- / #content-wrapper -->
    <div id="main-menu-bg"></div>
</div> <!-- / #main-wrapper -->


<style type="text/css">
    .a_margin{
        margin-left: 10px;
    }

    table tbody tr td > button {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: normal;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;

        padding: 1px 5px;
        font-size: 12px;
        line-height: 1.5;
        border-radius: 3px;

        margin-bottom: 3px;
        margin-top: 3px;
    }
    table tbody tr td:last-child>a {
        margin-right: 20px;

        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: normal;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;

        padding: 1px 5px;
        font-size: 12px;
        line-height: 1.5;
        border-radius: 3px;

        color: #fff;
        background-color: #5bc0de;
        border-color: #46b8da;


        margin-bottom: 3px;
        margin-top: 3px;

    }
</style>


<!-- Pixel Admin's javascripts -->
<script src="<?php echo Yii::$app->request->hostInfo;?>/public/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::$app->request->hostInfo;?>/public/js/pixel-admin.min.js"></script>
<script src="<?php echo Yii::$app->request->hostInfo;?>/public/js/uploadPreview.js"></script>

<script type="text/javascript">
    init.push(function () {
        //Javascript code here

        $('[data-id="refreshSystemCache"]').click(function () {
            var sysHref = $(this).attr('data-href');

            $.get(sysHref, {}, function (res) {
                res = JSON.parse(res);

                if (res.status) {
                    alert(res.msg);
                    return false;
                }

                alert(res.msg);
            })
        })
    })
    window.PixelAdmin.start(init);
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>