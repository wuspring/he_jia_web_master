<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\IntergralArticle */

$this->title = '编辑文章';
$this->params['breadcrumbs'][] = ['label' => '积分商城', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '积分文章', 'url' => ['intergral-article/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
