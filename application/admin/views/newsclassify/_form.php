<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Newsclassify */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/js/uploadPreview.js');
?>
<script src="/public/js/base_upload.js"></script>
<script src="/public/js/upload-hooks/preview.js"></script>

    <script>
        var uploadPath = '/upload/img';
    </script>
    <?php $form = ActiveForm::begin([
     'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
        'fieldConfig' => [
        'inputOptions' => ['class' => 'form-control'],
        'labelOptions'=>['class'=>'col-sm-2 control-label'],
        'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
    ]); ?>

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
        <div style="float: right;text-align: right">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '创建') : Yii::t('app', '保存修改'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

<div class="panel-body form-controls-demo">
    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
    <div class="form-group field-newcategory-pid has-success">
        <label class="col-sm-2 control-label" for="newcategory-pid">上级</label><div class="col-sm-10">
        <?php echo  $parent;?>
        <div class="help-block"></div></div>
    </div>

    <?= $form->field($model, 'icoImg')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => false,
            ],
            'server' => Url::toRoute(['newsclassify/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ]); ?>

    <?= $form->field($model, 'isShow')->radioList(['0'=>'不显示','1'=>'显示']) ?>

    <?= $form->field($model, 'isRdm')->radioList(['0'=>'不推荐','1'=>'推荐'])?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <?php ActiveForm::end(); ?>

</div>
