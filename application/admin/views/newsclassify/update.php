<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Newsclassify */

$this->title = '修改文章分类: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => '资讯管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name,'url'=>'javascript:void(0)'];
$this->params['breadcrumbs'][] = '编辑';
?>
<div class="panel">
    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
        'parent'=>$parent,
    ]) ?>

</div>

