<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Fenxiaoshang */

$this->title = '分销商审核 ： ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Fenxiaoshangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fenxiaoshang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
