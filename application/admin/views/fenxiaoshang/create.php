<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Fenxiaoshang */

$this->title = 'Create Fenxiaoshang';
$this->params['breadcrumbs'][] = ['label' => 'Fenxiaoshangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fenxiaoshang-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
