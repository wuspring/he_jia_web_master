<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MemberCouponSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Member Coupons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-coupon-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Member Coupon', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'store_id',
            'member_id',
            'coupon_id',
            'coupon_auto_send_id',
            // 'begin_time',
            // 'end_time',
            // 'is_expire',
            // 'is_use',
            // 'is_delete',
            // 'addtime',
            // 'type',
            // 'integral',
            // 'price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
