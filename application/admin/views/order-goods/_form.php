<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OrderGoods */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-goods-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
    </div>

    <?= $form->field($model, 'order_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goods_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goods_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goods_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goods_integral')->textInput() ?>

    <?= $form->field($model, 'fallinto_state')->textInput() ?>

    <?= $form->field($model, 'goods_num')->textInput() ?>

    <?= $form->field($model, 'fallInto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goods_pay_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'buyer_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'createTime')->textInput() ?>

    <?= $form->field($model, 'modifyTime')->textInput() ?>

    <?= $form->field($model, 'sku_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'attr')->textarea(['rows' => 6]) ?>

    <?php ActiveForm::end(); ?>

</div>
