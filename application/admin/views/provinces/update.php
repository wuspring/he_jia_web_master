<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Provinces */

$this->title = '编辑';
$this->params['breadcrumbs'][] = ['label' => '城市管理', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '地区列表', 'url' => ['provinces/index']];
$this->params['breadcrumbs'][] = $this->title;



?>
<div class="panel">
    <?= $this->render('_form', [ 'etitle' => $this->title,
        'pModel' => $model,
    ]) ?>

</div>
