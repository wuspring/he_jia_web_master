<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FloorIndex */
/* @var $form yii\widgets\ActiveForm */
?>
<style type="text/css">
    .input-select {
        background: #dff0d8;
        border-color: #d0e6be;
        color: #468847;
        background-size: 20px 20px;
        display: block;
        border: 1px solid;
        padding: 5px 10px;
        border-radius: 2px;
        margin-top: 2px;
        position: relative;
    }

    .input-select a {
        display: block;
        line-height: 30px;
        padding: 2px 10px;
        color: #333;
    }

    .input-select a.active, .input-select a:hover {
        color: #205f82;
    }

    .input-select a img {
        display: block;
        float: left;
        height: 30px;
        margin-right: 10px;
    }

</style>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active > a {
        color: #fff !important;
        background-color: #1a7ab9;
    }

    .goods-pic {
        width: 5.5rem;
        height: 5.5rem;
        display: inline-block;
        background-color: #ddd;
        background-size: cover;
        background-position: center;
        margin-right: 1rem;
    }
</style>
<?php $form = ActiveForm::begin(); ?>
<div class="panel-heading">
    <span class="panel-title"><?= Html::encode($this->title) ?></span>
    <div style="float: right;text-align: right">
        <?= Html::submitButton('保存', ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<ul class="nav nav-tabs dl_tab">
    <li class="col-lg-2 tab_title active" data-id="group" data-val="1"><a href="javascript:void(0)">首页展示</a></li>
    <li class="col-lg-2 tab_title" data-id="group" data-val="2"><a href="javascript:void(0)">采购页面展示</a></li>
</ul>

<div class="panel-body" data-id="panel-body" data-val="1">
    <div class="floor-index-form">
        <?= $form->field($model, 'img')->textInput()->label('首页楼层') ?>
        <?= $form->field($model, 'brand')->textInput(['maxlength' => true, 'placeholder' => '请输入添加商户的名称'])->label('推荐商户') ?>
    </div>
</div>
<div class="panel-body" data-id="panel-body" data-val="2" style="display: none">
    <div class="floor-index-form">
        <?= $form->field($model, 'zh_first_imgs')->textInput()->label('展会楼层轮播') ?>
    </div>
    <div class="floor-index-form">
        <?= $form->field($model, 'zh_img')->textInput()->label('展会楼层') ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<div data-id="pics_build" style="display:none;"></div>
<script src="/public/js/jquery.min.js"></script>
<script src="/public/js/base_upload.js"></script>
<script src="/public/js/grids/grid_upload.js"></script>
<script src="/public/js/inputs/input_select.js"></script>
<script src="/public/js/upload-hooks/preview.js"></script>
<script>
    var uploadPath = '/upload/img?more=true';
    // GridUpload($('#floorindex-img'), false);
    // GridUpload($('#floorindex-zh_img'), false);

    GridUpload($('#floorindex-img'));
    GridUpload($('#floorindex-zh_first_imgs'));
    GridUpload($('#floorindex-zh_img'));
</script>
<script>
    var MORE_FILES = true,
        InputSelectCtrlRequestPath = '<?= \DL\service\UrlService::build('floor-index/store'); ?>',
        InputSelectCtrlDataPath = '<?= \DL\service\UrlService::build('floor-index/store-info'); ?>';

    InputSelectCtrl($('#floorindex-brand'));

    //previewCtrl([{category: 'floorindex', id: 'zh_first_imgs', type: 'zh_first_imgs'}])
</script>
<script>
    $('[data-id="group"]').click(function () {
        var that = $(this);
        that.addClass('active').siblings().removeClass('active');
        $('[data-id="panel-body"]').each(function () {
            if ($(this).attr('data-val') == that.attr('data-val')) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    })
</script>