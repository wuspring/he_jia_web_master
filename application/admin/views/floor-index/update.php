<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FloorIndex */

$this->title = "设置";
$this->params['breadcrumbs'][] = ['label' => '城市管理', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '楼层管理', 'url' => ['floor-index/index']];
$this->params['breadcrumbs'][] = ['label' => '楼层设置', 'url' => ['floor-index/info', 'cid' => $model->good_class_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>
</div>
