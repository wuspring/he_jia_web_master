<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FloorIndex */

$this->title = 'Create Floor Index';
$this->params['breadcrumbs'][] = ['label' => 'Floor Indices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="floor-index-create">
    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
