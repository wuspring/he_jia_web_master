<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TicketGoodsCoupon */

$this->title = 'Create Ticket Goods Coupon';
$this->params['breadcrumbs'][] = ['label' => 'Ticket Goods Coupons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">
    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
        'ticketId' => $ticketId,
    ]) ?>
    </div>
</div>
