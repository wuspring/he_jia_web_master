<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\TicketGoodsCouponSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '展会页展示券列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">
    <p>
        <?= Html::a('添加店铺券', ['create', 'ticketId' => $ticketId], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            array(
                'attribute' => 'goods_coupon_id',
                'label' => '店铺信息',
                'value' => function ($filterModel) {
                    return $filterModel->goodsCoupon->user->nickname;
                },
            ),

            'sort',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>