<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FitupAnswer */

$this->title = 'Update Fitup Answer: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fitup Answers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fitup-answer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
