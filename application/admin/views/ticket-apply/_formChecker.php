<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TicketApply */
/* @var $form yii\widgets\ActiveForm */

function drewPictures($str)
{
$goodIds = json_decode($str, false);
$goodIds or $goodIds = [0];

$goods = \common\models\Goods::find()->where([
'and',
['in', 'id', $goodIds],
['=', 'isdelete', 0],
['=', 'goods_state', 1],
])->all();
$str = '';

foreach ($goods As $good) {
$str .= "<span><img src='{$good->goods_pic}' alt='' style='width: 80px; height: 60px;display: inline-block'><span>{$good->goods_name}</span></span>";
}

return $str;
}

?>

<?php
$form = ActiveForm::begin();

$dic = $model->getStatusDic();
if (isset($dic[0])) {
    unset($dic[0]);
}
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
        <div style="float: right;text-align: right">
            <?= Html::submitButton('提交', ['class' =>  'btn btn-primary']) ?>
        </div>
    </div>

    <div class="panel-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'user_id',
                    'label' => '商品分类',
                    'value' => $model->store->nickname,
                ],
                [
                    'attribute' => 'ticket_id',
                    'label' => '展会名称',
                    'value' => $model->ticket->name,
                ],
                [
                    'attribute' => 'create_time',
                    'label' => '申请时间',
                ],
            ],
        ]) ?>

    </div>

    <div class="panel-body">
        <span class="panel-title">参展商品</span>
        <div class="panel-body">
            <table class="table">
                <thead>
                爆款预约
                </thead>
                <tbody>
                <tr>
                    <td class="col-md-3"><b>商品名称</b></td>
                    <td class="col-md-3"><b>封面图</b></td>
                    <td class="col-md-3"><b>活动价格</b></td>
                    <td class="col-md-3"><b>库存</b></td>
                </tr>
                <?php if ($couponGoods) :?>
                    <?php foreach ($couponGoods as $shop): ?>
                        <tr>
                            <td class="col-md-3"><?= $shop->goods->goods_name; ?></td>
                            <td class="col-md-3"><img src="<?= $shop->good_pic; ?>" style="max-width: 300px" alt=""></td>
                            <td class="col-md-3"><?= $shop->good_price; ?></td>
                            <td class="col-md-3"><?= $shop->good_amount; ?></td>
                        </tr>
                    <?php endforeach;?>
                <?php else :?>
                    <tr>
                        <td colspan="4" class="col-md-12" style="text-align: center">暂未添加</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>

        <div class="panel-body">
            <table class="table">
                <thead>
                预存 享特价
                </thead>
                <tbody>
                <tr>
                    <td class="col-md-3"><b>商品名称</b></td>
                    <td class="col-md-3"><b>封面图</b></td>
                    <td class="col-md-3"><b>活动价格</b></td>
                    <td class="col-md-3"><b>库存</b></td>
                </tr>
                <?php if ($orderGoods) :?>
                    <?php foreach ($orderGoods as $shop):?>
                        <tr>
                            <td class="col-md-3"><?= $shop->goods->goods_name; ?></td>
                            <td class="col-md-3"><img src="<?= $shop->good_pic; ?>" style="max-width: 300px" alt=""></td>
                            <td class="col-md-3"><?= $shop->good_price; ?></td>
                            <td class="col-md-3"><?= $shop->good_amount; ?></td>
                        </tr>
                    <?php endforeach;?>
                <?php else :?>
                    <tr>
                        <td colspan="4" class="col-md-12" style="text-align: center">暂未添加</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>


<div class="panel-body form-controls-demo">
<div class="ticket-apply-form">



    <?= $form->field($model, 'id', ['options' => ['style' => 'display:none']])->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'user_id', ['options' => ['style' => 'display:none']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ticket_id', ['options' => ['style' => 'display:none']])->textInput() ?>

    <?php $form->field($model, 'zw_pos', [])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList($dic, ['prompt' => '请选择'])->label('审核') ?>

    <?= $form->field($model, 'reason')->textarea(['rows' => 6])->label('拒绝理由') ?>


    <?php ActiveForm::end(); ?>

</div>
</div>
