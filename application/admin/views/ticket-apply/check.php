<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TicketApply */

$this->title = '审核';
$this->params['breadcrumbs'][] = ['label' => '展会管理', 'url' => ['ticket/manage']];
$this->params['breadcrumbs'][] = ['label' => '参展审核', 'url' => ['ticket-apply/index', 'id' => $model->ticket_id]];
$this->params['breadcrumbs'][] = $this->title;

?>

    <?= $this->render('_formChecker', [
        'model' => $model,
        'couponGoods' => $couponGoods,
        'orderGoods' => $orderGoods
    ]) ?>

</div>
