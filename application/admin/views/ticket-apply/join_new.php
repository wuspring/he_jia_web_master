<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TicketApply */

$this->title = '申请参加';
$this->params['breadcrumbs'][] = ['label' => 'Ticket Applies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel">



    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
        'couponGoods' => isset($couponGoods) ? $couponGoods : [],
        'orderGoods' => isset($orderGoods) ? $orderGoods : []
    ]) ?>


</div>
