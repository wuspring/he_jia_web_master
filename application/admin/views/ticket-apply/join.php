<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TicketApply */

$this->title = '申请参加';
$this->params['breadcrumbs'][] = ['label' => 'Ticket Applies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
        'goods' => $goods,
        'couponGoods' => isset($couponGoods) ? $couponGoods : [],
        'orderGoods' => isset($orderGoods) ? $orderGoods : []
    ]) ?>

</div>
