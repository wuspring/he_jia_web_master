<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Coupon */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="panel-body form-controls-demo">
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],
        'fieldConfig' => [
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions'=>['class'=>'col-sm-2 control-label'],
            'template' => "{label}<div class='col-sm-8'>{input}{hint}{error}</div>",],
    ]); ?>
    <div class="form-group">
        <div class="col-sm-offset-2 col-xs-8" style="text-align: right;">
            <?= Html::submitButton($model->isNewRecord ? '添加' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php // $form->field($model, 'type')->radioList(['4'=>'通用券']) ?>

    <?php // $form->field($model, 'store_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'desc')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'pic_url')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'discount_type')->radioList([1=>'满减']) ?>
    <?= $form->field($model, 'discount_type', ['options' => ['style' => 'display:none;']])->textInput(['value' => '1']) ?>
    <?= $form->field($model, 'min_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sub_price')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'discount')->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'expire_type')->radioList([1=>'领取后N天过期',2=>'指定有效期']) ?>
    <?= $form->field($model, 'expire_type', ['options' => ['style' => 'display:none;']])->textInput(['value' => '2']) ?>
    <?= $form->field($model, 'expire_day', ['options' => ['style' => 'display:none;']])->textInput(['value' => '0']) ?>

    <?php $form->field($model, 'expire_day')->textInput() ?>

    <?= $form->field($model, 'begin_time')->widget(DateTimePicker::classname(), [
        'options' => [
                'placeholder' => '',
        ],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]); ?>

    <?= $form->field($model, 'end_time')->widget(DateTimePicker::classname(), [
        'options' => [
            'placeholder' => '',
        ],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]);  ?>

    <?= $form->field($model, 'total_count')->textInput() ?>

    <?php // $form->field($model, 'is_join')->textInput() ?>

    <?php // $form->field($model, 'sort')->textInput() ?>

    <?php // $form->field($model, 'is_integral')->textInput() ?>

    <?php // $form->field($model, 'integral')->textInput() ?>

    <?php // $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'total_num')->textInput() ?>



    <?php // $form->field($model, 'user_num')->textInput() ?>

    <?php // $form->field($model, 'goods_id_list')->textInput(['maxlength' => true]) ?>

    <?php ActiveForm::end(); ?>
</div>

<script>
    init.push(function () {
        checkView();
        $("input[name='Coupon[expire_type]']").on('change',function () {
            checkView();
        });

        function checkView() {
            var expire_type = $("input[name='Coupon[expire_type]']:checked").val();
            if (expire_type == 1) {
                $('.field-coupon-expire_day').show();
                $('.field-coupon-begin_time').hide();
                $('.field-coupon-end_time').hide();
            } else {
                $('.field-coupon-expire_day').hide();
                $('.field-coupon-begin_time').show();
                $('.field-coupon-end_time').show();
            }
        }
    });
</script>


