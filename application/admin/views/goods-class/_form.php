<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Goodsclass */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/js/uploadPreview.js');
?>
<script src="/public/js/base_upload.js"></script>
<script src="/public/js/upload-hooks/preview.js"></script>
<script>
    var uploadPath = '/upload/img';
</script>

<?php $form = ActiveForm::begin([
    'options' => ['enctype' =>'multipart/form-data'],
    'fieldConfig' => [
        'inputOptions' => ['class' => 'form-control'],
        'labelOptions'=>['class'=>'col-sm-2 control-label'],
        'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
]); ?>
<div class="panel-heading">
    <span class="panel-title"><?= Html::encode($this->title) ?></span>
    <div class="form-group" style="float:right;text-align: right;">
        <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
    </div>
</div>
<div class="panel-body">
    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
    <div class="form-group field-goodsclass-fid">
        <label class="col-sm-2 control-label" for="goodsclass-fid">父级</label>
        <div class="col-sm-10">
            <?php echo $parent;?>

            <div class="help-block"></div>
        </div>
    </div>

    <?= $form->field($model, 'icoImg')->textInput() ?>

    <?= $form->field($model, 'is_show')->radioList(['0'=>'显示','1'=>'不显示']) ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";

    $(function(){
        var Config  = [
            {category : 'goodsclass', id : 'icoimg', type : 'icoimg'},
        ];
        previewCtrl(Config);
    });
</script>