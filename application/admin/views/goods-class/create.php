<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Goodsclassify */

$this->title = '添加商品分类';
$this->params['breadcrumbs'][] = ['label' => '商品管理', 'url' => 'javascript:void(0)'];
$this->params['breadcrumbs'][] = ['label' => '商品分类', 'url' => ['goods-class/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
        'parent'=>$parent,
    ]) ?>

</div>