<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TicketAsk */

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Ticket Asks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-ask-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
