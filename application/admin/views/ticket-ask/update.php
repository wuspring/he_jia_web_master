<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\TicketAsk */

$this->title = '申请人信息: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => '索票信息', 'url' => ['ticket-ask/index', 'id' => $model->ticket->id]];
$this->params['breadcrumbs'][] = '索票信息';
?>
<style>
    .tleft {
        text-align: left!important;
    }
</style>
<?php $form = ActiveForm::begin([
    'id' => 'goodsfrom',
    'options' => [
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
        'inputOptions' => ['class' => 'form-control'],
        'labelOptions' => ['class' => 'col-sm-1 control-label tleft'],
        'template' => "{label}<div class='col-sm-8'>{input}{hint}{error}</div>",
    ],
]); ?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <div class="panel-body">
        <table id="w0" class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th>姓名</th>
                <td><?=$model->name?></td>
            </tr>
            <tr>
                <th>城市</th>
                <td><?=$model->cityName->cname?></td>
            </tr>
            <tr>
                <th>票据</th>
                <td><?=$model->ticket->name?></td>
            </tr>
            <tr>
                <th>数量</th>
                <td><?=$model->ticket_amount?></td>
            </tr>
            <tr>
                <th>渠道</th>
                <td><?=$model->resource?></td>
            </tr>

            <tr>
                <th>时间</th>
                <td><?=$model->create_time?></td>
            </tr>
            </tbody>
        </table>




            <?= $form->field($model, 'mobile')->textInput(['maxlength' => true])->label('手机号') ?>

            <?= $form->field($model, 'address')->textarea(['rows' => 6])->label('地址') ?>

            <?= $form->field($model, 'shipping_code')->textInput(['maxlength' => true])->label('快递号') ?>

             <div style="display: none">
                 <?= $form->field($model, 'status')->hiddenInput()?>
             </div>

            <div class="form-group">
                <label class="col-sm-1 control-label "></label>
                <div class="col-sm-8">
                    <?php if ($model->status==0):?>
                        <?= Html::submitButton('送出', ['class' => 'btn btn-success','onclick'=>'return tiyz(1)']) ?>
                        <?= Html::submitButton('刪除', ['class' => 'btn btn-primary','onclick'=>'return tiyz(2)']) ?>
                    <?php else:?>
                        <?= Html::submitButton('已送出', ['class' => 'btn btn-success']) ?>
                    <?php endif;?>

                </div>
            </div>
            <?php ActiveForm::end(); ?>

    </div>
</div>

<script>
  function tiyz(st) {
      $("#ticketask-status").val(st);
      return true;
  }

</script>








