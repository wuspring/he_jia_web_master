<?php

use yii\helpers\Html;
use yii\grid\GridView;
use DL\service\UrlService;

/* @var $this yii\web\View */
/* @var $searchModel admin\search\TicketAskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '索票信息';
$this->params['breadcrumbs'][] = $this->title;

$showTab = false;
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>
<div class="panel">

    <div class="panel-heading">
         <span class="panel-title"><?= Html::encode($this->title . ' | ' . '门票申请') ?></span>
    </div>

    <?php if ($showTab) :if ($caTypeInfo) :?>
    <ul class="nav nav-tabs dl_tab">
        <li class="col-lg-2 tab_title <?=($active=='JIE_HUN')?'active':''?>"><a  href="<?=UrlService::build(['ticket-ask/ca-index','tab'=>'JIE_HUN']);?>">婚庆<?= ($caTypeInfo ? '采购' : '展'); ?></a></li>
        <li class="col-lg-2 tab_title <?=($active=='JIA_ZHUANG')?'active':''?>"><a  href="<?=UrlService::build(['ticket-ask/ca-index','tab'=>'JIA_ZHUANG']);?>">家装<?= ($caTypeInfo ? '采购' : '展'); ?></a></li>
        <li class="col-lg-2 tab_title <?=($active=='YUN_YING')?'active':''?>"><a  href="<?=UrlService::build(['ticket-ask/ca-index','tab'=>'YUN_YING']);?>">孕婴<?= ($caTypeInfo ? '采购' : '展'); ?></a></li>
    </ul>
    <?php else :?>
        <ul class="nav nav-tabs dl_tab">
            <li class="col-lg-2 tab_title <?=($active=='JIE_HUN')?'active':''?>"><a  href="<?=UrlService::build(['ticket-ask/index','tab'=>'JIE_HUN']);?>">婚庆<?= ($caTypeInfo ? '采购' : '展'); ?></a></li>
            <li class="col-lg-2 tab_title <?=($active=='JIA_ZHUANG')?'active':''?>"><a  href="<?=UrlService::build(['ticket-ask/index','tab'=>'JIA_ZHUANG']);?>">家装<?= ($caTypeInfo ? '采购' : '展'); ?></a></li>
            <li class="col-lg-2 tab_title <?=($active=='YUN_YING')?'active':''?>"><a  href="<?=UrlService::build(['ticket-ask/index','tab'=>'YUN_YING']);?>">孕婴<?= ($caTypeInfo ? '采购' : '展'); ?></a></li>
        </ul>
    <?php endif;endif; ?>


    <form method="get" class="row" style="padding-top: 15px">
        <input type="hidden" name="r" value="ticket-ask/index">
        <?php if (isset($_GET['tab'])):?>
            <input type="hidden" name="tab" value="<?=$_GET['tab']?>">
        <?php else:?>
            <input type="hidden" name="tab" value="JIE_HUN">
        <?php endif;?>
    </form>
    <div class="panel-body">
        <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'name',
                    'label'=>'姓名',
                ],
                [
                    'attribute' => 'provinces_id',
                    'label'=>'城市',
                    'value'=>function($filetModel){

                       return $filetModel->cityName->cname;
                     }

                ],
                [
                    'attribute' => 'ticket_id',
                    'label'=>'门票',
                    'value'=>function($filetModel){
                        return $filetModel->ticket->name;
                    }
                ],

                [
                    'attribute' => 'mobile',
                    'label'=>'手机号',

                ],

                [
                    'attribute' => 'status',
                    'label'=>'状态',
                    'value'=>function($filetModel){
                        return $filetModel->statusInfo;
                    }

                ],
                                [
                    'attribute' => 'create_time',
                    'label'=>'申请时间'
                ],
                [
                    'attribute' => 'channel',
                    'label'=>'推广渠道',
                    'value'=>function($filetModel){
                        return $filetModel->channel;
                    }

                ],


                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => '操作',
                    'template' => '{update}'
                ],
            ],
        ]); ?>
    </div>


</div>
<script>
    $(function () {
        $('#w0 .form-group').addClass('col-sm-2').last().css('margin-top', '25px');
        $('#w0 .form-group').removeClass('form-group');
        $('#w0').addClass('form-group');
    })
</script>