<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StoreShop */

$this->title = '修改店铺: ' . $model->store_name;
$this->params['breadcrumbs'][] = ['label' => 'Store Shops', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
