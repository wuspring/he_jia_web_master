<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SelectCity */

$this->title = 'Create Select City';
$this->params['breadcrumbs'][] = ['label' => 'Select Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="select-city-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
