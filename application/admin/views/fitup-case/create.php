<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FitupCase */

$this->title = '添加';
$this->params['breadcrumbs'][] = ['label' => '装修学堂', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '案例列表', 'url' => ['fitup-case/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
        'strHtml'=>$strHtml,
        'drowList'=>$drowList
    ]) ?>

</div>
