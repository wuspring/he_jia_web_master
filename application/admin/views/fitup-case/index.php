<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '案例列表';
$this->params['breadcrumbs'][] = ['label' => '装修学堂', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    .a_margin{margin-left: 10px}
</style>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title">
            <?= Html::encode($this->title) ?>
            <span style="float: right;text-align: right;">
        <?= Html::a('添加案例', ['create'], ['class' => 'btn btn-success']) ?>
    </span>

        </span>
    </div>

    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
           // 'fitup_company',
            [
                'attribute' => 'user_id',
                'label' => '店铺名称',
                'value' => function ($filterModel) {
                    return $filterModel->store->nickname;
                },
            ],
            'address',
            'design',
            'area',
            [
                'attribute' => 'house_type',
                'label' => '户型',
                'value' => function ($filterModel) {
                    return $filterModel->getFitupAttr($filterModel->house_type)->name;
                },
            ],
            [
                'attribute' => 'design_style',
                'label' => '风格',
                'value' => function ($filterModel) {
                    return $filterModel->getFitupAttr($filterModel->design_style)->name;
                },
            ],
            [
                'attribute' => 'provinces_id',
                'label' => '城市',
                'value' => function ($filterModel) {
                    return $filterModel->cityName;
                },
            ],
            [
                'attribute' => 'create_time',
                'label' => '添加时间',
            ],
            // 'is_del',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '操作',
                'template' => '{update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {

                        return Html::a('查看', $url, ['title' => '查看', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('更新', $url, ['title' => '更新', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                    },
                    'delete' => function ($url, $model, $key) {
                        $unDeleteArray = [1];
                        if (!in_array($model->id, $unDeleteArray)) {
                            return Html::a('删除', $url, ['title' => '删除', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                        }
                    },
                ],

            ],
        ],
    ]); ?>
    </div>
</div>

