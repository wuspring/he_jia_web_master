<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FitupCase */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile(Yii::$app->request->hostInfo . '/public/ueditor/ueditor.config.js');
$this->registerJsFile(Yii::$app->request->hostInfo . '/public/ueditor/ueditor.all.min.js');
$this->registerJsFile(Yii::$app->request->hostInfo . '/public/ueditor/lang/zh-cn/zh-cn.js');
?>
<style>
    .form-control.editor {
        margin-top: 20px;
        padding: 0;
        margin: 20px 0;
        width: 100%;
        height: auto;
        border: none;
    }
</style>
<?php $form = ActiveForm::begin([
    'options' => ['enctype' =>'multipart/form-data'],
    'fieldConfig' => [
        'inputOptions' => ['class' => 'form-control'],
        'labelOptions'=>['class'=>'col-sm-2 control-label'],
        'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
]); ?>

<div class="panel-heading">
    <span class="panel-title"><?= Html::encode($this->title) ?></span>
    <div style="float: right;text-align: right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '发布') : Yii::t('app', '保存修改'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>


<div class="panel-body">

    <?php $form->field($model, 'fitup_company')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('标题') ?>
    <?= $form->field($model, 'keywords')->textarea(['rows' => 6])->label('关键字') ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6])->label('描述') ?>

    <?= $form->field($model, 'design')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'area')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'cover_pic')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => false,
            ],
            'server' => Url::toRoute(['fitup-case/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ])->label("商品主图"); ?>
    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'house_type')->dropDownList($model->getFitupChilder(1),['prompt'=>'请选择'])->label('选择户型') ?>

    <?= $form->field($model, 'design_style')->dropDownList($model->getFitupChilder(2),['prompt'=>'请选择'])->label('设计风格') ?>

    <?= $form->field($model, 'budget')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_id')->dropDownList($model->user,['prompt'=>'请选择'])->label('公司名称')?>

    <?php $form->field($model, 'is_recommend')->radioList(['0'=>'不推荐','1'=>'推荐'])->label('是否推荐') ?>

    <?php $model->provinces_id=json_decode($model->provinces_id); ?>
    <?= $form->field($model, 'provinces_id')->checkboxList($model->getSelectCity())->label('选择城市') ?>

    <div id="content_model">
        <?php if(!empty($model->tab_text)):?>
            <?php foreach ($model->tab_text as $k=>$item):?>
                <div class="editor">
                    <div class="form-group fitupCase-tab_text">
                        <label class="col-sm-2 control-label" for="name">类型</label>
                        <div class="col-sm-10">
                            <select style="width: 80%;float: left" class="form-control" name="FitupCase[tab_text][<?=$k?>][title]">
                                <?php foreach ($drowList as $drowK=>$drowV):?>
                                      <?php if ($drowK==$item->title):?>
                                          <option value="<?=$drowK?>" selected><?=$drowV?></option>
                                      <?php else:?>
                                        <option value="<?=$drowK?>"><?=$drowV?></option>
                                      <?php endif;?>
                                <?php endforeach;?>
                            </select>
                            <button type="button" class="btn btn-danger btn-rounded" onclick="delUser(this);">删除</button>
                        </div>
                    </div>
                    <div class="form-group field-fitupCase-tab_text">
                        <label class="col-sm-2 control-label" for="fitupCase-tab_text">内容</label>
                        <div class="col-sm-10">
                            <textarea id="fitupCase-tab_text<?=$k?>" class="form-control editor"  name="FitupCase[tab_text][<?=$k?>][content]" rows="6"><?=$item->content?></textarea>
                        </div>
                    </div>
                </div>
                <script>
                    init.push(function () {
                        var ik = UE.getEditor('fitupCase-tab_text<?=$k?>', {
                            'zIndex': 9,
                        });
                    });
                </script>
            <?php endforeach;?>
        <?php endif;?>
    </div>

    <div>
        <div class="form-group field-job-name">
            <label class="col-sm-2 control-label" for="name"></label>
            <div class="col-sm-10">
                <button type="button" id="user_btn" class="btn btn-success btn-rounded">添加案例类型</button>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    var k =<?=count($model->tab_text) ?>;
    var strHtml='<?=$strHtml?>';
    function delUser(obj){
        $(obj).parent().parent().parent().remove();
    };

    $('#user_btn').click(function () {

        var html = '<div class="model_div"><div class="form-group field-fitupCase-name">\n' +
            '<label class="col-sm-2 control-label" for="name">标题</label>\n' +
            '<div class="col-sm-10">\n'+
            '<select style="width: 80%;float: left" class="form-control" name="FitupCase[tab_text]['+k+'][title]">'+
            strHtml+
             '</select>'+
            '<button type="button" class="btn btn-danger btn-rounded" onclick="delUser(this);">删除</button>'+
            '</div>\n' +
            '</div>'+'<div class="form-group field-fitupCase-tab_text">\n' +
            '<label class="col-sm-2 control-label" for="fitupCase-tab_text">内容</label>\n' +
            '<div class="col-sm-10">\n' +
            '<textarea id="fitupCase-tab_text'+ k +'" class="form-control editor" name="FitupCase[tab_text]['+ k +'][content]" rows="6"></textarea>\n' +
            '</div>\n' +
            '</div></div>';

        $('#content_model').append(html);

        var ue = UE.getEditor('fitupCase-tab_text'+k, {
            'zIndex': 9,
        });
        k = k+1;
    });
</script>