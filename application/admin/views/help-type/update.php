<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HelpType */

$this->title = '更新';
$this->params['breadcrumbs'][] = ['label' => '帮助中心', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '分类信息', 'url' => ['help-type/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
