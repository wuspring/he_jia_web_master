<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FitupAppointment */

$this->title = '编辑 ';
$this->params['breadcrumbs'][] = ['label' => 'Fitup Appointments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="panel">
    <div class="panel-heading">
         <span class="panel-title">
             <?= Html::encode($this->title) ?>
         </span>
    </div>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
