<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Goods */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="panel-body form-controls-demo">
    <?php $form = ActiveForm::begin([
        'id' => 'goodsfrom',
        'options' => [
            'class' => 'form-horizontal',
            'enctype' => 'multipart/form-data'
        ],
        'fieldConfig' => [
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions' => ['class' => 'col-sm-2 control-label '],
            'template' => "{label}<div class='col-sm-8'>{input}{hint}{error}</div>",],
    ]); ?>

    <div class="form-group">
        <label class="col-sm-2 control-label "></label>
        <div class="col-sm-8" style="text-align: right">
            <?= Html::submitButton($model->isNewRecord ? '提交' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','onclick'=>'return tiyz()']) ?>
        </div>
    </div>

    <?= $form->field($model, 'money')->textInput(['maxlength' => true])->label('金额') ?>

    <div style="display: none">
        <?php $model->valid_day = 0;echo $form->field($model, 'valid_day')->textInput(['maxlength' => true, 'type'=>'number','placeholder'=>'请设置有效天数,例如:30天，从用户领取日计算30天'])->label('优惠劵有效期') ?>

        <?php echo $form->field($model, 'ticket_id')->hiddenInput(['value' => $ticket->id])?>
    </div>

    <?= $form->field($model, 'condition')->textInput(['maxlength' => true,'placeholder'=>'请设置最低使用额度,当满足时方可使用。例如:3000'])->label('最低使用额度') ?>

    <?= $form->field($model, 'describe')->textarea(['rows'=>3,'placeholder'=>'请输入描述,例如:满500减100'])->label('描述') ?>

    <?php ActiveForm::end(); ?>
</div>
