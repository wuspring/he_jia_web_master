<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GoodsCoupon */

$this->title = '增加优惠劵';
$this->params['breadcrumbs'][] = ['label' => 'Goods Coupons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <?= $this->render('_form_store', [
        'model' => $model,
        'ticket' => $ticket,
    ]) ?>

</div>
