<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\GoodsSkuSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="goods-sku-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'goods_id') ?>

    <?= $form->field($model, 'attr') ?>

    <?= $form->field($model, 'num') ?>

    <?= $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'integral') ?>

    <?php // echo $form->field($model, 'goods_code') ?>

    <?php // echo $form->field($model, 'picimg') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
