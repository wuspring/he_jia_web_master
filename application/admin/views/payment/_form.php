<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Payment */
/* @var $form yii\widgets\ActiveForm */
?>



    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
        <div style="float:right;text-align: right">
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </div>
<div class="panel-body">
    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
	<?= $form->field($model, 'code')->textInput(['maxlength' => 255])->label("支付代码") ?>
    <?= $form->field($model, 'state')->radioList(['0'=>'否','1'=>'是'],['prompt'=>0])?>

    <?php ActiveForm::end(); ?>

</div>
