<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\Request;
/* @var $this yii\web\View */
/* @var $model app\Models\Config */

$this->title = 'APP支付宝支付设置';
$this->params['breadcrumbs'][] = ['label' => '系统管理', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '支付管理', 'url' => ['payment/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel">
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal','enctype' =>'multipart/form-data'],]); ?>
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
        <div style="float:right;text-align: right">
            <button type="submit" class="btn btn-primary">编辑</button>
        </div>

    </div>
    <div class="panel-body">


        <div class="form-group">
            <label for="jq-validation-mchid" class="col-sm-3 control-label">APPID</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="app_id" name="app_id" value="<?=$model->app_id;?>" placeholder="APPID">
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-mchid" class="col-sm-3 control-label">应用私钥</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="merchant_private_key" name="merchant_private_key" value="<?=$model->merchant_private_key;?>" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="jq-validation-mchid" class="col-sm-3 control-label">支付宝公钥</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="alipay_public_key" name="alipay_public_key" value="<?=$model->alipay_public_key;?>" placeholder="alipay_public_key">
            </div>
        </div>

        <div class="form-group">
            <label for="jq-validation-key" class="col-sm-3 control-label">签名类型</label>
            <div class="col-sm-9">

                <label class="radio radio-inline">
                    <input type="radio" name="sign_type" value="RSA2" <?php if($model->sign_type=='RSA2'):?>checked<?php endif;?> class="px">
                    <span class="lbl">RSA2</span>
                </label>
                <label class="radio radio-inline">
                    <input type="radio" name="sign_type" value="RSA" <?php if($model->sign_type=='RSA'):?>checked<?php endif;?> class="px">
                    <span class="lbl">RSA</span>
                </label>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
<!-- /5. $JQUERY_VALIDATION -->