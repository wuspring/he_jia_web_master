<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\IntergralLuckDraw */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="intergral-luck-draw-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
    </div>

    <?= $form->field($model, 'member_id')->textInput() ?>

    <?= $form->field($model, 'prize_terms')->textInput() ?>

    <?= $form->field($model, 'draw_id')->textInput() ?>

    <?= $form->field($model, 'create_time')->textInput() ?>

    <?= $form->field($model, 'is_del')->textInput() ?>

    <?= $form->field($model, 'is_show')->textInput() ?>

    <?php ActiveForm::end(); ?>

</div>
