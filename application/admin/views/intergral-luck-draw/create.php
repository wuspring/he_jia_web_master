<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\IntergralLuckDraw */

$this->title = 'Create Intergral Luck Draw';
$this->params['breadcrumbs'][] = ['label' => 'Intergral Luck Draws', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intergral-luck-draw-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
