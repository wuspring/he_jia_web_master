<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Service */

$this->title = '编辑';
$this->params['breadcrumbs'][] = ['label' => '系统管理', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '承诺服务', 'url' => ['service/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="panel">

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>