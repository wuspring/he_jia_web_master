<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = '编辑';
$this->params['breadcrumbs'][] = ['label' => '订单管理', 'url' => 'javascript:void(0)'];
$this->params['breadcrumbs'][] = ['label' => '订单列表', 'url' => \DL\service\UrlService::build('order/index')];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
