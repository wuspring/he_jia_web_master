<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NavsWap */

$this->title = '添加导航';
$this->params['breadcrumbs'][] = ['label' => 'Navs Waps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
