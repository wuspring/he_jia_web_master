<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\NavsWap */
/* @var $form yii\widgets\ActiveForm */
?>
<style>

    *{padding:0;margin:0;}
    body{padding:20px;}
    .input-box,.input-box2{
        display: inline-block;
    }
    .wx-input{
        width: 180px;
        height: 30px;
        position: relative;
        padding-left: 2px;
        border-radius: 3px;
        border: 1px solid #ccc;
    }
    input::-ms-clear{display:none;}
</style>
<script src="<?php echo Yii::$app->request->hostInfo;?>/public/js/plumen.js"></script>
<?php $form = ActiveForm::begin(); ?>
<div class="panel-heading">
    <span class="panel-title">
        <?= Html::encode($this->title) ?>
        <div style="float: right;text-align: right;">
        <?= Html::submitButton('保存' , ['class' => 'btn btn-primary']) ?>
    </div>
    </span>
</div>

<div class="panel-body">




    <?= $form->field($model, 'provinces_id')->dropDownList(\common\models\SelectCity::getAllCityName(),['prompt' => '请选择'])->label('所在城市') ?>

    <?= $form->field($model, 'type')->dropDownList(['home'=>'首页']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'icon')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => false,
            ],
            'server' => \yii\helpers\Url::toRoute(['site/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ]); ?>

    <?= $form->field($model, 'icon_selected')->widget('manks\FileInput', [
        'clientOptions' => [
            'pick' => [
                'multiple' => false,
            ],
            'server' => \yii\helpers\Url::toRoute(['site/upload']),
            // 'accept' => [
            // 	'extensions' => 'png',
            // ],
        ],
    ]); ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->radioList(['1'=>'显示','0'=>'隐藏']) ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <?php ActiveForm::end(); ?>

</div>
