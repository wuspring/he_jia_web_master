<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Member */

$this->title = '编辑';

$this->params['breadcrumbs'][] = ['label' => '用户管理', 'url' => 'javascript:void(0)'];
$this->params['breadcrumbs'][] = ['label' => '会员管理', 'url' => ['member/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>