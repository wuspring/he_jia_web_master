<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '会员列表';
$this->params['breadcrumbs'][] = ['label' => '用户管理', 'url' => 'javascript:void(0)'];
$this->params['breadcrumbs'][] = ['label' => '会员管理', 'url' => ['member/index']];
$this->params['breadcrumbs'][] = $this->title;

$findContent = ['username' => '账号', 'nickname' => '昵称'];
?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">
        <div class="row">
            <form method="get">
               <input type="hidden" name="r" value="member/index">
                <div class="form-group col-md-12">
                    <div class="col-md-1" style="width: 132px;">
                        <select class="form-control col-md-6" name="content" style="display: inline-block;">
                            <option value="">请选择</option>
                            <?php
                            foreach ($findContent AS $status => $name) {
                                echo "<option value='{$status}'" . ($filterStatus == $status ? 'selected' : '') . " >{$name}</option>";
                            };
                            ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="keyword"  value="<?=($keyword)?$keyword:''?>" placeholder="请输入">
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-primary">筛选</button>
                    </div>
                </div>
            </form>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                // 'id',
                array(
                    'format' => 'raw',
                    'attribute' => 'avatar',
                    'label' => '头像',
                    'value' => function ($filterModel) {
                        return Html::img($filterModel->avatar, ['width' => 30]);
                    },
                ),
                'username',
                // 'password',
                'nickname',
                // 'role',
                array(
                    'attribute' => 'sex',
                    'label' => '性别',
                    'value' => function ($filterModel) {
                        return ($filterModel->sex == 1) ? '男' : '女';
                    },
                ),
                // 'birthday',
                //  'status',
                // 'avatarTm',
                // 'avatar',
                // 'email:email',
                // 'createTime',
                // 'modifyTime',
                // 'last_visit',
                // 'userType',
                // 'authkey',
                // 'accessToken',
                'remainder',
                'integral',
                // 'rank',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => '操作',
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('查看', $url, ['title' => '查看', 'data-toggle' => 'modal','class'=>'a_margin']);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('更新', $url, ['title' => '更新', 'data-toggle' => 'modal','class'=>'a_margin']);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('删除', $url, ['title' => '删除', 'data-toggle' => 'modal','class'=>'a_margin']);
                        },
                    ],

                ],
            ],
        ]); ?>
    </div>

</div>
