<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TicketInfo */

$this->title = 'Update Ticket Info: ' . $model->ticket_id;
$this->params['breadcrumbs'][] = ['label' => 'Ticket Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ticket_id, 'url' => ['view', 'id' => $model->ticket_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ticket-info-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
