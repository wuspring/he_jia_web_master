<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GoodsCoupon */

$this->title = '增加优惠劵';
$this->params['breadcrumbs'][] = ['label' => '商品管理', 'url' => 'javascript:void(0)'];
$this->params['breadcrumbs'][] = ['label' => '优惠券列表', 'url' => ['goods-coupon/admin-index-store']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <?= $this->render('_admin_form_store', [
        'model' => $model,
        'stores' => $stores
    ]) ?>

</div>
