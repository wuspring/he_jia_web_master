<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GoodsCoupon */

$this->title = '编辑优惠劵';
$this->params['breadcrumbs'][] = ['label' => '商品管理', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '商品列表', 'url' => ['goods/index']];
$this->params['breadcrumbs'][] = '编辑';
?>
<div class="panel">

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
