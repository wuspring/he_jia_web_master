<?php

use yii\helpers\Html;
use yii\grid\GridView;
use DL\service\UrlService;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '店铺优惠券列表';
$this->params['breadcrumbs'][] = ['label' => '商品管理', 'url' => 'javascript:void(0)'];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
    .a_margin{
        margin-left: 10px;
    }
</style>

<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?=Html::encode($this->title)?></span>
    </div>

    <div class="panel-body">
        <?= Html::a('添加店铺优惠劵', ['admin-create-store'], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute'=>'nickname',
                    'label'=>'店铺',
                    'value'=> function($model){
                        return $model->getUser()->nickname;
                    }
                ],
                'money',
                [
                       'attribute'=>'condition',
                       'label'=>'最低使用额度'
                ],
                'describe',
                [
                    'attribute'=>'valid_day',
                     'label'=>'有效期天数'
                ],
                'create_time',

                array(
                    'format' => 'raw',
                    'attribute' => 'goods_state',
                    'label'=>'状态',
                    'value'=>function($filterModel){

                        switch ($filterModel->is_show){
                            case '0':
                                return '<span class="label">已下架</span> | <button type="button" class="btn  btn-success btn-xs upcoupon" data-id="'.$filterModel->id.'">上架</button>';
                                break;
                            case 1:
                                return '<span class="label label-success">已上架</span> | <button type="button" class="btn btn-default btn-xs dncoupon" data-id="'.$filterModel->id.'">下架</button>';
                                break;
                        }

                    },
                ),
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>'操作',
                    'template' => '{delete}',
                ]
            ],
        ]); ?>

    </div>

</div>

<script>
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    $(document).on("click",'.upcoupon',function () {
        var id=$(this).attr('data-id');
        var upbut=this;

        $.post("<?php echo Url::toRoute(['goods-coupon/upcoupon']);?>",{_csrf:_csrf,id:id},function (res) {
            if (res.code==1){
                var html='<span class="label label-success">已上架</span> | <button type="button" class="btn btn-default btn-xs dncoupon" data-id="'+id+'">下架</button>'
                $(upbut).parent().html(html);
            }else{
                alert(res.mg);
            }
        },'json');

    });

    $(document).on("click",'.dncoupon',function () {
        var id=$(this).attr('data-id');
        var dnbut=this;

        $.post("<?php echo Url::toRoute(['goods-coupon/dncoupon']);?>",{_csrf:_csrf,id:id},function (res) {
            if (res.code==1){
                var html='<span class="label">已下架</span> | <button type="button" class="btn  btn-success btn-xs upcoupon" data-id="'+id+'">上架</button>'
                $(dnbut).parent().html(html);
            }else{
                alert(res.mg);
            }
        },'json');

    });

</script>










