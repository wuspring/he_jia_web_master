<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\IntergralTurntable */

$this->title = 'Create Intergral Turntable';
$this->params['breadcrumbs'][] = ['label' => 'Intergral Turntables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intergral-turntable-create">

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
