<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = '抽奖转盘';
$this->params['breadcrumbs'][] = ['label' => '积分商城', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel">

    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <div class="panel-body">
        <button class="btn btn-success resetProbability">重置概率</button>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'angle',
                'prize',
                'v',
//                'create_time',
                // 'is_del',
                // 'is_show',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => '操作',
                    'template' => '{update}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a('更新', $url, ['title' => '更新', 'data-toggle' => 'modal', 'class' => 'a_margin']);
                        },
                    ],

                ],
            ],
        ]); ?>
    </div>

</div>
<script>
    //重置概率
    $(".resetProbability").on('click', function () {

        var url = "<?=Url::toRoute(['intergral-turntable/reset-probability'])?>";
        $.get(url, function (res) {
            if (res.code == 1) {
                alert(res.msg);
                window.location.reload();
            }
        }, 'json')

    })

</script>
