<?php

use yii\helpers\Html;
use yii\grid\GridView;
use  DL\service\UrlService;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MemberCouponSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '核销记录';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }
    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>
<div class="panel">

    <div class="panel-heading">
        <div class="panel-title"><?= Html::encode($this->title) ?></div>
    </div>

    <ul class="nav nav-tabs dl_tab">
        <li class="col-lg-2 tab_title <?=($is_use==0)?'active':''?>"><a  href="<?=UrlService::build(['member-store-coupon/index','is_use'=>0])?>">领取记录</a></li>
        <li class="col-lg-2 tab_title <?=($is_use==1)?'active':''?>"><a  href="<?=UrlService::build(['member-store-coupon/index','is_use'=>1])?>">核销记录</a></li>
    </ul>
    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'member_id',
                    'label' => '会员名称',
                    'value'=>function($fildModel){
                        return (!empty($fildModel->member))?$fildModel->member->nickname:' ';
                    }
                ],
                [
                    'attribute' => 'coupon_id',
                    'label' => '优惠劵描述',
                    'value'=>function($fildModel){
                        return (!empty($fildModel->goodsCoupon))?$fildModel->goodsCoupon->describe:' ';
                    }
                ],
                 'begin_time',
                 'end_time',

                [
                    'attribute' => 'addtime',
                    'label' => '领取时间',
                ],
                // 'type',
                // 'integral',
                // 'price',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => '操作',
                    'template' => '{view}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {

                            return Html::a('<span class="glyphicon glyphicon-globe"></span>', $url, ['title' => '查看', 'data-toggle' => 'modal','class'=>'a_margin']);
                        },

                    ],

                ],
            ],
        ]); ?>
    </div>

</div>
