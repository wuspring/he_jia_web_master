<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FitupAttr */

$this->title = '编辑';
$this->params['breadcrumbs'][] = ['label' => '案例分类', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name,'url'=>'javascript:void(0)'];
$this->params['breadcrumbs'][] = '编辑';
?>
<div class="fitup-attr-update">

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
        'parent'=>$parent,
    ]) ?>

</div>
