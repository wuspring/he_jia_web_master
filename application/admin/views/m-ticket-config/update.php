<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MTicketConfig */

$this->title = '展会设置';
$this->params['breadcrumbs'][] = ['label' => '手机端', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '展会设置', 'url' => ['page/wap-index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .tab_title, .tab_title a {
        margin: 0;
        padding: 0;
        text-align: center;
    }

    .col-lg-2.tab_title.active>a {
        color: #fff!important;
        background-color: #1a7ab9;
    }
</style>
<div class="panel">

        <?= $this->render('_form', [ 'etitle' => $this->title,
            'model' => $model,
            'ticketId' => $ticketId,
        ]) ?>

</div>
<script>
    $('[data-id="tab-t"]').click(function () {
        $(this).addClass('active').siblings().removeClass('active');

        var v = $(this).attr('data-val');
        $('[data-id="tab"][data-val="' + v + '"]').show().siblings().hide();
    })
</script>
