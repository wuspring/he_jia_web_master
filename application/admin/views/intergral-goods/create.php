<?php

use yii\helpers\Html;
use DL\service\UrlService;

/* @var $this yii\web\View */
/* @var $model common\models\Goods */

$this->title = '发布商品';
$this->params['breadcrumbs'][] = ['label' => '积分商城', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '商品列表', 'url' => ['intergral-goods/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">

    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>