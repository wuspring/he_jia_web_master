<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\search\GoodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '商品列表';
$this->params['breadcrumbs'][] = ['label' => '积分商城', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?>
            <span style="float: right;text-align: right;">
        <?= Html::a('发布商品', ['create'], ['class' => 'btn btn-success']) ?>
    </span>

        </span>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>




    <div class="panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                array(
                    'format' => 'raw',
                    'attribute' => 'goods_image',
                    'label'=>'缩略图',
                    'value'=>function($filterModel){
                        $itm=$filterModel->goods_pic;
                        if ($itm){
                            return Html::img(yii::$app->request->hostInfo.$itm, ['width' =>30]);
                        }else{
                            return '无';
                        }

                    },
                ),
                array(
                    'attribute' => 'goods_name',
                    'label'=>'商品名称',
                    'value'=>function($filterModel){
                        return StringHelper::truncate($filterModel->goods_name, 20);

                    },
                ),
                array(
                    'attribute' => 'id',
                    'label'=>'商品ID',
                ),
                array(
                    'attribute' => 'gc_id',
                    'label'=>'商品分类',
                    'value'=>function($filterModel){
                        return $filterModel->gclass->name;

                    },
                ),
                [
                    'attribute' => 'goods_integral',
                    'label'=>'积分价格',
                ],
//                'goods_click',
//                'goods_salenum',
                array(
                    'format' => 'raw',
                    'attribute' => 'goods_state',
                    'label'=>'状态',
                    'value'=>function($filterModel){

                        switch ($filterModel->goods_state){
                            case 10:
                                return '<span class="label label-danger">商品违规</span>';
                                break;
                            case 0:
                                return '<span class="label">已下架</span> | <button type="button" class="btn  btn-success btn-xs upgoods" data-id="'.$filterModel->id.'">上架</button>';
                                break;
                            case 1:
                                return '<span class="label label-success">已上架</span> | <button type="button" class="btn btn-default btn-xs dngoods" data-id="'.$filterModel->id.'">下架</button>';
                                break;
                        }

                    },
                ),
                [
                    'attribute' => 'goods_hot',
                    'label'=>'积分首页展示',
                    'value' => function ($model) {
                        $dic = [
                            '0' => '不展示',
                            '1' => '展示',
                        ];

                        return isset($dic[$model->goods_hot]) ? $dic[$model->goods_hot] : '-';
                    }
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>'操作',
                    'template' => ' {update} {delete}',
                    'buttons'=>[
                        'view'=>function($url, $model, $key){
                            $url=yii::$app->request->hostInfo.'/admin.php?r=intergral-goods/view&id='.$model->id ;
                            return  Html::a('查看', $url, ['title' => '查看','data-toggle'=>'modal','class'=>'a_margin'] ) ;
                        },
                        'update' => function ($url, $model, $key) {
                            return  Html::a('更新', $url, ['title' => '更新','data-toggle'=>'modal','class'=>'a_margin'] ) ;
                        },
                        'delete'=> function ($url, $model, $key) {
                            return  Html::a('删除',$url, ['title' => '删除','data-toggle'=>'modal', 'data-id'=>$model->id,'class'=>'a_margin'] ) ;
                        },
                    ],

                ],
            ],
        ]); ?>
    </div>

</div>
<script type="text/javascript">
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";

    $(document).on("click",'.upgoods',function () {
        var id=$(this).attr('data-id');
        var upbut=this;

        $.post("<?php echo Url::toRoute(['intergral-goods/upgoods']);?>",{_csrf:_csrf,id:id},function (res) {
            if (res.code==1){
                var html='<span class="label label-success">已上架</span> | <button type="button" class="btn btn-default btn-xs dngoods" data-id="'+id+'">下架</button>'
                $(upbut).parent().html(html);
            }else{
                alert(res.mg);
            }
        },'json');

    });

    $(document).on("click",'.dngoods',function () {
        var id=$(this).attr('data-id');
        var dnbut=this;

        $.post("<?php echo Url::toRoute(['intergral-goods/dngoods']);?>",{_csrf:_csrf,id:id},function (res) {
            if (res.code==1){
                var html='<span class="label">已下架</span> | <button type="button" class="btn  btn-success btn-xs upgoods" data-id="'+id+'">上架</button>'
                $(dnbut).parent().html(html);
            }else{
                alert(res.mg);
            }
        },'json');

    });

    $(".fa-bitbucket").click(function(){
        if (confirm("确定要删除该分类么？")) {
            var del=$(this).parent('a');
            var _csrf =$("#_csrf").val();
            $.post("<?php echo Url::toRoute('intergral-goods/delete');?>"+"&id="+del.attr('data-id'),{_csrf:_csrf},function(data){
                if (data.status) {
                    window.location.reload();
                }else{
                    alert(data.msg);
                }

            },'json');
        }
    });
</script>