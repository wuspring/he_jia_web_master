<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\HelpType;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\HelpCont */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/ueditor/ueditor.config.js');
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/ueditor/ueditor.all.min.js');
$this->registerJsFile( Yii::$app->request->hostInfo.'/public/ueditor/lang/zh-cn/zh-cn.js');
?>
<?php
 $helpType=HelpType::findAll(['is_del'=>0]);
 $helpTypeMap=ArrayHelper::map($helpType,'id','type');
?>
<style>

    #helpcont-content {margin-top: 20px;padding:0;margin:20px 0;width:100%;height:auto;border: none;}
</style>

<?php $form = ActiveForm::begin([
    'options' => ['enctype' =>'multipart/form-data'],
    'fieldConfig' => [
        'inputOptions' => ['class' => 'form-control'],
        'labelOptions'=>['class'=>'col-sm-2 control-label'],
        'template' => "{label}<div class='col-sm-10'>{input}{hint}{error}</div>",],
]); ?>

<div class="panel-heading">
         <span class="panel-title">
             <?= Html::encode($this->title) ?>
             <div style="float: right;text-align: right">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '发布') : Yii::t('app', '保存修改'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
         </span>
</div>

<div class="help-cont-form">
    <div class="panel-body">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'keywords')->textarea(['rows' => 6])->label('关键字') ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6])->label('描述') ?>
    
    <?= $form->field($model, 'type_id')->dropDownList($helpTypeMap,['prompt'=>'请选择']) ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6])->label("内容") ?>
    <?= $form->field($model, 'is_show')->radioList(['0'=>'显示','1'=>'不显示']) ?>

    <?php ActiveForm::end(); ?>
    </div>
</div>
<script type="text/javascript">
    init.push(function () {
        var ue = UE.getEditor('helpcont-content',{
            'zIndex':9,
        });
    });
</script>