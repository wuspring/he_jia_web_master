<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HelpCont */

$this->title = '编辑';
$this->params['breadcrumbs'][] = ['label' => '帮助中心', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = ['label' => '信息列表', 'url' => ['help-cont/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
