<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\search\GoodsclassifySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '商品分类';
$this->params['breadcrumbs'][] = ['label' => '积分商城', 'url' => 'javascript:void(0);'];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    i.fa-plus, i.fa-pencil {
        margin-right: 20px;
    }
</style>
<link href="<?php echo Yii::$app->request->hostInfo;?>/public/css/tree.css" rel="stylesheet" type="text/css">
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?>
            <span style="float: right;text-align: right;">
        <a class="btn btn-success" href="<?php echo Url::toRoute('/intergral-goods-class/create');?>">添加顶级分类</a>
    </span>


        </span>
    </div>

    <div class="panel-body">
        <div class="tree well">
            <ul>
                <?php foreach ($tree as $key => $value):?>
                    <li>
                        <span><i class="<?php echo !empty($value['tree'])? 'fa fa-plus-square':'fa fa-leaf';?>"></i></span>
                        <a href="javascript:void(0);"  style="display: inline-block;width: 80px;overflow: hidden;height :20px; line-height: 20px;padding: 4px 0;"><?php echo $value['name'];?></a>
                        <a href="<?php echo Url::toRoute(['/intergral-goods-class/create','cid'=>$value['id']]);?>"><i class="fa fa-plus"></i></a>
                        <a href="<?php echo Url::toRoute(['/intergral-goods-class/update','id'=>$value['id']]);?>"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0);" class="delete"  ><i data="<?php echo $value['id'];?>" class="fa fa-trash-o"></i></a>
                        <?php if (!empty($value['tree'])): ?>
                            <ul>
                                <?php foreach ($value['tree'] as $k1 => $v1):?>
                                    <li style="display: none;">
                                        <span><i class="<?php echo !empty($v1['tree'])? 'fa fa-plus-square':'fa fa-leaf';?>"></i></span>
                                        <a href="javascript:void(0);"><?php echo $v1['name'];?></a>
                                        <a href="<?php echo Url::toRoute(['/intergral-goods-class/create','cid'=>$v1['id']]);?>"><i class="fa fa-plus"></i></a>
                                        <a href="<?php echo Url::toRoute(['/intergral-goods-class/update','id'=>$v1['id']]);?>"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0);" class="delete" ><i  data="<?php echo $v1['id'];?>" class="fa fa-trash-o"></i></a>
                                        <?php if (!empty($value['tree'])): ?>
                                            <ul>
                                                <?php foreach ($v1['tree'] as $k2 => $v2):?>
                                                    <li style="display: none;"><span><i class="<?php echo !empty($v2['tree'])? 'fa fa-minus-square':'fa fa-leaf';?>"></i></span>
                                                        <a href="javascript:void(0);" value="<?php echo $v2['id'];?>"><?php echo $v2['name'];?></a>
                                                        <a href="<?php echo Url::toRoute(['/intergral-goods-class/update','id'=>$v2['id']]);?>"><i class="fa fa-pencil"></i></a>
                                                        <a href="javascript:void(0);" class="delete" ><i data="<?php echo $v2['id'];?>" class="fa fa-trash-o"></i></a>
                                                    </li>
                                                <?php endforeach?>
                                            </ul>
                                        <?php endif ?>
                                    </li>
                                <?php endforeach?>
                            </ul>
                        <?php endif ?>
                    </li>
                <?php endforeach?>
            </ul>
        </div>
        <input type="hidden" value="<?php echo Yii::$app->getRequest()->getCsrfToken();?>" name="_csrf" id="_csrf">
    </div>
</div>
        <script type="text/javascript">
            init.push(function () {
                $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', '展开分类');

                $('.tree li.parent_li > span').on('click', function (e) {
                    var children = $(this).parent('li.parent_li').find(' > ul > li');
                    if (children.is(":visible")) {
                        children.hide();
                        $(this).attr('title', '展开分类').find(' > i').addClass('fa-plus-square').removeClass('fa-minus-square');
                    } else {
                        children.show();
                        $(this).attr('title', '折叠分类').find(' > i').addClass('fa-minus-square').removeClass('fa-plus-square');
                    }
                    e.stopPropagation();
                });

                $(".fa-trash-o").click(function(){
                    if (confirm("确定要删除该分类么？")) {
                        var del=$(this);
                        var _csrf =$("#_csrf").val();
                        $.post("<?php echo Url::toRoute('/intergral-goods-class/delete');?>"+"&id="+$(del).attr('data'),{_csrf:_csrf},function(data){
                            if (data.status) {
                                $(del).parent().parent().remove();
                                alert(data.mg);
                            }else{
                                alert(data.mg);
                            }

                        },'json');
                    }
                });
            });

        </script>