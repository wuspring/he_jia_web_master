<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Ticket */

$this->title = '编辑: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => '展会管理', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => Yii::$app->request->referrer];

$this->params['breadcrumbs'][] = '编辑';
?>
<div class="panel">


    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
