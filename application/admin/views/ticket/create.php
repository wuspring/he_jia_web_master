<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Ticket */

$this->title = '添加展会';
$this->params['breadcrumbs'][] = ['label' => '展会管理', 'url' => ['ticket/manage']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel">
    <?= $this->render('_form', [ 'etitle' => $this->title,
        'model' => $model,
    ]) ?>

</div>
