<?php

use yii\helpers\Html;
use yii\grid\GridView;
use DL\service\UrlService;
use \common\models\Ticket;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MoneySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '参展优惠劵';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            array(
                    'attribute'=>'goods_id',
                    'label'=> '举办日期',
                    'value'=>function($filterModel){
                        if ($filterModel->goods_id==-1){
                                 return '店铺通用劵';
                        }else{
                                 return $filterModel->goods->goods_name;
                        }
                    }
                ),
            array(
                'attribute'=>'valid_day',
                'label'=> '有效期'
            ),
            array(
                'attribute'=>'money',
                'label'=> '金额'
            ),
            array(
                'attribute'=>'describe',
                'label'=> '描述'
            ),
            array(
                'attribute'=>'condition',
                'label'=> '最低使用条件'
            ),
            array(
                'attribute'=>'create_time',
                'label'=> '添加时间'
            ),
        ],
    ]); ?>
    </div>
</div>

