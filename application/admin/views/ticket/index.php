<?php

use yii\helpers\Html;
use yii\grid\GridView;
use DL\service\UrlService;
use yii\widgets\LinkPager;
use common\models\Ticket;

/* @var $this yii\web\View */
/* @var $searchModel admin\search\TicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '展会设置';
$this->params['breadcrumbs'][] = ['label' => '展会管理', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>
    <div class="panel-body">
        <div class="panel">
            <?php if ($list) :?>
               <?php foreach ($list  as $k => $v): ?>
              <div class="panel-heading">
                   <span class="panel-title">
                       <?= $v->name; ?>
                       <span style="float: right">
                           <?php
                                if ($v->status) {
                                    $href = sprintf('ask-ticket/%s', str_replace('_', '-', strtolower($v->type)));
                                    echo Html::a('页面设置', [$href], ['class' => 'btn btn-primary']);
                                }
                           ?>
                           <a class="btn btn-default" href="<?= UrlService::build(['ticket/update', 'id'=>$v->id]); ?>">编辑</a>
                       </span>
                   </span>
               </div>
              <div class="panel-body">
                  <table class="table table-striped table-bordered detail-view">
                      <tbody>
                          <tr><th class="col-lg-3">类型</th><td><?= $v->typeInfo; ?></td></tr>
                          <tr><th class="col-lg-3">预定金额</th><td><?= $v->order_price; ?></td></tr>
                          <tr><th>状态</th><td><?= $v->statusInfo; ?></td></tr>
                          <tr><th>预约总量</th><td><?= $v->amount; ?></td></tr>
                          <tr><th>地址</th><td><?= $v->address; ?></td></tr>
                          <tr><th>添加时间</th><td><?= $v->create_time; ?></td></tr>
                      </tbody>
                  </table>
                  <table class="table">
                      <thead>
                            投放详情
                      </thead>
                      <tbody>
                      <tr>
                          <td class="col-md-3">投放城市</td>
                          <td class="col-md-3">城市编号</td>
                          <td class="col-md-3">预约数量</td>
                      </tr>
                      <?php foreach ($v->ticketInfor as $tk=>$tv):?>
                          <tr>
                              <td class="col-md-3"><?=$tv->cityName?></td>
                              <td class="col-md-3"><?=$tv->provinces_id?></td>
                              <td class="col-md-3"><?=$tv->ticket_amount?></td>
                          </tr>
                      <?php endforeach;?>
                      </tbody>
                  </table>

              </div>
               <?php endforeach;?>
               <?php endif; ?>
        </div>

        <?= LinkPager::widget(['pagination' => $pagination,]) ?>
    </div>
</div>