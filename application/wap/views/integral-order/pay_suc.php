<div class="head">
    <a href="javascript:history.back(-1);"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>兑换成功</h2>
</div>
<!--- 兑换成功 -->
<div id="success">

  <div class="tip">
    <div class="box">
      <img src="/public/wap/images/success_ico.png"/>
      <h2>兑换成功</h2>
      <p>去查看我的订单吧~</p>
      <div class="clear"></div>
    </div>
  </div> 
 
  <div class="operating">
    <a class="button1" href="<?= \DL\service\UrlService::build('integral/index') ?>">返回首页</a>
    <a class="button2" href="<?= \DL\service\UrlService::build(['member/integral-orderlist']); ?>">查看订单</a>
  </div>  

  <!--- 热门商品 -->
  <div class="pro_list">
    <div class="title">
      <h2>热门商品</h2>
      <a href="<?= \DL\service\UrlService::build('integral-menu/gift'); ?>">查看更多></a>
    </div>  
    <div class="cont">
      <ul>
          <?php foreach ($goods AS $good) :?>
        <li>
          <a href="<?= \DL\service\UrlService::build(['integral/goods-detail', 'id' => $good->id]); ?>">
            <img style="height: 150px" src="<?= $good->goods_pic; ?>"/>
            <p style="height: 36px;overflow:hidden;"><?= $good->goods_name; ?></p>
            <b><?= intval($good->goods_integral); ?>积分</b>
          </a>
        </li>
          <?php endforeach; ?>
        <div class="clear"></div>
      </ul> 

    </div>  

  </div>  
</div>
