
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>确认订单</h2>
</div>
<div class="casesure_address">
    <!-- 有无地址，二选一 -->

    <?php if ($order->receiver) :?>
    <a href="<?= \DL\service\UrlService::build(['address/address-list', 'number' => $order->orderid]); ?>" class="casesure_address_list">
        <img src="/public/wap/images/icon_right1.png" alt="" class="img_r">
        <img src="/public/wap/images/location.jpg" alt="" class="img_l">
        <div class="con">
            <p class="p1"><span><?= $order->receiver_name; ?></span><span><?= $order->receiver_mobile; ?></span></p>
            <p class="p2">收货地址：<span><?= "{$order->receiver_address}"; ?></span></p>
        </div>
    </a>
    <?php else :?>
        <a href="<?= \DL\service\UrlService::build(['address/address-list', 'number' => $order->orderid]); ?>" class="casesure_address_add btn btn_big">+ 添加收货地址</a>
    <?php endif; ?>
</div>
<?php foreach ($orderGoods AS $orderGood) :?>
<div class="casesure_product">
    <a class="casesure_product_con" href="<?= \DL\service\UrlService::build(['integral/goods-detail', 'id' => $orderGood->goods_id]); ?>">
        <img src="<?= $orderGood->goods_pic; ?>" alt="<?= $orderGood->goods_name; ?>">
        <p class="p_name ellipsis"><?= $orderGood->goods_name; ?></p>
        <p class="p_more"><?= $orderGood->attr; ?></p>
        <p class="p_price orange"><span class="s float-right">x <?= $orderGood->goods_num; ?></span><span><?= intval($orderGood->goods_price); ?></span>积分</p>
    </a>
</div>
<?php endforeach; ?>
<div class="casesure_remark">
    <dl>
        <dt>配送方式</dt>
        <dd>包邮</dd>
    </dl>
    <dl>
        <dt>配送时间</dt>
        <dd class="dd" id="casepaydate"><?= $order->expand; ?></dd>
    </dl>
</div>
<div class="case_btns">
    <div class="case_btns_con">
        <?php if ($order->receiver) :?>
        <a href="<?= \DL\service\UrlService::build(['integral-order/pay', 'number' => $order->orderid]); ?>" class="btn">提交订单</a>
        <?php else : ?>
        <a href="javascript:alert('请完善收货地址信息');" class="btn disabled">提交订单</a>
        <?php endif; ?>
        <p class="p">消费：<span class="s orange"><span><?= intval($order->order_amount); ?>积分</span></span></p>
    </div>
</div>

<script type="text/javascript">
    //配送时间选择
    var mobileSelect1 = new MobileSelect({
        trigger: '#casepaydate',
        title: '配送时间',
        wheels: [
            {data: <?= json_encode($expand); ?>}
        ],
        position:[2], //初始化定位
        callback:function(indexArr, data) {
            var p = {}
            p.number = '<?= $order->orderid; ?>';
            p.expand = data[0];

            $.post('<?= \DL\service\UrlService::build('integral-order/update'); ?>', p, function (res) {
                res = JSON.parse(res);

                if (res.status) {
                    return false;
                }

                alert(res.msg);
            });
        }
    });
</script>
