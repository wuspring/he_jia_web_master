<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/13
 * Time: 16:25
 */
use yii\helpers\Url;
?>

<!--- 我的积分 -->
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>我的积分</h2>
</div>
<div id="my_points">
    <div class="points_bg">
        <!--- 积分数量 -->
        <div class="quantity">
            <h3><?=$member->integral?></h3>
            <p>我的积分</p>
        </div>
        <!--- 导航 -->
        <div class="user_info">
            <div class="user_data">
                <ul>
                    <li>
                        <a href="<?=Url::toRoute(['integral-menu/integral-record'])?>">
                            <img src="/public/wap/images/my_points__nav_ico1.png"/>
                            <p>积分变动</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?=Url::toRoute(['integral-menu/integral-gain'])?>">
                            <img src="/public/wap/images/my_points__nav_ico2.png"/>
                            <p>积分赚取</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?=Url::toRoute(['integral-menu/integral-signin'])?>">
                            <img src="/public/wap/images/my_points__nav_ico3.png"/>
                            <p>积分签到</p>
                        </a>
                    </li>
                    <div class="clear"></div>
                </ul>
            </div>
        </div>
        <!--- 积分商城广告 -->
        <div class="slider carousel slide" data-ride="carousel">
            <!-- 指示符 -->
            <ul class="carousel-indicators">
                <li data-target="#slider" data-slide-to="0" class="active"></li>
                <li data-target="#slider" data-slide-to="1"></li>
            </ul>
            <!-- 轮播图片 -->
            <div class="carousel-inner">
               <?php foreach ($advertisingWheel as $k=>$item):?>
                    <a href="<?=$item->url?>" class="carousel-item <?=($k==0)?'active':''?>">
                        <img src="<?=$item->picture?>">
                    </a>
               <?php endforeach;?>
            </div>
            <!-- 左右翻页不需要但是删除会报错 -->
            <a id="carleft" class="left carousel-control" href="#slider" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a id="carright" class="right carousel-control" href="#slider" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <!--- 积分广告2等分 -->
        <div class="points_mall_gg2">
            <?php foreach ($leftAndRight as $k_pic=>$pic):?>
              <?php if ($k_pic==0):?>
                    <a href="<?=$pic->url?>"><img src="<?=$pic->picture?>"/></a>
               <?php else:?>
                    <a href="<?=$pic->url?>"><img src="<?=$pic->picture?>"/></a>
               <?php endif;?>
            <?php endforeach;?>
            <div class="clear"></div>
        </div>
    </div>

    <!--- 我能兑换 -->
    <div class="pro_list">
        <div class="title">
            <h2>我能兑换</h2>
            <a href="<?=Url::toRoute(['integral-menu/gift'])?>">查看更多></a>
        </div>
        <div class="cont">
            <ul>
                <?php foreach ($intergralGood as $k => $v):?>
                    <li>
                        <a href="<?=Url::toRoute(['integral/goods-detail','id'=>$v->id])?>">
                            <img style="height: 120px" src="<?=$v->goods_pic?$v->goods_pic:'/public/wap/images/loading.jpg'?>"/>
                            <p><?=$v->goods_name?></p>
                            <span><b><?= intval($v->goods_integral)?>积分</b></span>
                        </a>
                    </li>
                <?php endforeach;?>
                <div class="clear"></div>
            </ul>
        </div>
    </div>

</div>


<!-- 轮播图-手指滑动效果 -->
<script type="text/javascript">

    $(function () {
        // 获取手指在轮播图元素上的一个滑动方向（左右）
        // 获取界面上轮播图容器
        var $carousels = $('.carousel');
        var startX,endX;
        // 在滑动的一定范围内，才切换图片
        var offset = 50;
        // 注册滑动事件
        $carousels.on('touchstart',function (e) {
            // 手指触摸开始时记录一下手指所在的坐标x
            startX = e.originalEvent.touches[0].clientX;

        });
        $carousels.on('touchmove',function (e) {
            // 目的是：记录手指离开屏幕一瞬间的位置 ，用move事件重复赋值
            endX = e.originalEvent.touches[0].clientX;
        });
        $carousels.on('touchend',function (e) {
            //console.log(endX);
            //结束触摸一瞬间记录手指最后所在坐标x的位置 endX
            //比较endX与startX的大小，并获取每次运动的距离，当距离大于一定值时认为是有方向的变化
            var distance = Math.abs(startX - endX);
            if (distance > offset){
                //说明有方向的变化
                //根据获得的方向 判断是上一张还是下一张出现
                $(this).carousel(startX >endX ? 'next':'prev');
            }
        })
    });
</script> 
