<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/13
 * Time: 14:09
 */
use yii\helpers\Url;
?>

<link rel="stylesheet" type="text/css" href="/public/wap/css/prize.css">
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>积分抽奖</h2>
</div>
<div class="integral_luckdraw">
    <div class="luckdraw_top">
        <a href="javascript:void(0)" class="a_right" data-toggle="modal" data-target="#myModalTicket">活动规则</a>
        <p>您已拥有<span class="remainderNum"><?=$playNum?></span>次抽奖机会，点击立即抽奖！</p>
    </div>
    <div class="luckdraw_title">
        <h4>积分抽奖</h4>
        <h5>POINTS DRAW</h5>
    </div>
    <div class="luckdraw_con1" style="margin:0;">
        <!-- 大转盘效果，下面按钮只展示 -->
        <div class="lctc_left">
            <div class="g-content">
                <div class="g-lottery-case">
                    <div class="g-left">
                        <div class="g-lottery-box">
                            <div class="g-lottery-img">
                                <a class="playbtn" href="javascript:void (0);" title="开始抽奖"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="luckdraw_title">
        <h4>中奖名单</h4>
        <h5>LIST OF WINNERS</h5>
    </div>
    <div class="luckdraw_con2 ">
        <div class="luckdraw_notice">
            <ul class="list-unstyled">

                <?php foreach ($getPizeList as $k=>$item):?>
                    <li><p><span><?=substr_cut($item->member->mobile)?></span>获得<span><?=$item->intergralTurntable->prize?></span></p></li>
                <?php endforeach;?>

            </ul>
        </div>
    </div>
    <div class="luckdraw_title">
        <h4>本期奖品</h4>
        <h5>CURRENT PRIZE</h5>
    </div>
    <div class="luckdraw_con3">
        <?php foreach ($prize as $k=>$value):?>

            <p><b><?=$value->prize_grade?>：</b><?=$value->prize_name?></p>
        <?php endforeach;?>

    </div>

</div>

<!-- 模态框- 活动规则 弹框 -->
<div class="modal modal_ticket fade" id="myModalTicket">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">活动规则</p>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <?= \DL\vendor\ConfigService::init('INDEX_INTERGRAL_SHOP')->get('lottery_rules');?>
            </div>
        </div>
    </div>
</div>


<!-- 模态框- 抽奖结果 弹框 -->
<div class="modal modal_explain modal_explain_sm fade" id="myModalExplain2">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">抽奖结果</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align">恭喜您，您抽中了四等奖呦~</p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">确 定</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/public/index/js/jquery.rotate.min.js"></script>
<script type="text/javascript">
    //公告滚动
    noticeScroll();

    //公告滚动
    function noticeScroll(){
        $.fn.myScroll = function(options){
            var defaults = {
                speed:40,
                rowHeight:30
            };
            var opts = $.extend({}, defaults, options),intId = [];
            function marquee(obj, step){
                obj.find('ul').animate({
                    marginTop: '-=1'
                },0, function() {
                    var s = Math.abs(parseInt($(this).css("margin-top")));
                    if(s >= step){
                        $(this).find("li").slice(0, 1).appendTo($(this));
                        $(this).css("margin-top", 0);
                    }
                });
            };
            this.each(function(i){
                var sh = opts["rowHeight"],speed = opts["speed"],_this = $(this);
                intId[i] = setInterval(function(){
                    if(_this.find("ul").height()<=_this.height()){
                        clearInterval(intId[i]);
                    }else{
                        marquee(_this, sh);
                    }
                }, speed);

                _this.hover(function(){
                    clearInterval(intId[i]);
                },function(){
                    intId[i] = setInterval(function(){
                        if(_this.find("ul").height()<=_this.height()){
                            clearInterval(intId[i]);
                        }else{
                            marquee(_this, sh);
                        }
                    }, speed);
                });
            });
        };

        $(".luckdraw_notice").myScroll({
            speed:40,
            rowHeight:30
        });
    }


    //点击转盘 转
    $(".playbtn").on('click',function () {
        lottery();
    });


    function lottery() {
        $.ajax({
            type: 'POST',
            url: '<?=Url::toRoute(['integral-menu/doaward'])?>',
            dataType: 'json',
            cache: false,
            success: function (json) {
                if(json.code==1){
                    var a = json.angle; //角度
                    var p = json.prize; //奖项
                    $(".playbtn").rotate({
                        duration: 3000, //转动时间
                        angle: 0,
                        animateTo: 1800 + a, //转动角度
                        callback: function () {
                            $(".remainderNum").text(json.playNum);
                            $("#myModalExplain2").modal('show');
                            $(".text-align").text(json.prize);
                            // var con = confirm('当前积分还可以有' + json.prize + '机会。\n还要再来一次吗？');
                        }
                    });
                }else {
                    alert(json.msg, function () {
                        return false;
                    });

                    return false;
                }
            }
        });
    }
</script>