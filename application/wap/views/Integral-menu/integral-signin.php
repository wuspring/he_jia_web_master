<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/13
 * Time: 19:03
 */
use  yii\helpers\Url;
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>积分签到</h2>
</div>
<div class="signin_top">
    <a href="javascript:void(0)" class="signin_top_rule" data-toggle="modal" data-target="#myModalTicket">签到规则</a>
    <img src="/public/wap/images/img_signin_mrqd.png" alt="每日签到">
    <p>签到领积分 购物不花钱</p>
    <?php if (empty($qianDao)):?>
        <a href="javascript:void(0)" class="btn btn_big quQianDao">立即签到</a>
    <?php else:?>
        <a href="javascript:void(0)" class="btn btn_big disabled">签到成功</a>
    <?php endif;?>


    <!-- <a href="javascript:void(0)" class="btn btn_big disabled">签到成功</a> -->
</div>
<div class="signin_list">

    <div class="list_ajax">

    </div>
</div>


<!-- 模态框- 签到规则 弹框 -->
<div class="modal modal_ticket fade" id="myModalTicket">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">签到规则</p>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p><?=$model->explain?></p>
            </div>
        </div>
    </div>
</div>

<!-- 模态框- 签到成功 弹框 -->
<div class="modal modal_explain modal_explain_sm fade" id="myModalExplain2">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">签到成功</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align">您已经签到成功，连续签到积分翻倍呦</p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal" id="signinSure" >确 定</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    // 页数
    var page = 0;
    // 每页展示5个
    var size = 10;
    var sum='<?php echo $pages->totalCount;?>';
    var p= Math.ceil(sum/size);
    // dropload
    var result='';

    $('#pro_list').dropload({
        scrollArea : window,
        distance:10,
        loadDownFn : function(me){
            if (page<p) {
                page++;
                // 拼接HTML
                $.get('<?php echo Url::toRoute(["integral-menu/integral-signin"]);?>',{page:page,'per-page':size},function(data){
                    result = '';
                    $.each(data.list,function(key,val){

                        result += '<div class="con">\n' +
                            '        <span class="s_right float-right">+'+Math.abs(val.pay_integral)+'</span>\n' +
                            '        <p class="p1">签到</p>\n' +
                            '        <p class="p2">'+val.create_time+'</p>\n' +
                            '    </div>';
                    });
                    $(".list_ajax").append(result);
                    // 每次数据插入，必须重置
                    me.resetload();
                },'json');
            }else{
                // 锁定
                me.lock();
                // 无数据
                me.noData();
                // 为了测试，延迟1秒加载
                // 插入数据到页面，放到最后面
                $('.infolist').append(result);

                //settime();
                // 每次数据插入，必须重置
                me.resetload();
            }
        }
    });

    $('.signin_top .quQianDao').bind('click',function(){
        var url = "<?=Url::toRoute(['integral-menu/click-signin'])?>";

        $.get(url,function (res) {
            if (res.code== 1){
                $("#myModalExplain2").modal('show');
                $(this).addClass('disabled').html('签到成功');
            }else {
                alert('签到失败')
            }
        },'json');
    });

    //确定签到
    $("#signinSure").on('click',function () {
        $("#myModalExplain2").modal('hide');
        window.location.href="<?=Url::toRoute(['integral-menu/integral-signin'])?>"
    })

</script>

