<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/13
 * Time: 16:41
 */
use yii\helpers\Url;
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>积分变动</h2>
</div>


<!--- 积分变动 -->
<div id="treasure_change">

    <div class="operating">
        <span>明细筛选</span>
        <a class="cycle" href="javascript:void(0)" id="select_jftime">今日</a>
    </div>

    <div class="card_data">
        <h3><?=$member->integral?></h3>
        <p>总积分</p>
        <ul>
            <li class="clickType" tabType="in">获取积分：<span id="getIntergral">0</span></li>
            <li  class="clickType" tabType="out">使用积分：<span id="reduceIntegral">0</span></li>
        </ul>
    </div>

    <!--- 列表 -->
    <div id="accordion" class="list">

        <div class="list_ajax">

        </div>

    </div>

</div>

<script type="text/javascript">

    // 页数
    var page = 0;
    // 每页展示5个
    var size = 10;
    var sum = '<?php echo $pages->totalCount;?>';
    var p = Math.ceil(sum / size);
    var result = '';
    var dateType=1;
    var integralType='in';

    $(function () {
       getList();
    });
    function getList() {
        $('.list').dropload({
            scrollArea: window,
            distance: 10,
            loadDownFn: function (me) {
                if (page < p) {
                    page++;
                    // 拼接HTML
                    result = '';
                    $.get('<?php echo Url::toRoute(["integral-menu/integral-record"]);?>', {
                        page: page,
                        'per-page': size,
                        dateType:dateType,
                        integralType:integralType,
                    }, function (data) {
                        sum = data.pagecount;
                        p= Math.ceil(sum/size);
                        if (data.integralType == 'in'){
                            $.each(data.list, function (key, val) {
                                var url = "<?=Url::to(['fitup-school/fitup-case-detail','id'=>''])?>" + val.id;
                                result += '<div class="card">\n' +
                                    '    <div class="card-header" id="heading'+val.id+'">\n' +
                                    '      <h5 class="mb-0">\n' +
                                    '        <button class="collapsed" data-toggle="collapse" data-target="#collapse'+val.id+'" aria-expanded="false" aria-controls="collapse'+val.id+'">\n' +
                                    '          <h2>获取积分</h2>\n' +
                                    '          <p>'+val.create_time+'</p>\n' +
                                    '          <b>+'+Math.abs(val.pay_integral)+'</b>\n' +
                                    '        </button>\n' +
                                    '      </h5>\n' +
                                    '    </div>\n' +
                                    '    <div id="collapse'+val.id+'" class="collapse" aria-labelledby="heading'+val.id+'" data-parent="#accordion">\n' +
                                    '      <div class="card-body">\n';

                                        if(val.orderId == 'login'){
                                            result += '<ul>          <li>说明<span>'+val.expand+'</span></li>\n' +
                                                '          <li>赠送积分<span>'+Math.abs(val.pay_integral)+'</span></li>\n' +
                                                '        </ul>\n';
                                        }else {
                                            result += '<ul>        <li>说明<span>'+val.expand+'</span></li>\n'+
                                                '          <li>消费金额<span>￥'+Math.abs(val.pay_integral)+'</span></li>\n' +
                                                '          <li>订单号<span>'+val.orderId+'</span></li>\n' +
                                                '        </ul>\n';
                                        }

                                result +='      </div>\n' +
                                    '    </div>\n' +
                                    '  </div>'
                            });
                        } else {
                            $.each(data.list, function (key, val) {
                                result += '<div class="card">\n' +
                                    '    <div class="card-header" id="heading'+val.id+'">\n' +
                                    '      <h5 class="mb-0">\n' +
                                    '        <button class="collapsed" data-toggle="collapse" data-target="#collapse'+val.id+'" aria-expanded="false" aria-controls="collapse'+val.id+'">\n' +
                                    '          <h2>使用积分</h2>\n' +
                                    '          <p>'+val.create_time+'</p>\n' +
                                    '          <b>-'+val.pay_integral+'</b>\n' +
                                    '        </button>\n' +
                                    '      </h5>\n' +
                                    '    </div>\n' +
                                    '    <div id="collapse'+val.id+'" class="collapse" aria-labelledby="heading'+val.id+'" data-parent="#accordion">\n' +
                                    '      <div class="card-body">\n' +
                                    '        <ul>\n' +
                                    '          <li>说明<span>'+val.expand+'</span></li>\n' +
                                    '          <li>消费积分<span>-'+val.pay_integral+'</span></li>\n' +
                                    '          <li>订单号<span>'+val.orderId+'</span></li>\n' +
                                    '        </ul>\n' +
                                    '      </div>\n' +
                                    '    </div>\n' +
                                    '  </div>'
                            });
                        }
                        $('#getIntergral').html(data.getIntegral);
                        $('#reduceIntegral').html(data.reduceIntegral);

                        $('.list_ajax').append(result);
                        // 每次数据插入，必须重置
                        me.resetload();
                    }, 'json');
                } else {

                    // 锁定
                    me.lock();
                    // 无数据
                    me.noData();
                    // 为了测试，延迟1秒加载
                    // 插入数据到页面，放到最后面
                    $('.infolist').append(result);
                    //settime();
                    // 每次数据插入，必须重置
                    me.resetload();
                }
            }
        });
    }

    //日期
    var mobileSelect1 = new MobileSelect({
        trigger: '#select_jftime',
        title: '选择时间',
        wheels: [
            {data:[{'id':1,'value':'今日'},{'id':2,'value':'本周'},{'id':3,'value':'本月'}]}
        ],
        position:[0], //初始化定位
        callback:function(indexArr, data){
            // console.log(data);
            dateType=data[0]['id'];
            $(".list_ajax").children().remove();
            $(".dropload-down").remove();
            page = 0;
            p = 1;
            getList();
        }
    });

    //点击获取积分
    $(".clickType").on('click',function () {
        page = 0;
        p = 1;
        sum = '';
        integralType=$(this).attr('tabType');
        $(".list_ajax").children().remove();
        $(".dropload-down").remove();
        getList();
    })

</script>
