<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/13
 * Time: 15:03
 */

use wap\models\IntergralGoodsClass;
use yii\helpers\Url;

?>
<?php
 $gc_id_key = empty($get_gc_id)?0:$get_gc_id;
 $gc_id_obj = IntergralGoodsClass::findOne($gc_id_key);
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>
<!--- 我的积分 -->
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>积分列表</h2>
</div>
<div id="points_list">
    <!--- 积分列表banner -->
    <div class="points_list_banner">
        <img src="<?=$indexPic->picture?>"/>
    </div>
    <!--- 积分广告2等分 -->
    <div class="points_mall_gg2">
       <?php foreach ($leftAndRight as $k=>$item):?>
           <a href="<?=$item->url?>"><img src="<?=$item->picture?>"/></a>
        <?php endforeach;?>
        <div class="clear"></div>
    </div>

    <!--- 我能兑换 -->
    <div class="list">
        <div class="tab">
            <a href="javascript:void(0)" id="select_lp" class="a_more active" style="cursor: pointer;"><?=empty($gc_id_obj)?'礼品分类':$gc_id_obj->name?></a>
            <a href="javascript:void(0)" id="select_fw" class="a_more" style="cursor: pointer;">积分范围</a>
            <!--- 3种状态，默认为md,分值从高到低up,低到高down -->
            <a class="a_ud <?=$sort_str?>" tabSort="<?=$sort_str?>" href="javascript:void(0)" id="select_upDown">积分值</a>
        </div>
        <div class="cont" style="overflow:hidden">
            <ul style="overflow: hidden" class="cont_ajax">

            </ul>
        </div>
    </div>

</div>


<!-- 轮播图-手指滑动效果 -->
<script>
    // 页数
    var page = 0;
    // 每页展示6个
    var size = 6;
    var sum = '<?php echo $pages->totalCount;?>';
    var p = Math.ceil(sum / size);
    var result = '';
    var gc_id = '<?=$get_gc_id?>', fw_id = 0,up_id='md';

    $(function () {
        getList();
    });
    //礼品分类
    var mobileSelect = new MobileSelect({
        trigger: '#select_lp',
        title: '礼品类型',
        wheels: [
            {data:<?=$gc_id?>}
        ],
        position:[<?=empty($get_gc_id)?0:$get_gc_id-1?>], //初始化定位
        callback:function(indexArr, data){
            console.log(data);
            gc_id=data[0]['id'];
            $(".cont_ajax").children().remove();
            $(".dropload-down").remove();
            page = 0;
            p = 1;
            getList();
        }
    });

    // 范围
    var mobileSelect1 = new MobileSelect({
        trigger: '#select_fw',
        title: '积分范围',
        wheels: [
            {data:<?=$scope?>}
        ],
        position:[1], //初始化定位
        callback:function(indexArr, data){
            console.log(data);
            fw_id=data[0]['id'];
            $(".cont_ajax").children().remove();
            $(".dropload-down").remove();
            page = 0;
            p = 1;
            getList();
        }
    });
    //积分排序
    $("#select_upDown").on('click',function () {

        $(".cont_ajax").children().remove();
        $(".dropload-down").remove();
        var sort_str = $(this).attr('tabSort');
        if (sort_str == 'md'){
            up_id = 'up';
            $(this).removeClass('md');
            $(this).addClass('up');
            $(this).attr('tabSort','up')
        }
        if (sort_str == 'up'){
            up_id = 'down';
            $(this).removeClass('up');
            $(this).addClass('down');
            $(this).attr('tabSort','down')
        }
        if (sort_str == 'down'){
            up_id = 'up';
            $(this).removeClass('down');
            $(this).addClass('up');
            $(this).attr('tabSort','up')
        }
        page = 0;
        p = 1;
        getList();
    });
    function getList() {
        $('.cont').dropload({
            scrollArea: window,
            distance: 10,
            loadDownFn: function (me) {
                console.log(page,p);
                if (page < p) {
                    page++;
                    // 拼接HTML
                    result = '';
                    $.get('<?php echo Url::toRoute(["integral-menu/gift"]);?>', {
                        page: page,
                        'per-page': size,
                        gc_id:gc_id,
                        fw_id:fw_id,
                        up_id:up_id,
                    }, function (data) {
                        $.each(data.list, function (key, val) {
                            var url = "<?=Url::toRoute(['integral/goods-detail','id'=>''])?>" + val.id;
                            result += '<li>\n' +
                                '                    <a href="'+url+'">\n' +
                                '                        <img style="height: 170px" src="'+val.pic+'"/>\n' +
                                '                        <p style="height: 36px;overflow:hidden">'+val.name+'</p>\n' +
                                '                        <span><b>'+val.price+'积分</b></span>\n' +
                                '                    </a>\n' +
                                '                </li>';
                        });
                        $('.cont_ajax').append(result);
                        // 每次数据插入，必须重置
                        me.resetload();
                    }, 'json');
                } else {

                    // 锁定
                    me.lock();
                    // 无数据
                    me.noData();
                    // 为了测试，延迟1秒加载
                    // 插入数据到页面，放到最后面
                    $('.infolist').append(result);
                    //settime();
                    // 每次数据插入，必须重置
                    me.resetload();
                }
            }
        });
    }

</script>
