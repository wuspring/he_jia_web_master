<?php
use \wap\models\User;

function getUser($userId)
{
    $model = User::findOne(['id'=>$userId]);
    return $model?:(new User());
}

?>
<div class="header header_jia">
    <div class="header_city float-right" id="select_city"><?= $defaultCity->cname; ?></div>
</div>
<div class="slider carousel slide" data-ride="carousel">
    <!-- 指示符 -->
    <?php if ($ticketConfig) :?>
    <ul class="carousel-indicators">
        <?php $ticketConfig->banners = array_values($ticketConfig->banners);
        foreach ($ticketConfig->banners AS $index => $banner) :?>
        <li data-target="#slider" data-slide-to="<?= $index; ?>" class="<?= $index ? '' : 'active'; ?>"></li>
        <?php endforeach; ?>
    </ul>
    <!-- 轮播图片 -->
    <div class="carousel-inner">
        <?php foreach ($ticketConfig->banners AS $index => $banner) :?>
        <a href="<?= strlen($banner[0]) ? $banner[0] : ''; ?>" class="carousel-item <?= $index ? '' : 'active'; ?>">
            <img src="<?= strlen($banner[1]) ? $banner[1] : ''; ?>">
        </a>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
</div>

<ul class="list-unstyled column_menu_ul" style="background-color: <?= $ticketConfig ? $ticketConfig->colors : '';?>">
    <?php if ($ticketConfig) : foreach ($ticketConfig->top_guiders AS $guider) :?>
    <li><a href="<?= $guider['href']; ?>"><img src="<?= $guider['defaultIco']; ?>"><span class="text-truncate"><?= $guider['text']; ?></span></a></li>
    <?php endforeach;endif; ?>
</ul>


<?php if ($ticketConfig and $ticketConfig->ad_1) : $sectionDatas = array_values($ticketConfig->ad_1); $secKey = 'ad1'; ?>
<?php if (count($sectionDatas) > 1) :?>
        <div class="slider carousel slide" data-ride="carousel">
            <!-- 指示符 -->
            <ul class="carousel-indicators">
                <?php foreach ($sectionDatas AS $index => $sectionData) :?>
                    <li data-target="#slider<?= $secKey; ?>" data-slide-to="<?= $index; ?>" class="<?= $index ? '' : 'active'; ?>"></li>
                <?php endforeach; ?>
            </ul>
            <!-- 轮播图片 -->
            <div class="carousel-inner">
                <?php foreach ($sectionDatas AS $index => $sectionData) :?>
                <a href="<?= $sectionData[0]; ?>" class="carousel-item <?= $index ? '' : 'active'; ?>">
                    <img src="<?= $sectionData[1]; ?>">
                </a>
                <?php endforeach; ?>
            </div>
        </div>
<?php else :?>
<div class="com_ad2">
    <?php foreach ($sectionDatas AS $index => $sectionData) :?>
        <a href="<?= $sectionData[0]; ?>">
            <img data-url="<?= $sectionData[1]; ?>" class="loading_img" src="<?= $sectionData[1]; ?>>
        </a>
    <?php endforeach; ?>
</div>
<?php endif; ?>
<?php endif; ?>


<div class="column_form">
    <div class="com_title">
        <div class="com_title_con">
            <a href="<?= \DL\service\UrlService::build(['ask', 'ticketId' => $ticketId]); ?>">活动详情></a>
            <span class="s_title"><span>家博会</span>免费索票</span>
        </div>
    </div>
    <div class="column_form_con">
        <div class="cfc_more">
            <dl>
                <dt>活动时间：</dt>
                <dd><?= date('Y年m月d日', strtotime($ticket->open_date)); ?>-<?= date('m月d日', strtotime($ticket->end_date)); ?></dd>
            </dl>
            <dl>
                <dt>活动地址：</dt>
                <dd><?= $ticket->open_place; ?>（<?= $ticket->address; ?>）</dd>
            </dl>
        </div>
        <form data-id="join" action="<?= \DL\service\UrlService::build('jz/ask-ticket'); ?>" method="post" class="cfc_form">
            <div class="input-group">
                <div class="input-group-prepend">
                    <img src="/public/wap/images/icon_input_phone.png">
                </div>
                <input type="number" class="form-control" name="mobile" placeholder="请输入您的手机号">
            </div>
            <div class="input-group input_yzm">
                <div class="input-group-prepend">
                    <img src="/public/wap/images/icon_input_yzm.png">
                </div>
                <input type="number" onkeyup="if(value.length>6)value=value.slice(0,6)" class="form-control" placeholder="请输入验证码" name="code">
                <div class="input-group-append">
                    <a href="javascript:void(0)" data-id="getCode1" class="btn btn_yzm">获取验证码</a>
                </div>
            </div>
            <input type="hidden" name="ticketId" value="<?= $ticket->id; ?>">
            <input type="hidden" name="cityId" value="<?= $ticket->citys; ?>">
            <input type="hidden" name="type" value="<?= $ticket->type; ?>">

            <button type="submit" data-id="submit" class="btn btn_big">立即索票</button>
            <p class="p_more"><b>温馨提示：</b>请详细填写以上消息，我们将免费为您快递一份价值<?= $ticket->ticket_price; ?>元的门票展会资料</p>
        </form>

    </div>
</div>

<?php if ($ticketConfig and $ticketConfig->ad_2) : $sectionDatas = array_values($ticketConfig->ad_2); $secKey = 'ad2'; ?>
    <?php if (count($sectionDatas) > 1) :?>
        <div class="slider carousel slide" data-ride="carousel">
            <!-- 指示符 -->
            <ul class="carousel-indicators">
                <?php foreach ($sectionDatas AS $index => $sectionData) :?>
                    <li data-target="#slider<?= $secKey; ?>" data-slide-to="<?= $index; ?>" class="<?= $index ? '' : 'active'; ?>"></li>
                <?php endforeach; ?>
            </ul>
            <!-- 轮播图片 -->
            <div class="carousel-inner">
                <?php foreach ($sectionDatas AS $index => $sectionData) :?>
                    <a href="<?= $sectionData[0]; ?>" class="carousel-item <?= $index ? '' : 'active'; ?>">
                        <img src="<?= $sectionData[1]; ?>">
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    <?php else :?>
        <div class="com_ad2">
            <?php foreach ($sectionDatas AS $index => $sectionData) :?>
                <a href="<?= $sectionData[0]; ?>">
                    <img data-url="<?= $sectionData[1]; ?>" class="loading_img" src="<?= $sectionData[1]; ?>">
                </a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
<?php endif; ?>

<!-- 预存享特价/爆款预约/参展品牌 -->
<div class="index_prestore">
    <ul class="nav nav-pills nav-justified index_prestore_tab"  role="tablist">
        <li class="nav-item"><a href="#prestore_tab1" data-toggle="tab" class="nav-link active"><img src="/public/wap/images/icon_tab_prestore.png" alt="预存享特价"><span>预存享特价</span></a></li>
        <li class="nav-item"><a href="#prestore_tab2" data-toggle="tab" class="nav-link"><img src="/public/wap/images/icon_tab_boutique.png" alt="爆款预约"><span>爆款预约</span></a></li>
        <li class="nav-item"><a href="#prestore_tab3" data-toggle="tab" class="nav-link"><img src="/public/wap/images/icon_tab_brand.png" alt="参展品牌"><span>参展品牌</span></a></li>
    </ul>
    <div class="tab-content index_prestore_list">
        <!-- 预存享特价 -->
        <div class="tab-pane active" id="prestore_tab1">
            <ul class="list-unstyled clearfix com_productlist com_productlistone">
                <?php foreach ($teJiaGoods AS $goodInfo) :?>
                <li>
                    <a href="<?= \DL\service\UrlService::build(['goods/detail','city_id'=>$defaultCity->id, 'id' => $goodInfo->id]); ?>" class="product">
                        <span class="product_img float-left">
                            <img src="<?= $goodInfo->goods_pic; ?>" class="loading_img" data-url="<?= $goodInfo->goods_pic; ?>" alt="<?= $goodInfo->goods_name; ?>"></span>
                        <div class="product_con">
                            <p class="product_name text-truncate"><?= $goodInfo->goods_name; ?></p>
                            <p class="product_price glay">专享价<span><?= $goodInfo->goods_price; ?></span>/<span><?=$goodInfo->unit;?></span><span class="s"><span><?= $goodInfo->goods_marketprice; ?></span>/<span><?=$goodInfo->unit;?></span></span></p>
                            <p class="product_tag orange"><img src="/public/wap/images/icon_time_orange.png">仅剩<span><?=$goodInfo->goods_storage;?></span><span>件</span></p>
                            <p class="product_handsel">订金：<span class="orange">￥<span><?= $ticket->order_price; ?></span></span></p>
                        </div>
                    </a>
                    <a href="javascript:void(0)" class="btn float-right" data-id="order-create" data-ticket="<?= $ticket->id; ?>" data-good="<?= $goodInfo->id; ?>">立即预存</a>
                </li>
                <?php endforeach; ?>

            </ul>
            <div class="com_morebtn">
                <a href="<?= \DL\service\UrlService::build(['goods/lists', 'type' => 'ORDER']); ?>" class="btn">查看更多<img src="/public/wap/images/icon_links.png" alt="more"></a>
            </div>
        </div>
        <!-- 爆款预约 -->
        <div class="tab-pane" id="prestore_tab2">
            <ul class="list-unstyled clearfix com_productlist">
                <?php foreach ($baoKuanGood AS $goodInfo) :?>
                <li>
                    <a href="javascript:void(0)" data-id="get-coupon" data-ticket="<?= $ticket->id; ?>" data-good="<?= $goodInfo->id;?>" data-href="<?= \DL\service\UrlService::build(['goods/detail','city_id'=>$defaultCity->id, 'id' => $goodInfo->id]); ?>" class="product">
                        <span class="product_img"><img src="<?= $goodInfo->goods_pic; ?>" class="loading_img" data-url="<?= $goodInfo->goods_pic; ?>" alt="<?= $goodInfo->goods_name; ?>"></span>
                        <p class="product_name text-truncate"><?= $goodInfo->goods_name; ?></p>
                        <p class="product_tag orange float-right"><img src="/public/wap/images/icon_time_orange.png">仅剩<span><?=$goodInfo->goods_storage;?></span><span>件</span></p>
                        <p class="product_logo"><img src="<?=getUser($goodInfo->user_id)->avatarTm?>"></p>
                        <p class="product_price orange">￥<span><?= $goodInfo->goods_price; ?></span><span class="s1">/<?= $goodInfo->unit; ?></span></p>
                    </a>
                </li>
                <?php endforeach; ?>
            </ul>
            <div class="com_morebtn">
                <a href="<?= \DL\service\UrlService::build(['goods/lists', 'type' => 'COUPON']); ?>" class="btn">查看更多<img src="/public/wap/images/icon_links.png" alt="more"></a>
            </div>
        </div>

        <!-- 爆款预约 -->
        <div class="tab-pane" id="prestore_tab3">
            <ul class="list-unstyled clearfix com_brandlist">
                <?php foreach ($storeGcScores AS $storeGcScore) :?>
                <li style="display: none"><a href="<?php \DL\service\UrlService::build(['shop/detail','city_id'=>$defaultCity->id, 'id' => $storeGcScore->user_id]); ?>"><img src="<?= getUser($storeGcScore->user_id)->avatarTm; ?>" alt=""></a></li>
                    <li data-id="order-store" data-ticket="<?= $ticket->id; ?>" data-store="<?= $storeGcScore->user_id; ?>"><a href="javascript:void(0)"><img src="<?= getUser($storeGcScore->user_id)->avatarTm; ?>" alt=""></a></li>
                <?php endforeach; ?>
            </ul>
            <div class="com_morebtn">
                <a href="<?= \DL\service\UrlService::build(['shop/brand']); ?>" class="btn">查看更多<img src="/public/wap/images/icon_links.png" alt="more"></a>
            </div>
        </div>

    </div>
</div>


<?php if ($ticketConfig and $ticketConfig->ad_3) : $sectionDatas = array_values($ticketConfig->ad_3); $secKey = 'ad3'; ?>
    <?php if (count($sectionDatas) > 1) :?>
        <div class="slider carousel slide" data-ride="carousel">
            <!-- 指示符 -->
            <ul class="carousel-indicators">
                <?php foreach ($sectionDatas AS $index => $sectionData) :?>
                    <li data-target="#slider<?= $secKey; ?>" data-slide-to="<?= $index; ?>" class="<?= $index ? '' : 'active'; ?>"></li>
                <?php endforeach; ?>
            </ul>
            <!-- 轮播图片 -->
            <div class="carousel-inner">
                <?php foreach ($sectionDatas AS $index => $sectionData) :?>
                    <a href="<?= $sectionData[0]; ?>" class="carousel-item <?= $index ? '' : 'active'; ?>">
                        <img src="<?= $sectionData[1]; ?>">
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    <?php else :?>
        <div class="com_ad2">
            <?php foreach ($sectionDatas AS $index => $sectionData) :?>
                <a href="<?= $sectionData[0]; ?>">
                    <img data-url="<?= $sectionData[1]; ?>" class="loading_img" src="<?= $sectionData[1]; ?>">
                </a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
<?php endif; ?>


<div class="column_fitup">
    <div class="com_title">
        <div class="com_title_con">
            <a href="<?=\yii\helpers\Url::toRoute(['fitup-school/fitup-case'])?>">更多></a>
            <span class="s_title">装修效果图</span>
        </div>
    </div>
    <ul class="column_fitup_ul list-unstyled">
        <?php foreach ($storeCases AS $index => $storeCase) :?>
        <li class="li<?= $index+1; ?>"><a href="<?= \DL\service\UrlService::build(['fitup-school/fitup-case-detail', 'id' => $storeCase->id]); ?>" style="background-image: url(<?= $storeCase->cover_pic; ?>);"></a></li>
        <?php endforeach; ?>
    </ul>

</div>

<?php if ($ticketConfig and $ticketConfig->ad_4) : $sectionDatas = array_values($ticketConfig->ad_4); $secKey = 'ad4'; ?>
    <?php if (count($sectionDatas) > 1) :?>
        <div class="slider carousel slide" data-ride="carousel">
            <!-- 指示符 -->
            <ul class="carousel-indicators">
                <?php foreach ($sectionDatas AS $index => $sectionData) :?>
                    <li data-target="#slider<?= $secKey; ?>" data-slide-to="<?= $index; ?>" class="<?= $index ? '' : 'active'; ?>"></li>
                <?php endforeach; ?>
            </ul>
            <!-- 轮播图片 -->
            <div class="carousel-inner">
                <?php foreach ($sectionDatas AS $index => $sectionData) :?>
                    <a href="<?= $sectionData[0]; ?>" class="carousel-item <?= $index ? '' : 'active'; ?>">
                        <img src="<?= $sectionData[1]; ?>">
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    <?php else :?>
        <div class="com_ad2">
            <?php foreach ($sectionDatas AS $index => $sectionData) :?>
                <a href="<?= $sectionData[0]; ?>">
                    <img data-url="<?= $sectionData[1]; ?>" class="loading_img" src="<?= $sectionData[1]; ?>">
                </a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
<?php endif; ?>


<!-- 和家资讯 -->
<div class="index_news">
    <div class="com_title">
        <div class="com_title_con">
            <a href="<?= \DL\service\UrlService::build(['news/fitup-news-list', 'fid' => 1]); ?>">更多></a>
            <span class="s_title">和家资讯</span>
        </div>
    </div>
    <div class="index_news_list">
        <?php foreach ($newes AS $news) :?>
        <a href="<?= \DL\service\UrlService::build(['news/news-detail', 'id' => $news->id]); ?>" class="inl_con">
            <img src="<?= $news->themeImg; ?>" class="img loading_img" data-url="<?= $news->themeImg; ?>">
            <div class="con">
                <p class="p_name ellipsis"><?= $news->title; ?></p>
                <p class="p_time">
                    <span class="s1"><?= $news->newsclassify->name; ?></span>
                    <span class="s2"><?= date('Y-m-d', strtotime($news->createTime)); ?></span></p>
            </div>
        </a>
        <?php endforeach; ?>
    </div>
</div>

<!-- 底部承诺 -->
<?php if(!empty($serviceList)):?>
<div class="promise">
    <ul class="promise_ul list-unstyled clearfix">
        <?php foreach ($serviceList as $key=>$item):?>
            <li>
                <img src="<?=$item->icon?>">
                <p class="p1 green"><?=$item->name?></p>
                <p class="p2"><?=$item->desc?></p>
            </li>
        <?php endforeach;?>
    </ul>
</div>
<?php endif;?>
<?php include __DIR__ . '/common/footer.php'; ?>

<div class="modal modal_explain modal_explain_sm fade show" id="myModalExplain23" style="padding-right: 17px; display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">活动详情</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align"><?= $ticket->notice_words; ?></p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">知道了</button>
            </div>
        </div>
    </div>
</div>

<!-- 轮播图-手指滑动效果 -->
<script type="text/javascript">
    $('[data-id="ticket-detail"]').click(function () {
        $('#myModalExplain23').modal();
    })

    $(function () {
        // 获取手指在轮播图元素上的一个滑动方向（左右）
        // 获取界面上轮播图容器
        var $carousels = $('.carousel');
        var startX,endX;
        // 在滑动的一定范围内，才切换图片
        var offset = 50;
        // 注册滑动事件
        $carousels.on('touchstart',function (e) {
            // 手指触摸开始时记录一下手指所在的坐标x
            startX = e.originalEvent.touches[0].clientX;
        });
        $carousels.on('touchmove',function (e) {
            // 目的是：记录手指离开屏幕一瞬间的位置 ，用move事件重复赋值
            endX = e.originalEvent.touches[0].clientX;
        });
        $carousels.on('touchend',function (e) {
            //console.log(endX);
            //结束触摸一瞬间记录手指最后所在坐标x的位置 endX
            //比较endX与startX的大小，并获取每次运动的距离，当距离大于一定值时认为是有方向的变化
            var distance = Math.abs(startX - endX);
            if (distance > offset){
                //说明有方向的变化
                //根据获得的方向 判断是上一张还是下一张出现
                $(this).carousel(startX >endX ? 'next':'prev');
            }
        })
    });
</script>
<script>
    $('[data-id="submit"]').click(function () {
        var name = $('input[name="name"]'),
            mobile = $('input[name="mobile"]'),
            address = $('input[name="address"]'),
            code = $('input[name="code"]');

        if (code.val().length < 1) {
            alert("请输入验证码");
            return false;
        }

        if (!/^1\d{10}$/.test(mobile.val())) {
            console.log(mobile.attr('data-default'));
            if (!/^1\d{10}$/.test(mobile.attr('data-default'))) {
                alert("请输入正确的手机号码");
                return false;
            }

            mobile.val(mobile.attr('data-default'));
        }

        $('form[data-id="join"]').submit();
    })

    var mobileDom = $('input[name="mobile"]'),
        codeDom = $('a[data-id="getCode1"]'),
        getCodeCycle = 60;

    codeDom.click(function () {
        var mobile = mobileDom.val();
        if (!/^1\d{10}$/.test(mobile)) {
            alert("请输入正确的手机号");
            return false;
        }

        if (!codeDom.hasClass('disabled')) {
            $.post(
                GET_MSG_CODE,
                {mobile : mobile},
                function (res) {
                    res = JSON.parse(res);
                    if (res.status) {
                        if (codeDom.hasClass('disabled')) {
                            return false;
                        }

                        codeDom.attr('data-cycle', getCodeCycle).addClass('disabled');

                        var cycleClock = setInterval(function () {
                            var cycle = parseInt(codeDom.attr('data-cycle'));
                            if (cycle > 0) {
                                codeDom.text("" + cycle +"s").attr('data-cycle', cycle-1);
                            } else {
                                codeDom.text('验证码').removeClass('disabled');
                                clearInterval(cycleClock);
                            }
                        }, 1000);
                    }
                }
            )
        }
    });
</script>