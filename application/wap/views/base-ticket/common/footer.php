


<!--side-->
<?php

global $secretCityId;
$ms = \DL\Project\Page::init()->get($secretCityId, \DL\Project\Page::PAGE_INDEX);

$icon = [];
if ($ms) {
    $icon = isset($ms->icon1) && strlen($ms->icon1) ? json_decode($ms->icon1, true) : [];
    if ($icon) {
        $icon = explode('@', reset($icon));
        $icon[1] = translateAbsolutePath($icon[1]);
    }
}

?>
<?php if ($icon) :?>
<div class="right-slide">
    <ul class="right-slide-in">
        <li class="border-ridus">
            <a class="external" href="<?= $icon[0] ?>">
                <img src="<?= $icon[1]; ?>">
            </a>
        </li>
    </ul>
</div>
<?php endif; ?>
<!--side end-->
<div class="return-top" style=""></div>
<?php if ($ticketConfig and $ticketConfig->guiders) :
    $requestHref = trim($_SERVER['REQUEST_URI'], '/');
    ?>
    <div class="footer_menu menu_5">
        <ul class="list-unstyled">
            <?php foreach ($ticketConfig->guiders AS $guider) :?>
                <li class="<?= $requestHref == trim($guider['href'], '/') ? 'active' : ''; ?>">
                    <a href="<?= $guider['href']; ?>">
                        <img src="<?= $guider['defaultIco']; ?>" class="img">
                        <img src="<?= $guider['selectIco']; ?>" class="img_h">
                        <span><?= $guider['text']; ?></span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>