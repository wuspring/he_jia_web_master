<?php

function explodeAmountString($string)
{
    $result = '';
    $len = 0;
    while ($len < strlen($string)) {
        $result .= sprintf('<span>%s</span>', mb_strcut($string, $len, 1));
        $len++;
    }

    return $result;
}

?>
<div class="header" style="position: fixed;top: 0;width: 100%;z-index: 999;">
    <div class="header_city float-right" id="select_city"
         style="cursor: pointer;z-index: 9999;"><?= $defaultCity->cname ?></div>
</div>
<div class="head" style="top: 0;height: 45px;width: 100%;">
    <h2 style="z-index: 9998;position: fixed;text-align: center;margin: 0 auto;padding-left: 40%;">免费索票</h2>
</div>

<?php if (count($ticketConfig->spbanners) > 1) : $ticketConfig->spbanners = array_values($ticketConfig->spbanners); ?>

    <!-- 大于1张 -->
    <div class="slider carousel slide" style="" data-ride="carousel">
        <!-- 指示符 -->
        <ul class="carousel-indicators" style="bottom: 40px;">
            <?php foreach ($ticketConfig->spbanners AS $index => $banner) : ?>
                <li data-target="#slider" data-slide-to="<?= $index ?>" class="<?= $index ? '' : 'active'; ?>"></li>
            <?php endforeach; ?>
        </ul>
        <!-- 轮播图片 -->
        <div class="carousel-inner">
            <?php foreach ($ticketConfig->spbanners AS $index => $banner) : ?>
                <a href="<?= strlen($banner[0]) ? $banner[0] : 'javascript:void(0)'; ?>"
                   class="carousel-item <?= $index ? '' : 'active'; ?>">
                    <img src="<?= $banner[1]; ?>">
                </a>
            <?php endforeach; ?>
        </div>
        <!-- 左右翻页不需要但是删除会报错 -->
        <a id="carleft" class="left carousel-control" href="#slider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a id="carright" class="right carousel-control" href="#slider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
<?php else : ?>
    <!-- 如果只有一张 -->
    <?php foreach ($ticketConfig->spbanners AS $banner) : ?>
        <div class="com_ad2" style="">
            <a href="<?= strlen($banner[0]) ? $banner[0] : 'javascript:void(0)'; ?>"><img src="<?= $banner[1]; ?>"
                                                                                          alt=""></a>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<div class="explosion_form"
     style="background-image: url(<?= $ticketConfig->spbg_1 ? $ticketConfig->spbg_1[0] : ''; ?>);">
    <p class="p_num orange">已有<?= explodeAmountString($askAmount); ?>人索票</p>
    <div class="text-center explosion_notice" style="height: 20px;">
        <ul class="list-unstyled">
            <?php foreach ($askLists AS $askList) : ?>
                <li><p><span><?= substr_cut($askList->mobile); ?></span>已在平台索票成功</p></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <form data-id="join" action="<?= \DL\service\UrlService::build('jz/ask-ticket'); ?>" method="post" class="cfc_form" onsubmit="return false">
        <div class="input-group">
            <div class="input-group-prepend">
                <img src="/public/wap/images/icon_input_name.png">
            </div>
            <input type="text" class="form-control" name="name" placeholder="请输入您的姓名">
        </div>
        <div class="input-group">
            <div class="input-group-prepend">
                <img src="/public/wap/images/icon_input_phone.png">
            </div>
            <input type="number" class="form-control" placeholder="请输入您的手机号" name="mobile">
        </div>
        <div class="input-group input_yzm">
            <div class="input-group-prepend">
                <img src="/public/wap/images/icon_input_yzm.png">
            </div>
            <input type="number" onkeyup="if(value.length>6)value=value.slice(0,6)" class="form-control" placeholder="请输入验证码" name="code">
            <div class="input-group-append">
                <a href="javascript:void(0)" data-id="getCode1" class="btn btn_yzm">获取验证码</a>
            </div>
        </div>

        <input type="hidden" name="ticketId" value="<?= $ticketId; ?>">
        <?php if ($ticket) : ?>
            <input type="hidden" name="cityId" value="<?= $ticket->citys; ?>">
            <input type="hidden" name="type" value="<?= $ticket->type; ?>">
        <?php endif; ?>
        <button type="button" data-id="submit" class="btn btn_big">立即索票</button>
        <?php if ($ticket) : ?>
            <p class="p_more">
                <a href="javascript:void(0)" data-id="ticket-detail" class="orange float-right">索票须知</a>
                价值<?= intval($ticket->ticket_price); ?>元的门票展会资料免费到家</p>
        <?php endif; ?>
    </form>

</div>
<div class="explosion_ad"
     style="background-color: <?= $ticketConfig->spbg_color; ?>;background-image: url(<?= $ticketConfig->spbg_2 ? $ticketConfig->spbg_2[0] : ''; ?>);">
    <div class="explosion_title">
        <img src="/public/wap/images/bg_title_exp.png">
        <span>家博会亮点</span>
        <img src="/public/wap/images/bg_title_exp.png">
    </div>
    <ul class="explosion_ad_list list-unstyled clearfix">

        <?php foreach ($ticketConfig->star AS $star) :
            if (strlen($star[0])) :?>
                <li><a href="<?= $star[0]; ?>"><img src="<?= $star[1]; ?>" alt="<?= $star[3]; ?>"></a></li>
            <?php else : ?>
                <li><a href="javascript:void(0);" data-id="shadowNotice" data-title="<?= $star[3]; ?>"
                       data-data="<?= $star[2]; ?>"><img src="<?= $star[1]; ?>" alt="<?= $star[3]; ?>"></a></li>
            <?php endif;endforeach; ?>
    </ul>
    <?php if ($goodCoupons) : ?>
        <div class="explosion_title">
            <img src="/public/wap/images/bg_title_exp.png">
            <span>优惠劵</span>
            <img src="/public/wap/images/bg_title_exp.png">
        </div>
        <div class="com_coupon">
            <?php foreach ($goodCoupons AS $goodCoupon) : ?>
                <div class="com_coupon_con">
                    <a href="javascript:void(0)" data-id="store-coupon" data-coupon="<?= $goodCoupon->id; ?>"
                       data-ticket="<?= $ticketId; ?>" class="float-right a">立即<br>领取</a>
                    <img src="<?= $goodCoupon->user->avatar; ?>" alt="<?= $goodCoupon->user->nickname; ?>" class="img">
                    <div class="con">
                        <p class="p1 orange">￥<span><?= $goodCoupon->money; ?></span>优惠劵</p>
                        <p class="p2 text-truncate"><?= $goodCoupon->describe; ?></p>
                        <!--						<p class="p3 text-truncate">(有效期:2019.05.01-05.03)</p>-->
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="com_morebtn">
                <a href="<?= \DL\service\UrlService::build(['coupon/index', 'ticketId' => $ticketId]); ?> "
                   class="btn bg_white">查看更多<img src="/public/wap/images/icon_links.png" alt="more"></a>
            </div>
        </div>
    <?php endif; ?>
</div>

<?php if ($ticket && (!empty($teJiaGoods) || !empty($baoKuanGood))) : ?>
    <div class="explosion_prestore"
         style="background-image: url(<?= $ticketConfig->spbg_3 ? $ticketConfig->spbg_3[0] : ''; ?>);">
        <?php if (!empty($teJiaGoods)): ?>
            <div class="explosion_title2">
                <a href="javascript:void(0)" class="btn" data-id="reload"
                   data-type="<?= \common\models\TicketApplyGoods::TYPE_ORDER; ?>"
                   data-ticket="<?= $ticketId; ?>">换一批</a>
                <img src="/public/wap/images/bg_title_exp2.png">
                <span class="orange">预存享特价</span>
            </div>
            <ul class="list-unstyled clearfix com_productlist com_productlisttwo pt-2"
                id="<?= \common\models\TicketApplyGoods::TYPE_ORDER . $ticketId; ?>">
                <?php foreach ($teJiaGoods as $tk => $tv): ?>
                    <li>
                        <a href="<?= \DL\service\UrlService::build(['goods/detail', 'id' => $tv->id]); ?>"
                           class="product">
                        <span class="product_img"><img src="<?= $tv->goods_pic ?>" class="loading_img"
                                                       data-url="<?= $tv->goods_pic ?>"
                                                       alt="<?= $tv->goods_name; ?>"></span>
                            <p class="product_name text-truncate"><?= $tv->goods_name; ?></p>
                            <p class="product_handsel">订金：<span
                                        class="orange">￥<span><?= $ticket->order_price; ?></span></span></p>
                            <p class="product_price">
                                专享价：￥<span><?= $tv->goods_price ?></span>/<span><?= $tv->unit; ?></span></p>
                            <p class="product_price">市场价：<span
                                        class="s">￥<span><?= $tv->goods_marketprice ?></span>元/<span><?= $tv->unit; ?></span></span>
                            </p>
                            <p class="product_tag orange"><img src="/public/wap/images/icon_time_orange.png">仅剩<span
                                        data-id="storage"
                                        data-good="<?= $tv->id; ?>"><?= $tv->good_amount ?></span><span>件</span></p>
                        </a>
                        <a href="javascript:void(0)" class="btn" data-id="order-create" data-ticket="<?= $ticketId; ?>"
                           data-good="<?= $tv->id; ?>">立即预存</a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
        <?php if (!empty($baoKuanGood)): ?>
            <div class="explosion_title2 pt-1">
                <a href="javascript:void(0)" class="btn" data-id="reload"
                   data-type="<?= \common\models\TicketApplyGoods::TYPE_COUPON; ?>"
                   data-ticket="<?= $ticketId; ?>">换一批</a>
                <img src="/public/wap/images/icon_tab_boutique.png">
                <span class="orange">爆款预约</span>
            </div>
            <ul class="list-unstyled clearfix com_productlist com_productlistone pt-2"
                id="<?= \common\models\TicketApplyGoods::TYPE_COUPON . $ticketId; ?>">
                <?php foreach ($baoKuanGood as $bgk => $bgv): ?>
                    <li>
                        <a href="<?= \DL\service\UrlService::build(['goods/detail', 'id' => $bgv->id]); ?>"
                           class="product">
                        <span class="product_img float-left"><img src="<?= $bgv->goods_pic ?>" class="loading_img"
                                                                  data-url="<?= $bgv->goods_pic ?>"
                                                                  alt="<?= $bgv->goods_name ?>"></span>
                            <div class="product_con">
                                <p class="product_name ellipsis mb-0"><?= $bgv->goods_name ?></p>
                                <p class="product_handsel mt-0">预约价：<span
                                            class="orange">￥<span><?= $bgv->goods_price ?></span></span>/<?= $bgv->unit; ?>
                                </p>
                                <p class="product_price">市场价：<span
                                            class="s">￥<span><?= $bgv->goods_marketprice ?></span>/<span><?= $bgv->unit; ?></span></span>
                                </p>
                                <p class="product_tag orange"><img src="/public/wap/images/icon_time_orange.png">仅剩<span
                                            data-id="storage"
                                            data-good="<?= $bgv->id; ?>"><?= $bgv->good_amount ?></span><span>件</span>
                                </p>
                            </div>
                        </a>
                        <a href="javascript:void(0)" class="btn float-right" data-id="get-coupon" data-source="1"
                           data-ticket="<?= $ticketId; ?>" data-good="<?= $bgv->id; ?>">立即预约</a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
<?php endif; ?>
<div class="explosion_brand" style="background-color: <?= $ticketConfig->spbg_color; ?>;">
    <div class="explosion_title">
        <img src="/public/wap/images/bg_title_exp.png">
        <span>合作品牌</span>
        <img src="/public/wap/images/bg_title_exp.png">
    </div>
    <div class="topNav swiper-container swiper-container-horizontal swiper-container-free-mode"
         style="background-color: <?= $ticketConfig->spbg_color; ?>;">
        <div class="swiper-wrapper com_tab" style="transition-duration: 300ms;transform: translate3d(0px, 0px, 0px);">
            <?php foreach ($goodClass as $gck => $gcv): ?>
                <a class="swiper-slide <?= $gck ? '' : 'active'; ?>" data-id="tags"
                   data-key="<?= $gcv->id ?>"><span><?= $gcv->name ?></span></a>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="explosion_brand_con">
        <?php foreach ($goodClass as $gck => $gcv): ?>
            <ul class="list-unstyled clearfix com_brandlist" data-id="tagContent" data-key="<?= $gcv->id ?>"
                style="display: <?= $gck ? 'none' : 'block'; ?>">
                <?php if (isset($joinStoreGc[$gcv->id])) : ?>
                    <?php $icoLimit = 1;
                    foreach ($joinStoreGc[$gcv->id] as $son_v): ?>
                        <?php if ($ticketConfig->ico_limit and $ticketConfig->ico_limit < $icoLimit) {
                            continue;
                        }
                        $icoLimit++;
                        ?>
                        <li style="overflow:  hidden;">
                            <a href="<?= \DL\service\UrlService::build(['shop/detail', 'id' => $son_v->user_id]); ?>">
                                <img src="<?= $son_v->avatar ?>" alt="<?= $son_v->nickname; ?>">
                            </a>
                        </li>
                    <?php endforeach;endif; ?>
            </ul>
        <?php endforeach; ?>
        <div class="com_morebtn">
            <a href="<?= \DL\service\UrlService::build('shop/index'); ?>" class="btn bg_white">查看更多<img
                        src="/public/wap/images/icon_links.png" alt="more"></a>
        </div>
    </div>
    <script>
        $('[data-id="tags"').click(function () {
            var that = $(this),
                key = that.attr('data-key');

            that.addClass('active').siblings().removeClass('active');
            $('[data-id="tagContent"][data-key="' + key + '"]').show().siblings("ul").hide();
        })
    </script>
    <div class="explosion_title">
        <img src="/public/wap/images/bg_title_exp.png">
        <span>现场热图</span>
        <img src="/public/wap/images/bg_title_exp.png">
    </div>
    <div class="explosion_news">
        <!-- Swiper -->
        <div class="swiper-container swiper-container-initialized swiper-container-horizontal">
            <div class="swiper-wrapper">
                <?php foreach ($ticketConfig->hot_pics AS $hpic) : ?>
                    <div class="swiper-slide"><img src="<?= $hpic; ?>" alt=""></div>
                <?php endforeach; ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
    </div>
    <div class="explosion_title">
        <img src="/public/wap/images/bg_title_exp.png">
        <span>路线导航</span>
        <img src="/public/wap/images/bg_title_exp.png">
    </div>
    <div class="explosion_map">
        <img src="<?= $ticketConfig->guider_pic ? $ticketConfig->guider_pic[0] : ''; ?>">
    </div>
    <?php if ($ticketConfig->spad_1) : $sectionDatas = array_values($ticketConfig->spad_1);
        $secKey = 'spad_1'; ?>
        <?php if (count($sectionDatas) > 1) : ?>
            <div class="slider carousel slide" data-ride="carousel">
                <!-- 指示符 -->
                <ul class="carousel-indicators">
                    <?php foreach ($sectionDatas AS $index => $sectionData) : ?>
                        <li data-target="#slider<?= $secKey; ?>" data-slide-to="<?= $index; ?>"
                            class="<?= $index ? '' : 'active'; ?>"></li>
                    <?php endforeach; ?>
                </ul>
                <!-- 轮播图片 -->
                <div class="carousel-inner">
                    <?php foreach ($sectionDatas AS $index => $sectionData) : ?>
                        <a href="<?= $sectionData[0]; ?>" class="carousel-item <?= $index ? '' : 'active'; ?>">
                            <img src="<?= $sectionData[1]; ?>">
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php else : ?>
            <div class="com_ad">
                <?php foreach ($sectionDatas AS $index => $sectionData) : ?>
                    <a href="<?= $sectionData[0]; ?>">
                        <img data-url="<?= $sectionData[1]; ?>" class="loading_img" src="<?= $sectionData[1]; ?>">
                    </a>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <div class="explosion_title">
        <img src="/public/wap/images/bg_title_exp.png">
        <span>服务保障</span>
        <img src="/public/wap/images/bg_title_exp.png">
    </div>
    <?php if (!empty($serviceList)): ?>
        <div class="promise explosion_promise">
            <ul class="promise_ul list-unstyled clearfix">
                <?php foreach ($serviceList as $key => $item): ?>
                    <li>
                        <img src="<?= $item->icon ?>">
                        <p class="p1 green"><?= $item->name ?></p>
                        <p class="p2"><?= $item->desc ?></p>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <div class="explosion_phone">
        <a href="tel:0991-4677878"><img src="/public/wap/images/icon_phone_red.png">服务热线：<span><?= $phone; ?></span></a>
    </div>
</div>
<div class="modal modal_explain fade show" id="myModalExplain23" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">索票须知</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align"><?= $ticket->notice_words; ?></p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">知道了</button>
            </div>
        </div>
    </div>
</div>
<?php include __DIR__ . '/common/footer.php'; ?>
<!-- 轮播图-手指滑动效果 -->
<script type="text/javascript">

    $(function () {
        // 获取手指在轮播图元素上的一个滑动方向（左右）
        // 获取界面上轮播图容器
        var $carousels = $('.carousel');
        var startX, endX;
        // 在滑动的一定范围内，才切换图片
        var offset = 50;
        // 注册滑动事件
        $carousels.on('touchstart', function (e) {
            // 手指触摸开始时记录一下手指所在的坐标x
            startX = e.originalEvent.touches[0].clientX;

        });
        $carousels.on('touchmove', function (e) {
            // 目的是：记录手指离开屏幕一瞬间的位置 ，用move事件重复赋值
            endX = e.originalEvent.touches[0].clientX;
        });
        $carousels.on('touchend', function (e) {
            //console.log(endX);
            //结束触摸一瞬间记录手指最后所在坐标x的位置 endX
            //比较endX与startX的大小，并获取每次运动的距离，当距离大于一定值时认为是有方向的变化
            var distance = Math.abs(startX - endX);
            if (distance > offset) {
                //说明有方向的变化
                //根据获得的方向 判断是上一张还是下一张出现
                $(this).carousel(startX > endX ? 'next' : 'prev');
            }
        })
    });
</script>

<script type="text/javascript">
    //公告滚动
    noticeScroll();

    //公告滚动
    function noticeScroll() {
        $.fn.myScroll = function (options) {
            var defaults = {
                speed: 60,
                rowHeight: 20
            };
            var opts = $.extend({}, defaults, options), intId = [];

            function marquee(obj, step) {
                obj.find('ul').animate({
                    marginTop: '-=1'
                }, 0, function () {
                    var s = Math.abs(parseInt($(this).css("margin-top")));
                    if (s >= step) {
                        $(this).find("li").slice(0, 1).appendTo($(this));
                        $(this).css("margin-top", 0);
                    }
                });
            };
            this.each(function (i) {
                var sh = opts["rowHeight"], speed = opts["speed"], _this = $(this);
                intId[i] = setInterval(function () {
                    if (_this.find("ul").height() <= _this.height()) {
                        clearInterval(intId[i]);
                    } else {
                        marquee(_this, sh);
                    }
                }, speed);

                _this.hover(function () {
                    clearInterval(intId[i]);
                }, function () {
                    intId[i] = setInterval(function () {
                        if (_this.find("ul").height() <= _this.height()) {
                            clearInterval(intId[i]);
                        } else {
                            marquee(_this, sh);
                        }
                    }, speed);
                });
            });
        };

        $(".explosion_notice").myScroll({
            speed: 60,
            rowHeight: 20
        });
    }

    $('[data-id="shadowNotice"]').click(function () {
        var title = $(this).attr('data-title'),
            data = $(this).attr('data-data');

        alert(data, function () {
        }, {title: title, button: '知道了'});
    });
</script>

<!-- tab左右滑动 -->
<script type="text/javascript">
    var mySwiper = new Swiper('.topNav', {
        freeMode: true,
        preventClicks: false,//默认true
        freeModeMomentumRatio: 0.5,
        slidesPerView: 'auto',
    });


</script>
<script>
    var swiper = new Swiper('.explosion_news .swiper-container', {
        slidesPerView: 1.5,
        spaceBetween: 5,
        initialSlide: 1,
        loop: true,
        centeredSlides: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
</script>
<script>
    $('[data-id="ticket-detail"]').click(function () {
        $('#myModalExplain23').modal();
    })

    $('[data-id="submit"]').click(function () {
        var name = $('input[name="name"]'),
            mobile = $('input[name="mobile"]'),
            address = $('input[name="address"]'),
            code = $('input[name="code"]');

        if (!/^1\d{10}$/.test(mobile.val())) {
            console.log(mobile.attr('data-default'));
            if (!/^1\d{10}$/.test(mobile.attr('data-default'))) {
                alert("请输入正确的手机号码");
                return false;
            }
            mobile.val(mobile.attr('data-default'));
        }

        if (code.val().length < 1) {
            alert("请输入验证码");
            return false;
        }
        var url = "<?= \DL\service\UrlService::build('jz/ask-ticket-by-ajax'); ?>";
        var formData = $('form[data-id="join"]').serialize();
        $.post(url,formData,function (res) {
            if(res.status==1){
                location.url = res.data.url;
            }else{
                alert(res.msg);
            }
        },'json');
        //$('form[data-id="join"]').submit();
    })

    var mobileDom = $('input[name="mobile"]'),
        codeDom = $('a[data-id="getCode1"]'),
        getCodeCycle = 60;

    codeDom.click(function () {
        var mobile = mobileDom.val();
        if (!/^1\d{10}$/.test(mobile)) {
            alert("请输入正确的手机号");
            return false;
        }

        if (!codeDom.hasClass('disabled')) {
            $.post(
                GET_MSG_CODE,
                {mobile: mobile},
                function (res) {
                    res = JSON.parse(res);
                    if (res.status) {
                        if (codeDom.hasClass('disabled')) {
                            return false;
                        }

                        codeDom.attr('data-cycle', getCodeCycle).addClass('disabled');

                        var cycleClock = setInterval(function () {
                            var cycle = parseInt(codeDom.attr('data-cycle'));
                            if (cycle > 0) {
                                codeDom.text("" + cycle + "s").attr('data-cycle', cycle - 1);
                            } else {
                                codeDom.text('验证码').removeClass('disabled');
                                clearInterval(cycleClock);
                            }
                        }, 1000);
                    }
                }
            )
        }
    });

    $('[data-id="reload"]').click(function () {
        var type = $(this).attr('data-type'),
            ticketId = $(this).attr('data-ticket');

        $.get('<?= \DL\service\UrlService::build('api/reload-goods'); ?>', {
            ticketId: ticketId,
            type: type
        }, function (res) {
            $('#' + type + ticketId).empty().html(res);
            // refreshButton();
        });
    })
</script>