<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>报名成功</h2>
    <a href="/" style="float: right;">
        <img src="/public/images/icon-home.png" style="width: 18px;height: 18px;" alt="主页">
    </a>
</div>

<div class="ticketaddress">
    <div class="ticketaddress_con1">
        <img src="/public/wap/images/img_warn_success.png" alt="报名成功" class="img">
        <div class="con">
            <p class="p1">恭喜您报名成功！</p>
            <p>活动时间：<?= date('m月d日', strtotime($ticket->open_date)); ?>-<?= date('m月d日', strtotime($ticket->end_date)); ?></p>
            <p>活动地址：<?= $ticket->open_place; ?>（<?= $ticket->address; ?>）</p>
        </div>
    </div>
    <div class="ticketaddress_con2">
        <p><b>温馨提示：</b>请详细填写以上消息，我们将免费为您快递一份价值<?= $ticket->ticket_price;?>元的门票展会资料</p>
        <form  data-id="setAddress" action="<?= \DL\service\UrlService::build('ticket-address'); ?>" method="post">
            <textarea name="address"  class="textarea" placeholder="请输入您的收货地址"></textarea>
            <input type="hidden" name="ask_id" value="<?= $ticketAsk->id; ?>">
            <button type="button" data-id="submit" class="btn btn_big">立即提交</button>
        </form>
    </div>
</div>

<?php if ($ticketConfig->ad_ticket_address) : $sectionDatas = array_values($ticketConfig->ad_ticket_address); $secKey = 'adTd_'; ?>
    <?php if (count($sectionDatas) > 1) :?>
        <div class="slider carousel slide" data-ride="carousel">
            <!-- 指示符 -->
            <ul class="carousel-indicators">
                <?php foreach ($sectionDatas AS $index => $sectionData) :?>
                    <li data-target="#slider<?= $secKey; ?>" data-slide-to="<?= $index; ?>" class="<?= $index ? '' : 'active'; ?>"></li>
                <?php endforeach; ?>
            </ul>
            <!-- 轮播图片 -->
            <div class="carousel-inner">
                <?php foreach ($sectionDatas AS $index => $sectionData) :?>
                    <a href="<?= $sectionData[0]; ?>" class="carousel-item <?= $index ? '' : 'active'; ?>">
                        <img src="<?= $sectionData[1]; ?>">
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    <?php else :?>
        <div class="com_ad2">
            <?php foreach ($sectionDatas AS $index => $sectionData) :?>
                <a href="<?= $sectionData[0]; ?>">
                    <img src="<?= $sectionData[1]; ?>" class="loading_img" data-url="<?= $sectionData[1]; ?>">
                </a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
<?php endif; ?>
<script>
    $('[data-id="submit"]').click(function () {
        var address = $('[name="address"]').val();

        if (address.length < 1) {
            alert("请完善地址信息");
            return false;
        }

        // $('form[data-id="setAddress"]').submit();


        var form = $('form[data-id="setAddress"]'),
            formData = new FormData(form.get(0)),
            url = form.attr('action');

        $.ajax({
            url: url,
            data : formData,
            type: 'POST',
            cache: false,
            processData: false,
            contentType: false,
            success: function (res) {
                res = JSON.parse(res);
                if (res.status) {
                    alert("快递地址提交成功，请注意接听来电", function () {
                        window.location.href = '<?= \DL\service\UrlService::build("index/index"); ?>';
                    });

                    return false;
                }

                alert (res.msg, function () {
                    return false;
                })
            }
        });
    });
</script>
