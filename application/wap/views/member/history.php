<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/6
 * Time: 13:36
 */

$this->title = '浏览历史';
$this->params['breadcrumbs'][] = $this->title;

use  yii\helpers\Url;

?>
<style>
    ul{
        overflow: hidden;
    }
</style>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>

    <h2>浏览记录</h2>
</div>
<!--- 浏览记录 -->
<div class="pro_list">
    <div class="browse_record_ajax">

    </div>
</div>


<script type="text/javascript">
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var obj ={};
    // 页数
    var page = 0;
    // 每页展示5个
    var size = 10;
    var sum='<?php echo $pages->totalCount;?>';
    var p= Math.ceil(sum/size);
    // dropload
    var result='';
    var dataTimeHtml;
    var dataTimeStr='';
    $('#pro_list').dropload({
        scrollArea : window,
        distance:10,
        loadDownFn : function(me){
            if (page<p) {
                page++;
                // 拼接HTML
                result = '';
                var goodDetail='';
                $.get('<?php echo Url::toRoute(["member/history"]);?>',{page:page,'per-page':size},function(data){

                    $.each(data.list,function(key,val){
                        result = '';
                        if (dataTimeStr == key){

                            $.each(val, function (son_k, son_v) {
                                goodDetail ="<?=Url::toRoute(['goods/detail','id'=>''])?>"+son_v.goodId;
                                result += '          <li>\n' +
                                    '                        <a href="'+goodDetail+'">\n' +
                                    '                            <img style="height:110px" src="' + son_v.pic + '"/>\n' +
                                    '                            <p>' + son_v.name + '</p>\n' +
                                    '                            <span>¥<b>' + son_v.price + '</b></span>\n' +
                                    '                        </a>\n' +
                                    '                    </li>';
                            });
                            dataTimeHtml.find('ul').append(result);
                        }else {
                            result += '        <div class="box">\n' +
                                '            <div class="title">\n' +
                                '                <h2>'+key+'</h2>\n' +
                                '            </div>\n' +
                                '            <div class="cont">\n' +
                                '                <ul>';
                            $.each(val, function (son_k, son_v) {
                                goodDetail ="<?=Url::toRoute(['goods/detail','id'=>''])?>"+son_v.goodId;
                                result += '          <li>\n' +
                                    '                        <a href="'+goodDetail+'">\n' +
                                    '                            <img style="height:110px" src="' + son_v.pic + '"/>\n' +
                                    '                            <p>' + son_v.name + '</p>\n' +
                                    '                            <span>¥<b>' + son_v.price + '</b></span>\n' +
                                    '                        </a>\n' +
                                    '                    </li>';
                            });
                            result += '  </ul>\n' +
                                '\n' +
                                '            </div>\n' +
                                '        </div>';

                            $('.browse_record_ajax').append(result);
                        }

                        dataTimeHtml = $(".browse_record_ajax .box").last();
                        dataTimeStr = dataTimeHtml.find('h2').text();

                    });

                    // 每次数据插入，必须重置
                    me.resetload();
                },'json');
            }else{

                // 锁定
                me.lock();
                // 无数据
                me.noData();
                // 为了测试，延迟1秒加载
                // 插入数据到页面，放到最后面
                $('.infolist').append(result);

                //settime();
                // 每次数据插入，必须重置
                me.resetload();

            }
        }
    });


</script>
