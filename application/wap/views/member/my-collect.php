<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/6
 * Time: 14:46
 */

$this->title = '我的收藏';
$this->params['breadcrumbs'][] = $this->title;
use  yii\helpers\Url;
use  wap\logic\CollectLogic;
?>
<?php
  $collectCount=CollectLogic::init()->getCollectCount();
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>

<div class="head">
    <a href="<?=Url::toRoute(['member/index'])?>"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>我的收藏</h2>
</div>
<!--- 预定商品 -->
<div id="collection">
    <div class="nav">
        <a class="click_state <?=($type=='RESERVE_GOOD')?'cur':''?>"  href="<?=Url::toRoute(['member/my-collect','type'=>'RESERVE_GOOD'])?>">预定商品(<?=$collectCount['reserve']?>)</a>
        <a class="click_state <?=($type=='SUBSCRIBE_GOOD')?'cur':''?>" href="<?=Url::toRoute(['member/my-collect','type'=>'SUBSCRIBE_GOOD'])?>">预约商品(<?=$collectCount['subscribe']?>)</a>
        <a class="click_state <?=($type=='STORE')?'cur':''?>" href="<?=Url::toRoute(['member/my-collect','type'=>'STORE'])?>">收藏店铺(<?=$collectCount['store']?>)</a>
    </div>
    <div class="con">
        <ul class="conAjax">
        </ul>
    </div>
</div>

<script>
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var obj ={};
    // 页数
    var page = 0;
    // 每页展示5个
    var size = 10;
    var sum='<?php echo $pages->totalCount;?>';
    var p= Math.ceil(sum/size);
    var type = '<?=empty($type)?'RESERVE_GOOD':$type?>';
    var result='';
    $('.con').dropload({
        scrollArea : window,
        distance:10,
        loadDownFn : function(me){
            if (page<p) {
                page++;
                // 拼接HTML
                result = '';
                var togoodDetail='';
                $.get('<?php echo Url::toRoute(["member/my-collect"]);?>',{page:page,'per-page':size,type:type},function(data){
                    result = '';
                    switch (data.type) {
                        case 'RESERVE_GOOD':
                            $.each(data.list,function(key,val){
                                togoodDetail ="<?=Url::toRoute(['goods/detail','id'=>''])?>"+val.goodId;
                               result+='  <li>\n' +
                                   '                <div class="pic">\n' +
                                   '                    <a href="'+togoodDetail+'"><img src="'+val.pic+'"/></a>\n' +
                                   '                </div>\n' +
                                   '                <div class="con">\n' +
                                   '                    <h3>'+val.name+'</h3>\n' +
                                   '                    <p>专享价：￥'+val.price+'<s>￥'+val.marketprice+'</s></p>\n' +
                                   '                    <span>¥<b>'+val.order_price+'</b></span>\n' +
                                   '                </div>\n' +
                                   '                <div class="operating">\n' +
                                   '                    <a class="delete"  tabId='+val.collectId+' href="javascript:void(0)">删除</a>\n' +
                                   '                </div>\n' +
                                   '            </li>'
                            });
                            break;
                        case 'SUBSCRIBE_GOOD':
                            $.each(data.list,function(key,val){
                                togoodDetail ="<?=Url::toRoute(['goods/detail','id'=>''])?>"+val.goodId;
                                result+='      <li>\n' +
                                    '        <div class="pic">\n' +
                                    '          <a href="'+togoodDetail+'"><img src="'+val.pic+'"/></a>\n' +
                                    '        </div>\n' +
                                    '        <div class="con">\n' +
                                    '          <h3>'+val.name+'</h3>\n' +
                                    '          <p>'+val.brief+'</s></p>\n' +
                                    '          <span>¥<b>'+val.price+'</b></span>\n' +
                                    '        </div>  \n' +
                                    '        <div class="operating">\n' +
                                    '          <a class="delete"  tabId='+val.collectId+' href="javascript:void(0)">删除</a>\n' +
                                    '        </div>  \n' +
                                    '      </li>'
                            });
                            break;
                        case 'STORE':
                            $.each(data.list,function(key,val){
                               var toStore = "<?=Url::toRoute(['shop/detail','id'=>''])?>"+val.storeId;
                                result+='      <li class="shop">\n' +
                                    '        <div class="pic">\n' +
                                    '          <a href="'+toStore+'"><img src="'+val.pic+'"/></a>\n' +
                                    '        </div>\n' +
                                    '        <div class="con">\n' +
                                    '          <h3>'+val.name+'</h3>\n' +
                                    '          <em>'+val.evaluate+'条评论</em>\n' +
                                    '          <label>共'+val.attempt+'家体验店</label>\n' +
                                    '        </div>  \n' +
                                    '        <div class="operating">\n' +
                                    '          <a class="delete"  tabId='+val.collectId+' href="javascript:void(0)">删除</a>\n' +
                                    '        </div>  \n' +
                                    '      </li>'
                            });
                            break;
                        default:

                    }
                    $('.conAjax').append(result);
                    // 每次数据插入，必须重置
                    me.resetload();
                },'json');
            }else{

                // 锁定
                me.lock();
                // 无数据
                me.noData();
                // 为了测试，延迟1秒加载
                // 插入数据到页面，放到最后面
                $('.infolist').append(result);

                //settime();
                // 每次数据插入，必须重置
                me.resetload();

            }
        }
    });

    //点击删除
    $(document).on('click','.delete',function () {
        var thisTab=$(this);
        var collectId = $(this).attr('tabId');
        var url = "<?=Url::toRoute(['member/delete-collect'])?>";
        $.ajax({
            type: "GET",
            url:  url,
            data: {collectId:collectId},
            dataType: "json",
            success: function(data){
                 alert(data.msg);
                if (data.code==1){
                    thisTab.parent().parent().remove();
                 }
            }
        });

    })
</script>