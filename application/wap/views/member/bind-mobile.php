<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/5
 * Time: 18:16
 */

use  yii\helpers\Url;

?>
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>手机号验证</h2>
</div>
<!-- 注册 -->
<div id="login">
    <!-- Tab内容 -->
    <div class="tab_con tab-content">
        <div id="home" class="container tab-pane active"><br>
            <ul>
                <li><input class="text text_icon3" type="text" id="member-mobile" placeholder="请输入手机号"/></li>
                <li class="verification">
                    <input class="text text_icon4" type="text" id="getYzm"   placeholder="请输入验证码"/>
                    <a class="button" id="smscode" href="javascript:void(0)">获取验证码</a>
                </li>
                <li><input class="submit" type="submit" value="提交修改"/></li>
            </ul>
        </div>
    </div>
</div>


<!-- 模态框- 提醒弹框-3s自动消失 -->
<div class="modal modal_explain modal_explain_sm modal_remind fade" id="myModalRemind">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">提醒</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align" id="tipInfo"></p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn" data-dismiss="modal">确 定</a>
            </div>
        </div>
    </div>
</div>
<script>
    var issend=true;
    var t=60;
    var code='';
    $("#smscode").click(function () {
        if (issend){
            var mobile=$("#member-mobile").val();

            var check_phone_number = /^1[345689]\d{9}$/;
            if (mobile.length == 0) {

                $("#myModalRemind").modal('show');
                $("#tipInfo").text('手机号不能为空');
                return;
            }
            if (mobile.length != 11) {
                $("#myModalRemind").modal('show');
                $("#tipInfo").text('请输入有效的手机号');
                return;
            }
            if (!mobile.match(check_phone_number)) {
                $("#myModalRemind").modal('show');
                $("#tipInfo").text('请输入有效的手机号');
                return;
            }

            isMobile(mobile);
        }

    });

    //点击提交
    $(".submit").on('click',function () {
        var mobile=$("#member-mobile").val();
        var getYzm=$("#getYzm").val();
        var check_phone_number = /^1[345689]\d{9}$/;
        if (mobile.length == 0) {
            $("#myModalRemind").modal('show');
            $("#tipInfo").text('手机号不能为空');
            return;
        }
        if (mobile.length != 11) {
            $("#myModalRemind").modal('show');
            $("#tipInfo").text('请输入有效的手机号');
            return;
        }
        if (!mobile.match(check_phone_number)) {
            $("#myModalRemind").modal('show');
            $("#tipInfo").text('请输入有效的手机号');
            return;
        }
        if (getYzm.length == 0) {
            $("#myModalRemind").modal('show');
            $("#tipInfo").text('验证码不能为空');
            return;
        }

        $.post('<?php echo Url::toRoute(['member/bind-mobile']);?>',{mobile:mobile,getYzm:getYzm},function(res){
            if (res.code==1){
                $("#myModalRemind").modal('show');
                $("#tipInfo").text(res.msg);
                $("#myModalRemind").find('.btn').addClass('clickToSelf');
            }else{
                $("#myModalRemind").modal('show');
                $("#tipInfo").text(res.msg);
            }
        },'json');

    });

    function update_a(num,t) {

        var get_code=$('#smscode');
        if(num == t) {
            get_code.text('重新发送');
            issend=true;
            $("#smscode").removeClass('disabled');
        }
        else {
            var printnr = t-num;
            get_code.text(printnr +"s");
        }
    }

    //点击到个人中心
    $(document).on('click','.clickToSelf',function () {
        window.location.href='<?=Url::toRoute(['member/member-edit'])?>';
    });

    function isMobile(mobile) {
        $.post('<?php echo Url::toRoute(['member/is-mobile']);?>',{mobile:mobile},function(res){
            if (res.code==1){
                 issend=false;
                for(i=1;i<=60;i++) {
                    window.setTimeout("update_a(" + i + ","+t+")", i * 1000);
                }
                $("#smscode").addClass('disabled');
                $.post('<?php echo Url::toRoute(['api/smscode']);?>',{mobile:mobile},function(res){
                },'json');
            }else {
                $("#myModalRemind").modal('show');
                $("#tipInfo").text(res.msg);
            }
        },'json');

    }

</script>