<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/6
 * Time: 11:12
 */

use yii\helpers\Url;

$this->title = '评论列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>

    <h2>我的评论</h2>
</div>
<!--- 我的评论 -->
<div id="my_comment">
    <?php if(!empty($comentpic)):?>
        <div class="banner">
            <a href="<?=$comentpic->url?>"> <img src="<?=$comentpic->picture?>"/></a>
        </div>
    <?php endif;?>
    <div class="con">

    </div>
</div>
<script>
    var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var obj = {};
    // 页数
    var page = 0;
    // 每页展示5个
    var size = 10;
    var sum = '<?php echo $pages->totalCount;?>';
    var p = Math.ceil(sum / size);
    var state = '<?=empty($state) ? 0 : $state?>';
    var result = '';
    $('#my_comment').dropload({
        scrollArea: window,
        distance: 10,
        loadDownFn: function (me) {
            if (page < p) {
                page++;
                // 拼接HTML
                $.get('<?php echo Url::toRoute(["member/evaluate-list"]);?>', {
                    page: page,
                    'per-page': size,
                    state: state
                }, function (data) {
                    result = '';
                    $.each(data.list, function (key, val) {
                        result += '<div class="box">\n' +
                            '            <div class="info">\n' +
                            '                <p>商品质量：<b>'+val.spzl+'</b>星 商户服务：<b>'+val.shfw+'</b>星</p>\n' +
                            '                <span>'+val.create_time+'</span>\n' +
                            '                <div class="clear"></div>\n' +
                            '            </div>\n' +
                            '            <div class="tex"><p>'+val.content+'</p></div>\n' +
                            '            <div class="pic">\n' +
                            '                <ul>\n';
                               $.each(val.imgs,function (sonk,sonv) {
                                   result +='<li><img src="'+sonv+'"/></li>'
                               });
                         if (val.is_intergral_goods==1){
                             result +='<div clss="clear"></div>\n' +
                                 '                </ul>\n' +
                                 '            </div>\n' +
                                 '            <div class="pro toGoodsDetaile" tabType="1" goodId="'+val.goodsId+'">\n' +
                                 '                <img src="'+val.goodPic+'"/>\n' +
                                 '                <h3>'+val.goods_name+'</h3>\n' +
                                 '                <p>'+val.goodBrief+'</p>\n'+
                                   '                <span><b>'+val.goodsPrice+'</b>积分</span>\n' +
                                 '            </div>\n' +
                                 '        </div>';
                         }else {
                             result +='<div clss="clear"></div>\n' +
                             '                </ul>\n' +
                             '            </div>\n' +
                             '            <div class="pro toGoodsDetaile" tabType="0" goodId="'+val.goodsId+'">\n' +
                             '                <img src="'+val.goodPic+'"/>\n' +
                             '                <h3>'+val.goods_name+'</h3>\n' +
                             '                <p>'+val.goodBrief+'</p>\n'+
                             '                <span>¥<b>'+val.goodsPrice+'</b></span>\n' +
                                 '            </div>\n' +
                                 '        </div>';
                         }

                    });
                    $('.con').append(result);
                    // 每次数据插入，必须重置
                    me.resetload();
                }, 'json');
            } else {

                // 锁定
                me.lock();
                // 无数据
                me.noData();
                // 为了测试，延迟1秒加载
                // 插入数据到页面，放到最后面
                //$('.conAjax').append(result);

                //settime();
                // 每次数据插入，必须重置
                me.resetload();

            }
        }
    });
    //点击到详情
    $(document).on('click','.toGoodsDetaile',function () {
        var is_inter=$(this).attr('tabType');

        var goodId=$(this).attr('goodId');
        if (is_inter == 1){
            window.location.href="<?=Url::toRoute(['integral/goods-detail','id'=>''])?>"+goodId;
        } else {
              window.location.href="<?=Url::toRoute(['goods/detail','id'=>''])?>"+goodId;
        }

    })
    

</script>
