<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/4
 * Time: 11:07
 */
use yii\helpers\Url;
?>

<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>我的预定</h2>
</div>
<!--- 订单空 -->
<div id="order_air">
    <img src="/public/wap/images/status_ico.png"/>
    <p>亲，暂无订单商品</p>
    <a href="<?=Url::toRoute(['index/index'])?>">去逛逛</a>
</div>