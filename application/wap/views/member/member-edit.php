<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/6
 * Time: 17:19
 */
$this->title = '我的设置';
$this->params['breadcrumbs'][] = $this->title;
use  yii\helpers\Url;
?>

<div class="head">
    <a href="<?=Url::toRoute(['member/index'])?>"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>我的设置</h2>
</div>
<!--- 用户编辑 -->
<div id="user_edit">
    <div class="box">
        <ul>
            <li>
                <a href="javascript:void(0)">
                    头像
                    <img class="jt" src="/public/wap/images/jt_ico.png" />
                    <div class="user_head">
                        <input class="file" type="file" id="avatarfilebut"/>
                        <img id="avatarimg" src="<?=$member->avatarTm?>"/>
                    </div>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a href="<?=Url::toRoute(['member/alter-nickname'])?>">
                    昵称
                    <img class="jt" src="/public/wap/images/jt_ico.png"/>
                    <span><?=$member->nickname?></span>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a href="javascript:void(0)" >
                    性别
                    <img class="jt" src="/public/wap/images/jt_ico.png"/>
                    <span id="select_sex">
                        <?php if ($member->sex == 1){
                            echo  '男';
                        }else if ($member->sex == 2){
                            echo  '女';
                        }else{
                            echo  '未知';
                        }?>
                    </span>
                    <div class="clear"></div>
                </a>
            </li>
            <li>

                <a href="javascript:void(0)" class="com_linktwo">
                    生日
                    <input type="date" name=""  class="inpdate input_date" value="<?php $nowDate=empty($member->birthday_year)?date('Y-m-d'):$member->birthday_year; echo date('Y-m-d',strtotime($nowDate));?>">
                    <img class="jt" src="/public/wap/images/jt_ico.png"/>
                    <span><?=$member->birthday_year?></span>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a href="<?=Url::toRoute(['address/address-list'])?>">
                    收货地址
                    <img class="jt" src="/public/wap/images/jt_ico.png"/>
                    <div class="clear"></div>
                </a>
            </li>
        </ul>
    </div>
    <div class="box">
        <ul>
            <li><h2>账户安全</h2></li>
            <li>
                <a href="<?=Url::toRoute(['member/alter-pwd'])?>">
                    登录密码
                    <img class="jt" src="/public/wap/images/jt_ico.png"/>
                    <span>已设置</span>
                    <div class="clear"></div>
                </a>

            </li>
            <li>
                <a href="<?=Url::toRoute(['member/bind-mobile'])?>">
                    手机号验证
                    <img class="jt" src="/public/wap/images/jt_ico.png"/>
                    <span><?=empty($member->mobile)?'未验证':'已验证'?></span>
                    <div class="clear"></div>
                </a>
            </li>
        </ul>
    </div>
    <div class="quit"><a href="<?=Url::toRoute(['index/logout'])?>">退出当前账户</a></div>

</div>
<script>
    var sexIndex = '<?=($member->sex)-1?>';
    //选择男女
    var mobileSelect1 = new MobileSelect({
        trigger: '#select_sex',
        title: '选择性别',
        wheels: [
            {data:['男','女','未知']}
        ],
        position:[sexIndex], //初始化定位
        callback:function(indexArr, data){
            //sexIndex=indexArr[0];
            var sex= indexArr[0]+1;
            $.post('<?php echo Url::toRoute(['member/alter-sex']);?>',{sex:sex},function(res){
                console.log(res)
            },'json')
        }
    });

    //日期选择后，显示值
    $('.com_linktwo .input_date').change(function () {
       // save("birthday",$(this).val());
        $(this).siblings('span').html($(this).val());
        $.post('<?php echo Url::toRoute(['member/alter-birthday']);?>',{birthday:$(this).val()},function(res){
        },'json')
    });

    //头像上传
    $("#avatarfilebut").change(function (e) {

        var formData = new FormData();
        var file= e.currentTarget.files[0];
        formData.append('file',file);
        formData.append('_csrf', _csrf);
        $.ajax('<?php echo Url::toRoute(['member/uploadimgv']);?>', {
            method: 'POST',
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false,
            xhr: function () {
                var xhr = new XMLHttpRequest();
                return xhr;
            },

            success: function (re) {
                if(re.code == 0) {
                    $("#avatarimg").attr('src',re.url);
                    var pic=re.attachment;
                    $.post('<?php echo Url::toRoute(['member/alter-avatar']);?>',{avatar:pic},function(res){
                        console.log(res)
                    },'json')
                }else {
                    alert("上传失败")
                }
            },
        });
    });

</script>
