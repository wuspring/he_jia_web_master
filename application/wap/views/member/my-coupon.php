<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/6
 * Time: 17:02
 */
$this->title = '我的收藏';
$this->params['breadcrumbs'][] = $this->title;
use  yii\helpers\Url;
use  wap\logic\CouponLogic;
?>
<?php
$stateArr =CouponLogic::init()->getCouponCount();
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>
<div class="head">
    <a href="<?=Url::toRoute(['member/index'])?>"><img src="/public/wap/images/icon_left.png" alt="返回"></a>

    <h2>我的优惠券</h2>
</div>
<!--- 优惠券 -->
<div id="coupon">
    <div class="nav">
        <a class="<?=($state==0)?'cur':''?>" href="<?=Url::toRoute(['member/my-coupon','state'=>0])?>">全部(<?=$stateArr['allCount']?>)</a>
        <a  class="<?=($state==1)?'cur':''?>" href="<?=Url::toRoute(['member/my-coupon','state'=>1])?>">待使用(<?=$stateArr['daiShiYong']?>)</a>
        <a style="display: none" class="<?=($state==2)?'cur':''?>" href="<?=Url::toRoute(['member/my-coupon','state'=>2])?>">待评价(<?=$stateArr['daiPingJia']?>)</a>
        <a  class="<?=($state==3)?'cur':''?>" href="<?=Url::toRoute(['member/my-coupon','state'=>3])?>">已使用(<?=$stateArr['yiShiYong']?>)</a>
        <a  class="<?=($state==4)?'cur':''?>" href="<?=Url::toRoute(['member/my-coupon','state'=>4])?>">已过期(<?=$stateArr['yiGuoqi']?>)</a>
    </div>

    <div class="con">
        <ul class="conAjax">

        </ul>
    </div>

</div>

<script>
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var obj ={};
    // 页数
    var page = 0;
    // 每页展示5个
    var size = 10;
    var sum='<?php echo $pages->totalCount;?>';
    var p= Math.ceil(sum/size);
    var state = '<?=empty($state)?0:$state?>';
    var result='';
    $('.con').dropload({
        scrollArea : window,
        distance:10,
        loadDownFn : function(me){
            if (page<p) {
                page++;
                // 拼接HTML
                $.get('<?php echo Url::toRoute(["member/my-coupon"]);?>',{page:page,'per-page':size,state:state},function(data){
                    result = '';
                    switch (parseInt(data.state)) {
                        case 0:
                            $.each(data.list,function(key,val){
                                    if (val.state==1){
                                        result+=' <li>\n' +
                                            '   <div class="info"> \n';
                                               if (val.couponType==1){
                                                   result+='<h3>'+val.storeName+'</h3>\n';
                                               }else {
                                                   result+='<h3>'+val.goodName+'</h3>\n';
                                               }
                                            result+='  <p><i>·</i>有效期至'+val.end_time+'</p>\n' +
                                            '          <span><i>·</i>核销码：<b>'+val.code+'</b></span>\n' +
                                            '        </div>\n' +
                                            '\n' +
                                            '        <div class="cards">\n' +
                                            '          <img class="status_bg" src="/public/wap/images/status1_bg.png"/>\n' +
                                            '          <div class="status">  \n' +
                                            '            <p class="text_shadow">减<label>¥</label><b>'+val.money+'</b></p>\n' +
                                            '            <span class="color_orange box_shadow">满'+val.condition+'</span>\n' +
                                            '          </div>  \n' +
                                            '        </div>\n' +
                                            '\n' +
                                            '      </li>';

                                    }else if (val.state==2){
                                        result+='      <li>\n' +
                                            '        <div class="info"> \n';
                                            if (val.couponType==1){
                                                result+='<h3>'+val.storeName+'</h3>\n';
                                            }else {
                                                result+='<h3>'+val.goodName+'</h3>\n';
                                            };
                                        result+= '          <p><i>·</i>有效期至'+val.end_time+'</p>\n' +
                                            '          <span><i>·</i>状态：<label>待评价</label></span>\n' +
                                            '        </div>\n' +
                                            '\n' +
                                            '        <div class="cards">\n' +
                                            '          <img class="status_bg" src="/public/wap/images/status2_bg.png"/>\n' +
                                            '          <a class="link" href="">\n' +
                                            '            <div class="status">\n' +
                                            '              <em class="em1 text_shadow">去评价</em>\n' +
                                            '            </div>  \n' +
                                            '          </a>\n' +
                                            '        </div>\n' +
                                            '\n' +
                                            '      </li>';
                                    } else  if (val.state==3){
                                        result+='      <li>\n' +
                                            '        <div class="info"> \n';
                                                if (val.couponType==1){
                                                    result+='<h3>'+val.storeName+'</h3>\n';
                                                }else {
                                                    result+='<h3>'+val.goodName+'</h3>\n';
                                                };
                                        result+='          <p><i>·</i>有效期至'+val.end_time+'</p>\n' +
                                            '          <span><i>·</i>状态：<label>已使用</label></span>\n' +
                                            '        </div>\n' +
                                            '\n' +
                                            '        <div class="cards">\n' +
                                            '          <img class="status_bg" src="/public/wap/images/status3_bg.png"/>\n' +
                                            '          <div class="status">\n' +
                                            '            <em class="em2 text_shadow">已使用</em>\n' +
                                            '          </div>  \n' +
                                            '        </div>\n' +
                                            '\n' +
                                            '      </li>';
                                    } else if (val.state==4){

                                        result+='      <li>\n' +
                                            '        <div class="info"> \n';
                                            if (val.couponType==1){
                                                result+='<h3>'+val.storeName+'</h3>\n';
                                            }else {
                                                result+='<h3>'+val.goodName+'</h3>\n';
                                            };
                                        result+='          <p><i>·</i>有效期至'+val.end_time+'</p>\n' +
                                            '          <span><i>·</i>状态：<label>已过期</label></span>\n' +
                                            '        </div>\n' +
                                            '\n' +
                                            '        <div class="cards">\n' +
                                            '          <img class="status_bg" src="/public/wap/images/status3_bg.png"/>\n' +
                                            '          <div class="status">\n' +
                                            '            <p class="text_shadow">减<label>¥</label><b>'+val.money+'</b></p>\n' +
                                            '            <span class="color_gray box_shadow">满'+val.condition+'</span>\n' +
                                            '          </div>  \n' +
                                            '        </div>\n' +
                                            '\n' +
                                            '      </li>';
                                    }

                            });
                            break;
                        case 1:
                            $.each(data.list,function(key,val){
                                result+=' <li>\n' +
                                    '        <div class="info"> \n';
                                    if (val.couponType==1){
                                        result+='<h3>'+val.storeName+'</h3>\n';
                                    }else {
                                        result+='<h3>'+val.goodName+'</h3>\n';
                                    }
                                result+= '          <p><i>·</i>有效期至'+val.end_time+'</p>\n' +
                                    '          <span><i>·</i>核销码：<b>'+val.code+'</b></span>\n' +
                                    '        </div>\n' +
                                    '\n' +
                                    '        <div class="cards">\n' +
                                    '          <img class="status_bg" src="/public/wap/images/status1_bg.png"/>\n' +
                                    '          <div class="status">  \n' +
                                    '            <p class="text_shadow">减<label>¥</label><b>'+val.money+'</b></p>\n' +
                                    '            <span class="color_orange box_shadow">满'+val.condition+'</span>\n' +
                                    '          </div>  \n' +
                                    '        </div>\n' +
                                    '\n' +
                                    '      </li>'
                            });
                            break;
                        case 2:
                            $.each(data.list,function(key,val){
                                result+='<li>\n' +
                                    '        <div class="info"> \n';
                                if (val.couponType==1){
                                    result+='<h3>'+val.storeName+'</h3>\n';
                                }else {
                                    result+='<h3>'+val.goodName+'</h3>\n';
                                };
                                result+=' <p><i>·</i>有效期至'+val.end_time+'</p>\n' +
                                    '          <span><i>·</i>状态：<label>待评价</label></span>\n' +
                                    '        </div>\n' +
                                    '\n' +
                                    '        <div class="cards">\n' +
                                    '          <img class="status_bg" src="/public/wap/images/status2_bg.png"/>\n' +
                                    '          <a class="link" href="">\n' +
                                    '            <div class="status">\n' +
                                    '              <em class="em1 text_shadow">去评价</em>\n' +
                                    '            </div>  \n' +
                                    '          </a>\n' +
                                    '        </div>\n' +
                                    '\n' +
                                    '      </li>';
                                console.log(result);
                            });
                            break;
                        case 3:
                            $.each(data.list,function(key,val){
                                result+='      <li>\n' +
                                    '        <div class="info"> \n';
                                if (val.couponType==1){
                                    result+='<h3>'+val.storeName+'</h3>\n';
                                }else {
                                    result+='<h3>'+val.goodName+'</h3>\n';
                                };
                                result+='          <p><i>·</i>有效期至'+val.end_time+'</p>\n' +
                                    '          <span><i>·</i>状态：<label>已使用</label></span>\n' +
                                    '        </div>\n' +
                                    '\n' +
                                    '        <div class="cards">\n' +
                                    '          <img class="status_bg" src="/public/wap/images/status3_bg.png"/>\n' +
                                    '          <div class="status">\n' +
                                    '            <em class="em2 text_shadow">已使用</em>\n' +
                                    '          </div>  \n' +
                                    '        </div>\n' +
                                    '\n' +
                                    '      </li>';
                            });
                            break;
                        case 4:
                            $.each(data.list,function(key,val){
                                result+='      <li>\n' +
                                    '        <div class="info"> \n';
                                if (val.couponType==1){
                                    result+='<h3>'+val.storeName+'</h3>\n';
                                }else {
                                    result+='<h3>'+val.goodName+'</h3>\n';
                                };
                                result+='          <p><i>·</i>有效期至'+val.end_time+'</p>\n' +
                                    '          <span><i>·</i>状态：<label>已过期</label></span>\n' +
                                    '        </div>\n' +
                                    '\n' +
                                    '        <div class="cards">\n' +
                                    '          <img class="status_bg" src="/public/wap/images/status3_bg.png"/>\n' +
                                    '          <div class="status">\n' +
                                    '            <p class="text_shadow">减<label>¥</label><b>'+val.money+'</b></p>\n' +
                                    '            <span class="color_gray box_shadow">满'+val.condition+'</span>\n' +
                                    '          </div>  \n' +
                                    '        </div>\n' +
                                    '\n' +
                                    '      </li>';
                            });
                            break;
                        default:
                    }
                    $('.conAjax').append(result);
                    // 每次数据插入，必须重置
                    me.resetload();
                },'json');
            }else{

                // 锁定
                me.lock();
                // 无数据
                me.noData();
                // 为了测试，延迟1秒加载
                // 插入数据到页面，放到最后面
                //$('.conAjax').append(result);

                //settime();
                // 每次数据插入，必须重置
                me.resetload();

            }
        }
    });

</script>
