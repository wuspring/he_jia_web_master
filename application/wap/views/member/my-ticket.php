<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/11
 * Time: 11:56
 */

use yii\helpers\Url;

?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>

    <h2>我的门票</h2>
</div>
<!--- 我的门票 -->
<div id="my_ticket">
      <div class="div_ajax">

      </div>
</div>

<!-- 模态框- 门票说明 弹框 -->
<div class="modal modal_ticket fade" id="myModalTicket">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">门票说明</p>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p><?=isset($model->tickets_notes)?$model->tickets_notes:''?></p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var obj ={};
    // 页数
    var page = 0;
    // 每页展示5个
    var size = 10;
    var sum='<?php echo $pages->totalCount;?>';
    var p= Math.ceil(sum/size);
    // dropload
    var result='';
    $('#my_ticket').dropload({
        scrollArea : window,
        distance:10,
        loadDownFn : function(me){
            if (page<p) {
                page++;
                // 拼接HTML
                result = '';
                $.get('<?php echo Url::toRoute(["member/my-ticket"]);?>',{page:page,'per-page':size},function(data){

                    $.each(data.list,function(key,val){
                        result += '<div class="box">\n' +
                            '        <div class="title">\n' +
                            '            <h2>'+val.name+'</h2>\n' +
                            '            <a href="javascript:void(0)" data-toggle="modal" data-target="#myModalTicket">门票说明 ></a>\n' +
                            '        </div>\n' +
                            '        <div class="con">\n' +
                            '            <ul>\n' +
                            '                <li>活动城市：'+val.city+'</li>\n' +
                            '                <li>活动时间：'+val.time+'</li>\n' +
                            '                <li>活动地点：'+val.address+'</li>\n' +
                            '            </ul>\n' +
                            '        </div>\n' +
                            '    </div>';
                    });

                    $('.div_ajax').append(result);
                    // 每次数据插入，必须重置
                    me.resetload();
                },'json');
            }else{

                // 锁定
                me.lock();
                // 无数据
                me.noData();
                // 为了测试，延迟1秒加载
                // 插入数据到页面，放到最后面
                $('.infolist').append(result);

                //settime();
                // 每次数据插入，必须重置
                me.resetload();

            }
        }
    });


</script>