<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/11
 * Time: 10:30
 */
use  yii\helpers\Url;
?>

<?php
$stateArr = \wap\logic\AppointmentOrderLogic::init()->getAppointmentOrderCount();
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>
<div class="head">
    <a href="<?=Url::toRoute(['member/index'])?>"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>我的预约</h2>
</div>
<!--- 我的订单 -->
<div id="my_order">
    <div class="nav">
        <a class="<?=($state==-1)?'cur':''?>" href="<?=Url::toRoute(['member/my-appointment','state'=>-1])?>">全部(<?=$stateArr['allCount']?>)</a>
        <a  class="<?=($state==1)?'cur':''?>" href="<?=Url::toRoute(['member/my-appointment','state'=>1])?>">待使用(<?=$stateArr['daiShiYong']?>)</a>
        <a  class="<?=($state==3)?'cur':''?>" href="<?=Url::toRoute(['member/my-appointment','state'=>3])?>">待评价(<?=$stateArr['daiPingJia']?>)</a>

    </div>
    <div class="cont">

    </div>
</div>
<script>
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var obj ={};
    // 页数
    var page = 0;
    // 每页展示5个
    var size = 10;
    var sum='<?php echo $pages->totalCount;?>';
    var p= Math.ceil(sum/size);
    var state = '<?=empty($state)?0:$state?>';
    var result='';
    $('#my_order').dropload({
        scrollArea : window,
        distance:10,
        loadDownFn : function(me){
            if (page<p) {
                page++;
                // 拼接HTML
                $.get('<?php echo Url::toRoute(["member/my-appointment"]);?>',{page:page,'per-page':size,state:state},function(data){
                    result = '';
                    switch (parseInt(data.state)) {
                        case -1:
                            $.each(data.list,function(key,val){
                                var Url = "<?=Url::toRoute(['member/my-appointment-detail','orderid'=>''])?>"+val.orderId;
                                var toEvalate ="<?=Url::toRoute(['member/more-evaluate','orderid'=>''])?>"+val.orderId;
                                if (val.state == 0){
                                    result+='  <div class="box">\n' +
                                        '      <a href="'+Url+'">\n' +
                                        '        <div class="order_status">\n' +
                                        '          <p>\n' +
                                        '            <img src="/public/wap/images/shop_ico.png"/>\n' +
                                        '            '+val.storeName+'\n' +
                                        '          </p>\n' +
                                        '          <span>'+val.stateStr+'</span>\n' +
                                        '        </div> \n' +
                                        '\n' +
                                        '        <div class="project">\n' +
                                        '          <img src="'+val.goodPic+'"/>\n' +
                                        '          <div class="project_cont">\n' +
                                        '            <h3>'+val.goodName+'</h3>\n' +
                                        '            <span>'+val.brief+'</span>\n' +
                                        '            <p><b>¥</b>'+val.goodPrice+'<label>X'+val.num+'</label></p>\n' +
                                        '          </div>\n' +
                                        '        </div>\n' +
                                        '      </a>\n' +
                                        '      <div class="operating">\n' +
                                        '        <p>优惠码:'+val.orderId+'</p>\n' +
                                        '      </div>\n' +
                                        '    </div>';
                                } else if(val.state == 1){
                                    result+='  <div class="box">\n' +
                                        '      <a href="'+Url+'">\n' +
                                        '        <div class="order_status">\n' +
                                        '          <p>\n' +
                                        '            <img src="/public/wap/images/shop_ico.png"/>\n' +
                                        '            '+val.storeName+'\n' +
                                        '          </p>\n' +
                                        '          <span>'+val.stateStr+'</span>\n' +
                                        '        </div> \n' +
                                        '\n' +
                                        '        <div class="project">\n' +
                                        '          <img src="'+val.goodPic+'"/>\n' +
                                        '          <div class="project_cont">\n' +
                                        '            <h3>'+val.goodName+'</h3>\n' +
                                        '            <span>'+val.brief+'</span>\n' +
                                        '            <p><b>¥</b>'+val.goodPrice+'<label>X'+val.num+'</label></p>\n' +
                                        '          </div>\n' +
                                        '        </div>\n' +
                                        '      </a>\n' +
                                        '      <div class="operating">\n' +
                                        '        <p>优惠码:'+val.orderId+'</p>\n' +
                                        '      </div>\n' +
                                        '    </div>';
                                }else if (val.state == 3){
                                    result+='    <div class="box">\n' +
                                        '      <a href="'+Url+'">\n' +
                                        '        <div class="order_status">\n' +
                                        '          <p>\n' +
                                        '            <img src="/public/wap/images/shop_ico.png"/>\n' +
                                        '            '+val.storeName+'\n' +
                                        '          </p>\n' +
                                        '          <span>'+val.stateStr+'</span>\n' +
                                        '        </div> \n' +
                                        '\n' +
                                        '        <div class="project">\n' +
                                        '          <img src="'+val.goodPic+'"/>\n' +
                                        '          <div class="project_cont">\n' +
                                        '            <h3>'+val.goodName+'</h3>\n' +
                                        '            <span>'+val.brief+'</span>\n' +
                                        '            <p><b>¥</b>'+val.goodPrice+'<label>X'+val.num+'</label></p>\n' +
                                        '          </div>\n' +
                                        '        </div>\n' +
                                        '      </a>\n' +
                                        '      <div class="operating">\n' +
                                        '        <a class="solid" href="'+toEvalate+'">立即评价</a>\n' +
                                        '      </div>\n' +
                                        '    </div>'
                                }else {
                                    result+='    <div class="box">\n' +
                                        '      <a href="'+Url+'">\n' +
                                        '        <div class="order_status">\n' +
                                        '          <p>\n' +
                                        '            <img src="/public/wap/images/shop_ico.png"/>\n' +
                                        '            '+val.storeName+'\n' +
                                        '          </p>\n' +
                                        '          <span><label>已完成</label></span>\n' +
                                        '        </div> \n' +
                                        '\n' +
                                        '        <div class="project">\n' +
                                        '          <img src="'+val.goodPic+'"/>\n' +
                                        '          <div class="project_cont">\n' +
                                        '            <h3>'+val.goodName+'</h3>\n' +
                                        '            <span>'+val.brief+'</span>\n' +
                                        '            <p><b>¥</b>'+val.goodPrice+'<label>X'+val.num+'</label></p>\n' +
                                        '          </div>\n' +
                                        '        </div>\n' +
                                        '      </a>\n' +
                                        '    \n' +
                                        '    </div>'
                                }
                            });
                            break;
                        case 1:
                            $.each(data.list,function(key,val){
                                var Url = "<?=Url::toRoute(['member/my-appointment-detail','orderid'=>''])?>"+val.orderId;
                                var toEvalate ="<?=Url::toRoute(['member/more-evaluate','orderid'=>''])?>"+val.orderId;
                                result+='  <div class="box">\n' +
                                    '      <a href="'+Url+'">\n' +
                                    '        <div class="order_status">\n' +
                                    '          <p>\n' +
                                    '            <img src="/public/wap/images/shop_ico.png"/>\n' +
                                    '            '+val.storeName+'\n' +
                                    '          </p>\n' +
                                    '          <span>'+val.stateStr+'</span>\n' +
                                    '        </div> \n' +
                                    '\n' +
                                    '        <div class="project">\n' +
                                    '          <img src="'+val.goodPic+'"/>\n' +
                                    '          <div class="project_cont">\n' +
                                    '            <h3>'+val.goodName+'</h3>\n' +
                                    '            <span>'+val.brief+'</span>\n' +
                                    '            <p><b>¥</b>'+val.goodPrice+'<label>X'+val.num+'</label></p>\n' +
                                    '          </div>\n' +
                                    '        </div>\n' +
                                    '      </a>\n' +
                                    '      <div class="operating">\n' +
                                    '        <p>优惠码:'+val.orderId+'</p>\n' +
                                    '      </div>\n' +
                                    '    </div>';
                            });
                            break;
                        case 3:
                            $.each(data.list,function(key,val){
                                var Url = "<?=Url::toRoute(['member/my-appointment-detail','orderid'=>''])?>"+val.orderId;
                                var toEvalate ="<?=Url::toRoute(['member/more-evaluate','orderid'=>''])?>"+val.orderId;
                                result+='    <div class="box">\n' +
                                    '      <a href="'+Url+'">\n' +
                                    '        <div class="order_status">\n' +
                                    '          <p>\n' +
                                    '            <img src="/public/wap/images/shop_ico.png"/>\n' +
                                    '            '+val.storeName+'\n' +
                                    '          </p>\n' +
                                    '          <span>'+val.stateStr+'</span>\n' +
                                    '        </div> \n' +
                                    '\n' +
                                    '        <div class="project">\n' +
                                    '          <img src="'+val.goodPic+'"/>\n' +
                                    '          <div class="project_cont">\n' +
                                    '            <h3>'+val.goodName+'</h3>\n' +
                                    '            <span>'+val.brief+'</span>\n' +
                                    '            <p><b>¥</b>'+val.goodPrice+'<label>X'+val.num+'</label></p>\n' +
                                    '          </div>\n' +
                                    '        </div>\n' +
                                    '      </a>\n' +
                                    '      <div class="operating">\n' +
                                    '        <a class="solid" href="'+toEvalate+'">立即评价</a>\n' +
                                    '      </div>\n' +
                                    '    </div>'
                            });
                            break;
                        default:
                    }
                    $('.cont').append(result);
                    // 每次数据插入，必须重置
                    me.resetload();
                },'json');
            }else{

                // 锁定
                me.lock();
                // 无数据
                me.noData();
                // 为了测试，延迟1秒加载
                // 插入数据到页面，放到最后面
                //$('.conAjax').append(result);

                //settime();
                // 每次数据插入，必须重置
                me.resetload();

            }
        }
    });

</script>