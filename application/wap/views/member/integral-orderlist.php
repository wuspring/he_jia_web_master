<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/5
 * Time: 14:28
 */

use yii\helpers\Url;

?>
<?php
$stateArr = \wap\logic\IntegralOrderLogic::init()->getAppointmentOrderCount();
?>

<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>

<div class="head">
    <a href="<?=Url::toRoute(['member/index'])?>"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>积分订单</h2>
</div>
<!--- 我的订单 -->
<div id="my_order">
    <div class="nav">
        <a class="<?=($state==-1)?'cur':''?>" href="<?=Url::toRoute(['member/integral-orderlist','state'=>-1])?>">全部(<?=$stateArr['allNum']?>)</a>
        <a class="<?=($state==1)?'cur':''?>" href="<?=Url::toRoute(['member/integral-orderlist','state'=>1])?>">待发货(<?=$stateArr['daiFaNum']?>)</a>
        <a class="<?=($state==2)?'cur':''?>" href="<?=Url::toRoute(['member/integral-orderlist','state'=>2])?>">待收货(<?=$stateArr['daiShouNum']?>)</a>
        <a class="<?=($state==3)?'cur':''?>" href="<?=Url::toRoute(['member/integral-orderlist','state'=>3])?>">待评价(<?=$stateArr['daiPingNum']?>)</a>

    </div>
    <div class="cont" id="my_order_ajax">

    </div>
</div>



<!-- 模态框- 确认收货 弹框 -->
<div class="modal modal_explain modal_explain_sm fade" id="myModalCasesure">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">确认收货</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align">是否确认收货？</p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <button type="button" class="btn btn_outline" data-dismiss="modal" style="width: 40%;min-width: 100px;">取 消</button>
                <button type="button" class="btn sureReceiving" data-dismiss="modal" data-order="" style="width: 40%;min-width: 100px;">确 定</button>
            </div>
        </div>
    </div>
</div>
<script>
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var obj ={};
    // 页数
    var page = 0;
    // 每页展示5个
    var size = 10;
    var sum='<?php echo $pages->totalCount;?>';
    var p= Math.ceil(sum/size);
    var state = '<?=empty($state)?0:$state?>';
    var result='';
    var url= "<?=Url::toRoute(['member/integral-orderdetail'])?>"+"&orderid=";
    var statusStr=['立即支付','待发货','确认收货','立即评价'];
    $('#my_order').dropload({
        scrollArea : window,
        distance:10,
        loadDownFn : function(me){
            if (page<p) {
                page++;
                // 拼接HTML
                $.get('<?php echo Url::toRoute(["member/integral-orderlist"]);?>',{page:page,'per-page':size,state:state},function(r){

                    $.each(r.list, function (key, val) {
                        var  toEvaluate = "<?=Url::toRoute(['member/evaluate','orderid'=>''])?>"+key;
                        var toPay="<?=Url::toRoute(['integral-order/pay','number'=>''])?>"+key;
                        result += '        <div class="box">\n' +
                            '            <a href="'+url+key+'">\n' +
                            '                <div class="order_status">\n' +
                            '                    <p>订单号：'+key+'</p>\n' +
                            '                    <span>'+val[0].order_state_str+'</span>\n' +
                            '                </div>';
                        $.each(val, function (sonk, sonv) {
                            result += '<div class="project">\n' +
                                '                    <img src="'+sonv.goods_pic+'"/>\n' +
                                '                    <div class="project_cont">\n' +
                                '                        <h3>'+sonv.name+'</h3>\n' +
                                '                        <span>'+sonv.attr+'</span>\n' +
                                '                        <p>'+sonv.goods_price+'积分<label>X'+sonv.num+'</label></p>\n' +
                                '                    </div>\n' +
                                '                </div>'
                        });
                        var stateNum=val[0].order_state;
                        if (stateNum == 0){
                            result += ' </a>\n' +
                                '            <div class="operating">\n' +
                                '                <a class="solid" href="'+toPay+'">'+statusStr[stateNum]+'</a>\n' +
                                '            </div>\n' +
                                '        </div>';
                        }else if (stateNum == 1) {
                            result += ' </a>\n' +
                                '        </div>';
                        }else if (stateNum == 2){
                            result += ' </a>\n' +
                                '            <div class="operating">\n' +
                                '                <a class="solid sureReceiving" data-order="'+key+'" href="javascript:void(0)">'+statusStr[stateNum]+'</a>\n' +
                                '            </div>\n' +
                                '        </div>';
                        } else if (stateNum == 3) {
                            result += ' </a>\n' +
                                '            <div class="operating">\n' +
                                '                <a class="solid" href="'+toEvaluate+'">'+statusStr[stateNum]+'</a>\n' +
                                '            </div>\n' +
                                '        </div>';
                        }
                    });

                    $('#my_order_ajax').append(result);
                    // 每次数据插入，必须重置
                    me.resetload();
                },'json');
            }else{

                // 锁定
                me.lock();
                // 无数据
                me.noData();
                // 为了测试，延迟1秒加载
                // 插入数据到页面，放到最后面
                //$('.conAjax').append(result);

                //settime();
                // 每次数据插入，必须重置
                me.resetload();

            }
        }
    });

    //确认收货
    $(document).on('click','.sureReceiving',function () {
        var  dataorder = $(this).attr('data-order');
            $("#myModalCasesure").modal('show');
            $("#myModalCasesure").find('.btn').attr('data-order',dataorder);
    });

    $(".sureReceiving").on('click',function () {
          var  dataOrderId = $(this).attr('data-order');
          console.log(dataOrderId);

          var  goEvaluate = "<?=Url::toRoute(['member/evaluate','orderid'=>''])?>"+dataOrderId;
          var  receivingUrl = "<?=Url::toRoute(['member/receiving-good'])?>";

          $.post(receivingUrl,{orderid:dataOrderId},function (res) {
              alert(res.msg);
              if (res.code==1){
                  window.location.href=goEvaluate
              }
          },'json');

    })

</script>



