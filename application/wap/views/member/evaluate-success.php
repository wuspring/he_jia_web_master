<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/6
 * Time: 11:08
 */
use  yii\helpers\Url;
$this->title = '评论成功';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>评论成功</h2>
</div>
<!--- 兑换成功 -->
<div id="success">

    <div class="tip">
        <div class="box">
            <img src="/public/wap/images/success_ico.png"/>
            <h2>评论成功</h2>
            <p>已经评论成功，您的评论是我们前进的动力~</p>
            <div class="clear"></div>
        </div>
    </div>

    <div class="operating">
        <a class="button1" href="<?=Url::toRoute(['index/index'])?>">返回首页</a>
        <a class="button2" href="<?=Url::toRoute(['member/evaluate-list'])?>">查看评价</a>
    </div>

    <!--- 热门商品 -->
    <div class="pro_list">
        <div class="title">
            <h2>热门商品</h2>
            <a href="<?=Url::toRoute(['goods/lists','type'=>'COUPON'])?>">查看更多></a>
        </div>
        <div class="cont">
            <ul>
                <?php foreach ($goods as $k=>$v):?>
                    <li>
                        <a href="<?=Url::toRoute(['goods/detail','id'=>$v->id])?>">
                            <img style="height: 110px;" src="<?=$v->goods_pic?>"/>
                            <p><?=$v->goods_name?></p>
                            <span>¥<b><?=$v->goods_price?></b></span>
                        </a>
                    </li>
                <?php endforeach;?>
                <div class="clear"></div>
            </ul>

        </div>

    </div>