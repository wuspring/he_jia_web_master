<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/11
 * Time: 10:30
 */
use  yii\helpers\Url;
use wap\models\Order;
?>
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>订单详情</h2>
</div>
<!--- 商品订单详情 -->
<div id="order_details_pro">

    <div class='casedetail_status pure'>
        <?php if ($orderDetail->order_state == Order::STATUS_PAY || $orderDetail->order_state == Order::STATUS_WAIT ):?>
            <div class="casedetail_status_top"><img src="/public/wap/images/order_details_pro_ico1.png">待核销</div>
            <p>亲，请前往店铺消费~</p>
        <?php elseif ($orderDetail->order_state == Order::STATUS_RECEIVED):?>
            <div class="casedetail_status_top"><img src="/public/wap/images/order_details_pro_ico1.png">待评价</div>
            <p>亲，我们很在意您的评价哦~</p>
        <?php endif;?>

    </div>

    <div class="order_lists">
        <div class="box">
            <a href="<?=Url::toRoute(['shop/detail','id'=>$user->id])?>">
                <div class="order_status">
                    <p>
                        <img src="/public/wap/images/shop_ico.png">
                        <?=$user->nickname?>优惠券
                    </p>
                    <span><?=$orderDetail->orderState?></span>
                </div>

            </a>

            <?php foreach ($orderGoods as $k=>$item):?>
            <a href="<?=Url::toRoute(['goods/detail','id'=>$item->goods_id])?>" class="project">
                <img src="<?=$item->goods_pic?>">
                <div class="project_cont">
                    <h3><?=$item->goods_name?></h3>
                    <span><?=$item->goods->mobile_body?></span>
                    <p><b>¥</b><?=$item->goods_price?><label>X<?=$item->goods_num?></label></p>
                </div>
            </a>
            <?php endforeach;?>
        </div>
    </div>
    <div class="casedetail_center">
        <div class="order_tit">优惠码</div>
        <div class="yhm orange_color"><p><?=$orderDetail->orderid?></p></div>
    </div>
    <div class="casedetail_center">
        <div class="order_tit">订单信息</div>
        <ul>
            <li>订单编号：<?=$orderDetail->orderid?></li>
            <?php if ($orderDetail->order_state >= Order::STATUS_RECEIVED ):?>
                <li>总价：¥ <?=$orderDetail->order_amount?></li>
            <?php endif;?>
            <li>预约时间：<?=$orderDetail->add_time?></li>
        </ul>
    </div>
    <?php if ($orderDetail->order_state == Order::STATUS_RECEIVED):?>
        <div class="placeholder"></div><!--- 占位50px -->
        <div class="operating">
            <a href="<?=Url::toRoute(['member/more-evaluate','orderid'=>$orderDetail->orderid])?>">立即评价</a>
        </div>
    <?php endif;?>


</div>
