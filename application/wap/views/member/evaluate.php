<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/5
 * Time: 18:16
 */
use  yii\helpers\Url;
?>
<style>
    .sc_pic{
        height: 80px;
        width: 80px;
    }
    .closeImg{
        float: right;
    }

</style>
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <a href="javascript:void(0)" class="a_right orange tiJiao">发布</a>
    <h2>评论</h2>
</div>
<!--- 评论 -->
<div id="comment">

    <?php foreach ($intergral_order_goods as $k=>$v):?>

    <a href="<?=Url::toRoute(['integral-treasure/detail','id'=>$v->goods_id])?>" class="project">
            <img src="<?=$v->goods_pic?>"/>
            <h3><?=$v->goods_name?></h3>
            <p><?=empty($v->attr)?'':$v->attr?></p>
            <span><?=intval($v->goods_price)?>积分</span>
    </a>

    <div class="operating">
        <div class="title">我们很在意您的评价，感谢您的描述！</div>
        <div class="star">
            <span>商品质量：</span>
            <ul class="acf_star spzl">
                <li data-val='1'></li>
                <li data-val='2'></li>
                <li data-val='3'></li>
                <li data-val='4'></li>
                <li data-val='5'></li>
            </ul>
            <div class="clear"></div>
        </div>
        <div class="star">
            <span>商户服务：</span>
            <ul class="acf_star2 shfw">
                <li data-val='1'></li>
                <li data-val='2'></li>
                <li data-val='3'></li>
                <li data-val='4'></li>
                <li data-val='5'></li>
            </ul>
            <div class="clear"></div>
        </div>
        <div class="textarea "><textarea class="textarea_str" placeholder="请输入您的评语"></textarea></div>
        <ul class="poto list-unstyled" style="overflow: hidden">
            <li>
                    <div class="camera upload_btn">
                        <input class="file" type="file" multiple accept="image/*" />
                        <img src="/public/wap/images/camera_ico.jpg"/>
                    </div>
            </li>

        </ul>
    </div>
    <?php endforeach;?>
</div>

<script>
    $(function(){
        $(".acf_star li").mouseenter(function(){
            $(this).addClass('active').siblings().addClass('active');
            $(this).nextAll().removeClass('active');
        });
        $(".acf_star2 li").mouseenter(function(){
            $(this).addClass('active').siblings().addClass('active');
            $(this).nextAll().removeClass('active');
        })
    });
</script>

<script type="text/javascript">
    var imgArr = new Array();
    var spzl=new Array();  //质量评分
    var shfw=new Array(); //商户评分
    var textarea=new Array();  //内容
    var is_sucess=0;

    $(".tiJiao").on('click',function () {
       // 质量评分
        $(".spzl").each(function(index,item){
            spzl.push($(this).children('.active').length);
        });
      //  服务评分
        $(".shfw").each(function(index,item){
            shfw.push($(this).children('.active').length);
        });

        // for (var i=0;i<2;i++){
        //     spzl.push(3);
        //     shfw.push(3);
        // }

        //内容信息
        $(".textarea_str").each(function (index,item) {
            textarea.push($(this).val());
        });
        //图片数组
        $(".list-unstyled").each(function (index,item) {
            var img_arr=$(this).children('li:not(".upload_btn")').children('img');
            var arr=[];
            $.each(img_arr,function (ind,objff) {
                arr.push($(objff).attr('src'));
            });
            imgArr.push(arr);
            arr=[];
        });

        $.ajax({
            type: "POST", // 数据提交类型
            url: "<?=Url::toRoute(['member/evaluate-post',]);?>", // 发送地址
            data: {
                orderid:"<?=$intergralOrder->orderid?>",
                spzl:spzl,
                shfw:shfw,
                textarea:textarea,
                img:imgArr
            }, //发送数据
           // async: true, // 是否异步
            dataType: 'json',
            success: function (res) {
                console.log(res);
                if (res.code==1){
                    window.location.href='<?=Url::toRoute(['member/evaluate-success'])?>';
                } else {
                    alert("评价失败");
                    window.location.reload();
                }
              //  is_sucess=res.code;
            },
        });
    });

    // 上传图片过程
    $(".upload_btn").on('change',"input[type=file]",function () {

        var filePath = $(this).val();  //读取图片路径

        var imgobj=this.files;  //读取图片对象
        var skuimg=$(this);

        for (var i = 0; i < imgobj.length; i++) {
            var formData = new FormData();

            formData.append("file", this.files[i]);
            $.ajax({
                type: "POST", // 数据提交类型
                url: "<?=Url::toRoute(['member/uploadimgv']);?>", // 发送地址
                data: formData, //发送数据
                async: true, // 是否异步
                dataType: 'json',
                processData: false, //processData 默认为false，当设置为true的时候,jquery ajax 提交的时候不会序列化 data，而是直接使用data
                contentType: false, //
                success: function (re) {
                    if (re.code == 0) {
                        var html = '<li>\n' +
                            ' <em class="closeImg">×</em>\n' +
                            ' <img style="height:80px" src="' + re.attachment + '" class="sc_pic" alt="">\n' +
                            ' </li>';
                        $(skuimg).parent().parent().parent().append(html);
                    }else {
                        alert("上传失败")
                    }
                },
                error: function () {
                    alert("上传失败")
                },
            });
        }
    });

    //删除图片
    $(document).on('click', '.closeImg', function () {
        $(this).parent().remove();
        var url = $(this).parent().children("img").attr("src");
        // imgArr.splice($.inArray(url, imgArr), 1);
    });


</script>