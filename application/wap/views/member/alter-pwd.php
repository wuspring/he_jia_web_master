<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/5
 * Time: 18:16
 */
use  yii\helpers\Url;
?>

<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>修改密码</h2>
</div>
<!-- 注册 -->
<div id="login">
    <!-- Tab内容 -->
    <div class="tab_con tab-content">
        <div id="home" class="container tab-pane active"><br>
            <ul>
                <li><input class="text text_icon2" type="password" name="originPwd" placeholder="请输入原始密码"/></li>
                <li><input class="text text_icon2" type="password"  name="newPwd" placeholder="请输入新密码"/></li>
                <li><input class="text text_icon2" type="password" name="surePwd" placeholder="请确认新密码"/></li>
                <li><input class="submit" type="submit" value="提交修改"/></li>
            </ul>
        </div>
    </div>
</div>


<!-- 模态框- 提醒弹框-3s自动消失 -->
<div class="modal modal_explain modal_explain_sm modal_remind fade" id="myModalRemind">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">提醒</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align" id="tipInfo"></p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn" data-dismiss="modal">确 定</a>
            </div>
        </div>
    </div>
</div>
<script>

    //点击提交
    $(".submit").on('click',function () {
        var  originPwd=$("input[name='originPwd']").val();
        var  newPwd=$("input[name='newPwd']").val();
        var  surePwd=$("input[name='surePwd']").val();
        if (originPwd.length==0){
            $("#myModalRemind").modal('show');
            $("#tipInfo").text('原始密码不能为空');
           return;
        }
        if (newPwd.length==0){
            $("#myModalRemind").modal('show');
            $("#tipInfo").text('新密码不能为空');
            return;
        }
        if (surePwd.length==0){
            $("#myModalRemind").modal('show');
            $("#tipInfo").text('再次输入密码不能为空');
            return;
        }
        if (newPwd.length<5||newPwd.length>13) {
            $("#myModalRemind").modal('show');
            $("#tipInfo").text('密码应在6~12个字符之间');
            return;
        }
        if (newPwd!=surePwd){
            $("#myModalRemind").modal('show');
            $("#tipInfo").text('两次输入的密码不一致');
            return;
        }

        $.post('<?php echo Url::toRoute(['member/alter-pwd']);?>',{originPwd:originPwd,newPwd:newPwd,surePwd:surePwd},function(res){

            if (res.code==1){
                $("#myModalRemind").modal('show');
                $("#tipInfo").text(res.msg);
                $("#myModalRemind").find('.btn').addClass('clickToSelf');
            }else{
                $("#myModalRemind").modal('show');
                $("#tipInfo").text(res.msg);
            }
        },'json')

    });
    //点击到个人中心
    $(document).on('click','.clickToSelf',function () {
        window.location.href='<?=Url::toRoute(['member/member-edit'])?>';
    })

</script>