<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/4
 * Time: 11:07
 */
$this->title = '我的预定';
$this->params['breadcrumbs'][] = $this->title;
use  yii\helpers\Url;
use  wap\logic\MemberOrderLoginc;
?>
<?php
$stateArr = MemberOrderLoginc::init()->getOrderState();
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>
<div class="head">
    <a href="<?=Url::toRoute(['member/index'])?>"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>我的预定</h2>
</div>
<!--- 我的订单 -->
<div id="my_order">
    <div class="nav">
        <a class="<?=($state==-1)?'cur':''?>" href="<?=Url::toRoute(['member/my-reserve','state'=>-1])?>">全部(<?=$stateArr['allCount']?>)</a>
        <a class="<?=($state==0)?'cur':''?>" href="<?=Url::toRoute(['member/my-reserve','state'=>0])?>">待付款(<?=$stateArr['daiZhiFu']?>)</a>
        <a class="<?=($state==1)?'cur':''?>" href="<?=Url::toRoute(['member/my-reserve','state'=>1])?>">待使用(<?=$stateArr['daiShiYong']?>)</a>
        <a class="<?=($state==3)?'cur':''?>" href="<?=Url::toRoute(['member/my-reserve','state'=>3])?>">待评价(<?=$stateArr['daiPingJia']?>)</a>
    </div>
    <div class="cont">

    </div>
</div>

<!-- 模态框- 文本提醒-简易版 弹框 -->
<div class="modal modal_explain modal_explain_sm fade" id="myModalExplain">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">信息</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align tipInfor">确认取消订单吗?</p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <button type="button" class="btn cancelSure" orderId="" data-dismiss="modal">确 定</button>
            </div>
        </div>
    </div>
</div>

<script>
    var _csrf="<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var obj ={};
    // 页数
    var page = 0;
    // 每页展示5个
    var size = 10;
    var sum='<?php echo $pages->totalCount;?>';
    var p= Math.ceil(sum/size);
    var state = '<?=empty($state)?0:$state?>';
    var result='';
    var toDetail ='';

    $('#my_order').dropload({
        scrollArea : window,
        distance:10,
        loadDownFn : function(me){
            if (page<p) {
                page++;
                // 拼接HTML
                result = '';
                $.get('<?php echo Url::toRoute(["member/my-reserve"]);?>',{page:page,'per-page':size,state:state},function(data){

                    switch (parseInt(data.state)) {
                        case -1:
                            $.each(data.list,function(key,val){
                                toDetail = "<?=Url::toRoute(['member/my-reserve-detail','orderid'=>''])?>"+val.orderId;
                                var toPay="<?=Url::toRoute(['order/submit','number'=>''])?>"+val.orderId;
                                var toEvalate ="<?=Url::toRoute(['member/more-evaluate','orderid'=>''])?>"+val.orderId;
                                result+=' <div class="box">\n' +
                                    '      <a href="'+toDetail+'">\n' +
                                    '        <div class="order_status">\n' +
                                    '          <p>\n' +
                                    '            <img src="/public/wap/images/shop_ico.png"/>\n' +
                                    '            '+val.storeName+'\n' +
                                    '          </p>\n' +
                                    '          <span>'+val.stateStr+'</span>\n' +
                                    '        </div> \n' +
                                    '\n' +
                                    '        <div class="project">\n' +
                                    '          <img src="'+val.pic+'"/>\n' +
                                    '          <div class="project_cont">\n' +
                                    '            <h3>'+val.goodName+'</h3>\n' +
                                    '            <span>专享价：￥'+val.goods_price+'<s>￥'+val.goods_marketprice+'</s></span>\n' +
                                    '            <p><b>¥</b>'+val.dingJin+'<label>X'+val.num+'</label></p>\n' +
                                    '          </div>\n' +
                                    '        </div>\n' +
                                    '      </a>\n';

                                    if (val.state==0){
                                        result+=' <div class="operating">\n' +
                                            '        <a class="hollow cancelOrder" data-order='+val.orderId+' href="javascript:void(0)">取消订单</a>\n' +
                                            '        <a class="solid" href="'+toPay+'">立即付款</a>\n' +
                                            '      </div>\n' +
                                            '    </div>';
                                    }else if (val.state==1) {
                                        result+='  <div class="operating">\n' +
                                            '        <p>优惠码：201905154575454555</p>\n' +
                                            '      </div>\n' +
                                            '    </div>';
                                    }else if (val.state==3){

                                        result+='<div class="operating">\n' +
                                            '        <a class="solid" href="'+toEvalate+'">立即评价</a>\n' +
                                            '      </div>\n' +
                                            '    </div>';
                                    }else {
                                        result+='</div>';
                                    }
                            });
                            break;
                        case 0:
                            $.each(data.list,function(key,val){
                                toDetail = "<?=Url::toRoute(['member/my-reserve-detail'])?>"+"&orderid="+val.orderId;
                                var toPay="<?=Url::toRoute(['order/submit','number'=>''])?>"+val.orderId;
                                result+=' <div class="box">\n' +
                                    '      <a href="'+toDetail+'">\n' +
                                    '        <div class="order_status">\n' +
                                    '          <p>\n' +
                                    '            <img src="/public/wap/images/shop_ico.png"/>\n' +
                                    '            '+val.storeName+'\n' +
                                    '          </p>\n' +
                                    '          <span>'+val.stateStr+'</span>\n' +
                                    '        </div> \n' +
                                    '\n' +
                                    '        <div class="project">\n' +
                                    '          <img src="'+val.pic+'"/>\n' +
                                    '          <div class="project_cont">\n' +
                                    '            <h3>'+val.goodName+'</h3>\n' +
                                    '            <span>专享价：￥'+val.goods_price+'<s>￥'+val.goods_marketprice+'</s></span>\n' +
                                    '            <p><b>¥</b>'+val.dingJin+'<label>X'+val.num+'</label></p>\n' +
                                    '          </div>\n' +
                                    '        </div>\n' +
                                    '      </a>\n'+
                                    ' <div class="operating">\n' +
                                        '        <a class="hollow cancelOrder" data-order='+val.orderId+' href="javascript:void(0)">取消订单</a>\n' +
                                        '        <a class="solid" href="'+toPay+'">立即付款</a>\n' +
                                        '      </div>\n' +
                                        '    </div>';
                            });
                            break;
                        case 1:
                            $.each(data.list,function(key,val){
                                toDetail = "<?=Url::toRoute(['member/my-reserve-detail'])?>"+"&orderid="+val.orderId;
                                var toEvalate ="<?=Url::toRoute(['member/more-evaluate','orderid'=>''])?>"+val.orderId;
                                result+=' <div class="box">\n' +
                                    '      <a href="'+toDetail+'">\n' +
                                    '        <div class="order_status">\n' +
                                    '          <p>\n' +
                                    '            <img src="/public/wap/images/shop_ico.png"/>\n' +
                                    '            '+val.storeName+'\n' +
                                    '          </p>\n' +
                                    '          <span>'+val.stateStr+'</span>\n' +
                                    '        </div> \n' +
                                    '\n' +
                                    '        <div class="project">\n' +
                                    '          <img src="'+val.pic+'"/>\n' +
                                    '          <div class="project_cont">\n' +
                                    '            <h3>'+val.goodName+'</h3>\n' +
                                    '            <span>专享价：￥'+val.goods_price+'<s>￥'+val.goods_marketprice+'</s></span>\n' +
                                    '            <p><b>¥</b>'+val.dingJin+'<label>X'+val.num+'</label></p>\n' +
                                    '          </div>\n' +
                                    '        </div>\n' +
                                    '      </a>\n'+
                                    '  <div class="operating">\n' +
                                    '        <p>优惠码：201905154575454555</p>\n' +
                                    '      </div>\n' +
                                    '    </div>';
                            });
                            break;
                        case 3:
                            $.each(data.list,function(key,val){
                                toDetail = "<?=Url::toRoute(['member/my-reserve-detail'])?>"+"&orderid="+val.orderId;
                                var toEvalate ="<?=Url::toRoute(['member/more-evaluate','orderid'=>''])?>"+val.orderId;
                                result+=' <div class="box">\n' +
                                    '      <a href="'+toDetail+'">\n' +
                                    '        <div class="order_status">\n' +
                                    '          <p>\n' +
                                    '            <img src="/public/wap/images/shop_ico.png"/>\n' +
                                    '            '+val.storeName+'\n' +
                                    '          </p>\n' +
                                    '          <span>'+val.stateStr+'</span>\n' +
                                    '        </div> \n' +
                                    '\n' +
                                    '        <div class="project">\n' +
                                    '          <img src="'+val.pic+'"/>\n' +
                                    '          <div class="project_cont">\n' +
                                    '            <h3>'+val.goodName+'</h3>\n' +
                                    '            <span>专享价：￥'+val.goods_price+'<s>￥'+val.goods_marketprice+'</s></span>\n' +
                                    '            <p><b>¥</b>'+val.dingJin+'<label>X'+val.num+'</label></p>\n' +
                                    '          </div>\n' +
                                    '        </div>\n' +
                                    '      </a>\n'+
                                    '      <div class="operating">\n' +
                                    '        <a class="solid" href="'+toEvalate+'">立即评价</a>\n' +
                                    '      </div>\n' +
                                    '    </div>';
                            });
                            break;

                        default:
                    }
                    $('.cont').append(result);
                    // 每次数据插入，必须重置
                    me.resetload();
                },'json');
            }else{

                // 锁定
                me.lock();
                // 无数据
                me.noData();
                // 为了测试，延迟1秒加载
                // 插入数据到页面，放到最后面
                //$('.conAjax').append(result);

                //settime();
                // 每次数据插入，必须重置
                me.resetload();

            }
        }
    });

    //点击取消订单
    $(document).on('click','.cancelOrder',function () {
        var orderId=$(this).attr("data-order");
        $("#myModalExplain").modal('show');
        $("#myModalExplain").find('.btn').attr('orderId',orderId);
    });

    //取消订单
    $(document).on('click','.cancelSure',function () {
        var cancelUrl="<?=Url::toRoute(['member/cancel-reserve'])?>";
        var cancelOrder=$(this).attr('orderId');
        $.post(cancelUrl,{orderid:cancelOrder},function (res) {
             alert(res.msg);
             if (res.code == 1){
                 window.location.reload();
             }
        },'json');
    })
</script>