<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/5
 * Time: 17:31
 */

$this->title = '订单详情';
$this->params['breadcrumbs'][] = $this->title;
use  yii\helpers\Url;
?>
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>订单详情</h2>
</div>
<!--- 商品订单详情 -->
<div id="order_details_pro">
    <div class='casedetail_status pure'>
        <?php if ($data->order_state == 0):?>
            <div class="casedetail_status_top"><img src="/public/wap/images/order_details_pro_ico3.png">待付款</div>
            <p>亲，请立即付款预定哦~</p>
        <?php elseif ($data->order_state == 1):?>
            <div class="casedetail_status_top"><img src="/public/wap/images/order_details_pro_ico1.png">待使用</div>
            <p>亲，请前往店铺消费~</p>
        <?php elseif ($data->order_state == 3):?>
            <div class="casedetail_status_top"><img src="/public/wap/images/order_details_pro_ico1.png">待评价</div>
            <p>亲，我们很在意您的评价哦~</p>
        <?php elseif ($data->order_state == 4):?>
            <div class="casedetail_status_top"><img src="/public/wap/images/order_details_pro_ico4.png">已完成</div>
            <p>亲，预定订单交易完成~</p>
        <?php endif;?>
    </div>

    <div class="order_lists">
        <div class="box">
            <a href="<?=Url::toRoute(['shop/detail','id'=>$storeInfo->id])?>">
                <div class="order_status">
                    <p>
                        <img src="/public/wap/images/shop_ico.png">
                        <?=$storeInfo->nickname?>
                    </p>
                    <span><?=$data->orderState?></span>
                </div>
            </a>
                <a href="<?=Url::toRoute(['goods/detail','id'=>$goodDetail->id])?>" class="project">
                    <img src="<?=$goodDetail->goods_pic?>">
                    <div class="project_cont">
                        <h3><?=$goodDetail->goods_name?></h3>
                        <span>专享价：￥<?=intval($goodDetail->goods_price)?>/<?=$goodDetail->unit?><s>￥<?=intval($goodDetail->goods_marketprice)?>/<?=$goodDetail->unit?></s></span>
                        <p><b>¥</b><?=$ticketDetail->order_price?><label>X1</label></p>
                    </div>
                </a>
        </div>
    </div>

    <?php if ($data->order_state >0):?>
        <div class="casedetail_center">
            <div class="order_tit">优惠码</div>
            <div class="yhm gray_color"><p><?=$systemCoupon->code?></p></div>
        </div>
    <?php endif;?>

    <div class="casedetail_center">
        <div class="order_tit">订单信息</div>
        <ul>
            <li>订单编号：<?=$data->orderid?></li>
            <li>总价：¥ <?=$data->order_amount?></li>
            <li>预约状态：<?=$data->orderState?></li>
        </ul>
    </div>
    <div class="placeholder"></div><!--- 占位50px -->

    <?php if ($data->order_state == 0):?>
        <div class="operating">
            <p>合计<span>¥<b><?=$data->order_amount?><b></span></p>
            <a href="<?=Url::toRoute(['payment/pay','number'=>$data->orderid])?>">立即付款</a>
        </div>
    <?php elseif ($data->order_state == 1):?>

    <?php elseif($data->order_state == 3):?>
        <div class="operating">
            <a href="<?=Url::toRoute(['member/more-evaluate','orderid'=>$data->orderid])?>">立即评价</a>
        </div>
    <?php endif;?>


</div>