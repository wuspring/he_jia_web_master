<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/5
 * Time: 18:16
 */
use  yii\helpers\Url;
?>

<!--- 用户中心首页 -->
<div id="user">
    <div class="user_bg">
        <h3 class="user_title">个人中心</h3>
        <div class="user_info">
                 <div class="user_head">
                     <a href="<?=Url::toRoute(['member/member-edit'])?>">
                         <img src="<?=empty($model->avatar)?'/public/wap/images/user_head.jpg':$model->avatar?>"/>
                         <h3><?=empty($model->nickname)?'-':$model->nickname?></h3>
                         <p><?=empty($model->mobile)?'-':$model->mobile?></p>
                     </a>
                 </div>
                 <div class="tag <?=$qianDao?'active':''?>"><a href="<?=Url::toRoute(['integral-menu/integral-signin'])?>">签到领积分</a></div>
                 <div class="user_data">
                     <div class="line"></div>
                     <ul>
                         <li><a  href="<?=Url::toRoute(['integral-menu/my-integral'])?>"><p><?=empty($model->integral)?'0':$model->integral?></p><span>积分</span></a></li>
                         <li><a href="<?=Url::toRoute(['member/my-coupon'])?>"><p><?=empty($couponCount)?'0':$couponCount?></p><span>优惠券</span></a></li>
                         <div class="clear"></div>
                     </ul>
                 </div>
        </div>
        <div class="user_nav">
            <ul>
                    <li><a href="<?=Url::toRoute(['member/my-reserve'])?>"><img src="/public/wap/images/user_nav_ico1.png"/><p>我的预定</p></a></li>
                    <li><a href="<?=Url::toRoute(['member/my-appointment'])?>"><img src="/public/wap/images/user_nav_ico2.png"/><p>我的预约</p></a></li>
                    <li><a href="<?=Url::toRoute(['member/my-ticket'])?>"><img src="/public/wap/images/user_nav_ico3.png"/><p>我的门票</p></a></li>
                    <li><a href="<?=Url::toRoute(['member/integral-orderlist'])?>"><img src="/public/wap/images/user_nav_ico4.png"/><p>积分订单</p></a></li>
                    <li><a href="<?=Url::toRoute(['member/evaluate-list'])?>"><img src="/public/wap/images/user_nav_ico5.png"/><p>我的评论</p></a></li>
                    <li><a href="<?=Url::toRoute(['integral-treasure/my-record'])?>"><img src="/public/wap/images/user_nav_ico6.png"/><p>我的夺宝</p></a></li>
                    <li><a href="<?=Url::toRoute(['member/my-collect'])?>"><img src="/public/wap/images/user_nav_ico7.png"/><p>我的收藏</p></a></li>
                    <li><a href="<?=Url::toRoute(['member/history'])?>"><img src="/public/wap/images/user_nav_ico8.png"/><p>浏览记录</p></a></li>
                <div class="clear"></div>
            </ul>
        </div>
    </div>

    <!--- 热门商品 -->
    <div class="pro_list">
        <div class="title">
            <h2>热门推荐</h2>
            <?php if ($is_zhanhui==1):?>
                <a href="<?=Url::toRoute(['goods/lists','type'=>'COUPON'])?>">查看更多></a>
            <?php else:?>
                <a href="<?=Url::toRoute(['goods/lists'])?>">查看更多></a>
            <?php endif;?>

        </div>
        <div class="cont">
            <ul>
                <?php foreach ($goods as $k=>$v):?>
                    <li>
                        <a href="<?=Url::toRoute(['goods/detail','id'=>$v->id])?>">
                            <img style="height: 110px" src="<?=$v->goods_pic?>"/>
                            <p><?=$v->goods_name?></p>
                            <span>¥<b><?=$v->goods_price?></b></span>
                        </a>
                    </li>
                <?php endforeach;?>
                <div class="clear"></div>
            </ul>
        </div>
    </div>
</div>
<?php if ($select_menu == 0):?>
    <?php include __DIR__ . '/common/footer_sf.php'; ?>
<?php else:?>

    <?php include __DIR__ . '/common/footer_xf.php'; ?>
<?php  endif;?>
