<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/6
 * Time: 10:58
 *
 */
$this->title = '引导评论';
$this->params['breadcrumbs'][] = $this->title;
use  yii\helpers\Url;
?>

<div>
    <p>确认收货成功</p>
    <a href="<?=Url::toRoute(['index/index'])?>">返回首页</a>
    <a href="<?=Url::toRoute(['member/evaluate','orderid'=>$intergralOrder->orderid])?>">立即评价</a>

</div>
