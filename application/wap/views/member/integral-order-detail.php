<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/5
 * Time: 17:31
 */
use  yii\helpers\Url;
?>

<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>订单详情</h2>
</div>
<!--- 订单详情 -->
<div id="order_details_integral">
    <div class="casedetail_status">
        <?php
         switch ($intergralOrder->order_state){
             case 0:
                 echo  '<div class="casedetail_status_top">待支付</div><p>商品已发货，请耐心等待运输哦~</p>
                        <img src="/public/wap/images/order_ico1.png"/>';
                 break;
             case 1:
                 echo  '<div class="casedetail_status_top">待发货</div><p>已经尽快备货中，请耐心等待~</p>
                        <img src="/public/wap/images/order_ico1.png"/>';
                 break;
             case 2:
                 echo  '<div class="casedetail_status_top">待收货</div><p>商品已发货，请耐心等待运输哦~</p>
                        <img src="/public/wap/images/order_ico3.png"/>';
                 break;
             case 3:
                 echo  '<div class="casedetail_status_top">待评价</div><p>亲，请留下您的宝贵评价哦~</p>
                        <img src="/public/wap/images/order_ico2.png"/>';
                 break;
             case 4:
                 echo  '<div class="casedetail_status_top">订单完成</div><p>感谢你的评价</p>
                        <img src="/public/wap/images/order_ico1.png"/>';
                 break;
             default:
                 echo '异常';
         }
        ?>

    </div>
    <div class="casedetail_address">
        <a href="javascript:void(0)">
            <img src="/public/wap/images/location.jpg" class="location">
            <div class="cs_middle">
                <div class="v1"><?=$intergralOrder->receiver_name?>  <?=$intergralOrder->receiver_mobile?></div>
                <div class="v2"><?=$intergralOrder->receiver_address?></div>
            </div>
            <div class="cs_right"><img src="/public/wap/images/arrow.jpg" alt=""></div>
        </a>
    </div>
    <div class="order_lists">
        <ul>
            <?php foreach ($intergral_order_goods as $k=>$v):?>
                <li>
                    <a href="<?=Url::toRoute(['integral/goods-detail','id'=>$v->goods_id])?>">
                        <div class="order_l"><img src="<?=$v->goods_pic?>" class="order_pic"></div>
                        <div class="order_r">
                            <p> <?=$v->goods_name?></p>
                            <div class="order_gg"><?=empty($v->attr)?'':$v->attr?></div>
                            <div class="order_bot">
                                <span class="order_price"><span class="order_price_label"></span><?=intval($v->goods_price)?>积分</span><span class="order_num">x<?=$v->goods_num?></span>
                            </div>
                        </div>
                    </a>
                </li>
            <?php endforeach;?>
        </ul>
    </div>

    <div class="casedetail_center">
        <div class="order_tit">订单信息</div>
        <ul>
            <li>订单编号：<?=$intergralOrder->orderid?></li>
            <li>兑换时间：<?=$intergralOrder->add_time?></li>
            <?php if ($intergralOrder->order_state >=2):?>
                <li>发货时间：<?=$intergralOrder->deliveryTime?></li>
            <?php endif;?>
        </ul>
    </div>

    <div class="operating">
        <?php if ($intergralOrder->order_state == 0):?>
            <a href="<?=Url::toRoute(['integral-order/pay','number'=>$intergralOrder->orderid])?>">立即支付</a>
        <?php elseif ($intergralOrder->order_state == 2):?>
            <a href="javascript:void(0)" class="receiving_goods" data-toggle="modal" data-target="#myModalCasesure">确认收货</a>
        <?php elseif ($intergralOrder->order_state == 3):?>
            <a href="<?=Url::toRoute(['member/evaluate','type'=>'JiFen','orderid'=>$intergralOrder->orderid])?>">立即评价</a>
        <?php endif;?>
    </div>
</div>
<!-- 模态框- 确认收货 弹框 -->
<div class="modal modal_explain modal_explain_sm fade" id="myModalCasesure">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">确认收货</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align">是否确认收货？</p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <button type="button" class="btn btn_outline" data-dismiss="modal" style="width: 40%;min-width: 100px;">取 消</button>
                <button type="button" class="btn" data-dismiss="modal" style="width: 40%;min-width: 100px;">确 定</button>
            </div>
        </div>
    </div>
</div>
<script>


    //确认收货 后到评价页面
    $("#myModalCasesure").on('click','.btn',function () {
        var dataOrderId='<?=$intergralOrder->orderid?>';
        var  receivingUrl = "<?=Url::toRoute(['member/receiving-good'])?>";
        var  goEvaluate = "<?=Url::toRoute(['member/evaluate','orderid'=>''])?>"+dataOrderId;
        $.post(receivingUrl,{orderid:dataOrderId},function (res) {
            alert(res.msg);
            if (res.code==1){
                window.location.href=goEvaluate
            }
        },'json');
    })

</script>
