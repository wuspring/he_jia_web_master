<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>

<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>最新揭晓</h2>
</div>
<div class="integral_treasure">
    <?php if(!empty($records)):?>
        <?php foreach ($records as $key=>$item):?>
        <div class="integral_treasure_con <?=$key==0?'mt-0':''?>">
            <div class="d_user">
                <img src="<?=$item->avatar?>">
                <p class="p1">用户名：</p>
                <p class="p1"><?=$item->nickname?></p>
            </div>
            <div class="d_more"><span class="s_right float-right">参与人次：<span><?=$item->count?>人次</span></span>揭晓时间：<span><?=$item->end_time?></span></div>
        </div>
        <?php endforeach;?>
    <?php endif;?>
</div>