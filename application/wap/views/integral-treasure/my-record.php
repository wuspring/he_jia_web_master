<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>

<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>夺宝记录</h2>
</div>
<!--- 夺宝记录 -->
<div id="treasure_record">
    <ul id="container_list">

    </ul>
</div>

<script>
    var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var page = 0,size = 10;
    var totalCount= 10;
    var p = Math.ceil(totalCount/size);
    var list = '';

    $(function () {

        getList();
    });

    function getList() {
        $('#treasure_record').dropload({
            scrollArea: window,
            distance:10,
            loadDownFn : function(me){
                list = '';
                if (page < p) {
                    page++;
                    // 拼接HTML
                    $.post('<?php echo Url::toRoute(["integral-treasure/my-record"]);?>', {
                        _csrf:_csrf,
                        page: page,
                        size: size,
                    }, function (res) {
                        if(res.status==1){
                            totalCount = res.data.totalCount;
                            p= Math.ceil(totalCount/size);
                            list = '';
                            $.each(res.data.list, function (key, val) {
                                list += sureList(val);
                            });
                            $('#container_list').append(list);
                            me.resetload();
                        }else{
                            alert(res.msg);
                        }
                    }, 'json');
                } else {
                    // 锁定
                    me.lock();
                    // 无数据
                    me.noData();
                    // 插入数据到页面，放到最后面
                    $('#container_list').append(list);
                    // 每次数据插入，必须重置
                    me.resetload();
                }
            }
        });
    }


    function sureList(obj){
        var html = '';
        var goods_url = "<?= Url::toRoute(['integral-treasure/detail','id'=>''])?>" + obj.treasure_id;
        html += '<li>\n' +
            '            <div class="pic">\n' +
            '                <a href="'+ goods_url +'">\n' +
            '                    <img src="'+ obj.goods_pic +'"/>\n' +
            '                </a>\n' +
            '            </div>\n' +
            '            <div class="box">\n' +
            '                <h3><a href="'+ goods_url +'">'+ obj.goods_name +'</a></h3>\n' +
            '                <div class="data">\n' +
            '                    <p>时间：'+ obj.create_time +'</p>\n' +
            '                </div>\n' +
            '                <div class="operating">\n' +
            '                    <p>参与人次：<span>'+ obj.count +'人次</span></p>\n';
        if(obj.is_state==0){
            if(obj.is_selected){
                html += '                    <a class="color_orange" href="javascript:void(0)">中奖</a>\n';
            }else{
                html += '                    <a class="color_gray" href="javascript:void(0)">未中奖</a>\n';
            }
        }

        html += '                </div>\n' +
            '            </div>\n' +
            '        </li>';
        return html;
    }
</script>