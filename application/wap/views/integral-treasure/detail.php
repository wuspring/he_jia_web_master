<?php
/* @var $this yii\web\View */
use yii\helpers\Url;

?>

<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>商品详情</h2>
</div>

<?php if(!empty($goods->goods_image)):?>
    <div class="slider carousel slide" data-ride="carousel">
        <!-- 指示符 -->
        <ul class="carousel-indicators">
            <?php foreach ($goods->goods_image as $key=>$item):?>
                <li data-target="#slider" data-slide-to="<?=$key?>" class="<?=$key==0?'active':''?>"></li>
            <?php endforeach;?>
        </ul>
        <!-- 轮播图片 -->
        <div class="carousel-inner">
            <?php foreach ($goods->goods_image as $key=>$item):?>
                <a href="javascript:void(0)" class="carousel-item <?=$key==0?'active':''?>">
                    <img src="<?=$item['imgurl']?>">
                </a>
            <?php endforeach;?>
        </div>
        <!-- 左右翻页不需要但是删除会报错 -->
        <a id="carleft" class="left carousel-control" href="#slider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a id="carright" class="right carousel-control" href="#slider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
<?php endif;?>

<div class="product_intrgralcon1">
    <p class="p_name"><?=$goods->goods_name?></p>
    <div class="d_con1">
        <p class="p1 orange"><span><?=$goods->goods_integral?></span>积分<span class="s">/次</span></p>
        <div class="progress">
            <div class="progress-bar" style="width:<?=($goods->goods_salenum/$goods->goods_storage)*100?>%"></div>
        </div>
        <div class="progress_text"><span>剩余<?=($goods->goods_storage-$goods->goods_salenum)>0?($goods->goods_storage-$goods->goods_salenum):0?>人次</span>已参与<?=$goods->goods_salenum?>人次</div>
    </div>
    <div class="d_con2">
        <span>我要夺宝</span>
        <div class="num">
            <a href="javascript:void(0)" class="a_reduce" data-id="amountCtrl" data-type="reduce"><img src="/public/wap/images/icon_reduce.jpg" alt="减"></a>
            <input type="text" name="amount" class="inp" value="1">
            <a href="javascript:void(0)" class="a_plus" data-id="amountCtrl" data-type="plus"><img src="/public/wap/images/icon_plus.jpg" alt="加"></a>
        </div>
    </div>
</div>
<div class="product_intrgralcon2">
    <p>夺宝规则：</p>
    <p><?= \DL\vendor\ConfigService::init('INDEX_INTERGRAL_SHOP')->get('int_rules');?></p>
</div>
<!-- 商品介绍/规格参数/售后保障/用户评论 -->
<div class="product_intrgraltab">
    <ul class="nav nav-pills nav-justified product_intrgraltab_list"  role="tablist">
        <li class="nav-item"><a href="#intrgral_tab1" data-toggle="tab" class="nav-link active"><span>商品介绍</span></a></li>
        <li class="nav-item"><a href="#intrgral_tab2" data-toggle="tab" class="nav-link"><span>规格参数</span></a></li>
        <li class="nav-item"><a href="#intrgral_tab3" data-toggle="tab" class="nav-link"><span>售后保障</span></a></li>
    </ul>
    <div class="tab-content" style="min-height: 100px;">
        <!-- 商品介绍 -->
        <div class="tab-pane product_intrgral_content active" id="intrgral_tab1">
            <?=$goods->goods_body?>
        </div>
        <!-- 规格参数 -->
        <div class="tab-pane product_intrgral_content" id="intrgral_tab2">
            <?=$goods->goods_jingle?>
        </div>
        <!-- 售后保障 -->
        <div class="tab-pane product_intrgral_content" id="intrgral_tab3">
            <?=$goods->mobile_body?>
        </div>
    </div>
</div>
<div class="product_intrgralbtn1">
    <?php if ($goods->goods_state) :?>
        <a href="javascript:void(0)" class="btn btn_big" data-id="getTreasure">立即夺宝</a>
    <?php else :?>
        <a href="javascript:alert('活动已结束')" class="btn btn_big">立即夺宝</a>
    <?php endif; ?>
</div>


<!-- 轮播图-手指滑动效果 -->
<script type="text/javascript">

    $(function () {
        // 获取手指在轮播图元素上的一个滑动方向（左右）
        // 获取界面上轮播图容器
        var $carousels = $('.carousel');
        var startX,endX;
        // 在滑动的一定范围内，才切换图片
        var offset = 50;
        // 注册滑动事件
        $carousels.on('touchstart',function (e) {
            // 手指触摸开始时记录一下手指所在的坐标x
            startX = e.originalEvent.touches[0].clientX;

        });
        $carousels.on('touchmove',function (e) {
            // 目的是：记录手指离开屏幕一瞬间的位置 ，用move事件重复赋值
            endX = e.originalEvent.touches[0].clientX;
        });
        $carousels.on('touchend',function (e) {
            //console.log(endX);
            //结束触摸一瞬间记录手指最后所在坐标x的位置 endX
            //比较endX与startX的大小，并获取每次运动的距离，当距离大于一定值时认为是有方向的变化
            var distance = Math.abs(startX - endX);
            if (distance > offset){
                //说明有方向的变化
                //根据获得的方向 判断是上一张还是下一张出现
                $(this).carousel(startX >endX ? 'next':'prev');
            }
        })
    });

    $('[data-id="getTreasure"]').click(function () {
        var amount = $('input[name="amount"]').val(),
            data = {id : '<?= $goods->id; ?>'};
        if (parseInt(amount)) {
            data.amount = amount;
            $.get('<?= Url::to(['integral-treasure/get-treasure']); ?>', data, function(res) {
                res = JSON.parse(res);
                if (res.status) {
                    alert("您已经成功参加夺宝，请耐心等待结果",function () {},{title:'夺宝成功'});
                    setTimeout(function () {
                        window.location.reload();
                    },2000);
                    return false;
                }
                alert(res.msg);
            });
        }
    });


    // 增减控件
    $('a[data-id="amountCtrl"]').click(function () {
        var type = $(this).attr('data-type'),
            amountDom = $('input[name="amount"]');

        switch (type) {
            case 'plus' :
                var val = parseInt(amountDom.val()) + 1;
                break;
            case 'reduce' :
                var val = parseInt(amountDom.val()) - 1; val = (val>=1) ? val : 1;
                break;
        }
        amountDom.val(val).change();
    });
</script>