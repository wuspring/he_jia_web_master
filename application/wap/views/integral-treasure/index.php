<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>

<!--- 积分夺宝 -->
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>积分夺宝 </h2>
</div>
<div id="points_treasure">
    <div class="points_bg" style="background-image: url(<?=$topAd->picture?>);">
        <!--- 导航 -->
        <div class="user_info">
            <div class="user_data">
                <ul>
                    <li>
                        <a href="<?=Url::to(['integral-treasure/record'])?>">
                            <img src="/public/wap/images/treasure_ico1.png"/>
                            <span>最新揭晓</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=Url::to(['integral-treasure/my-record'])?>">
                            <img src="/public/wap/images/treasure_ico2.png"/>
                            <span>夺宝记录</span>
                        </a>
                    </li>
                    <div class="clear"></div>
                </ul>
            </div>
        </div>
    </div>

    <!--- 夺宝商品 -->
    <div class="treasure_pro" >
        <div class="title">
            <h2>夺宝商品</h2>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#myModalTicket">规则介绍></a>
        </div>
        <div class="cont" id="container">
            <ul id="container_list">

            </ul>
        </div>

    </div>
    <!-- 模态框- 夺宝规则 弹框 -->
    <div class="modal modal_ticket fade" id="myModalTicket">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- 模态框头部 -->
                <div class="modal-header">
                    <p class="p_title">夺宝规则</p>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <!-- 模态框主体 -->
                <div class="modal-body">
                    <?= \DL\vendor\ConfigService::init('INDEX_INTERGRAL_SHOP')->get('int_rules');?>
                </div>
            </div>
        </div>
    </div>
    
</div>

<script>
    var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var page = 0,size = 10;
    var fid=0;
    var totalCount= 10;
    var p = Math.ceil(totalCount/size);
    var list = '';

    $(function () {

        getList();
    });

    function getList() {
        $('#container').dropload({
            scrollArea: window,
            distance:10,
            loadDownFn : function(me){
                list = '';
                if (page < p) {
                    page++;
                    // 拼接HTML
                    fid = $('.com_tab2 > a.active').data('id');
                    $.post('<?php echo Url::toRoute(["integral-treasure/list"]);?>', {
                        _csrf:_csrf,
                        page: page,
                        size: size,
                        cityId: cityIds[cityIndex],
                    }, function (res) {
                        if(res.status==1){
                            totalCount = res.data.totalCount;
                            p= Math.ceil(totalCount/size);
                            list = '';
                            $.each(res.data.list, function (key, val) {
                                list += sureList(val);
                            });
                            $('#container_list').append(list);
                            me.resetload();
                        }else{
                            alert(res.msg);
                        }
                    }, 'json');
                } else {
                    // 锁定
                    me.lock();
                    // 无数据
                    me.noData();
                    // 插入数据到页面，放到最后面
                    $('#container_list').append(list);
                    // 每次数据插入，必须重置
                    me.resetload();
                }
            }
        });
    }


    function sureList(obj){
        var html = '';
        var goods_url = "<?=Url::to(['integral-treasure/detail','id'=>''])?>" + obj.id;
        html += '<li>\n' +
            '                    <div class="pic">\n' +
            '                        <a href="' + goods_url + '">\n' +
            '                            <img src="'+ obj.goods_pic+'"/>\n' +
            '                        </a>\n' +
            '                    </div>\n' +
            '                    <div class="box">\n' +
            '                        <a href="'+goods_url+'"><h3>'+ obj.goods_name +'</h3></a>\n' +
            '                        <div class="data">\n' +
            '                            <div class="schedule">\n' +
            '                                <div class="process"></div>\n' +
            '                            </div>\n' +
            '                            <p>已参与'+ obj.goods_salenum +'人次<span>剩余'+ (obj.goods_storage-obj.goods_salenum) +'人次</span></p>\n' +
            '                        </div>\n' +
            '                        <div class="operating">\n' +
            '                            <p>'+ obj.goods_integral +'积分<span>/次</span></p>' +
            '                            <a href="' + goods_url + '">立即夺宝</a>' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </li>';
        return html;
    }
</script>