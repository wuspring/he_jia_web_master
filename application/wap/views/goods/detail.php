<?php
/* @var $this yii\web\View */
?>
<div class="head" style="position: fixed;top: 0;z-index: 9999;width: 100%;">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <a href="/" style="float: right;">
        <img src="/public/images/icon-home.png" style="width: 18px;height: 18px;" alt="主页">
    </a>
    <h2>商品详情</h2>
</div>
<div class="slider carousel slide" style="padding-top: 45px" data-ride="carousel">
    <!-- 指示符 -->
    <ul class="carousel-indicators">
        <?php if (!empty($goods->goods_image) && count($goods->goods_image) > 1): ?>
            <?php foreach ($goods->goods_image as $key => $item): ?>
                <li data-target="#slider" data-slide-to="<?= $key ?>" class="<?= $key == 0 ? 'active' : '' ?>"></li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>
    <!-- 轮播图片 -->
    <div class="carousel-inner">
        <?php if (!empty($goods->goods_image)): ?>
            <?php foreach ($goods->goods_image as $key => $item): ?>
                <a href="javascript:void(0)" class="carousel-item <?= $key == 0 ? 'active' : '' ?>">
                    <img src="<?= $item->imgurl ?>">
                </a>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <!-- 左右翻页不需要但是删除会报错 -->
    <a id="carleft" class="left carousel-control" href="#slider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a id="carright" class="right carousel-control" href="#slider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- 收藏和分享暂未做，需要调整位置 -->
<div class="product_con1">
    <p class="p_name"><?= $goods->goods_name ?></p>
    <div class="d_collect float-right">
        <a href="javascript:void(0)" class="btn btn_outline click_collect" data-collect="0"
           data-type="<?= $goods->collect_type ?>" style="<?= $goods->is_collect == 0 ? '' : 'display: none' ?>"><img
                    src="/public/wap/images/icon_star.png">收藏</a>
        <a href="javascript:void(0)" class="btn btn_outline active click_collect" data-collect="1"
           data-type="<?= $goods->collect_type ?>" style="<?= $goods->is_collect == 1 ? '' : 'display: none' ?>"><img
                    src="/public/wap/images/icon_stared.png">已收藏</a>
    </div>
    <p class="p_more"><?= $goods->mobile_body ?></p>
    <p class="p_price1 float-right mt-2">市场价：<span>￥<?= $goods->goods_marketprice ?></span></p>
    <p class="p_price">
        <?php if ($goods->coupon_or_order == \wap\models\TicketApplyGoods::TYPE_COUPON):
            echo '预约价：';
        elseif ($goods->coupon_or_order == \wap\models\TicketApplyGoods::TYPE_ORDER):
            echo '预存价：';
        else:
            echo '抢购价：';
        endif;
        ?>
        <span class="orange">￥<span><?= $goods->goods_price ?></span></span><span>/<?= $goods->unit ?></span>
    </p>
    <dl class="dl_spec">
        <dt>参数：</dt>
        <dd>
            <?= $goods->goods_jingle ?>
        </dd>
    </dl>
</div>
<div class="coupondetail_shop product_shop clearfix">
    <a href="<?= \yii\helpers\Url::to(['shop/detail', 'id' => $shop->user_id]) ?>">
        <img src="<?= $shop->avatar ?>" alt="<?= $shop->nickname ?>" class="img">
    </a>
    <a href="<?= \yii\helpers\Url::to(['shop/detail', 'id' => $shop->user_id]) ?>"
       class="p1 text-truncate"><?= $shop->nickname ?></a>
    <a href="<?= \yii\helpers\Url::to(['shop/detail', 'id' => $goods->user_id]) ?>"
       class="btn float-right mt-1">进入店铺</a>
    <p class="p4 orange mt-1">共<span><?= $shop->store_amount ?></span>家体验店</p>
</div>
<div class="product_content" style="min-height: 100px;">
    <?= $goods->goods_body ?>
</div>
<?php if (!empty($goods->coupon_or_order)): ?>
    <?php if ($goods->coupon_or_order == \wap\models\TicketApplyGoods::TYPE_COUPON): ?>
        <div class="product_btn">
            <a href="javascript:void(0)" data-id="get-coupon" data-good="<?= $goods->id ?>"
               data-ticket="<?= $goods->ticket_id ?>" class="btn btn_big">立即预约</a>
        </div>
    <?php endif; ?>
    <?php if ($goods->coupon_or_order == \wap\models\TicketApplyGoods::TYPE_ORDER): ?>
        <div class="product_btn">
            <a href="javascript:void(0)" data-id="order-create" data-good="<?= $goods->id ?>"
               data-ticket="<?= $goods->ticket_id ?>" class="btn btn_big">立即预存</a>
        </div>
    <?php endif; ?>
<?php endif; ?>
<!--side-->
<?php

global $secretCityId;
$ms = \DL\Project\Page::init()->get($secretCityId, \DL\Project\Page::PAGE_INDEX);

$icon = [];
if ($ms) {
    $icon = isset($ms->icon) && strlen($ms->icon) ? json_decode($ms->icon, true) : [];
    if ($icon) {
        $icon = explode('@', reset($icon));
        $icon[1] = translateAbsolutePath($icon[1]);
    }
}

?>
<?php if ($icon) : ?>
    <div class="right-slide">
        <ul class="right-slide-in">
            <li class="border-ridus">
                <a class="external" href="<?= $icon[0] ?>">
                    <img src="<?= $icon[1]; ?>">
                </a>
            </li>
        </ul>
    </div>
<?php endif; ?>
<!--side end-->
<div class="return-top" style=""></div>

<!-- 轮播图-手指滑动效果 -->
<script type="text/javascript">
    $(function () {
        // 获取手指在轮播图元素上的一个滑动方向（左右）
        // 获取界面上轮播图容器
        var $carousels = $('.carousel');
        var startX, endX;
        // 在滑动的一定范围内，才切换图片
        var offset = 50;
        // 注册滑动事件
        $carousels.on('touchstart', function (e) {
            // 手指触摸开始时记录一下手指所在的坐标x
            startX = e.originalEvent.touches[0].clientX;

        });
        $carousels.on('touchmove', function (e) {
            // 目的是：记录手指离开屏幕一瞬间的位置 ，用move事件重复赋值
            endX = e.originalEvent.touches[0].clientX;
        });
        $carousels.on('touchend', function (e) {
            //console.log(endX);
            //结束触摸一瞬间记录手指最后所在坐标x的位置 endX
            //比较endX与startX的大小，并获取每次运动的距离，当距离大于一定值时认为是有方向的变化
            var distance = Math.abs(startX - endX);
            if (distance > offset) {
                //说明有方向的变化
                //根据获得的方向 判断是上一张还是下一张出现
                $(this).carousel(startX > endX ? 'next' : 'prev');
            }
        })

        //已预约
        var goods_id = "<?=$goods->id?>";
        if (hasCouponGoods.indexOf(goods_id) > -1) {
            $('body [data-id="get-coupon"]').addClass('disabled').text('已预约');
        }

        //点击收藏 点击取消
        $(".click_collect").on('click', function () {
            var is_collect = $(this).attr('data-collect');
            var collect_type = $(this).attr('data-type');
            var ticketId = '<?=$goods->ticket_id?>';
            var url = "<?=\yii\helpers\Url::toRoute(['goods/collect'])?>";

            if (Object.keys(userInfo).length < 1) {
                alert("您还没有登录哦");
                return false;
            }
            if (is_collect == 1) {   //取消收藏
                $.get(url, {is_collect: 1, goodsId: goods_id, type: collect_type}, function (res) {
                    if (res.code == 1) {
                        $('[data-collect="0"]').show();
                        $('[data-collect="1"]').hide();
                    }
                    alert(res.msg);
                }, 'json');

            } else {
                $.get(url, {is_collect: 0, goodsId: goods_id, type: collect_type, ticketId: ticketId}, function (res) {
                    if (res.code == 1) {
                        $('[data-collect="0"]').hide();
                        $('[data-collect="1"]').show();
                    }
                    alert(res.msg);
                }, 'json');
            }
        });
    });
</script>