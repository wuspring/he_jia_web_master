<div class="head" style="position: fixed;top: 0;z-index: 1000;width: 100%;">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2><?= $title; ?></h2>
</div>
<div class="topNav swiper-container swiper-container-horizontal swiper-container-free-mode" style="margin-top: 45px;">
    <div class="swiper-wrapper com_tab" style="transition-duration: 300ms;transform: translate3d(0px, 0px, 0px);">

        <a class="swiper-slide active" data-id="cate" data-cate="<?= $firstCategory['cid']; ?>"><span><?= $firstCategory['title']; ?></span></a>
        <?php foreach ($categorys AS $category) :?>
        <a class="swiper-slide"  data-id="cate" data-cate="<?= $category->id; ?>"><span><?= $category->name; ?></span></a>
        <?php endforeach; ?>
    </div>
</div>
<div>
    <ul class="list-unstyled clearfix com_productlist com_productlistone mt-2 pt-2" data-id="cateCon" data-cate="0" data-p="1" data-type="<?= $type;?>" >
        <?php foreach ($goods AS $good) :?>
        <li>
            <a href="<?= \DL\service\UrlService::build(['goods/detail', 'id' => $good->id,'city_id'=>$defaultCity->id]);?>" class="product">
                <span class="product_img float-left"><img src="<?= strlen($good->goods_pic) ? $good->goods_pic : '/public/wap/images/loading.jpg'; ?>" class="loading_img" data-url="<?= $good->goods_pic; ?>" alt="<?= $good->goods_name; ?>"></span>
                <div class="product_con">
                    <p class="product_name ellipsis"><?= $good->goods_name; ?></p>
                    <p class="product_tag orange"><img src="/public/wap/images/icon_time_orange.png">仅剩<span data-id="storage" data-good="<?= $good->id; ?>">
                            <?= ((int)$good->ticket_id) ? $good->good_amount : $good->goods_storage; ?>
                        </span><span>件</span></p>
                    <p class="product_handsel"><span class="orange">￥<span><?= $good->goods_price; ?></span><span class="s">/<?= $good->unit; ?></span></span></p>
                </div>
            </a>
            <?php if ($type == \wap\models\TicketApplyGoods::TYPE_ORDER) :?>
            <a href="javascript:void(0)" class="btn float-right" data-id="order-create" data-good="<?= $good->id; ?>" data-ticket="<?= $good->ticket_id; ?>">立即预存</a>
            <?php elseif ($type == \wap\models\TicketApplyGoods::TYPE_COUPON) :?>
            <a href="javascript:void(0)" class="btn float-right" data-id="get-coupon" data-good="<?= $good->id; ?>" data-ticket="<?= $good->ticket_id; ?>">立即预约</a>
            <?php endif; ?>
        </li>
        <?php endforeach; ?>

    </ul>

    <?php if ($categorys): foreach ($categorys AS $category):?>
        <ul class="list-unstyled clearfix com_productlist com_productlistone mt-2 pt-2"  data-id="cateCon" data-cate="<?= $category->id; ?>" data-p="0" data-type="<?= $type;?>" style="display: none"></ul>
    <?php endforeach; endif; ?>
</div>


<!-- tab左右滑动 -->
<script type="text/javascript">
    var mySwiper = new Swiper('.topNav', {
        freeMode: true,
        preventClicks : false,//默认true
        freeModeMomentumRatio: 0.5,
        slidesPerView: 'auto',
    });

    $('[data-id="cate"]').click(function () {
        var that = $(this),
            cate = that.attr('data-cate'),
            activeDom = $('[data-id="cateCon"][data-cate="' + cate + '"]');
        that.addClass('active').siblings().removeClass('active');

        loadLists(activeDom);
        activeDom.show().siblings().hide();
    });

    var loadListsStatus = true;
    var loadLists = function (dom) {
        if (!loadListsStatus) {
            return false;
        }

        var data ={};
        data.cid = dom.attr('data-cate');
        data.p = dom.attr('data-p');
        data.type = dom.attr('data-type');

        loadListsStatus = false;
        $.get('<?= \DL\service\UrlService::build('goods/api-lists'); ?>', data, function (res) {
            loadListsStatus = true;

            res = JSON.parse(res);
            if (res.status) {
                var rdata = res.data.lists;

                if (rdata.length) {
                    for (var i=0; i<rdata.length; i++) {
                        var li = $('<li>').append(
                            $('<a>').attr('href', '<?= urldecode(\DL\service\UrlService::build(['goods/detail','city_id'=>$defaultCity->id,'id' => '###'])); ?>'.replace(/###/, rdata[i].id)).addClass('product').append(
                                $('<span>').addClass('product_img float-left').append($('<img>').addClass('loading_img').attr({
                                    src : (rdata[i].goods_pic.length ? rdata[i].goods_pic : '/public/wap/images/loading.jpg'),
                                    'data-url' : (rdata[i].goods_pic.length ? rdata[i].goods_pic : '/public/wap/images/loading.jpg'),
                                    'alt' : rdata[i].goods_name
                                })),
                                $('<div>').addClass('product_con').append(
                                    $('<p>').addClass('product_name ellipsis').text(rdata[i].goods_name),
                                    $('<p>').addClass('product_tag orange').html('<img src="/public/wap/images/icon_time_orange.png">仅剩<span data-id="storage" data-good="' + rdata[i].id + '">' + rdata[i].good_amount + '</span><span>件</span>'),
                                    $('<p>').addClass('product_name product_handsel').append(
                                        $('<span>').addClass('orange').append(
                                            '￥',
                                            $('<span>').text(rdata[i].goods_price),
                                            $('<span>').addClass('s').text('/' + rdata[i].unit),
                                        )
                                    )
                                )
                            )
                        );

                        if (['order-create', 'get-coupon'].indexOf(rdata[i].g_type) > -1) {
                            var bName;
                            if ('order-create' == rdata[i].g_type) {
                                bName = '预存';
                            } else {
                                bName = '预约';
                            }

                            li.append(
                                $('<a>').addClass('btn float-right ' + (rdata[i].has ? 'disabled' : ''))
                                    .attr({
                                        'data-id' : rdata[i].g_type,
                                        'data-good' : rdata[i].id,
                                        'data-ticket' : rdata[i].ticketId
                                    }).text((rdata[i].has ? '已' : '立即') + bName)
                            )
                        }
                        dom.append(li);
                    }

                    dom.attr('data-p', parseInt(data.p)+1);
                }

                return false;
            }
            alert(res.msg);
        })
    };

    $(window).scroll(function(){
        var dom = $('[data-id="cateCon"]').not(":hidden");
        loadLists(dom);
    })
</script>
