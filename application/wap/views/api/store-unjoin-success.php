<?php
global $currentCity;

?>
<div class="modal-dialog" style="top:0">
    <div class="modal-content" style="height: 203px;">
        <!-- 模态框头部 -->
        <div class="modal-header">
            <img src="/public/wap/images/icon_ok.png" alt="ok" class="img">
            <div class="con">
                <p class="p_title">店铺预约成功</p>
                <p class="p_more"><?= $store->nickname; ?></p>
            </div>
        </div>
        <!-- 模态框主体 -->
        <div class="modal-body">
            <div class="con_img">
                <img src="<?= $store->avatar; ?>" alt="<?= $store->nickname; ?>" class="img">
                <p class="p1"><?= $store->nickname; ?></p>
                <p class="p4 orange">共<span><?= $storeShopsAmount; ?></span>家体验店</p>
            </div>
        </div>
        <!-- 模态框底部 -->
        <div class="modal-footer">
            <button type="button" class="btn" data-dismiss="modal">确 定</button>
        </div>
    </div>
</div>