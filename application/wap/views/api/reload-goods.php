<?php
use wap\models\TicketApplyGoods;
use \wap\models\Order;
use \wap\models\OrderGoods;

$hasOrderGoodsIds = [];
if (!$userInfo = Yii::$app->user->isGuest) {
    $userInfo = Yii::$app->user->identity;

    $orders = Order::find()->where([
        'and',
        ['=', 'buyer_id', $userInfo->id],
        ['>', 'add_time', date('Y-m-d', strtotime('-30 days'))],
        ['=', 'type', Order::TYPE_ZHAN_HUI],
        [
            'or',
            [
                'and',
                ['=', 'ticket_type', Order::TICKET_TYPE_COUPON]
            ],
            [
                'and',
                ['not in', 'order_state', [Order::STATUS_WAIT]],
                ['=', 'ticket_type', Order::TICKET_TYPE_ORDER]
            ]
        ]
    ])->all();

    if ($orders) {
        $orderIds = arrayGroupsAction($orders, function ($order) {
            return $order->orderid;
        });

        $hasOrders = OrderGoods::find()->where([
            'in', 'order_id', $orderIds
        ])->all();

        $hasOrderGoodsIds = arrayGroupsAction($hasOrders, function ($hasOrder) {
            return $hasOrder->goods_id;
        });
    }
}
?>

<?php if ($type == TicketApplyGoods::TYPE_ORDER) :
    foreach ($teJiaGoods as $tk=>$tv):?>
    <li>
        <a href="<?= \DL\service\UrlService::build(['goods/detail','city_id'=>$defaultCity->id, 'id' => $tv->id]); ?>" class="product">
            <span class="product_img"><img src="<?=$tv->goods_pic?>" class="loading_img" data-url="<?=$tv->goods_pic?>" alt="<?=$tv->goods_name;?>"></span>
            <p class="product_name text-truncate"><?=$tv->goods_name;?></p>
            <p class="product_handsel">订金：<span class="orange">￥<span><?= $ticket->order_price; ?></span></span></p>
            <p class="product_price">专享价：￥<span><?=$tv->goods_price?></span>/<span><?=$tv->unit;?></span></p>
            <p class="product_price">市场价：<span class="s">￥<span><?=$tv->goods_marketprice?></span>元/<span><?=$tv->unit;?></span></span></p>
            <p class="product_tag orange"><img src="/public/wap/images/icon_time_orange.png">仅剩<span data-id="storage" data-good="<?= $tv->id; ?>"><?=$tv->good_amount?></span><span>件</span></p>
        </a>
        <?php if (in_array($tv->id, $hasOrderGoodsIds)) :?>
            <a href="javascript:void(0)" class="btn float-right disabled" >已预存</a>
        <?php else :?>
            <a href="javascript:void(0)" class="btn" data-id="order-create" data-ticket="<?= $ticketId; ?>" data-good="<?= $tv->id; ?>">立即预存</a>
        <?php endif;?>
    </li>
<?php endforeach;else : foreach ($baoKuanGood AS $bgv) :?>
        <li>
            <a href="<?= \DL\service\UrlService::build(['goods/detail','city_id'=>$defaultCity->id, 'id' => $bgv->id]); ?>" class="product">
                <span class="product_img float-left"><img src="<?=$bgv->goods_pic?>" class="loading_img" data-url="<?=$bgv->goods_pic?>" alt="<?=$bgv->goods_name?>"></span>
                <div class="product_con">
                    <p class="product_name ellipsis mb-0"><?=$bgv->goods_name?></p>
                    <p class="product_handsel mt-0">预约价：<span class="orange">￥<span><?=$bgv->goods_price?></span></span>/<?=$bgv->unit;?></p>
                    <p class="product_price">市场价：<span class="s">￥<span><?=$bgv->goods_marketprice?></span>/<span><?=$bgv->unit;?></span></span></p>
                    <p class="product_tag orange"><img src="/public/wap/images/icon_time_orange.png">仅剩<span data-id="storage" data-good="<?= $bgv->id; ?>"><?=$bgv->good_amount?></span><span>件</span></p>
                </div>
            </a>
            <?php if (in_array($bgv->id, $hasOrderGoodsIds)) :?>
                <a href="javascript:void(0)" class="btn float-right disabled" >已预约</a>
            <?php else :?>
            <a href="javascript:void(0)" class="btn float-right" data-id="get-coupon" data-source="1" data-ticket="<?= $ticketId; ?>" data-good="<?= $bgv->id;?>">立即预约</a>
            <?php endif;?>
        </li>
<?php endforeach;endif; ?>