<?php
global $currentCity;

?>
<div class="modal-dialog">
    <div class="modal-content" style="height: 358px;">
        <!-- 模态框头部 -->
        <div class="modal-header">
            <img src="/public/wap/images/icon_ok.png" alt="ok" class="img">
            <div class="con">
                <p class="p_title">已预约成功</p>
                <p class="p_more">线上预约锁定，凭短信至展会现场下单~</p>
            </div>
        </div>
        <!-- 模态框主体 -->
        <div class="modal-body">
            <div class="con_img">
                <img src="<?= $orderGood->goods_pic; ?>" alt="<?= $orderGood->goods_name; ?>" class="img" style="height: 85px">
                <p class="p1 mt-0 ellipsis"><?= $orderGood->goods_name; ?></p>
                <p class="p2">预约价：<span class="orange">￥<span><?= $orderGood->goods_price; ?></span></span>/<span><?= $orderGood->goods->unit; ?></span></p>
                <p class="p3">市场价：<span>￥<span><?= $orderGood->goods->goods_marketprice; ?></span>/<span><?= $orderGood->goods->unit; ?></span></span></p>
            </div>
            <div class="con_more orange">
                <dl>
                    <dt>活动时间：</dt>
                    <dd><?= date('m月d日', strtotime($ticket->open_date)); ?> - <?= date('m月d日', strtotime($ticket->end_date)); ?></dd>
                </dl>
                <dl>
                    <dt>活动地址：</dt>
                    <dd><?= $ticket->open_place; ?>（<?= $ticket->address; ?>）</dd>
                </dl>
            </div>
        </div>
        <!-- 模态框底部 -->
        <?php if ($source) :?>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">确 定</button>
            </div>
        <?php else :?>
            <div class="modal-footer">
                <a href="<?= \DL\service\UrlService::build(['member/my-appointment-detail', 'orderid' => $order->orderid]); ?>" class="btn btn_outline ml-2 mr-2">查看订单</a>
                <button type="button" class="btn ml-2 mr-2" data-dismiss="modal">确 定</button>
            </div>
        <?php endif; ?>
    </div>
</div>