<?php
global $currentCity;

?>
<div class="modal-dialog">
    <div class="modal-content" style="height: 393px;">
        <!-- 模态框头部 -->
        <div class="modal-header">
            <img src="<?= $store->avatar; ?>" alt="<?= $store->nickname; ?>" class="img">
            <p class="p1"><?= $store->nickname; ?></p>
            <p class="p4 orange">共<span><?= $storeShopsAmount; ?></span>家体验店</p>
        </div>
        <!-- 模态框主体 -->
        <div class="modal-body">
            <form method="post" action="<?= \DL\service\UrlService::build('api/auto-sign'); ?>">
                <div class="form-group">
                    <input type="text" class="form-control" data-id="name000" name="name" placeholder="请输入您的姓名">
                </div>
                <div class="form-group">
                    <input type="number" class="form-control" data-id="mobile000" name="mobile" placeholder="请输入您的手机号">
                </div>
                <div class="form-group form_yzm">
                    <a href="javascript:void(0)"  data-id="getCode000" class="btn btn_yzm float-right">获取验证码</a>
                    <!-- <a href="javascript:void(0)" class="btn btn_yzm float-right disabled">50s</a> -->
                    <input type="number" onkeyup="if(value.length>6)value=value.slice(0,6)" class="form-control" data-id="code000" name="code"  placeholder="请输入验证码">
                </div>
                <input type="hidden" name="cityId" value="<?= $ticket->citys; ?>">
                <input type="hidden" name="location" value="<?= $location; ?>">
                <button type="submit" class="btn btn_big" data-dismiss="modal"  data-id="confirm" data-toggle="modal" data-target="#myModalShopSuc2">立即预约</button>
                <dl class="dl_more">
                    <dd class="ml-0">
                        <p><b>温馨提示：</b>请详细填写以上消息，我们将免费为您快递一份价值<?= (int)$ticket->ticket_price; ?>元的门票展会资料</p>
                    </dd>
                </dl>
            </form>
        </div>
    </div>
</div>

<script>
    var mobileDom000 = $('input[data-id="mobile000"]'),
        codeDom000 = $('a[data-id="getCode000"]'),
        getCodeCycle000 = 60;

    codeDom000.click(function () {
        var mobile = mobileDom000.val();
        if (!/^1\d{10}$/.test(mobile)) {
            alert("请输入正确的手机号");
            return false;
        }

        if (!codeDom000.hasClass('disabled')) {
            $.post(
                GET_MSG_CODE,
                {mobile : mobile},
                function (res) {
                    res = JSON.parse(res);
                    if (res.status) {
                        if (codeDom000.hasClass('disabled')) {
                            return false;
                        }

                        codeDom000.attr('data-cycle', getCodeCycle000).addClass('disabled');

                        var cycleClock = setInterval(function () {
                            var cycle = parseInt(codeDom000.attr('data-cycle'));
                            if (cycle > 0) {
                                codeDom000.text("" + cycle +"s").attr('data-cycle', cycle-1);
                            } else {
                                codeDom000.text('验证码').removeClass('disabled');
                                clearInterval(cycleClock);
                            }
                        }, 1000);
                    }
                }
            )
        }
    });

    var domShadow = $('#globalLoginShadow');
    domShadow.find('[data-id="confirm"]').bind('click', function () {
        var form = $('#globalLoginShadow').find('form').eq(0);
        //
        var fData = {};
        form.serializeArray().forEach(function (d) {
            fData[d.name] = d.value;
        });
        if (fData.name.length < 1) {
            alert ('请输入姓名');
            return false;
        }
        if (!/^1\d{10}$/.test(fData.mobile)) {
            alert ('请输入手机号');
            return false;
        }
        if (fData.code.length < 1) {
            alert ('请输入手机验证码');
            return false;
        }

        $.ajax({
            url: form.attr('action'),
            data : new FormData(form.get(0)),
            type: 'POST',
            cache: false,
            processData: false,
            contentType: false,
            success: function (res) {
                res = JSON.parse(res);
                if (res.status) {
                    new Promise(function (resolve) {
                        domShadow.modal('hide');
                        setTimeout(resolve, 500, res.data)
                    }).then(function(data) {
                        alertSuccessHtml(data.href);
                    });

                    return false;
                }

                alert(res.msg);
            }
        });

        return false;
    });
</script>