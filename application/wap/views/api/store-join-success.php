<?php
global $currentCity;

?>
<div class="modal-dialog">
<div class="modal-content">
    <!-- 模态框头部 -->
    <div class="modal-header">
        <img src="/public/wap/images/icon_ok.png" alt="ok" class="img">
        <div class="con">
            <p class="p_title">店铺预约成功</p>
            <p class="p_more"><?= $store->nickname; ?></p>
        </div>
    </div>
    <!-- 模态框主体 -->
    <div class="modal-body">
        <div class="con_img">
            <img src="<?= $store->avatar; ?>" alt="<?= $store->nickname; ?>" class="img">
            <p class="p1"><?= $store->nickname; ?></p>
            <p class="p2">展位号：<span><?= $storeApply->zw_pos; ?></span></p>
        </div>
        <div class="con_more orange">
            <dl>
                <dt>活动时间：</dt>
                <dd><?= date('m月d日', strtotime($ticket->open_date)); ?> - <?= date('m月d日', strtotime($ticket->end_date)); ?></dd>
            </dl>
            <dl>
                <dt>活动地址：</dt>
                <dd><?= $ticket->open_place; ?>（<?= $ticket->address; ?>）</dd>
            </dl>
        </div>
    </div>
    <!-- 模态框底部 -->
    <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">确 定</button>
    </div>
</div>
</div>