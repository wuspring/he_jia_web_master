<?php
global $currentCity;

?>
<div class="modal-dialog">
    <div class="modal-content" style="height: 470px;">
        <!-- 模态框主体 -->
        <div class="modal-body">
            <p class="p1 orange">领劵成功</p>
            <p class="p2"><?= trim($goodCoupon->user->nickname); ?>品牌<?= $goodCoupon->money; ?>元优惠劵</p>
            <div class="con">
                <p class="p">活动规则</p>
                <div class="d_con">
                    <?= $goodCoupon->describe; ?>
                </div>
                <p>使用日期：<?= date('Y-m-d', strtotime($mCoupon->begin_time)); ?>至<?= date('Y-m-d', strtotime($mCoupon->end_time)); ?> </p>
                <a href="<?= \DL\service\UrlService::build('member/my-coupon'); ?>" class="a orange">查看已领优惠券 &gt;&gt;</a>
            </div>
            <a href="javascript:void(0)" class="btn orange" data-dismiss="modal">确 定</a>
            <?php if ($ticket) :?>
            <p class="p_more">注：该券仅限展会现场（<?= $ticket->open_place; ?>）下单使用，单笔订单仅限使用一张不找零不兑现</p>
            <?php else :?>
                <p class="p_more">注：该券仅限商家（<?= trim($goodCoupon->user->nickname); ?>）下单使用，单笔订单仅限使用一张不找零不兑现</p>
            <?php endif; ?>
        </div>
    </div>
</div>