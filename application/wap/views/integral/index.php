<?php
/* @var $this yii\web\View */
use  yii\helpers\Url;
?>

<!--- 积分商城首页 -->
<div class="head">
    <h2>积分商城</h2>
</div>
<div id="points_mall">
    <?php if(!empty($banners)):?>
    <div class="slider carousel slide" data-ride="carousel">
        <!-- 指示符 -->
        <ul class="carousel-indicators">
            <?php foreach ($banners as $key=>$item):?>
            <li data-target="#slider" data-slide-to="<?=$key?>" class="<?=$key==0?'active':''?>"></li>
            <?php endforeach;?>
        </ul>
        <!-- 轮播图片 -->
        <div class="carousel-inner">
            <?php foreach ($banners as $key=>$item):?>
            <a href="javascript:void(0)" class="carousel-item <?=$key==0?'active':''?>">
                <img src="<?=$item?>">
            </a>
            <?php endforeach;?>
        </div>
        <!-- 左右翻页不需要但是删除会报错 -->
        <a id="carleft" class="left carousel-control" href="#slider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a id="carright" class="right carousel-control" href="#slider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <?php endif;?>

    <div class="user_nav">
        <ul>
            <li><a href="<?=\yii\helpers\Url::to(['integral-menu/gift'])?>"><img src="/public/wap/images/points_nav_ico1.png"/><p>全部礼品</p></a></li>
            <li><a href="<?=\yii\helpers\Url::to(['integral-treasure/index'])?>"><img src="/public/wap/images/points_nav_ico2.png"/><p>积分夺宝</p></a></li>
            <li><a href="<?=\yii\helpers\Url::to(['integral-menu/luck-draw'])?>"><img src="/public/wap/images/points_nav_ico3.png"/><p>积分抽奖</p></a></li>
            <li><a href="<?=\yii\helpers\Url::to(['integral-menu/my-integral'])?>"><img src="/public/wap/images/points_nav_ico4.png"/><p>我的积分</p></a></li>
            <li><a href="<?=\yii\helpers\Url::to(['integral-cart/index'])?>"><img src="/public/wap/images/points_nav_ico5.png"/><p>购物车</p></a></li>
            <div class="clear"></div>
        </ul>
    </div>
    <!--- 积分商城广告 -->
    <?php if(!empty($navNextAd)):?>
    <div class="points_mall_gg">
        <?php foreach ($navNextAd as $key=>$item):?>
        <a href="<?=$item['url']?>">
            <img src="<?=$item['picture']?>" alt="<?=$item['title']?>"/>
        </a>
        <?php endforeach;?>
    </div>
    <?php endif;?>
    <!--- 热门商品 -->
    <div class="pro_list">
        <div class="title">
            <h2>热门商品</h2>
            <a href="<?=\yii\helpers\Url::to(['integral-menu/gift'])?>">查看更多></a>
        </div>
        <div class="cont">
            <ul>
                <?php if(!empty($hots_goods)):?>
                    <?php foreach ($hots_goods as $k=>$it):?>
                        <li>
                            <a href="<?=\yii\helpers\Url::to(['integral/goods-detail','id'=>$it->id])?>">
                                <img style="height: 120px" src="<?=$it->goods_pic?>"/>
                                <p><?=$it->goods_name?></p>
                                <span><b><?=$it->goods_integral?>积分</b></span>
                            </a>
                        </li>
                    <?php endforeach;?>
                <?php endif;?>
                <div class="clear"></div>
            </ul>
        </div>
    </div>

    <!--- 家居用品 -->
    <?php if(!empty($floors)):?>
        <?php foreach ($floors as $key=>$item):?>
            <?php if(!empty($item->goods)):?>
                <div class="pro_list">
                    <div class="title">
                        <h2><?=$item->name?></h2>
                        <a href="<?=Url::to(['integral-menu/gift','gc_id'=>$item->id])?>">查看更多></a>
                    </div>
                    <div class="cont">
                        <ul>
                            <?php if(!empty($item->goods)):?>
                            <?php foreach ($item->goods as $k=>$it):?>
                            <li>
                                <a href="<?=\yii\helpers\Url::to(['integral/goods-detail','id'=>$it->id])?>">
                                    <img style="height: 120px" src="<?=$it->goods_pic?>"/>
                                    <p><?=$it->goods_name?></p>
                                    <span><b><?=$it->goods_integral?>积分</b></span>
                                </a>
                            </li>
                            <?php endforeach;?>
                            <?php endif;?>
                            <div class="clear"></div>
                        </ul>
                    </div>
                </div>
            <?php endif;?>
        <?php endforeach;?>
    <?php endif;?>
</div>

<!-- 菜单是5个，需要加menu_5.四个则不加 -- 首页菜单 -->
<?php
$requestHref = trim($_SERVER['REQUEST_URI'], '/');
?>
<?php if(!empty($menus)):?>
    <div class="footer_menu <?=count($menus)==5?'menu_5':''?>">
        <ul class="list-unstyled">
            <?php foreach ($menus as $key=>$item):?>
                <li class="<?=$requestHref == trim($item->url, '/')?'active':''?>">
                    <a href="<?=$item->url?>">
                        <img src="<?=$item->icon?>" class="img">
                        <img src="<?=$item->icon_selected?>" class="img_h">
                        <span><?=$item->name?></span>
                    </a>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
<?php endif;?>

<!-- 轮播图-手指滑动效果 -->
<script type="text/javascript">

    $(function () {
        // 获取手指在轮播图元素上的一个滑动方向（左右）
        // 获取界面上轮播图容器
        var $carousels = $('.carousel');
        var startX,endX;
        // 在滑动的一定范围内，才切换图片
        var offset = 50;
        // 注册滑动事件
        $carousels.on('touchstart',function (e) {
            // 手指触摸开始时记录一下手指所在的坐标x
            startX = e.originalEvent.touches[0].clientX;

        });
        $carousels.on('touchmove',function (e) {
            // 目的是：记录手指离开屏幕一瞬间的位置 ，用move事件重复赋值
            endX = e.originalEvent.touches[0].clientX;
        });
        $carousels.on('touchend',function (e) {
            //console.log(endX);
            //结束触摸一瞬间记录手指最后所在坐标x的位置 endX
            //比较endX与startX的大小，并获取每次运动的距离，当距离大于一定值时认为是有方向的变化
            var distance = Math.abs(startX - endX);
            if (distance > offset){
                //说明有方向的变化
                //根据获得的方向 判断是上一张还是下一张出现
                $(this).carousel(startX >endX ? 'next':'prev');
            }
        })
    });

</script>