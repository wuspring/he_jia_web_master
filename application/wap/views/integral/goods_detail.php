<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
use \DL\service\UrlService;
?>
<script>
    // sku data;
    var goodSku = <?= json_encode($goods->skuInfoData); ?>,
        // 选择的SKU
        setGoodSkuInfo = {},
        GOOD_ID = <?= (int)$goods->id; ?>,
        skuCondition = <?= (int)$goods->use_attr; ?>,
        ORDER_ROUT_PAY = '<?= UrlService::build('integral-order/create'); ?>',
        CART_ROUTE_ADD = '<?= UrlService::build('integral-cart/add'); ?>';
</script>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>

<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>商品详情</h2>
</div>
<?php if(!empty($goods->goods_image)):?>
    <div class="slider carousel slide" data-ride="carousel">
        <!-- 指示符 -->
        <ul class="carousel-indicators">
            <?php foreach ($goods->goods_image as $key=>$item):?>
                <li data-target="#slider" data-slide-to="<?=$key?>" class="<?=$key==0?'active':''?>"></li>
            <?php endforeach;?>
        </ul>
        <!-- 轮播图片 -->
        <div class="carousel-inner">
            <?php foreach ($goods->goods_image as $key=>$item):?>
                <a href="javascript:void(0)" class="carousel-item <?=$key==0?'active':''?>">
                    <img src="<?=$item?>">
                </a>
            <?php endforeach;?>
        </div>
        <!-- 左右翻页不需要但是删除会报错 -->
        <a id="carleft" class="left carousel-control" href="#slider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a id="carright" class="right carousel-control" href="#slider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
<?php endif;?>

<div class="product_intrgralcon1">
    <p class="p_name"><?=$goods->goods_name?></p>
    <p class="p_price2 mt-1 float-right">市场价：￥<span><?=$goods->goods_marketprice?></span></p>
    <p class="p_price">积分<span class="orange"><span><?=intval($goods->goods_integral)?></span></span></p>
    <p class="p_remark">重要提示：<br>如有质量问题或使用咨询，请拨打售后服务热线：<b><?=\DL\vendor\ConfigService::init('system')->get('telephone')?></b></p>
</div>
<div class="product_intrgralcon2">
    <p>积分规则：</p>
    <p><?=\DL\vendor\ConfigService::init('INDEX_INTEGRAL')->get('rule')?></p>
</div>
<!-- 商品介绍/规格参数/售后保障/用户评论 -->
<div class="product_intrgraltab">
    <ul class="nav nav-pills nav-justified product_intrgraltab_list"  role="tablist">
        <li class="nav-item"><a href="#intrgral_tab1" data-toggle="tab" class="nav-link active"><span>商品介绍</span></a></li>
        <li class="nav-item"><a href="#intrgral_tab2" data-toggle="tab" class="nav-link"><span>规格参数</span></a></li>
        <li class="nav-item"><a href="#intrgral_tab3" data-toggle="tab" class="nav-link"><span>售后保障</span></a></li>
        <li class="nav-item"><a href="#intrgral_tab4" data-toggle="tab" class="nav-link"><span>用户评论</span></a></li>
    </ul>
    <div class="tab-content" style="min-height: 100px;">
        <!-- 商品介绍 -->
        <div class="tab-pane product_intrgral_content active" id="intrgral_tab1">
            <?= $goods->goods_body?>
        </div>
        <!-- 规格参数 -->
        <div class="tab-pane product_intrgral_content" id="intrgral_tab2">
            <?= $goods->goods_jingle?>
        </div>
        <!-- 售后保障 -->
        <div class="tab-pane product_intrgral_content" id="intrgral_tab3">
            <?= $goods->mobile_body?>
        </div>
        <!-- 用户评论 -->
        <div class="tab-pane" id="intrgral_tab4">
            <div class="shop_assess_all">
                <div class="shopaa_left">
                    <p class="p1 orange"><?=$goods->evaluation_good_star?></p>
                    <p class="p2">总体评价</p>
                    <p class="p3">共<span><?=$goods->evaluation_count?></span>条评论</p>
                </div>
                <div class="shopaa_right">
                    <dl>
                        <dt>商品质量：</dt>
                        <dd>
                            <span class="s_star">
                                <?php for($i=1;$i<=5;$i++){ ?>
                                    <span class="<?=($i<=$qualityScore)?'active':''?>"></span>
                                <?php } ?>
                            </span>
                            <span class="orange"><?=$qualityScore?></span>
                        </dd>
                    </dl>
                    <dl>
                        <dt>商户服务：</dt>
                        <dd>
                            <span class="s_star">
                                <?php for($i=1;$i<=5;$i++){ ?>
                                    <span class="<?=($i<=$serviceScore)?'active':''?>"></span>
                                <?php } ?>
                            </span>
                            <span class="orange"><?=$serviceScore?></span>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="shop_assess_list" id="container_list">


            </div>
        </div>
    </div>
</div>
<div class="product_intrgralbtn">
    <div class="product_intrgralbtns">
        <a href="javascript:void(0)" class="btn a_cart" data-id="a_cart" data-toggle="modal" data-target="#myModalProduct">加入购物车</a>
        <a href="javascript:void(0)" class="btn a_buy" data-id="a_buy" data-toggle="modal" data-target="#myModalProduct">立即兑换</a>
    </div>
</div>
<script>
    $('[data-id="a_cart"]').click(function () {
        $('[data-type="pay"]').attr('data-info', 'cart');
    });
    $('[data-id="a_buy"]').click(function () {
        $('[data-type="pay"]').attr('data-info', 'buy');
    });
</script>

<!-- 模态框- 选择商品规格 弹框 -->
<div class="detail-alert hide"></div>
<div class="detail-select-spec hide">
    <!-- 关闭叉叉 -->
    <div class="close"><span></span></div>
    <div class="dss-price">
        <img src="<?= $goods->goods_pic; ?>" data-id="good-image">
        <span class="t1">积分：<span class="t11 orange" data-id="price"><?= intval($goods->goods_integral); ?></span></span>
        <span class="t2">库存<span data-id="storage"><?= $goods->goods_storage; ?></span>件</span>
    </div>
    <form class="dss-form">
        <!-- 属性多的时候可以滚动 -->
        <div class="dss-con">
            <?php if ($goods->use_attr) : ?>
                <?php foreach ($goods->attrInfoData AS $gid => $attr) :?>
                    <div class="dss-type">
                        <div class="title">选择<?= $attr['name']; ?></div>
                        <div class="tab">
                        <?php foreach ($attr['sons'] AS $aid => $attrInfo) :?>
                            <span data-id="attr" data-type="<?=$gid;?>" data-val="<?= $aid;?>">
                                <?= $attrInfo['name']; ?></span>
                        <?php endforeach; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else :?>
                <div class="dss-type">
                    <div class="title">选择规格</div>
                    <div class="tab">
                        <span class="hover">默认</span>
                    </div>
                </div>
            <?php endif; ?>

            <div class="dss-type">
                <div class="title">购买数量</div>
                <div class="num">
                    <a href="javascript:void(0)" class="a_reduce"  data-id="amountCtrl" data-type="reduce"><img src="/public/wap/images/icon_reduce.jpg" alt="减"></a>
                    <input type="text" class="inp" name="amount" value="1" data-limit="<?= $goods->goods_storage; ?>">
                    <a href="javascript:void(0)" class="a_plus" data-id="amountCtrl" data-type="plus"><img src="/public/wap/images/icon_plus.jpg" alt="加"></a>
                </div>
            </div>
        </div>
        <a href="javascript:void(0)" data-type="pay" <?= ((int)$goods->use_attr ? '' : 'data-sku="0"'); ?> class="btn btn_big">确 定</a>
    </form>
</div>
<script type="text/javascript">
    //选择规格弹框 
    $('.product_intrgralbtns .btn').bind('click', function() {
        $('.detail-alert,.detail-select-spec').removeClass('hide');
    });
    $('.detail-select-spec .close').bind('click', function() {
        $('.detail-alert,.detail-select-spec').addClass('hide');
    });
</script>

<!-- 轮播图-手指滑动效果 -->
<script type="text/javascript">

    var numArraySort = function (array)
    {
        var tmp;
        for(i=0; i<array.length-1; i++) {
            for(j=0;j<array.length-1-i; j++){
                if(parseInt(array[j])<parseInt(array[j+1])) {
                    tmp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tmp;
                }
            }
        }
        return array;
    };

    $(function () {
        // 获取手指在轮播图元素上的一个滑动方向（左右）
        // 获取界面上轮播图容器
        var $carousels = $('.carousel');
        var startX,endX;
        // 在滑动的一定范围内，才切换图片
        var offset = 50;
        // 注册滑动事件
        $carousels.on('touchstart',function (e) {
            // 手指触摸开始时记录一下手指所在的坐标x
            startX = e.originalEvent.touches[0].clientX;
        });
        $carousels.on('touchmove',function (e) {
            // 目的是：记录手指离开屏幕一瞬间的位置 ，用move事件重复赋值
            endX = e.originalEvent.touches[0].clientX;
        });
        $carousels.on('touchend',function (e) {
            //console.log(endX);
            //结束触摸一瞬间记录手指最后所在坐标x的位置 endX
            //比较endX与startX的大小，并获取每次运动的距离，当距离大于一定值时认为是有方向的变化
            var distance = Math.abs(startX - endX);
            if (distance > offset){
                //说明有方向的变化
                //根据获得的方向 判断是上一张还是下一张出现
                $(this).carousel(startX >endX ? 'next':'prev');
            }
        })

        getList();
    });

    var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var page = 0,size = 10;
    var totalCount= 10;
    var p = Math.ceil(totalCount/size);
    var list = '';

    function getList() {
        $('#intrgral_tab4').dropload({
            scrollArea: window,
            distance:10,
            loadDownFn : function(me){
                list = '';
                if (page < p) {
                    page++;
                    // 拼接HTML
                    $.post('<?php echo Url::toRoute(["integral/comment-list"]);?>', {
                        _csrf:_csrf,
                        page: page,
                        size: size,
                        goodsId: "<?=$goods->id?>",
                    }, function (res) {
                        if(res.status==1){
                            totalCount = res.data.totalCount;
                            p= Math.ceil(totalCount/size);
                            list = '';
                            $.each(res.data.list, function (key, val) {
                                list += sureList(val);
                            });
                            $('#container_list').append(list);
                            me.resetload();
                        }else{
                            alert(res.msg);
                        }
                    }, 'json');
                } else {
                    // 锁定
                    me.lock();
                    // 无数据
                    me.noData();
                    // 插入数据到页面，放到最后面
                    $('#container_list').append(list);
                    // 每次数据插入，必须重置
                    me.resetload();
                }
            }
        });
    }

    function sureList(obj){
        var html = '';
        html += '<div class="shopal_con">\n' +
            '            <img src="'+ obj.avatar +'" alt="'+ obj.nickname +'" class="img">\n' +
            '            <div class="con">\n' +
            '                <p class="p1"><span class="s float-right">'+ obj.createDate +'</span><span>'+ obj.nickname +'</span></p>\n' +
            '                <p class="p2">商品质量：<span class="orange">'+ obj.spzl +'</span>星&nbsp;&nbsp;商户服务：<span class="orange">'+ obj.shfw +'</span>星</p>\n' +
            '                <p class="p3">'+ obj.content +'</p>\n';
        if(obj.imgs){
            html += '<div class="d_imgs">\n';
            for (var i = 0; i < obj.imgs.length; i++) {
                html += '<img src="'+ obj.imgs[i] +'">';
            }
            html += '</div>\n';
        }
        html += '            </div>\n' +
            '        </div>';
        return html;
    }

    $('span[data-id="attr"]').click(function () {
        var i = $(this).attr('data-type'),
            val = $(this).attr('data-val');
        $('span[data-id="attr"]').each(function () {
            if ($(this).attr('data-type') == i ) {
                if ($(this).attr('data-val') == val) {
                    $(this).addClass('guige_cur hover');
                } else {
                    $(this).removeClass('guige_cur hover');
                }
            }
        });
        setGoodSkuInfo[i] = val;
        setSkuInfo();
    });
    var setSkuInfo = function () {
        var i = <?= count($goods->attrInfoData); ?>,
            k = Object.keys(setGoodSkuInfo),
            v = Object.values(setGoodSkuInfo);

        if (k.length >= i) {
            var key = numArraySort(v).join('-');
            if (goodSku[key] == undefined) {
                alert("获取信息异常，请稍后重试");
                return false;
            }

            // setting
            $('[data-type="pay"]').attr('data-sku', goodSku[key].id);
            $('a[data-id="collect"]').attr('data-sku', goodSku[key].id);

            $('img[data-id="good-image"]').attr({
                'src' : goodSku[key].picimg,
                'jqimg' : goodSku[key].picimg,
            });
            $('span[data-id="storage"]').text(goodSku[key].num);
            $('input[name="amount"]').attr('data-limit', goodSku[key].num);
            $('span[data-id="price"]').text(goodSku[key].price);
        }
    }

    // 增减控件
    $('a[data-id="amountCtrl"]').click(function () {
        var type = $(this).attr('data-type'),
            amountDom = $('input[name="amount"]');

        switch (type) {
            case 'plus' :
                var val = parseInt(amountDom.val()) + 1;
                break;
            case 'reduce' :
                var val = parseInt(amountDom.val()) - 1;
                break;
        }

        amountDom.val(val).change();
    });

    // 数量输入控件
    $('input[name="amount"]').change(function () {
        var val = $(this).val(),
            limit = $(this).attr('data-limit');
        if (!/^\d+$/.test(val)) {
            val = 0;
        }
        val = parseInt(val);

        if (val < 0 ) {
            alert("请输入有效的商品数量");
            val = 0;
        }
        if (val > parseInt(limit)) {
            alert("小二正在努力补货中...");
            val = limit;
        }

        $(this).val(val);
    });

    var rebuildQueryString = function (path, query) {
        var param = [], splitKey;
        for (var i in query) {
            param.push(i + '=' + query[i]);
        }

        splitKey = path.indexOf('?') > -1 ? '&' : '?';
        return path + splitKey + param.join('&');
    };

    // 立即购买
    var addBuyFunc = function (dom) {
        if (dom.attr('data-sku') == undefined) {
            alert("请选择购买商品的规格信息");
            return false;
        }

        var amount = $('input[name="amount"]').val(),
            sku_id = dom.attr('data-sku'),
            query = {};
        query.id = GOOD_ID;
        query.amount = amount;
        query.sku = sku_id;


        window.location.href = rebuildQueryString(ORDER_ROUT_PAY, query);
    };


    // 添加商品到购物车
    var addCart = function (dom) {
        var skuId = dom.attr('data-sku'),
            amount = $('input[name="amount"]').val();

        if (userInfo.length < 1) {
            alert("您尚未登录");
            return false;
        }
        $.ajax({
            url: CART_ROUTE_ADD,
            type:'get',
            data:{id : GOOD_ID, skuId : skuId, amount : amount},
            dataType:'json',
            success:function(data){
                if (data.status) {
                    alert("添加购物车成功");
                    // refreshCart();
                    $('.detail-alert,.detail-select-spec').addClass('hide');
                    return true;
                }
                if (data.msg.length) {
                    alert(data.msg);
                } else {
                    alert("添加失败");
                }
            }
        });
    };


    $('[data-type="pay"]').click(function () {
        var that = $(this),
            type = that.attr('data-info');

        switch (type) {
            case 'cart' :
                addCart($(this));
                break;
            case 'buy' :
                addBuyFunc($(this));
                break;
        }
    });

</script>
