<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>

<div id="container">
    <div class="head">
        <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
        <h2>用户评价</h2>
    </div>
    <div class="shop_assess_all">
        <div class="shopaa_left">
            <p class="p1 orange"><?=$shop->score?$shop->score:0?></p>
            <p class="p2">总体评价</p>
            <p class="p3">共<span><?=$shop->judge_amount?></span>条评论</p>
        </div>
        <div class="shopaa_right">
            <dl>
                <dt>商品质量：</dt>
                <dd>
                    <span class="s_star">
                        <?php for($i=1;$i<=5;$i++){ ?>
                            <span class="<?=($i<=$qualityScore)?'active':''?>"></span>
                        <?php } ?>
                    </span>
                    <span class="orange"><?= $qualityScore ?></span>
                </dd>
            </dl>
            <dl>
                <dt>商户服务：</dt>
                <dd>
                    <span class="s_star">
                        <?php for($i=1;$i<=5;$i++){ ?>
                            <span class="<?=($i<=$serviceScore)?'active':''?>"></span>
                        <?php } ?>
                    </span>
                    <span class="orange"><?= $serviceScore ?></span>
                </dd>
            </dl>
        </div>
    </div>
    <div class="shop_assess_list" id="container_list">

    </div>
</div>

<script type="text/javascript">
    var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var page = 0,size = 10;
    var totalCount= 10;
    var p = Math.ceil(totalCount/size);
    var list = '';
    var storeId = "<?=$_GET['storeId']?>";
    $(function () {
        getList();
    });

    function getList() {
        $('#container').dropload({
            scrollArea: window,
            distance:10,
            loadDownFn : function(me){
                list = '';
                if (page < p) {
                    page++;
                    // 拼接HTML
                    $.post('<?php echo Url::toRoute(["shop/comment-list"]);?>', {
                        _csrf:_csrf,
                        page: page,
                        size: size,
                        storeId: storeId,
                    }, function (res) {
                        if(res.status==1){
                            totalCount = res.data.totalCount;
                            p= Math.ceil(totalCount/size);
                            list = '';
                            $.each(res.data.list, function (key, val) {
                                list += sureList(val);
                            });
                            $('#container_list').append(list);
                            me.resetload();
                        }else{
                            alert(res.msg);
                        }
                    }, 'json');
                } else {
                    // 锁定
                    me.lock();
                    // 无数据
                    me.noData();
                    // 插入数据到页面，放到最后面
                    $('#container_list').append(list);
                    // 每次数据插入，必须重置
                    me.resetload();
                }
            }
        });
    }

    function sureList(obj){
        var html = '';
        html += '<div class="shopal_con">\n' +
            '            <img src="'+ obj.avatar +'" alt="'+ obj.nickname +'" class="img">\n' +
            '            <div class="con">\n' +
            '                <p class="p1"><span class="s float-right">'+ obj.createDate +'</span><span>'+ obj.nickname +'</span></p>\n' +
            '                <p class="p2">商品质量：<span class="orange">'+ obj.spzl +'</span>星&nbsp;&nbsp;商户服务：<span class="orange">'+ obj.shfw +'</span>星</p>\n' +
            '                <p class="p3">'+ obj.content +'</p>\n';
        if(obj.imgs){
            html += '<div class="d_imgs">\n';
            for (var i = 0; i < obj.imgs.length; i++) {
                html += '<img src="'+ obj.imgs[i] +'">';
            }
            html += '</div>\n';
        }
        html += '            </div>\n' +
            '        </div>';
        return html;
    }
</script>