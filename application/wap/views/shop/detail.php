<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>

<div class="head" style="position: fixed;top: 0;z-index: 9999;width: 100%;">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <a href="/" style="float: right;"><img src="/public/images/icon-home.png" style="width: 18px;height: 18px;" alt="主页"></a>
    <h2><?=$shop->nickname?></h2>
</div>

<div class="shopdetail_con" style="padding-top: 50px">
    <div class="shoplist_con mt-0">
        <img src="<?=$shop->avatar?>" alt="<?=$shop->nickname?>" class="img">
        <p class="p_name text-truncate"><?=$shop->nickname?></p>
        <a href="javascript:void(0)" class="btn btn_outline float-right click_collect" data-collect="0" style="<?=($shop->is_collect==1)?'display: none':''?>"><img src="/public/wap/images/icon_star.png">收藏</a>
        <a href="javascript:void(0)" class="btn btn_outline float-right active click_collect" data-collect="1" style="<?=($shop->is_collect==0)?'display: none':''?>"><img src="/public/wap/images/icon_stared.png">已收藏</a>
        <p class="p_shop orange mt-1">共<span><?=$shop->store_amount?></span>家体验店</p>
        <p class="p_name text-truncate">
            <?php for($i=1;$i<=5;$i++){ ?>
            <em class="<?=($i<=$shop->score)?'active':''?>"></em>
            <?php } ?>
            <span><span><?=$shop->judge_amount?></span>条评论</span>
        </p>
        <a href="javascript:void(0)" class="a_coupon" data-toggle="modal" data-target="#myModalTicket1">
            <span class="s_right orange">更多活动<img src="/public/wap/images/icon_right.png" alt=""></span>
            <?php if(!empty($shop->tabs)):?>
                <?php foreach ($shop->tabs as $key=>$item):?>
                    <?php if($key<1):?>
                        <span class="y"><?=$item['tab']?></span>
                        <span class="text-truncate"><?=$item['info']?></span>
                    <?php endif;?>
                <?php endforeach;?>
            <?php endif;?>
        </a>
    </div>
</div>

<?php if(!empty($shop->coupons)):?>
<div class="shopdetail_coupon">
    <div class="com_title">
        <div class="com_title_con">
            <a href="<?=Url::to(['coupon/notes','storeId'=>$shop->user_id])?>">使用规则></a>
            <span class="s_title">领取优惠劵</span>
        </div>
    </div>
    <div class="com_coupon">
        <!-- 点击整条，弹出领取弹框  已经领取 的加ed  点击无效 -->
        <?php if(!empty($shop->coupons)):?>
            <?php foreach ($shop->coupons as $key=>$item):?>
            <div class="com_coupon_con">
                <a href="javascript:void(0)" class="float-right a" data-id="store-coupon" data-coupon="<?= $item->id; ?>" data-ticket="<?=$shop->ticket_id; ?>">立即<br>领取</a>
                <img src="<?=$shop->avatar?>" alt="<?=$shop->nickname?>" class="img">
                <div class="con">
                    <p class="p1 orange">￥<span><?=$item->money?></span>优惠劵</p>
                    <p class="p2 text-truncate">支付满<?=$item->condition?>元可用</p>
<!--                    <p class="p3 text-truncate">(有效期:2019.05.01-05.03)</p>-->
                </div>
            </div>
        </div>
        <?php endforeach; endif;?>
    </div>
</div>
<?php endif;?>

<?php if(!empty($shop->order_goods)):?>
<div class="shopdetail_prestore">
    <div class="com_title">
        <div class="com_title_con">
            <span class="s_title">预定享特价</span>
        </div>
    </div>
    <ul class="list-unstyled clearfix com_productlist com_productlistone pt-2">
        <?php foreach ($shop->order_goods as $key=>$item):?>
        <li>
            <a href="<?=Url::to(['goods/detail','id'=>$item->good_id])?>" class="product">
                <span class="product_img float-left"><img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?=$item->good_pic?>" alt="<?=$item->goods_name?>"></span>
                <div class="product_con">
                    <p class="product_name text-truncate"><?=$item->goods_name?></p>
                    <p class="product_price orange">专享价<span><?=$item->good_price?></span>元/<span><?=$item->unit?></span><span class="s"><span><?=$item->goods_marketprice?></span>元/<span><?=$item->unit?></span></span></p>
                    <p class="product_tag orange"><img src="/public/wap/images/icon_time_orange.png">仅剩<span><?=$item->good_amount?></span><span>件</span></p>
                    <p class="product_handsel">订金：<span class="orange">￥<span><?=$item->order_price?></span></span></p>
                </div>
            </a>
            <a href="javascript:void(0)" data-id="order-create" data-good="<?=$item->good_id?>" data-ticket="<?=$item->ticket_id?>" class="btn float-right">立即预定</a>
        </li>
        <?php endforeach;?>
    </ul>
</div>
<?php endif;?>

<?php if(!empty($shop->advance_goods)):?>
<div class="shopdetail_appointment">
    <div class="com_title">
        <div class="com_title_con">
            <span class="s_title">爆款预约</span>
        </div>
    </div>
    <ul class="list-unstyled clearfix com_productlist com_productlistone pt-2">
        <?php foreach ($shop->advance_goods as $key=>$item):?>
        <li>
            <a href="<?=Url::to(['goods/detail','id'=>$item->good_id])?>" class="product">
                <span class="product_img float-left"><img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?=$item->good_pic?>" alt="<?=$item->goods_name?>"></span>
                <div class="product_con">
                    <p class="product_name ellipsis"><?=$item->goods_name?></p>
                    <p class="product_tag orange"><img src="/public/wap/images/icon_time_orange.png">仅剩<span><?=$item->good_amount?></span><span>件</span></p>
                    <p class="product_handsel"><span class="orange">￥<span><?=$item->good_price?></span><span class="s">/<?=$item->unit?></span></span></p>
                </div>
            </a>
            <a href="javascript:void(0)" data-id="get-coupon" data-good="<?=$item->good_id?>" data-ticket="<?=$item->ticket_id?>" class="btn float-right">立即预约</a>
        </li>
        <?php endforeach;?>
    </ul>
</div>
<?php endif;?>

<?php if(!empty($shop->shop_goods)):?>
<div class="shopdetail_prestore">
    <div class="com_title">
        <div class="com_title_con">
            <span class="s_title">全部商品</span>
        </div>
    </div>
    <ul class="list-unstyled clearfix com_productlist com_productlisttwo pt-2">
        <?php foreach ($shop->shop_goods as $key=>$item):?>
        <li>
            <a href="<?=Url::to(['goods/detail','id'=>$item->id])?>" class="product">
                <span class="product_img"><img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?=$item->goods_pic?>" alt="<?=$item->goods_name?>"></span>
                <p class="product_name ellipsis"><?=$item->goods_name?></p>
                <p class="product_handsel"><span class="orange">￥<span><?=$item->goods_price?></span></span>/<?=$item->unit?></p>
            </a>
            <?php if (in_array($item->id, array_keys($orderGoodIds))) :?>
            <a href="javascript:void(0)" data-id="order-create" data-good="<?=$item->id?>" data-ticket="<?= $orderGoodIds[$item->id];?>" class="btn">立即预约</a>
            <?php elseif (in_array($item->id, array_keys($orderCouponIds))) :?>
            <a href="javascript:void(0)" data-id="get-coupon" data-good="<?=$item->id?>" data-ticket="<?=$orderCouponIds[$item->id];?>" class="btn">立即预约</a>
            <?php else : ?>
            <a href="javascript:void(0)" class="btn disabled">立即预约</a>
            <?php endif; ?>
        </li>
        <?php endforeach;?>
    </ul>
</div>
<?php endif;?>

<div class="shopdetail_con">
    <div class="com_title">
        <div class="com_title_con">
            <span class="s_title">品牌介绍</span>
        </div>
    </div>
    <div class="shopdetail_brand up mt-0">
        <?= $shop->describe?>
    </div>
</div>

<?php if(!empty($shop->shop_list)):?>
<div class="shopdetail_shore">
    <div class="com_title">
        <div class="com_title_con">
            <span class="s_title">到店体验</span>
        </div>
    </div>
    <div class="shopdetail_shore_list">
    <?php foreach ($shop->shop_list as $key=>$item):?>
        <a href="<?=Url::to(['shop/shop-map','storeId'=>$item->user_id])?>">
            <img src="<?=$shop->avatar?>" alt="" class="img">
            <p class="p_name text-truncate"><?=$item->store_name?></p>
            <p class="p_address"><?=$item->address?></p>
        </a>
    <?php endforeach;?>
    </div>
</div>
<?php endif;?>

<!-- 模态框- 参展店铺-优惠活动 弹框 -->
<div class="modal modal_ticket fade" id="myModalTicket1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">优惠活动</p>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <?php if(!empty($shop->tabs)):?>
                    <?php foreach ($shop->tabs as $key=>$item):?>
                    <p><b><?=$item['tab']?>：</b><?=$item['info']?></p>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>

<script>
    //点击收藏 点击取消
    $(".click_collect").on('click',function () {
        var shop_id=<?=$shop->user_id?>;
        var is_collect=$(this).attr('data-collect');
        var url="<?=Url::toRoute(['shop/collect'])?>";

        if (Object.keys(userInfo).length < 1) {
            alert ("您还没有登录哦");
            return false;
        }
        if (is_collect==1){   //取消收藏
            $.get(url,{is_collect:1,shop_id:shop_id},function (res) {
                if (res.code==1){
                    $('[data-collect="0"]').show();
                    $('[data-collect="1"]').hide();
                }
                alert(res.msg);
            },'json');

        }else {
            $.get(url,{is_collect:0,shop_id:shop_id},function (res) {
                if (res.code==1){
                    $('[data-collect="0"]').hide();
                    $('[data-collect="1"]').show();
                }
                alert(res.msg);
            },'json');
        }
    });
</script>
