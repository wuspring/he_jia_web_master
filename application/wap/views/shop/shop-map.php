<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>
<script type="text/javascript" src="https://map.qq.com/api/js?v=2.exp&key=YLQBZ-4M5LX-PBP44-TG3PB-XAD35-RYF2Q"></script>

<div class="head">
    <a href="javascript:history.go(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>门店地址</h2>
</div>

<div class="shop_store">
    <!-- Swiper -->
    <div class="swiper-container gallery-top swiper-container-initialized swiper-container-horizontal">
        <div class="swiper-wrapper">
            <?php if(!empty($shop_list)):?>
                <?php foreach ($shop_list as $key=>$item):?>
                <div class="swiper-slide">
                    <div id="allMap_<?=$item->id?>" style="height: 680px"></div>
                    <script>
                        var center<?=$item->id?> = new qq.maps.LatLng(<?=$item->lat?>,<?=$item->lng?>);
                        var map<?=$item->id?> = new qq.maps.Map(document.getElementById("allMap_<?=$item->id?>"), {
                            center: center<?=$item->id?>,
                            zoom: 15
                        });
                        var marker<?=$item->id?> = new qq.maps.Marker({
                            position: center<?=$item->id?>,
                            map: map<?=$item->id?>
                        });
                        marker<?=$item->id?>.setPosition(center<?=$item->id?>);
                    </script>
                </div>
                <?php endforeach;?>
            <?php endif;?>
        </div>
    </div>
    <div class="swiper-container gallery-thumbs swiper-container-initialized swiper-container-horizontal">
        <div class="swiper-wrapper">
            <?php if(!empty($shop_list)):?>
                <?php foreach ($shop_list as $key=>$item):?>
                <div class="swiper-slide">
                    <p class="p1"><span class="s"><img src="/public/wap/images/icon_address.png"><span><?=$item->distance?>km</span></span><span class="text-truncate"><?=$item->store_name?></span></p>
                    <p class="p2"><span>营业时间：</span><?=$item->work_times?>(周一到周日)</p>
                    <p class="p2"><span>门店地址：</span><?=$item->address?></p>
                </div>
                <?php endforeach;?>
            <?php endif;?>
        </div>
    </div>
</div>

<script>
    var galleryTop = new Swiper('.shop_store .gallery-top', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        onlyExternal : true,
        spaceBetween: 10,
    });
    var galleryThumbs = new Swiper('.shop_store .gallery-thumbs', {
        spaceBetween: 50,
        centeredSlides: true,
        slidesPerView: 'auto',
        touchRatio: 0.2,
        slideToClickedSlide: true
    });
    galleryTop.params.control = galleryThumbs;
    galleryThumbs.params.control = galleryTop;

    $(function () {
        var mapHeight = document.body.clientHeight-15;

        $('.swiper-slide div').css('height',mapHeight);
    });
</script>