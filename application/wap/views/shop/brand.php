<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>
<div class="head" style="position: fixed;top: 0;z-index: 9999;width: 100%;">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <a href="/" style="float: right;"><img src="/public/images/icon-home.png" style="width: 18px;height: 18px;" alt="主页"></a>
    <h2>和家网</h2>
</div>
<div id="container" style="padding-top: 50px">
    <ul class="nav nav-pills nav-justified index_prestore_tab" role="tablist">
        <li class="nav-item"><a href="<?=Url::to(['shop/index'])?>" class="nav-link"><span>精选店铺</span></a></li>
        <li class="nav-item"><a href="javascript:void(0)" class="nav-link active"><span>参展品牌</span></a></li>
    </ul>

    <div class="topNav swiper-container swiper-container-horizontal swiper-container-free-mode">
        <div class="swiper-wrapper com_tab2">
            <?php if(!empty($categorys)):?>
                <?php foreach ($categorys as $key=>$item):?>
                    <a href="javascript:void(0)" class="swiper-slide <?=$key==0?'active':''?>" data-id="<?=$item->id?>">
                        <img src="<?=$item->icoImg?>">
                        <span class="text-truncate"><?=$item->name?></span>
                    </a>
                <?php endforeach;?>
            <?php endif;?>
        </div>
    </div>

    <div class="shoplist" id="container_list">

    </div>
</div>


<!-- 菜单是5个，需要加menu_5.四个则不加 -->
<?php if(!empty($menus)):?>
    <div class="footer_menu <?=count($menus)==5?'menu_5':''?>">
        <ul class="list-unstyled">
            <?php foreach ($menus as $key=>$item):?>
                <li>
                    <a href="<?=$item->url?>">
                        <img src="<?=$item->icon?>" class="img">
                        <img src="<?=$item->icon_selected?>" class="img_h">
                        <span><?=$item->name?></span>
                    </a>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
<?php endif;?>

<!-- tab左右滑动 -->
<script type="text/javascript">
    var mySwiper = new Swiper('.topNav', {
        freeMode: true,
        preventClicks : false,//默认true
        freeModeMomentumRatio: 0.5,
        slidesPerView: 'auto',
    });

    var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var page = 0,size = 10;
    var fid=0;
    var totalCount= 10;
    var p = Math.ceil(totalCount/size);
    var list = '';

    $(function () {
        $('.com_tab2').on('click','a',function () {
            $('.com_tab2 a.active').removeClass('active');
            $(this).addClass('active');
            page = 0;
            p = 1;
            $('#container_list').empty();
            $('#container_list').next().remove();
            getList();
        });
        getList();
    });

    function getList() {
        $('#container').dropload({
            scrollArea: window,
            distance:10,
            loadDownFn : function(me){
                list = '';
                if (page < p) {
                    page++;
                    // 拼接HTML
                    fid = $('.com_tab2 > a.active').data('id');
                    $.post('<?php echo Url::toRoute(["shop/list"]);?>', {
                        _csrf:_csrf,
                        page: page,
                        size: size,
                        cityId: cityIds[cityIndex],
                        type: 'brand',
                        fid: fid
                    }, function (res) {
                        if(res.status==1){
                            totalCount = res.data.totalCount;
                            p= Math.ceil(totalCount/size);
                            list = '';
                            $.each(res.data.list, function (key, val) {
                                list += sureList(val);
                            });
                            $('#container_list').append(list);
                            me.resetload();
                        }else{
                            alert(res.msg);
                        }
                    }, 'json');
                } else {
                    // 锁定
                    me.lock();
                    // 无数据
                    me.noData();
                    // 插入数据到页面，放到最后面
                    $('#container_list').append(list);
                    // 每次数据插入，必须重置
                    me.resetload();
                }
            }
        });
    }

    function sureList(obj){
        var html = '';
        var shop_url = "<?=Url::to(['shop/detail','city_id'=>$defaultCity->id,'id'=>''])?>" + obj.id;
        var goods_url = "<?=Url::to(['goods/detail','city_id'=>$defaultCity->id,'id'=>''])?>";
        html += '<div class="shoplist_con">\n' +
            '<img src="'+ obj.avatar +'" alt="'+ obj.nickname +'" class="img" data-id="order-store" data-ticket="'+obj.ticketId+'" data-store="'+obj.id+'">\n' +
            '<p class="p_name text-truncate"><span class="float-right"><span>'+ obj.judge_amount +'</span>条评论</span>'+ obj.nickname +'</p>\n' +
            '<a href="'+ shop_url +'" class="btn float-right">进入店铺</a>\n' +
            '<p class="p_shop orange mt-1">共<span>'+ obj.store_amount +'</span>家体验店</p>\n' +
            '<p class="p_tag">';

        for(var i=0;i<obj.tab_text.length;i++){
            if(i==0){
                html += '<span class="s_tag s_red"><span>'+ obj.tab_text[i].name +'</span></span>';
            }else if(i==1){
                html += '<span class="s_tag s_orange"><span>'+ obj.tab_text[i].name +'</span></span>';
            }else if(i==2){
                html += '<span class="s_tag s_blue"><span>'+ obj.tab_text[i].name +'</span></span>';
            }
        }

        html += '</p>';
        if(obj.zw_pos){
            html +='<p class="p_zhan">展位号：<span>'+ obj.zw_pos +'</span></p>';
        }
        var coupon_url = "<?=Url::to(['coupon/index'])?>";
        var coupon_desc = '';
        var length = obj.coupons.length>2?2:obj.coupons.length;
        for (var i = 0; i < length; i++) {
            coupon_desc += obj.coupons[i].describe + ';';
        }
        html += '<a href="'+ coupon_url +'" class="a_coupon"><span class="s_right orange">更多优惠<img src="/public/wap/images/icon_right.png" alt=""></span><img src="/public/wap/images/bg_coupon3.jpg" class="y"><span>'+ coupon_desc +'</span></a>';
        html += '</div>';
        return html;
    }
</script>
