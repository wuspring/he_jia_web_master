<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/12
 * Time: 15:05
 */
use yii\helpers\Url;
?>
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>我要提问</h2>
</div>
<div class="fitup_ask">
    <form action="">
        <textarea class="textarea" placeholder="请输入您的问题"></textarea>

        <a href="javascript:void(0)" class="btn btn_big">提交信息</a>
    </form>
</div>



<!-- 模态框- 文本提醒-简易版 弹框 -->
<div class="modal modal_explain modal_explain_sm fade" id="myModalExplain2">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">提交成功</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align">您已成功提交问答，请耐心等待回复！</p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <a href="<?=Url::toRoute(['fitup-school/index'])?>" class="btn" >确 定</a>
            </div>
        </div>
    </div>
</div>


<div class="modal modal_explain modal_explain_sm modal_remind fade" id="myModalRemind">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">提醒</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align" id="tipInfo"></p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn" data-dismiss="modal">确 定</a>
            </div>
        </div>
    </div>
</div>

<script>

    //点击提交
    $(".btn_big").on('click',function () {
        var content = $(".textarea").val();
        if(content.length == 0){
            $("#myModalRemind").modal('show');
            $("#tipInfo").text('内容不能为空');
            return ;
        }
        var url = "<?=Url::toRoute(['fitup-school/fitup-question'])?>";
        $.post(url,{content:content},function (res) {
            if (res.code == 1){
                $("#myModalExplain2").modal('show');
            }else {
                $("#myModalRemind").modal('show');
                $("#tipInfo").text('提交失败');
            }
        },'json')
    })
</script>