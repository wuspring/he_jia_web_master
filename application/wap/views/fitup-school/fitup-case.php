<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/13
 * Time: 9:13
 */

use yii\helpers\Url;

?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>装修案例</h2>
</div>
<div class="fitupcase_tab">
    <a href="javascript:void(0)" id="select_hx" class="a_more active">户型</a>
    <a href="javascript:void(0)" id="select_fg" class="a_more">风格</a>
    <a href="javascript:void(0)" id="select_kj" class="a_more">空间</a>
    <a href="javascript:void(0)" id="select_tj">推荐</a>
    <a href="javascript:void(0)" id="select_zx">最新</a>
</div>
<div class="fitupcase_list">

    <div class="fitupcase_ajax">

    </div>

</div>
<script type="text/javascript">

    // 页数
    var page = 0;
    // 每页展示5个
    var size = 10;
    var sum = '<?php echo $pages->totalCount;?>';
    var p = Math.ceil(sum / size);
    var result = '';
    var hx_type=0,fg_type=0,kj_type=0,tj_type=0,zj_type=0;

    $(function () {
        getList();
    });
    //户型选择
    var mobileSelect1 = new MobileSelect({
        trigger: '#select_hx',
        title: '户型',
        wheels: [
            {data:<?=$house_type?>}
        ],
        position:[1], //初始化定位
        callback:function(indexArr, data){
             console.log(data);
            hx_type=data[0]['id'];
            $(".fitupcase_ajax").children().remove();
            $(".dropload-down").remove();
            page = 0;
            p = 1;
            getList();
        }
    });

    //风格选择
    var mobileSelect2 = new MobileSelect({
        trigger: '#select_fg',
        title: '风格',
        wheels: [
            {data:<?=$design_style?>}
        ],
        position:[1], //初始化定位
        callback:function(indexArr, data){
            // console.log(data);
            fg_type=data[0]['id'];
            $(".fitupcase_ajax").children().remove();
            $(".dropload-down").remove();
            page = 0;
            p = 1;
            getList();
        }
    });

    //空间选择
    var mobileSelect3 = new MobileSelect({
        trigger: '#select_kj',
        title: '空间',
        wheels: [
            {data:<?=$area_type?>}
        ],
        position:[1], //初始化定位
        callback:function(indexArr, data){
            // console.log(data);
            kj_type=data[0]['id'];
            $(".fitupcase_ajax").children().remove();
            $(".dropload-down").remove();
            page = 0;
            p = 1;
            getList();
        }
    });
      //推荐
    $("#select_tj").on('click',function () {
        $(".fitupcase_ajax").children().remove();
        $(".dropload-down").remove();
        page = 0;
        p = 1;
        tj_type=1;
        getList();
    });
        //最新
    $("#select_zx").on('click',function () {
        $(".fitupcase_ajax").children().remove();
        $(".dropload-down").remove();
        page = 0;
        p = 1;
        zj_type=1;
        getList();
    });
    function getList() {
        $('.fitupcase_list').dropload({
            scrollArea: window,
            distance: 10,
            loadDownFn: function (me) {
                if (page < p) {
                    page++;
                    // 拼接HTML
                    result = '';
                    $.get('<?php echo Url::toRoute(["fitup-school/fitup-case"]);?>', {
                        page: page,
                        'per-page': size,
                        hx_type:hx_type,
                        fg_type:fg_type,
                        kj_type:kj_type,
                        tj_type:tj_type,
                        zj_type:zj_type,
                    }, function (data) {
                        $.each(data.list, function (key, val) {
                            var url = "<?=Url::to(['fitup-school/fitup-case-detail','id'=>''])?>" + val.id;
                            result += '<div  class="fitupcase_list_con">\n' +
                                '            <a href="'+url+'" class="flc_a">\n' +
                                '                <img src="'+val.pic+'" alt="">\n' +
                                '                <p class="p1 text-truncate">'+val.name+'</p>\n' +
                                '                <p class="p2"><span>'+val.style+'</span>|<span>'+val.area+'</span>|<span>'+val.houseType+'</span>|<span>'+val.houseInType+'</span></p>\n' +
                                '            </a>\n' +
                                '            <a href="shopdetail.html" class="flc_shop"><img src="/public/wap/images/icon_shop_green.png">'+val.storeName+'</a>\n' +
                                '        </div>';
                        });
                        $('.fitupcase_ajax').append(result);
                        // 每次数据插入，必须重置
                        me.resetload();
                    }, 'json');
                } else {

                    // 锁定
                    me.lock();
                    // 无数据
                    me.noData();
                    // 为了测试，延迟1秒加载
                    // 插入数据到页面，放到最后面
                    $('.infolist').append(result);
                    //settime();
                    // 每次数据插入，必须重置
                    me.resetload();
                }
            }
        });
    }
</script>
