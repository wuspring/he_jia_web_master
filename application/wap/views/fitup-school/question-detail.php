<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/12
 * Time: 16:35
 */
use yii\helpers\Url;
?>

<div class="head">
    <a href="<?=Url::toRoute(['fitup-school/questions-list'])?>"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>装修问答</h2>
</div>
<div class="fitupask_list">
    <div class="fitupask_list_con">
        <p class="p_title ellipsis"><em>Q</em><?=$problem->problem?></p>
        <div class="d_user">
            <img src="<?=$problem->member->avatar?>">
            <span class="s_right float-right"><span class="orange"><?=$problem->answer_num?></span>回答</span>
            <p class="p1"><?=$problem->member->nickname?></p>
            <p class="p2"><?=date('Y-m-d',strtotime($problem->answer_num))?></p>
        </div>
    </div>
    <div class="fitupask_list_con">
        <?php foreach ($allAnswer as $k=>$item):?>
            <div class="d_user">
                <img src="<?=($item->member->avatar)?$item->member->avatar:'/public/wap/images/user_head.jpg'?>">
                <span class="s_right float-right mt-0"><?=date('Y-m-d',strtotime($item->create_time))?></span>
                <p class="p1"><?=$item->member->nickname?></p>
                <p class="p2"><?=$item->content?></p>
            </div>
        <?php endforeach;?>
    </div>
</div>
<div class="product_btn">
    <a href="javascript:void(0)" class="btn btn_big">我要回答</a>
</div>

<!-- 模态框- 提醒弹框-3s自动消失 -->
<div class="modal modal_explain modal_explain_sm modal_remind fade" id="myModalRemind">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">提醒</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align" id="tipInfo"></p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn" data-dismiss="modal">确 定</a>
            </div>
        </div>
    </div>
</div>

<script>

    //点击回答
    $(".btn_big").on('click',function () {

        var urlto = "<?=Url::toRoute(['fitup-school/answer-question','id'=>$id])?>";
        var  url = "<?=Url::toRoute(['fitup-school/judge-login'])?>";
        $.get(url,function (res) {
            if (res.code == 1){
                $("#myModalRemind").modal('show');
                $("#tipInfo").text(res.msg)
            } else {
                window.location.href=urlto;
            }
        },'json');
    })

</script>