<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/13
 * Time: 11:00
 */
use  yii\helpers\Url;
?>

<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>案例详情</h2>
</div>
<div class="fitup_casedetail_con1">
    <img src="<?=$fitupCase->cover_pic?>" alt="" class="img">
    <p class="p1 text-truncate"><?=$fitupCase->name?></p>
    <p class="p2"><span><?=$fitupCase->getFitupAttr($fitupCase->design_style)?></span>|<span><?=$fitupCase->area?>㎡</span>|<span><?=$fitupCase->getFitupAttr($fitupCase->house_type)?></span>|<span>-</span></p>
</div>
<div class="fitup_casedetail_con2">
    <div class="con"><a href="shopdetail.html"><img src="/public/wap/images/icon_shop_green.png"><?=$user->nickname?></a>设计师：<span><?=$fitupCase->design?></span></div>
    <div class="con">半包预算：<span><?=$fitupCase->budget?></span></div>
</div>
<div class="fitup_casedetail_con3">
    <p class="p_title"><span class="orange">案例详情</span></p>
    <div class="content">
        <?php foreach ($cont as $k=>$item):?>
            <p><?=$fitupCase->getFitupAttr($item->title)?></p>
            <div>
                <?=$item->content?>
            </div>
        <?php endforeach;?>


    </div>
</div>
<div class="product_btn">
    <a href="javascript:void(0)" class="btn btn_big clickYuyue">立即预约</a>
</div>



<!-- 模态框- 装修案例预约弹框 -->
<div class="modal modal_form fade" id="myModalFitupCase">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p1" style="text-align: center;font-weight: bold">请留下您的信息<br>方便我们为您提供专属服务</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">

                    <div class="form-group">
                        <input type="text" class="form-control" id="name" placeholder="请输入您的姓名">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="mobile" placeholder="请输入您的手机号">
                    </div>
                    <div class="form-group form_yzm">
                        <a href="javascript:void(0)" id="smscode"  class="btn btn_yzm float-right">获取验证码</a>
                        <!-- <a href="javascript:void(0)" class="btn btn_yzm float-right disabled">50s</a> -->
                        <input type="text" class="form-control" id="code" placeholder="请输入验证码">
                    </div>
                    <button  class="btn btn_big" id="liJiYuYue">立即预约</button>
                    <dl class="dl_more">
                        <dd class="ml-0">
                            <p><b>温馨提示：</b>填写您的信息，我们为您提供以下服务：免费量房,免费户型设计,免费预算报价</p>
                        </dd>
                    </dl>
            </div>
        </div>
    </div>
</div>

<!-- 模态框- 未参展店铺预约成功弹框/装修案例预约成功弹框 -->
<div class="modal modal_success fade" id="myModalShopSuc2">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <img src="/public/wap/images/icon_ok.png" alt="ok" class="img">
                <div class="con">
                    <p class="p_title">案例预约成功</p>
                    <p class="p_more"><?=$user->nickname?></p>
                </div>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <div class="con_img">
                    <img src="<?=$user->avatar?>" alt="" class="img">
                    <p class="p1"><?=$user->nickname?></p>
                    <p class="p4 orange">共<span><?=$user->storeCount?></span>家体验店</p>
                </div>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">确 定</button>
            </div>
        </div>
    </div>
</div>

<!-- 提示信息 -->
<div class="modal modal_explain modal_explain_sm modal_remind fade" id="myModalRemind">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">提醒</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align" id="tipInfo"></p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn" data-dismiss="modal">确 定</a>
            </div>
        </div>
    </div>
</div>


<script>


    var issend=true;
    var t=60;
    var code='';
    var  user_id= <?=$fitupCase->user_id?>;

    $(document).on('click','#smscode',function () {

        var mobile=$("#mobile").val();

        var check_phone_number = /^1[3456789]\d{9}$/;
        if (mobile.length == 0) {
            tipModel('手机号不能为空');
            return;
        }
        if (mobile.length != 11) {
            tipModel('请输入有效的手机号');
            return;
        }
        if (!mobile.match(check_phone_number)) {

            tipModel('请输入有效的手机号');
            return;
        }
        if (issend){
            issend=false;
            for(i=1;i<=60;i++) {
                window.setTimeout("update_a(" + i + ","+t+")", i * 1000);
            }
            $("#smscode").addClass('disabled');
            $.post('<?php echo Url::toRoute(['api/smscode']);?>',{mobile:mobile},function(res){
                if (res.state==1){
                    code=res.code;
                }
                alert(res.msg)
            },'json')
        }

    });

    function update_a(num,t) {
        var get_code=document.getElementById('smscode');
        if(num == t) {
            get_code.innerHTML =" 重新发送 ";
            issend=true;
            $("#smscode").removeClass('disabled');
        }
        else {
            var printnr = t-num;
            get_code.innerHTML =printnr +" 秒";
        }
    }


    //点击
    $(".clickYuyue").on('click',function () {

        var  url = "<?= Url::toRoute(['fitup-school/judge-login'])?>";
        $.get(url,function (res) {
            if (res.code==1){
                //tipModel(res.msg);
                $("#myModalFitupCase").modal();
            }else {
                $("#myModalFitupCase").modal('show');
            }
        },'json');
    });

    //立即预约
    $("#liJiYuYue").on('click',function () {
        var smscode=$("#code").val();
        var mobile=$("#mobile").val();
        var name=$("#name").val();

        var check_phone_number = /^1[3456789]\d{9}$/;
        if (mobile.length == 0) {
            tipModel('手机号不能为空');
            return;
        }
        if (mobile.length != 11) {
            tipModel('请输入有效的手机号');
            return;
        }
        if (!mobile.match(check_phone_number)) {
            tipModel('请输入有效的手机号');
            return;
        }
        if (name.length==0){
            tipModel('请输入名字');
            return;
        }
        if (smscode.length==0){
            tipModel('请输入验证码');
            return;
        }

        $.post('<?php echo Url::toRoute(['fitup-school/fitup-appointment']);?>',{smscode:smscode,mobile:mobile,name:name,user_id:user_id},function(res){
            if (res.code==1){
                $("#myModalFitupCase").modal('hide');
                $("#myModalShopSuc2").modal('show');

            }else{
                tipModel(res.msg)
            }
        },'json')

    });

    //点击提示
    function  tipModel(text) {
         alert(text)
        // $("#myModalRemind").modal('show');
        // $("#tipInfo").text(text);
    }

</script>

