<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/12
 * Time: 13:30
 */
$this->title = '装修学堂';
$this->params['breadcrumbs'][] = $this->title;

use  yii\helpers\Url;

?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>
<div class="head">
    <h2>装修学堂</h2>
</div>
<div class="topNav swiper-container swiper-container-horizontal swiper-container-free-mode">
    <div class="swiper-wrapper com_tab2">
        <a href="<?= Url::toRoute(['fitup-school/fitup-case']) ?>" class="swiper-slide"><img
                    src="/public/wap/images/_temp/img_type11.png"><span>装修案例</span></a>
        <?php foreach ($newClass as $k => $v): ?>
            <a href="<?= Url::toRoute(['news/fitup-news-list', 'fid' => $v->id]) ?>" class="swiper-slide"><img
                        src="<?= $v->icoImg ?>"><span><?= $v->name ?></span></a>
        <?php endforeach; ?>
    </div>
</div>
<div class="fitupschool_ask">
    <div class="com_title">
        <div class="com_title_con">
            <a href="javascript:void(0)" style="width: 50%;">累计解决<span><?= $problemNum ?></span>例用户提问</a>
            <span class="s_title">装修问答</span>
        </div>
    </div>
    <div class="slider carousel slide" data-ride="carousel">
        <!-- 指示符 -->
        <ul class="carousel-indicators" style="display: none;">
            <li data-target="#slider" data-slide-to="0" class="active"></li>
            <li data-target="#slider" data-slide-to="1"></li>
            <li data-target="#slider" data-slide-to="2"></li>
        </ul>
        <!-- 轮播图片 -->
        <div class="carousel-inner">
          <?php foreach ($problemStr_list as $klist=>$vlist):?>
            <ul class="list-unstyled fitupschool_ask_ul carousel-item <?=($klist == 0)?'active':''?>">
                <?php foreach ($vlist as $k=>$item):?>
                <li><a href="<?=Url::toRoute(['fitup-school/question-detail','id'=>$item['id']])?>" class="text-truncate"><?=$item['problem']?></a></li>
                <?php endforeach;?>
            </ul>
          <?php endforeach;?>
        </div>
        <!-- 左右翻页不需要但是删除会报错 -->
        <a id="carleft" class="left carousel-control" href="#slider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a id="carright" class="right carousel-control" href="#slider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="fitupschool_ask_btns">
        <a href="javascript:void(0)" id="myQuestion" class="btn"><img src="/public/wap/images/icon_fitup_ask.png" alt="我要提问">我要提问</a>
        <a href="<?=Url::toRoute(['fitup-school/questions-list'])?>" class="btn btn-outline"><img src="/public/wap/images/icon_fitup_answer.png"
                                                                 alt="我要回答">我要回答</a>
    </div>
</div>

<div class="com_ad">
    <a href="<?= $fitupAdvertising ? $fitupAdvertising->url : ''; ?>"><img src="<?= $fitupAdvertising ? $fitupAdvertising->picture : ''; ?>" alt=""></a>
</div>

<!-- 和家资讯 -->
<div class="index_news">
    <div class="index_news_list">

    </div>
</div>

<!-- 菜单是5个，需要加menu_5.四个则不加 -->

<?php if ($select_menu == 0):?>
    <?php include __DIR__ . '/common/footer_sf.php'; ?>
<?php else:?>

    <?php include __DIR__ . '/common/footer_xf.php'; ?>
<?php  endif;?>


<!-- 模态框- 提醒弹框-3s自动消失 -->
<div class="modal modal_explain modal_explain_sm modal_remind fade" id="myModalRemind">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">提醒</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align" id="tipInfo"></p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn" data-dismiss="modal">确 定</a>
            </div>
        </div>
    </div>
</div>
<!-- tab左右滑动 -->

<script type="text/javascript">

    // 页数
    var page = 0;
    // 每页展示5个
    var size = 10;
    var sum = '<?php echo $pages->totalCount;?>';
    var p = Math.ceil(sum / size);
    var result = '';
    $('.index_news').dropload({
        scrollArea: window,
        distance: 10,
        loadDownFn: function (me) {
            if (page < p) {
                page++;
                // 拼接HTML
                result = '';
                $.get('<?php echo Url::toRoute(["fitup-school/index"]);?>', {
                    page: page,
                    'per-page': size
                }, function (data) {
                    $.each(data.list, function (key, val) {
                        var url = "<?=Url::to(['news/news-detail','id'=>''])?>" + val.id;
                        result += '<a href="'+url+'" class="inl_con">\n' +
                            '            <img src="'+val.pic+'" class="img loading_img"\n' +
                            '                 data-url="'+val.pic+'">\n' +
                            '            <div class="con">\n' +
                            '                <p class="p_name ellipsis">'+val.title+'</p>\n' +
                            '                <p class="p_time"><span class="s1">会展资讯</span><span class="s2">'+val.time+'</span></p>\n' +
                            '            </div>\n' +
                            '        </a>';
                    });
                    $('.index_news_list').append(result);
                    // 每次数据插入，必须重置
                    me.resetload();
                }, 'json');
            } else {

                // 锁定
                me.lock();
                // 无数据
                me.noData();
                // 为了测试，延迟1秒加载
                // 插入数据到页面，放到最后面
                $('.infolist').append(result);

                //settime();
                // 每次数据插入，必须重置
                me.resetload();

            }
        }
    });

    var mySwiper = new Swiper('.topNav', {
        freeMode: true,
        preventClicks: false,//默认true
        freeModeMomentumRatio: 0.5,
        slidesPerView: 'auto',
    });

    $("#myQuestion").on('click',function () {

        var  urlToQuestion = "<?=Url::toRoute(['fitup-school/fitup-question'])?>";
        var  url = "<?=Url::toRoute(['fitup-school/judge-login'])?>";
        $.get(url,function (res) {
            if (res.code==1){
                $("#myModalRemind").modal('show');
                $("#tipInfo").text(res.msg);
            }else {
                window.location.href=urlToQuestion
            }
        },'json');

    })

    $(function () {
        // 获取手指在轮播图元素上的一个滑动方向（左右）
        // 获取界面上轮播图容器
        var $carousels = $('.carousel');
        var startX,endX;
        // 在滑动的一定范围内，才切换图片
        var offset = 50;
        // 注册滑动事件
        $carousels.on('touchstart',function (e) {
            // 手指触摸开始时记录一下手指所在的坐标x
            startX = e.originalEvent.touches[0].clientX;

        });
        $carousels.on('touchmove',function (e) {
            // 目的是：记录手指离开屏幕一瞬间的位置 ，用move事件重复赋值
            endX = e.originalEvent.touches[0].clientX;
        });
        $carousels.on('touchend',function (e) {
            //console.log(endX);
            //结束触摸一瞬间记录手指最后所在坐标x的位置 endX
            //比较endX与startX的大小，并获取每次运动的距离，当距离大于一定值时认为是有方向的变化
            var distance = Math.abs(startX - endX);
            if (distance > offset){
                //说明有方向的变化
                //根据获得的方向 判断是上一张还是下一张出现
                $(this).carousel(startX >endX ? 'next':'prev');
            }
        })
    });
</script>

