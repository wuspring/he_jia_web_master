<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/14
 * Time: 18:37
 */
?>
<?php $nowUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];?>
<!-- 菜单是5个，需要加menu_5.四个则不加 -->
<?php if(!empty($home_menus)):?>
    <div class="footer_menu<?=count($home_menus)==5?'_5':''?>">
        <ul class="list-unstyled">
            <?php foreach ($home_menus as $key=>$item):?>
                <li class="<?=(strpos($nowUrl,$item->url)!==false)?'active':''?>">
                    <a href="<?=$item->url?>" >
                        <img src="<?=$item->icon?>" class="img">
                        <img src="<?=$item->icon_selected?>" class="img_h">
                        <span><?=$item->name?></span>
                    </a>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
<?php endif;?>
