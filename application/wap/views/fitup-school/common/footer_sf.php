<?php if ($ticketConfig->guiders) :
    $requestHref = trim($_SERVER['REQUEST_URI'], '/');
    ?>
    <div class="footer_menu menu_5">
        <ul class="list-unstyled">
            <?php foreach ($ticketConfig->guiders AS $guider) :?>
                <li class="<?= $requestHref == trim($guider['href'], '/') ? 'active' : ''; ?>">
                    <a href="<?= $guider['href']; ?>">
                        <img src="<?= $guider['defaultIco']; ?>" class="img">
                        <img src="<?= $guider['selectIco']; ?>" class="img_h">
                        <span><?= $guider['text']; ?></span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>