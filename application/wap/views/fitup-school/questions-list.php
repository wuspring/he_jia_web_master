<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/12
 * Time: 15:14
 */

use yii\helpers\Url;

?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>
<div class="head">
    <a href="<?=Url::toRoute(['fitup-school/index'])?>"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>装修问答</h2>
</div>
<div class="fitupask_tab">
    <a href="<?=Url::toRoute(['fitup-school/questions-list','type'=>-1])?>" class="<?=($type == -1)?'active':''?>"><span>所有问题</span></a>
    <a href="<?=Url::toRoute(['fitup-school/questions-list','type'=>1])?>" class="<?=($type == 1)?'active':''?>"><span>已回答</span></a>
    <a href="<?=Url::toRoute(['fitup-school/questions-list','type'=>0])?>" class="<?=($type == 0)?'active':''?>"><span>待回答</span></a>
</div>
<div class="fitupask_list">
    <div class="fitupask_ajax">

    </div>
</div>

<script>
    // 页数
    var page = 0;
    // 每页展示5个
    var size = 10;
    var sum = '<?php echo $pages->totalCount;?>';
    var p = Math.ceil(sum / size);
    var result = '';
    var type = '<?=$type?>';
    $('.fitupask_list').dropload({
        scrollArea: window,
        distance: 10,
        loadDownFn: function (me) {
            if (page < p) {
                page++;
                // 拼接HTML
                result = '';
                $.get('<?php echo Url::toRoute(["fitup-school/questions-list"]);?>', {
                    page: page,
                    'per-page': size,
                    type:type
                }, function (data) {
                    $.each(data.list, function (key, val) {console.log(val)
                        var url = "<?=Url::to(['fitup-school/question-detail','id'=>''])?>" + val.id;
                        result += '<a href="'+url+'" class="fitupask_list_con">\n' +
                            '        <p class="p_title ellipsis"><em>Q</em>'+val.title+'</p>\n' +
                            '        <div class="d_user">\n' +
                            '            <img src="'+ (val.avatar == null ? '/public/wap/images/user_head.jpg' : val.avatar)+'">\n' +
                            '            <span class="s_right float-right"><span class="orange">'+val.num+'</span>回答</span>\n' +
                            '            <p class="p1">'+val.nickname+'</p>\n' +
                            '            <p class="p2">'+val.date+'</p>\n' +
                            '        </div>\n' +
                            '    </a>';
                    });
                    $('.fitupask_ajax').append(result);
                    // 每次数据插入，必须重置
                    me.resetload();
                }, 'json');
            } else {

                // 锁定
                me.lock();
                // 无数据
                me.noData();
                // 为了测试，延迟1秒加载
                // 插入数据到页面，放到最后面
                $('.infolist').append(result);
                //settime();
                // 每次数据插入，必须重置
                me.resetload();
            }
        }
    });

</script>