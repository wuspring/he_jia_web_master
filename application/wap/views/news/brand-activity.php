<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>

<div id="container">
    <div class="head">
        <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
        <h2>品牌活动</h2>
    </div>
    <div class="brandactivity" id="container_list">

    </div>
</div>


<!-- tab左右滑动 -->
<script type="text/javascript">
    var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var page = 0,size = 10,cid=0;
    var totalCount= '<?=$totalCount?>';
    var p= Math.ceil(totalCount/size);
    var list = '';
    $(function () {
        // dropload
        $('#container').dropload({
            scrollArea: window,
            distance:10,
            loadDownFn : function(me){
                list = '';
                if (page < p) {
                    page++;
                    // 拼接HTML
                    $.post('<?php echo Url::toRoute(["news/brand-activity"]);?>', {
                        _csrf:_csrf,
                        page: page,
                        size: size,
                        cityId: cityIds[cityIndex],
                    }, function (res) {
                        if(res.status==1){
                            list = '';
                            $.each(res.data.list, function (key, val) {
                                list += sureList(val);
                            });
                            $('#container_list').append(list);
                            me.resetload();
                        }else{
                            alert(res.msg);
                        }
                    }, 'json');
                } else {
                    // 锁定
                    me.lock();
                    // 无数据
                    me.noData();
                    // 插入数据到页面，放到最后面
                    $('#container_list').append(list);
                    // 每次数据插入，必须重置
                    me.resetload();
                }
            }
        });

        function sureList(obj){
            var html = '';
            var url = "<?=Url::to(['news/brand-detail','id'=>''])?>" + obj.id;
            html += '<a href="'+ url +'" class="brandactivity_list">\n' +
                '<img src="'+ obj.themeImg +'" class="img" alt="'+ obj.title +'">\n' +
                '<p class="p1 text-truncate">'+ obj.title +'</p>\n' +
                '<p class="p2">\n' +
                '<span class="s_read float-right"><img src="/public/wap/images/icon_read.png"><span>'+ obj.clickRate +'</span></span>\n' +
                '<span><img src="/public/wap/images/icon_date.png"><span>'+ obj.createTime +'</span></span>\n' +
                '</p>\n' +
                '</a>';
            return html;
        }
    });
</script>