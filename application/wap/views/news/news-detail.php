<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>

<div class="head" style="position: fixed;top: 0;z-index: 9999;width: 100%;">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <a href="/" style="float: right;">
        <img src="/public/images/icon-home.png" style="width: 18px;height: 18px;" alt="主页">
    </a>
    <h2>资讯详情</h2>
</div>
<div class="newsdetail" style="padding-top: 50px">
    <h3><?=$model->title?></h3>
    <p class="p_more">
        <span class="s_read float-right"><img src="/public/wap/images/icon_read.png"><span><?=$model->clickRate?></span></span>
        <span><img src="/public/wap/images/icon_date.png"><span><?=$model->createTime?></span></span>
    </p>
    <div class="con">
        <?= $model->content?>
    </div>
</div>