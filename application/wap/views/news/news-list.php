<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>

<div id="container">
    <div class="head">
        <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
        <h2>和家资讯</h2>
    </div>
    <div class="topNav swiper-container swiper-container-horizontal swiper-container-free-mode">
        <div class="swiper-wrapper com_tab">
            <?php if(!empty($cats)):?>
                <?php foreach ($cats as $key=>$item):?>
                    <a href="javascript:void(0)" data-id="<?=$item->id?>" class="swiper-slide <?=($key==0)?'active':''?>"><span><?=$item->name?></span></a>
                <?php endforeach;?>
            <?php endif;?>
        </div>
    </div>
    <div class="index_news_list mt-1" id="container_list">

    </div>
</div>

<!-- tab左右滑动 -->
<script type="text/javascript">
    var mySwiper = new Swiper('.topNav', {
        freeMode: true,
        preventClicks : false,//默认true
        freeModeMomentumRatio: 0.5,
        slidesPerView: 'auto',
    });

    var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var page = 0,size = 10,fid=0;
    var totalCount= 10;
    var p= Math.ceil(totalCount/size);
    var list = '';
    $(function () {
        getList();

        $('.com_tab').on('click','a',function () {
            $('.com_tab a').removeClass('active');
            $(this).addClass('active');
            fid = $('.com_tab .active').attr('data-id');
            page = 0;
            p = 1;
            $('#container_list').empty();
            $('#container_list').next().remove();
            getList();
        });
    });

    function getList() {
        $('#container').dropload({
            scrollArea: window,
            distance:10,
            loadDownFn : function(me){
                model = me;
                list = '';
                if (page < p) {
                    page++;
                    // 拼接HTML
                    fid = $('.com_tab .active').attr('data-id');
                    $.post('<?php echo Url::toRoute(["news/news-list"]);?>', {
                        _csrf:_csrf,
                        page: page,
                        size: size,
                        cityId: cityIds[cityIndex],
                        fid: fid
                    }, function (res) {
                        if(res.status==1){
                            totalCount = res.data.totalCount;
                            p= Math.ceil(totalCount/size);
                            list = '';
                            $.each(res.data.list, function (key, val) {
                                list += sureList(val);
                            });
                            $('#container_list').append(list);
                            me.resetload();
                        }else{
                            alert(res.msg);
                        }
                    }, 'json');
                } else {
                    // 锁定
                    me.lock();
                    // 无数据
                    me.noData();
                    // 插入数据到页面，放到最后面
                    $('#container_list').append(list);
                    // 每次数据插入，必须重置
                    me.resetload();
                }
            }
        });
    }
    function sureList(obj){
        var html = '';
        var url = "<?=Url::to(['news/news-detail','id'=>''])?>" + obj.id;
        html += '<a href="' + url +'" class="inl_con">\n' +
            '<img src="/public/wap/images/loading.jpg" class="img loading_img" data-url="'+ obj.themeImg +'">\n' +
            '<div class="con">\n' +
            '<p class="p_name ellipsis">'+ obj.title +'</p>\n' +
            '<p class="p_time"><span class="s1">会展资讯</span><span class="s2">'+ obj.createDate +'</span></p>\n' +
            '</div>\n' +
            '</a>';
        return html;
    }
</script>