<?php
/* @var $this yii\web\View */
use yii\helpers\Url;

?>
<link rel="stylesheet" href="<?php echo Yii::$app->request->hostInfo; ?>/public/css/dropload.css">
<script src="<?php echo Yii::$app->request->hostInfo; ?>/public/js/dropload.js"></script>
<div id="container">
    <div class="head">
        <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
        <h2>爆款预约</h2>
    </div>
    <div class="topNav swiper-container swiper-container-horizontal swiper-container-free-mode">
        <div class="swiper-wrapper com_tab" style="transition-duration: 300ms;transform: translate3d(0px, 0px, 0px);">
            <a class="swiper-slide active" data-id="0"><span>精品推荐</span></a>
            <?php if(!empty($cats)):?>
                <?php foreach ($cats as $key=>$item):?>
                <a class="swiper-slide" data-id="<?=$item['id']?>"><span><?=$item['name']?></span></a>
                <?php endforeach;?>
            <?php endif;?>
        </div>
    </div>

    <ul class="list-unstyled clearfix com_productlist com_productlistone mt-2 pt-2" id="container_list">

    </ul>
</div>

<!-- tab左右滑动 -->
<script type="text/javascript">
    var mySwiper = new Swiper('.topNav', {
        freeMode: true,
        preventClicks : false,//默认true
        freeModeMomentumRatio: 0.5,
        slidesPerView: 'auto',
    });
    var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>";
    var page = 0,size = 10,cid=0;
    var totalCount= 1;
    var p= Math.ceil(totalCount/size);
    var list = '';
    $(function () {
        // dropload
        getList();

        //切换导航
        $('.topNav a').on('click',function () {
            $('.topNav a').removeClass('active');
            $(this).addClass('active');
            $('#container_list').empty();
            cid = $('.topNav .active').attr('data-id');
            page = 0;
            p = 1;
            $('#container_list').next().remove();
            getList();
        });
    });


    function getList() {
        $('#container').dropload({
            scrollArea: window,
            distance:10,
            loadDownFn : function(me){
                list = '';
                if (page < p) {
                    page++;
                    // 拼接HTML
                    $.post('<?php echo Url::toRoute(["appointment/page-list"]);?>', {
                        _csrf:_csrf,
                        page: page,
                        size: size,
                        cityId: cityIds[cityIndex],
                        cid: cid
                    }, function (res) {
                        if(res.status==1){
                            totalCount = res.data.totalCount;
                            p= Math.ceil(totalCount/size);
                            list = '';
                            $.each(res.data.list, function (key, val) {
                                list += sureList(val);
                            });
                            $('#container_list').append(list);
                            me.resetload();
                        }else{
                            alert(res.msg);
                        }
                    }, 'json');
                } else {
                    // 锁定
                    me.lock();
                    // 无数据
                    me.noData();
                    // 插入数据到页面，放到最后面
                    $('#container_list').append(list);
                    // 每次数据插入，必须重置
                    me.resetload();
                }
            }
        });
    }

    function sureList(val){
        var html = '';
        var url = "<?=Url::to(['goods/detail','id'=>''])?>" + val.id;
        html += '<li>\n' +
            '            <a href="'+ url +'" class="product">\n' +
            '                <span class="product_img float-left"><img src="/public/wap/images/loading.jpg" class="loading_img" data-url="'+val.goods_pic+'" alt="'+val.goods_name+'"></span>\n' +
            '                <div class="product_con">\n' +
            '                    <p class="product_name ellipsis">'+val.goods_name+'</p>\n' +
            '                    <p class="product_tag orange"><img src="/public/wap/images/icon_time_orange.png">仅剩<span>'+val.goods_storage+'</span><span>件</span></p>\n' +
            '                    <p class="product_handsel"><span class="orange">￥<span>'+val.goods_price+'</span><span class="s">/'+val.unit+'</span></span></p>\n' +
            '                </div>\n' +
            '            </a>\n';

        if (hasCouponGoods.indexOf(val.id) > -1) {
            $(this).addClass('disabled').text('已预约');
            html += '            <a href="javascript:void(0)" class="btn float-right disabled" data-good="'+val.id+'" data-ticket="'+ val.ticket_id +'">已预约</a>\n';
        }else{
            html += '            <a href="javascript:void(0)" class="btn float-right" data-id="get-coupon" data-good="'+val.id+'" data-ticket="'+ val.ticket_id +'">立即预约</a>\n';
        }

        html +='        </li>';
        return html;
    }
</script>