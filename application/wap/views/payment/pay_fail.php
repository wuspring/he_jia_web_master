

<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>支付失败</h2>
</div>
<!--- 付款提示 -->
<div id="payment_tip">
    <div class="tip">
        <div class="box">
            <img src="/public/wap/images/successful-close.png"/>
            <h3>抱歉支付失败，请再次提交</h3>
            <div class="clear"></div>
        </div>
    </div>

    <div class="casedetail_center">
        <div class="order_tit">实付款：<span>¥<b><?= $order->order_amount; ?></b></span></div>
        <ul>
            <li>订单编号：<?= $order->orderid; ?></li>
        </ul>
    </div>

    <div class="operating">
        <a class="button1" href="<?= \DL\service\UrlService::build('index/index'); ?>">返回首页</a>
        <a class="button2" href="<?= \DL\service\UrlService::build(['order/submit', 'number' => $order->orderid]); ?>">再次支付</a>
    </div>
</div> 
