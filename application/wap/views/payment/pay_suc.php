<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>支付成功</h2>
</div>
<!--- 付款提示 -->
<div id="payment_tip">
    <div class="tip">
        <div class="box">
            <img src="/public/wap/images/order_success_ico.png"/>
            <h2>您已经付款成功，</h2>
            <p>预定成功，请立即前往商家消费！</p>
            <div class="clear"></div>
        </div>
    </div>

    <div class="casedetail_center">
        <div class="order_tit">实付款：<span>¥<b><?= $order->order_amount; ?></b></span></div>
        <ul>
            <li>订单编号：<?= $order->orderid; ?></li>
            <li>优惠券码：<?= $order->systemCoupon->code; ?></li>
        </ul>
    </div>

    <div class="operating">
        <a class="button1" href="<?= \DL\service\UrlService::build('index/index'); ?>">返回首页</a>
        <a class="button2" href="<?= \DL\service\UrlService::build(['member/my-reserve-detail', 'orderid' => $order->orderid]); ?>">查看订单</a>
    </div>
</div> 
