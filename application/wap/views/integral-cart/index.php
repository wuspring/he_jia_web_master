<?php

use DL\service\UrlService;

?>
<script>
    var editStatus = false;
</script>
<body>
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <a href="javascript:void(0)" class="a_right orange">编辑</a>
    <h2>购物车</h2>
</div>
<div class="mycart_list">
    <?php foreach ($carts AS $cart) : ?>
    <div class="mycart_list_con">
        <a href="javascript:void(0)" class="a_select <?= $cart->ispay ? 'active' : ''; ?>"  data-id="checks" data-val="<?=$cart->id?>"></a>
        <a href="javascript:void(0)" class="a_select"  data-id="ac-checks" data-val="<?=$cart->id?>" style="display: none"></a>
        <a href="<?= UrlService::build(['integral/goods-detail', 'id' => $cart->goodsId]); ?>">
            <img src="<?= $cart->goods_image; ?>" alt="<?= $cart->goodsName; ?>" class="img">
        </a>
        <a href="<?= UrlService::build(['integral/goods-detail', 'id' => $cart->goodsId]); ?>" class="con">
            <p class="p_name ellipsis"><?= $cart->goodsName; ?></p>
            <p class="p_spec">
                <?php foreach (explode(',', $cart->getAttrInfo()) AS $attr) :?>
                    <?= str_replace(':', ' : ', trim($attr)); ?><br>
                <?php endforeach; ?>
            </p>
            <p class="p_price orange"><span><?= $cart->goodsPrice; ?></span>积分</p>
        </a>
        <div class="num">
            <a href="javascript:void(0)" class="a_reduce" data-id="amountCtrl" data-sku="<?= $cart->sku_id;?>" data-info="<?= $cart->goodsId;?>" data-type="reduce"><img src="/public/wap/images/icon_reduce.jpg" alt="减"></a>
            <input type="text" class="inp" name="amount" data-info="<?= $cart->goodsId;?>" data-sku="<?= $cart->sku_id;?>" value="<?= $cart->goodsNum; ?>" data-limit="<?= $cart->amountLimit; ?>">
            <a href="javascript:void(0)" class="a_plus" data-id="amountCtrl" data-info="<?= $cart->goodsId;?>" data-sku="<?= $cart->sku_id;?>" data-type="plus"><img src="/public/wap/images/icon_plus.jpg" alt="加"></a>
        </div>
    </div>
    <?php endforeach; ?>
</div>
<div class="mycart_btn">
    <div class="mycart_btns mycart_btns1" style="display: none;">
        <a href="javascript:void(0)" class="a_select <?= count($carts) == $amount ? 'active' : ''; ?>" data-id="checkAll">全选</a>
        <a href="javascript:void(0)" class="a_select" data-id="ac-checkAll" style="display: none">全选</a>
        <a href="javascript:void(0)" class="btn btn_outline btn_delect">删除</a>
    </div>
    <div class="mycart_btns mycart_btns2">
        <a href="javascript:void(0)" class="a_select <?= count($carts) == $amount ? 'active' : ''; ?>" data-id="checkAll">全选</a>
        <a href="<?= UrlService::build('integral-cart/create-order'); ?>" <?= $amount ? '' : 'disabled'; ?> class="btn <?= $amount ? '' : 'disabled'; ?>">结算</a>
        <p>不含运费<span class="s">合计<span class="orange"><?= $totalPrice; ?>积分</span></span></p>
    </div>
</div>

<script type="text/javascript">
    // 选择单条产品
    $('.mycart_list_con .a_select').bind('click',function(){
        $(this).toggleClass('active');
    });

    $('[data-id="ac-checks"]').click(function () {
        if ($('[data-id="ac-checks"].active').length >= $('[data-id="ac-checks"]').length) {
            $('[data-id="ac-checkAll"]').addClass('active');
        } else {
            $('[data-id="ac-checkAll"]').removeClass('active');
        }
    });
    $('[data-id="ac-checkAll"]').click(function () {
        var that = $(this);
        if (that.hasClass('active')) {
            $('[data-id="ac-checks"]').removeClass('active');
            that.removeClass('active');
        } else {
            $('[data-id="ac-checks"]').addClass('active');
            that.addClass('active');
        }
    });
    //
    $('.btn_delect').click(function () {
        var ids = [];

        $('[data-id="ac-checks"].active').each(function () {
            ids.push($(this).attr('data-val'));
        });

        $.ajax({
            url: '<?= UrlService::build('integral-cart/delete'); ?>',
            type : 'post',
            async : false,
            data:{
                ids : ids.join(',')
            },
            dataType:'json',
            success:function(data) {
                if (data.status) {
                    window.location.reload();
                    return false;
                }
                alert(data.msg);
            }
        });

        console.log(ids);
    });

    // 头部编辑
    $('.head .a_right').bind('click',function(){
        if($(this).hasClass('a_edit')){
            $('[data-id="ac-checks"], [data-id="ac-checkAll"]').hide();
            $('[data-id="checks"], [data-id="checkAll"]').show();
            // 完成
            $(this).html('编辑').removeClass('a_edit');
            $('.mycart_btns1').hide();
            $('.mycart_btns2').show();
        }else{
            $('[data-id="ac-checks"], [data-id="ac-checkAll"]').show();
            $('[data-id="checks"], [data-id="checkAll"]').hide();
            // 编辑
            $(this).html('完成').addClass('a_edit');
            $('.mycart_btns1').show();
            $('.mycart_btns2').hide();
        }
    });
</script>
<script>
    // 全选
    $('a[data-id="checkAll"]').click(function () {
        var status = $(this).hasClass('active');

        $.ajax({
            url: '<?= UrlService::build('integral-cart/check'); ?>',
            type : 'post',
            async : false,
            data:{
                status : status ? 0 : 1,
                ids : [].join(',')
            },
            dataType:'json',
            success:function(data) {
                if (data.status) {
                    window.location.reload();
                    return false;
                }
                alert(data.msg);
            }
        })
    });

    // 单选
    $('a[data-id="checks"]').click(function () {
        var status = $(this).hasClass('active'),
            id = $(this).attr('data-val');

        $.ajax({
            url: '<?= UrlService::build('integral-cart/check'); ?>',
            type : 'post',
            async : false,
            data:{
                status : status ? 1 : 0,
                ids : [id].join(',')
            },
            dataType:'json',
            success:function(data) {
                if (data.status) {
                    window.location.reload();
                    return false;
                }
                alert(data.msg);
            }
        })
    });

    // 增减控件
    $('a[data-id="amountCtrl"]').click(function () {
        var type = $(this).attr('data-type'),
            id = $(this).attr('data-info'),
            skuId = $(this).attr('data-sku'),
            amountDom = $('input[name="amount"][data-info="' + id + '"][data-sku="' + skuId + '"]');

        switch (type) {
            case 'plus' :
                var val = parseInt(amountDom.val()) + 1;
                break;
            case 'reduce' :
                var val = parseInt(amountDom.val()) - 1;
                break;
        }

        amountDom.val(val).change();
    });

    // 数量输入控件
    $('input[name="amount"]').change(function () {
        var val = $(this).val(),
            id = $(this).attr('data-info'),
            skuId = $(this).attr('data-sku'),
            limit = $(this).attr('data-limit');
        if (!/^\d+$/.test(val)) {
            val = 0;
        }
        val = parseInt(val);

        if (val < 0 ) {
            alert("请输入有效的商品数量");
            val = 0;
        }
        if (val > parseInt(limit)) {
            alert("小二正在努力补货中...");
            val = limit;
        }

        $.ajax({
            url: '<?= UrlService::build('integral-cart/add'); ?>',
            type : 'get',
            async : false,
            data:{
                id : id,
                amount : val,
                skuId : skuId
            },
            dataType:'json',
            success:function(data) {
                if (data.status) {
                    window.location.reload();
                    return false;
                }
                $(this).val(val);
                alert(data.msg);
            }
        })
    });

    $('a[data-id="delete"]').click(function () {
        removeCart($(this));
    });
</script>