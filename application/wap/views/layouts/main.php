<?php

use wap\models\SelectCity;
use yii\helpers\ArrayHelper;
use wap\models\Provinces;
use common\assets\AppAsset;
use \DL\vendor\ConfigService;
use wap\models\OrderGoods;
use wap\models\Order;
use yii\helpers\Url;

global $currentCity;

// select group citys
$selectCitys = SelectCity::find()->where([])->all();
$cityIds = ArrayHelper::getColumn($selectCitys, 'provinces_id');
$cityIds or $cityIds = [0];
$groupCitys = Provinces::find()->where([
    'and',
    ['=', 'level', 2],
    ['in', 'id', $cityIds]
])->all();
$groupCityDatas = [
    'cityIds' => [],
    'cityNames' => []
];
foreach ($groupCitys AS $groupCity) {
    $groupCityDatas['cityIds'][] = $groupCity->id;
    $groupCityDatas['cityNames'][] = $groupCity->cname;
}

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
global $title, $currentCity;

isset($title) or $title = ConfigService::init('system')->get('name');
isset($keywords) or $keywords = ConfigService::init('system')->get('keywords');
isset($description) or $description = ConfigService::init('system')->get('description');

?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0;" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $title; ?></title>
    <meta name="keywords" content="<?= $keywords; ?>">
    <meta name="description" content="<?= $description; ?>">
    <link rel="icon" href="/public/wap/images/favicon.ico" mce href="favicon.ico" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="/public/wap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/public/wap/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="/public/wap/css/swiper.min.css"/>
    <link rel="stylesheet" type="text/css" href="/public/wap/css/mobileSelect.css"/>
    <link rel="stylesheet" type="text/css" href="/public/wap/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/public/wap/css/user.css">

    <script type="text/javascript" src="/public/wap/js/jquery.min.js"></script>
    <script type="text/javascript" src="/public/wap/js/popper.min.js"></script>
    <script type="text/javascript" src="/public/wap/js/bootstrap.js"></script>
    <script type="text/javascript" src="/public/wap/js/loading.js"></script>
    <script type="text/javascript" src="/public/wap/js/base.js"></script>
    <script type="text/javascript" src="/public/wap/js/mobileSelect.min.js"></script>

    <!-- tab左右滑动js -->
    <script type="text/javascript" src="/public/wap/js/swiper.min.js"></script>

    <script>
        var _csrf = "<?php echo Yii::$app->getRequest()->getCsrfToken();?>",
            <?php if (Yii::$app->user->isGuest): ?>
            userInfo = [],
            hasCouponGoods = [], //预约商品
            hasCouponIds = [], //领取优惠券
            hasStoreIds = []; //收藏店铺
        <?php else :
        $userInfo = Yii::$app->user->identity;
        $info = [
            'id' => $userInfo->id,
            'username' => $userInfo->username,
            'nickname' => $userInfo->nickname,
            'avatar' => $userInfo->avatar,
            'birthday' => date('Y-m-d', strtotime($userInfo->birthday))
        ];

        $hasOrderGoodsIds = [];

        $orders = Order::find()->where([
            'and',
            ['=', 'buyer_id', $userInfo->id],
            ['>', 'add_time', date('Y-m-d', strtotime('-30 days'))],
            ['=', 'type', Order::TYPE_ZHAN_HUI],
            [
                'or',
                [
                    'and',
//                                ['in', 'order_state', [Order::STATUS_WAIT, Order::STATUS_PAY]],
                    ['=', 'ticket_type', Order::TICKET_TYPE_COUPON]
                ],
                [
                    'and',
                    ['not in', 'order_state', [Order::STATUS_WAIT]],
                    ['=', 'ticket_type', Order::TICKET_TYPE_ORDER]
                ]
            ]
        ])->all();

        if ($orders) {
            $orderIds = arrayGroupsAction($orders, function ($order) {
                return $order->orderid;
            });

            $hasOrders = OrderGoods::find()->where([
                'in', 'order_id', $orderIds
            ])->all();

            $hasOrderGoodsIds = arrayGroupsAction($hasOrders, function ($hasOrder) {
                return $hasOrder->goods_id;
            });
        }

        $memberStoreCoupons = \wap\models\MemberStoreCoupon::findAll([
            'member_id' => $userInfo->id
        ]);
        $couponIds = arrayGroupsAction($memberStoreCoupons, function ($memberStoreCoupon) {
            return $memberStoreCoupon->coupon_id;
        });

        $dTickets = \DL\Project\CityTicket::init($currentCity->id)->currentFactionTickets();
        $dTicketIds = $dTickets ? array_keys($dTickets) : [0];

        $dLogs = \wap\models\MemberTicketStore::find()->where([
            'and',
            ['=', 'member_id', $userInfo->id],
            ['in', 'ticket_id', $dTicketIds]
        ])->all();

        $hasStoreIds = $dLogs ? arrayGroupsAction($dLogs, function ($dLog) {
            return $dLog->store_id;
        }) : [];
        ?>
        userInfo = <?= json_encode($info); ?>,
            hasCouponGoods = <?= json_encode($hasOrderGoodsIds); ?>,
            hasCouponIds = <?= json_encode($couponIds); ?>,
            hasStoreIds = <?= json_encode($hasStoreIds); ?>;
        <?php endif; ?>

        var GET_MSG_CODE = '<?= DL_DEBUG ? \DL\service\UrlService::build('api/get-code', 'index.php') : '/api/get-code' ; ?>',
            GET_ORDER_STORE = '<?= \DL\service\UrlService::build('api/order-store'); ?>',
            GET_GOOD_PRICE = '<?= \DL\service\UrlService::build('api/order-goods-price'); ?>',
            GET_STORE_COUPONS = '<?= \DL\service\UrlService::build('api/get-coupon'); ?>',
            GET_ORDER_GOODS = '<?= \DL\service\UrlService::build('api/get-order-goods'); ?>';
    </script>
</head>

<body>
<?php $this->beginBody() ?>
<?= $content; ?>

<script type="text/javascript">

    var cityIds = <?= json_encode($groupCityDatas['cityIds']); ?>,
        cityNames = <?= json_encode($groupCityDatas['cityNames']); ?>,
        cityIndex = <?= (int)array_search($currentCity->id, $groupCityDatas['cityIds']); ?>;

    //城市选择
    var mobileSelect1 = new MobileSelect({
        trigger: '#select_city',
        title: '选择城市',
        wheels: [
            {data: cityNames}
        ],
        position: [cityIndex], //初始化定位
        callback: function (indexArr, data) {
            $.ajax({
                url: '<?=Url::toRoute(['index/set-city'])?>',
                data: {pr_id: cityIds[indexArr]},
                success: function (res) {
                    res = JSON.parse(res);
                    if (res.status) {
                        window.location.href =res.data; // '/wap.php';
                        // window.location.reload();
                        return false;
                    }
                    alert(res.msg);
                },
            });
        }
    });

    $(function () {
        refreshButton();
    });
</script>
<div class="modal modal_form fade" id="globalLoginShadow"></div>
<div class="modal modal_success fade" id="globalSuccessShadow"></div>
<div class="modal modal_couponsuc fade" id="globalCouponSuccessShadow"></div>
<script type="text/javascript" src="/public/wap/js/base_bind.js"></script>
<div style="display: none;">
    <?php echo Url::toRoute(['index/index']);?>
</div>
<?php $this->endBody()?>
</body>
</html>
<?php $this->endPage() ?>

