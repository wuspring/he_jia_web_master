<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/6
 * Time: 17:39
 */
$this->title = '地址列表';
$this->params['breadcrumbs'][] = $this->title;

use  yii\helpers\Url;
?>
<div class="head">
    <a href="<?= Url::toRoute(['member/member-edit']) ?>"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>收货地址</h2>
</div>
<!--- 收货地址 -->
<div id="receipt_address_list">
    <div class="cont">
        <?php foreach ($addressList as $k => $v): ?>
            <div class="box"  <?= isset($get['number']) ? ('data-id="setAddress" data-val="' . $v->id . '" data-order="' . $get['number']. '"') : ''; ?> >
                <div class="user_info">
                    <a href="#">
                        <h3><?= $v->name ?><span><?= $v->mobile ?></span></h3>
                        <p><?= $v->getProvince($v->province) ?> <?= $v->getProvince($v->city) ?> <?= $v->getProvince($v->region) ?> <?= $v->address ?></p>
                    </a>
                </div>
                <?php if (!isset($get['number'])) :?>
                <div class="operating">
                    <?php if ($v->isDefault == 1): ?>
                        <a class="radio clickSetDefault" tabDefault="on" setAddressId="<?=$v->id?>" href="javascript:void(0)">
                            <img src="/public/wap/images/radio_ico1.png"/> 设置为默认地址
                        </a>
                    <?php else: ?>
                        <a class="radio clickSetDefault" tabDefault="off" setAddressId="<?=$v->id?>" href="javascript:void(0)">
                            <img src="/public/wap/images/radio_ico2.png"/>  设置为默认地址
                        </a>
                    <?php endif; ?>
                    <a class="button" href="<?= Url::toRoute(['address/edit-address', 'id' => $v->id]) ?>">编辑</a>
                    <a class="button deleteAddress" addressId="<?= $v->id ?>" href="javascript:void(0)">删除</a>
                    <div class="clear"></div>
                </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>

    </div>
    <div class="tc_nav">
        <a href="<?php $p = array_merge(['address/add-address'], $get); echo Url::toRoute($p) ?>" class="color_red">增加新地址</a>
    </div>
</div>

<!-- 模态框- 文本提醒-简易版 弹框 -->
<div class="modal modal_explain modal_explain_sm fade" id="myModalExplain">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">信息</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align"></p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">确 定</button>
            </div>
        </div>
    </div>
</div>

<script>
    //删除地址
    $(".deleteAddress").on('click', function () {
        var addressId = $(this).attr('addressId');
        $.post("<?php echo Url::toRoute(['address/delete-address']);?>", {addressId: addressId}, function (res) {
            $("#myModalExplain").modal('show');
            $(".text-align").text(res.msg);
            if (res.code == 1) {
                $("#myModalExplain").find('.btn').addClass('clickReload');
            }
        }, 'json')

    });

    $(document).on('click', '.clickReload', function () {
        window.location.reload();
    });

    var rebuildQueryString = function (path, query) {
        var param = [], splitKey;
        for (var i in query) {
            param.push(i + '=' + query[i]);
        }

        splitKey = path.indexOf('?') > -1 ? '&' : '?';
        return path + splitKey + param.join('&');
    };

    $('[data-id="setAddress"]').click(function () {
        var that = $(this),
            data = {};

        data.number = that.attr('data-order');
        data.setAddress = that.attr('data-val');

        $.post('<?= \DL\service\UrlService::build('integral-order/update'); ?>', data, function (res) {
            res = JSON.parse(res);

            if (res.status) {
                window.location.href = rebuildQueryString('<?= \DL\service\UrlService::build('integral-order/confirm'); ?>',
                    {number : data.number});
                return false;
            }

            alert (res.msg);
        })
    });

    //设置默认地址
    $(document).on('click','.clickSetDefault',function () {

        var status=$(this).attr('tabDefault');
        var setAddressId=$(this).attr('setAddressId');
        if (status == 'off'){
            $.post("<?php echo Url::toRoute(['address/set-default']);?>", {setAddressId: setAddressId}, function (res) {
                $("#myModalExplain").modal('show');
                $(".text-align").text(res.msg);
                if (res.code == 1) {
                    $("#myModalExplain").find('.btn').addClass('clickReload');
                }
            }, 'json')
        }
    })

</script>
