<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/6
 * Time: 17:56
 */

use yii\bootstrap\ActiveForm;
use  yii\helpers\Url;
use wap\models\Provinces;

?>

<!-- honeySwitch 开关 -->
<link rel="stylesheet" href="/public/wap/css/honeySwitch.css">
<script src="/public/wap/js/honeySwitch.js"></script>
<style>
    .tijiao {
        font-size: 18px;
        line-height: 50px;
        text-align: center;
        display: block;
        background: #fc7203;
        color: white;
        width: 100%;
        border: none;
    }

    .help-block-error {
        display: none;
    }
    input{
        border: none;
        width: 100%;
    }
</style>

<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>

    <h2>收货地址</h2>
</div>
<!--- 用户编辑 -->

<?php
$request = ['address/add-address'];
isset($get['number']) and $request = array_merge($request, ['number' => $get['number']]);

$form = ActiveForm::begin([
    'id' => 'memberaddress',
    'action' => Url::toRoute($request),
    'fieldConfig' => [
        'options' => ['class' => ''],
        'inputOptions' => ['class' => 'name_zjxs'],
        'template' => "{input}\n{hint}\n{error}",
        'horizontalCssClasses' => [
            'label' => '',
            'offset' => '',
            'wrapper' => '',
            'error' => '',
            'hint' => '',
        ],
    ],

]); ?>
<div id="edit_address">
    <div class="box">
        <ul>
            <li>
                <div class="con">
                    <label>收货人</label>
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => '请输入收货人真实姓名']) ?>
                    <div class="clear"></div>
                </div>
            </li>
            <li>
                <div class="con">
                    <label>手机号</label>
                    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true, 'placeholder' => '请输入收货人手机号']) ?>
                    <div class="clear"></div>
                </div>
            </li>
            <li>
                <a href="javascript:void(0)">
                    所在地区
                    <img class="jt" src="/public/wap/images/jt_ico.png"/>
                    <span id="trigger_area">请选择</span>
                    <div class="clear"></div>
                </a>
            </li>
            <li>
                <a href="javascript:void(0)">
                    <?= $form->field($model, 'address')->textInput(['maxlength' => true, 'placeholder' => "详细地址"]) ?>
                    <div class="clear"></div>
                </a>
            </li>
        </ul>
    </div>
    <div style="display: none">
        <?= $form->field($model, 'isDefault')->textInput(['maxlength' => true,'value'=>0]) ?>
        <?= $form->field($model, 'province')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="box">
        <ul>
            <li>
                设为默认地址
                <div class="common-row">
                    <div class="cell-left"></div>
                    <div class="cell-right"><span class="switch-off" id="bluetooth" themeColor="#fc7203"></span></div>
                </div>
                <div class="clear"></div>
            </li>

        </ul>
    </div>
    <div class="quit">
        <button class="tijiao" type="submit">保 存</button>
    </div>

</div>
<?php ActiveForm::end(); ?>

<!-- 模态框- 文本提醒-简易版 弹框 -->
<div class="modal modal_explain modal_explain_sm fade" id="myModalExplain">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">信息</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align"></p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">确 定</button>
            </div>
        </div>
    </div>
</div>

<script>
    var $form = $('#memberaddress');
    $form.on('beforeSubmit', function () {
        var data = $form.serialize();
        $.ajax({
            url: $form.attr('action'),
            type: 'POST',
            data: data,
            dataType: "json",
            success: function (data) {
                // 执行成功
                $(".text-align").children().remove();
                $(".text-align").text('');
                var text = '';
                $.each(data, function (key, value) {
                    text += '<li>' + value + '</li>';
                });
                $("#myModalExplain").modal('show');
                $(".text-align").append(text);
            },
            // error: function(jqXHR, errMsg) {
            //     alert("网络不佳");
            //     alert(errMsg)
            // }
        });
        return false; // 防止默认提交
    });

    $('#memberaddress').on('afterValidate', function (event, messages, errorAttributes) {

        if (errorAttributes.length > 0) {
            var info = messages[errorAttributes[0].id][0];
            $("#myModalExplain").modal('show');
            $(".text-align").text(info);
        }
    });

    //点击默认地址
    $("#bluetooth").on('click', function () {
        var classAttr = $(this).attr('class');
        if (classAttr == 'switch-off') {
            $("#address-isdefault").val(1);
        } else {
            $("#address-isdefault").val(0);
        }
    });

    $(function () {
        $.post("<?php echo Url::toRoute(['provinces/index']);?>", {}, function (res) {
            // mobileSelect2.updateWheels(res);
            //省市县选择
            var mobileSelect2 = new MobileSelect({
                trigger: '#trigger_area',
                title: '选择省市县',
                wheels: [
                    {data: res}
                ],

                position: [0, 0],
                callback: function (indexArr, data) {
                    console.log(data);      //返回选中的json数据
                    console.log(data[0]['id']);

                    $("#address-province").val(data[0]['id']);
                    $("#address-city").val(data[0]['id']);
                    $("#address-region").val(data[0]['id']);

                    // save('diqu',JSON.stringify(data));
                }
            });
        }, 'json')
    });
</script>
