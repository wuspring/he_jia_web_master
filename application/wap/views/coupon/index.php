<?php

?>
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>优惠劵</h2>
</div>
<div class="topNav swiper-container swiper-container-horizontal swiper-container-free-mode">
    <div class="swiper-wrapper com_tab" style="transition-duration: 300ms;transform: translate3d(0px, 0px, 0px);">
        <a class="swiper-slide active" data-id="cate" data-cate="0"><span>精品推荐</span></a>
        <?php if ($categorys): foreach ($categorys AS $category):?>
        <a class="swiper-slide" data-id="cate" data-cate="<?= $category->id; ?>"><span><?= $category->name; ?></span></a>
        <?php endforeach; endif; ?>
    </div>
</div>
<div>
    <div class="com_coupon" data-id="cateCon" data-cate="0" data-p="1" data-ticket="<?= $ticketId; ?>">
        <?php foreach ($recommendCoupons AS $recommendCoupon) : $user=\common\models\User::findOne($recommendCoupon->user_id); ?>
        <div class="com_coupon_con">
            <a href="javascript:void(0)" class="float-right a" data-id="store-coupon" data-coupon="<?= $recommendCoupon->id; ?>" data-ticket="<?= $ticketId; ?>">立即<br/>领取</a>

            <img src="<?= $user ? $user->avatar : ''; ?>" alt="<?= $user ? $user->nickname : ''; ?>" class="img">
            <div class="con">
                <p class="p1 orange">￥<span><?=intval($recommendCoupon->money)?></span>优惠劵</p>
                <p class="p2 text-truncate"><?=$recommendCoupon->describe?></p>
<!--                <p class="p3 text-truncate">(有效期:2019.05.01-05.03)</p>-->
            </div>
        </div>
        <?php endforeach; ?>
    </div>
    <?php if ($categorys): foreach ($categorys AS $category):?>
    <div class="com_coupon" data-id="cateCon" data-cate="<?= $category->id; ?>" data-p="0"  data-ticket="<?= $ticketId; ?>" style="display: none"></div>
    <?php endforeach; endif; ?>
</div>

<!-- tab左右滑动 -->
<script type="text/javascript">
    var mySwiper = new Swiper('.topNav', {
        freeMode: true,
        preventClicks : false,//默认true
        freeModeMomentumRatio: 0.5,
        slidesPerView: 'auto',
    });

    $('[data-id="cate"]').click(function () {
        var that = $(this),
            cate = that.attr('data-cate'),
            activeDom = $('[data-id="cateCon"][data-cate="' + cate + '"]');
            that.addClass('active').siblings().removeClass('active');

            loadLists(activeDom);
            activeDom.show().siblings().hide();
    });

    var loadListsStatus = true;
    var loadLists = function (dom) {
        if (!loadListsStatus) {
            return false;
        }

        var data ={};
        data.cid = dom.attr('data-cate');
        data.p = dom.attr('data-p');
        data.ticketId = dom.attr('data-ticket');

        loadListsStatus = false;
        $.get('<?= \DL\service\UrlService::build('coupon/api-list'); ?>', data, function (res) {
            loadListsStatus = true;

            res = JSON.parse(res);
            if (res.status) {
                var rdata = res.data.lists;

                if (rdata.length) {
                    for (var i=0; i<rdata.length; i++) {
                        var li = $('<div>').addClass('com_coupon_con ' + (rdata[i].has ? 'ed' : '')).append(
                            $('<a>').attr({
                                'href' : 'javascript:void(0)',
                                'data-id' : 'store-coupon',
                                'data-coupon' : rdata[i].id,
                                'data-ticket' : res.data.ticketId
                            }).addClass('float-right a').html((rdata[i].has ? '已领取' : "立即<br/>领取")),
                            $('<img>').attr({
                                'src' : rdata[i].store.avatar,
                                'alt' : rdata[i].store.nickname
                            }).addClass('img'),
                            $('<div>').addClass('con').append(
                                $('<p>').addClass('p1 orange').append(
                                    '￥', $('<span>').text(parseInt(rdata[i].money)), '优惠劵'
                                ),
                                $('<p>').addClass('p2 text-truncate').text(rdata[i].describe)
                            )
                        );

                        dom.append(li);
                    }

                    dom.attr('data-p', parseInt(data.p)+1);
                }

                return false;
            }
            alert (res.msg);
        })
    };

    $(window).scroll(function(){
        var dom = $('[data-id="cateCon"]').not(":hidden");
        loadLists(dom);
    });
</script>
