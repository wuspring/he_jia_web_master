<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>

<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>优惠劵使用规则</h2>
</div>
<div class="coupondetail_con">
    <div class="coupondetail_shop clearfix">
        <img src="<?=$shop->avatar?>" alt="<?=$shop->nickname?>" class="img">
        <p class="p1 text-truncate"><?=$shop->nickname?></p>
        <p class="p4 orange">共<span><?=$shop->store_amount?></span>家体验店</p>
        <a href="<?=Url::to(['shop/score','storeId'=>$shop->id])?>" class="a_assess">
            <?php for($i=1;$i<=5;$i++){ ?>
                <em class="<?=($i<=$shop->score)?'active':''?>"></em>
            <?php } ?>
            <span><?=$shop->judge_amount?>条评论</span>
        </a>
        <?php if(!empty($shop->zw_post)):?>
        <p class="p2">展位号：<span><?=$shop->zw_pos?></span></p>
        <?php endif;?>
    </div>
    <div class="coupondetail_more">
        <p>优惠劵使用说明：</p>
        <div class="con">
            <?= \DL\vendor\ConfigService::init('system')->get('coupon_desc');?>
        </div>
    </div>
</div>

<div class="coupondetail_list">
    <div class="com_title">
        <div class="com_title_con">
            <a href="<?=Url::to(['coupon/index'])?>">更多店铺优惠劵></a>
            <span class="s_title">店铺优惠劵</span>
        </div>
    </div>
    <div class="com_coupon">
        <!-- 点击整条，弹出领取弹框  已经领取 的加ed  点击无效 -->
        <?php if(!empty($shop->coupons)):?>
            <?php foreach ($shop->coupons as $key=>$item):?>
                <div class="com_coupon_con">
                    <a href="javascript:void(0)" class="float-right a">立即领取</a>
                    <img src="<?=$shop->avatar?>" alt="<?=$shop->nickname?>" class="img">
                    <div class="con">
                        <p class="p1 orange">￥<span><?=$item->money?></span>优惠劵</p>
                        <p class="p2 text-truncate">支付满<?=$item->condition?>元可用</p>
                        <p class="p3 text-truncate">(有效期:2019.05.01-05.03)</p>
                    </div>
                </div>
            <?php endforeach;?>
        <?php endif;?>
    </div>
</div>