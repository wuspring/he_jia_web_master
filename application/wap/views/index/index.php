<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/4
 * Time: 10:04
 */

use  yii\helpers\Url;

?>
<div class="header">
    <div class="header_city float-right" id="select_city"><?= $defaultCity->cname ?></div>
    <a href="<?= Yii::$app->request->hostInfo ?>/wap.php" class="header_logo">
        <img src="<?= \DL\vendor\ConfigService::init('system')->get('waplogo') ?>" alt="<?= \DL\vendor\ConfigService::init('system')->get('name') ?>">
    </a>
</div>
<div class="slider carousel slide" data-ride="carousel">
    <!-- 指示符 -->
    <?php if (!empty($banners) && count($banners) > 1): ?>
        <ul class="carousel-indicators">
            <?php foreach ($banners as $key => $item): ?>
                <li data-target="#slider" data-slide-to="<?= $key ?>" class="<?= $key == 0 ? 'active' : '' ?>"></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <!-- 轮播图片 -->
    <div class="carousel-inner">
        <?php if (!empty($banners)): ?>
            <?php foreach ($banners as $key => $item): ?>
                <a href="<?= $item['url'] ?>" class="carousel-item <?= $key == 0 ? 'active' : '' ?>">
                    <img src="<?= $item['picture'] ?>">
                </a>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <!-- 左右翻页不需要但是删除会报错 -->
    <a id="carleft" class="left carousel-control" href="#slider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a id="carright" class="right carousel-control" href="#slider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<?php if (!empty($magics)): ?>
<!-- 轮播图下魔方 需要1-8张图片的各个排布-->
<div class="index_ad">
    <?php if (count($magics) == 1): ?>
        <!-- 1张 -->
        <div class="com_ad">
            <?php for ($i = 0; $i < count($magics); $i++){ ?>
                <a href="<?= $magics[$i]['url'] ?>">
                    <img src="/public/wap/images/loading.jpg" class="loading_img"
                         data-url="<?= $magics[$i]['picture'] ?>" alt="<?= $magics[$i]['title'] ?>">
                </a>
            <?php } ?>
        </div>
    <?php elseif (count($magics) == 2): ?>
        <!-- 2张 -->
        <div class="index_ad_2">
            <?php for ($i = 0; $i < count($magics); $i++){ ?>
                <a href="<?= $magics[$i]['url'] ?>">
                    <img src="/public/wap/images/loading.jpg" class="loading_img"
                         data-url="<?= $magics[$i]['picture'] ?>" alt="<?= $magics[$i]['title'] ?>">
                </a>
            <?php } ?>
        </div>
    <?php elseif (count($magics) == 3): ?>
        <!-- 3张 -->
        <div class="index_ad_3">
            <?php for ($i = 0; $i < count($magics); $i++){ ?>
                <a href="<?= $magics[$i]['url'] ?>">
                    <img src="/public/wap/images/loading.jpg" class="loading_img"
                         data-url="<?= $magics[$i]['picture'] ?>" alt="<?= $magics[$i]['title'] ?>">
                </a>
            <?php } ?>
        </div>
    <?php elseif (count($magics) == 4): ?>
        <!-- 4张 -->
        <div class="index_ad_4">
            <?php for ($i = 0; $i < count($magics); $i++){ ?>
                <a href="<?= $magics[$i]['url'] ?>">
                    <img src="/public/wap/images/loading.jpg" class="loading_img"
                         data-url="<?= $magics[$i]['picture'] ?>" alt="<?= $magics[$i]['title'] ?>">
                </a>
            <?php } ?>
        </div>
    <?php elseif (count($magics) == 5): ?>
        <!-- 5张 -->
        <div class="index_ad_5">
            <div class="index_ad_2 pb-0">
                <?php for ($i = 0; $i < 2; $i++){ ?>
                    <a href="<?= $magics[$i]['url'] ?>">
                        <img src="/public/wap/images/loading.jpg" class="loading_img"
                             data-url="<?= $magics[$i]['picture'] ?>" alt="<?= $magics[$i]['title'] ?>">
                    </a>
                <?php } ?>
            </div>
            <div class="index_ad_3 pt-0">
                <?php for ($i = 2; $i < count($magics); $i++){ ?>
                    <a href="<?= $magics[$i]['url'] ?>">
                        <img src="/public/wap/images/loading.jpg" class="loading_img"
                             data-url="<?= $magics[$i]['picture'] ?>" alt="<?= $magics[$i]['title'] ?>">
                    </a>
                <?php } ?>
            </div>
        </div>
    <?php elseif (count($magics) == 6): ?>
        <!-- 6张 -->
        <div class="index_ad_6">
            <div class="index_ad_2 pb-0">
                <?php for ($i = 0; $i < 2; $i++){ ?>
                    <a href="<?= $magics[$i]['url'] ?>">
                        <img src="/public/wap/images/loading.jpg" class="loading_img"
                             data-url="<?= $magics[$i]['picture'] ?>" alt="<?= $magics[$i]['title'] ?>">
                    </a>
                <?php } ?>
            </div>
            <div class="index_ad_4 pt-0">
                <?php for ($i = 2; $i < count($magics); $i++){ ?>
                    <a href="<?= $magics[$i]['url'] ?>">
                        <img src="/public/wap/images/loading.jpg" class="loading_img"
                             data-url="<?= $magics[$i]['picture'] ?>" alt="<?= $magics[$i]['title'] ?>">
                    </a>
                <?php } ?>
            </div>
        </div>
    <?php elseif (count($magics) == 7): ?>
        <!-- 7张 -->
        <div class="index_ad_7">
            <div class="index_ad_3 pb-0">
                <?php for ($i = 0; $i < 3; $i++){ ?>
                    <a href="<?= $magics[$i]['url'] ?>">
                        <img src="/public/wap/images/loading.jpg" class="loading_img"
                             data-url="<?= $magics[$i]['picture'] ?>" alt="<?= $magics[$i]['title'] ?>">
                    </a>
                <?php } ?>
            </div>
            <div class="index_ad_4 pt-0">
                <?php for ($i = 3; $i < count($magics); $i++){ ?>
                    <a href="<?= $magics[$i]['url'] ?>">
                        <img src="/public/wap/images/loading.jpg" class="loading_img"
                             data-url="<?= $magics[$i]['picture'] ?>" alt="<?= $magics[$i]['title'] ?>">
                    </a>
                <?php } ?>
            </div>
        </div>
    <?php else: ?>
        <!-- 8张 -->
        <div class="index_ad_8">
            <div class="index_ad_4 pb-0">
                <?php for ($i = 0; $i < 4; $i++){ ?>
                    <a href="<?= $magics[$i]['url'] ?>">
                        <img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?= $magics[$i]['picture'] ?>" alt="<?= $magics[$i]['title'] ?>">
                    </a>
                <?php } ?>
            </div>
            <div class="index_ad_4 pt-0">
                <?php for ($i = 4; $i < count($magics); $i++){ ?>
                    <a href="<?= $magics[$i]['url'] ?>">
                        <img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?= $magics[$i]['picture'] ?>" alt="<?= $magics[$i]['title'] ?>">
                    </a>
                <?php } ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<?php endif; ?>

<!-- 预存享特价/爆款预约 上 广告图 -->
<?php if(!empty($hot_ads)):?>
    <div class="com_ad">
        <?php for ($i = 0; $i < count($hot_ads);$i++){ ?>
            <a href="<?= $hot_ads[$i]['url'] ?>">
                <img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?= $hot_ads[$i]['picture'] ?>" alt="<?= $hot_ads[$i]['title'] ?>">
            </a>
        <?php } ?>
    </div>
<?php endif; ?>
<?php if(!empty($applyGoods)||!empty($reserGoods)):?>
<!-- 预存享特价/爆款预约 -->
<div class="index_prestore">
    <ul class="nav nav-pills nav-justified index_prestore_tab" role="tablist">
        <li class="nav-item">
            <a href="#prestore_tab1" data-toggle="tab" class="nav-link active">
                <img src="/public/wap/images/icon_tab_prestore.png" alt="预存享特价"><span>预存享特价</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#prestore_tab2" data-toggle="tab" class="nav-link">
                <img src="/public/wap/images/icon_tab_boutique.png" alt="爆款预约"><span>爆款预约</span>
            </a>
        </li>
    </ul>
    <div class="tab-content index_prestore_list">
        <!-- 预存享特价 -->
        <div class="tab-pane active" id="prestore_tab1">
            <ul class="list-unstyled clearfix com_productlist com_productlistone">
                <?php if(!empty($applyGoods)):?>
                <?php foreach ($applyGoods as $key=>$good):?>
                <li>
                    <a href="<?= Url::to(['goods/detail','id' => $good->id]); ?>" class="product">
                        <span class="product_img float-left">
                            <img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?=$good->goods_pic?>" alt="<?=$good->goods_name?>">
                        </span>
                        <div class="product_con">
                            <p class="product_name text-truncate"><?=$good->goods_name?></p>
                            <p class="product_price orange">专享价<span><?=$good->goods_price?></span>元/<span><?=$good->unit?></span><span
                                        class="s"><span><?=$good->goods_marketprice?></span>元/<span><?=$good->unit?></span></span></p>
                            <p class="product_tag orange"><img
                                        src="/public/wap/images/icon_time_orange.png">仅剩<span>11</span><span>件</span>
                            </p>
                            <p class="product_handsel">订金：<span class="orange">￥<span><?=$good->ticket_money?></span></span></p>
                        </div>
                    </a>
                    <a href="javascript:void(0)" class="btn float-right" data-id="order-create" data-good="<?=$good->id?>" data-ticket="<?=$good->ticket_id?>">立即预存</a>
                </li>
                <?php endforeach;?>
                <?php endif;?>

            </ul>
            <div class="com_morebtn">
                <a href="<?=Url::to(['goods/lists','type'=>\wap\models\TicketApplyGoods::TYPE_ORDER])?>" class="btn">查看更多<img src="/public/wap/images/icon_links.png" alt="more"></a>
            </div>
        </div>
        <!-- 爆款预约 -->
        <div class="tab-pane" id="prestore_tab2">
            <ul class="list-unstyled clearfix com_productlist">
                <?php if(!empty($reserGoods)):?>
                    <?php foreach ($reserGoods as $key=>$good):?>
                    <li>
                        <a href="<?=Url::to(['goods/detail', 'id' => $good->id])?>" class="product">
                            <span class="product_img">
                                <img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?=$good->goods_pic?>" alt="<?=$good->goods_name?>">
                            </span>
                            <p class="product_name text-truncate"><?=$good->goods_name?></p>
                            <p class="product_tag orange float-right">
                                <img src="/public/wap/images/icon_time_orange.png">仅剩<span><?=$good->goods_storage?></span><span>件</span>
                            </p>
                            <p class="product_logo"><img src="<?=$good->store->avatar?>"></p>
                            <p class="product_price orange">￥<span><?= $good->goods_price; ?></span><span class="s1">/<?=$good->unit?></span></p>
                        </a>
                    </li>
                    <?php endforeach;?>
                <?php endif;?>
            </ul>
            <div class="com_morebtn">
                <a href="<?=Url::to(['goods/lists','type'=>\wap\models\TicketApplyGoods::TYPE_COUPON])?>" class="btn">查看更多<img src="/public/wap/images/icon_links.png" alt="more"></a>
            </div>
        </div>

    </div>
</div>
<?php endif; ?>

<!-- 预存享特价/爆款预约 上 广告图 -->
<?php if(!empty($rmd_top_ads)):?>
    <div class="com_ad">
        <?php for ($i = 0; $i < count($rmd_top_ads);$i++){ ?>
            <a href="<?= $rmd_top_ads[$i]['url'] ?>">
                <img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?= $rmd_top_ads[$i]['picture'] ?>" alt="<?= $rmd_top_ads[$i]['title'] ?>">
            </a>
        <?php } ?>
    </div>
<?php endif; ?>
<!-- 精选推荐 魔方图 -->
<?php if(!empty($rmd_ads)):?>
<div class="index_groom">
    <div class="index_groom_top clearfix">

        <a href="<?=$rmd_ads[0]['url']?>" class="igt_left"><img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?=$rmd_ads[0]['picture']?>" alt=""></a>
        <?php if(count($rmd_ads)>1):?>
        <div class="igt_right">
            <?php for ($i = 1; $i < (count($rmd_ads)>3?3:count($rmd_ads));$i++){ ?>
            <a href="<?= $rmd_ads[$i]['url'] ?>"><img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?= $rmd_ads[$i]['picture'] ?>" alt=""></a>
            <?php } ?>
        </div>
        <?php endif;?>
    </div>
    <?php if(count($rmd_ads)>3):?>
    <div class="com_ad2">
        <?php for ($i = 3; $i < count($rmd_ads);$i++){ ?>
            <a href="<?= $rmd_ads[$i]['url'] ?>"><img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?= $rmd_ads[$i]['picture'] ?>" alt=""></a>
        <?php } ?>
    </div>
    <?php endif;?>
</div>

<?php endif;?>

<!-- 首页楼层 1F s -->
<?php if(!empty($floors)):?>
<?php foreach ($floors as $key=>$fl):?>
    <?php if(!empty($fl['topimg'])):?>
        <div class="com_ad">
            <?php foreach ($fl['topimg'] as $k=>$item):?>
            <a href="<?= $item['url'] ?>"><img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?= $item['imgurl'] ?>" alt=""></a>
            <?php endforeach;?>
        </div>
    <?php endif;?>
<div class="index_floor">
    <div class="com_title">
        <div class="com_title_con">
            <a href="<?=$fl['url']?>">更多></a>
            <span class="s_tag"><?=$key+1;?>F</span>
            <span class="s_title"><?=$fl['name'];?></span>
        </div>
    </div>
    <?php if(!empty($fl['nextimg'])):?>
        <div class="com_ad">
            <?php foreach ($fl['nextimg'] as $k=>$item):?>
                <a href="<?= $item['url'] ?>"><img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?= $item['imgurl'] ?>" alt=""></a>
            <?php endforeach;?>
        </div>
    <?php endif;?>
    <?php if(!empty($fl['goods'])):?>
    <ul class="com_productlist list-unstyled clearfix">
        <?php foreach ($fl['goods'] as $k=>$good):?>
        <li>
            <a href="<?=Url::to(['goods/detail','id'=>$good->id])?>" class="product">
                <span class="product_img"><img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?=$good->goods_pic?>" alt="<?=$good->goods_name?>"></span>
                <p class="product_name text-truncate"><?=$good->goods_name?></p>
                <p class="shop"><img src="/public/wap/images/icon_shop_green.png">共<span><?=$good->shop_count?></span>家体验店</p>
            </a>
        </li>
        <?php endforeach;?>
    </ul>
    <?php endif;?>
</div>
<?php endforeach;?>
<?php endif;?>

<!-- 资讯上广告图 -->
<?php if(!empty($news_top_ads)):?>
    <div class="com_ad">
        <?php for ($i = 0; $i < count($news_top_ads);$i++){ ?>
            <a href="<?= $news_top_ads[$i]['url'] ?>">
                <img src="/public/wap/images/loading.jpg" class="loading_img" data-url="<?= $news_top_ads[$i]['picture'] ?>" alt="<?= $news_top_ads[$i]['title'] ?>">
            </a>
        <?php } ?>
    </div>
<?php endif; ?>

<!-- 和家资讯 -->
<div class="index_news">
    <div class="com_title">
        <div class="com_title_con">
            <a href="<?=Url::to(['news/news-list'])?>">更多></a>
            <span class="s_title">和家资讯</span>
        </div>
    </div>
    <div class="index_news_list">
        <?php if(!empty($news)):?>
            <?php foreach ($news as $key=>$item):?>
            <a href="<?=Url::to(['news/news-detail','id'=>$item->id])?>" class="inl_con">
                <img src="/public/wap/images/loading.jpg" class="img loading_img" data-url="<?=$item->themeImg?$item->themeImg:'/public/wap/images/loading.jpg'?>">
                <div class="con">
                    <p class="p_name ellipsis"><?=$item->title?></p>
                    <p class="p_time"><span class="s1">会展资讯</span><span class="s2"><?= date('Y-m-d',strtotime($item->createTime))?></span></p>
                </div>
            </a>
            <?php endforeach;?>
        <?php endif;?>
    </div>
</div>

<!-- 底部承诺 -->
<?php if(!empty($serviceList)):?>
<div class="promise">
    <ul class="promise_ul list-unstyled clearfix">
        <?php foreach ($serviceList as $key=>$item):?>
            <li>
                <img src="<?=$item->icon?>">
                <p class="p1 green"><?=$item->name?></p>
                <p class="p2"><?=$item->desc?></p>
            </li>
        <?php endforeach;?>
    </ul>
</div>
<?php endif;?>
<?php $nowUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];?>
<!-- 底部菜单 -->
<!-- 菜单是5个，需要加menu_5.四个则不加 -->
<?php if(!empty($home_menus)):?>
<div class="footer_menu <?=count($home_menus)==5?'menu_5':''?>">
    <ul class="list-unstyled">
        <?php foreach ($home_menus as $key=>$item):?>
        <li class="<?=(strpos($nowUrl,$item->url)!==false)?'active':''?>">
            <a href="<?=$item->url?>" >
                <img src="<?=$item->icon?>" class="img">
                <img src="<?=$item->icon_selected?>" class="img_h">
                <span><?=$item->name?></span>
            </a>
        </li>
        <?php endforeach;?>
    </ul>
</div>
<?php endif;?>

<!-- 轮播图-手指滑动效果 -->
<script type="text/javascript">
    $(function () {
        // 获取手指在轮播图元素上的一个滑动方向（左右）
        // 获取界面上轮播图容器
        var $carousels = $('.carousel');
        var startX, endX;
        // 在滑动的一定范围内，才切换图片
        var offset = 50;
        // 注册滑动事件
        $carousels.on('touchstart', function (e) {
            // 手指触摸开始时记录一下手指所在的坐标x
            startX = e.originalEvent.touches[0].clientX;

        });
        $carousels.on('touchmove', function (e) {
            // 目的是：记录手指离开屏幕一瞬间的位置 ，用move事件重复赋值
            endX = e.originalEvent.touches[0].clientX;
        });
        $carousels.on('touchend', function (e) {
            //console.log(endX);
            //结束触摸一瞬间记录手指最后所在坐标x的位置 endX
            //比较endX与startX的大小，并获取每次运动的距离，当距离大于一定值时认为是有方向的变化
            var distance = Math.abs(startX - endX);
            if (distance > offset) {
                //说明有方向的变化
                //根据获得的方向 判断是上一张还是下一张出现
                $(this).carousel(startX > endX ? 'next' : 'prev');
            }
        })
    });
</script>