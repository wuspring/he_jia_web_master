<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/4
 * Time: 10:04
 */
use yii\bootstrap\ActiveForm;
use  yii\helpers\Url;
$this->title = '忘记密码';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- 注册 -->
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>找回密码</h2>
</div>
<!-- 注册 -->
<div id="login">
    <!-- Tab内容 -->
    <div class="tab_con tab-content">
        <div id="home" class="container tab-pane active"><br>
            <ul>
                <li><input class="text text_icon3" type="number" id="mobile" placeholder="请输入手机号"/></li>
                <li class="verification">
                    <input class="text text_icon4" type="number" id="code"  placeholder="请输入验证码"/>
                    <a class="button" id="smscode" href="javascript:void(0)">获取验证码</a>
                </li>
                <li><input class="text text_icon2" type="password" id="password" placeholder="请输入密码"/></li>
                <li><input class="text text_icon2" type="password"  id="password1" placeholder="请确认密码"/></li>
                <li><input class="submit" type="submit" id="quren" value="提交修改"/></li>
            </ul>
        </div>
    </div>
</div>



<!-- 模态框- 提醒弹框-3s自动消失 -->
<div class="modal modal_explain modal_explain_sm modal_remind fade" id="myModalRemind">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">提醒</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align" id="tipInfo"></p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn" data-dismiss="modal">确 定</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var issend=true;
    var t=60;
    var code='';
    $(document).on('click','#smscode',function () {

        var mobile=$("#mobile").val();

        var check_phone_number = /^1[3456789]\d{9}$/;
        if (mobile.length == 0) {
            tipModel('手机号不能为空');
            return;
        }
        if (mobile.length != 11) {
            tipModel('请输入有效的手机号');
            return;
        }
        if (!mobile.match(check_phone_number)) {

            tipModel('请输入有效的手机号');
            return;
        }
        if (issend){
            issend=false;
            for(i=1;i<=60;i++) {
                window.setTimeout("update_a(" + i + ","+t+")", i * 1000);
            }
            $.post('<?php echo Url::toRoute(['api/smscode']);?>',{mobile:mobile},function(res){
                if (res.state==1){
                    code=res.code;
                }
                alert(res.msg)
            },'json')
        }

    });

    function update_a(num,t) {
        var get_code=document.getElementById('smscode');
        if(num == t) {
            get_code.innerHTML =" 重新发送 ";
            issend=true;
        }
        else {
            var printnr = t-num;
            get_code.innerHTML =printnr +" 秒";
        }
    }
    $("#quren").click(function () {
        var smscode=$("#code").val();
        var password=$("#password").val();
        var password1=$("#password1").val();
        var mobile=$("#mobile").val();

        var check_phone_number = /^1[3456789]\d{9}$/;
        if (mobile.length == 0) {
            tipModel('手机号不能为空');
            return;
        }
        if (mobile.length != 11) {
            tipModel('请输入有效的手机号');
            return;
        }
        if (!mobile.match(check_phone_number)) {
            tipModel('请输入有效的手机号');
            return;
        }
        if (smscode.length==0){
            tipModel('请输入验证码');
            return;
        }

        if (password!=password1){
            tipModel('两次密码不一致');
            return;
        }
        if(password.length<6||password.length>12){
            tipModel('密码长度应该在6-12个字符');
            return;
        }
        $.post('<?php echo Url::toRoute(['index/forgetpwd']);?>',{mobile:mobile,password:password,smscode:smscode},function(res){
            if (res.code==1){
                tipModel(res.msg);
                $("#myModalRemind").find('.btn').addClass('clickLogin');
            }else{
                tipModel(res.msg)
            }
        },'json')
    });

    //点击提示
    function  tipModel(text) {
        $("#myModalRemind").modal('show');
        $("#tipInfo").text(text);
    }
    //点击到登录
    $(document).on('click','.clickLogin',function () {
        window.location.href="<?php echo Url::toRoute(['index/login']);?>"
    })
</script>