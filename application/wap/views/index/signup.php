<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/4
 * Time: 10:04
 */

use yii\bootstrap\ActiveForm;
use  yii\helpers\Url;
use  wap\models\MemberRg;

?>
<style>
    .text_uspa{
        margin-bottom: 20px;
    }
    .help-block-error{
        display: none;
    }
</style>

<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>注册</h2>
</div>
<!-- 注册 -->
<div id="login">
    <?php $form = ActiveForm::begin([
        'id' =>'reg_form2',
        'action' => Url::toRoute(['index/signup']),
        'fieldConfig' => [
            'options'=>['class'=>''],
            'template' => "{input}\n{hint}\n{error}",
            'horizontalCssClasses' => [
                'label' => '',
                'offset' => 'col-sm-offset-4',
                'wrapper' => '',
                'error' => '',
                'hint' => '',
            ],
        ],

    ]); ?>
    <!-- Tab内容 -->
    <div class="tab_con tab-content">
        <div id="home" class="container tab-pane active"><br>

            <ul>
                <li>
                    <?= $form->field($model, 'username')->textInput(['placeholder'=>'请输入账号','class'=>'text text_icon1']) ?>
                </li>
                <li>
                    <?= $form->field($model, 'mobile')->textInput(['placeholder'=>'请输入手机号','class'=>'text text_icon3']) ?>
                </li>
                <li class="verification">
                    <?= $form->field($model, 'smscode')->textInput(['placeholder'=>'请输入验证码','class'=>'text text_icon4']) ?>
                    <a href="javascript:void(0);" id="smscode" class="button">获取验证码</a>
                </li>
                <li>
                    <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'请输入密码','class'=>'text text_icon2']) ?>
                </li>
                <li>
                    <?= $form->field($model, 'password1')->passwordInput(['placeholder'=>'请确认密码','class'=>'text text_icon2']) ?>
                </li>
                <li><input class="submit" type="submit" value="立即注册"/></li>
            </ul>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <!-- 其他 -->
    <div class="other">
        <p>已有账号？<a class="go_register" href="<?=Url::toRoute(['index/login'])?>">立即登录</a></p>
    </div>
</div>

<!-- 模态框- 文本提醒-简易版 弹框 -->
<div class="modal modal_explain modal_explain_sm fade" id="myModalExplain">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- 模态框头部 -->
            <div class="modal-header">
                <p class="p_title">信息</p>
            </div>
            <!-- 模态框主体 -->
            <div class="modal-body">
                <p class="text-align"></p>
            </div>
            <!-- 模态框底部 -->
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">确 定</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var issend=true;
    var t=60;
    $("#smscode").click(function () {
        if (issend){
            var mobile=$("#memberrg-mobile").val();
            var check_phone_number = /^1[3456789]\d{9}$/;
            if (mobile.length == 0) {
                tipModel('手机号不能为空');
                return;
            }
            if (mobile.length != 11) {
                tipModel('请输入有效的手机号');
                return;
            }
            if (!mobile.match(check_phone_number)) {
                tipModel("请输入有效的手机号");
                return;
            }
            issend=false;

            for(i=1;i<=60;i++) {
                window.setTimeout("update_a(" + i + ","+t+")", i * 1000);
            }
            $("#smscode").addClass('disabled');
            $.post('<?php echo Url::toRoute(['api/smscode']);?>',{mobile:mobile},function(res){
                if (res.state==1){
                    //code=res.code;
                }

            },'json')
        }

    });

    var $form = $('#reg_form2');
    $form.on('beforeSubmit', function() {
        var data = $form.serialize();
        $.ajax({
            url: $form.attr('action'),
            type: 'POST',
            data: data,
            dataType: "json",
            success: function (data) {
                 console.log(data);
                $(".text-align").children().remove();
                $(".text-align").text('');
                if (data.code==1){
                    tipModel(data.msg);
                    $("#myModalExplain").find('.btn').addClass('toIndexPage');
                } else {

                    var text='';
                    $.each(data, function (key, value) {
                        switch (key){
                            case 'username':
                                text+='<li>'+'账号:'+value+'</li>';
                                break;
                            case 'mobile':
                                text+='<li>'+'手机号:'+value+'</li>';
                                break;
                            case 'smscode':
                                text+='<li>'+'验证码:'+value+'</li>';
                                break;
                            case 'password':
                                text+='<li>'+'密码:'+value+'</li>';
                                break;
                            case 'password1':
                                text+='<li>'+'密码:'+value+'</li>';
                                break;
                            case 'code':
                                text+='<li>'+value+'</li>';
                                break;
                            default:
                                text+='<li>'+'密码:'+value+'</li>';
                        }
                    });
                    $("#myModalExplain").modal('show');
                    $(".text-align").append(text);
                }

            },
            // error: function(jqXHR, errMsg) {
            //     alert("网络不佳");
            //     alert(errMsg)
            // }
        });
        return false; // 防止默认提交
    });

    $('#reg_form2').on('afterValidate',function (event, messages, errorAttributes){
        if(errorAttributes.length>0){
            var ddd =messages[errorAttributes[0].id][0];
            tipModel(ddd);
        }
    });
    function update_a(num,t) {
        var get_code=document.getElementById('smscode');
        if(num == t) {
            get_code.innerHTML =" 重新发送 ";
            issend=true;
            $("#smscode").removeClass('disabled');
        }
        else {
            var printnr = t-num;
            get_code.innerHTML =printnr +" 秒";
        }
    }
    //点击到首页
    $(document).on('click','.toIndexPage',function () {
        window.location.href="<?=Url::toRoute(['index/index'])?>";
    });

    function  tipModel(text) {
        $("#myModalExplain").modal('show');
        $(".text-align").text(text);
    }
</script>