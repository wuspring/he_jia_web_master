<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/4
 * Time: 10:04
 */
use yii\bootstrap\ActiveForm;
use  yii\helpers\Url;
$this->title = '会员登录';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .help-block-error{
        display: none;
    }
</style>
<!-- 登录 -->
<div id="login">

    <!-- Tab标签 -->
    <div class="tab_nav">
        <img class="tab_nav_bg" src="/public/wap/images/tab_nav_bg.jpg"/>
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active"  href="<?=Url::toRoute(['index/login'])?>">账户登录</a>
            </li>
            <li class="nav-item">
                <a class="nav-link "  href="<?=Url::toRoute(['index/fast-login'])?>">快捷登录</a>
            </li>
        </ul>
    </div>
    <?php $form = ActiveForm::begin([
        'id' =>'loginform',
        'action' => Url::toRoute(['index/login']),

        'fieldConfig' => [
            'options'=>['class'=>''],
            'template' => "{input}\n{hint}\n{error}",
            'horizontalCssClasses' => [
                'label' => '',
                'offset' => 'col-sm-offset-4',
                'wrapper' => '',
                'error' => '',
                'hint' => '',
            ],
        ],

    ]); ?>
    <!-- Tab内容 -->
    <div class="tab_con tab-content">
        <div id="home" class="container tab-pane active"><br>
            <ul>
                <li>
                    <?= $form->field($model, 'username')->textInput(['type'=>'number','placeholder'=>'请输入账号','class'=>'text text_icon1']) ?>
                </li>
                <li>
                    <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'请输入密码','class'=>'text text_icon2']) ?>
                </li>
                <li><input class="submit" type="submit" value="立即登录"/></li>
            </ul>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <!-- 其他 -->
    <div class="other">
        还没有账号？<a class="go_register" href="<?=Url::toRoute(['index/signup'])?>">立即注册</a><a class="forget" href="<?=Url::toRoute(['index/forgetpwd'])?>">忘记密码？</a>
    </div>

    <!-- 第三方登录 -->
    <div class="other_login">
        <h2>使用第三方账号登录</h2>
        <p>
            <a href="<?= \DL\service\UrlService::build('api/wechat-login'); ?>"><img src="/public/wap/images/login_ico1.png"/></a>
            <a href="#"><img src="/public/wap/images/login_ico2.png"/></a>
            <a href="#"><img src="/public/wap/images/login_ico3.png"/></a>
        </p>
    </div>
    <!-- 条款 -->
    <div class="terms">
        <p>登录表示您已经阅读过并且您已同意和家网的关于<br/><a href="<?=Url::toRoute(['index/member-agreement'])?>">《会员注册协议》</a>的条款</p>
    </div>
    <!-- 模态框- 文本提醒-简易版 弹框 -->
    <div class="modal modal_explain modal_explain_sm fade" id="myModalExplain">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- 模态框头部 -->
                <div class="modal-header">
                    <p class="p_title">信息</p>
                </div>
                <!-- 模态框主体 -->
                <div class="modal-body">
                    <p class="text-align"></p>
                </div>
                <!-- 模态框底部 -->
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">确 定</button>
                </div>
            </div>
        </div>
    </div>

<script>
    var $form = $('#loginform');
    $form.on('beforeSubmit', function() {
        var data = $form.serialize();
        $.ajax({
            url: $form.attr('action'),
            type: 'POST',
            data: data,
            dataType: "json",
            success: function (data) {
                // 执行成功
                $(".text-align").children().remove();
                $(".text-align").text('');
                var text='';
                $.each(data, function (key, value) {
                    text+='<li>'+value+'</li>';
                });
                $("#myModalExplain").modal('show');
                $(".text-align").append(text);
            },
            // error: function(jqXHR, errMsg) {
            //     alert("网络不佳");
            //     alert(errMsg)
            // }
        });
        return false; // 防止默认提交
    });

    $('#loginform').on('afterValidate',function (event, messages, errorAttributes){

        if(errorAttributes.length>0){
            var  info =messages[errorAttributes[0].id][0];
            $("#myModalExplain").modal('show');
            $(".text-align").text(info);
        }
    });
</script>
