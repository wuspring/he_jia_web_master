<?php

use DL\service\UrlService;
use wap\models\Order;

global $currentCity;

?>
<div class="head">
    <a href="javascript:history.back(-1)"><img src="/public/wap/images/icon_left.png" alt="返回"></a>
    <h2>确认订单</h2>
</div>
<div class="casesure_product">
    <ul class="list-unstyled clearfix com_productlist com_productlistone">
        <?php foreach ($order->orderGoodsList AS $good) :?>
        <li>
            <a href="<?= UrlService::build(['goods/detail', 'id' => $good['goods_id']]); ?>" class="product">
                <span class="product_img float-left"><img src="<?= $good['goods_pic']; ?>" class="loading_img" data-url="<?= $good['goods_pic']; ?>" alt="<?= $good['goods_name']; ?>"></span>
                <div class="product_con">
                    <p class="product_name text-truncate"><?= $good['goods_name']; ?></p>
                    <p class="product_price orange">专享价<span><?= $good['goods_price']; ?></span>元/<span>套</span><span class="s"><span><?= $good['sell_price']; ?></span>元/<span>套</span></span></p>
                    <p class="product_handsel mt-4">订金：<span class="orange">￥<span><?= $order->order_amount; ?></span></span></p>
                </div>
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
</div>
<div class="casesure_paytype">
    <p class="p1">选择支付方式</p>
    <div class="casesure_paytype_list">
        <div class="list <?= $order->pay_method == Order::PAY_MECHOD_WECHAT?'active':''; ?>" data-id="setPay" data-val="<?=Order::PAY_MECHOD_WECHAT?>"><img src="/public/wap/images/icon_paytype_wx.png" alt="微信支付"><span>微信支付</span></div>
        <div class="list <?= $order->pay_method == Order::PAY_MECHOD_ALIPAY?'active':''; ?>"  data-id="setPay"  data-val="<?=Order::PAY_MECHOD_ALIPAY?>"><img src="/public/wap/images/icon_paytype_zfb.png" alt="支付宝"><span>支付宝</span></div>
    </div>
</div>
<div class="case_btns">
    <div class="case_btns_con">
        <a data-href="<?= UrlService::build(['payment/pay', 'number' => $order->orderid]); ?>" data-id="pay" class="btn">提交订单</a>
        <p class="p">合计：<span class="s orange">￥<span><?= $order->order_amount; ?></span></span></p>
    </div>


</div>


<script type="text/javascript">
    $(function () {
        //点击切换支付方式
        $('.casesure_paytype_list .list').bind('click',function(){
            $(this).addClass('active').siblings().removeClass('active');
        })
    });
</script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
<script type="text/javascript">
    $('[data-id="pay"]').click(function () {
        var href = $(this).attr('data-href'),
            data = {};

        var method = $('[data-id="setPay"].active').attr('data-val');

        if (method == 'WECHAT') {
            if (isWeiXin()) {
                data.type = "WeiXin";

                // 获取openId
                $.get('<?= UrlService::build('api/get-openid'); ?>', {}, function (res) {
                    res = JSON.parse(res);
                    if (res.status) {
                        data.code = res.data.code;
                        $.ajax({
                            type: "post",
                            url: href,
                            data: data,
                            dataType: "json",
                            success: function(res){
                                if (res.status) {
                                    var info = res.data;
                                    callpay(info.parameters);
                                    return true;
                                }

                                alert(res.msg);
                            }
                        });
                        return false;
                    }

                    alert(res.msg);
                })

            } else {
                $.ajax({
                    type: "post",
                    url: href,
                    data: data,
                    dataType: "json",
                    success: function(res){
                        if (res.status) {
                            var info = res.data;
                            window.location.href = info.request_path;
                            return true;
                        }

                        alert(res.msg);
                    }
                });
            }
        } else {
            window.location.href = href;
        }


        return false;
    });

    $('[data-id="setPay"]').click(function () {
        var that = $(this),
            type = that.attr('data-id'),
            val = that.attr('data-val'),
            data = {
                number : '<?= $order->orderid; ?>'
            };
        data[type] = val;

        $.ajax({
            url: '<?= UrlService::build('order/update'); ?>',
            type : 'post',
            async : false,
            data : data,
            dataType:'json',
            success:function(data) {
                if (data.status) {
                    window.location.reload();
                    return false;
                }
                alert(data.msg);
            }
        });
    });

    //调用微信JS api 支付
    function jsApiCall(data)
    {
        WeixinJSBridge.invoke(
            'getBrandWCPayRequest',
            data,
            function(res){
                if (res.err_msg == "get_brand_wcpay_request:ok") {
                    window.location.href = '<?= \Yii::$app->request->hostInfo . UrlService::build('member/my-reserve'); ?>';
                }else if (res.err_msg == "get_brand_wcpay_request:cancel") {
                    alert("支付取消", function () {
                        return false;
                    })
                }
            }
        );
    }

    function callpay(data)
    {
        if (typeof WeixinJSBridge == "undefined"){
            if( document.addEventListener ){
                document.addEventListener('WeixinJSBridgeReady', jsApiCall(data), false);
            }else if (document.attachEvent){
                document.attachEvent('WeixinJSBridgeReady', jsApiCall(data));
                document.attachEvent('onWeixinJSBridgeReady', jsApiCall(data));
            }
        }else{
            jsApiCall(data);
        }
    }
</script>