<?php

namespace wap\controllers;

use wap\logic\StoreLogic;

use wap\models\Goods;
use wap\models\GoodsClass;
use wap\models\GoodsCoupon;
use wap\models\MemberStoreCoupon;
use wap\models\StoreGcScore;
use yii\helpers\ArrayHelper;

class CouponController extends BaseController
{
    // 列表条数
    public $pageLimit = 10;

    /**
     * 优惠券列表
     *
     * @author daorli
     * @date 2019-6-12
     */
    public function actionIndex($ticketId=0)
    {
        $currentCity = $this->getRequestCity();    //城市id
        $categorys = GoodsClass::find()->where([
            'and',
            ['in', 'fid', [1, 2, 3]],
            ['=', 'is_show', 0],
            ['=', 'is_del', 0],
        ])->orderBy('sort desc')->all();

        // 当前有效店铺
        $useFulStoreIds = StoreLogic::init()->usefulStoreIds($currentCity);

        //精品推荐
        $query = GoodsCoupon::find()->alias('gc');
        $recommendCoupon = $query->leftJoin('user as user', 'user.id = gc.user_id')
            ->andFilterWhere(['user.provinces_id' => $currentCity])->andFilterWhere(['gc.is_del' => 0])
            ->andFilterWhere(['in', 'user.id', $useFulStoreIds])
            ->limit($this->pageLimit)->all();

        return $this->render('index',
            [
                'ticketId' => $ticketId,
                'categorys' => $categorys,
                'recommendCoupons' => $recommendCoupon
            ]);
    }

    /**
     * 优惠券列表API
     *
     * @author daorli
     * @date 2019-6-13
     */
    public function actionApiList()
    {
        $category = \Yii::$app->request->get('cid', 0);
        $page = \Yii::$app->request->get('p', 1);

        $currentCity = $this->getRequestCity();    //城市id
        $useFulStoreIds = StoreLogic::init()->usefulStoreIds($currentCity); // 当前有效店铺

        $filter = [
            'and',
            ['=', 'user.provinces_id', $currentCity],
            ['=', 'gc.is_del', 0],
            ['in', 'user.id', $useFulStoreIds],
        ];

        $query = GoodsCoupon::find()->alias('gc');
        $query->leftJoin('user as user', 'user.id = gc.user_id')->where($filter);

        if ($category) {
            $query->leftJoin(['g' => Goods::tableName()], 'g.id=gc.goods_id')->andFilterWhere(['g.gc_id' => $category]);
        }

        $coupons = $query->offset($page * $this->pageLimit)
            ->limit($this->pageLimit)->all();


        $memberHasCouponsIds = [];
        if (\Yii::$app->user->identity) {
            $hasCoupons = MemberStoreCoupon::findAll([
                'member_id' => \Yii::$app->user->id
            ]);

            $memberHasCouponsIds = ArrayHelper::getColumn($hasCoupons, 'coupon_id');
        }

        $data = [];
        foreach ($coupons AS $coupon) {
            $ca = $coupon->attributes;
            $storeInfo = $coupon->user;
            $ca['has'] = (int)in_array($coupon->id, $memberHasCouponsIds);

            $ca['store'] = [
                'nickname' => $storeInfo->nickname,
                'avatar' => $storeInfo->avatar,
                'avatarTm' => $storeInfo->avatarTm,
                'provinces_id' => $storeInfo->provinces_id,
            ];
            $data[] = $ca;
        }

        $ticketId = \Yii::$app->request->get('ticketId', 0);

        apiSendSuccess("ok", [
            'lists' => $data,
            'ticketId' => $ticketId,
        ]);
    }

    /**
     * 优惠券详情
     * @author wufeng
     * @date 2019-6-13
     * @return string
     */
    public function actionNotes($storeId)
    {
        $store_info = StoreLogic::init()->storeInfo($storeId,true);
        return $this->render('notes',[
            'shop' => $store_info
        ]);
    }

}

