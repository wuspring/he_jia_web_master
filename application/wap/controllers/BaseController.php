<?php

namespace wap\controllers;

use common\models\Channel;
use DL\Project\TecentMapApi;
use wap\models\IpMemory;
use wap\models\Provinces;
use yii;
use wap\models\SelectCity;
use DL\service\CacheService;
use DL\vendor\ConfigService;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * AddressController implements the CRUD actions for Address model.
 */
class BaseController extends Controller
{
    public $defaultCity;
    public $cityDatas = [];

    protected $memberInfo;

    public $webBrowserKey;
    protected $defaultCityKey = 'ADMIN_SELECT_CITY_ID';
    public function beforeAction($action){

        $route=\yii::$app->session->get('routedata');
        $act=Yii::$app->controller->getRoute();

        if (empty($route)) {
            $route=array();
            $route[0]=array(
                'route'=>Yii::$app->controller->getRoute(),
                'params'=>Yii::$app->request->getQueryParams(),
            );
            $route[1]=array(
                'route'=>Yii::$app->controller->getRoute(),
                'params'=>Yii::$app->request->getQueryParams(),
            );
        }else{
            if (count($route[0])!=2){
                $route=array();
                $route[0]=array(
                    'route'=>Yii::$app->controller->getRoute(),
                    'params'=>Yii::$app->request->getQueryParams(),
                );
                $route[1]=array(
                    'route'=>Yii::$app->controller->getRoute(),
                    'params'=>Yii::$app->request->getQueryParams(),
                );
            }else{
                if ($act=='index/set-city'){
                    $route[0]=$route[1];
                }else{
                    $route[0]=$route[1];
                    $route[1]=array(
                        'route'=>Yii::$app->controller->getRoute(),
                        'params'=>Yii::$app->request->getQueryParams(),
                    );
                }
            }

        }
        \yii::$app->session->set('routedata',$route);


        return parent::beforeAction($action);

    }
    public function init()
    {
        parent::init();
        session_status()==2 or session_start();
        $this->webBrowserKey = '__CITY_INFO__' . session_id();

        $this->memberInfo = \Yii::$app->user->identity;

        if (!empty($_GET['channel'])){
            \yii::$app->session->set('channel',$_GET['channel']);

            // 统计渠道来源
            $channel = new Channel();
            $channel->channel = trim($_GET['channel']);
            $channel->create_time = date('Y-m-d H:i:s');
            $channel->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * 获取当前城市信息
     *
     * @return int provinces id
     */
    protected function getRequestCity()
    {
        $session = \Yii::$app->session;
        $cityId = $session->get($this->webBrowserKey);

        if (!$cityId) {
            // 默认城市
            $cityId = ConfigService::init(ConfigService::EXPAND)->get('default_city');
            $ip = $_SERVER['REMOTE_ADDR'];
            if (!strlen($ip) and isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }

            $ipRelation = (array)CacheService::init()->get('__IP_RELATION_INFO__');
            if (isset($ipRelation[$ip])) {
                $requestId = $ipRelation[$ip];
            } else {
                $uniqueKey = md5(trim($ip));

                $ipInfo = IpMemory::findOne([
                    'unique_key' => $uniqueKey
                ]);

                $requestId = $cityId;
                if ($ipInfo) {
                    $requestId = $ipInfo->provinces_id;
                } else {
                    $position = TecentMapApi::init()->getPositionByIp();
                    if($position->status==0){
                        $provinces = Provinces::findOne($position->result->ad_info->adcode);
                        $requestId = $provinces ? $provinces->id : $cityId;
                    }

                    $ipInfo = new IpMemory();
                    $ipInfo->ip = $ip;
                    $ipInfo->provinces_id = $requestId;
                    $ipInfo->unique_key = $uniqueKey;
                    $ipInfo->save();
                    $ipRelation[$ip] = $requestId;
                    CacheService::init()->set('__IP_RELATION_INFO__', $ipRelation);
                }

            }

            $selectCitys = SelectCity::find()->where([])->all();
            $selectCityIds = ArrayHelper::getColumn($selectCitys, 'provinces_id');

            in_array($requestId, $selectCityIds) and $cityId = $requestId;

            $session->set($this->webBrowserKey, $cityId);
        }
        if(isset($_GET['city_id']) && $cityId!=$_GET['city_id']){
            $cityId = $_GET['city_id'];
            $provinces = Provinces::findOne($cityId);
            $selectCity  = SelectCity::findOne([
                'provinces_id' => $provinces->id
            ]);
            $selectCity or apiSendError("暂不支持该城市");

            $session = \Yii::$app->session;
            $session->set($this->webBrowserKey, $selectCity->provinces_id);
            \yii::$app->session->set('citypinyin',$provinces->pinyin);
            \yii::$app->session->set('citybianhao',$provinces->id);
        }
        global $secretCityId;
        $secretCityId = $cityId;
        return $cityId;
    }

    /**
     * 获取展会类型
     * @author wufeng
     * @date 2019-6-11
     * @return mixed|string
     */
    public function getTicketType(){
        $session = \Yii::$app->session;
        $ticketType = $session->get('ticketType');
        if(empty($ticketType)){
            $ticketType = 'HOME';
            $session->set('ticketType',$ticketType);
        }
        return $ticketType;
    }

    /**
     * render display
     * @author daorli
     * Date: 2019/6/5
     *
     * @param string $view
     * @param array $params
     *
     * @return string
     */
    public function render($view, $params = [])
    {
        global $currentCity;
        $cityId = $this->getRequestCity();
        $currentCity = Provinces::findOne($cityId);

        $params = array_merge([
            'defaultCity' => $currentCity
        ], $params);

        return parent::render($view, $params);
    }

    /**
     * ip定位
     */
    public function getIpPostion(){
        //初始化
        $ch = curl_init();
        //设置抓取的url
        $key = 'OF3BZ-75B3X-YWJ42-7HSOY-YCQ7Q-Y4FLP';
        $ipUrl = "https://apis.map.qq.com/ws/location/v1/ip?key=".$key."&output=JSON";
        curl_setopt($ch, CURLOPT_URL, $ipUrl);
        //设置头文件的信息作为数据流输出
        curl_setopt($ch, CURLOPT_HEADER, 1);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // 关闭SSL验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        //执行命令
        $data = curl_exec($ch);
        if (curl_errno($ch)) {
            return curl_error($ch);
        }
        //关闭URL请求
        curl_close($ch);
        //显示获得的数据
        $data = [
            'status'=>0,
            'message'=>"query ok",
            "result"=> [
                "location"=> [
                    "lng"=>116.407526,
                    "lat"=>39.90403
                ]
            ]
        ];
        return $data;
    }
}
