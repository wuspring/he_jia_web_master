<?php
namespace wap\controllers;
use chenkby\region\RegionAction;
use wap\models\Address;
use wap\models\Provinces;
use Yii;
use yii\filters\AccessControl;


class AddressController extends BaseController
{

    public function actions()
    {
        $actions=parent::actions();
        $actions['get-region']=[
            'class'=>RegionAction::className(),
            'model'=>Provinces::className()
        ];
        return $actions;
    }
    /**
     * Notes:地址列表
     * User: qin
     * Date: 2019/6/4
     * Time: 11:17
     * @return string
     */
    public function actionAddressList()
    {
        $get = Yii::$app->request->get();
        unset($get['r']);

        $addressList = Address::find()->where(['memberId'=>yii::$app->user->id])->orderBy('id DESC')->all();

        return $this->render('address-list', [
            'addressList' =>$addressList,
            'get' => $get
        ]);
    }

    /**
     * Notes: 新增地址
     * User: qin
     * Date: 2019/6/4
     * Time: 11:18
     * @return string|\yii\web\Response
     */
    public function actionAddAddress()
    {
        $model = new Address();
        $get = Yii::$app->request->get();
        unset($get['r']);

        if (Yii::$app->request->isAjax){
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                $model->memberId=yii::$app->user->id;
                if ($model->save()){
                    if ($model->isDefault==1) {
                        Address::updateAll(['isDefault'=>0],'memberId='.yii::$app->user->id." and id !=".$model->id);
                    }

                    $request = array_merge(['address-list'], $get);
                    return $this->redirect($request);
                }
            }else{
                return json_encode($model->firstErrors);
            }
        }

        return $this->render('add_address', [
            'model' => $model,
            'get' => $get
        ]);

    }

    /**
     * Notes: 新增地址
     * param id地址id
     * User: qin
     * Date: 2019/6/4
     * Time: 11:18
     * @return string|\yii\web\Response
     */
    public function actionEditAddress($id)
    {
        $model = Address::findOne($id);

        if (Yii::$app->request->isAjax){
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                $model->memberId=yii::$app->user->id;
                if ($model->save()){
                    if ($model->isDefault==1) {
                        Address::updateAll(['isDefault'=>0],'memberId='.yii::$app->user->id." and id !=".$model->id);
                    }
                    return $this->redirect(['address-list']);
                }
            }else{
                return json_encode($model->firstErrors);
            }
        }

        return $this->render('edit_address', [
            'model' => $model,
        ]);

    }

    /**
     * Notes: 删除地址
     * User: qin
     * Date: 2019/6/12
     * Time: 10:40
     */
    public  function actionDeleteAddress(){

            $post = Yii::$app->request->post();
            $deleteNum=Address::deleteAll(['id'=>$post['addressId']]);
            if ($deleteNum>0){
                $json=array('code'=>1,'msg'=>'删除成功!');
            }else{
                $json=array('code'=>0,'msg'=>'删除失败!');
            }
            echo json_encode($json);yii::$app->end();
    }

    /**
     * Notes: 设置为默认地址
     * User: qin
     * Date: 2019/6/17
     * Time: 18:53
     */
    public function actionSetDefault(){

        $data = Yii::$app->request->post();
        $model = Address::findOne($data['setAddressId']);
        $model->isDefault=1;
        if ($model->save()){
            $json=array('code'=>1,'msg'=>'设置默认地址成功!');
            Address::updateAll(['isDefault'=>0],'memberId='.yii::$app->user->id." and id !=".$data['setAddressId']);
        }else{
            $json=array('code'=>0,'msg'=>'设置默认地址失败!');
        }
        return json_encode($json);
    }

}
