<?php

namespace wap\controllers;


use common\models\NavsWap;
use common\models\UploadForm;
use wap\logic\ApplyGoodsLogic;
use wap\logic\AppointmentOrderLogic;
use wap\logic\CollectLogic;
use wap\logic\CouponLogic;
use wap\logic\MemberOrderLoginc;
use wap\logic\OrderLogic;
use wap\models\Address;
use wap\models\Advertising;
use wap\models\Collect;
use wap\models\Comment;
use wap\models\Config;
use wap\models\Goods;
use wap\models\GoodsCoupon;
use wap\models\History;
use wap\models\IntegralLog;
use wap\models\IntergralGoods;
use wap\models\IntergralOrder;
use wap\models\IntergralOrderGoods;
use wap\models\Member;
use wap\models\MemberStoreCoupon;
use wap\models\MTicketConfig;
use wap\models\Order;
use wap\models\OrderGoods;
use wap\models\SystemCoupon;
use wap\models\Ticket;
use wap\models\TicketApply;
use wap\models\TicketApplyGoods;
use wap\models\TicketAsk;
use wap\models\User;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\UploadedFile;

class MemberController extends BaseController
{

    protected $caType=Ticket::CA_TYPE_ZHAN_HUI;
    protected $formType;
    /**
     * Notes:会员中心首页
     * User: qin
     * Date: 2019/6/4
     * Time: 11:07
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest){
            return $this->redirect(['index/fast-login']);
        }
        $cityId = $this->getRequestCity();
        $select_menu = 0;
        $session_ticket = Yii::$app->session->get('ticketType');
        if ($session_ticket == 'TICKET'){              //史方写的导航
            $session_ticket_id = Yii::$app->session->get('ticketId');
            $ticketConfig = MTicketConfig::findOne([
                'ticket_id' => $session_ticket_id
            ]);
            if(!empty($ticketConfig)){
                $ticketConfig->guiders = decodeJsInputUploadIco($ticketConfig->guiders);
            }else{
                $ticketConfig = new MTicketConfig();
            }
        }else{     //西方的导航
            // 首页菜单
            $homeMenus = NavsWap::find()->where(['provinces_id'=>$cityId,'status'=>1,'type'=>'home'])->limit(5)->orderBy('sort ASC')->all();
            $select_menu = 1;
        }

        $userId = empty(Yii::$app->user->id)?0:Yii::$app->user->id;
        $model = Member::findOne($userId);
        $couponCount = MemberStoreCoupon::find()->andWhere(['member_id'=>$userId])->count();
        $qianDao = IntegralLog::find()->andFilterWhere(['memberId'=>Yii::$app->user->id,'type'=>IntegralLog::INTEGRALTYPE])
            ->andWhere(['like','create_time',date('Y-m-d')])->one();
        //爆款预约
        $reserGoods = ApplyGoodsLogic::getSubscribeGoodsList($cityId);

            if (!empty($reserGoods)){
                $goods = $reserGoods;
                $is_zhanhui = 1;
            }else{
                $goods = Goods::find()->where(['isdelete'=>0,'goods_state'=>1])->limit(6)->all();
                $is_zhanhui = 0;
            }
            if ($select_menu == 0){
                return $this->render('index', [
                    'model' => $model,
                    'goods' => $goods,
                    'couponCount'=>$couponCount,
                    'ticketConfig' =>$ticketConfig,
                    'select_menu' =>$select_menu,
                    'is_zhanhui' =>$is_zhanhui,
                    'qianDao'=>$qianDao
                ]);
            }else{
                return $this->render('index', [
                    'model' => $model,
                    'goods' => $goods,
                    'couponCount'=>$couponCount,
                    'home_menus' =>$homeMenus,
                    'select_menu' =>$select_menu,
                    'is_zhanhui' =>$is_zhanhui,
                    'qianDao'=>$qianDao
                ]);
            }

    }

    /**
     * 获取当前城市发行票据
     * @author daorli
     * Date: 2019/6/5
     */
    protected function getTicketId()
    {
        $ticket = Ticket::findOne([
            'ca_type' => $this->caType,
            'type' => $this->formType,
            'citys' => $this->getRequestCity(),
            'status' => 1
        ]);

        return $ticket ? $ticket->id : 0;
    }

    /**
     * Notes: 我的预定
     * User: qin
     * Date: 2019/6/5
     * Time: 14:14
     */
    public function actionMyReserve($state=-1)
    {
        //检测失效订单
        OrderLogic::init()->checkUnpayOrder();

        $memberOrderLogic = MemberOrderLoginc::init();
        $data=Order::find()->andFilterWhere([
            'buyer_id'=>Yii::$app->user->id,
            'type'=>Order::TYPE_ZHAN_HUI,
            'order_state'=>($state==-1)?'':$state,
            'ticket_type' => TicketApplyGoods::TYPE_ORDER
        ]);

        $paixu='add_time DESC';
        //SORT_DESC  SORT_ASC
        $page = $this->getPagedRows($data, ['order' => $paixu, 'pageSize' => '6', 'rows' => 'models']);

        if ($state == -1 && empty($page['models'])){
            return $this->render('my-reserve-null');
        }

        if (isset($_GET['per-page'])){
            $list=array();
            foreach ($page['models'] as $item){
                $jsonlist['orderId'] = $item->orderid;
                $jsonlist['storeId'] = $item->user_id;
                $storeInfor = $memberOrderLogic->getStoreInfor($item->user_id);
                $goodInfor = $memberOrderLogic->getGoodInfor($item->orderid);
                $ticketInfo = $memberOrderLogic->getDingJin($item->ticket_id);
                $goodDetail =$memberOrderLogic->getGoodDetail($goodInfor->goods_id);
                $jsonlist['storeName'] = $storeInfor->nickname;
                $jsonlist['goodName'] = $goodInfor->goods_name;
                $jsonlist['goods_price'] = intval($goodInfor->goods_price).'/'.$goodDetail->unit;
                $jsonlist['pic'] = $goodInfor->goods_pic;
                $jsonlist['goods_marketprice'] = $goodDetail->goods_marketprice.'/'.$goodDetail->unit;;
                $jsonlist['dingJin'] = $ticketInfo->order_price;
                $jsonlist['num'] = $goodInfor->goods_num;
                $jsonlist['state'] = $item->order_state;
                $jsonlist['stateStr'] = $item->orderState;
                $list[]=$jsonlist;
            }
            $json=array('code'=>1,'pagecount'=>$page['pages']->totalCount,'list'=>$list, 'state' =>$state);

            echo json_encode($json);yii::$app->end();
        }
        return $this->render('my-reserve',[
            'list' => $page['models'],
            'pages' => $page['pages'],
            'state' =>$state
        ]);

    }

    /**
     * Notes: 取消我的预定
     * User: qin
     * Date: 2019/6/18
     * Time: 13:32
     */
    public function actionCancelReserve(){
        $post = Yii::$app->request->post();

        $order = Order::find()->andFilterWhere(['orderid'=>$post['orderid']])->one();
        $order->order_state = Order::STATUS_CLOSE;

        if ($order->save()){
            $json = array('code'=>1,'msg'=>'取消成功');
        }else{
            $json = array('code'=>0,'msg'=>'取消失败');
        }
      return json_encode($json);
    }
    /**
     * Notes: 我的预定  详情
     * User: qin
     * Date: 2019/6/10
     * Time: 19:16
     */
    public  function actionMyReserveDetail($orderid){

        $memberOrderLogic = MemberOrderLoginc::init();

        $data=Order::find()->andFilterWhere(['orderid'=>$orderid])->one();
        $storeInfo =$memberOrderLogic->getStoreInfor($data->user_id);
        $goodInfo =$memberOrderLogic->getGoodInfor($data->orderid);
        $goodDetail =$memberOrderLogic->getGoodDetail($goodInfo->goods_id);
        $ticketDetail =$memberOrderLogic->getDingJin($data->ticket_id);
        $systemCoupon =SystemCoupon::findOne($data->relation_coupon);
        return $this->render('my-reserve-detail',[
            'data'=>$data,
            'storeInfo' => $storeInfo,
            'goodInfo'=>$goodInfo,
            'goodDetail'=>$goodDetail,
            'ticketDetail'=>$ticketDetail,
            'systemCoupon'=>$systemCoupon
        ]);

    }

    /**
     * Notes: 我的预约列表
     * param  $state 订单状态 -1 表示所有
     * User: qin
     * Date: 2019/6/11
     * Time: 10:30
     */

    public  function actionMyAppointment($state=-1){
        //检测失效订单
        OrderLogic::init()->checkUnpayOrder();
        $appointmentLogic = AppointmentOrderLogic::init();
        $data=Order::find()->andFilterWhere([
            'buyer_id'=>Yii::$app->user->id,
            'type'=>Order::TYPE_ZHAN_HUI,

            'ticket_type' => TicketApplyGoods::TYPE_COUPON
        ]);
        if ($state==-1){

        }elseif ($state == 1){
            $data->andWhere(['or' , 'order_state = 1' , 'order_state = 0']);
        }else{
            $data->andWhere(['order_state'=>$state]);
        }

        $paixu='add_time DESC';
        //SORT_DESC  SORT_ASC
        $page = $this->getPagedRows($data, ['order' => $paixu, 'pageSize' => '6', 'rows' => 'models']);

        if ($state == -1 && empty($page['models'])){
            return $this->render('my-reserve-null');
        }

        if (isset($_GET['per-page'])){
            $list=array();
            foreach ($page['models'] as $item){
                 $stateStr =$item->orderState;
                 $storeInfo = $appointmentLogic->getStoreInfo($item->user_id);
                 $goodInfo = $appointmentLogic->getGoodOrderInfo($item->orderid);
                 $goodBrief =  $appointmentLogic->getGoodInfo($goodInfo->goods_id);
                $jsonlist['orderId'] = $item->orderid;
                $jsonlist['storeId'] = $item->user_id;
                $jsonlist['storeName'] = $storeInfo->nickname;
                $jsonlist['goodName'] = $goodInfo->goods_name;
                $jsonlist['goodPrice'] = $goodInfo->goods_price;
                $jsonlist['goodId'] = $goodInfo->goods_id;
                $jsonlist['goodPic'] = $goodInfo->goods_pic;
                $jsonlist['num'] = $goodInfo->goods_num;
                $jsonlist['brief'] = $goodBrief->mobile_body;
                $jsonlist['state'] = $item->order_state;
                $jsonlist['stateStr'] = $stateStr;

                $list[]=$jsonlist;
            }
            $json=array('code'=>1,'pagecount'=>$page['pages']->totalCount,'list'=>$list, 'state' =>$state);

            echo json_encode($json);yii::$app->end();
        }
        return $this->render('my-appointment',[
            'list' => $page['models'],
            'pages' => $page['pages'],
            'state' =>$state
        ]);

    }

    /**
     * Notes:我的预约详细页
     * User: qin
     * Date: 2019/6/17
     * Time: 16:21
     */
    public  function actionMyAppointmentDetail($orderid=0){

        $orderDetail=Order::find()->andFilterWhere(['orderid'=>$orderid])->one();
        $user = User::findOne($orderDetail->user_id);
        $orderGoods=OrderGoods::find()->andFilterWhere(['order_id'=>$orderid])->all();

        return $this->render('my-appointment-detail',[
            'orderDetail'=>$orderDetail,
            'user'=>$user,
            'orderGoods'=>$orderGoods
        ]);
    }
    /**
     * Notes:我的门票
     * User: qin
     * Date: 2019/6/11
     * Time: 11:55
     */
    public function actionMyTicket(){

        $data=TicketAsk::find()->andFilterWhere([
            'member_id'=>Yii::$app->user->id,
        ]);

        $paixu='create_time DESC';
        //SORT_DESC  SORT_ASC
        $page = $this->getPagedRows($data, ['order' => $paixu, 'pageSize' => '6', 'rows' => 'models']);

        if (isset($_GET['per-page'])){
            $list=array();
            foreach ($page['models'] as $item){
                $ticketInfo = Ticket::findOne($item->ticket_id);

                $jsonlist['name'] = $ticketInfo->typeInfo.'展会门票';
                $jsonlist['city'] = $ticketInfo->cityName->cname;
                $openDate =strtotime($ticketInfo->open_date);
                $endDate =strtotime($ticketInfo->end_date);
                $jsonlist['time'] = date('Y',$openDate).'年'.date('m',$openDate).'月'.date('d',$openDate).'日'.'-'.date('m',$endDate).'月'.date('d',$endDate).'日';
                $jsonlist['address'] = $ticketInfo->address;
                $list[]=$jsonlist;
            }
            $json=array('code'=>1,'pagecount'=>$page['pages']->totalCount,'list'=>$list);

            echo json_encode($json);yii::$app->end();
        }
        $text = Config::find()->where(['cKey'=>'INDEX_EXPAND'])->one();
        $model = json_decode($text->cValue);
        return $this->render('my-ticket',[
            'model'=>$model,
            'list' => $page['models'],
            'pages' => $page['pages'],
        ]);
    }

    /**
     * Notes:积分订单列表
     * User: qin
     * Date: 2019/6/4
     * Time: 11:09
     * @param int $state
     * @param string $search_key
     * @return string
     */
    public function actionIntegralOrderlist($state=-1)
    {
        $order_state = ($state == -1) ? '' : intval($state);
        $data = IntergralOrder::find()->andFilterWhere(['order_state' => $order_state])->andFilterWhere(['buyer_id' => Yii::$app->user->id]);

        $paixu='add_time DESC';
        //SORT_DESC  SORT_ASC
        $page = $this->getPagedRows($data, ['order' => $paixu, 'pageSize' => '6', 'rows' => 'models']);

        if (isset($_GET['per-page'])){
            $list=array();
            foreach ($page['models'] as $item){

                $intergral_order_goods = IntergralOrderGoods::find()->andWhere(['order_id' => $item->orderid])->all();
                $sonList = array();
                foreach ($intergral_order_goods as $k => $v) {
                    $arr['orderid'] = $v->order_id;
                    $arr['name'] = $v->goods_name;
                    $arr['attr'] = empty($v->attr) ? '' : $v->attr;
                    $arr['num'] = $v->goods_num;
                    $arr['goods_price'] = intval($v->goods_price);
                    $arr['goods_pic'] = $v->goods_pic;
                    $arr['order_state'] = $item->order_state;
                    $arr['order_state_str'] = $item->orderState;
                    $sonList[] = $arr;
                }
                $list[$item->orderid] = $sonList;
            }
            $json=array('code'=>1,'pagecount'=>$page['pages']->totalCount,'list'=>$list, 'state' =>$state);

            echo json_encode($json);yii::$app->end();
        }
        return $this->render('integral-orderlist',[
            'list' => $page['models'],
            'pages' => $page['pages'],
            'state' =>$state
        ]);
    }


    /**
     * Notes:积分订单详情
     * param   $orderid 订单订单的orderid
     * User: qin
     * Date: 2019/6/4
     * Time: 11:12
     * @param $orderid
     * @return string
     */
    public function actionIntegralOrderdetail($orderid)
    {

        $intergralOrder = IntergralOrder::find()->andFilterWhere(['orderid' => $orderid])->one();

        $intergral_order_goods = IntergralOrderGoods::find()->andWhere(['order_id' => $orderid])->all();

        return $this->render('integral-order-detail', [
            'intergralOrder' => $intergralOrder,
            'intergral_order_goods' => $intergral_order_goods
        ]);
    }

    /**
     * Notes: 确认积分商品收到货
     * User: qin
     * Date: 2019/6/18
     * Time: 15:59
     */
    public function actionReceivingGood(){
        $post = Yii::$app->request->post();
        $intergralOrder = IntergralOrder::find()->andFilterWhere(['orderid' => $post['orderid']])->one();
        $intergralOrder->order_state = 3;
        if ($intergralOrder->save()){
            $json = array('code'=>1,'msg'=>'收货成功');
        }else{
            $json = array('code'=>0,'msg'=>'收货失败');
        }
        return json_encode($json);
    }

    /**
     * Notes: 积分订单 确认收货后到引导用户评价页面
     * param   $orderid 订单订单的orderid
     * User: qin
     * Date: 2019/6/6
     * Time: 10:47
     */
    public function actionTipEvaluate($orderid)
    {

        $intergralOrder = IntergralOrder::find()->andFilterWhere(['orderid' => $orderid])->one();

        empty($intergralOrder) and die('订单没有查到');

        $intergralOrder->order_state = IntergralOrder::STATUS_WAIT_JUDGE;

        if ($intergralOrder->save()) {
            return $this->render('tip-evaluate', [
                'intergralOrder' => $intergralOrder
            ]);
        }
    }

    /**
     * Notes: 积分订单评价
     * param   $orderid 订单订单的orderid
     * User: qin
     * Date: 2019/6/5
     * Time: 18:14
     */
    public function actionEvaluate()
    {
        $data = Yii::$app->request->get();
        $intergralOrder = IntergralOrder::find()->andFilterWhere(['orderid' => $data['orderid']])->one();
        $intergral_order_goods = IntergralOrderGoods::find()->andWhere(['order_id' => $data['orderid']])->all();

        return $this->render('evaluate', [
            'intergralOrder' => $intergralOrder,
            'intergral_order_goods' => $intergral_order_goods
        ]);
    }


    /**
     * Notes: 预定订单评价
     * param   $orderid 订单订单的orderid
     * User: qin
     * Date: 2019/6/5
     * Time: 18:14
     */
    public function actionMoreEvaluate()
    {
        $data = Yii::$app->request->get();
        $intergralOrder = Order::find()->andFilterWhere(['orderid' => $data['orderid']])->one();
        $intergral_order_goods = OrderGoods::find()->andWhere(['order_id' => $data['orderid']])->all();

        return $this->render('more-evaluate', [
            'intergralOrder' => $intergralOrder,
            'intergral_order_goods' => $intergral_order_goods
        ]);
    }
    /**
     * Notes: 用户浏览记录
     * User: qin
     * Date: 2019/6/6
     * Time: 13:35
     */
    public function actionHistory()
    {
        $data=History::find()->andFilterWhere(['member_id' => Yii::$app->user->id]);

        $paixu='create_time DESC';
        //SORT_DESC  SORT_ASC
        $page = $this->getPagedRows($data, ['order' => $paixu, 'pageSize' => '6', 'rows' => 'models']);

        if (isset($_GET['per-page'])){
            $list=array();
            foreach ($page['models'] as $item){
                $good = Goods::findOne($item->goods_id);
                $jsonlist['goodId'] = $item->goods_id;
                $jsonlist['pic'] = $good->goods_pic;
                $jsonlist['price'] = intval($good->goods_price);
                $jsonlist['name'] = $good->goods_name;
                $list[$item->add_time][] = $jsonlist;
            }

            $json=array('code'=>1,'pagecount'=>$page['pages']->totalCount,'list'=>$list);

            echo json_encode($json);yii::$app->end();
        }

        return $this->render('history', [
            'list' => $page['models'],
            'pages' => $page['pages'],
        ]);

    }
    /**
     * Notes: 我的收藏
     * User: qin
     * Date: 2019/6/6
     * Time: 13:35
     */
    public  function actionMyCollect($type='RESERVE_GOOD'){

        $data=Collect::find()->andFilterWhere(['member_id' => Yii::$app->user->id,'type_wap'=>$type]);

        $paixu='create_time DESC';
        //SORT_DESC  SORT_ASC
        $page = $this->getPagedRows($data, ['order' => $paixu, 'pageSize' => '6', 'rows' => 'models']);

        if (isset($_GET['per-page'])){
            $list=array();
            $now = date('Y-m-d');

            switch ($type){
                case Collect::TYPE_RESERVE_GOOD:   //预定商品

                    foreach ($page['models'] as $item){
                        $good = Goods::findOne($item->goods_id);
                        $ticket = Ticket::findOne($item->ticket_id);
                        $jsonlist['collectId'] = $item->id;
                        $jsonlist['goodId'] = $item->goods_id;
                        $jsonlist['pic'] = $good->goods_pic;
                        $jsonlist['marketprice'] = intval($good->goods_marketprice).'/'.$good->unit;        //市场价
                        $jsonlist['price'] = intval($good->goods_price).'/'.$good->unit;                 //商品价格
                        $jsonlist['name'] = $good->goods_name;
                        $jsonlist['order_price'] = $ticket->order_price;
                        $jsonlist['is_over'] = ($now>$ticket->end_date)?1:0;      //是否过期 1为过期
                        $list[]=$jsonlist;
                    }
                    break;
                case Collect::TYPE_SUBSCRIBE_GOOD:   //预约商品
                    foreach ($page['models'] as $item){
                        $good = Goods::findOne($item->goods_id);
                        $ticket = Ticket::findOne($item->ticket_id);
                        $jsonlist['collectId'] = $item->id;
                        $jsonlist['goodId'] = $item->goods_id;
                        $jsonlist['pic'] = $good->goods_pic;
                        $jsonlist['price'] = intval($good->goods_price);
                        $jsonlist['name'] = $good->goods_name;
                        $jsonlist['brief'] = $good->mobile_body;   //商品介绍
                        $jsonlist['is_over'] = ($now>$ticket->end_date)?1:0;      //是否过期 1为过期
                        $list[]=$jsonlist;
                    }
                    break;
                case Collect::TYPE_STORE:   //店铺
                    $collectLog = CollectLogic::init();
                    foreach ($page['models'] as $item){
                        $store = User::findOne($item->shop_id);
                        $jsonlist['collectId'] = $item->id;
                        $jsonlist['storeId'] = $item->shop_id;
                        $jsonlist['pic'] = $store->avatar;
                        $evaluateCount = $collectLog->getAllEvaluate($item->shop_id);
                        $jsonlist['evaluate'] = count($evaluateCount);
                        $jsonlist['name'] = $store->nickname;
                        $attemptCount =$collectLog->getAllAttempt($item->shop_id);
                        $jsonlist['attempt'] =  count($attemptCount); //体验店
                        $list[]=$jsonlist;
                    }
                    break;
                default:
            }
            $json=array('code'=>1,'pagecount'=>$page['pages']->totalCount,'list'=>$list, 'type' =>$type);

            echo json_encode($json);yii::$app->end();
        }


        return $this->render('my-collect', [
            'list' => $page['models'],
            'pages' => $page['pages'],
            'type' =>$type
        ]);
    }

    /**
     * Notes: 删除我的收藏
     * User: qin
     * Date: 2019/6/10
     * Time: 13:50
     */
    public function actionDeleteCollect(){
        $data = Yii::$app->request->get();
        $json = array('code'=>0,'msg'=>'信息');
        $deleteNum = Collect::deleteAll(['id'=>$data['collectId']]);
        if ($deleteNum>0){
            $json = array('code'=>1,'msg'=>'删除成功');
        }else{
            $json = array('code'=>0,'msg'=>'删除失败');
        }
        return json_encode($json);
    }
    /**
     * Notes: 我的优惠劵
     * User: qin
     * Date: 2019/6/6
     * Time: 15:36
     */
    public  function actionMyCoupon($state=0){

        $couponLogic = CouponLogic::init();
        $couponLogic->updateExpire();
        $data=MemberStoreCoupon::find()->andFilterWhere(['member_id' => Yii::$app->user->id]);

        switch ($state){
            case 0:      //全部
                break;
            case 1:      // 待使用
                $data->andWhere(['is_use'=>0,'is_expire'=>0]);
                break;
            case 2:       //待评价
                $data->andWhere(['is_use'=>1,'is_evaluate'=>0]);
                break;
            case 3:         //已使用
               // $data->andWhere(['is_use'=>1,'is_evaluate'=>1]);
                $data->andWhere(['is_use'=>1,'is_evaluate'=>0]);
                break;
            case 4:          //已过期
                $data->andWhere(['is_expire'=>1]);
                break;
            default:

        }
        $paixu='addtime DESC';
        //SORT_DESC  SORT_ASC
        $page = $this->getPagedRows($data, ['order' => $paixu, 'pageSize' => '6', 'rows' => 'models']);

        if (isset($_GET['per-page'])){
            $list=array();
            foreach ($page['models'] as $item){
                $good_coupon = GoodsCoupon::findOne($item->coupon_id);
                $jsonlist['couponId'] = $item->id;
                $jsonlist['end_time'] = date('Y-m-d',strtotime($item->end_time));
                $jsonlist['code'] = $item->code;
                $storeInfo = $couponLogic->getStoreInfo($good_coupon->user_id);
                $jsonlist['storeName'] = $storeInfo->nickname;
                $goodInfo = $couponLogic->getGoodInfo($good_coupon->goods_id);
                $jsonlist['goodName'] = empty($goodInfo)?'':$goodInfo->goods_name;
                $jsonlist['couponType'] = ($good_coupon->goods_id ==-1)?1:0;          //区分店铺优惠劵和商品优惠劵   1为店铺劵 0为商品劵
                $jsonlist['state'] = $couponLogic->getCouponState($item->id);
                $jsonlist['money'] = intval($good_coupon->money);
                $jsonlist['condition'] = empty($good_coupon->condition)?0:intval($good_coupon->condition);
                $list[]=$jsonlist;
            }
            $json=array('code'=>1,'pagecount'=>$page['pages']->totalCount,'list'=>$list, 'state' =>$state);

            echo json_encode($json);yii::$app->end();
        }
        return $this->render('my-coupon',[
            'list' => $page['models'],
            'pages' => $page['pages'],
            'state' =>$state
        ]);
    }
    /**
     * Notes:会员个人信息设置
     * User: qin
     * Date: 2019/6/4
     * Time: 11:14
     * @return string
     */
    public  function actionMemberEdit(){

        $member = Member::findOne(Yii::$app->user->id);
        return $this->render('member-edit',[
            'member' =>$member
        ]);
    }
    /**
     * Notes: 积分订单评价 post 提交
     * User: qin
     * Date: 2019/6/5
     * Time: 18:14
     */
    public function actionEvaluatePost()
    {
        $json = array('code' => 1, 'msg' => '评论成功');
        $data = Yii::$app->request->post();
        $imgs = isset($data['img'])?$data['img']:[];
        $spzl = $data['spzl'];
        $shfw = $data['shfw'];
        $textarea = $data['textarea'];
        $intergral_order_goods = IntergralOrderGoods::find()->andWhere(['order_id' => $data['orderid']])->all();
        foreach ($intergral_order_goods as $k => $v) {

            $comment = new  Comment();
            $comment->order_id = $data['orderid'];
            $comment->goods_id = $v->goods_id;
            $comment->content = isset($textarea[$k])?$textarea[$k]:'';
            $comment->imgs = isset($imgs[$k])?json_encode($imgs[$k]):'';
            $comment->member_id = Yii::$app->user->id;
            $comment->create_time = date("Y-m-d H:i:s");
            $comment->spzl = isset($spzl[$k])?$spzl[$k]:0;
            $comment->shfw = isset($shfw[$k])?$shfw[$k]:0;
            $comment->save();
        }
        IntergralOrder::updateAll(['order_state' => 4], ['orderid' => $data['orderid']]);
        return json_encode($json);
    }

    /**
     * Notes: 积分订单评价 post 提交
     * User: qin
     * Date: 2019/6/5
     * Time: 18:14
     */
    public function actionEvaluatePostMore()
    {
        $json = array('code' => 1, 'msg' => '评论成功');
        $data = Yii::$app->request->post();
        $imgs = isset($data['img'])?$data['img']:[];
        $spzl = $data['spzl'];
        $shfw = $data['shfw'];
        $textarea = $data['textarea'];
        $order_goods = OrderGoods::find()->andWhere(['order_id' => $data['orderid']])->all();
        foreach ($order_goods as $k => $v) {
            $comment = new  Comment();
            $comment->order_id = $data['orderid'];
            $comment->goods_id = $v->goods_id;
            $comment->content = isset($textarea[$k])?$textarea[$k]:'';
            $comment->imgs = isset($imgs[$k])?json_encode($imgs[$k]):'';
            $comment->member_id = Yii::$app->user->id;
            $comment->create_time = date("Y-m-d H:i:s");
            $comment->spzl = isset($spzl[$k])?$spzl[$k]:0;
            $comment->shfw = isset($shfw[$k])?$shfw[$k]:0;
            $comment->save();
        }
        Order::updateAll(['order_state' => 4], ['orderid' => $data['orderid']]);
        return json_encode($json);
    }
    /**
     * Notes: 评价成功页面
     * User: qin
     * Date: 2019/6/6
     * Time: 11:21
     */
    public function actionEvaluateSuccess()
    {
        //热门推荐
        $goods = Goods::find()->limit(6)->all();

        return $this->render('evaluate-success',[
            'goods'=>$goods
        ]);
    }


    /**
     * Notes:用户评论列表
     * User: qin
     * Date: 2019/6/6
     * Time: 11:11
     */
    public function actionEvaluateList()
    {

        $data=Comment::find()->andFilterWhere(['member_id' => Yii::$app->user->id]);

        $paixu='create_time DESC';
        //SORT_DESC  SORT_ASC
        $page = $this->getPagedRows($data, ['order' => $paixu, 'pageSize' => '6', 'rows' => 'models']);

        if (isset($_GET['per-page'])){
            $list=array();
            foreach ($page['models'] as $item){
                $orderLength = strlen($item->order_id);
                if ($orderLength >16){
                    $goods = Goods::findOne($item->goods_id);
                    $jsonlist['spzl'] = $item->spzl;
                    $jsonlist['shfw'] = $item->shfw;
                    $jsonlist['content'] = $item->content;
                    $jsonlist['create_time'] = date('Y-m-d',strtotime($item->create_time));
                    $jsonlist['imgs'] =empty($item->imgs)?'':json_decode($item->imgs);
                    $jsonlist['goodsId'] =$goods->id;
                    $jsonlist['goodsPrice'] =$goods->goods_price;
                    $jsonlist['goodPic'] =$goods->goods_pic;
                    $jsonlist['goodBrief'] =$goods->mobile_body;
                    $jsonlist['goods_name'] =$goods->goods_name;
                    $jsonlist['is_intergral_goods'] =0;
                }else{
                    $goods = IntergralGoods::findOne($item->goods_id);
                    $jsonlist['spzl'] = $item->spzl;
                    $jsonlist['shfw'] = $item->shfw;
                    $jsonlist['content'] = $item->content;
                    $jsonlist['create_time'] = date('Y-m-d',strtotime($item->create_time));
                    $jsonlist['imgs'] =empty($item->imgs)?'':json_decode($item->imgs);
                    $jsonlist['goodsId'] =$goods->id;
                    $jsonlist['goodsPrice'] =intval($goods->goods_integral);
                    $jsonlist['goodPic'] =$goods->goods_pic;
                    $jsonlist['goodBrief'] =$goods->mobile_body;
                    $jsonlist['goods_name'] =$goods->goods_name;
                    $jsonlist['is_intergral_goods'] =1;
                }
                $list[]=$jsonlist;
            }
            $json=array('code'=>1,'pagecount'=>$page['pages']->totalCount,'list'=>$list);

            echo json_encode($json);yii::$app->end();
        }
        $cityId = $this->getRequestCity();

        $comentpic = Advertising::find()->where(['province_id'=>$cityId,'adid'=>12])->orderBy('id DESC')->one();
        return $this->render('evaluate-list',[
            'list' => $page['models'],
            'pages' => $page['pages'],
            'comentpic' =>$comentpic
        ]);
    }

    /**
     * Notes: 修改密码
     * User: qin
     * Date: 2019/6/11
     * Time: 16:09
     */
    public function actionAlterPwd(){

        if (Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
            $model=Member::findOne(yii::$app->user->id);
            $validate =password_verify($post['originPwd'],$model->password);
            if ($validate){
                $model->password= Yii::$app->security->generatePasswordHash($post['newPwd']);
                if ($model->save()){
                    $json=array('code'=>1,'msg'=>'修改成功!');
                }else{
                    $json=array('code'=>0,'msg'=>'修改失败!');
                }
            }else{
                $json=array('code'=>0,'msg'=>'原密码错误!');
            }
            echo json_encode($json);yii::$app->end();
        }

        return $this->render('alter-pwd');
    }

    /**
     * Notes: 绑定手机号
     * User: qin
     * Date: 2019/6/11
     * Time: 16:09
     */
    public function actionBindMobile(){

        if (Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
            $mobileSms = Yii::$app->session->get('SMS_CODE');

            $mobile_sms = $post['mobile'].'-'.$post['getYzm'];
            $member = Member::findOne(Yii::$app->user->id);
            if ($mobile_sms == $mobileSms['value_str']) {
                $member->mobile = $post['mobile'];
                if ($member->save()) {
                    $json=array('code'=>1,'msg'=>'修改成功!');
                }else{
                    $json=array('code'=>1,'msg'=>'修改失败!');
                }
            }else{
                $json=array('code'=>0,'msg'=>'验证码错误!');
            }
            return json_encode($json);
        }

        return $this->render('bind-mobile');
    }

    /**
     * Notes: 初始化设置手机号
     * @author daorli
     *
     * Date: 2019/6/24
     */
    public function actionSetMobile()
    {
        if (Yii::$app->user->isGuest) {
            stopFlow("您尚未登录");
        }

        $member = Member::findOne(Yii::$app->user->id);
        if (!preg_match('/^at_/', $member->username )) {
            stopFlow("您暂时不可以设置哦");
        }

        if (Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
            $mobileSms = Yii::$app->session->get('SMS_CODE');

            $mobile_sms = $post['mobile'].'-'.$post['getYzm'];
            $member = Member::findOne(Yii::$app->user->id);
            if ($mobile_sms == $mobileSms['value_str']) {
                $member->mobile = $post['mobile'];
                $member->username = $post['mobile'];
                if ($member->save()) {
                    $json=array('code'=>1,'msg'=>'修改成功!');
                }else{
                    $json=array('code'=>1,'msg'=>'修改失败!');
                }
            }else{
                $json=array('code'=>0,'msg'=>'验证码错误!');
            }
            return json_encode($json);
        }

        return $this->render('set-mobile');
    }

    /**
     * Notes: 查询绑定的手机号是否存在
     * User: qin
     * Date: 2019/6/11
     * Time: 17:29
     */
    public function  actionIsMobile(){

        $post = Yii::$app->request->post();

        $member = Member::find()->andWhere(['mobile'=>$post['mobile']])->one();
        if (empty($member)){
            $json=array('code'=>1,'msg'=>'可以绑定!');
        }else{
            $json=array('code'=>0,'msg'=>'已经存在!');
        }
        return json_encode($json);
    }

    /**
     * Notes: 修改用户昵称
     * User: qin
     * Date: 2019/6/11
     * Time: 18:27
     */
    public  function actionAlterNickname(){


        if (Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
            $model=Member::findOne(yii::$app->user->id);
                $model->nickname= $post['nickname'];
                if ($model->save()){
                    $json=array('code'=>1,'msg'=>'修改成功!');
                }else{
                    $json=array('code'=>0,'msg'=>'修改失败!');
                }
            echo json_encode($json);yii::$app->end();
        }

     return $this->render('alter-nickname');
    }
    /**
     * Notes: 修改用户性别
     * User: qin
     * Date: 2019/6/11
     * Time: 18:27
     */
    public  function actionAlterSex(){

        if (Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
            $model=Member::findOne(yii::$app->user->id);
            $model->sex= $post['sex'];
            if ($model->save()){
                $json=array('code'=>1,'msg'=>'修改成功!');
            }else{
                $json=array('code'=>0,'msg'=>'修改失败!');
            }
            echo json_encode($json);yii::$app->end();
        }

    }
    /**
     * Notes: 修改用户生日
     * User: qin
     * Date: 2019/6/11
     * Time: 18:27
     */
    public  function actionAlterBirthday(){

        if (Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
            $model=Member::findOne(yii::$app->user->id);
            $model->birthday_year= $post['birthday'];
            if ($model->save()){
                $json=array('code'=>1,'msg'=>'修改成功!');
            }else{
                $json=array('code'=>0,'msg'=>'修改失败!');
            }
            echo json_encode($json);yii::$app->end();
        }

    }
    /**
     * Notes: 修改用户头像
     * User: qin
     * Date: 2019/6/11
     * Time: 18:27
     */
    public  function actionAlterAvatar(){

        if (Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
            $model=Member::findOne(yii::$app->user->id);
            $model->avatar= $post['avatar'];
            $model->avatarTm= $post['avatar'];
            if ($model->save()){
                $json=array('code'=>1,'msg'=>'修改成功!');
            }else{
                $json=array('code'=>0,'msg'=>'修改失败!');
            }
            echo json_encode($json);yii::$app->end();
        }
    }

    /**
     * Notes:我的足迹
     * User: qin
     * Date: 2019/6/4
     * Time: 11:19
     * @return string
     */
    public function actionVisit()
    {
        $model = Member::findOne(yii::$app->user->id);

        return $this->render('visit', [
            'model' => $model,
        ]);

    }

    /**
     * Notes: 删除收藏
     * User: qin
     * Date: 2019/6/4
     * Time: 11:30
     * @throws \yii\base\ExitException
     */
    public function actionDelcollect()
    {
        $json = array('code' => 0, 'msg' => '参数错误');
        if (yii::$app->request->isPost) {

        }
        echo json_encode($json);
        yii::$app->end();
    }

    public function getPagedRows($query, $config = [])
    {
        $countQuery = clone $query;

        $pages = new Pagination(['totalCount' => $countQuery->count()]);

        if (isset($config['pageSize'])) {
            $pages->setPageSize($config['pageSize'], true);
        }

        $rows = $query->offset($pages->offset)->limit($pages->limit);
        if (isset($config['order'])) {
            $rows = $rows->orderBy($config['order']);
        }
        $rows = $rows->all();

        $rowsLable = 'rows';
        $pagesLable = 'pages';

        if (isset($config['rows'])) {
            $rowsLable = $config['rows'];
        }
        if (isset($config['pages'])) {
            $pagesLable = $config['pages'];
        }

        $ret = [];
        $ret[$rowsLable] = $rows;
        $ret[$pagesLable] = $pages;

        return $ret;
    }

    /**
     * 上传评价照片
     */
    public function actionUploadimgv()
    {
        header("Access-Control-Allow-Origin:*");
        $json = array('code' => 0, 'error' => '参数错误');

        if (Yii::$app->request->isPost) {

            $uploadedFile = new UploadForm();

            $uploadedFile->file = UploadedFile::getInstanceByName("file");

            if ($uploadedFile->file && $uploadedFile->validate()) {

                $pieces = explode("/", $uploadedFile->file->type);
                $filename = $uploadedFile->createImagePathWithExtension($uploadedFile->file->extension, '/uploads/comment/');
//                $filename=$uploadedFile->createImagePathMD5($uploadedFile->file->extension,'/uploads/goods/'.$store_id.'/',$uploadedFile->file->tempName);
                $uploadedFile->saveImage($uploadedFile->file, $filename);

                $json = array(
                    'code' => 0,
                    'url' => yii::$app->request->hostInfo . $filename,
                    'attachment' => $filename
                );
            } else {
                $json = array('code' => 1, 'msg' => '图片保存失败');

            }

        } else {
            $json = array('code' => 1, 'msg' => "上传失败");
        }

        echo json_encode($json);
        yii::$app->end();
    }
}
