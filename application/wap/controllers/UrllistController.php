<?php


namespace wap\controllers;

use yii;
use yii\helpers\Url;
use yii\web\Controller;

class UrllistController extends Controller
{
    public function actionIndex(){
        $list = [];
        $list[]=array(
            'name'=>'首页',
            'url'=>yii::$app->request->hostInfo."或".'/',
        );
        $list[]=array(
            'name'=>'家博会页',
            'url'=>Url::toRoute(['jz/index','ticketId'=>'']),
        );

        $list[]=array(
            'name'=>'登录页',
            'url'=>Url::toRoute(['member/login']),
        );

        $list[]=array(
            'name'=>'注册页',
            'url'=>Url::toRoute(['member/reg']),
        );
        $list[]=array(
            'name'=>'会员中心',
            'url'=>Url::toRoute(['member/index']),
        );

        $list[]=array(
            'name'=>'积分商城',
            'url'=>Url::toRoute(['integral/index']),
        );

        $list[]=array(
            'name'=>'商品列表',
            'url'=>Url::toRoute(['goods/lists']),
        );

        $list[]=array(
            'name'=>'商品详情',
            'url'=>Url::toRoute(['goods/detail','id'=>'']),
        );
        $list[]=array(
            'name'=>'索赔页面',
            'url'=>Url::toRoute(['jz/ask','ticketId'=>'']),
        );
        $list[]=array(
            'name'=>'装修学堂',
            'url'=>Url::toRoute(['fitup-school/index']),
        );
        /*$list[]=array(
            'name'=>'支付回调',
            'url'=>Url::toRoute(['payment/notify']),
        );*/

        echo json_encode($list);exit;
    }
}