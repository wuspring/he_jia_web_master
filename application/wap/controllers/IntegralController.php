<?php

namespace wap\controllers;

use common\models\NavsWap;
use DL\service\IntergralGoodsService;
use DL\vendor\ConfigService;
use wap\logic\AdvertisingLogic;
use wap\models\Comment;
use wap\models\IntergralGoods;
use wap\models\IntergralGoodsClass;

class IntegralController extends BaseController
{
    /**
     * 积分首页
     * @author wufeng
     * @date 2019-6-13
     * @return string
     */
    public function actionIndex()
    {
        $banners = ConfigService::init('INDEX_INTERGRAL_SHOP')->get('int_banner');
        $banners = explode(',',$banners);
        $cityId = $this->getRequestCity();
        // 积分商城 菜单下广告
        $navNextAd = AdvertisingLogic::adList(5,$cityId);
        // 分类
        $cats = IntergralGoodsClass::find()->where(['fid'=>0,'is_del'=>0])->orderBy(['sort'=>SORT_DESC])->all();
        $cats = arrayGroupsAction($cats,function ($item){
            $info = $item->attributes;
            $goodsList = IntergralGoods::find()->where([
                'gc_id'=> $item->id,
                'goods_state'=>1,
                'isdelete'=>0
            ])->orderBy(['goods_hot'=>SORT_DESC,'goods_click'=>SORT_DESC])->limit(3)->all();
            $info['goods'] = arrayGroupsAction($goodsList,function ($good){
                $goodsInfo = $good->attributes;
                return (object)$goodsInfo;
            });
            return (object)$info;
        });
        // 热门商品
        $hots_goods = IntergralGoods::find()->where([
            'goods_hot'=>1,
            'goods_state'=>1,
            'isdelete'=>0
        ])->orderBy(['goods_click'=>SORT_DESC])->limit(3)->all();
        // 菜单
        $menus = NavsWap::find()->where(['provinces_id'=>$cityId,'status'=>1,'type'=>'home'])->limit(5)->orderBy('sort ASC')->all();

        return $this->render('index',[
            'banners' => $banners,
            'navNextAd' => $navNextAd,
            'floors' => $cats,
            'hots_goods' => $hots_goods,
            'menus' => $menus,
        ]);
    }

    /**
     * 积分商品详情
     * @author wufeng
     * @date 2019-6-13
     * @param $id
     * @return string
     * @throws \Exception
     */
    public function actionGoodsDetail($id){
        $goodDetail = IntergralGoodsService::detail($id);
        $where = [
            'and',
            ['=','goods_id',$id],
            ['like','order_id','8%', false]
        ];
        $qualityScore = Comment::find()->andFilterWhere($where)->average('spzl');
        $serviceScore = Comment::find()->andFilterWhere($where)->average('shfw');
        return $this->render('goods_detail',[
            'goods' => $goodDetail,
            'qualityScore' => $qualityScore,
            'serviceScore' => $serviceScore,
        ]);
    }

    /**
     * 积分商品-评论列表
     * @author wufeng
     * @date 2019-6-13
     * @param $id
     * @return string
     * @throws \Exception
     */
    public function actionCommentList(){
        if(\Yii::$app->request->isPost){
            $goodsId = \Yii::$app->request->post('goodsId',0);
            $page = \Yii::$app->request->post('page',1);
            $size = \Yii::$app->request->post('size',10);

            $where = [
                'and',
                ['=','goods_id',$goodsId],
                ['like','order_id','8%', false]
            ];
            $commentList = Comment::find()->where($where)->offset(($page-1)*$size)->limit($size)->all();

            $list = [];
            if(!empty($commentList)){
                foreach ($commentList as $key=>$item){
                    $arr = [];
                    $arr = $item->attributes;
                    $arr['avatar'] = $item->member->avatar;
                    $arr['nickname'] = $item->member->nickname;
                    $arr['imgs'] = json_decode($item->imgs,true);
                    $arr['createDate'] = date('Y-m-d',strtotime($item->create_time));
                    $list[] = $arr;
                }
            }
            $totalCount = Comment::find()->where($where)->count();
            apiSendSuccess('ok',[
                'list'=> $list,
                'totalCount'=> $totalCount,
            ]);
        }
    }

}
