<?php
/**
 * Created by PhpStorm.
 * User: 何孝林
 * Date: 2018/7/10
 * Time: 16:02
 */

namespace wap\controllers;

use common\models\MemberForm;
use common\wxpay\JsApiPay;
use DL\Project\CityTicket;
use DL\Project\Sms;
use DL\service\UrlService;
use wap\logic\StoreLogic;
use wap\logic\ThirdResourceLoginLogic;
use wap\models\Goods;
use wap\models\GoodsCoupon;
use wap\models\Member;
use wap\models\MemberStoreCoupon;
use wap\models\MemberTicketStore;
use wap\models\Order;
use wap\models\OrderGoods;
use wap\models\Ticket;
use wap\models\TicketApply;
use wap\models\TicketApplyGoods;
use wap\models\TicketAsk;
use wap\models\User;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class ApiController extends BaseController
{
    public function actionGetCode(){
        $json=array('state'=>0,'msg'=>'参数错误');
        if (Yii::$app->request->isPost) {
            $post=Yii::$app->request->post();
            $cityId = $this->getRequestCity();
            $code=createRandKey(6);
            $time= date('Y-m-d H:i:s');
           // $response=true;
            $response = \DL\Project\Sms::init()->code($post['mobile'], $code, $cityId);
            if ($response){
                $session = \Yii::$app->session;
                $session->set('SMS_CODE', [
                    'value_str' => "{$post['mobile']}-{$code}",
                    'value' => $code,
                    'expire' => time() + 60 * 30   // 有效期30分钟
                ]);
                $json=array(
                	'status'=>1,
                    'state'=>1,
                    'value_str' => "{$post['mobile']}-{$code}",
                    'msg'=>'短信发送成功!',
                    'code'=>$code,
                    'time'=>$time,
                );
            }else{
                $json=array(
                	'status'=>0,
                    'state'=>0,
                    'msg'=>'短信发送失败!',
                );
            }

        }
        echo json_encode($json);yii::$app->end();
    }

    public function actionSmscode(){
        $json=array('state'=>0,'msg'=>'参数错误');
        if (Yii::$app->request->isPost) {
            $post=Yii::$app->request->post();
            $cityId = $this->getRequestCity();
            $code=createRandKey(6);
            $time= date('Y-m-d H:i:s');
           // $response=true;
            $response = \DL\Project\Sms::init()->code($post['mobile'], $code, $cityId);
            if ($response){
                $session = \Yii::$app->session;
                $session->set('SMS_CODE', [
                    'value_str' => "{$post['mobile']}-{$code}",
                    'value' => $code,
                    'expire' => time() + 60 * 30   // 有效期30分钟
                ]);
                $json=array(
                    'state'=>1,
                    'value_str' => "{$post['mobile']}-{$code}",
                    'msg'=>'短信发送成功!',
                    'code'=>$code,
                    'time'=>$time,
                );
            }else{
                $json=array(
                    'state'=>0,
                    'msg'=>'短信发送失败!',
                );
            }

        }
        echo json_encode($json);yii::$app->end();
    }

    /**
     * 通用注册入口
     * @author daorli
     * @date 2019-6-19
     */
    public function actionAutoSign()
    {
        $mobile = Yii::$app->request->post('mobile', '');
        $code = Yii::$app->request->post('code', '###');

        $session = \Yii::$app->session;
        $smsCode = $session->get('SMS_CODE');
        $smsCode or apiSendError("验证码无效", 'index/index');
        $smsCode['expire'] > time()  or apiSendError("验证码已过期", 'index/index');
        "{$code}" == $smsCode['value'] or apiSendError("请输入正确的验证码", 'index/index');

        // 查询并注册手机号为新用户
        $member = Member::findOne([
            'mobile' => $mobile
        ]);

        $initMember = false;
        // 用户不存在 自动注册
        if (!$member) {
            $newPwd = createRandKey();
            // 新用户注册
            $member = new Member();
            $member->username = $member->getNewUserName();
            $member->mobile = $mobile;
            $member->wxnick = '用户_' . createRandKey();
            $member->nickname = $member->wxnick;
            $member->setPassword($newPwd);
            $member->status = (string)$member::STATUS_ACTIVE;
            $member->save($newPwd);
            if(!empty($session->get('channel'))){
                $member->channel = $session->get('channel');
            }
            $initMember = $newPwd;
            $session->set('INIT_PWD', $newPwd);
        }

        // 自动登录
        $memberForm = new MemberForm();
        $memberForm->wxlogin($member->id);

        $cityId = (int)Yii::$app->request->post('cityId', 0);
        if ($cityId) {
            $type = Yii::$app->request->post('type', '###');
            $tickets = CityTicket::init($cityId)->currentTickets($type);
            if (isset($tickets[$type]) and $tickets[$type]) {
                $ticketAsk = TicketAsk::findOne([
                    'mobile' => $member->mobile,
                    'ticket_id' => $tickets[$type]->id
                ]);

                if (!$ticketAsk) {
                    $ticketAsk = new TicketAsk();
                    $ticketAsk->name = Yii::$app->request->post('name', $member->nickname);
                    $ticketAsk->provinces_id = $cityId;
                    $ticketAsk->ticket_id = $tickets[$type]->id;
                    $ticketAsk->ticket_amount = 1;
                    $ticketAsk->mobile = $member->mobile;
                    $ticketAsk->resource = '默认';
                    $ticketAsk->address = Yii::$app->request->post('address', '');
                    $ticketAsk->status = 0;
                    $ticketAsk->create_time = date('Y-m-d H:i:s');
                    $ticketAsk->save();
                }
            }
        }

        $href = Yii::$app->request->post('location', '');
        if (strlen($href)) {
            if ($initMember) {
                if (strpos($href, '?') > -1 ) {
                    $href .= "&new_member=1";
                } else {
                    $href .= "?new_member=1";
                }
            }

            $url = urldecode($href);
            header("Location: {$url}");
        }

        die();
    }
    /**
     * 店铺预约
     *
     * @author daorli
     * @date 2019-6-6
     */
    public function actionOrderStore()
    {
        try {
            $storeId = Yii::$app->request->get('storeId', 0);
            $ticketId = Yii::$app->request->get('ticketId', 0);

            $ticket = Ticket::findOne($ticketId);
            $ticket or apiSendError("未找到有效展会信息");
            $ticket->status or apiSendError("展会已结束");
            $store = StoreLogic::init()->info($storeId);

            if (Yii::$app->user->isGuest) {
                $storeShops = StoreLogic::init()->shops($store->user_id);
                $storeShopsAmount = count($storeShops);

                $storeApply = TicketApply::findOne([
                    'ticket_id' => $ticket->id,
                    'user_id' => $store->user_id,
                    'status' => 1
                ]);

                $location = UrlService::build(['order-store', 'storeId' => $storeId, 'ticketId' => $ticketId]);
                $tmp = $storeApply ? 'store-join' : 'store-unjoin';

                return $this->render($tmp, [
                    'store' => $store,
                    'ticket' => $ticket,
                    'storeApply' => $storeApply,
                    'storeShops' => $storeShops,
                    'storeShopsAmount' => $storeShopsAmount,
                    'location' => $location
                ]);
            }

            $memberInfo = Yii::$app->user->identity;
            $log = MemberTicketStore::findOne([
                'ticket_id' => $ticket->id,
                'store_id' => $store->user_id,
                'member_id' => $memberInfo->id
            ]);
            $log and apiSendError("您已预约该店铺");

            $log = new MemberTicketStore();
            $log->member_id = $memberInfo->id;
            $log->ticket_id = $ticket->id;
            $log->store_id = $store->user_id;

            if ($log->save()) {
                \DL\Project\Sms::init()->storeHuoDongYuYue($memberInfo->mobile, $store->user_id, $ticket->id);

                $newMemberStatus = \Yii::$app->request->get('new_member', 0);
                if ($newMemberStatus) {
                    $session = \Yii::$app->session;
                    $initPwd = $session->get('INIT_PWD');
                    \DL\Project\Sms::init()->zhuCeHuiYuan($memberInfo->id, $initPwd);
                }

                apiSendSuccess("预约成功", [
                    'href' => UrlService::build(['order-store-success', 'storeId' => $storeId, 'ticketId' => $ticketId])
                ]);
            }

            apiSendError("预约失败");
        } catch (\Exception $e) {
            apiSendError($e->getMessage());
        }
    }

    /**
     * 店铺预约
     *
     * @author daorli
     * @date 2019-6-6
     */
    public function actionOrderStoreSuccess()
    {
        try {
            $storeId = Yii::$app->request->get('storeId', 0);
            $ticketId = Yii::$app->request->get('ticketId', 0);

            $ticket = Ticket::findOne($ticketId);
            $ticket or apiSendError("未找到有效展会信息");
            $ticket->status or apiSendError("展会已结束");

            $store = StoreLogic::init()->info($storeId);
            $storeApply = TicketApply::findOne([
                'ticket_id' => $ticket->id,
                'user_id' => $store->user_id,
                'status' => 1
            ]);

            $tmp = $storeApply ? 'store-join-success' : 'store-unjoin-success';
            $tmp = 0 ? 'store-join-success' : 'store-unjoin-success';

            $storeShops = StoreLogic::init()->shops($store->user_id);
            $storeShopsAmount = count($storeShops);

            return $this->render($tmp, [
                'store' => $store,
                'ticket' => $ticket,
                'storeApply' => $storeApply,
                'storeShopsAmount' => $storeShopsAmount
            ]);
        } catch (\Exception $e) {
            apiSendError($e->getMessage());
        }
    }

    /**
     * 爆款预约弹框
     * @author daorli
     * @date 2019-6-10
     */
    public function actionOrderGoodsPrice()
    {
        $goodId = Yii::$app->request->get('goodsId', 0);
        $ticketId = Yii::$app->request->get('ticketId', 0);

        $source = Yii::$app->request->get('source', 0);

        $ticket = Ticket::findOne($ticketId);
        $ticket or apiSendError("未找到有效展会信息");
        $ticket->status or apiSendError("展会已结束");

        $good = Goods::findOne([
            'id' => $goodId,
            'goods_state' => 1,
            'isdelete' => 0,
        ]);
        $good or apiSendError("该商品已下架");
        $applyInfo = TicketApplyGoods::findOne([
            'good_id' => $good->id,
            'ticket_id' => $ticket->id
        ]);

        if (Yii::$app->user->isGuest) {
            $location = UrlService::build(['order/create-price', 'goodsId' => $good->id, 'ticketId' => $ticketId, 'source' => $source]);
            return $this->render('order-goods-price', [
                'ticket' => $ticket,
                'applyInfo' => $applyInfo,
                'location' => $location,
                'good' => $good
            ]);
        }

//        $memberInfo = Yii::$app->user->identity;
        header('Location: ' . UrlService::build(['order/create-price', 'goodsId' => $goodId, 'ticketId' => $ticketId, 'source' => $source]));
        die();
//        $applyInfo or apiSendError("商品未参加本次活动");
    }


    /**
     * 爆款预约弹框
     * @author daorli
     * @date 2019-6-13
     *
     * @return string
     */
    public function actionOrderGoodsPriceSuccess()
    {
        $key = Yii::$app->request->get('key', 0);
        $source = Yii::$app->request->get('source', 0);
        $orderId = strrev($key);

        $order = Order::findOne(['orderid' => $orderId]);
        $order or apiSendError("未找到有效的订单信息");

        $orderGood = OrderGoods::findOne(['order_id' => $order->orderid]);
        $ticket = Ticket::findOne((int)$order->ticket_id);

        $ticketApplyGoodsInfo = TicketApplyGoods::findOne([
            'ticket_id' => $order->ticket_id,
            'good_id' => $orderGood->goods_id
        ]);

        return $this->render('order-goods-price-success', [
            'ticket' => $ticket,
            'order' => $order,
            'source' => $source,
            'orderGood' => $orderGood,
            'ticketApplyGoodsInfo' => $ticketApplyGoodsInfo
        ]);
    }

    /**
     * 领取优惠券
     *
     * @author daorli
     * @date 2019-6-10
     */
    public function actionGetCoupon()
    {
        $couponId = Yii::$app->request->get('couponId', 0);
        $ticketId = Yii::$app->request->get('ticketId', 0);

        $goodCoupon = GoodsCoupon::findOne([
            'id' => $couponId,
            'is_del' => 0,
        ]);
        $goodCoupon or apiSendError("未找到有效的优惠券");

        $store = $goodCoupon->getUser();

        if (Yii::$app->user->isGuest) {
            $ticket = Ticket::findOne($ticketId);
            $storeApply = false;
            if ($ticket) {
                $storeApply = TicketApply::findOne([
                    'ticket_id' => $ticket->id,
                    'user_id' => $store->id,
                    'status' => 1
                ]);
            }

            $storeShops = StoreLogic::init()->shops($store->id);
            $storeShopsAmount = count($storeShops);
            $location = UrlService::build(['get-coupon', 'couponId' => $couponId, 'ticketId' => $ticketId]);
            return $this->render('get-store-coupon', [
                'store' => $store,
                'ticket' => $ticket,
                'storeApply' => $storeApply,
                'location' => $location,
                'storeShopsAmount' => $storeShopsAmount,
                'goodCoupon' => $goodCoupon
            ]);
        }

        $memberInfo = Yii::$app->user->identity;
        $memberStore = MemberStoreCoupon::findOne(['coupon_id' =>$couponId, 'member_id'=>$memberInfo->id]);
        $memberStore and apiSendError("您已领取过该优惠券");

        $goodCoupon->num += 1;
        $goodCoupon->save();

        $user = User::findOne($goodCoupon->user_id);
        $user and $user->refreshCache();

        $memberStoreCoupon = new MemberStoreCoupon();
        $memberStoreCoupon->store_id = $goodCoupon->user_id;
        $memberStoreCoupon->member_id = $memberInfo->id;
        $memberStoreCoupon->coupon_id = $goodCoupon->id;
        $memberStoreCoupon->addtime = date('Y-m-d H:i:s');
        $memberStoreCoupon->type = 2;
        $memberStoreCoupon->price = $goodCoupon->money;
        if ($goodCoupon->type == 'ticket') {
            $ticket = Ticket::findOne($goodCoupon->ticket_id);
            $memberStoreCoupon->begin_time = $ticket->open_date;  //开始时间
            $memberStoreCoupon->end_time = date('Y-m-d H:i:s', strtotime($ticket->end_date) + 24 * 3600);  //结束时间
        } else {
            $addDay = '+' . $goodCoupon->valid_day . ' day';
            $endTime = date('Y-m-d H:i:s', strtotime($addDay));
            $memberStoreCoupon->begin_time = date('Y-m-d H:i:s');  //开始时间
            $memberStoreCoupon->end_time = $endTime;  //结束时间
        }

        $memberStoreCoupon->ticket_id = 0;  //结束时间
        $tickets = CityTicket::init($store->provinces_id)->currentFactionTickets();
        if ($tickets) {
            $ticketIds = array_keys($tickets);
            $applys = TicketApply::find()->where([
                'and',
                ['in', 'ticket_id', $ticketIds],
                ['=', 'user_id', $store->id],
                ['=', 'status', 1],
            ])->one();
            if ($applys) {
                $memberStoreCoupon->ticket_id = $applys->ticket_id;
            }
        }

        if ($memberStoreCoupon->create()) {
            Sms::init()->youHuiQuanYuYue($memberInfo->mobile, $memberStoreCoupon->coupon_id, $memberStoreCoupon->ticket_id);

            apiSendSuccess("领取成功", [
                'href' => UrlService::build(['get-coupon-success', 'couponId' => $memberStoreCoupon->id]),
                'couponId' => $couponId
            ]);
        }

        apiSendError("领取失败");

    }


    /**
     * 领取优惠券成功
     *
     * @author daorli
     * @date 2019-6-10
     */
    public function actionGetCouponSuccess()
    {
        $couponId = Yii::$app->request->get('couponId', 0);

        $mCoupon = MemberStoreCoupon::findOne([
            'id' => $couponId,
            'is_delete' => 0,
        ]);
        $mCoupon or apiSendError("未找到有效的优惠券");
        $goodCoupon = $mCoupon->getGoodsCoupon();

        $ticket = $mCoupon->getTicket();
        return $this->render('get-store-coupon-success', [
            'goodCoupon' => $goodCoupon,
            'mCoupon' => $mCoupon,
            'ticket' => $ticket
        ]);
    }

    public function actionGetOrderGoods()
    {
        $ticketId = \Yii::$app->request->get('ticketId', 0);
        $goodId = \Yii::$app->request->get('goodsId', 0);

        $ticket = Ticket::findOne($ticketId);
        $ticket or apiSendError("未找到有效展会信息");
        $ticket->status or apiSendError("展会已结束");

        $good = Goods::findOne([
            'id' => $goodId,
            'goods_state' => 1,
            'isdelete' => 0,
        ]);
        $good or apiSendError("该商品已下架");

        $ticketApplyGoods = TicketApplyGoods::findOne([
            'ticket_id' => $ticket->id,
            'good_id' => $good->id,
            'status' => 1,
        ]);
        $ticketApplyGoods or apiSendError("该商品未参加活动");
        $location = UrlService::build(['order/create', 'ticketId' => $ticketId, 'goodsId' => $goodId]);

        if (!Yii::$app->user->isGuest) {
            apiSendSuccess("ok", [
                'location' => $location
            ]);
        }

        return $this->render('get-order-goods', [
            'ticket' => $ticket,
            'good' => $good,
            'ticketApplyGoods' => $ticketApplyGoods,
            'location' => UrlService::build(['get-order-goods', 'ticketId' => $ticketId, 'goodsId' => $goodId])
        ]);
    }

    /**
     * 不加载main.php
     * @author daorli
     * @date 2019-6-6
     *
     * @param string $content
     *
     * @return string
     */
    public function renderContent($content)
    {
        return $content;
    }

    public function actionGetOpenid()
    {

        if (Yii::$app->user->isGuest) {
            die('not found');
        }
        $member = Member::findOne(Yii::$app->user->id);
        $openId = $member->wxopenid;
        if (!strlen($openId)) {
            $jsApi = new JsApiPay();
            $openId = $jsApi->GetOpenid();

            $member->wxopenid = $openId;
            $member->save();
        }

        apiSendSuccess("ok", [
            'code' => $openId
        ]);
    }

    /**
     * Notes:判断是否登录
     * User: qin
     * Date: 2019/6/12
     * Time: 18:22
     */
    public  function actionJudgeLogin(){

        if (Yii::$app->user->isGuest){
            $json = array('code'=>1,'msg'=>'请先登录');
        }else{
            $json = array('code'=>0,'msg'=>'已经登录');
        }
        return json_encode($json);
    }

    /**
     * 微信h5支付回调
     *
     * @author daorli
     * @date 2019-6-18
     */
    public function actionOrderStatus()
    {
        $number = Yii::$app->request->get('number', '###');
        $order = Order::findOne([
            'orderid' => $number
        ]);

        $order or stopFlow("未找到有效的订单");

        if ($order->order_state == \common\models\Order::STATUS_PAY) {
            header('Location: ' . UrlService::build(['payment/success', 'number' => $number]));
        } else {
            header('Location: ' . UrlService::build(['payment/pay-fail', 'number' => $number]));
        }

        die();
    }

    /**
     * 展会索票页刷新商品
     *
     * @author daorli
     * @date 2019-6-19
     *
     * @return string
     */
    public function actionReloadGoods()
    {
        $ticketId = Yii::$app->request->get('ticketId', 0);
        $type = Yii::$app->request->get('type', TicketApplyGoods::TYPE_COUPON);

        $ticket = Ticket::findOne($ticketId);

        $teJiaGoodsAmount = Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 'g.id=t.good_id')->andFilterWhere(['t.ticket_id'=>$ticketId,'t.type'=>TicketApplyGoods::TYPE_ORDER])->count();
        $baoKuanGoodAmount = Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 'g.id=t.good_id')->andFilterWhere(['t.ticket_id'=>$ticketId,'t.type'=>TicketApplyGoods::TYPE_COUPON])->count();
        $p1 = rand(1, intval($teJiaGoodsAmount / 6));
        $p2 = rand(1, intval($baoKuanGoodAmount / 6));

        //预存 享特价
        $teJiaGoods = Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 'g.id=t.good_id')->andFilterWhere(['t.ticket_id'=>$ticketId,'t.type'=>TicketApplyGoods::TYPE_ORDER])->select(['g.*', 't.good_amount'])->orderBy(' t.sort desc')->offset(6 *($p1-1))->limit(6)->all();

        //爆款预约
        $baoKuanGood = Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 'g.id=t.good_id')->andFilterWhere(['t.ticket_id'=>$ticketId,'t.type'=>TicketApplyGoods::TYPE_COUPON])->select(['g.*', 't.good_amount'])->orderBy(' t.sort desc')->offset(6 *($p2-1))->limit(6)->all();

        return $this->render('reload-goods', [
            'type' => $type,
            'ticket' => $ticket,
            'ticketId' => $ticketId,
            'teJiaGoods' => $teJiaGoods,
            'baoKuanGood' => $baoKuanGood
        ]);
    }

    /**
     * 微信快捷登陆
     * @author daorli
     * @date 2019-6-24
     */
    public function actionWechatLogin()
    {
        $member = ThirdResourceLoginLogic::wechatLogin();

        $member or stopFlow("用户登录失败");

        if (!strlen($member->mobile)) {
            header('Location: ' . UrlService::build('member/set-mobile'));
            die();
        }

        header('Location: ' . UrlService::build('index/index'));
    }

}