<?php

namespace wap\controllers;

use DL\vendor\ConfigService;
use wap\models\Advertising;
use wap\models\Config;
use wap\models\HelpCont;
use wap\models\IntegralLog;
use wap\models\IntergralGoods;
use wap\models\IntergralGoodsClass;
use wap\models\IntergralLuckDraw;
use wap\models\IntergralPrize;
use wap\models\IntergralScope;
use wap\models\IntergralTurntable;
use wap\models\Member;
use Yii;
use yii\data\Pagination;

class IntegralMenuController extends BaseController
{

    /**
     * Notes: 积分抽奖
     * User: qin
     * Date: 2019/6/13
     * Time: 14:08
     * @return string
     */
    public function actionLuckDraw()
    {

        if (\Yii::$app->user->isGuest){
            $playNum=0;
        }else{
            $playNum=($this->memberInfo->integral)/10;
        }
        //当前期
        $now_term=IntergralPrize::find()->andFilterWhere(['pid_terms'=>0])->orderBy('prize_terms DESC')->limit(1)->one();
        //当前期奖品
        $prize=IntergralPrize::find()->andFilterWhere(['pid_terms'=>$now_term->id])->orderBy('sort DESC')->limit(5)->all();

        //当前获奖名单
        $prizeArr=[1,2,3,5,6,8,9];          //获奖id
        $getPizeList=IntergralLuckDraw::find()->andFilterWhere(['prize_terms'=>$now_term->prize_terms])->andFilterWhere(['in','draw_id',$prizeArr])->all();


        return $this->render('luck-draw',[
            'playNum' =>intval($playNum),
            'now_term' =>$now_term,
            'prize' =>$prize,
            'getPizeList'=>$getPizeList
        ]);
    }

    /**
     * Notes:礼品
     * User: qin
     * Date: 2019/6/13
     * Time: 15:02
     */
    public function actionGift(){
        $getdata = Yii::$app->request->get();
        $get_gc_id =  isset($getdata['gc_id']) && $getdata['gc_id']>0?$getdata['gc_id']:'';
        $get_fw_id =  isset($getdata['fw_id'])  && $getdata['fw_id']>0?$getdata['fw_id']:'';
        $get_up_str =  isset($getdata['up_id'])?$getdata['up_id']:'md';
        // 默认为md,分值从高到低up,低到高down
        $data=IntergralGoods::find()->andFilterWhere(['gc_id'=>$get_gc_id]);
        if (!empty($get_fw_id)){
            $scope=IntergralScope::findOne($get_fw_id);
            $data->andWhere([
                'and',
                ['>=','goods_integral',$scope->min],
                ['<=','goods_integral',$scope->max]
                ]);
        }

        switch ($get_up_str){
            case 'md':
                $paixu='goods_addtime DESC';
                break;
            case 'down':
                $paixu='goods_integral DESC';
                break;
            case 'up':
                $paixu='goods_integral ASC';
                break;
        }

        //SORT_DESC  SORT_ASC
        $page = $this->getPagedRows($data, ['order' => $paixu, 'pageSize' => '6', 'rows' => 'models']);

        if (isset($_GET['per-page'])){
            $list=array();
            foreach ($page['models'] as $item){
                $jsonlist['id'] = $item->id;
                $jsonlist['pic'] = empty($item->goods_pic)?'/public/wap/images/loading.jpg':$item->goods_pic;
                $jsonlist['name'] = $item->goods_name;
                $jsonlist['price'] = empty($item->goods_integral)?'0':intval($item->goods_integral);
                $list[]=$jsonlist;
            }
            $json=array('code'=>1,'pagecount'=>$page['pages']->totalCount,'list'=>$list,'pages' => $page['pages'],'sort_str'=>$get_up_str);

            echo json_encode($json);yii::$app->end();
        }

        $gc_id = IntergralGoodsClass::find()->all();
        $scope = IntergralScope::find()->all();
        $gc_id_json = $this->getList($gc_id);
        $gc_id_json = array_merge([
            ['id'=>0,'value'=>'礼品分类']
        ],$gc_id_json);

        $scope_json = $this->getList($scope);
        //积分列表主图
        $indexPic = Advertising::find()->andWhere(['adid'=>10,'isShow'=>1])->one();
        //积分列表左右图
        $leftAndRight = Advertising::find()->andWhere(['adid'=>11,'isShow'=>1])->orderBy('sort DESC')->all();

        return $this->render('gift-list',[
            'gc_id'=>json_encode($gc_id_json),
            'get_gc_id'=>$get_gc_id,
            'scope'=>json_encode($scope_json),
            'indexPic'=>$indexPic,
            'sort_str' => $get_up_str,
            'leftAndRight'=>$leftAndRight,
            'list' => $page['models'],
            'pages' => $page['pages'],
        ]);

    }

    public  function getList($arr=[]){

        foreach ($arr as $k=>$v){
            $list[]=array(
                'id'=>$v->id,
                'value'=>$v->name,
            );
        }
        return $list;
    }

    /**
     * Notes: 我的积分
     * User: qin
     * Date: 2019/6/13
     * Time: 16:24
     */
    public function actionMyIntegral(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['index/login']);
        }
        $member = Member::findOne(Yii::$app->user->id);

        //轮播的广告位
        $advertisingWheel = Advertising::find()->andWhere(['adid'=>8,'isShow'=>1])->all();
        //左右的广告位 通过排序实现区分左右图
        $leftAndRight = Advertising::find()->andWhere(['adid'=>9,'isShow'=>1])->orderBy('sort DESC')->all();

        $intergralGood = IntergralGoods::find()->limit(3)->all();
        return $this->render('my-integral',[
            'member'=>$member,
            'intergralGood'=>$intergralGood,
            'advertisingWheel'=>$advertisingWheel,
            'leftAndRight' =>$leftAndRight
        ]);
    }

    /**
     * Notes:积分变更记录
     * User: qin
     * Date: 2019/6/13
     * Time: 16:39
     */
    public function actionIntegralRecord(){
        $member = Member::findOne(Yii::$app->user->id);
        $get = Yii::$app->request->get();
        $data=IntegralLog::find()->andFilterWhere(['memberId'=>Yii::$app->user->id]);
        $dateType = isset($get['dateType'])?$get['dateType']:1;
        $integralType = isset($get['integralType'])?$get['integralType']:'in';

        switch ($dateType){
            case 1:
                $start_time = mktime(0,0,0,date('m'),date('d'),date('Y'));
                $end_time = mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
                $start_time_date = date('Y-m-d H:i:s',$start_time);
                $end_time_date = date('Y-m-d H:i:s',$end_time);
                $data->andWhere([
                    'and',
                    ['>','create_time',$start_time_date],
                    ['<','create_time',$end_time_date]
                ]);

                break;
            case 2:
                $start_time = mktime(0,0,0,date('m'),date('d')-date('w')+1,date('Y'));
                $end_time = mktime(23,59,59,date('m'),date('d')-date('w')+7,date('Y'));
                $start_time_date = date('Y-m-d H:i:s',$start_time);
                $end_time_date = date('Y-m-d H:i:s',$end_time);
                $data->andWhere([
                    'and',
                    ['>','create_time',$start_time_date],
                    ['<','create_time',$end_time_date]
                ]);
                break;
            case 3:
                $start_time = mktime(0,0,0,date('m'),1,date('Y'));
                $end_time = mktime(23,59,59,date('m'),date('t'),date('Y'));
                $start_time_date = date('Y-m-d H:i:s',$start_time);
                $end_time_date = date('Y-m-d H:i:s',$end_time);
                $data->andWhere([
                    'and',
                    ['>','create_time',$start_time_date],
                    ['<','create_time',$end_time_date]
                ]);
                break;
            default:

        }
        if ($integralType == 'in'){
            $data->andWhere(['<=','pay_integral',0]);
        }else{
            $data->andWhere(['>=','pay_integral',0]);
        }

        $paixu='create_time DESC';
        //SORT_DESC  SORT_ASC
        $page = $this->getPagedRows($data, ['order' => $paixu, 'pageSize' => '6', 'rows' => 'models']);

        if (isset($_GET['per-page'])){
            $list=array();
            foreach ($page['models'] as $item){
                $jsonlist['id'] = $item->id;
                $jsonlist['create_time'] = $item->create_time;
                $jsonlist['pay_integral'] = $item->pay_integral;
                $jsonlist['expand'] = empty($item->expand)?'-':$item->expand;
                $jsonlist['orderId'] = $item->orderId;

                $list[]=$jsonlist;
            }

            // 获取积分
            $getIntegral = IntegralLog::find()->andWhere([
                'and',
                ['memberId'=>Yii::$app->user->id],
                ['<=','pay_integral',0],
                ['>','create_time',$start_time_date],
                ['<','create_time',$end_time_date]
            ])->sum('pay_integral');
            $getIntegral = abs($getIntegral);
            //使用积分
            $reduceIntegral = IntegralLog::find()->andWhere([
                'and',
                ['memberId'=>Yii::$app->user->id],
                ['>=','pay_integral',0],
                ['>','create_time',$start_time_date],
                ['<','create_time',$end_time_date]
            ])->sum('pay_integral');
            $reduceIntegral = $reduceIntegral?$reduceIntegral:0;

            $json=array(
                'code'=>1,
                'pagecount'=>$page['pages']->totalCount,
                'list'=>$list,
                'pages' => $page['pages'],
                'integralType'=>$integralType,
                'getIntegral'=>$getIntegral,
                'reduceIntegral'=>$reduceIntegral,
            );
            echo json_encode($json);yii::$app->end();
        }

        return $this->render('integral-record',[
            'member'=>$member,
            'list' => $page['models'],
            'pages' => $page['pages'],
            'integralType'=>$integralType
        ]);
    }

    /**
     * Notes:积分获取
     * User: qin
     * Date: 2019/6/13
     * Time: 18:58
     */
    public function actionIntegralGain(){

        $integralGain = HelpCont::findOne(18);
        return $this->render('integral-gain',['integralGain'=>$integralGain]);
    }

    /**
     * Notes:积分签到
     * User: qin
     * Date: 2019/6/13
     * Time: 18:58
     */
    public function actionIntegralSignin(){

        $member = Member::findOne(Yii::$app->user->id);

        $get = Yii::$app->request->get();
        $data=IntegralLog::find()->andFilterWhere(['memberId'=>Yii::$app->user->id,'type'=>IntegralLog::INTEGRALTYPE]);

        $paixu='create_time DESC';
        //SORT_DESC  SORT_ASC
        $page = $this->getPagedRows($data, ['order' => $paixu, 'pageSize' => '6', 'rows' => 'models']);

        if (isset($_GET['per-page'])){
            $list=array();
            foreach ($page['models'] as $item){
                $jsonlist['id'] = $item->id;
                $jsonlist['create_time'] = $item->create_time;
                $jsonlist['pay_integral'] = $item->pay_integral;
                $jsonlist['expand'] = empty($item->expand)?'-':$item->expand;
                $list[]=$jsonlist;
            }
            $json=array('code'=>1,'pagecount'=>$page['pages']->totalCount,'list'=>$list,'pages' => $page['pages']);

            echo json_encode($json);yii::$app->end();
        }

        $qianDao=IntegralLog::find()->andFilterWhere(['memberId'=>Yii::$app->user->id,'type'=>IntegralLog::INTEGRALTYPE])
                                    ->andWhere(['like','create_time',date('Y-m-d')])->one();

        //签到规则
        $text = Config::find()->where(['cKey'=>'INDEX_INTEGRAL'])->one();
        $model = json_decode($text->cValue);
        return $this->render('integral-signin',[
            'member'=>$member,
            'model' => $model,
            'list' => $page['models'],
            'pages' => $page['pages'],
            'qianDao'=>$qianDao
        ]);

    }

    /**
     * Notes: 点击签到
     * User: qin
     * Date: 2019/6/13
     * Time: 19:16
     */
    public function actionClickSignin(){

        $member = Member::findOne(Yii::$app->user->id);

        $integralLog = new  IntegralLog();
        $integralLog ->memberId = Yii::$app->user->id;
        $integralLog ->orderId = IntegralLog::ORDER_ID_LOGIN;

        $integral = ConfigService::init('INDEX_INTEGRAL')->get('sign_integral');
        $continuation = ConfigService::init('INDEX_INTEGRAL')->get('continuation');
        $time=date('Y-m-d',strtotime("-{$continuation} day"));
        $num=  IntegralLog::find()->where("memberId=". $member->id ." and DATE_FORMAT(create_time, '%Y%m%d')>=  '".$time."'  and  DATE_FORMAT(create_time, '%Y%m%d') <= DATE_FORMAT(now(), '%Y%m%d')")->count();
        $integralLog->expand = "签到获得{$integral}积分";
        if (($num+1)>= $continuation){
            $reward = ConfigService::init('INDEX_INTEGRAL')->get('reward');
            $integral += $reward;
            $integralLog->expand  .= "连续签到额外获得{$reward}积分";
        }
        $integralLog ->pay_integral = -(intval($integral));
        $integralLog ->old_integral = $member->integral;
        $integralLog ->now_integral = intval($member->integral)+$integral;
        $integralLog ->create_time = date('Y-m-d H:i:s');
        $integralLog ->type = IntegralLog::INTEGRALTYPE;
        $integralLog->save();
        $member->integral += $integral;
        if ($member->save()){
            $json=array('code'=>1,'msg'=>'签到成功');
        }else{
            $json=array('code'=>0,'msg'=>'签到失败');
        }
        return  json_encode($json);
    }


    /**
     * 积分大转盘__点击后可以旋转
     */
    public  function  actionDoaward()
    {
        $result=array('code'=>0,'msg'=>'存储异常');

        $now_prize=IntergralPrize::find()->andFilterWhere(['pid_terms'=>0])->orderBy('prize_terms DESC')->limit(1)->one();
        $now_time=date('Y-m-d H:i:s');
        if (\Yii::$app->user->isGuest){
            $result=array('code'=>0,'msg'=>'请先登录');
        }else{
            if ($now_prize->start_time<=$now_time && $now_time<=$now_prize->end_time){
                $member=Member::findOne(\Yii::$app->user->id);
                $member_interal=intval($member->integral);
                if ($member_interal>=10){
                    //更新积分
                    $member->integral-=10;
                    $member->save();

                    //控制转盘
                    $prize_arr =IntergralTurntable::find()->asArray()->all();
                    foreach ($prize_arr as $key => $val) {
                        $arr[$val['id']] = $val['v'];
                    }
                    $rid = $this->getRand($arr);   //根据概率获取奖项id
                    $res = $prize_arr[$rid - 1];  //中奖项

                    //积分数据变更记录
                    $this->InteralRelationData($res);

                    $result['code'] =1; //状态
                    $resNUm=($member_interal-10)/10;
                    $result['playNum'] =($member_interal-10>=10)?intval($resNUm):0; //次数
                    $result['angle'] =floatval($res['angle']); //随机生成一个角度
                    $result['prize'] = $res['prize'];
                    $result['msg'] ='抽奖成功';

                }else{
                    $result=array('code'=>0,'msg'=>'积分不足');
                }
            }else{
                $result=array('code'=>0,'msg'=>'本期已经结束');
            }

        }
        return json_encode($result);
    }

    /**
     * 积分相关表的数据变更
     */
    public  function InteralRelationData($res){

        $member=Member::findOne($this->memberInfo->id);
        //当前期数
        $now_prize=IntergralPrize::find()->orderBy('prize_terms DESC')->limit(1)->one();
        //点击抽奖记录
        $intergralLuckDraw=new IntergralLuckDraw();
        $orderNumber=$intergralLuckDraw->newOrderNumber;
        $intergralLuckDraw->member_id=$this->memberInfo->id;
        $intergralLuckDraw->prize_terms=$now_prize->prize_terms;      //获奖期限
        $intergralLuckDraw->draw_id=$res['id'];             //获奖id
        $intergralLuckDraw->create_time=date('Y-m-d H:i:s');
        $intergralLuckDraw->number=$orderNumber;
        $intergralLuckDraw->save();
        //更改获奖数据
        switch ($res['id']){
            case 1:  //特等奖
                IntergralTurntable::updateAll(['v'=>0],['id'=>1]);
                break;
            case 2;    //三等奖
                IntergralTurntable::updateAll(['v'=>0],['in','id',[2,8]]);
                break;
            case 3;
                IntergralTurntable::updateAll(['v'=>0],['in','id',[3,9]]);
                break;
            case 5;    //一等奖
                IntergralTurntable::updateAll(['v'=>0],['id'=>5]);
                break;
            case 6;  //二等奖
                IntergralTurntable::updateAll(['v'=>0],['id'=>6]);
                break;
            case 8;    //三等奖
                IntergralTurntable::updateAll(['v'=>0],['in','id',[2,8]]);
                break;
            case 9;
                IntergralTurntable::updateAll(['v'=>0],['in','id',[3,9]]);
                break;
            default:
                break;
        }
        //积分变更记录
        $integralLog=new IntegralLog();
        $integralLog->memberId=$this->memberInfo->id;
        $integralLog->orderId=$orderNumber;
        $integralLog->pay_integral=10;
        $integralLog->old_integral=intval($member->integral)+10;
        $integralLog->now_integral=$member->integral;
        $integralLog->expand='积分抽奖';
        $integralLog->create_time=date('Y-m-d H:i:s');
        $integralLog->save();

    }


    /**
     * @param $proArr
     * @return int|string
     *  中奖概率计算
     */
    public  function getRand($proArr) {
        $result = '';
        //概率数组的总概率精度
        $proSum = array_sum($proArr);
        //概率数组循环
        foreach ($proArr as $key => $proCur) {
            $randNum = mt_rand(1, $proSum);
            if ($randNum <= $proCur) {
                $result = $key;
                break;
            } else {
                $proSum -= $proCur;
            }
        }
        unset ($proArr);
        return $result;
    }

    /**
     * Notes:分页
     * User: qin
     * Date: 2019/6/13
     * Time: 9:09
     * @param $query
     * @param array $config
     * @return array
     */

    public function getPagedRows($query, $config = [])
    {
        $countQuery = clone $query;

        $pages = new Pagination(['totalCount' => $countQuery->count()]);

        if (isset($config['pageSize'])) {
            $pages->setPageSize($config['pageSize'], true);
        }

        $rows = $query->offset($pages->offset)->limit($pages->limit);
        if (isset($config['order'])) {
            $rows = $rows->orderBy($config['order']);
        }
        $rows = $rows->all();

        $rowsLable = 'rows';
        $pagesLable = 'pages';

        if (isset($config['rows'])) {
            $rowsLable = $config['rows'];
        }
        if (isset($config['pages'])) {
            $pagesLable = $config['pages'];
        }

        $ret = [];
        $ret[$rowsLable] = $rows;
        $ret[$pagesLable] = $pages;

        return $ret;
    }

}
