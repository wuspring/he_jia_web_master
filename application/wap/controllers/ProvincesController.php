<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/12
 * Time: 10:04
 */

namespace wap\controllers;

use wap\models\Provinces;
use yii\web\Controller;

class ProvincesController extends Controller
{
    public function actionIndex($pid=1){
        $list=$this->getList();

        return json_encode($list);
    }

    public function getList($pid=1){
        $data=Provinces::find()->where(['upid'=>$pid])->orderBy(" id asc")->all();
        $list=array();
        foreach ($data as $item){
            if ($item->level!=3){
                $list[]=array(
                    'id'=>$item->id,
                    'value'=>$item->cname,
                    'childs'=>$this->getList($item->id),
                );
            }else{
                $list[]=array(
                    'id'=>$item->id,
                    'value'=>$item->cname,
                );
            }

        }

        return $list;
    }
}