<?php
namespace wap\controllers;

use DL\service\UrlService;
use wap\logic\WePayLogic;
use wap\models\Config;
use wap\models\GoodsSku;
use wap\models\Member;
use wap\models\Money;
use wap\models\Order;
use wap\models\OrderGoods;
use wap\models\OrderReback;
use wap\models\Payment;
use wap\models\Recharge;
use wap\models\SystemCoupon;
use wap\models\TicketInfo;
use common\models\WxPayUnifiedOrder;
use common\wxpay\NativePay;
use DL\vendor\ConfigService;
use DL\WechatPay;
use DL\WechatRefund;


class PaymentController extends BaseController
{
    /**
     * 支付
     */
    public function actionPay()
    {
        $orderId = \Yii::$app->request->get('number', '###');

        $order = Order::findOne([
            'orderid' => $orderId,
            'order_state' => Order::STATUS_WAIT
        ]);

        $order or stopFlow("未找到待支付的订单");

        $order->pay_sn = $order->getNewPaySn();
        $order->save();

        if ($order->order_amount <= 0) {
            stopFlow("支付金额异常");
        }

        switch ($order->pay_method) {
            case $order::PAY_MECHOD_ACCOUNT :
                $this->payAccount($order);
                break;
            case $order::PAY_MECHOD_WECHAT :
                $payType = \Yii::$app->request->post('type', 'normal');
                if ($payType == 'WeiXin') {
                    $this->payWechatJs($order);
                } else {
                    $this->payWechat($order);
                }

                break;
            case $order::PAY_MECHOD_ALIPAY :
                $this->payAlipay($order);
                break;
            default:
                die('op, system error...');
        }
    }


    /**
     * 微信支付 - H5
     *
     * @param Order $order
     *
     * @return string|void
     */
    final function payWechat(Order $order)
    {
        $post = $_REQUEST;
        if ($post == null) {
            $post = file_get_contents("php://input");
        }

        if ($post == null) {
            $post = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
        }

        $member = Member::findOne($this->memberInfo->id);
//        if ($member->integral < $order->integral_amount) {
//            stopFlow("账户积分不足",'order/index');
//        }

        $notifyPath = ConfigService::init('weixin')->get('notifyUrl');

        $notify = new NativePay();
        $input = new WxPayUnifiedOrder();
        $input->SetBody('订单'.$order->orderid);
        $input->SetAttach($order->orderid);
        $input->SetOut_trade_no($order->pay_sn);
        $input->SetSpbill_create_ip(getClientIp());
        $money = (floatval($order->order_amount) + floatval($order->freight))*100;
        $input->SetTotal_fee($money);
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetGoods_tag($order->orderid);
        $input->SetNotify_url($notifyPath);
        $input->SetTrade_type("MWEB");
        $input->SetProduct_id($order->pay_sn);

        $result = $notify->GetPayUrl($input);

        $path = \Yii::$app->request->hostInfo . UrlService::build('member/my-reserve');
        $requestPath = "{$result['mweb_url']}&redirect_url={$path}";

        if ($result) {
            $wechatInfo = new WePayLogic();
            $parameters = array(
                'appId' => $result['appid'],                                //小程序ID
                'timeStamp' => (string)time(),                          //时间戳
                'nonceStr' => $wechatInfo->createNoncestr(),                  //随机串
                'package' => 'prepay_id=' . $result['prepay_id'],        //数据包
                'signType' => 'MD5'                                     //签名方式
            );
            $parameters['paySign'] = $wechatInfo->getSign($parameters);

            apiSendSuccess("ok", [
                'data' => $result,
                'type' => 'wechat',
                'order' => $order,
                'parameters' => $parameters,
                'request_path' => $requestPath
            ]);
        }

        apiSendError("支付失败");
    }

    final function payWechatJs(Order $order)
    {
        $post = $_REQUEST;
        if ($post == null) {
            $post = file_get_contents("php://input");
        }

        if ($post == null) {
            $post = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
        }

        $member = Member::findOne($this->memberInfo->id);
//        if ($member->integral < $order->integral_amount) {
//            stopFlow("账户积分不足",'order/index');
//        }

        $notifyPath = ConfigService::init('weixin')->get('notifyUrl');

        $notify = new NativePay();
        $input = new WxPayUnifiedOrder();
        $input->SetBody('订单'.$order->orderid);
        $input->SetAttach($order->orderid);
        $input->SetOut_trade_no($order->pay_sn);
        $input->SetSpbill_create_ip(getClientIp());
        $money = (floatval($order->order_amount) + floatval($order->freight))*100;
        $input->SetTotal_fee($money);
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetGoods_tag($order->orderid);
        $input->SetNotify_url($notifyPath);
        $input->SetTrade_type("JSAPI");
        $input->SetProduct_id($order->pay_sn);

        $buyer = Member::findOne($order->buyer_id);
        $input->SetOpenid($buyer->wxopenid);

        $result = $notify->GetPayUrl($input);

        $path = \Yii::$app->request->hostInfo . UrlService::build(['api/order-status', 'number' => $order->orderid]);
        $requestPath = "redirect_url={$path}";

        if ($result) {
            $wechatInfo = new WePayLogic();
            $parameters = array(
                'appId' => $result['appid'],                                //小程序ID
                'timeStamp' => (string)time(),                          //时间戳
                'nonceStr' => $wechatInfo->createNoncestr(),                  //随机串
                'package' => 'prepay_id=' . $result['prepay_id'], //数据包
                'signType' => 'MD5'                                     //签名方式
            );
            $parameters['paySign'] = $wechatInfo->getSign($parameters);

            apiSendSuccess("ok", [
                'data' => $result,
                'type' => 'wechat',
                'order' => $order,
                'parameters' => $parameters,
                'request_path' => $requestPath
            ]);
        }
//
        apiSendError("支付失败");


//        header('Location: ' . $requestPath);
    }

    /**
     * 账户支付
     *
     * @param Order $order
     */
    final function payAccount(Order $order)
    {
        $member = Member::findOne($this->memberInfo->id);
        if ($member->remainder < ($order->order_amount + $order->freight)) {
            apiSendError("账户余额不足");
        }
    }

    /**
     * 阿里支付
     * @param Order $order
     */
    final function payAlipay(Order $order)
    {
        $member = Member::findOne($this->memberInfo->id);
        if ($member->integral < $order->integral_amount) {
            stopFlow("账户积分不足",'order/index');
        }

//        require ROOT_PATH.'/common/alipay/pagepay/buildermodel/AlipayTradePagePayContentBuilder.php';
//        require ROOT_PATH.'/common/alipay/pagepay/service/AlipayTradeService.php';

        require ROOT_PATH.'/common/aliwappay/wappay/service/AlipayTradeService.php';
        require ROOT_PATH.'/common/aliwappay/wappay/buildermodel/AlipayTradeWapPayContentBuilder.php';

        $salt = \Yii::$app->request->get('salt');
        $sigo = substr($salt, 0, 2);

        //配置信息
        $payment_config = Payment::findOne(['code'=>'alipay']);
        $config = json_decode($payment_config->payment_config,true);
        //商户订单号，商户网站订单系统中唯一订单号，必填
        $out_trade_no = trim($order->pay_sn);

        //订单名称，必填
        $subject = trim('订单支付');

        //付款金额，必填
        $money = floatval($order->order_amount) + floatval($order->freight);

        $total_amount = trim($money);

        //商品描述，可空
        $body = trim($order->pay_sn);

        //构造参数

        $payRequestBuilder = new \AlipayTradeWapPayContentBuilder();
        $payRequestBuilder->setBody($body);
        $payRequestBuilder->setSubject($subject);
        $payRequestBuilder->setOutTradeNo($out_trade_no);
        $payRequestBuilder->setTotalAmount($total_amount);
        $payRequestBuilder->setTimeExpress('1m');
        $config['gatewayUrl'] = 'https://openapi.alipay.com/gateway.do';
        $config['charset'] = 'UTF-8';

        $aop = new \AlipayTradeService($config);

        $returnUrl = \Yii::$app->request->hostInfo . UrlService::build(['api/order-status', 'number' => $order->orderid]);
//        $notifyUrl = \Yii::$app->request->hostInfo . UrlService::build(['alipay/notify']);
        $notifyUrl = \Yii::$app->request->hostInfo . '/alipay_notify.php';

        /**
         * pagePay 电脑网站支付请求
         * @param $builder 业务参数，使用buildmodel中的对象生成。
         * @param $return_url 同步跳转地址，公网可以访问
         * @param $notify_url 异步通知地址，公网可以访问
         * @return $response 支付宝返回的信息
         */
        $response = $aop->wapPay($payRequestBuilder, $returnUrl, $notifyUrl);

        //输出表单
        var_dump($response);
        die;
    }


    protected function getOrderId()
    {
        $orderId = date("ymdHis") . createRandKey(6);
        $order = Order::findOne(['orderid' => $orderId]);
        return $order ? $this->getOrderId() : $orderId;
    }



    /**
     * 支付成功
     */
    public function actionSuccess($number)
    {
        $order = Order::findOne(['orderid'=>$number]);

        return $this->render('pay_suc',['order'=>$order]);
    }

    /**
     * 支付失败
     */
    public function actionPayFail($number)
    {
        $order = Order::findOne(['orderid'=>$number]);
        return $this->render('pay_fail',['order'=>$order]);
    }


    /**
     * 支付回调
     */
    public function actionNotify()
    {
        $orderPaySn = WechatPay::notify();
        file_put_contents(ROOT_PATH . '/runtimes/notify.txt', $orderPaySn, FILE_APPEND);

        $this->_notifyOrderByPaySn($orderPaySn);
    }

    public function actionMockNotify($key='')
    {
        if (strlen($key)) {
            DL_DEBUG and $this->_notifyOrderByPaySn($key);
        }
       die('not found');
    }

    /**
     * 回调流程
     * @param $orderPaySn
     */
    private function _notifyOrderByPaySn($orderPaySn)
    {
        if(preg_match('/^ch\d+$/', $orderPaySn)) {
            $rechargeRecord = Recharge::findOne([
                'pay_sn' => $orderPaySn,
                'state' => Recharge::WAIT
            ]);

            $rechargeRecord or die('SUCCESS');
            $rechargeRecord->payTime = date('Y-m-d H:i:s');
            $rechargeRecord->state = $rechargeRecord::FINISH;

            $member = Member::findOne($rechargeRecord->memberId);
            $member or die("FAILED");

            $money = new Money();
            $money->member_id = $member->id;
            $money->name = '-';
            $money->type = $money::TYPE_CHARGE;
            $money->money = $rechargeRecord->money;
            $money->status = $money::STATUS_FINISH;
            $money->create_time = date('Y-m-d H:i:s');
            $money->save();

            $member->remainder = $member->remainder + $rechargeRecord->money;
            $rechargeRecord->save() and $member->save();

            die('SUCCESS');

            // 退款回调
        } elseif (preg_match('/^re\d+$/', $orderPaySn)) {
            $orderReback = OrderReback::findOne([
                'reback_sn' => $orderPaySn,
                'status' => 0
            ]);

            if ($orderReback) {
                $orderReback->status = 1;
                $orderReback->save();
            }

            $order = Order::findOne([
                'orderid' => $orderReback->orderid
            ]);
            switch ($order->type) {
                case $order::TYPE_ZHAN_HUI :
                    $coupon = SystemCoupon::findOne($order->relation_coupon);
                    if ($coupon) {
                        $coupon->status = $coupon::STATUS_FALSE;
                        $coupon->save();
                    }
                    break;
            }

            die('SUCCESS');

        } else {
            $order = Order::findOne([
                'pay_sn' => $orderPaySn,
                'order_state' => Order::STATUS_WAIT
            ]);
            if ($order) {
                $order->order_state = Order::STATUS_PAY;
                if ($order->save()) {
                    $orderGoods = OrderGoods::find()->where(['order_id'=>$order->orderid])->orderBy('id ASC')->all();
                    foreach ($orderGoods AS $orderGood) {
                        $good = $orderGood->goods;
                        if ((int)$good->id) {
                            if ((int)$orderGood->sku_id) {
                                $skuInfo = GoodsSku::findOne($orderGood->sku_id);
                                $skuInfo->num = $skuInfo->num - $orderGood->goods_num;
                                $skuInfo->save();

                                $goodsSkuInfos = GoodsSku::findAll([
                                    'goods_id' => $good->id
                                ]);
                                $goodsSkuInfos = formatObjLists($goodsSkuInfos);
                                $good->attr = json_encode($goodsSkuInfos);
                            } else {
                                $good->goods_storage = $good->goods_storage - $orderGood->goods_num;
                            }

                            $good->goods_salenum += $orderGood->goods_num;
                            $good->save();
                        }
                    }

                    if ($order->type == $order::TYPE_ZHAN_HUI) {
                        $ticketInfo = TicketInfo::findOne([
                            'ticket_id' => $order->ticket_id
                        ]);

                        if ($ticketInfo) {
                            $ticketInfo->ticket_amount++;
                            $ticketInfo->save();
                        }

                        $coupon = SystemCoupon::findOne($order->relation_coupon);
                        if ($coupon) {
                            $coupon->status = $coupon::STATUS_TRUE;
                            $coupon->save();
                        }
                    }
                }

            }
            die('SUCCESS');
        }
    }

    /**
     * 取消订单
     */
    public function actionCancel($number='###')
    {
        $order = Order::findOne([
            'orderid' => $number,
//            'order_state' => Order::STATUS_PAY,
        ]);

        $order or apiSendError("未找到有效的订单信息");

        $result = true;
        $title = "取消订单";
        if ($order->order_state == Order::STATUS_PAY) {
            $result = false;
            $title = '退款';

            $reReback = new OrderReback();
            $rebackLog = $reReback::findOne([
                'orderid' => $order->orderid,
                'status' => 0
            ]);

            if (!$rebackLog) {
                $reReback->orderid = $order->orderid;
                $reReback->pay_sn = $order->pay_sn;
                $reReback->reback_sn = $reReback->getNewRepaySn();
                $reReback->status = 0;
                $reReback->save();

                $rebackLog = $reReback;
            }

            $rebackLog->create_time = date('Y-m-d H:i:s');
            $reReback->save();

            switch ($order->pay_method) {
                case $order::PAY_MECHOD_WECHAT :
                    $result = $this->_rebackWechat($order);
                    break;
            }
        }

        if ($result) {
            $order->order_state = Order::STATUS_CLOSE;
            $order->finnshed_time = date('Y-m-d H:i:s');
            $order->save();
            apiSendSuccess("{$title}成功");
        }

        apiSendError("{$title}失败");
    }

    private function _rebackWechat(Order $order)
    {
        try {
            $payment = Payment::findOne(['code' => 'wechat']);
            $paymentInfo = json_decode($payment->payment_config);
            $config = Config::findOne(['cKey' => 'weixin']);
            $configData = json_decode($config->cValue);

            $reReback = OrderReback::findOne([
                'orderid' => $order->orderid,
                'status' => 0
            ]);

            if (!$reReback) {
                throw new \Exception("系统错误，支付信息异常");
            }

            $wechatRefund = new WechatRefund($configData->appid, $paymentInfo->mchid, $paymentInfo->key, $configData->notifyUrl);
            return $wechatRefund->refund($reReback->pay_sn, $reReback->reback_sn, $order->order_amount * 100, $order->order_amount * 100);
        } catch (\Exception $e) {

            return false;
        }
    }

}
