<?php
namespace wap\controllers;


use common\wxpay\JsApiPay;
use DL\Project\Sms;
use DL\service\UrlService;
use wap\logic\OrderLogic;
use wap\models\Member;
use wap\models\Order;
use wap\models\OrderGoods;
use wap\models\Ticket;
use wap\models\TicketApplyGoods;

class OrderController extends BaseController
{
    public function init()
    {
        parent::init();

        if (\Yii::$app->user->isGuest) {
            stopFlow("您尚未登录");
        }
    }

    /**
     * 预约享特价
     * @author daorli
     * @date 2019-6-13
     */
    public function actionCreate()
    {
        try {
            $ticketId = \Yii::$app->request->get('ticketId', '0');
            $ticket = Ticket::findOne([
                'id' => $ticketId,
                'status' => 1
            ]);
            $ticket or apiSendError("活动已结束");

            $goodId = \Yii::$app->request->get('goodsId', 0);
            $ticketGood = TicketApplyGoods::findOne([
                'ticket_id' => $ticket->id,
                'good_id' => $goodId,
                'type' => TicketApplyGoods::TYPE_ORDER,
            ]);

            $ticketGood or apiSendError("该商品未参加本次活动");
            $good = $ticketGood->goods;

            $orderService = OrderLogic::init();
            $order = $orderService->createNewEmptyOrder($this->memberInfo->id, $good->user_id, Order::TYPE_ZHAN_HUI, $ticketType=Order::TICKET_TYPE_ORDER, $ticket->id);
            $orderService->addOrderGoodToOrder($order->orderid, $good->id, 1, $ticket->id);

            $url = UrlService::build(['order/submit', 'number' => $order->orderid]);
            header("Location: {$url}");
            die();
        } catch (\Exception $e) {
            stopFlow($e->getMessage());
        }
    }


    public function actionUpdate()
    {
        $this->memberInfo or apiSendError("用户未登录");
        $orderId = \Yii::$app->request->post('number', 0);
        $order = Order::findOne([
            'orderid' => $orderId,
            'buyer_id' => $this->memberInfo->id,
            'order_state' => Order::STATUS_WAIT
        ]);

        $order or apiSendError("订单信息未找到");

        $payMethod = \Yii::$app->request->post('setPay', '');
        if (strlen($payMethod)) {
            if (in_array($payMethod, [
                $order::PAY_MECHOD_ACCOUNT,
                $order::PAY_MECHOD_WECHAT,
                $order::PAY_MECHOD_ALIPAY,
            ])) {
                $order->pay_method = trim($payMethod);
            }
        }

        $order->save() and apiSendSuccess("OK");

        apiSendError("更新订单信息失败");
    }


    /**
     * 爆款预约
     * @author daorli
     * @date 2019-6-13
     */
    public function actionCreatePrice()
    {
        try {
            $ticketId = \Yii::$app->request->get('ticketId', '0');
            $ticket = Ticket::findOne([
                'id' => $ticketId,
                'status' => 1
            ]);
            $ticket or apiSendError("活动已结束");

            $goodId = \Yii::$app->request->get('goodsId', 0);
            $ticketGood = TicketApplyGoods::findOne([
                'ticket_id' => $ticket->id,
                'good_id' => $goodId,
                'type' => TicketApplyGoods::TYPE_COUPON,
            ]);
            $ticketGood or apiSendError("该商品未参加本次活动");
            $orders = Order::findAll([
                'buyer_id' => $this->memberInfo->id,
                'ticket_id' => $ticket->id,
            ]);

            if ($orders) {
                $orderIds = arrayGroupsAction($orders, function ($order) {
                    return $order->orderid;
                });

                $orderGoods = OrderGoods::find()->where([
                    'and',
                    ['in', 'order_id', $orderIds],
                    ['=', 'goods_id', $ticketGood->good_id]
                ])->all();

                $orderGoods and apiSendError("该商品已预订");
            }
            $good = $ticketGood->goods;

            $orderService = OrderLogic::init();
            $order = $orderService->createNewEmptyOrder($this->memberInfo->id, $good->user_id, Order::TYPE_ZHAN_HUI, TicketApplyGoods::TYPE_COUPON, $ticket->id);
            $orderService->addOrderGoodToOrder($order->orderid, $good->id, 1, $ticket->id);

            // 发送短信
            Sms::init()->baoKuanYuYue($this->memberInfo->mobile, $good->id, $ticket->id);
            $newMemberStatus = \Yii::$app->request->get('new_member', 0);
            if ($newMemberStatus) {
                $session = \Yii::$app->session;
                $initPwd = $session->get('INIT_PWD');
                Sms::init()->zhuCeHuiYuan($this->memberInfo->id, $initPwd);
            }

            $source = \Yii::$app->request->get('source', 0);
            $url = UrlService::build(['api/order-goods-price-success', 'key' => strrev($order->orderid), 'source' => $source]);

            apiSendSuccess("预约成功", [
                'href' => $url,
                'goodsId' => $good->id
            ]);
        } catch (\Exception $e) {
            apiSendError($e->getMessage());
        }
    }

    public function actionSubmit($number='###')
    {
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
            $member = Member::findOne(\Yii::$app->user->id);
            $openId = $member->wxopenid;
            if (!strlen($openId)) {
                $jsApi = new JsApiPay();
                $openId = $jsApi->GetOpenid();

                $member->wxopenid = $openId;
                $member->save();
            }
        }

        $order = Order::findOne([
            'orderid' => $number,
            'order_state' => Order::STATUS_WAIT
        ]);

        $order or stopFlow("未找到待支付的订单");

        return $this->render('create', [
            'order' => $order
        ]);
    }

}