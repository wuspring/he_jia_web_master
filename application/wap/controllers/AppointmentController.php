<?php
/**
 * 爆品预约
 * @author  wufeng
 * @date 2019-6-6
 */
namespace wap\controllers;

use DL\Project\CityTicket;
use wap\logic\ApplyGoodsLogic;
use wap\logic\OrderLogic;
use wap\models\GoodsClass;
use wap\models\Order;
use wap\models\OrderGoods;
use wap\models\TicketApplyGoods;

class AppointmentController extends BaseController
{
    /**
     * 商品列表
     * @author  wufeng
     * @date 2019-6-6
     * @return string
     */
    public function actionIndex()
    {
        $cityId = $this->getRequestCity();
        //分类
        $cats = GoodsClass::find()->where(['and',['!=','fid',''],['is_del'=>0]])->orderBy(['sort'=>SORT_ASC])->asArray()->all();
        $opts = ['size'=>10,'page'=>1];
        $totalCount = ApplyGoodsLogic::getSubscribeGoodsCount($cityId,$opts);
        return $this->render('index',[
            'cats'=>$cats,
            'totalCount'=> $totalCount
        ]);
    }

    /**
     * 商品列表
     * @author  wufeng
     * @date 2019-6-6
     * @return string
     */
    public function actionPageList(){
        if(\Yii::$app->request->isPost){
            $parms = \Yii::$app->request->post();

            $opts = ['size'=>$parms['size'],'page'=>$parms['page']];
            $exwhere = [];
            if(isset($parms['cid']) && $parms['cid'] > 0){
                $exwhere = [
                    'or',
                    ['g.gc_id'=>$parms['cid']],
                    ['g.egc_id'=>$parms['cid']]
                ];
            }

            $goodsList = ApplyGoodsLogic::getSubscribeGoodsList($parms['cityId'],$opts,$exwhere);
            $totalCount = ApplyGoodsLogic::getSubscribeGoodsCount($parms['cityId'],$opts,$exwhere);
            apiSendSuccess('ok',[
                'list'=>$goodsList,
                'totalCount'=> $totalCount
            ]);
        }
    }

    /**
     * 商品预约
     * @author  wufeng
     * @date 2019-6-6
     * @return string
     */
    public function actionCreate(){
        if(\Yii::$app->request->isPost){
            if(\Yii::$app->user->isGuest){
                apiSendError('nologin');
            }
            try {
                $type = \Yii::$app->request->post('type', '###');
                $currentTickets = CityTicket::init($this->getRequestCity())->currentTickets();

                isset($currentTickets[$type]) or apiSendError("活动尚未开展");

                $ticket = $currentTickets[$type];
                $ticket or apiSendError("活动已结束");

                $goodId = \Yii::$app->request->post('goodId', 0);
                $ticketGood = TicketApplyGoods::findOne([
                    'ticket_id' => $ticket->id,
                    'good_id' => $goodId,
                    'type' => TicketApplyGoods::TYPE_COUPON,
                ]);
                $ticketGood or apiSendError("该商品未参加本次活动");
                $orders = Order::findAll([
                    'buyer_id' => $this->memberInfo->id,
                    'ticket_id' => $ticket->id,
                ]);

                if ($orders) {
                    $orderIds = arrayGroupsAction($orders, function ($order) {
                        return $order->orderid;
                    });

                    $orderGoods = OrderGoods::find()->where([
                        'and',
                        ['in', 'order_id', $orderIds],
                        ['=', 'goods_id', $ticketGood->good_id]
                    ])->all();

                    $orderGoods and apiSendError("该商品已预订");
                }
                $good = $ticketGood->goods;

                $orderService = OrderLogic::init();
                $order = $orderService->createNewEmptyOrder($this->memberInfo->id, $good->user_id, Order::TYPE_ZHAN_HUI, TicketApplyGoods::TYPE_COUPON, $ticket->id);
                $orderService->addOrderGoodToOrder($order->orderid, $good->id, 1, $ticket->id);

                // 发送短信
                \DL\Project\Sms::init()->baoKuanYuYue($this->memberInfo->mobile, $good->id, $ticket->id);
                $newMemberStatus = \Yii::$app->request->get('new_member', 0);
                if ($newMemberStatus) {
                    $session = \Yii::$app->session;
                    $initPwd = $session->get('INIT_PWD');
                    \DL\Project\Sms::init()->zhuCeHuiYuan($this->memberInfo->id, $initPwd);
                }

                apiSendSuccess("您已成功预约{$good->goods_name}产品", [
                    'key' => strrev($order->orderid),
                    'type' => 'COUPON',
                ]);
                die();
            } catch (\Exception $e) {
                apiSendError($e->getMessage());
            }
        }
    }


}
