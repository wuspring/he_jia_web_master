<?php
namespace wap\controllers;

use common\models\Ticket;

/**
 * Class JzController
 * 家装展会页面
 * @author daorli
 *
 * @package wap\controllers
 */
class JzController extends BaseTicketController
{
    // 票类型
    protected $formType;
    // 票分类
    protected $caType=Ticket::CA_TYPE_ZHAN_HUI;

    public function init()
    {
        parent::init();

        $this->formType = Ticket::TYPE_JIA_ZHUANG;
    }
}
