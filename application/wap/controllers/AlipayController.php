<?php

namespace wap\controllers;

use common\models\Order;
use common\models\Payment;
use common\models\Recharge;
use yii\web\Controller;
use Yii;
use wap\models\GoodsSku;
use wap\models\Member;
use wap\models\Money;
use wap\models\OrderGoods;
use wap\models\OrderReback;
use common\models\SystemCoupon;
use wap\models\TicketInfo;

require ROOT_PATH.'/common/aliwappay/wappay/service/AlipayTradeService.php';
require ROOT_PATH.'/common/aliwappay/wappay/buildermodel/AlipayTradeWapPayContentBuilder.php';

class AlipayController extends PaymentController
{
    /**
     * 支付宝异步回调地址
     * @author wufeng
     * @date 2018-11-08
     */
    public function actionNotify(){
        libxml_disable_entity_loader(true);
        $arr=$_POST;

        //配置信息
        $payment_config = Payment::findOne(['code'=>'alipay']);
        $config = json_decode($payment_config->payment_config,true);

        $config['gatewayUrl'] = 'https://openapi.alipay.com/gateway.do';
        $config['charset'] = 'UTF-8';

        $alipaySevice = new \AlipayTradeService($config);
        $alipaySevice->writeLog(var_export($_POST,true));
        $result = $alipaySevice->check($arr);

        /* 实际验证过程建议商户添加以下校验。
        1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
        2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
        3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
        4、验证app_id是否为该商户本身。
        */


        if($result) {//验证成功
            //商户订单号
            $out_trade_no = $arr['out_trade_no'];

            $order = Order::findOne([
                'pay_sn' => $out_trade_no,
                'order_state' => Order::STATUS_WAIT
            ]);

            if (!$order) {
                die('order status error');
            }
            //交易状态
            $trade_status = $arr['trade_status'];
            if($arr['trade_status'] == 'TRADE_FINISHED') {
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //请务必判断请求时的total_amount与通知时获取的total_fee为一致的
                //如果有做过处理，不执行商户的业务程序
                //注意：
                //退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知

                $this->_notifyOrderByPaySn($out_trade_no);
            }
            else if ($arr['trade_status'] == 'TRADE_SUCCESS') {
                $this->_notifyOrderByPaySn($out_trade_no);
            }


            echo "success";	//请不要修改或删除
        }else {
            //验证失败
            echo "fail";
        }
    }

    /**
     * 回调流程
     * @param $orderPaySn
     */
    protected function _notifyOrderByPaySn($orderPaySn)
    {
        if(preg_match('/^ch\d+$/', $orderPaySn)) {
            $rechargeRecord = Recharge::findOne([
                'pay_sn' => $orderPaySn,
                'state' => Recharge::WAIT
            ]);

            $rechargeRecord or die('SUCCESS');
            $rechargeRecord->payTime = date('Y-m-d H:i:s');
            $rechargeRecord->state = $rechargeRecord::FINISH;

            $member = Member::findOne($rechargeRecord->memberId);
            $member or die("FAILED");

            $money = new Money();
            $money->member_id = $member->id;
            $money->name = '-';
            $money->type = $money::TYPE_CHARGE;
            $money->money = $rechargeRecord->money;
            $money->status = $money::STATUS_FINISH;
            $money->create_time = date('Y-m-d H:i:s');
            $money->save();

            $member->remainder = $member->remainder + $rechargeRecord->money;
            $rechargeRecord->save() and $member->save();

            die('SUCCESS');

            // 退款回调
        } elseif (preg_match('/^re\d+$/', $orderPaySn)) {
            $orderReback = OrderReback::findOne([
                'reback_sn' => $orderPaySn,
                'status' => 0
            ]);

            if ($orderReback) {
                $orderReback->status = 1;
                $orderReback->save();
            }

            $order = Order::findOne([
                'orderid' => $orderReback->orderid
            ]);
            switch ($order->type) {
                case $order::TYPE_ZHAN_HUI :
                    $coupon = SystemCoupon::findOne($order->relation_coupon);
                    if ($coupon) {
                        $coupon->status = $coupon::STATUS_FALSE;
                        $coupon->save();
                    }
                    break;
            }

            die('SUCCESS');

        } else {
            $order = Order::findOne([
                'pay_sn' => $orderPaySn,
                'order_state' => Order::STATUS_WAIT
            ]);
            if ($order) {
                $order->order_state = Order::STATUS_PAY;
                if ($order->save()) {
                    $orderGoods = OrderGoods::find()->where(['order_id'=>$order->orderid])->orderBy('id ASC')->all();
                    foreach ($orderGoods AS $orderGood) {
                        $good = $orderGood->goods;
                        if ((int)$good->id) {
                            if ((int)$orderGood->sku_id) {
                                $skuInfo = GoodsSku::findOne($orderGood->sku_id);
                                $skuInfo->num = $skuInfo->num - $orderGood->goods_num;
                                $skuInfo->save();

                                $goodsSkuInfos = GoodsSku::findAll([
                                    'goods_id' => $good->id
                                ]);
                                $goodsSkuInfos = formatObjLists($goodsSkuInfos);
                                $good->attr = json_encode($goodsSkuInfos);
                            } else {
                                $good->goods_storage = $good->goods_storage - $orderGood->goods_num;
                            }

                            $good->goods_salenum += $orderGood->goods_num;
                            $good->save();
                        }
                    }

                    if ($order->type == $order::TYPE_ZHAN_HUI) {
                        $ticketInfo = TicketInfo::findOne([
                            'ticket_id' => $order->ticket_id
                        ]);

                        if ($ticketInfo) {
                            $ticketInfo->ticket_amount++;
                            $ticketInfo->save();
                        }

                        $coupon = SystemCoupon::findOne($order->relation_coupon);
                        if ($coupon) {
                            $coupon->status = $coupon::STATUS_TRUE;
                            $coupon->save();
                        }
                    }
                }

            }
            die('SUCCESS');
        }
    }
}
