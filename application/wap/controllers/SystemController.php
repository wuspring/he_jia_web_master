<?php
namespace wap\controllers;


use common\models\Comment;
use common\models\GoodsClass;
use common\models\GoodsCoupon;
use common\models\MemberStoreCoupon;
use common\models\News;
use common\models\StoreGcScore;
use common\models\Ticket;
use common\models\TicketApply;
use common\models\User;
use DL\Project\CityTicket;
use DL\Project\Store;
use DL\service\UrlService;
use DL\vendor\Page;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

/**
 * Class SystemController
 * 系统页面
 *
 * @package wap\controllers
 */
class SystemController extends BaseController
{
    /**
     * 流程停止(StopFlow)着陆页-成功页面
     * @author daorli
     * @date 2019-6-6
     *
     * @return string
     */
    public function actionSuccess()
    {
        $error = \Yii::$app->session->get('__ERROE_INFOMATION__');
        $error or $error = [];

        isset($error['msg']) or $error['msg'] = '成功';
        isset($error['href']) or $error['href'] = UrlService::build('index/index');

        return $this->render('success', [
            'error' => $error,
            'timeOut' => 3
        ]);
    }

    /**
     * 流程停止(StopFlow)着陆页-失败页面
     * @author daorli
     * @date 2019-6-6
     *
     * @return string
     */
    public function actionError()
    {
        $error = \Yii::$app->session->get('__ERROE_INFOMATION__');
        $error or $error = [];

        isset($error['msg']) or $error['msg'] = '系统错误';
        isset($error['href']) or $error['href'] = UrlService::build('index/index');

        return $this->render('error', [
            'error' => $error,
            'timeOut' => 3
        ]);
    }
}
