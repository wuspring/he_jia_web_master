<?php

namespace wap\controllers;

use common\models\MemberForm;
use common\models\Service;
use common\models\Ticket;
use wap\models\HelpCont;
use wap\models\Member;
use wap\models\MemberFastRg;
use common\models\NavsWap;
use wap\logic\AdvertisingLogic;
use wap\logic\ApplyGoodsLogic;
use wap\models\FloorWap;
use wap\models\Goods;
use wap\models\MemberRg;
use wap\models\News;
use wap\models\StoreShop;
use wap\models\TicketApplyGoods;
use Yii;
use wap\models\Provinces;
use wap\models\SelectCity;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * AddressController implements the CRUD actions for Address model.
 */
class IndexController extends BaseController
{
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['logout', 'signup'],
//                'rules' => [
//                    [
//                        'actions' => ['signup'],
//                        'allow' => true,
//                        'roles' => ['?', '@'],
//                    ],
//                    [
//                        'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//        ];
//    }

    /**
     *  手机端首页
     * @author  wufeng
     * @date 2019-6-5
     */
    public function actionIndex()
    {
        $cityId = $this->getRequestCity();
        //banners
        $banners =  AdvertisingLogic::adList(1,$cityId);
//        魔方
        $magics=  AdvertisingLogic::adList(2,$cityId);
        // 预存享特价/爆款预约 上 广告图
        $hot_ads = AdvertisingLogic::adList(3,$cityId);
        // 预存 享特价 商品列表
        $applyGoods = ApplyGoodsLogic::getApplyGoodsList($cityId,['size'=>3,'page'=>1]);
        //爆款预约
        $reserGoods = ApplyGoodsLogic::getSubscribeGoodsList($cityId);
        //精选推荐广告
        $rmd_ads = AdvertisingLogic::adList(4,$cityId);
        //推荐顶部广告
        $rmd_top_ads = AdvertisingLogic::adList(13,$cityId);
        //资讯顶部广告
        $news_top_ads = AdvertisingLogic::adList(14,$cityId);

        $ticket_ids = ApplyGoodsLogic::getCityExpIds($cityId);
        // 楼层管理
        $floors = FloorWap::find()->where([
            'and',
            ['in','ticket_id',$ticket_ids],
            ['=','status',1]
        ])->orderBy(['sort'=>SORT_ASC])->asArray()->all();
        if(!empty($floors)){
            foreach ($floors as $key=>$item){
                $floors[$key]['topimg'] = !empty($item['topimg'])?json_decode($item['topimg'],true):[];
                $floors[$key]['nextimg'] = !empty($item['nextimg'])?json_decode($item['nextimg'],true):[];
                $goods_list = [];
                $list = TicketApplyGoods::find()->where([
                    'and',
                    ['status'=>1],
                    ['ticket_id'=>$item['ticket_id']],
                    ['is_index'=>1],
                ])->orderBy(['sort'=>SORT_DESC,'id'=>SORT_DESC])->limit(4)->all();
                $goods_list = arrayGroupsAction($list, function ($gid) {
                    $good = Goods::findOne($gid->good_id);
                    $good or $good = (new Goods());
                    $info = $good->attributes;
                    $info['goods_price'] = $gid->good_price;
                    $info['goods_pic'] = $gid->good_pic;
                    $info['goods_storage'] = $gid->good_amount;
                    $info['store'] = $good->user;
                    $info['type'] = $gid->ticket->type;
                    //体验店数量
                    $info['shop_count'] = StoreShop::find()->where(['user_id'=>$good->user_id])->count();
                    return (object)$info;
                });
                $floors[$key]['goods'] = $goods_list;
            }
        }
        //和家资讯
        $news = News::find()->where([
            'and',
            ['isShow'=>1],
            ['like','provinces_id',$cityId]
        ])->orderBy(['createTime'=>SORT_DESC,'clickRate'=>SORT_DESC])->limit(3)->all();
        // 首页菜单
        $homeMenus = NavsWap::find()->where(['provinces_id'=>$cityId,'status'=>1,'type'=>'home'])->limit(5)->orderBy('sort ASC')->all();
        // 品质承诺
        $serviceList = Service::find()->where(['provinces_id'=>$cityId,'pos'=>Service::POS_HOME])->orderBy('sort ASC')->all();

        return $this->render('index',[
            'banners'=>$banners,
            'magics'=>$magics,
            'hot_ads'=>$hot_ads,
            'applyGoods'=>$applyGoods,
            'reserGoods'=>$reserGoods,
            'rmd_ads'=>$rmd_ads,
            'floors'=>$floors,
            'home_menus'=> $homeMenus,
            'news'=> $news,
            'rmd_top_ads'=> $rmd_top_ads,
            'news_top_ads'=> $news_top_ads,
            'serviceList'=> $serviceList,
        ]);
    }

    /**
     * Notes: 登录
     * User: qin
     * Date: 2019/6/4
     * Time: 10:16
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new MemberForm();
        if (Yii::$app->request->isAjax){
            if ($model->load(Yii::$app->request->post()) && $model->validate()){
                $model->login();
                return $this->goBack();
            }else{
                return json_encode($model->firstErrors);
            }
        }
        return $this->render('login', [
            'model' => $model,
        ]);

    }

    /**
     * Notes: 快速登录
     * User: qin
     * Date: 2019/6/5
     * Time: 11:23
     * @return string|Response
     */
    public function actionFastLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new MemberFastRg();
        if (Yii::$app->request->isAjax) {

            if ($model->load(Yii::$app->request->post()) && $model->validate()){
                $member = Member::find()->andWhere(['mobile'=>$model->mobile])->one();
                if (empty($member)){
                    $model->username = $model->mobile;
                    $model->avatar = "";
                    $model->role = 0;
                    $model->sex = 1;
                    $model->nickname = $model->username;
                    $model->createTime = date('Y-m-d H:i:s');
                    $model->status = 1;
                    $model->modifyTime = $model->createTime;
                    $model->password = Yii::$app->security->generatePasswordHash('123');
                    if ($model->save(0)){
                        $login = new MemberForm();
                        $login->autologin($model->id);
                        Yii::$app->session->remove('SMS_CODE');
                        return $this->redirect(['member/index']);
                    }
                }else{
                    Yii::$app->session->remove('SMS_CODE');
                    $login = new MemberForm();
                    $login->autologin($member->id);
                    return $this->redirect(['member/index']);
                }
            }else{
                return json_encode($model->firstErrors);
            }
        }
        return $this->render('fast_login', [
            'model' => $model,
        ]);
    }

    /**
     * Notes: 忘记密码
     * User: qin
     * Date: 2019/6/14
     * Time: 10:11
     * @return string
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     */
    public  function actionForgetpwd(){

        if (yii::$app->request->isPost){
            $json=array('code'=>0,'msg'=>'手机号错误!');
            $post=yii::$app->request->post();
            $member=Member::find()->where(['mobile'=>$post['mobile']])->one();
            if (empty($member)){
                $json=array(
                    'state'=>0,
                    'msg'=>'该手机号未注册!',
                );
                echo json_encode($json);yii::$app->end();
            }
            $smsdata=yii::$app->session->get('SMS_CODE');
            if (!empty($smsdata)){
                $smsdataStr = $post['mobile'].'-'.$post['smscode'];
                if ($smsdata['value_str']==$smsdataStr){
                        $member->password=Yii::$app->security->generatePasswordHash($post['password']);;
                        if ($member->save()){
                            Yii::$app->session->remove('SMS_CODE');
                            $json=array('code'=>1,'msg'=>'您的密码已找回，用新密码登录吧!');
                        }else{
                            $json=array('code'=>0,'msg'=>'找回密码失败!');
                        }
                }else{
                    $json=array('code'=>0,'msg'=>'验证码错误!');
                }

            }else{

                $json=array('code'=>0,'msg'=>'验证码无效!');
            }
            return json_encode($json);
        }
        return $this->render('forgetpwd');
    }

    /**
     * Notes: 退出登录
     * User: qin
     * Date: 2019/6/4
     * Time: 10:17
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Notes: 用户注册
     * User: qin
     * Date: 2019/6/4
     * Time: 15:32
     * @return string|\yii\web\Response
     * @throws \yii\base\Exception
     */

    public function actionSignup()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new MemberRg();

        if (Yii::$app->request->isAjax){
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->avatar = "";
                $model->role = 0;
                $model->sex = 1;
                $model->nickname = $model->username;
                $model->createTime = date('Y-m-d H:i:s');
                $model->status = 1;
                $model->modifyTime = $model->createTime;
                $model->password = Yii::$app->security->generatePasswordHash($model->password);
                if ($model->save(0)) {
                    Yii::$app->session->remove('SMS_CODE');
                    $login = new MemberForm();
                    $login->autologin($model->id);
                    $json=array('code'=>1,'msg'=>'注册成功，去首页看看吧~');
                    return json_encode($json);
  //                  return $this->redirect(['member/index']);
                } else {
                    return json_encode($model->firstErrors);
                }
            }else{
                return json_encode($model->firstErrors);
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }


    /**
     * Notes:会员协议
     * User: qin
     * Date: 2019/6/14
     * Time: 10:01
     */
    public  function actionMemberAgreement(){

        $helpCont = HelpCont::findOne(4);

        return $this->render('member-agreement',[
            'helpCont'=>$helpCont
        ]);
    }

    /**
     * 设置默认城市
     *
     * @author daorli
     */
    public function actionSetCity()
    {
        $provincesId = \Yii::$app->request->get('pr_id', 0);
        $provinces = Provinces::findOne($provincesId);

        $provinces or apiSendError("未找到有效的城市信息");

        $selectCity  = SelectCity::findOne([
            'provinces_id' => $provinces->id
        ]);
        $selectCity or apiSendError("暂不支持该城市");

        $session = \Yii::$app->session;
        $session->set($this->webBrowserKey, $selectCity->provinces_id);
        \yii::$app->session->set('citypinyin',$provinces->pinyin);
        \yii::$app->session->set('citybianhao',$provinces->id);
        $route=\yii::$app->session->get('routedata');
        $url="#";

        if (isset($route[0]['route'])){

            if (empty($route[0]['params'])){
                if ($route[0]['route']=='index/index'){
                    $url=  Url::toRoute([$route[0]['route'],'SecretCityId'=>$provinces->pinyin]);
                }else{
                    $url=  Url::toRoute([$route[0]['route']]);
                }
            }else{

                if (strpos($route[0]['route'],'jz') !== false){

                    if (isset($route[0]['params']['ticketId'])){
                        $ticket = Ticket::findOne([
                            'ca_type' => Ticket::CA_TYPE_ZHAN_HUI,
                            'type' =>Ticket::TYPE_JIA_ZHUANG,
                            'citys' => $provinces->id,
                            'status' => 1
                        ]);

                        $route[0]['params']['ticketId']=$ticket ? $ticket->id : 0;

                    }
                }
                if ($route[0]['route']=='index/index'){
                    $route[0]['params']['SecretCityId']=$provinces->pinyin;

                }
                $list=array_merge([$route[0]['route']],$route[0]['params']);


                $url=Url::toRoute($list);
            }
        }
        //return $this->goBack();
//        print_r($url);exit;
        apiSendSuccess("切换城市成功",$url);
    }
}




