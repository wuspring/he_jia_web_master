<?php

namespace wap\controllers;

use wap\logic\AdvertisingLogic;
use wap\models\Advertising;
use wap\models\IntegralLog;
use wap\models\IntergralTreasure;
use wap\models\IntergralTreasureLog;

class IntegralTreasureController extends BaseController
{
    /**
     * 积分夺宝
     * @author wufeng
     * @date 2019-6-13
     * @return string
     */
    public function actionIndex()
    {
        $cityId = $this->getRequestCity();
        // 积分夺宝顶部图
        $topAd = AdvertisingLogic::adList(6);

        return $this->render('index',[
            'topAd'=> !empty($topAd)?(object)$topAd[0]:(new Advertising())
        ]);
    }

    /**
     * 积分夺宝列表
     * @author wufeng
     * @date 2019-6-13
     */
    public function actionList(){
        if(\Yii::$app->request->isPost){
            $page = \Yii::$app->request->post('page',1);
            $size = \Yii::$app->request->post('size',10);

            $where = [
                'goods_state'=>1,
                'isdelete'=>0,
            ];
            $goodsList = IntergralTreasure::find()->where($where)->offset(($page-1)*$size)
                ->limit($size)->orderBy(['goods_addtime'=>SORT_DESC])->all();

            $list = [];
            if(!empty($goodsList)){
                foreach ($goodsList as $key=>$item){
                    $arr = $item->attributes;
                    $arr['goods_image'] = json_decode($item->goods_image,true);
                    $list[] = (object)$arr;
                }
            }

            $totalCount = IntergralTreasure::find()->where($where)->count();

            apiSendSuccess('ok',[
                'list'=>$list,
                'totalCount' => $totalCount
            ]);
        }
    }

    /**
     * 积分夺宝商品详情
     * @author wufeng
     * @date 2019-6-13
     * @param $id 夺宝商品ID
     */
    public function actionDetail($id){
        $goods = IntergralTreasure::findOne($id);
        $goods->goods_image = json_decode($goods->goods_image,true);
        return $this->render('detail',[
            'goods'=>$goods
        ]);
    }

    /**
     * 积分夺宝 参加夺宝
     * @author wufeng
     * @date 2019-6-13
     */
    public function actionGetTreasure()
    {
        $id = \Yii::$app->request->get('id', 0);
        $amount = (int)\Yii::$app->request->get('amount', 0);

        $amount or apiSendError("请输入领票的数量");

        $member = \Yii::$app->user->identity;
        $member or apiSendError("您尚未登录");

        $treasure = IntergralTreasure::findOne([
            'id' => $id,
            'goods_state' => 1,
            'isdelete' => 0,
        ]);
        $treasure or apiSendError("活动已结束");

        $totalIntegral = $treasure->goods_integral * $amount;
        $oldIntegral = $member->integral;
        if ($oldIntegral < $totalIntegral) {
            apiSendError("积分不足");
        }
        $member->integral = $oldIntegral - $totalIntegral;
        $member->save() or apiSendError("网络异常");

        $applyGroupKey = 'db' . date('YmdH:i:s') . '_' . $member->id . '_' . $treasure->id;

        $integralLog = new IntegralLog();
        $integralLog->orderId = $applyGroupKey;
        $integralLog->memberId = $member->id;
        $integralLog->old_integral = $oldIntegral;
        $integralLog->now_integral = $member->integral;
        $integralLog->pay_integral = $totalIntegral;
        $integralLog->create_time = date('Y-m-d H:i:s');
        $integralLog->expand = "参加积分夺宝活动[{$applyGroupKey}]";
        $integralLog->save();

        for ($i=1; $i<=$amount; $i++) {
            $log = new IntergralTreasureLog();
            $log->member_id = (int)$member->id;
            $log->treasure_id = (int)$treasure->id;
            $log->integral = $treasure->goods_integral;
            $log->key = $log->getCreateKey();
            $log->create_time = date('Y-m-d H:i:s');
            $log->apply_group = $applyGroupKey;

            $log->save();
        }

        $treasure->goods_salenum = IntergralTreasureLog::find()->where([
            'treasure_id' => $treasure->id
        ])->count();
        $treasure->save();

        $treasure->goods_salenum >= $treasure->goods_storage and $this->_setTreasure($treasure->id);

        apiSendSuccess("夺宝成功");
    }

    /**
     * 设置获奖人员
     */
    private function _setTreasure($iTreasureId)
    {
        // 获奖人数
        $number = 1;
        $iTreasure = IntergralTreasure::findOne([
            'id' => $iTreasureId,
            'goods_state' => 1
        ]);

        $iTreasure or stopFlow("系统异常");

        $unsetIds = [0];
        $treasure = [];
        for ($i=1; $i<= $number; $i++) {
            $query = IntergralTreasureLog::find()->where([
                'and',
                ['not in', 'id', $unsetIds],
                ['=', 'treasure_id', $iTreasureId]
            ]);

            $amount = $query->count();
            $key = rand(0, $amount-1);
            $ticket = $query->offset($key)->limit(1)->one();
            $treasure[] = $ticket->id;
            $unsetIds[] = $ticket->id;
        }

        $iTreasure->treasure_log_ids = json_encode($treasure);
        $iTreasure->goods_state = 0;
        $iTreasure->end_time = date('Y-m-d H:i:s');
        $iTreasure->save();

        if ($iTreasure->is_virtual) {
            $data = $iTreasure->attributes;

            unset($data['id'], $data['goods_salenum'], $data['goods_collect'], $data['treasure_log_id']);
            $data['goods_addtime'] = date('Y-m-d H:i:s');
            $data['goods_state'] = 1;
            $data['from_id'] >0 or $data['from_id'] = $iTreasure->id;

            $newTreasure = new IntergralTreasure();
            $newTreasure->setAttributes($data);
            $newTreasure->save();
        }
    }

    /**
     * 夺宝记录
     * @author wufeng
     * @date 2019-6-13
     */
    public function actionMyRecord(){
        if(\Yii::$app->user->isGuest){
            return $this->redirect(['index/login']);
        }

        if(\Yii::$app->request->isPost){
            $where = [
                'and',
                ['A.member_id' => \Yii::$app->user->id]
            ];
            $recordList = IntergralTreasureLog::find()->alias('A')
                ->leftJoin('intergral_treasure B','A.treasure_id = B.id')->where($where)->groupBy('A.apply_group')->orderBy('A.id desc')->all();
            $list = [];
            if(!empty($recordList)){
                foreach ($recordList as $key=>$item){
                    $arr = $item->attributes;
                    $arr['goods_name'] = $item->intergralTreasure->goods_name;
                    $arr['goods_pic'] = $item->intergralTreasure->goods_pic;
                    $arr['create_time'] = $item->create_time;
                    $count = IntergralTreasureLog::find()->where([
                        'treasure_id'=>$item->treasure_id,
                        'apply_group'=>$item->apply_group
                    ])->count();
                    $arr['count'] = $count?$count:0;
                    $arr['is_state'] = $item->intergralTreasure->goods_state;
                    $logIds = empty($item->intergralTreasure->treasure_log_ids)?[]:json_decode($item->intergralTreasure->treasure_log_ids,true);
                    $arr['is_selected'] = in_array($item->id,$logIds)?1:0;
                    $list[] = $arr;
                }
            }

            $totalCount = IntergralTreasureLog::find()->alias('A')
                ->leftJoin('intergral_treasure B','A.treasure_id = B.id')->where($where)->count();

            apiSendSuccess('ok',[
               'list'=>$list,
               'totalCount' => $totalCount
            ]);
        }

        return $this->render('my-record');
    }

    /**
     * 最新揭晓
     * @author wufeng
     * @date 2019-6-14
     */
    public function actionRecord(){
        $recordsList = IntergralTreasure::find()->where([
            'and',
            ['=', 'goods_state', 0],
            ['=', 'isdelete', '0']
        ])->orderBy('end_time desc')->limit('10')->all();

        if(!empty($recordsList)){
            foreach ($recordsList as $key=>$item){
                $arr = $item->attributes;
                $log_ids = json_decode($item->treasure_log_ids,true);
                $log = IntergralTreasureLog::find()->where(['in','id',$log_ids])->one();

                $arr['nickname'] = $log->member->nickname;
                $arr['avatar'] = $log->member->avatar?$log->member->avatar:'/public/wap/images/user_head.jpg';

                $count = IntergralTreasureLog::find()->where([
                    'member_id'=>$log->member_id,
                    'treasure_id'=>$log->treasure_id
                ])->count();
                $arr['count'] = $count?$count:0;
                $records[] = (object)$arr;
            }
        }
        return $this->render('record',[
            'records' => $records,
        ]);
    }
}
