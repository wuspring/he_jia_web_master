<?php

namespace wap\controllers;

use DL\Project\CityTicket;
use DL\Project\Store;
use wap\logic\StoreLogic;
use wap\models\Collect;
use wap\models\Goods;
use wap\models\GoodsClass;
use wap\models\History;
use wap\models\Order;
use wap\models\OrderGoods;
use wap\models\StoreGcScore;
use wap\models\Ticket;
use wap\models\TicketApplyGoods;
use wap\models\TicketAsk;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;

class GoodsController extends BaseController
{
    // 列表数量
    protected $listAmount = 10;

    /**
     * 商品列表
     * @author daorli
     * @date 2019-6-14
     *
     * @return string
     */
    public function actionLists()
    {
        // 商品列表类型
        $type = \Yii::$app->request->get('type', 'default');

        $currentCity = $this->getRequestCity();
        // 当前有效店铺
        $useFulStoreIds = StoreLogic::init()->usefulStoreIds($currentCity);

        $categorys = GoodsClass::find()->where([
            'and',
            ['in', 'fid', [1, 2, 3]],
            ['=', 'is_show', 0],
            ['=', 'is_del', 0],
        ])->orderBy('sort desc')->all();

        $firstCategory = [
            'title' => '精品推荐',
            'cid' => 0
        ];

        $tickets = CityTicket::init($currentCity)->currentFactionTickets();
        $ticketIds = array_keys($tickets);

        $query = Goods::find()->alias('g')->leftJoin(['tg' => TicketApplyGoods::tableName()], 'g.id=tg.good_id')->select('tg.good_amount, tg.ticket_id, g.*');
        switch ($type) {
            // 预定享特价
            case TicketApplyGoods::TYPE_ORDER :
                $ticketIds or $ticketIds = [0];

                $title = '预存享特价';
                $goods = $query->where([
                    'and',
                    ['=', 'tg.type', TicketApplyGoods::TYPE_ORDER],
                    ['in', 'tg.ticket_id', $ticketIds],
                    ['=', 'g.isdelete', 0],
                    ['=', 'g.goods_state', 1],
                    ['in', 'g.user_id', $useFulStoreIds]
                ])->orderBy('tg.sort desc, g.admin_score desc')->limit($this->listAmount)->all();
                break;
            // 爆款预约
            case TicketApplyGoods::TYPE_COUPON :
                $ticketIds or $ticketIds = [0];

                $title = '爆款预约';
                $goods = $query->where([
                        'and',
                        ['=', 'tg.type', TicketApplyGoods::TYPE_COUPON],
                        ['in', 'tg.ticket_id', $ticketIds],
                        ['=', 'g.isdelete', 0],
                        ['=', 'g.goods_state', 1],
                        ['in', 'g.user_id', $useFulStoreIds]
                    ])->orderBy('tg.sort desc, g.admin_score desc')->limit($this->listAmount)->all();
                break;
            default :
                $title = '商品列表';
                $firstCategory = [
                    'title' => '所有商品',
                    'cid' => 0
                ];

                $filter = [
                    'and',
                    ['=', 'g.isdelete', 0],
                    ['=', 'g.goods_state', 1],
                    ['in', 'g.user_id', $useFulStoreIds],
                ];
                $ticketIds and $filter[] = ['in', 'tg.ticket_id', $ticketIds];

                $goods = $query->where($filter)->orderBy('tg.sort desc, g.admin_score desc')->limit($this->listAmount)->all();
        }

        return $this->render('lists', [
            'title' => $title,
            'firstCategory' => $firstCategory,
            'categorys' => $categorys,
            'goods' => $goods,
            'type' => $type,
        ]);
    }

    /**
     * 商品列表API接口
     * @author daorli
     * @date 2019-6-14
     */
    public function actionApiLists()
    {
        $p = (int)\Yii::$app->request->get('p', 0);

        // 商品列表类型
        $type = \Yii::$app->request->get('type', 'default');

        $currentCity = $this->getRequestCity();
        // 当前有效店铺
        $useFulStoreIds = StoreLogic::init()->usefulStoreIds($currentCity);

        $tickets = CityTicket::init($currentCity)->currentFactionTickets();
        $ticketIds = array_keys($tickets);
        $query = Goods::find()->alias('g')->leftJoin(['tg' => TicketApplyGoods::tableName()], 'g.id=tg.good_id')->select('tg.good_amount, tg.ticket_id, g.*');

        switch ($type) {
            // 预定享特价
            case TicketApplyGoods::TYPE_ORDER :
                $ticketIds or $ticketIds = [0];
                $filter = [
                    'and',
                    ['=', 'tg.type', TicketApplyGoods::TYPE_ORDER],
                    ['in', 'tg.ticket_id', $ticketIds],
                    ['=', 'g.isdelete', 0],
                    ['=', 'g.goods_state', 1],
                    ['in', 'g.user_id', $useFulStoreIds]
                ];
                $dType = 'order-create';
                break;
            // 爆款预约
            case TicketApplyGoods::TYPE_COUPON :
                $ticketIds or $ticketIds = [0];
                $filter = [
                    'and',
                    ['=', 'tg.type', TicketApplyGoods::TYPE_COUPON],
                    ['in', 'tg.ticket_id', $ticketIds],
                    ['=', 'g.isdelete', 0],
                    ['=', 'g.goods_state', 1],
                    ['in', 'g.user_id', $useFulStoreIds]
                ];
                $dType = 'get-coupon';
                break;
            default :
                $filter = [
                    'and',
                    ['=', 'g.isdelete', 0],
                    ['=', 'g.goods_state', 1],
                    ['in', 'g.user_id', $useFulStoreIds],
                ];
                $ticketIds and $filter[] = ['in', 'tg.ticket_id', $ticketIds];

                $dType = 'default';
        }

        $gcId = (int)\Yii::$app->request->get('cid', 0);
        if ($gcId) {
           $gcs = GoodsClass::getLists($gcId);
           $gcIds = ArrayHelper::getColumn($gcs, 'id');
            $filter[] = ['in', 'g.gc_id', $gcIds];
        }

        $goods = $query->where($filter)->orderBy('tg.sort desc, g.admin_score desc')
            ->offset($p * $this->listAmount)->limit($this->listAmount)->all();

        $hasOrderGoodsIds = [];
        if (\Yii::$app->user->identity) {
            $member = \Yii::$app->user->identity;

            $orders = Order::find()->where([
                'and',
                ['=', 'buyer_id', $member->id],
                ['>', 'add_time', date('Y-m-d', strtotime('-30 days'))],
                ['=', 'type', Order::TYPE_ZHAN_HUI],
                [
                    'or',
                    [
                        'and',
                        ['=', 'ticket_type', Order::TICKET_TYPE_COUPON]
                    ],
                    [
                        'and',
                        ['not in', 'order_state', [Order::STATUS_WAIT]],
                        ['=', 'ticket_type', Order::TICKET_TYPE_ORDER]
                    ]
                ]
            ])->all();

            if ($orders) {
                $orderIds = ArrayHelper::getColumn($orders, 'orderid');

                $hasOrders = OrderGoods::find()->where([
                    'in', 'order_id', $orderIds
                ])->all();

                $hasOrderGoodsIds = ArrayHelper::getColumn($hasOrders, 'goods_id');
            }
        }

        $goodDatas = [];
        foreach ($goods AS $good) {
            $c = $good->attributes;
            $c['good_amount'] = $good->good_amount;
            $c['ticketId'] = $good->ticket_id;
            $storeInfo = $good->user;
            $c['has'] = (int)in_array($good->id, $hasOrderGoodsIds);

            $c['store'] = [
                'nickname' => $storeInfo->nickname,
                'avatar' => $storeInfo->avatar,
                'avatarTm' => $storeInfo->avatarTm,
                'provinces_id' => $storeInfo->provinces_id,
            ];

            $c['g_type'] = $dType;

            unset($c['goods_body'], $c['goods_image'], $c['mobile_body'], $c['goods_jingle'], $c['second_kill_groups']);
            $goodDatas[] = $c;

        }
        apiSendSuccess("ok", [
            'lists' => $goodDatas
        ]);
    }

    /**
     * 商品详情
     * @author wufeng
     * @date 2019-6-11
     * @param $id 商品ID
     * @return string
     */
    public function actionDetail($id)
    {
        try {
            $goods = Goods::findOne($id);
            if(empty($goods) || $goods->goods_state==0){
                throw new ErrorException("商品已下架");
            }
            $goods->goods_image = json_decode($goods->goods_image);
            $shop = Store::init()->info($goods->user_id);
            $goods_info = $goods->attributes;
            $goods_info['coupon_or_order'] = '';
            $goods_info['ticket_id'] = 0;
            //参展商品
            $ticketGoods = TicketApplyGoods::findOne(['good_id'=>$goods->id]);
            $memberId = \Yii::$app->user->id;
            if(!empty($ticketGoods)){
                $goods_info['coupon_or_order'] = $ticketGoods->type;
                $goods_info['ticket_id'] = $ticketGoods->ticket_id;
                $is_collect = 0;
                switch ($ticketGoods->type){
                    case 'ORDER':
                        $type = Collect::TYPE_RESERVE_GOOD;break;
                    default:
                        $type = Collect::TYPE_SUBSCRIBE_GOOD;
                }
                if($memberId){
                    $is_collect = Collect::find()->where([
                        'member_id'=>$memberId,
                        'goods_id'=>$goods->id,
                        'type_wap'=> $type,
                    ])->one();
                    $is_collect = !empty($is_collect)?1:0;
                }
                $goods_info['is_collect'] = $is_collect;
                $goods_info['collect_type'] = $type;
            }

            // 更新商品信息点击量
            if ($memberId) {
                $history = new History();
                $history->member_id = $memberId;
                $history->goods_id = $goods->id;
                $history->add_time = date('Y-m-d');
                $history->create_time = date('Y-m-d H:i:s');
                $history->save();
            }
            $gcId = $this->_getGoodsTopGc($goods->gc_id);
            $storeGcScore = StoreGcScore::findOne([
                'user_id' => $goods->user_id,
                'gc_id' => $gcId
            ]);
            if ($storeGcScore) {
                $storeGcScore->score += 1;
                $storeGcScore->save();
            }

            $goods = (object)$goods_info;
            return $this->render('detail',[
                'goods'=>$goods,
                'shop'=>$shop,
            ]);
        } catch (\Exception $e) {
            stopFlow($e->getMessage());
        }
    }

    /**
     * 商品收藏
     * @author wufeng
     * @date 2019-6-18
     * @return false|string
     */
    public function actionCollect(){
        $data=\Yii::$app->request->get();
        $collectType=isset($data['is_collect']) ?$data['is_collect']:1;
        $goodsId=isset($data['goodsId']) ?$data['goodsId']:0;
        $type = isset($data['type']) ? $data['type']:0;
        $json = array('code' => 0);

        if (\Yii::$app->user->isGuest){
            $json=array('code' => 0,'msg' => '请先登录');
            return json_encode($json);
        }
        $memberId = \Yii::$app->user->id;
        if ($collectType==1){     //取消收藏
            Collect::deleteAll(['type'=>$type,'member_id'=>$memberId,'goods_id'=>$goodsId]);
            $json=array('code' => 1,'msg' => '取消成功');
        }else{

            $collect=new Collect();
            $collect->member_id = $memberId;
            $collect->goods_id = $goodsId;
            $collect->create_time = date('Y-m-d');
            $collect->ticket_id = $data['ticketId'];
            $collect->type = $type;
            $collect->type_wap = $type;
            $collect->save();
            $json=array('code' => 1,'msg' => '收藏成功');
        }
        return json_encode($json);
    }

    private function _getGoodsTopGc($gcId, $sonId=0)
    {
        $gc = GoodsClass::findOne($gcId);
        if (!$gc) {
            throw new \Exception('栏目信息异常');
        }

        if ($gc->fid) {
            return $this->_getGoodsTopGc($gc->fid, $gc->id);
        }

        return $sonId ? : $gcId;
    }
}
