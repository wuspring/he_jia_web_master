<?php


namespace wap\controllers;
use common\models\MemberForm;
use common\models\NavsWap;
use wap\models\Advertising;
use wap\models\FitupAnswer;
use wap\models\FitupAppointment;
use wap\models\FitupAttr;
use wap\models\FitupCase;
use wap\models\FitupProblem;
use wap\models\Member;
use wap\models\MTicketConfig;
use wap\models\News;
use wap\models\Newsclassify;
use wap\models\Ticket;
use wap\models\User;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;


/**
 * AddressController implements the CRUD actions for Address model.
 */
class FitupSchoolController extends BaseController
{

    /**
     * Notes: 装修学堂的首页
     * User: qin
     * Date: 2019/6/12
     * Time: 13:27
     */
    public function  actionIndex($ticketId=0){
        $cityId = $this->getRequestCity();

        $select_menu = 0;
        $session_ticket = Yii::$app->session->get('ticketType');
        if ($session_ticket == 'TICKET'){              //史方写的导航
            $session_ticket_id = Yii::$app->session->get('ticketId');
            $ticketConfig = MTicketConfig::findOne([
                'ticket_id' => $session_ticket_id
            ]);
            $ticketConfig->guiders = decodeJsInputUploadIco($ticketConfig->guiders);
        }else{     //西方的导航
            // 首页菜单
            $homeMenus = NavsWap::find()->where(['provinces_id'=>$cityId,'status'=>1,'type'=>'home'])->limit(5)->orderBy('sort ASC')->all();
            $select_menu = 1;
        }

        $newClass = Newsclassify::find()->andWhere(['fid'=>0])->orderBy("isRdm DESC")->all();

        $problemNum = FitupProblem::find()->andFilterWhere(['is_del' => 0, 'is_show' => 1])->andFilterWhere(['>', 'answer_num', 0])->count();

        $problemStr = FitupProblem::find()->andFilterWhere(['is_del' => 0, 'is_show' => 1])->limit(6)->asArray()->all();
        //装修问题
        $problemStr_list = array_chunk($problemStr, 2);

        $list_ids = Newsclassify::find()->where(['fid'=>1,'isShow'=>1])->all();
        $ids = ArrayHelper::getColumn($list_ids,'id');
        $data=News::find()->andFilterWhere([
            'and',
            ['isShow'=>1],
            ['like','provinces_id',$cityId],
            ['in','cid',$ids]
        ]);
        $paixu='createTime DESC';
        //SORT_DESC  SORT_ASC
        $page = $this->getPagedRows($data, ['order' => $paixu, 'pageSize' => '6', 'rows' => 'models']);

        if (isset($_GET['per-page'])){
            $list=array();
            foreach ($page['models'] as $item){
                $jsonlist['id'] = $item->id;
                $jsonlist['title'] = $item->title;
                $jsonlist['time'] = date('Y-m-d',strtotime($item->createTime));
                $jsonlist['pic'] = empty($item->themeImg)?'/public/wap/images/loading.jpg':$item->themeImg;
                $list[]=$jsonlist;
            }
            $json=array('code'=>1,'pagecount'=>$page['pages']->totalCount,'list'=>$list);

            echo json_encode($json);yii::$app->end();
        }

        //广告位显示
        $fitupAdvertising = Advertising::find()->andWhere(['isShow'=>1,'id'=>11])->one();
        if ($select_menu == 0){
            return $this->render('index',[
                'newClass' => $newClass,
                'problemNum' => $problemNum,
                'problemStr_list' =>$problemStr_list,
                'ticketConfig' =>$ticketConfig,
                'select_menu' =>$select_menu,
                'fitupAdvertising' =>$fitupAdvertising,
                'list' => $page['models'],
                'pages' => $page['pages'],
            ]);
        }else{
            return $this->render('index',[
                'newClass' => $newClass,
                'problemNum' => $problemNum,
                'problemStr_list' =>$problemStr_list,
                'fitupAdvertising' =>$fitupAdvertising,
                'list' => $page['models'],
                'pages' => $page['pages'],
                'home_menus' =>$homeMenus,
                'select_menu' =>$select_menu,
            ]);
        }
    }

    /**
     * 获取当前城市发行票据
     * @author daorli
     * Date: 2019/6/5
     */
    protected function getTicketId()
    {
        $ticket = Ticket::findOne([
            'ca_type' => $this->caType,
            'type' => $this->formType,
            'citys' => $this->getRequestCity(),
            'status' => 1
        ]);

        return $ticket ? $ticket->id : 0;
    }
    /**
     * Notes:判断是否登录
     * User: qin
     * Date: 2019/6/12
     * Time: 18:22
     */
    public  function actionJudgeLogin(){

        if (Yii::$app->user->isGuest){
            $json = array('code'=>1,'msg'=>'请先登录');
        }else{
            $json = array('code'=>0,'msg'=>'已经登录');
        }
        return json_encode($json);
    }
    /**
     * Notes: 装修提问
     * User: qin
     * Date: 2019/6/12
     * Time: 15:04
     */
    public function  actionFitupQuestion(){
        $cityId = $this->getRequestCity();
        //是post保存提交
        if (Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
            $fitProblem = new  FitupProblem();
            $fitProblem->problem = $post['content'];
            $fitProblem->provinces_id = $cityId;
            $fitProblem->member_id = Yii::$app->user->id;
            $fitProblem->answer_num = 0;
            $fitProblem->create_time = date('Y-m-d H:i:s');
            if ($fitProblem->save()){
                $json = array('code'=>1,'msg'=>'提问成功');
            }else{
                $json = array('code'=>0,'msg'=>'提问失败');
            }
            return json_encode($json);
        }

        return $this->render('fitup-question');
    }

    /**
     * Notes: 问答列表
     * User: qin
     * Date: 2019/6/12
     * Time: 15:04
     */
    public function  actionQuestionsList($type = -1){
        $data = FitupProblem::find()->andWhere(['is_show'=>1]);

        if ($type == -1){  //查询所有

        }elseif ($type == 0){       //查询没有回答的
             $data->andWhere(['answer_num'=>0]);
        }elseif ($type == 1){        //查询已经回答的
            $data->andWhere(['>','answer_num',0]);
        }
        $paixu=['sort'=>SORT_DESC,'create_time'=>SORT_DESC];
        //SORT_DESC  SORT_ASC
        $page = $this->getPagedRows($data, ['order' => $paixu, 'pageSize' => '6', 'rows' => 'models']);

        if (isset($_GET['per-page'])){
            $list=array();
            foreach ($page['models'] as $item){
                $member = Member::findOne($item->member_id);
                $jsonlist['id'] = $item->id;
                $jsonlist['title'] = $item->problem;
                $jsonlist['date'] = date('Y-m-d',strtotime($item->create_time));
                $jsonlist['avatar'] = empty($member->avatar)?'/public/wap/images/_temp/img_default_user.jpg':$member->avatar;
                $jsonlist['nickname'] = empty($member->nickname)?'/public/wap/images/_temp/img_default_user.jpg':$member->nickname;
                $jsonlist['num'] = $item->answer_num;
                $list[]=$jsonlist;
            }
            $json=array('code'=>1,'pagecount'=>$page['pages']->totalCount,'list'=>$list,'type' => $type);

            echo json_encode($json);yii::$app->end();
        }

        return $this->render('questions-list',[
            'list' => $page['models'],
            'pages' => $page['pages'],
            'type' => $type
        ]);

    }

    /**
     * Notes: 问答详情
     * param FitupAnswer表的id
     * User: qin
     * Date: 2019/6/12
     * Time: 16:34
     */
    public  function actionQuestionDetail($id = 0){

        $allAnswer = FitupAnswer::find()->andWhere(['problem_id'=>$id])->orderBy('create_time DESC')->all();
        $problem = FitupProblem::findOne($id);
        return $this->render('question-detail',[
            'allAnswer' =>$allAnswer,
            'problem' =>$problem,
            'id'=>$id
        ]);
    }

    /**
     * Notes: 用户回答问题
     * User: qin
     * Date: 2019/6/12
     * Time: 16:52
     * @return string
     */
    public function actionAnswerQuestion($id=0){

        if (Yii::$app->request->isPost){
            $data = Yii::$app->request->post();
            $problem = FitupProblem::findOne($data['id']);
            $fitupAnswer= new  FitupAnswer();
            $fitupAnswer->problem_id= $data['id'];
            $fitupAnswer->member_id= Yii::$app->user->id;
            $fitupAnswer->content= $data['content'];
            $fitupAnswer->create_time= date('Y-m-d H:i:s');
            if ($fitupAnswer->save()){
                $fitupAnswerCount = FitupAnswer::find()->andWhere(['problem_id'=>$data['id']])->count();
                $problem->answer_num =intval($fitupAnswerCount);
                $problem->save();
                $json = array('code'=>1,'msg'=>'回答成功');
            }else{
                $json = array('code'=>0,'msg'=>'回答失败');
            }
            return json_encode($json);
        }
        return $this->render('fitup-answer',[
            'id'=>$id]);
    }

    /**
     * Notes:装修详情
     * User: qin
     * Date: 2019/6/13
     * Time: 9:12
     */
    public function  actionFitupCaseDetail($id){

        $fitupCase = FitupCase::findOne($id);

        $user = User::findOne($fitupCase->user_id);

        $cont= json_decode($fitupCase->tab_text);

        return $this->render('fitup-case-detail',[
            'fitupCase'=>$fitupCase,
            'cont'=>$cont,
            'user'=>$user
        ]);

    }

    /**
     * Notes: 装修预约
     * User: qin
     * Date: 2019/6/13
     * Time: 11:32
     */
    public function actionFitupAppointment(){

        if (Yii::$app->request->isPost){
            $data = Yii::$app->request->post();
            $smsdata=yii::$app->session->get('SMS_CODE');
            if (!empty($smsdata)) {
                $smsdataStr = $data['mobile'] . '-' . $data['smscode'];
                if ($smsdata['value_str'] == $smsdataStr) {
                    $fitupAppoint = new  FitupAppointment();
                    $fitupAppoint->member_id = Yii::$app->user->id;
                    $fitupAppoint->name = $data['name'];
                    $fitupAppoint->mobile = $data['mobile'];
                    $fitupAppoint->user_id = $data['user_id'];
                    $fitupAppoint->create_time = date('Y-m-d H:i:s');
                    $fitupAppoint->save();
                    $json = array('code' => 1, 'msg' => '预约成功!');

                    // 查询并注册手机号为新用户
                    $member = Member::findOne([
                        'mobile' => $data['mobile']
                    ]);
                    // 用户不存在 自动注册
                    if (!$member) {
                        $newPwd = createRandKey();
                        // 新用户注册
                        $member = new Member();
                        $member->username = $member->getNewUserName();
                        $member->mobile = $data['mobile'];
                        $member->wxnick = '用户_' . createRandKey();
                        $member->nickname = $member->wxnick;
                        $member->setPassword($newPwd);
                        $member->status = (string)$member::STATUS_ACTIVE;
                        if(!empty(Yii::$app->session->get('channel'))){
                            $member->channel = Yii::$app->session->get('channel');
                        }
                        $member->save();
                        \DL\Project\Sms::init()->zhuCeHuiYuan($member->id, $newPwd);
                    }
                    // 自动登录
                    $memberForm = new MemberForm();
                    $memberForm->wxlogin($member->id);
                } else {
                    $json = array('code' => 0, 'msg' => '验证码错误!');
                }
            }else{
                $json = array('code' => 0, 'msg' => '验证码无效!');
            }
        }
        return json_encode($json);
    }


    /**
     * Notes:装修案例
     * User: qin
     * Date: 2019/6/13
     * Time: 9:12
     */
    public function  actionFitupCase(){
        $cityId = $this->getRequestCity();
        $getdata = Yii::$app->request->get();
        $hx_type = isset($getdata['hx_type']) && $getdata['hx_type']>0?$getdata['hx_type']:'';
        $fg_type = isset($getdata['fg_type']) && $getdata['fg_type']>0 ?$getdata['fg_type']:'';
        $kj_type = isset($getdata['kj_type']) && $getdata['kj_type']>0 ?$getdata['kj_type']:'';
        $tj_type = isset($getdata['tj_type']) && $getdata['tj_type']>0 ?$getdata['tj_type']:'';
        $zj_type = isset($getdata['zj_type']) && $getdata['zj_type']>0?$getdata['zj_type']:'';

        $data=FitupCase::find()->andFilterWhere([
            'and',
            ['like','provinces_id',$cityId],
            ['house_type'=>$hx_type],
            ['design_style'=>$fg_type],
            ['like','tab',$kj_type],
        ]);
        $paixu='create_time DESC';

        if (!empty($tj_type)){
            $paixu='is_recommend DESC';
        }

        if (!empty($zj_type)){
            $paixu='create_time DESC';
        }

        //SORT_DESC  SORT_ASC
        $page = $this->getPagedRows($data, ['order' => $paixu, 'pageSize' => '6', 'rows' => 'models']);

        if (isset($_GET['per-page'])){
            $list=array();
            foreach ($page['models'] as $item){
                $user = User::findOne($item->user_id);
                $style = FitupAttr::findOne($item->design_style);
                $houseType = FitupAttr::findOne($item->house_type);
                $kj_type_one = empty($getdata['kj_type'])?0:$getdata['kj_type'];
                $houseInType = FitupAttr::findOne($kj_type_one);

                $jsonlist['id'] = $item->id;
                $jsonlist['pic'] = $item->cover_pic;
                $jsonlist['name'] = $item->name;
                $jsonlist['storeName'] = $user->nickname;
                $jsonlist['style'] = $style->name;
                $jsonlist['houseType'] = $houseType->name;
                $jsonlist['area'] = $item->area;
                $jsonlist['houseInType'] = empty($houseInType->name)?'-':$houseInType->name;
                $list[]=$jsonlist;
            }
            $json=array('code'=>1,'pagecount'=>$page['pages']->totalCount,'list'=>$list,'pages' => $page['pages']);

            echo json_encode($json);yii::$app->end();
        }

        $house_type = FitupAttr::find()->andWhere(['pid'=>1])->all();
        $design_style = FitupAttr::find()->andWhere(['pid'=>2])->all();
        $area_type = FitupAttr::find()->andWhere(['pid'=>3])->all();
        $house_type_json=$this->getList($house_type);
        $design_style_json = $this->getList($design_style);
        $area_type_json = $this->getList($area_type);
        return $this->render('fitup-case',[
            'house_type'=>json_encode($house_type_json),
            'design_style' =>json_encode($design_style_json),
            'area_type' =>json_encode($area_type_json),
            'list' => $page['models'],
            'pages' => $page['pages'],
        ]);
    }

    public  function getList($arr=[]){

        foreach ($arr as $k=>$v){
            $list[]=array(
                'id'=>$v->id,
                'value'=>$v->name,
            );
        }
        return $list;
    }
    /**
     * Notes:分页
     * User: qin
     * Date: 2019/6/13
     * Time: 9:09
     * @param $query
     * @param array $config
     * @return array
     */

    public function getPagedRows($query, $config = [])
    {
        $countQuery = clone $query;

        $pages = new Pagination(['totalCount' => $countQuery->count()]);

        if (isset($config['pageSize'])) {
            $pages->setPageSize($config['pageSize'], true);
        }

        $rows = $query->offset($pages->offset)->limit($pages->limit);
        if (isset($config['order'])) {
            $rows = $rows->orderBy($config['order']);
        }
        $rows = $rows->all();

        $rowsLable = 'rows';
        $pagesLable = 'pages';

        if (isset($config['rows'])) {
            $rowsLable = $config['rows'];
        }
        if (isset($config['pages'])) {
            $pagesLable = $config['pages'];
        }

        $ret = [];
        $ret[$rowsLable] = $rows;
        $ret[$pagesLable] = $pages;

        return $ret;
    }



}




