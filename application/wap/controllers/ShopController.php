<?php

namespace wap\controllers;

use DL\Project\CityTicket;
use DL\Project\Store;
use dlExpand\Position;
use wap\logic\StoreLogic;
use wap\models\Collect;
use wap\models\Comment;
use wap\models\GoodsClass;
use wap\models\Ticket;
use wap\models\TicketApply;
use wap\models\TicketApplyGoods;
use wap\models\User;
use yii\helpers\ArrayHelper;

class ShopController extends BaseController
{
    /**
     * 店铺列表
     * @author wufeng
     * @date 2019-6-11
     * @return string
     */
    public function actionIndex()
    {
        $ticketType = $this->getTicketType();
        switch ($ticketType){
            case Ticket::TYPE_JIE_HUN:
                $fid = 2;break;
            case Ticket::TYPE_YUN_YING:
                $fid = 3;break;
            default:
                $fid = 1;
        }
        $category = GoodsClass::find()->where([
            'and',
            ['is_show'=>0],
            ['=','fid',$fid]
        ])->orderBy(['sort'=>SORT_ASC,'createTime'=>SORT_ASC])->all();

        return $this->render('index',[
            'categorys'=> $category
        ]);
    }

    /**
     * 参展品牌
     * @author wufeng
     * @date 2019-6-12
     * @return string
     */
    public function actionBrand(){
        $ticketType = $this->getTicketType();
        switch ($ticketType){
            case Ticket::TYPE_JIE_HUN:
                $fid = 2;break;
            case Ticket::TYPE_YUN_YING:
                $fid = 3;break;
            default:
                $fid = 1;
        }
        $category = GoodsClass::find()->where([
            'and',
            ['is_show'=>0],
            ['=','fid',$fid]
        ])->orderBy(['sort'=>SORT_ASC,'createTime'=>SORT_ASC])->all();

        return $this->render('brand',[
            'categorys'=> $category
        ]);
    }

    /**
     * 商铺列表
     * @author wufeng
     * @date 2019-6-11
     */
    public function actionList(){
        if(\Yii::$app->request->isPost){
            $parms = \Yii::$app->request->post();
            $cityId = $this->getRequestCity();
            $where = [
                'and',
                ['=','u.provinces_id',$cityId],
                ['=','u.status',1],
                ['=', 'assignment', '后台用户']
            ];
            if(isset($parms['fid']) && !empty($parms['fid'])){
                $where = array_merge($where,[
                    ['like','u.gc_ids',$parms['fid']],
                ]);
            }
            if(isset($parms['type']) && $parms['type']=='brand'){

                $tickets = CityTicket::init($cityId)->currentFactionTickets();
                $ticketIds = array_keys($tickets);
                $store_list = TicketApply::find()->where([
                    'and',
                    ['in','ticket_id',$ticketIds],
                    ['status'=>1]
                ])->all();
                $storeIds = ArrayHelper::getColumn($store_list,'user_id');
                $where = array_merge($where,[
                    ['in','u.id',$storeIds]
                ]);
            }

            $options = ['page'=>$parms['page'],'size'=>$parms['size']];
            $list = StoreLogic::init()->storeList($where,$options);

            $totalCount = StoreLogic::init()->storeListCount($where);

            apiSendSuccess('ok',[
               'list'=> $list,
               'totalCount'=> $totalCount,
            ]);
        }

    }

    /**
     * 店铺详情
     * @author wufeng
     * @date 2019-6-11
     * @param $id
     * @return string
     */
    public function actionDetail($id){

        $store_info = StoreLogic::init()->storeInfo($id,true);

        $orderGoodIds = [];
        $orderCouponIds = [];

        $ticketInfos = CityTicket::init($store_info->provinces_id)->currentFactionTickets();
        if ($ticketInfos) {
            $ticketIds = array_keys($ticketInfos);

            $goods = TicketApplyGoods::find()->where([
                'and',
                ['=', 'user_id', $store_info->user_id],
                ['in', 'ticket_id', $ticketIds],
            ])->all();

            foreach ($goods AS $good) {
                switch ($good->type) {
                    case TicketApplyGoods::TYPE_ORDER :
                        $orderGoodIds[$good->good_id] = $good->ticket_id;
                        break;
                    case TicketApplyGoods::TYPE_COUPON :
                        $orderCouponIds[$good->good_id] = $good->ticket_id;
                        break;
                }
            }
        }

        return $this->render('detail',[
            'shop' => $store_info,
            'orderGoodIds' => $orderGoodIds,
            'orderCouponIds' => $orderCouponIds,
        ]);
    }

    /**
     * 门店列表地图
     * @author wufeng
     * @date 2019-6-12
     * @param $storeId 店铺ID
     * @return string
     */
    public function actionShopMap($storeId){
        $ip = $this->getIpPostion();
        $shop_list = StoreLogic::init()->shops($storeId);
        $nowLat = $ip['result']['location']['lat'];
        $nowLng = $ip['result']['location']['lng'];
        $pos = new Position($nowLat,$nowLng);

        if(!empty($shop_list)){
            foreach ($shop_list as $key=>$shop){
                $info = (array)$shop;
                $info['distance'] = $pos->calculateFarByLocation($shop->lat,$shop->lng,2);
                $shop_list[$key] = (object)$info;
            }
        }

        return $this->render('shop-map',[
            'shop_list' => $shop_list
        ]);
    }

    /**
     * 店铺评论-评分
     * @author wufeng
     * @date 2019-6-13
     * @param $storeId 店铺ID
     * @return string
     */
    public function actionScore($storeId){
        $store_info = StoreLogic::init()->storeInfo($storeId,true);

        //店铺评价
        $goodsWithDelete = Store::init()->goods($storeId, [], [], false);
        $goodIdArr=ArrayHelper::getColumn($goodsWithDelete,'id');
        $goodIdArr or $goodIdArr = [0];
        $qualityScore = Comment::find()->andFilterWhere(['in','goods_id',$goodIdArr])->average('spzl');
        $serviceScore = Comment::find()->andFilterWhere(['in','goods_id',$goodIdArr])->average('shfw');


        return $this->render('score',[
            'shop' => $store_info,
            'qualityScore' => $qualityScore?round($qualityScore):0,
            'serviceScore' => $serviceScore?round($serviceScore):0,
        ]);
    }

    /**
     * 店铺评论列表
     * @author wufeng
     * @date 2019-6-13
     */
    public function actionCommentList(){
        if(\Yii::$app->request->isPost){
            $storeId = \Yii::$app->request->post('storeId',0);
            $page = \Yii::$app->request->post('page',1);
            $size = \Yii::$app->request->post('size',10);

            $goodsWithDelete = Store::init()->goods($storeId, [], [], false);
            $goodIdArr=ArrayHelper::getColumn($goodsWithDelete,'id');
            $goodIdArr or $goodIdArr = [0];
            $commentList = Comment::find()->andFilterWhere(['in','goods_id',$goodIdArr])
                ->offset(($page-1)*$size)->limit($size)->orderBy(['create_time'=>SORT_DESC])->all();
            $list = [];
            if(!empty($commentList)){
                foreach ($commentList as $key=>$item){
                    $arr = [];
                    $arr = $item->attributes;
                    $arr['avatar'] = $item->member->avatar;
                    $arr['nickname'] = $item->member->nickname;
                    $arr['imgs'] = json_decode($item->imgs,true);
                    $arr['createDate'] = date('Y-m-d',strtotime($item->create_time));
                    $list[] = $arr;
                }
            }

            $totalCount = Comment::find()->andFilterWhere(['in','goods_id',$goodIdArr])->count();

            apiSendSuccess('ok',[
                'list'=> $list,
                'totalCount'=> $totalCount,
            ]);
        }
    }

    /**
     * 店铺收藏
     * @author wufeng
     * @date 2019-6-14
     * @return false|string
     */
    public function actionCollect(){
        $data=\Yii::$app->request->get();
        $collectType=isset($data['is_collect']) ?$data['is_collect']:1;
        $shop_id=isset($data['shop_id']) ?$data['shop_id']:0;
        $json = array('code' => 0);

        if (\Yii::$app->user->isGuest){
            $json=array('code' => 0,'msg' => '请先登录');
            return json_encode($json);
        }
        $memberId = \Yii::$app->user->id;
        if ($collectType==1){     //取消收藏
            Collect::deleteAll(['type'=>Collect::TYPE_STORE,'member_id'=>$memberId,'shop_id'=>$shop_id]);
            $json=array('code' => 1,'msg' => '取消成功');
        }else{
            $collect=new Collect();
            $collect->member_id = $memberId;
            $collect->shop_id=$shop_id;
            $collect->create_time=date('Y-m-d');
            $collect->type=Collect::TYPE_STORE;
            $collect->type_wap = Collect::TYPE_STORE;
            $collect->save();
            $json=array('code' => 1,'msg' => '收藏成功');
        }
        return json_encode($json);
    }
}
