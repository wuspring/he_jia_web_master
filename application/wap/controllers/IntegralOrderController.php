<?php
namespace wap\controllers;

use wap\models\Address;
use wap\models\IntegralLog;
use wap\models\IntergralGoods;
use wap\models\IntergralOrder;
use wap\models\IntergralOrderGoods;
use wap\models\Member;
use wap\models\OrderGoods;
use wap\models\Provinces;

use DL\service\IntergralOrderService;
use DL\service\UrlService;
use yii\helpers\ArrayHelper;

/**
 * Class IntegralProductlistController
 * @package wap\controllers
 *  积分兑换
 */
class IntegralOrderController extends BaseController
{
    protected $memberInfo;

    public function init()
    {
        parent::init();

        \Yii::$app->user->isGuest and stopFlow("用户未登录");
        $this->memberInfo = \Yii::$app->user->identity;
    }

    /**
     * 创建订单
     * @author daorli
     * @date 2019-6-17
     */
    public function actionCreate()
    {
        $id = \Yii::$app->request->get('id', 0);
        $skuId = (int)\Yii::$app->request->get('sku', 0);
        $amount = (int)\Yii::$app->request->get('amount', 1);

        try {
            $order = IntergralOrderService::init()->create($this->memberInfo, $id, $amount, $skuId);

            $url = UrlService::build(['integral-order/confirm', 'number' => $order->orderid]);
            header("Location: {$url}");
            die;
        } catch (\Exception $e) {
            stopFlow($msg = $e->getMessage());
        }
    }

    /**
     * 确认积分订单信息
     * @author daorli
     * @date 2019-6-17
     *
     * @param string $number
     *
     * @return string
     */
    public function actionConfirm($number)
    {
        $order = IntergralOrder::findOne([
            'orderid' => $number,
            'order_state' => IntergralOrder::STATUS_WAIT_PAY,
        ]);

        $order or stopFlow("未找到待支付的积分订单");
        $orderGoods = IntergralOrderGoods::findAll([
            'order_id' => $order->orderid
        ]);

        $provinces = Provinces::findAll([
            'upid' => 1
        ]);
        $provinces = ArrayHelper::map($provinces,'id','cname');

        $expand = [
            IntergralOrder::EXPAND_WORKDAY,
            IntergralOrder::EXPAND_WEEKEND,
            IntergralOrder::EXPAND_ANYTIME,
        ];
        $addresses = Address::find()->where([
            'memberId' => $this->memberInfo->id,
            'is_delete' => 0
        ])->orderBy('isDefault desc')->all();

        return $this->render('confirm', [
            'order' => $order,
            'orderGoods' => $orderGoods,
            'provinces' => $provinces,
            'addresses' => $addresses,
            'expand' => $expand,
        ]);
    }

    /**
     * 更新订单信息
     * @author daorli
     * @date 2019-6-17
     */
    public function actionUpdate()
    {
        $orderId = \Yii::$app->request->post('number', 0);
        $order = IntergralOrder::findOne([
            'orderid' => $orderId,
            'buyer_id' => $this->memberInfo->id,
            'order_state' => IntergralOrder::STATUS_WAIT_PAY
        ]);

        $order or apiSendError("订单信息未找到");
        $addressId = (int)\Yii::$app->request->post('setAddress', 0);
        if ($addressId) {
            $address = Address::findOne($addressId);

            $order->receiver = $address->id;
            $order->receiver_name = $address->name;
            $order->receiver_mobile = $address->mobile;
            $order->receiver_address = $address->provinceInfo . ' ' . $address->address;
        }

        $expand = \Yii::$app->request->post('expand', '');
        if (strlen($expand)) {
            $order->expand = $expand;
        }

        $order->save() and apiSendSuccess("OK");


        apiSendError("更新订单信息失败");
    }

    /**
     * 积分订单支付
     * @author daorli
     * @date 2019-6-17
     *
     * @param $number
     */
    public function actionPay($number)
    {
        $order = IntergralOrder::findOne([
            'orderid' => $number,
            'order_state' => IntergralOrder::STATUS_WAIT_PAY,
        ]);
        $order or stopFlow("未找到待支付的积分订单");

        $member = Member::findOne($this->memberInfo->id);
        if ($member->integral < $order->order_amount) {
            stopFlow("账户积分不足",'order/index');
        }

        $oldIntegral = $member->integral;
        $member->integral = $member->integral - $order->integral_amount;

        $integralLog = new IntegralLog();
        $integralLog->orderId = $order->orderid;
        $integralLog->memberId = $this->memberInfo->id;
        $integralLog->old_integral =$oldIntegral;
        $integralLog->now_integral = $member->integral;
        $integralLog->pay_integral = $order->integral_amount;
        $integralLog->create_time = date('Y-m-d H:i:s');

        $url = UrlService::build(['integral-order/pay-failed', 'number' => $order->orderid]);
        if ($integralLog->save()) {
            $order->order_state = $order::STATUS_WAIT_SEND;
            $order->save();

            $url = UrlService::build(['integral-order/pay-success', 'number' => $order->orderid]);
        };

        header("Location: {$url}");
        die();
    }

    public function actionPaySuccess($number)
    {
        $order = IntergralOrder::findOne($number);
        $order or stopFlow("未找到有效的积分订单");

        $goods = IntergralGoods::find()->where([
            'isdelete' => 0,
            'goods_state' => 1,
        ])->limit(6)->all();

        return $this->render('pay_suc', [
            'goods' => $goods,
            'order' => $order
        ]);
    }

    public function actionPayFailed($number)
    {
        $order = IntergralOrder::findOne($number);
        $order or stopFlow("未找到有效的积分订单");

        $goods = IntergralGoods::find()->where([
            'isdelete' => 0,
            'goods_state' => 1,
        ])->limit(6)->all();

        return $this->render('pay_fail', [
            'order' => $order,
            'goods' => $goods
        ]);
    }
}
