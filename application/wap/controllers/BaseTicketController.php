<?php
namespace wap\controllers;

use common\models\Service;
use DL\Project\CityExpand;
use DL\vendor\ConfigService;
use wap\logic\StoreLogic;
use wap\models\FitupCase;
use wap\models\GoodsClass;
use wap\models\GoodsCoupon;

use wap\models\Member;
use wap\models\News;
use wap\models\Newsclassify;
use wap\models\StoreGcScore;
use wap\models\Ticket;
use wap\models\TicketApply;
use wap\models\TicketApplyGoods;
use wap\models\TicketAsk;
use wap\models\MTicketConfig;
use common\models\MemberForm;
use DL\Project\Sms;
use DL\service\CacheService;
use common\models\Goods;
use DL\service\UrlService;
use yii\helpers\ArrayHelper;

/**
 * Class BaseTicketController
 * 展会基类
 * @author daorli
 *
 * @package wap\controllers
 */
abstract class BaseTicketController extends BaseController
{
    // 票类型
    protected $formType;
    // 票分类
    protected $caType=Ticket::CA_TYPE_ZHAN_HUI;

    protected $ticketAmountKey;

    public function init()
    {
        parent::init();

        $session = \Yii::$app->session;
        $session->set('ticketType', 'TICKET');
    }

    /**
     * 索票页
     * @author daorli
     * Date: 2019/6/5
     *
     * @return string
     *
     * @throws \Exception
     */
    public function actionIndex($ticketId=0)
    {
        $ticketId = $ticketId ? $ticketId : $this->getTicketId();
        $ticket = false;

        $ticketConfig = false;

        $newses = [];

        if ($ticketId) {
            $ticket = Ticket::findOne($ticketId);
            $storeApplys = TicketApply::findAll([
                'ticket_id' => $ticket->id,
                'status' => 1
            ]);

            $ticketConfig = MTicketConfig::findOne([
                'ticket_id' => $ticket->id
            ]);

            $ticketConfig or $ticketConfig = new MTicketConfig();

            $ticketConfig->banners = decodeJsInputUpload($ticketConfig->banners);
            $ticketConfig->ad_1 = decodeJsInputUpload($ticketConfig->ad_1);
            $ticketConfig->ad_2 = decodeJsInputUpload($ticketConfig->ad_2);
            $ticketConfig->ad_3 = decodeJsInputUpload($ticketConfig->ad_3);
            $ticketConfig->ad_4 = decodeJsInputUpload($ticketConfig->ad_4);

            $ticketConfig->guiders = decodeJsInputUploadIco($ticketConfig->guiders);
            $ticketConfig->top_guiders = decodeJsInputUploadIco($ticketConfig->top_guiders);

            $classifies = Newsclassify::findAll([
                'fid' => 1
            ]);
            $classifyIds = $classifies ? ArrayHelper::getColumn($classifies, 'id') : [0];

            $newses = News::find()->where([
                'and',
                ['=', 'type', $ticket->type],
                ['in', 'cid', $classifyIds],
                ['=', 'isShow', 1],
                ['=', 'isRmd', 1],
            ])->orderBy('sort desc')->limit(3)->all();
        } else {
            $ticket = new Ticket();
        }

        $uKey = $this->getSessionKey($ticketId);
        $formData = CacheService::init()->get($uKey);
        $formData or $formData = [];

        //预存 享特价
        $teJiaGoods = Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 'g.id=t.good_id')->andFilterWhere(['t.ticket_id'=>$ticketId,'t.type'=>TicketApplyGoods::TYPE_ORDER])->orderBy(' t.sort desc')->limit(3)->all();

        //爆款预约
        $baoKuanGood = Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 'g.id=t.good_id')->andFilterWhere(['t.ticket_id'=>$ticketId,'t.type'=>TicketApplyGoods::TYPE_COUPON])->orderBy(' t.sort desc')->limit(3)->all();

        $applys = TicketApply::find()->where([
            'and',
            ['=', 'ticket_id', $ticketId],
            ['=', 'status', 1],
        ])->all();
        $storeIds = $applys ? ArrayHelper::getColumn($applys, 'user_id') : [0];

        // 参展店铺
        $storeGcScores = \wap\models\StoreGcScore::findBySql('select *, sum(`admin_score`) AS `total_score` from `store_gc_score` where `user_id` in (' . implode(',', $storeIds) . ') group by `user_id` order by `total_score` desc limit 0, 12')->all();

        // 装修案例
        $storeCases = FitupCase::findBySql('select `f`.* from `fitup_case` as `f` left join (select *, sum(`admin_score`) AS `total_score` from `store_gc_score` group by `user_id`) as `sc` on `sc`.`user_id`=`f`.`user_id` where `f`.`user_id` in (' . implode(',', $storeIds) . ') order by `sc`.`total_score` desc limit 0, 4')->all();

        \Yii::$app->session->set('ticketId', $ticketId);

        // 品质承诺
        $cityId = $this->getRequestCity();
        $serviceList = Service::find()->where(['provinces_id'=>$cityId,'pos'=>Service::POS_HOME])->orderBy('sort ASC')->all();

        return $this->render('../base-ticket/index', [
            'ticketConfig' => $ticketConfig,
            'ticketId' => $ticketId,
            'ticket' => $ticket,
            'teJiaGoods' => $teJiaGoods,
            'baoKuanGood' => $baoKuanGood,
            'storeGcScores' => $storeGcScores,
            'storeCases' => $storeCases,
            'newes' => $newses,
            'serviceList' => $serviceList,
        ]);
    }

    /**
     * 请求索票
     * @author daorli
     * Date: 2019/6/5
     */
    public function actionAskTicket()
    {
        $post = \Yii::$app->request->post();
        $ticketId = isset($post['ticketId']) ? $post['ticketId'] : '0';

        $to = isset($post['t']) ? $post['t'] : 'index/index';

        $formData = $post;
        $name = isset($post['name']) ? $post['name'] : '未填写(wap端)';
        $mobile = isset($post['mobile']) ? $post['mobile'] : $formData['mobile'];
        $address = isset($post['address']) ? $post['address'] : '';
        $cityId = isset($post['cityId']) ? $post['cityId'] : '#';
        $type = isset($post['type']) ? $post['type'] : '#';
        $resource = isset($post['resource']) ? $post['resource'] : '';
        $code = isset($post['code']) ? $post['code'] : '###';

        $session = \Yii::$app->session;
        $smsCode = $session->get('SMS_CODE');
        $smsCode or stopFlow("验证码无效", $to);
        $smsCode['expire'] > time()  or stopFlow("验证码已过期", $to);
        "{$code}" == $smsCode['value'] or stopFlow("请输入正确的验证码", $to);

        // 查询并注册手机号为新用户
        $member = Member::findOne([
            'mobile' => $mobile
        ]);

        $signMember = false;
        // 用户不存在 自动注册
        if (!$member) {
            $signMember = createRandKey();
            // 新用户注册
            $member = new Member();
            $member->username = $member->getNewUserName();
            $member->mobile = $mobile;
            $member->wxnick = '用户_' . createRandKey();
            $member->nickname = $member->wxnick;
            $member->setPassword($signMember);
            $member->status = (string)$member::STATUS_ACTIVE;
            $member->save();
        }
        // 自动登录
        $memberForm = new MemberForm();
        $memberForm->wxlogin($member->id);

        $ticketId or stopFlow("当前城市并未开展活动", $to);

        $time = time();
        $ticket = Ticket::findOne($ticketId);
        (strtotime($ticket->ask_ticket_start_date)) < $time or stopFlow("展会领票时间尚未开始");
        (strtotime($ticket->ask_ticket_end_date) + 3600 * 24) > $time or stopFlow("展会领票时间已截止");

        if (!strlen($name) or !preg_match('/^1\d{10}$/',$mobile)) {
            stopFlow("请完善收件人信息", $to);
        }

        $ticket = Ticket::findOne([
            'id' => $ticketId,
            'status' => Ticket::STATUS_NORMAL,
            'type' => $type
        ]);
        $ticket or stopFlow("该票已下线，下次快点哦", $to);

        $ticketAsk = TicketAsk::findOne([
            'ticket_id' => $ticket->id,
            'mobile' => $mobile
        ]);
        $ticketAsk and stopFlow("您已成功申请，不要贪心哦", $to);

        $ticketAsk = new TicketAsk();
        $ticketAsk->name = $name;
        $ticketAsk->member_id = $member->id;
        $ticketAsk->provinces_id = $cityId;
        $ticketAsk->ticket_id = $ticketId;
        $ticketAsk->mobile = $mobile;
        $ticketAsk->address = $address;
        $ticketAsk->resource = $resource;
        $channel=\yii::$app->session->get('channel');
        if (!empty($channel)){
            $ticketAsk->channel= $channel;
        }
        $ticketAsk->status = 0;
        $ticketAsk->create_time = date('Y-m-d H:i:s');

        \Yii::$app->session->set('ticketId', $ticketId);

        if ($ticketAsk->save()) {
            $post['getTicketId'] = $ticketAsk->id;
            $amount = TicketAsk::find()->where([
                'ticket_id' => $ticketId
            ])->sum('ticket_amount');

            $this->ticketAmountKey($ticketId, $amount);
        }

        Sms::init()->askTicktet($mobile, $ticketAsk->ticket_id);
        $signMember !== false and Sms::init()->zhuCeHuiYuan($ticketAsk->member_id, $signMember);

        header('Location: ' . UrlService::build(['ticket-address', 'ask_id' => $ticketAsk->id]));
        die();
    }

    /**
     * 请求索票 - ajax
     * @author wufeng
     * Date: 2019/12/11
     */
    public function actionAskTicketByAjax(){
        $post = \Yii::$app->request->post();
        $ticketId = isset($post['ticketId']) ? $post['ticketId'] : '0';

        $to = isset($post['t']) ? $post['t'] : 'index/index';

        $formData = $post;
        $name = isset($post['name']) ? $post['name'] : '未填写(wap端)';
        $mobile = isset($post['mobile']) ? $post['mobile'] : $formData['mobile'];
        $address = isset($post['address']) ? $post['address'] : '';
        $cityId = isset($post['cityId']) ? $post['cityId'] : '#';
        $type = isset($post['type']) ? $post['type'] : '#';
        $resource = isset($post['resource']) ? $post['resource'] : '';
        $code = isset($post['code']) ? $post['code'] : '###';

        $session = \Yii::$app->session;
        $smsCode = $session->get('SMS_CODE');
        $smsCode or apiSendError("验证码无效");
        $smsCode['expire'] > time()  or apiSendError("验证码已过期");
        "{$code}" == $smsCode['value'] or apiSendError("请输入正确的验证码");

        // 查询并注册手机号为新用户
        $member = Member::findOne([
            'mobile' => $mobile
        ]);

        $signMember = false;
        // 用户不存在 自动注册
        if (!$member) {
            $signMember = createRandKey();
            // 新用户注册
            $member = new Member();
            $member->username = $member->getNewUserName();
            $member->mobile = $mobile;
            $member->wxnick = '用户_' . createRandKey();
            $member->nickname = $member->wxnick;
            $member->setPassword($signMember);
            $member->status = (string)$member::STATUS_ACTIVE;
            $member->save();
        }
        // 自动登录
        $memberForm = new MemberForm();
        $memberForm->wxlogin($member->id);

        $ticketId or apiSendError("当前城市并未开展活动");

        $time = time();
        $ticket = Ticket::findOne($ticketId);
        (strtotime($ticket->ask_ticket_start_date)) < $time or apiSendError("展会领票时间尚未开始");
        (strtotime($ticket->ask_ticket_end_date) + 3600 * 24) > $time or apiSendError("展会领票时间已截止");

        if (!strlen($name) or !preg_match('/^1\d{10}$/',$mobile)) {
            apiSendError("请完善收件人信息");
        }

        $ticket = Ticket::findOne([
            'id' => $ticketId,
            'status' => Ticket::STATUS_NORMAL,
            'type' => $type
        ]);
        $ticket or apiSendError("该票已下线，下次快点哦");

        $ticketAsk = TicketAsk::findOne([
            'ticket_id' => $ticket->id,
            'mobile' => $mobile
        ]);
        $ticketAsk and apiSendError("您已成功申请，不要贪心哦");

        $ticketAsk = new TicketAsk();
        $ticketAsk->name = $name;
        $ticketAsk->member_id = $member->id;
        $ticketAsk->provinces_id = $cityId;
        $ticketAsk->ticket_id = $ticketId;
        $ticketAsk->mobile = $mobile;
        $ticketAsk->address = $address;
        $ticketAsk->resource = $resource;
        $channel=\yii::$app->session->get('channel');
        if (!empty($channel)){
            $ticketAsk->channel= $channel;
        }
        $ticketAsk->status = 0;
        $ticketAsk->create_time = date('Y-m-d H:i:s');

        \Yii::$app->session->set('ticketId', $ticketId);

        if ($ticketAsk->save()) {
            $post['getTicketId'] = $ticketAsk->id;
            $amount = TicketAsk::find()->where([
                'ticket_id' => $ticketId
            ])->sum('ticket_amount');

            $this->ticketAmountKey($ticketId, $amount);
        }

        Sms::init()->askTicktet($mobile, $ticketAsk->ticket_id);
        $signMember !== false and Sms::init()->zhuCeHuiYuan($ticketAsk->member_id, $signMember);

        $toUrl = UrlService::build(['ticket-address', 'ask_id' => $ticketAsk->id]);
        apiSendSuccess("ok",[
            'url'=> $toUrl
        ]);
    }

    /**
     * 完善地址信息
     * @author daorli
     * @date 2019-6-6
     *
     * @param int $ask_id
     *
     * @return string
     */
    public function actionTicketAddress($ask_id=0)
    {
        $member = \Yii::$app->user->identity;
        $member or stopFlow("您尚未登录");

        $ask_id = (int)$ask_id;
        $ask_id or $ask_id = (int)\Yii::$app->request->post('ask_id', 0);
        $ticketAsk = TicketAsk::findOne(['id' => $ask_id, 'member_id' => $member->id]);
        $ticketAsk or stopFlow("未找到申请的信息", 'index');

        if (\Yii::$app->request->isPost) {
            if (\Yii::$app->user->isGuest) {
                apiSendError("您尚未登录");
            }

            $member = \Yii::$app->user->identity;
            if ($member->id != $ticketAsk->member_id) {
                apiSendError("申请信息与当前用户不符");
            }
            if ($ticketAsk->status) {
                apiSendError("该申请收货信息已确认");
            }

            $ticketAsk->address = \Yii::$app->request->post('address', '');
            $ticketAsk->save() and apiSendSuccess("保存成功");

        }

        $ticket = $ticketAsk->ticket;
        $erWeiMa = ConfigService::init('system')->get('erweima');
        $ticketConfig = MTicketConfig::findOne([
            'ticket_id' => $ticket->id
        ]);

        $ticketConfig or $ticketConfig = new MTicketConfig();
        $ticketConfig->ad_ticket_address = decodeJsInputUpload($ticketConfig->ad_ticket_address);

        switch ($ticket->type) {
            case $ticket::TYPE_JIA_ZHUANG :
                $ticketTypeName = '家博会';
                break;
            case $ticket::TYPE_JIE_HUN :
                $ticketTypeName = '婚庆会';
                break;
            case $ticket::TYPE_YUN_YING :
                $ticketTypeName = '孕婴会';
                break;
        }


        return $this->render('../base-ticket/ticket_address', [
            'showPromise' => 1,
            'indexIndex' => 5,
            'weiXinHao' => 'HEJIA-7878',
            'erWeiMa' => translateAbsolutePath($erWeiMa),
            'ticketAsk' => $ticketAsk,
            'ticketTypeName' => $ticketTypeName,
            'ticket' => $ticket,
            'ticketConfig' => $ticketConfig,
        ]);
    }

    /**
     * 索票页
     * @param int $ticketId 展会ID
     * @return string
     * @throws \Exception
     */
    public function actionAsk($ticketId = 0)
    {
        $ticketId = $ticketId ? $ticketId : $this->getTicketId();
        $ticketConfig = false;

        $goodCoupons = [];
        $joinTicketStoreIds = [0];

        $ticket = Ticket::findOne($ticketId);

        $joinStoreGc = [];

        $phone = ConfigService::init('system')->get('telephone');

        \Yii::$app->session->set('ticketId', $ticketId);

        //家装类型
        $goodClass = GoodsClass::find()->andFilterWhere(['fid'=>1])->limit(7)->orderBy("id ASC")->all();
        if ($ticket) {
            $ticketConfig = MTicketConfig::findOne([
                'ticket_id' => $ticket->id
            ]);

            $ticketConfig or $ticketConfig = new MTicketConfig();

            $ticketConfig->banners = decodeJsInputUpload($ticketConfig->banners);

            $ticketConfig->ad_1 = decodeJsInputUpload($ticketConfig->ad_1);
            $ticketConfig->ad_2 = decodeJsInputUpload($ticketConfig->ad_2);
            $ticketConfig->ad_3 = decodeJsInputUpload($ticketConfig->ad_3);
            $ticketConfig->ad_4 = decodeJsInputUpload($ticketConfig->ad_4);
            $ticketConfig->guiders = decodeJsInputUploadIco($ticketConfig->guiders);
            $ticketConfig->top_guiders = decodeJsInputUploadIco($ticketConfig->top_guiders);
            $ticketConfig->star = decodeJsInputUpload($ticketConfig->star);
            $ticketConfig->guider_pic = explode(',', $ticketConfig->guider_pic);

            $ticketConfig->spbanners = decodeJsInputUpload($ticketConfig->spbanners);
            $ticketConfig->spbg_1 = explode(',', $ticketConfig->spbg_1);
            $ticketConfig->spbg_2 = explode(',', $ticketConfig->spbg_2);
            $ticketConfig->spbg_3 = explode(',', $ticketConfig->spbg_3);

            $ticketConfig->hot_pics = explode(',', $ticketConfig->hot_pics);
            $ticketConfig->spad_1 = decodeJsInputUpload($ticketConfig->spad_1);
            $storeApplys = TicketApply::findAll([
                'ticket_id' => $ticket->id,
                'status' => 1
            ]);

            if ($storeApplys) {
                $uStoreIds = StoreLogic::init()->usefulStoreIds();
                $joinTicketStoreIds = arrayGroupsAction($storeApplys, function ($storeApply) {
                    return $storeApply->user_id;
                });

                $joinTicketStoreIds = array_intersect($joinTicketStoreIds, $uStoreIds);
                $joinTicketStoreIds or $joinTicketStoreIds = [0];
            }

            //品牌优惠劵
            $goodCoupons = GoodsCoupon::find()->andFilterWhere([
                'and',
                ['in', 'user_id', $joinTicketStoreIds],
                ['>', 'num', 0],
                ['=', 'is_show', 1],
                ['=', 'is_del', 0],
            ])->limit($ticket->couple_nums)->all();


            //获取合作品牌
            $jieHunType=ArrayHelper::getColumn($goodClass,'id');
            $joinStore_gc=[];
            $storeGcScore=StoreGcScore::find()->select('*, `score` * `admin_score` as `a_score`')->andFilterWhere([
                'and',
                ['in','gc_id',$jieHunType],
                ['=', 'provinces_id', $ticket->citys],
                ['in', 'user_id', $joinTicketStoreIds]
            ])->orderBy('a_score desc')->all();
            foreach ($storeGcScore as $store){
                $joinStoreGc[$store->gc_id][] = StoreLogic::init()->info($store->user_id);
            }

            $cityConfig = CityExpand::init()->get($ticket->citys, CityExpand::TYPE_CITY_EXPAND);

            $cityConfig and $phone = $cityConfig->tel;
        }

        $uKey = $this->getSessionKey($ticketId);
        $formData = CacheService::init()->get($uKey);
        $formData or $formData = [];

        $teJiaGoodsAmount = Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 'g.id=t.good_id')->andFilterWhere(['t.ticket_id'=>$ticketId,'t.type'=>TicketApplyGoods::TYPE_ORDER])->count();
        $baoKuanGoodAmount = Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 'g.id=t.good_id')->andFilterWhere(['t.ticket_id'=>$ticketId,'t.type'=>TicketApplyGoods::TYPE_COUPON])->count();
        $p1 = rand(1, intval($teJiaGoodsAmount / 6));
        $p2 = rand(1, intval($baoKuanGoodAmount / 6));

        //预存 享特价
        $teJiaGoods = Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 'g.id=t.good_id')->andFilterWhere(['t.ticket_id'=>$ticketId,'t.type'=>TicketApplyGoods::TYPE_ORDER])->select(['g.*', 't.good_amount'])->orderBy(' t.sort desc')->offset(6 *($p1-1))->limit(6)->all();

        //爆款预约
        $baoKuanGood = Goods::find()->alias('g')->leftJoin(['t' => TicketApplyGoods::tableName()], 'g.id=t.good_id')->andFilterWhere(['t.ticket_id'=>$ticketId,'t.type'=>TicketApplyGoods::TYPE_COUPON])->select(['g.*', 't.good_amount'])->orderBy(' t.sort desc')->offset(6 *($p2-1))->limit(6)->all();

        $askAmount = TicketAsk::find()->where([
            'ticket_id' => $ticketId
        ])->count();
        $askAmount=$askAmount+$ticket->virtual_number;
        $askLists = TicketAsk::find()->where([
            'ticket_id' => $ticketId
        ])->orderBy('id desc')->limit(10)->all();


        // 品质承诺
        $cityId = $this->getRequestCity();
        $serviceList = Service::find()->where(['provinces_id'=>$cityId,'pos'=>Service::POS_TICKET])->orderBy('sort ASC')->all();

        return $this->render('../base-ticket/ask', [
            'phone' => $phone,
            'ticketId' => $ticketId,
            'ticketConfig' => $ticketConfig,
            'askAmount' => $askAmount,
            'askLists' => $askLists,
            'ticket' => $ticket,
            'goodCoupons' => $goodCoupons,
            'goodClass' => $goodClass,
            'teJiaGoods' => $teJiaGoods,
            'baoKuanGood' => $baoKuanGood,
            'joinStoreGc' => $joinStoreGc,
            'serviceList' => $serviceList,
        ]);
    }

    /**
     * 获取当前城市发行票据
     * @author daorli
     * Date: 2019/6/5
     */
    protected function getTicketId()
    {
        $ticket = Ticket::findOne([
            'ca_type' => $this->caType,
            'type' => $this->formType,
            'citys' => $this->getRequestCity(),
            'status' => 1
        ]);

        return $ticket ? $ticket->id : 0;
    }

    /**
     * 获取票据key
     * @author daorli
     * Date: 2019/6/5
     *
     * @param $ticketId
     * @param bool $amount
     *
     * @return int
     */
    public function ticketAmountKey($ticketId, $amount=false)
    {
        $ticketAmountKey = "AMOUNT_{$this->formType}_{$this->caType}_$ticketId";

        if ($amount === false) {
            $result = CacheService::init()->get($ticketAmountKey);

            if ($result === false) {
                $result = TicketAsk::find()->where([
                    'ticket_id' => $ticketId
                ])->sum('ticket_amount');

                CacheService::init()->set($ticketAmountKey, $amount);
            }

            return (int)$result;
        }

        CacheService::init()->set($ticketAmountKey, $amount);
    }

    /**
     * 获取session key
     * @author daorli
     * Date: 2019/6/5
     * 
     * @param $ticketId
     *
     * @return string
     */
    protected function getSessionKey($ticketId)
    {
        session_status()==2 or session_start();

        return "{$this->formType}_{$ticketId}_" . session_id();
    }
}
