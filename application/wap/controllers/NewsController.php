<?php

namespace wap\controllers;

use wap\logic\NewsLogic;
use wap\models\News;
use wap\models\Newsclassify;
use yii\helpers\ArrayHelper;

class NewsController extends BaseController
{
    /**
     * 资讯列表
     * @author wufeng
     * @date 2019-6-10
     * @return string
     */
    public function actionNewsList(){
        $cityId = $this->getRequestCity();

        if(\Yii::$app->request->isPost){

            $parms = \Yii::$app->request->post();
            $list_ids = Newsclassify::find()->where(['fid'=>$parms['fid'],'isShow'=>1])->all();
            $ids = ArrayHelper::getColumn($list_ids,'id');
            $where = [
                'and',
                ['isShow'=>1],
                ['like','provinces_id',$cityId],
                ['in','cid',$ids]
            ];
            $list = NewsLogic::newsList($where);

            $totalCount = NewsLogic::getNewsCount($where);
            apiSendSuccess('ok',[
                'list'=>$list,
                'totalCount' => $totalCount
            ]);
        }
        $newsClass = Newsclassify::find()->andFilterWhere(['fid'=>0,'isShow'=>1])->limit(8)->all();

        return $this->render('news-list',[
           'cats'=>$newsClass,
        ]);
    }

    /**
     * Notes: 装修学堂跳过来的
     * param  $fid 二级分类的id
     * User: qin
     * Date: 2019/6/12
     * Time: 14:08
     * @param int $fid
     */
    public function actionFitupNewsList($fid=0){

        $cityId = $this->getRequestCity();

        if(\Yii::$app->request->isPost){

            $parms = \Yii::$app->request->post();
            $where = [
                'and',
                ['isShow'=>1],
                ['like','provinces_id',$cityId],
                ['cid'=>$parms['classId']]
            ];
            $list = NewsLogic::newsList($where);

            $totalCount = NewsLogic::getNewsCount($where);
            apiSendSuccess('ok',[
                'list'=>$list,
                'totalCount' => $totalCount
            ]);
        }
        $newsClass = Newsclassify::find()->andFilterWhere(['fid'=>$fid,'isShow'=>1])->all();

         if (!empty($newsClass)){
             $classId = $newsClass[0]['id'];
         }else{
             $classId=0;
         }
        return $this->render('fitup-news-list',[
            'cats'=>$newsClass,
            'classId'=>$classId
        ]);
    }

    /**
     * 资讯详情
     * @author wufeng
     * @date 2019-6-10
     * @param $id
     * @return string
     */
    public function actionNewsDetail($id){
        $model = News::findOne($id);
        // 增加点击量
        NewsLogic::clickInt($id);
        return $this->render('news-detail',[
            'model'=>$model
        ]);
    }

    /**
     * @author wufeng
     * @date 2019-6-10
     * 品牌活动
     * @return string
     */
    public function actionBrandActivity()
    {
        $cityId = $this->getRequestCity();
        $where = [
            'and',
            ['cid'=>0,'isShow'=>1],
            ['like','provinces_id',$cityId]
        ];
        if(\Yii::$app->request->isPost){
            $list = NewsLogic::newsList($where);
            apiSendSuccess('ok',[
                'list'=>$list
            ]);
        }

        $totalCount = NewsLogic::getNewsCount($where);
        return $this->render('brand-activity',[
            'totalCount' => $totalCount
        ]);
    }

    /**
     * 品牌详情
     * @author wufeng
     * @date 2019-6-10
     * @param $id
     * @return string
     */
    public function actionBrandDetail($id){
        $model = News::findOne($id);
        return $this->render('brand-detail',[
            'model'=>$model
        ]);
    }
}
