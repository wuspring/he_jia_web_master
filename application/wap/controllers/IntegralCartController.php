<?php
/**
 * Created by PhpStorm.
 * User: 何孝林
 * Date: 2018/7/10
 * Time: 16:02
 */

namespace wap\controllers;
use DL\service\UrlService;
use wap\models\Address;
use wap\models\Cart;
use wap\models\IntergralGoods;
use wap\models\IntergralGoodsSku;
use wap\models\IntergralOrder;
use wap\models\IntergralOrderGoods;
use yii\web\Controller;

use \Yii;

class IntegralCartController extends BaseController
{
    /**
     * 购物车
     *
     * @author daorli
     * @date 2019-6-17
     *
     * @return string
     */
    public function actionIndex()
    {
        \Yii::$app->user->isGuest and stopFlow("用户未登录");

        $memberInfo = \Yii::$app->user->identity;
        $carts = Cart::findAll([
            'buyerId' => (int)$memberInfo->id,
//            'goods_type' => Cart::TYPE_INTERGRAL,
        ]);
        $amount = Cart::find()->where([
            'buyerId' => $memberInfo->id,
//            'goods_type' => Cart::TYPE_INTERGRAL,
            'ispay' => 1
        ])->count();

        $totalPrice = Cart::find()->where([
            'buyerId' => $memberInfo->id,
//            'goods_type' => Cart::TYPE_INTERGRAL,
            'ispay' => 1
        ])->sum('totalPrice');
        $totalPrice or $totalPrice = 0;

        $tmp = count($carts) ? 'index' : 'empty';
        return $this->render($tmp, [
            'carts' => $carts,
            'amount' => $amount,
            'totalPrice' => $totalPrice,
        ]);
    }


    /**
     * create
     * 创建购物车订单
     */
    public function actionCreateOrder()
    {
        $get = \Yii::$app->request->get();

        isset($get['type']) or $get['type'] = 'price';

        $carts = Cart::findAll([
            'buyerId' => \Yii::$app->user->id,
        ]);

        $scOrder  = new IntergralOrder();
        $scOrder->orderid = $scOrder->getNewNumber();

        $price = Cart::find()->where([
            'buyerId' => $this->memberInfo->id,
            'ispay' => 1
        ])->sum('totalPrice');
        foreach ($carts AS $cart) {
            if (((int)$cart->ispay)) {
                $cart->delete();
            }
        }

        $scOrder->goods_amount = $price;
        $scOrder->order_amount = $price;
        $scOrder->integral_amount = (int)$price;

        // 默认不使用优惠券
        $scOrder->buyer_id = (int)$this->memberInfo->id;
        $scOrder->buyer_name = $this->memberInfo->nickname;

        $scOrder->order_state = $scOrder::STATUS_WAIT_PAY;

        $scOrder->receiver = 0;
        $scOrder->add_time = date('Y-m-d H:i:s');

        $defaultAddress = Address::findOne([
            'memberId' => $this->memberInfo->id,
            'isDefault' => 1,
            'is_delete' => 0
        ]);
        if ($defaultAddress) {
            $scOrder->receiver = $defaultAddress->id;
            $scOrder->receiver_name = $defaultAddress->name;
            $scOrder->receiver_mobile = $defaultAddress->mobile;
            $scOrder->receiver_address = $defaultAddress->provinceInfo . ' ' . $defaultAddress->address;
        } else {
            $scOrder->receiver = 0;
        }

        $scOrder->order_state = $scOrder::STATUS_WAIT_PAY;

        if ($scOrder->save()) {
            // 创建商品快照
            foreach ($carts AS $cart) {
                $orderPhoto = new IntergralOrderGoods();
                $orderPhoto->order_id = $scOrder->orderid;
                $orderPhoto->goods_id = $cart->goodsId;
                $orderPhoto->goods_pic = $cart->goods_image;
                $orderPhoto->buyer_id = $this->memberInfo->id;
                $orderPhoto->goods_name = $cart->goodsName;
                $orderPhoto->goods_price = $cart->goodsPrice;

                $orderPhoto->goods_integral = 0;
                $orderPhoto->fallinto_state = 0;
                $orderPhoto->goods_num = $cart->goodsNum;
                $orderPhoto->fallInto = $cart->totalPrice;
                $orderPhoto->goods_pay_price = $cart->totalPrice;
                $orderPhoto->createTime = date('Y-m-d H:i:s');
                $orderPhoto->sku_id = $cart->sku_id;
                $orderPhoto->attr = $cart->attrInfo;
                $orderPhoto->modifyTime = date('Y-m-d H:i:s');

                $orderPhoto->save();
            }

            header("Location: " . UrlService::build(['integral-order/confirm', 'number' => $scOrder->orderid]));
        }

        die("创建订单失败，请重试");
    }

    /**
     * 编辑购物车商品
     *
     * @author daorli
     * @date 2019-6-17
     */
    public function actionAdd()
    {
        $id = (int)\Yii::$app->request->get('id');
        $amount = (int)\Yii::$app->request->get('amount');
        $skuId = (int)\Yii::$app->request->get('skuId');

        if ($id and $amount) {
            $goods = IntergralGoods::findOne([
                'id' => $id,
                'isdelete' => 0
            ]);
            $goods or apiSendError("商品已下架");


            $goodsImg = $goods->goods_pic;
            $attrInfo = [];
            $price = $goods->goods_integral;

            // 验证 SKU 正确性
            if ($skuId) {
                $sku = IntergralGoodsSku::findOne($skuId);
                $sku or apiSendError("商品信息有误，请稍后重试");

                if ($sku->num < $amount) {
                    apiSendError("库存不足");
                }

                $goodsImg = $sku->picimg;
                $attrInfo = json_decode($sku->attr, true);
                $price = $sku->integral;
            }

            $cart = Cart::findOne([
                'goodsId' => $id,
                'buyerId' => $this->memberInfo->id,
                'sku_id' => $skuId
            ]);

            if (!$cart) {
                $cart = new Cart();
                $cart->buyerId = (int)$this->memberInfo->id;
                $cart->goodsId = (int)$goods->id;
                $cart->goodsName = $goods->goods_name;
                $cart->goodsPrice = $price;
                $cart->goods_image = $goodsImg;
                $cart->attr = json_encode($attrInfo);
                $cart->sku_id = $skuId;
                $cart->ispay = 1;
            }

            $cart->goodsNum = $amount;
            $cart->totalPrice = ($cart->goodsNum * $cart->goodsPrice);
            $cart->goodsIntegral = 0;


            $cart->save() and apiSendSuccess();
        } else {
            $cart = Cart::findOne([
                'goodsId' => $id,
                'buyerId' => $this->memberInfo->id,
                'sku_id' => $skuId
            ]);

            if ($cart) {
                $this->actionDelete($cart->id);
            }
        }

        apiSendError("添加购物车失败");
    }

    /**
     * 移除
     */
    public function actionDelete()
    {
        $ids = Yii::$app->request->post('ids', '');
        $ids = explode(',', $ids);

        \Yii::$app->user->isGuest and apiSendError("用户未登录");

        $memberInfo = \Yii::$app->user->identity;

        if ($ids) {
            $carts = Cart::find()->where([
                'and',
                ['in', 'id', $ids],
                ['=', 'buyerId', (int)$memberInfo->id]
            ])->all();

            if ($carts) {
                foreach ($carts AS $cart) {
                    $cart->delete();
                }

            }
        }

        apiSendSuccess();
    }


    /**
     * @author daorli
     * @date 2019-6-17
     */
    public function actionCheck()
    {
        \Yii::$app->user->isGuest and apiSendError("用户未登录");
        $memberInfo = \Yii::$app->user->identity;

        $status = (int)Yii::$app->request->post('status', '0');
        $ids = Yii::$app->request->post('ids', '');

        $filter = [
            'and',
            ['=', 'buyerId', $memberInfo->id]
        ];
        if (strlen($ids)) {
            $ids = arrayGroupsAction(explode(',', $ids), function ($id) {
                return (int)trim($id);
            });
            $ids = array_unique($ids);

            empty($ids) and apiSendError("请选择相应的商品");
            $filter = array_merge($filter, [
                ['in', 'id', $ids]
            ]);
        }

        $checked = $status > 0 ? 1 : 0;
        $carts = Cart::find()->where($filter)->all();

        foreach ($carts AS $cart) {
            $cart->ispay = $checked;
            $cart->save();
        }

        apiSendSuccess("ok");
    }
}