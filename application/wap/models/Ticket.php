<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "ticket".
 *
 * @property string $id
 * @property string $name
 * @property string $ask_ticket_start_date
 * @property string $ask_ticket_end_date
 * @property string $open_date
 * @property string $end_date
 * @property string $arrive_method
 * @property string $ca_type
 * @property string $type
 * @property string $citys
 * @property string $order_price
 * @property string $open_place
 * @property string $address
 * @property integer $status
 * @property string $create_time
 * @property string $lng
 * @property string $lat
 * @property string $cover_pic
 * @property string $notice_words
 * @property string $ticket_price
 */
class Ticket extends \yii\db\ActiveRecord
{
    const STATUS_NORMAL = '1';
    const STATUS_END = '0';

    const TYPE_JIE_HUN = 'JIE_HUN';
    const TYPE_JIA_ZHUANG = 'JIA_ZHUANG';
    const TYPE_YUN_YING = 'YUN_YING';

    const CA_TYPE_CAI_GOU = 'CAI_GOU';    // 采购类索票
    const CA_TYPE_ZHAN_HUI = 'ZHAN_HUI';  // 展会类索票

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ask_ticket_start_date', 'ask_ticket_end_date', 'open_date', 'end_date', 'create_time'], 'safe'],
            [['arrive_method', 'citys', 'open_place', 'address', 'cover_pic', 'notice_words'], 'string'],
            [['ca_type', 'order_price', 'notice_words'], 'required'],
            [['order_price', 'lng', 'lat', 'ticket_price'], 'number'],
            [['status'], 'integer'],
            [['name', 'ca_type', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '票名'),
            'ask_ticket_start_date' => Yii::t('app', '领票起始时间'),
            'ask_ticket_end_date' => Yii::t('app', '领票截止时间'),
            'open_date' => Yii::t('app', '举办日期'),
            'end_date' => Yii::t('app', '停止日期'),
            'arrive_method' => Yii::t('app', '乘坐路线'),
            'ca_type' => Yii::t('app', '票分类 展会票、采购票'),
            'type' => Yii::t('app', '类型'),
            'citys' => Yii::t('app', '投放城市'),
            'order_price' => Yii::t('app', '预定金额'),
            'open_place' => Yii::t('app', '举办地点名称'),
            'address' => Yii::t('app', 'Address'),
            'status' => Yii::t('app', '状态'),
            'create_time' => Yii::t('app', 'Create Time'),
            'lng' => Yii::t('app', '经度'),
            'lat' => Yii::t('app', '纬度'),
            'cover_pic' => Yii::t('app', '宣传图'),
            'notice_words' => Yii::t('app', '索票宣传语'),
            'ticket_price' => Yii::t('app', '票价'),
        ];
    }

    public function getTypeDic()
    {
        return [
            self::TYPE_JIE_HUN => '婚庆展',
            self::TYPE_JIA_ZHUANG => '家装展',
            self::TYPE_YUN_YING => '孕婴展'
        ];
    }

    public function getTypeInfo()
    {
        $dic = $this->getTypeDic();

        return isset($dic[$this->type]) ? $dic[$this->type] : '-';
    }
    /**
     * 根据 citys 获取城市名
     */
    public  function  getCityName(){

            $provinces=Provinces::findOne($this->citys);

        return  $provinces;
    }
}
