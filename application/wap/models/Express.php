<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "express".
 *
 * @property string $id
 * @property string $name
 * @property integer $status
 * @property string $type
 * @property string $createTime
 * @property string $updateTime
 */
class Express extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'express';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['createTime', 'updateTime'], 'safe'],
            [['name', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '物流公司名称'),
            'status' => Yii::t('app', '显示'),
            'type' => Yii::t('app', '公司编号'),
            'createTime' => Yii::t('app', 'Create Time'),
            'updateTime' => Yii::t('app', 'Update Time'),
        ];
    }
}
