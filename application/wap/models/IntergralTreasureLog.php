<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "intergral_treasure_log".
 *
 * @property string $id
 * @property integer $member_id
 * @property integer $treasure_id
 * @property string $integral
 * @property string $key
 * @property string $create_time
 * @property string $apply_group
 */
class IntergralTreasureLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_treasure_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'treasure_id'], 'integer'],
            [['treasure_id'], 'required'],
            [['integral'], 'number'],
            [['create_time'], 'safe'],
            [['key', 'apply_group'], 'string', 'max' => 255],
            [['treasure_id', 'key'], 'unique', 'targetAttribute' => ['treasure_id', 'key'], 'message' => 'The combination of 夺宝活动ID and 券码 has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', 'Member ID'),
            'treasure_id' => Yii::t('app', '夺宝活动ID'),
            'integral' => Yii::t('app', '消耗积分'),
            'key' => Yii::t('app', '券码'),
            'create_time' => Yii::t('app', '参加时间'),
            'apply_group' => Yii::t('app', 'Apply Group'),
        ];
    }

    public function getMember()
    {
        $member = Member::findOne($this->member_id);
        return $member ?: (new Member());
    }

    public function getCreateKey()
    {
        $key = (10000 + $this->id) .  date('md') . createRandKey(6);

        $record = self::findOne([
            'key' => $key
        ]);
        return $record ? $this->getCreateKey() : $key;
    }

    /**
     * 关联夺宝商品
     * @return IntergralTreasure
     */
    public function getIntergralTreasure(){
        $model = IntergralTreasure::findOne($this->treasure_id);
        return $model ?: (new IntergralTreasure());
    }
}
