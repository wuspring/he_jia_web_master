<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "ingergral_treasure_log".
 *
 * @property string $id
 * @property integer $member_id
 * @property integer $treasure_id
 * @property integer $integral
 * @property string $key
 * @property string $create_time
 */
class IngergralTreasureLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ingergral_treasure_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'treasure_id', 'integral'], 'integer'],
            [['treasure_id'], 'required'],
            [['create_time'], 'safe'],
            [['key'], 'string', 'max' => 255],
            [['treasure_id', 'key'], 'unique', 'targetAttribute' => ['treasure_id', 'key'], 'message' => 'The combination of 夺宝活动ID and 券码 has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', 'Member ID'),
            'treasure_id' => Yii::t('app', '夺宝活动ID'),
            'integral' => Yii::t('app', '消耗积分'),
            'key' => Yii::t('app', '券码'),
            'create_time' => Yii::t('app', '参加时间'),
        ];
    }
}
