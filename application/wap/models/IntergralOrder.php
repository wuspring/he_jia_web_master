<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "intergral_order".
 *
 * @property string $orderid
 * @property string $pay_sn
 * @property string $user_id
 * @property string $buyer_id
 * @property string $buyer_name
 * @property string $goods_amount
 * @property string $order_amount
 * @property integer $integral_amount
 * @property string $pd_amount
 * @property string $freight
 * @property integer $evaluation_state
 * @property integer $order_state
 * @property string $receiver
 * @property string $receiver_name
 * @property string $receiver_state
 * @property string $receiver_address
 * @property string $receiver_mobile
 * @property string $receiver_zip
 * @property string $shipping
 * @property string $shipping_code
 * @property string $deliveryTime
 * @property string $add_time
 * @property string $payment_time
 * @property string $finnshed_time
 * @property integer $fallinto_state
 * @property string $coupon_id
 * @property string $pay_method
 * @property integer $reback_status
 * @property string $type
 * @property integer $ticket_id
 * @property integer $relation_coupon
 * @property string $pay_amount
 * @property string $ticket_type
 * @property string $expand
 */
class IntergralOrder extends \yii\db\ActiveRecord
{

    const STATUS_WAIT_PAY = '0'; // 待付款
    const STATUS_WAIT_SEND = '1'; // 待发货
    const STATUS_WAIT_RECIVE = '2'; // 待收货
    const STATUS_WAIT_JUDGE = '3';  // 待评价
    const STATUS_FINISH = '4'; // 评价完成


    const EXPAND_WORKDAY = '仅工作日送货';
    const EXPAND_WEEKEND = '仅周末送货';
    const EXPAND_ANYTIME = '任何时间均可';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderid'], 'required'],
            [['orderid', 'user_id', 'buyer_id', 'integral_amount', 'evaluation_state', 'order_state', 'receiver', 'shipping', 'fallinto_state', 'coupon_id', 'reback_status', 'ticket_id', 'relation_coupon'], 'integer'],
            [['goods_amount', 'order_amount', 'pd_amount', 'freight', 'pay_amount'], 'number'],
            [['deliveryTime', 'add_time', 'payment_time', 'finnshed_time'], 'safe'],
            [['expand'], 'string'],
            [['pay_sn', 'buyer_name', 'receiver_name', 'receiver_state', 'receiver_address', 'receiver_mobile', 'receiver_zip', 'shipping_code', 'type', 'ticket_type'], 'string', 'max' => 255],
            [['pay_method'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'orderid' => Yii::t('app', '订单编号'),
            'pay_sn' => Yii::t('app', '支付单号'),
            'user_id' => Yii::t('app', '商家ID'),
            'buyer_id' => Yii::t('app', '买家id'),
            'buyer_name' => Yii::t('app', '买家姓名'),
            'goods_amount' => Yii::t('app', '商品总价格'),
            'order_amount' => Yii::t('app', '订单总价格'),
            'integral_amount' => Yii::t('app', '订单积分'),
            'pd_amount' => Yii::t('app', '预存款支付金额'),
            'freight' => Yii::t('app', '运费'),
            'evaluation_state' => Yii::t('app', '评价状态 0未评价，1已评价，2已过期未评价'),
            'order_state' => Yii::t('app', '订单状态：0(已取消)0(默认):未付款;1:已付款;2:已发货;3:已收货;'),
            'receiver' => Yii::t('app', '收货人'),
            'receiver_name' => Yii::t('app', 'Receiver Name'),
            'receiver_state' => Yii::t('app', 'Receiver State'),
            'receiver_address' => Yii::t('app', 'Receiver Address'),
            'receiver_mobile' => Yii::t('app', 'Receiver Mobile'),
            'receiver_zip' => Yii::t('app', 'Receiver Zip'),
            'shipping' => Yii::t('app', '物流公司编号'),
            'shipping_code' => Yii::t('app', '物流单号'),
            'deliveryTime' => Yii::t('app', '发货时间'),
            'add_time' => Yii::t('app', '订单生成时间'),
            'payment_time' => Yii::t('app', '支付(付款)时间'),
            'finnshed_time' => Yii::t('app', '订单完成时间'),
            'fallinto_state' => Yii::t('app', '分成状态'),
            'coupon_id' => Yii::t('app', '使用的优惠券信息'),
            'pay_method' => Yii::t('app', '支付方式'),
            'reback_status' => Yii::t('app', '退货状态'),
            'type' => Yii::t('app', '订单种类'),
            'ticket_id' => Yii::t('app', '展会票据'),
            'relation_coupon' => Yii::t('app', '关联票据'),
            'pay_amount' => Yii::t('app', '支付金额'),
            'ticket_type' => Yii::t('app', '下订单 享特价、爆款预约'),
            'expand' => Yii::t('app', 'Expand'),
        ];
    }

    public function getOrderStatusDic()
    {
        $dic = [
            self::STATUS_WAIT_PAY => '待支付',
            self::STATUS_WAIT_SEND => '待发货',
            self::STATUS_WAIT_RECIVE => '待收货',
            self::STATUS_WAIT_JUDGE => '待评价',
        ];

        return $dic;
    }

    /**
     * 获取订单状态
     */
    public function getOrderState()
    {
        $dic = $this->getOrderStatusDic();

        return isset($dic[$this->order_state]) ? $dic[$this->order_state] : '';
    }

    public function getNewNumber()
    {
        $string =  '0819' . date('ymd') . createRandKey(6);
        $order = self::findOne([
            'orderid' => $string
        ]);

        return $order ? $this->getNewNumber() : $string;
    }
}
