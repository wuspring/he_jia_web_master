<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "memory_tickets".
 *
 * @property string $id
 * @property integer $ticket_id
 * @property string $provinces_id
 */
class MemoryTickets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'memory_tickets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id', 'provinces_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ticket_id' => Yii::t('app', 'Ticket ID'),
            'provinces_id' => Yii::t('app', 'Provinces ID'),
        ];
    }
}
