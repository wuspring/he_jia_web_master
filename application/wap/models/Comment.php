<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property string $order_id
 * @property integer $goods_id
 * @property string $type
 * @property string $content
 * @property string $imgs
 * @property integer $member_id
 * @property string $create_time
 * @property double $spzl
 * @property double $shfw
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'goods_id', 'member_id'], 'integer'],
            [['content', 'imgs'], 'string'],
            [['create_time'], 'safe'],
            [['spzl', 'shfw'], 'number'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'goods_id' => Yii::t('app', 'Goods ID'),
            'type' => Yii::t('app', 'good 好评 mid  中评 low 差评'),
            'content' => Yii::t('app', '内容'),
            'imgs' => Yii::t('app', '上传图片'),
            'member_id' => Yii::t('app', '会员ID'),
            'create_time' => Yii::t('app', '评论时间'),
            'spzl' => Yii::t('app', '商品质量'),
            'shfw' => Yii::t('app', '商户服务'),
        ];
    }

    /**
     * 关联会员
     * @return null|Member
     */
    public function getMember(){
        $model = Member::findOne($this->member_id);
        if(empty($model)){
            return new Member();
        }
        return $model;
    }
}
