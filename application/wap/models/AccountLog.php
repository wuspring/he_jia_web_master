<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "account_log".
 *
 * @property integer $id
 * @property string $type
 * @property string $money
 * @property integer $integral
 * @property string $remark
 * @property integer $member_id
 * @property string $create_time
 * @property string $sn
 */
class AccountLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['money'], 'number'],
            [['integral', 'member_id'], 'integer'],
            [['create_time'], 'safe'],
            [['type'], 'string', 'max' => 20],
            [['remark'], 'string', 'max' => 250],
            [['sn'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => '类型 money消费记录',
            'money' => '变动金额',
            'integral' => '积分',
            'remark' => '说明',
            'member_id' => '会员ID',
            'create_time' => '变动时间',
            'sn' => '变动记录号',
        ];
    }
}
