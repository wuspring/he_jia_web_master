<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "sms".
 *
 * @property string $id
 * @property integer $user_id
 * @property string $phone
 * @property string $type
 * @property string $content
 * @property string $create_time
 */
class Sms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'phone', 'type', 'content', 'create_time'], 'required'],
            [['user_id'], 'integer'],
            [['create_time'], 'safe'],
            [['phone'], 'string', 'max' => 20],
            [['type'], 'string', 'max' => 32],
            [['content'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'phone' => Yii::t('app', 'Phone'),
            'type' => Yii::t('app', 'Type'),
            'content' => Yii::t('app', 'Content'),
            'create_time' => Yii::t('app', 'Create Time'),
        ];
    }
}
