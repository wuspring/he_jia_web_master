<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "fallintolog".
 *
 * @property string $id
 * @property string $orderId
 * @property string $memberId
 * @property string $goodsId
 * @property string $fallIntoMoney
 * @property string $actualMoney
 * @property string $createTime
 * @property string $modifyTime
 * @property integer $state
 */
class Fallintolog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fallintolog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderId', 'memberId', 'goodsId', 'state'], 'integer'],
            [['fallIntoMoney', 'actualMoney'], 'number'],
            [['createTime', 'modifyTime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'orderId' => Yii::t('app', '订单编号'),
            'memberId' => Yii::t('app', '会员id'),
            'goodsId' => Yii::t('app', '商品id'),
            'fallIntoMoney' => Yii::t('app', '分成金额'),
            'actualMoney' => Yii::t('app', '实际分成'),
            'createTime' => Yii::t('app', 'Create Time'),
            'modifyTime' => Yii::t('app', 'Modify Time'),
            'state' => Yii::t('app', '领取状态 0表示未领取 1表示已分配'),
        ];
    }
}
