<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "intergral_prize".
 *
 * @property string $id
 * @property integer $pid_terms
 * @property integer $prize_terms
 * @property string $prize_name
 * @property string $prize_grade
 * @property string $prize_price
 * @property string $cover_pic
 * @property string $prize_describe
 * @property string $start_time
 * @property string $end_time
 * @property string $create_time
 * @property integer $is_del
 * @property integer $is_show
 * @property integer $sort
 */
class IntergralPrize extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_prize';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pid_terms', 'prize_terms', 'is_del', 'is_show', 'sort'], 'integer'],
            [['prize_price'], 'number'],
            [['prize_describe'], 'string'],
            [['start_time', 'end_time', 'create_time'], 'safe'],
            [['prize_name', 'prize_grade', 'cover_pic'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pid_terms' => Yii::t('app', '第几期'),
            'prize_terms' => Yii::t('app', '第几期'),
            'prize_name' => Yii::t('app', '奖品名称'),
            'prize_grade' => Yii::t('app', '奖品等级'),
            'prize_price' => Yii::t('app', '奖品价格'),
            'cover_pic' => Yii::t('app', '奖品主图'),
            'prize_describe' => Yii::t('app', '奖品描述'),
            'start_time' => Yii::t('app', '开始时间'),
            'end_time' => Yii::t('app', '结束时间'),
            'create_time' => Yii::t('app', '时间'),
            'is_del' => Yii::t('app', '软删除'),
            'is_show' => Yii::t('app', '是否显示'),
            'sort' => Yii::t('app', 'Sort'),
        ];
    }
}
