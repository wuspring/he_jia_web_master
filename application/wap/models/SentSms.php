<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "sent_sms".
 *
 * @property string $id
 * @property string $access_key_id
 * @property string $access_key_secret
 * @property string $sms_code
 * @property string $sms_name
 * @property string $content
 * @property string $type
 * @property integer $is_show
 * @property integer $is_del
 * @property integer $is_access_keys
 * @property string $create_time
 */
class SentSms extends \yii\db\ActiveRecord
{


    const TYPE_REG = 'REG';      // 注册
    const TYPE_QUICK_LOGON = 'QUICK_LOGON';       // 快捷登录
    const TYPE_FORGET_PASSWORD = 'FORGET_PASSWORD';      // 忘记密码
    const TYPE_ADDRESS_NOTICE = 'ADDRESS_NOTICE';  // 地址通知
    const TYPE_CABLE_TICKET = 'CABLE_TICKET';     // 展会索票

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sent_sms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_show', 'is_del', 'is_access_keys'], 'integer'],
            [['create_time'], 'safe'],
            [['access_key_id', 'access_key_secret', 'sms_code', 'sms_name', 'content', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'access_key_id' => Yii::t('app', '短信接口key'),
            'access_key_secret' => Yii::t('app', '短信接口secret'),
            'sms_code' => Yii::t('app', '模版CODE'),
            'sms_name' => Yii::t('app', '模版名称'),
            'content' => Yii::t('app', '通知信息'),
            'type' => Yii::t('app', '模板类型'),
            'is_show' => Yii::t('app', '是否显示'),
            'is_del' => Yii::t('app', '是否删除'),
            'is_access_keys' => Yii::t('app', '是否是accessKeys'),
            'create_time' => Yii::t('app', '创建时间'),
        ];
    }


    public function getTypeDic()
    {
        return [
            self::TYPE_REG => '注册',
            self::TYPE_QUICK_LOGON => '快速登录',
            self::TYPE_FORGET_PASSWORD => '忘记密码',
            self::TYPE_ADDRESS_NOTICE => '地址信息通知',
            self::TYPE_CABLE_TICKET => '展会索票',
        ];
    }

    public function getTypeInfo()
    {
        $dic = $this->getTypeDic();
        return isset($dic[$this->type]) ? $dic[$this->type] : '-';
    }
}
