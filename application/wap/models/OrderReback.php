<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "order_reback".
 *
 * @property string $orderid
 * @property string $pay_sn
 * @property string $reback_sn
 * @property integer $status
 * @property string $create_time
 */
class OrderReback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_reback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderid'], 'required'],
            [['orderid', 'status'], 'integer'],
            [['create_time'], 'safe'],
            [['pay_sn', 'reback_sn'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'orderid' => Yii::t('app', '订单编号'),
            'pay_sn' => Yii::t('app', '支付单号'),
            'reback_sn' => Yii::t('app', '支付单号'),
            'status' => Yii::t('app', '状态'),
            'create_time' => Yii::t('app', '订单完成时间'),
        ];
    }
}
