<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "member".
 *
 * @property string $id
 * @property string $username
 * @property string $password
 * @property string $nickname
 * @property integer $role
 * @property string $mobile
 * @property integer $sex
 * @property string $birthday
 * @property string $channel
 * @property string $status
 * @property string $avatarTm
 * @property string $avatar
 * @property string $email
 * @property string $createTime
 * @property string $modifyTime
 * @property string $expiryTime
 * @property integer $isText
 * @property string $last_visit
 * @property integer $userType
 * @property string $authkey
 * @property string $accessToken
 * @property string $remainder
 * @property integer $integral
 * @property integer $rank
 * @property string $brokerage
 * @property string $proxyBrokerage
 * @property string $pid
 * @property string $wxopenid
 * @property string $wxnick
 * @property string $ticket
 * @property string $qrodeUrl
 * @property integer $is_fenxiao
 * @property integer $is_daili
 * @property string $origin
 * @property string $cost_amount
 * @property string $birthday_year
 * @property integer $is_del
 */
class Member extends \yii\db\ActiveRecord
{

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_TYPE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role', 'sex', 'isText', 'userType', 'integral', 'rank', 'pid', 'is_fenxiao', 'is_daili', 'is_del'], 'integer'],
            [['birthday', 'createTime', 'modifyTime', 'expiryTime', 'last_visit'], 'safe'],
            [['remainder', 'brokerage', 'proxyBrokerage', 'cost_amount'], 'number'],
            [['origin'], 'string'],
            [['username', 'password', 'nickname', 'mobile', 'channel', 'status', 'avatarTm', 'avatar', 'email', 'authkey', 'accessToken', 'wxopenid', 'wxnick', 'ticket', 'qrodeUrl', 'birthday_year'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['mobile'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', '帐号'),
            'password' => Yii::t('app', '密码'),
            'nickname' => Yii::t('app', '昵称'),
            'role' => Yii::t('app', '角色名称'),
            'mobile' => Yii::t('app', '手机号'),
            'sex' => Yii::t('app', '性别'),
            'birthday' => Yii::t('app', '生日'),
            'channel' => Yii::t('app', '推广渠道'),
            'status' => Yii::t('app', '状态'),
            'avatarTm' => Yii::t('app', '头像缩略图'),
            'avatar' => Yii::t('app', '头像'),
            'email' => Yii::t('app', '邮箱'),
            'createTime' => Yii::t('app', '创建时间'),
            'modifyTime' => Yii::t('app', '修改时间'),
            'expiryTime' => Yii::t('app', '会员过期时间'),
            'isText' => Yii::t('app', '是否签署协议'),
            'last_visit' => Yii::t('app', '最后登录时间'),
            'userType' => Yii::t('app', '用户类别'),
            'authkey' => Yii::t('app', '授权'),
            'accessToken' => Yii::t('app', 'Access Token'),
            'remainder' => Yii::t('app', '余额'),
            'integral' => Yii::t('app', '积分'),
            'rank' => Yii::t('app', '等级分数'),
            'brokerage' => Yii::t('app', '分销商佣金'),
            'proxyBrokerage' => Yii::t('app', '经销商佣金'),
            'pid' => Yii::t('app', '上一级'),
            'wxopenid' => Yii::t('app', '微信openid'),
            'wxnick' => Yii::t('app', '微信昵称'),
            'ticket' => Yii::t('app', 'Ticket'),
            'qrodeUrl' => Yii::t('app', '推荐二维码'),
            'is_fenxiao' => Yii::t('app', '是否是分销商'),
            'is_daili' => Yii::t('app', '是否是代理商'),
            'origin' => Yii::t('app', '所在区域'),
            'cost_amount' => Yii::t('app', '消费总金额'),
            'birthday_year' => Yii::t('app', '年月日'),
            'is_del' => Yii::t('app', '软删除'),
        ];
    }


    /**
     * 获取推荐人名称
     * @author 黄西方
     * @date 2018-10-28
     */
    public function getParentName()
    {
        if($this->pid){
            $model = Member::findOne($this->pid);
            return $model->nickname;
        }else{
            return '无';
        }
    }

    public function getPmember()
    {
        $member = Member::findOne($this->pid);
        return $member ? $member->wxnick : '平台';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new \Exception('"findIdentityByAccessToken" is not implemented.');
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->authkey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }


    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function validatePassword($password)
    {

        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function getNewUserName()
    {
        $str = 'at_' . createRandKey('10','abcedfghijklimnopqrstuvwxyz0123456789');
        $member = Member::findOne([
            'username' => $str
        ]);

        return $member ? $this->getNewUserName() : $str;

    }

}
