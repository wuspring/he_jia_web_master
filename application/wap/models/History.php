<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $goods_id
 * @property string $add_time
 * @property string $create_time
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'goods_id'], 'integer'],
            [['add_time', 'create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', '会员ID'),
            'goods_id' => Yii::t('app', '商品ID'),
            'add_time' => Yii::t('app', '日期'),
            'create_time' => Yii::t('app', 'Create Time'),
        ];
    }
}
