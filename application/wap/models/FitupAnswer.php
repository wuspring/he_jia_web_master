<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "fitup_answer".
 *
 * @property string $id
 * @property integer $problem_id
 * @property integer $member_id
 * @property integer $pid
 * @property string $content
 * @property string $create_time
 * @property integer $is_del
 * @property integer $is_show
 */
class FitupAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fitup_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['problem_id', 'member_id', 'pid', 'is_del', 'is_show'], 'integer'],
            [['content'], 'string'],
            [['create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'problem_id' => Yii::t('app', '问题表的id'),
            'member_id' => Yii::t('app', '回答问题的用户'),
            'pid' => Yii::t('app', '互答时的用户id'),
            'content' => Yii::t('app', '回答内容'),
            'create_time' => Yii::t('app', '时间'),
            'is_del' => Yii::t('app', '软删除'),
            'is_show' => Yii::t('app', '后台可以控制是否显示'),
        ];
    }

    public function  getMember(){

        $member = Member::findOne($this->member_id);
        return  $member;
    }

}
