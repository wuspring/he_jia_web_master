<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "floor_index".
 *
 * @property string $id
 * @property string $provinces_id
 * @property string $good_class_id
 * @property string $type
 * @property string $img
 * @property string $zh_img
 * @property string $zh_first_imgs
 * @property string $brand
 * @property string $create_time
 */
class FloorIndex extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'floor_index';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provinces_id'], 'required'],
            [['good_class_id'], 'integer'],
            [['img', 'zh_img', 'zh_first_imgs'], 'string'],
            [['create_time'], 'safe'],
            [['provinces_id', 'type', 'brand'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provinces_id' => Yii::t('app', '城市ID'),
            'good_class_id' => Yii::t('app', '分类ID'),
            'type' => Yii::t('app', '楼层类型'),
            'img' => Yii::t('app', '楼层图片'),
            'zh_img' => Yii::t('app', '展会图片'),
            'zh_first_imgs' => Yii::t('app', '展会楼层首图'),
            'brand' => Yii::t('app', '品牌'),
            'create_time' => Yii::t('app', '添加时间'),
        ];
    }
}
