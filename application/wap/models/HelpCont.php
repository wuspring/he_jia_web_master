<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "help_cont".
 *
 * @property string $id
 * @property string $title
 * @property integer $type_id
 * @property integer $sort
 * @property string $content
 * @property string $create_time
 * @property integer $is_del
 * @property integer $is_show
 */
class HelpCont extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help_cont';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'sort', 'is_del', 'is_show'], 'integer'],
            [['content'], 'string'],
            [['create_time'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', '标题'),
            'type_id' => Yii::t('app', '所属类型'),
            'sort' => Yii::t('app', '排序'),
            'content' => Yii::t('app', '内容'),
            'create_time' => Yii::t('app', '添加时间'),
            'is_del' => Yii::t('app', '软删除'),
            'is_show' => Yii::t('app', '是否显示'),
        ];
    }
}
