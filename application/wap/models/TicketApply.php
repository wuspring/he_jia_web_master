<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "ticket_apply".
 *
 * @property string $id
 * @property string $user_id
 * @property integer $ticket_id
 * @property string $zw_pos
 * @property string $status
 * @property string $reason
 * @property string $good_ids
 * @property string $create_time
 */
class TicketApply extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_apply';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ticket_id', 'status'], 'integer'],
            [['reason', 'good_ids'], 'string'],
            [['create_time'], 'safe'],
            [['zw_pos'], 'string', 'max' => 255],
            [['user_id', 'ticket_id'], 'unique', 'targetAttribute' => ['user_id', 'ticket_id'], 'message' => 'The combination of User ID and 展会ID has already been taken.'],
            [['zw_pos'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'ticket_id' => Yii::t('app', '展会ID'),
            'zw_pos' => Yii::t('app', '展位号'),
            'status' => Yii::t('app', '状态'),
            'reason' => Yii::t('app', '审核回复'),
            'good_ids' => Yii::t('app', '申请参与展会商品（已弃用，不再维护）'),
            'create_time' => Yii::t('app', 'Create Time'),
        ];
    }
}
