<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "member".
 *
 * @property string $id
 * @property string $username
 * @property string $password
 * @property string $nickname
 * @property integer $role
 * @property string $mobile
 * @property integer $sex
 * @property string $birthday
 * @property string $status
 * @property string $avatarTm
 * @property string $avatar
 * @property string $email
 * @property string $createTime
 * @property string $modifyTime
 * @property string $expiryTime
 * @property integer $isText
 * @property string $last_visit
 * @property integer $userType
 * @property string $authkey
 * @property string $accessToken
 * @property string $remainder
 * @property integer $integral
 * @property integer $rank
 * @property string $brokerage
 * @property string $pid
 * @property string $wxopenid
 * @property string $wxnick
 * @property string $ticket
 * @property string $qrodeUrl
 * @property string $smscode
 * @property string $smstime
 */
class MemberRg extends \yii\db\ActiveRecord
{
    public $password1;
    public $rememberMe = false;
    public $smscode;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password','smscode','mobile'], 'required'],
            [['role', 'sex', 'isText', 'userType', 'integral', 'rank', 'pid'], 'integer'],
            [['birthday', 'createTime', 'modifyTime', 'expiryTime', 'status',  'last_visit'], 'safe'],
            [['remainder', 'brokerage'], 'number'],
            [['username'], 'unique','message'=>'已经被占用了'],
            [['mobile'], 'unique','message'=>'已经被占用了'],
            [['password1'], 'required','message'=>'不能为空'],
            [['smscode'], 'smsVerify'],
            ['password', 'string', 'max'=>12, 'min'=>6, 'tooLong'=>'密码请输入长度为6-12位字符', 'tooShort'=>'密码请输入长度为6-12位字符'],
            ['password1', 'compare', 'compareAttribute'=>'password','message'=>'两次密码不一致'],
            [['username', 'password', 'nickname', 'mobile','avatarTm', 'avatar', 'email', 'authkey', 'accessToken', 'wxopenid', 'wxnick', 'ticket', 'qrodeUrl'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', '帐号'),
            'password' => Yii::t('app', '密码'),
            'nickname' => Yii::t('app', '昵称'),
            'role' => Yii::t('app', '角色名称'),
            'mobile' => Yii::t('app', '手机号'),
            'sex' => Yii::t('app', '性别'),
            'birthday' => Yii::t('app', '生日'),
            'status' => Yii::t('app', '状态'),
            'avatarTm' => Yii::t('app', '头像缩略图'),
            'avatar' => Yii::t('app', '头像'),
            'email' => Yii::t('app', '邮箱'),
            'createTime' => Yii::t('app', '创建时间'),
            'modifyTime' => Yii::t('app', '修改时间'),
            'expiryTime' => Yii::t('app', '会员过期时间'),
            'isText' => Yii::t('app', '是否签署协议'),
            'last_visit' => Yii::t('app', '最后登录时间'),
            'userType' => Yii::t('app', '用户类别'),
            'authkey' => Yii::t('app', '授权'),
            'accessToken' => Yii::t('app', 'Access Token'),
            'remainder' => Yii::t('app', '余额'),
            'integral' => Yii::t('app', '积分'),
            'rank' => Yii::t('app', '等级分数'),
            'brokerage' => Yii::t('app', '佣金'),
            'pid' => Yii::t('app', '上一级'),
            'wxopenid' => Yii::t('app', '微信openid'),
            'wxnick' => Yii::t('app', '微信昵称'),
            'ticket' => Yii::t('app', 'Ticket'),
            'qrodeUrl' => Yii::t('app', '推荐二维码'),
            'smscode' => Yii::t('app', '验证码'),
        ];
    }

    /**
     * @param $mobile
     * @return bool
     * 重写 手机验证码验证
     */
    public  function  smsVerify($attribute, $params){
            $mobileSms = Yii::$app->session->get('SMS_CODE');
            if (!empty($mobileSms)){
                $mobile_sms = $this->mobile.'-'.$this->smscode;
                if ($mobile_sms != $mobileSms['value_str']) {
                      $this->addError($attribute, "填写错误");
                     return false;
                }
            }else{
                $this->addError($attribute, "错误");
                return false;
            }
        return true;
    }



}