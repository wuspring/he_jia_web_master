<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "floor_wap".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $topimg
 * @property string $nextimg
 * @property string $type
 * @property integer $sort
 * @property string $create_time
 * @property string $modify_time
 * @property integer $status
 * @property integer $ticket_id
 */
class FloorWap extends \yii\db\ActiveRecord
{
    const STATUS_NORMAL = 1;
    const STATUS_END = 0;

    const TYPE_JIE_HUN = 'JIE_HUN';
    const TYPE_JIA_ZHUANG = 'JIA_ZHUANG';
    const TYPE_YUN_YING = 'YUN_YING';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'floor_wap';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['topimg', 'nextimg'], 'string'],
            [['sort','status','ticket_id'], 'integer'],
            [['create_time', 'modify_time'], 'safe'],
            [['name', 'url', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '楼层名称'),
            'url' => Yii::t('app', '楼层链接'),
            'topimg' => Yii::t('app', '顶部广告'),
            'nextimg' => Yii::t('app', '楼层内广告'),
            'type' => Yii::t('app', '类型'),
            'sort' => Yii::t('app', '排序'),
            'status' => Yii::t('app', '状态'),
            'create_time' => Yii::t('app', '添加时间'),
            'modify_time' => Yii::t('app', '更新时间'),
            'ticket_id' => Yii::t('app', '展会ID'),
        ];
    }

    public function getTypeDic()
    {
        return [
            self::TYPE_JIE_HUN => '婚庆展',
            self::TYPE_JIA_ZHUANG => '家装展',
            self::TYPE_YUN_YING => '孕婴展'
        ];
    }
}