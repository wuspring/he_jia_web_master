<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "newsclassify".
 *
 * @property string $id
 * @property string $name
 * @property string $fid
 * @property integer $isShow
 * @property integer $isRdm
 * @property integer $sort
 * @property string $createTime
 * @property string $modifyTime
 */
class Newsclassify extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'newsclassify';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fid', 'isShow', 'isRdm', 'sort'], 'integer'],
            [['createTime', 'modifyTime'], 'safe'],
            [['name','icoImg'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '名称'),
            'icoImg' => Yii::t('app', '图标'),
            'fid' => Yii::t('app', '父级'),
            'isShow' => Yii::t('app', '是否显示'),
            'isRdm' => Yii::t('app', '是否推荐'),
            'sort' => Yii::t('app', '排序'),
            'createTime' => Yii::t('app', 'Create Time'),
            'modifyTime' => Yii::t('app', 'Modify Time'),
        ];
    }
}
