<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "ticket_apply_goods_data".
 *
 * @property string $id
 * @property integer $ticket_id
 * @property integer $user_id
 * @property integer $good_id
 * @property string $good_pic
 * @property string $good_price
 * @property string $type
 * @property string $create_time
 * @property string $good_amount
 * @property integer $status
 * @property integer $is_index
 * @property integer $sort
 */
class TicketApplyGoodsData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_apply_goods_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id', 'user_id', 'good_id', 'good_amount', 'status', 'is_index', 'sort'], 'integer'],
            [['good_pic'], 'string'],
            [['good_price'], 'number'],
            [['create_time'], 'safe'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ticket_id' => Yii::t('app', '展会ID'),
            'user_id' => Yii::t('app', '商铺ID'),
            'good_id' => Yii::t('app', '商品ID'),
            'good_pic' => Yii::t('app', '商品活动图片'),
            'good_price' => Yii::t('app', '商品价格'),
            'type' => Yii::t('app', '商品类型'),
            'create_time' => Yii::t('app', 'Create Time'),
            'good_amount' => Yii::t('app', '库存'),
            'status' => Yii::t('app', '审核状态'),
            'is_index' => Yii::t('app', '首页置顶'),
            'sort' => Yii::t('app', '排序'),
        ];
    }
}
