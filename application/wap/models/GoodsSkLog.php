<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "goods_sk_log".
 *
 * @property string $id
 * @property string $date
 * @property integer $goods_id
 * @property string $limbo_index
 * @property integer $num
 */
class GoodsSkLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_sk_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['goods_id', 'limbo_index', 'num'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', '日期'),
            'goods_id' => Yii::t('app', '商品ID'),
            'limbo_index' => Yii::t('app', '秒杀时段'),
            'num' => Yii::t('app', '当日商品数量'),
        ];
    }
}
