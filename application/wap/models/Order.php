<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property string $orderid
 * @property string $pay_sn
 * @property string $user_id
 * @property string $buyer_id
 * @property string $buyer_name
 * @property string $goods_amount
 * @property string $order_amount
 * @property integer $integral_amount
 * @property string $pd_amount
 * @property string $freight
 * @property integer $evaluation_state
 * @property integer $order_state
 * @property string $receiver
 * @property string $receiver_name
 * @property string $receiver_state
 * @property string $receiver_address
 * @property string $receiver_mobile
 * @property string $receiver_zip
 * @property string $shipping
 * @property string $shipping_code
 * @property string $deliveryTime
 * @property string $add_time
 * @property string $payment_time
 * @property string $finnshed_time
 * @property integer $fallinto_state
 * @property string $coupon_id
 * @property string $pay_method
 * @property integer $reback_status
 * @property string $type
 * @property integer $ticket_id
 * @property integer $relation_coupon
 * @property string $pay_amount
 * @property string $ticket_type
 */
class Order extends \yii\db\ActiveRecord
{
    public $typeInfo;

    const STATUS_WAIT = 0;      // 待支付
    const STATUS_PAY = 1;       // 已支付
    const STATUS_SEND = 2;      // 已发货
    const STATUS_RECEIVED = 3;  // 已收货
    const STATUS_JUDGE = 4;     // 已评价
    const STATUS_REFUSE = 5;    // 退换货

    const STATUS_CLOSE = 9;     // 已关闭
    const STATUS_CHECKOUT = 3;     // 已经核销（弃用）

    const JUDGE_STATUS_WAIT = 0;        // 未评价
    const JUDGE_STATUS_DONE = 1;        // 已评价
    const JUDGE_STATUS_TIME_OUT = 2;    // 已过期未评价

    const PAY_MECHOD_WECHAT = 'WECHAT';     // 微信支付
    const PAY_MECHOD_ALIPAY = 'ALIPAY';     // 支付宝支付
    const PAY_MECHOD_ACCOUNT = 'ACCOUNT';   // 账户支付
    const PAY_MECHOD_INTEGRAL = 'INTEGRAL';   // 积分支付

    const TYPE_ZHAN_HUI = 'ZHAN_HUI';

    const TICKET_TYPE_COUPON = TicketApplyGoods::TYPE_COUPON;   // 爆款预约
    const TICKET_TYPE_ORDER = TicketApplyGoods::TYPE_ORDER;     // 预存 享特价
    const STORE_TYPE_COUPON = 'STORE_COUPON';          // 店铺优惠劵

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderid'], 'required'],
            [['orderid', 'user_id', 'buyer_id', 'integral_amount', 'evaluation_state', 'order_state', 'receiver', 'shipping', 'fallinto_state', 'coupon_id', 'reback_status', 'ticket_id', 'relation_coupon'], 'integer'],
            [['goods_amount', 'order_amount', 'pd_amount', 'freight', 'pay_amount'], 'number'],
            [['deliveryTime', 'add_time', 'payment_time', 'finnshed_time'], 'safe'],
            [['pay_sn', 'buyer_name', 'receiver_name', 'receiver_state', 'receiver_address', 'receiver_mobile', 'receiver_zip', 'shipping_code', 'type', 'ticket_type'], 'string', 'max' => 255],
            [['pay_method'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'orderid' => Yii::t('app', '订单编号'),
            'pay_sn' => Yii::t('app', '支付单号'),
            'user_id' => Yii::t('app', '商家ID'),
            'buyer_id' => Yii::t('app', '买家id'),
            'buyer_name' => Yii::t('app', '买家姓名'),
            'goods_amount' => Yii::t('app', '商品总价格'),
            'order_amount' => Yii::t('app', '订单总价格'),
            'integral_amount' => Yii::t('app', '订单积分'),
            'pd_amount' => Yii::t('app', '预存款支付金额'),
            'freight' => Yii::t('app', '运费'),
            'evaluation_state' => Yii::t('app', '评价状态 0未评价，1已评价，2已过期未评价'),
            'order_state' => Yii::t('app', '订单状态：0(已取消)0(默认):未付款;1:已付款;2:已发货;3:已收货;'),
            'receiver' => Yii::t('app', '收货人'),
            'receiver_name' => Yii::t('app', 'Receiver Name'),
            'receiver_state' => Yii::t('app', 'Receiver State'),
            'receiver_address' => Yii::t('app', 'Receiver Address'),
            'receiver_mobile' => Yii::t('app', 'Receiver Mobile'),
            'receiver_zip' => Yii::t('app', 'Receiver Zip'),
            'shipping' => Yii::t('app', '物流公司编号'),
            'shipping_code' => Yii::t('app', '物流单号'),
            'deliveryTime' => Yii::t('app', '发货时间'),
            'add_time' => Yii::t('app', '订单生成时间'),
            'payment_time' => Yii::t('app', '支付(付款)时间'),
            'finnshed_time' => Yii::t('app', '订单完成时间'),
            'fallinto_state' => Yii::t('app', '分成状态'),
            'coupon_id' => Yii::t('app', '使用的优惠券信息'),
            'pay_method' => Yii::t('app', '支付方式'),
            'reback_status' => Yii::t('app', '退货状态'),
            'type' => Yii::t('app', '订单种类'),
            'ticket_id' => Yii::t('app', '展会票据'),
            'relation_coupon' => Yii::t('app', '关联票据'),
            'pay_amount' => Yii::t('app', '支付金额'),
            'ticket_type' => Yii::t('app', '下订单 享特价、爆款预约'),
        ];
    }

    /**
     * 关联会员表- member
     * @author wufeng
     * @date 2018-10-31
     */
    public function getMember()
    {
        $member = Member::findOne($this->buyer_id);
        return $member ? : (new Member());
    }

    public function getOrderStatusDic()
    {
        $dic = [
            self::STATUS_WAIT => '待支付',
            self::STATUS_PAY => '待发货',
            self::STATUS_SEND => '待收货',
            self::STATUS_RECEIVED => '待评价',
            self::STATUS_JUDGE => '已评价',    // 完成
            self::STATUS_CLOSE => '已关闭',
            self::STATUS_REFUSE => '退货',
            self::STATUS_CHECKOUT => '待评价'
        ];

        switch ($this->ticket_type) {
            case self::TICKET_TYPE_COUPON :
                $dic[self::STATUS_WAIT] = '待核销';
                break;
            case self::TICKET_TYPE_ORDER :
                $dic[self::STATUS_PAY] = '待核销';
                break;
        }

        return $dic;
    }

    public function getTicketTypeDic()
    {
        return [
            self::TICKET_TYPE_COUPON => '爆款预约',
            self::TICKET_TYPE_ORDER => '预存 享特价',
//            self::STORE_TYPE_COUPON => '店铺优惠劵',
        ];
    }

    public function getTicketTypeInfo()
    {
        $dic = $this->getTicketTypeDic();
        return isset($dic[$this->ticket_type]) ? $dic[$this->ticket_type] : '-';
    }
    /**
     * 获取订单状态
     */
    public function getOrderState()
    {
        $dic = $this->getOrderStatusDic();

        return (isset($dic[$this->order_state]) ? $dic[$this->order_state] : '');
    }

    public function getFirstGoods()
    {
        $orderGood = OrderGoods::findOne([
            'order_id' => $this->orderid
        ]);

        return $orderGood ?: (new OrderGoods());
    }

    public function getOrderGoodsAmount()
    {
        return OrderGoods::find()->where(['order_id' => $this->orderid])->count();
    }

    public function getOrderSenderName()
    {
        $name = '';
        if ((int)$this->shipping) {
            $express = Express::findOne($this->shipping);

            if ($express) {
                $name = $express->name;
            }
        }

        return $name;
    }

    /**
     * 获取订单商品列表
     * @author 黄西方
     * @date 2018-10-28
     * @throws \yii\base\ExitException
     */
    public function getOrderGoodsList($orderid=0)
    {
        $orderid or $orderid = $this->orderid;
        $list = [];
        $goods_list = OrderGoods::find()->where(['order_id'=>$orderid])->orderBy('id ASC')->all();
        foreach ($goods_list as $key=>$item){
            $list[] = [
                'goods_id'=>$item->goods_id,
                'store_id'=>$item->goods->user_id,
                'goods_pic'=> Yii::$app->request->hostInfo.$item->goods_pic,
                'goods_name'=>$item->goods_name,
                'goods_price'=>$item->goods_price,
                'sell_price'=>$item->sell_price,
                'goods_pay_price'=>$item->goods_pay_price,
                'goods_integral'=>$item->goods_integral,
                'goods_num'=>$item->goods_num,
                'attr'=>$item->attr,
            ];
        }
        return $list;
    }

    /**
     * 创建新订单号
     * @author daorli
     * @date 2019-6-10
     *
     * @return mixed|string
     */
    public function getNewOrderNumber()
    {
        $orderNum = date('Ymdhis') . createRandKey(4);

        $order = self::findOne([
            'orderid' => $orderNum
        ]);
        return $order ? $this->getNewOrderNumber() : $orderNum;
    }


    public function getJudgeStateDic()
    {
        $dic = [
            self::JUDGE_STATUS_WAIT => '未评价',
            self::JUDGE_STATUS_DONE => '已评价',
            self::JUDGE_STATUS_TIME_OUT => '已过期未评价'
        ];

        return $dic;
    }

    public function getJudgeState()
    {
        $dic = $this->getJudgeStateDic();

        return isset($dic[$this->evaluation_state]) ? $dic[$this->evaluation_state] : '';
    }

    public function getPayMethodInfo()
    {
        $dic = [
            self::PAY_MECHOD_WECHAT => '微信支付',
            self::PAY_MECHOD_ACCOUNT => '账户支付'
        ];
        return isset($dic[$this->pay_method]) ? $dic[$this->pay_method] : '';
    }

    public function getNewPaySn()
    {
        $orderPaySn = 'sn' . date('Ymdhis') . createRandKey(4);

        $paySn = Order::findOne([
            'pay_sn' => $orderPaySn
        ]);

        return $paySn ? $this->getNewPaySn() : $orderPaySn;
    }

    /**
     * 会员以获取购物券
     *
     * @return Coupon
     */
    public function getCoupon()
    {
        $couponId = $this->coupon_id ? : 0;
        $memberCoupon = MemberCoupon::findOne($couponId);
//        $coupon = Coupon::findOne($couponId);
        return $memberCoupon ? $memberCoupon->coupon : (new Coupon());
    }

    public function getRebackStatusInfo()
    {
        $dic = [
            '0' => '正常',
            '1' => '待审核',
            '2' => '同意',
            '3' => '不同意',
        ];

        return $dic[$this->reback_status];
    }

    public function getSystemCoupon()
    {
        $coupon = SystemCoupon::findOne($this->relation_coupon);
        return $coupon ?: (new SystemCoupon());
    }

    public function getTicket()
    {
        $ticket = Ticket::findOne($this->ticket_id);

        return $ticket ? : (new Ticket());
    }

    public function getGoodCoupon()
    {
        $couponId = $this->coupon_id ? : 0;
        $memberCoupon = GoodsCoupon::findOne($couponId);
        return $memberCoupon ? $memberCoupon : (new GoodsCoupon());
    }
}
