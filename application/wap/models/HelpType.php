<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "help_type".
 *
 * @property string $id
 * @property string $type
 * @property integer $sort
 * @property string $create_time
 * @property integer $is_del
 */
class HelpType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort', 'is_del'], 'integer'],
            [['create_time'], 'safe'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', '类型'),
            'sort' => Yii::t('app', '排序'),
            'create_time' => Yii::t('app', '添加时间'),
            'is_del' => Yii::t('app', '软删除'),
        ];
    }
}
