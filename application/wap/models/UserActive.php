<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "user_active".
 *
 * @property string $id
 * @property integer $provinces_id
 */
class UserActive extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_active';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'provinces_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provinces_id' => Yii::t('app', '所在城市'),
        ];
    }
}
