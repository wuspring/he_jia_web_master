<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "select_city".
 *
 * @property integer $id
 * @property integer $provinces_id
 * @property string $name
 * @property string $create_time
 */
class SelectCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'select_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provinces_id'], 'integer'],
            [['create_time'], 'safe'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provinces_id' => Yii::t('app', 'province表单的id'),
            'name' => Yii::t('app', '城市名'),
            'create_time' => Yii::t('app', '添加时间'),
        ];
    }
}
