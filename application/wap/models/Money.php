<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "money".
 *
 * @property string $id
 * @property integer $member_id
 * @property string $name
 * @property string $type
 * @property string $money
 * @property integer $status
 * @property string $create_time
 */
class Money extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'money';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'money', 'status', 'create_time'], 'required'],
            [['member_id', 'status'], 'integer'],
            [['money'], 'number'],
            [['create_time'], 'safe'],
            [['name', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', 'Member ID'),
            'name' => Yii::t('app', '提现人姓名'),
            'type' => Yii::t('app', '提现类型'),
            'money' => Yii::t('app', 'Money'),
            'status' => Yii::t('app', '体现状态 0：审核中， 1：已完成'),
            'create_time' => Yii::t('app', 'Create Time'),
        ];
    }
}
