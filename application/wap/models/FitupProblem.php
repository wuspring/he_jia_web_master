<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "fitup_problem".
 *
 * @property string $id
 * @property string $problem
 * @property integer $provinces_id
 * @property integer $member_id
 * @property integer $answer_num
 * @property string $create_time
 * @property integer $is_del
 * @property integer $is_show
 */
class FitupProblem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fitup_problem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['problem'], 'string'],
            [['provinces_id', 'member_id', 'answer_num', 'is_del', 'is_show'], 'integer'],
            [['create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'problem' => Yii::t('app', '用户提出的问题'),
            'provinces_id' => Yii::t('app', '城市id'),
            'member_id' => Yii::t('app', '会员id'),
            'answer_num' => Yii::t('app', '回答数量'),
            'create_time' => Yii::t('app', '时间'),
            'is_del' => Yii::t('app', '软删除'),
            'is_show' => Yii::t('app', '后台控制是否显示'),
        ];
    }

    public function  getMember(){

        $member = Member::findOne($this->member_id);
        return  $member;
    }
}
