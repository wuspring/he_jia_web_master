<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "intergral_turntable".
 *
 * @property string $id
 * @property double $angle
 * @property string $prize
 * @property integer $v
 * @property string $create_time
 * @property integer $is_del
 * @property integer $is_show
 */
class IntergralTurntable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_turntable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['angle'], 'number'],
            [['v', 'is_del', 'is_show'], 'integer'],
            [['create_time'], 'safe'],
            [['prize'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'angle' => Yii::t('app', '角度'),
            'prize' => Yii::t('app', '奖品名称'),
            'v' => Yii::t('app', '概率'),
            'create_time' => Yii::t('app', '时间'),
            'is_del' => Yii::t('app', '软删除'),
            'is_show' => Yii::t('app', '是否显示'),
        ];
    }
}
