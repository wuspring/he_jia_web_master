<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property string $id
 * @property string $memberId
 * @property string $nation
 * @property string $name
 * @property string $province
 * @property string $city
 * @property string $region
 * @property string $address
 * @property string $post_code
 * @property string $tel
 * @property string $mobile
 * @property integer $isDefault
 * @property integer $is_delete
 */
class Address extends \yii\db\ActiveRecord
{
    public $provincesInfo;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['name','mobile'], 'required'],
            [['province'], 'required','message' => '请选择省市县'],
            ['mobile','match','pattern'=>'/^[1][3456789][0-9]{9}$/','message' => '手机号格式填写不正确'],
            [['memberId', 'isDefault', 'is_delete'], 'integer'],
            [['nation', 'name', 'province', 'city', 'region', 'address', 'post_code', 'tel', 'mobile'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'memberId' => '会员id',
            'nation' => '国家',
            'name' => '收货人姓名',
            'province' => '省',
            'city' => '市',
            'region' => '地区',
            'address' => '详细地址',
            'post_code' => '邮编',
            'tel' => '座机',
            'mobile' => '手机号',
            'isDefault' => '是否默认地址',
            'is_delete' => 'Is Delete',
        ];
    }

    public function getProvince($id)
    {
        $province = Provinces::findOne([
            'id' => $id
        ]);

        return $province ?$province->cname : '-';
    }


    /**
     * 获取省市区- 地址
     * @param int $provincesId
     * @return mixed|string
     */
    public function getProvinceInfo($provincesId=0)
    {
        $provincesId or $provincesId = $this->region;

        $provinces = Provinces::findOne(['id' => $provincesId]);
        if (!$provinces) {
            return '-';
        }

        $this->provincesInfo = "{$provinces->cname}, {$this->provincesInfo}";

        if ($provinces->upid <= 1) {
            return $this->provincesInfo;
        }

        return $this->getProvinceInfo($provinces->upid);
    }

    /**
     * @param $value
     * @return string
     */
    public function getProvinces($value)
    {
        $province = Provinces::findOne([
            'id' => $value
        ]);
        return $province ? $province->cname : '-';
    }

}
