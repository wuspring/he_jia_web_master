<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "adsense".
 *
 * @property string $id
 * @property string $name
 * @property integer $state
 * @property string $createTime
 * @property string $modifyTime
 */
class Adsense extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adsense';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state'], 'integer'],
            [['createTime', 'modifyTime'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '广告位名称'),
            'state' => Yii::t('app', '状态:1有效 0无效'),
            'createTime' => Yii::t('app', '添加时间'),
            'modifyTime' => Yii::t('app', '修改时间'),
        ];
    }
}
