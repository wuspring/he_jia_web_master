<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property string $id
 * @property string $title
 * @property string $type
 * @property string $cid
 * @property string $themeImg
 * @property string $themeImgTm
 * @property string $brief
 * @property string $content
 * @property string $seokey
 * @property string $seoDepict
 * @property integer $isShow
 * @property integer $isRmd
 * @property integer $clickRate
 * @property string $writer
 * @property string $uid
 * @property string $createTime
 * @property string $provinces_id
 * @property string $modifyTime
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cid', 'isShow', 'isRmd', 'clickRate', 'uid', 'sort'], 'integer'],
            [['content'], 'string'],
            [['createTime', 'modifyTime'], 'safe'],
            [['title', 'type', 'themeImg', 'themeImgTm', 'brief', 'seokey', 'seoDepict', 'writer', 'provinces_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', '文章编号'),
            'title' => Yii::t('app', '文章标题'),
            'type' => Yii::t('app', '活动类型'),
            'cid' => Yii::t('app', '分类id'),
            'themeImg' => Yii::t('app', '主题图片'),
            'themeImgTm' => Yii::t('app', '主题缩略图'),
            'brief' => Yii::t('app', '简介'),
            'content' => Yii::t('app', '描述'),
            'seokey' => Yii::t('app', 'seo关键词'),
            'seoDepict' => Yii::t('app', 'seo描述'),
            'isShow' => Yii::t('app', '是否显示1显示 0不显示'),
            'isRmd' => Yii::t('app', '是否推荐1推荐 0 不推荐'),
            'clickRate' => Yii::t('app', '点击量'),
            'writer' => Yii::t('app', '作者'),
            'uid' => Yii::t('app', '发布人id'),
            'createTime' => Yii::t('app', 'Create Time'),
            'provinces_id' => Yii::t('app', '城市id'),
            'modifyTime' => Yii::t('app', 'Modify Time'),
            'sort' => Yii::t('app', '权重'),
        ];
    }

    public function getNewsclassify()
    {
        $newsclassify = Newsclassify::findOne($this->cid);
        return $newsclassify ? : (new Newsclassify());
    }
}
