<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "intergral_order_goods".
 *
 * @property string $id
 * @property string $order_id
 * @property string $goods_id
 * @property string $goods_pic
 * @property string $goods_name
 * @property string $goods_price
 * @property integer $goods_integral
 * @property integer $fallinto_state
 * @property integer $goods_num
 * @property string $fallInto
 * @property string $goods_pay_price
 * @property string $buyer_id
 * @property string $createTime
 * @property string $modifyTime
 * @property string $sku_id
 * @property string $attr
 * @property string $sell_price
 */
class IntergralOrderGoods extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_order_goods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'goods_id', 'goods_integral', 'fallinto_state', 'goods_num', 'buyer_id', 'sku_id'], 'integer'],
            [['goods_price', 'fallInto', 'goods_pay_price', 'sell_price'], 'number'],
            [['createTime', 'modifyTime'], 'safe'],
            [['attr'], 'string'],
            [['goods_pic', 'goods_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', '订单id'),
            'goods_id' => Yii::t('app', '商品id'),
            'goods_pic' => Yii::t('app', '商品图片'),
            'goods_name' => Yii::t('app', '商品名称'),
            'goods_price' => Yii::t('app', '商品价格'),
            'goods_integral' => Yii::t('app', '积分'),
            'fallinto_state' => Yii::t('app', '分成状态'),
            'goods_num' => Yii::t('app', '商品数量'),
            'fallInto' => Yii::t('app', '分成金额'),
            'goods_pay_price' => Yii::t('app', '商品实际成交价'),
            'buyer_id' => Yii::t('app', '买家ID'),
            'createTime' => Yii::t('app', 'Create Time'),
            'modifyTime' => Yii::t('app', 'Modify Time'),
            'sku_id' => Yii::t('app', 'sku编号'),
            'attr' => Yii::t('app', '规则'),
            'sell_price' => Yii::t('app', '售卖价格'),
        ];
    }
}
