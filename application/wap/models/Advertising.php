<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "advertising".
 *
 * @property string $id
 * @property string $adid
 * @property string $title
 * @property string $picture
 * @property string $pictureTm
 * @property string $url
 * @property integer $isShow
 * @property string $describe
 * @property integer $clickRate
 * @property integer $sort
 * @property integer $isTime
 * @property string $startTime
 * @property string $endTime
 * @property string $createTime
 * @property string $modifyTime
 * @property string $province_id
 */
class Advertising extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertising';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adid', 'isShow', 'clickRate', 'sort', 'isTime'], 'integer'],
            [['startTime', 'endTime', 'createTime', 'modifyTime'], 'safe'],
            [['title', 'picture', 'pictureTm', 'url', 'describe','province_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'adid' => Yii::t('app', '广告位id'),
            'title' => Yii::t('app', '标题'),
            'picture' => Yii::t('app', '图片'),
            'pictureTm' => Yii::t('app', '缩略图'),
            'url' => Yii::t('app', '跳转地址'),
            'isShow' => Yii::t('app', '是否显示1显示0为不显示'),
            'describe' => Yii::t('app', '描述'),
            'clickRate' => Yii::t('app', '点击量'),
            'sort' => Yii::t('app', '排序'),
            'isTime' => Yii::t('app', '是否是时间广告'),
            'startTime' => Yii::t('app', '开始时间'),
            'endTime' => Yii::t('app', '结束时间'),
            'createTime' => Yii::t('app', '创建时间'),
            'modifyTime' => Yii::t('app', '修改时间'),
            'province_id' => Yii::t('app', '城市ID'),
        ];
    }
}
