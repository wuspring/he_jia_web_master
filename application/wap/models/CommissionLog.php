<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "commission_log".
 *
 * @property integer $id
 * @property integer $memberId
 * @property string $orderId
 * @property string $amount
 * @property string $money
 * @property integer $status
 * @property string $create_time
 */
class CommissionLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['memberId', 'orderId', 'status'], 'integer'],
            [['amount', 'money'], 'number'],
            [['create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'memberId' => Yii::t('app', '会员'),
            'orderId' => Yii::t('app', 'Order ID'),
            'amount' => Yii::t('app', '订单金额'),
            'money' => Yii::t('app', '分成金额'),
            'status' => Yii::t('app', '领取状态 0表示未领取 1表示已分配'),
            'create_time' => Yii::t('app', '添加时间'),
        ];
    }
}
