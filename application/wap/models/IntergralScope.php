<?php

namespace wap\models;

use Yii;


class IntergralScope extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_scope';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['min','max'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'min' => Yii::t('app', '最小'),
            'max' => Yii::t('app', '最大'),
            'name' => Yii::t('app', '描述'),

        ];
    }
}
