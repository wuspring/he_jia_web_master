<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "store_shop".
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $provinces_id
 * @property string $store_name
 * @property string $description
 * @property string $address
 * @property string $work_days
 * @property string $work_times
 * @property string $notice
 * @property string $picitures
 * @property string $mobile
 * @property double $lat
 * @property double $lng
 */
class StoreShop extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_shop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'provinces_id'], 'required'],
            [['user_id', 'provinces_id'], 'integer'],
            [['description', 'address', 'work_times', 'notice', 'picitures'], 'string'],
            [['lat', 'lng'], 'number'],
            [['store_name', 'work_days', 'mobile'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', '商户id'),
            'provinces_id' => Yii::t('app', '所在城市'),
            'store_name' => Yii::t('app', '店铺名称'),
            'description' => Yii::t('app', '店铺简介'),
            'address' => Yii::t('app', '店铺地址'),
            'work_days' => Yii::t('app', '营业日期'),
            'work_times' => Yii::t('app', '营业时间'),
            'notice' => Yii::t('app', '宣传语、活动'),
            'picitures' => Yii::t('app', '图片'),
            'mobile' => Yii::t('app', '电话号码'),
            'lat' => Yii::t('app', '纬度'),
            'lng' => Yii::t('app', '经度'),
        ];
    }
}
