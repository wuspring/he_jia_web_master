<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "intergral_article".
 *
 * @property string $id
 * @property string $title
 * @property string $content
 * @property integer $read_num
 * @property integer $is_recommend
 * @property string $create_time
 * @property integer $is_del
 * @property integer $is_show
 */
class IntergralArticle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['read_num', 'is_recommend', 'is_del', 'is_show'], 'integer'],
            [['create_time'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', '文章标题'),
            'content' => Yii::t('app', '内容'),
            'read_num' => Yii::t('app', '阅读数量'),
            'is_recommend' => Yii::t('app', '是否推荐'),
            'create_time' => Yii::t('app', '时间'),
            'is_del' => Yii::t('app', '软删除'),
            'is_show' => Yii::t('app', '是否显示'),
        ];
    }
}
