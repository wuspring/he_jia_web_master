<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "goods_comment".
 *
 * @property string $id
 * @property string $goods_id
 * @property string $member_id
 * @property string $type
 * @property string $number
 * @property string $score
 * @property string $content
 * @property string $image
 * @property string $create_time
 */
class GoodsComment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['goods_id', 'member_id', 'type', 'number'], 'required'],
            [['goods_id', 'member_id'], 'integer'],
            [['score'], 'number'],
            [['content', 'image'], 'string'],
            [['create_time'], 'safe'],
            [['type', 'number'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'goods_id' => Yii::t('app', '商品ID'),
            'member_id' => Yii::t('app', '用户ID'),
            'type' => Yii::t('app', '评论类型 good: 好评,medium: 中评,bad:差评 '),
            'number' => Yii::t('app', '订单编号'),
            'score' => Yii::t('app', '评分'),
            'content' => Yii::t('app', '评论内容'),
            'image' => Yii::t('app', '上传图片'),
            'create_time' => Yii::t('app', '创建时间'),
        ];
    }
}
