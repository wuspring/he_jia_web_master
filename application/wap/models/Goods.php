<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "goods".
 *
 * @property string $id
 * @property string $goods_name
 * @property string $goods_jingle
 * @property integer $user_id
 * @property string $gc_id
 * @property string $egc_id
 * @property integer $use_attr
 * @property string $brand_id
 * @property string $goods_price
 * @property string $goods_promotion_price
 * @property integer $goods_promotion_type
 * @property string $goods_marketprice
 * @property string $goods_serial
 * @property integer $goods_storage_alarm
 * @property integer $goods_click
 * @property integer $goods_salenum
 * @property integer $goods_collect
 * @property string $attr
 * @property string $goods_spec
 * @property string $goods_body
 * @property string $mobile_body
 * @property integer $goods_storage
 * @property string $goods_pic
 * @property string $goods_image
 * @property integer $goods_state
 * @property string $goods_addtime
 * @property string $goods_edittime
 * @property integer $goods_vat
 * @property double $evaluation_good_star
 * @property integer $evaluation_count
 * @property integer $is_virtual
 * @property integer $is_appoint
 * @property integer $is_presell
 * @property integer $have_gift
 * @property integer $isdelete
 * @property integer $type
 * @property integer $integral
 * @property string $goods_weight
 * @property integer $is_second_kill
 * @property string $second_kill_groups
 * @property string $second_price
 * @property integer $second_limit
 * @property integer $goods_commend
 * @property integer $goods_hot
 * @property string $third_code
 * @property string $unit
 * @property string $admin_score
 */
class Goods extends \yii\db\ActiveRecord
{
    public $good_amount;
    public $ticket_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['goods_jingle', 'attr', 'goods_spec', 'goods_body', 'mobile_body', 'goods_image', 'second_kill_groups'], 'string'],
            [['user_id'], 'required'],
            [['user_id', 'gc_id', 'egc_id', 'use_attr', 'brand_id', 'goods_promotion_type', 'goods_storage_alarm', 'goods_click', 'goods_salenum', 'goods_collect', 'goods_storage', 'goods_state', 'goods_vat', 'evaluation_count', 'is_virtual', 'is_appoint', 'is_presell', 'have_gift', 'isdelete', 'type', 'integral', 'is_second_kill', 'second_limit', 'goods_commend', 'goods_hot', 'admin_score'], 'integer'],
            [['goods_price', 'goods_promotion_price', 'goods_marketprice', 'evaluation_good_star', 'goods_weight', 'second_price'], 'number'],
            [['goods_addtime', 'goods_edittime'], 'safe'],
            [['goods_name', 'goods_serial', 'goods_pic', 'unit'], 'string', 'max' => 255],
            [['third_code'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'goods_name' => Yii::t('app', '商品名称'),
            'goods_jingle' => Yii::t('app', '商品广告词'),
            'user_id' => Yii::t('app', '商家ID'),
            'gc_id' => Yii::t('app', '商品分类id'),
            'egc_id' => Yii::t('app', '拓展分类ID'),
            'use_attr' => Yii::t('app', '是否使用规格'),
            'brand_id' => Yii::t('app', '品牌id'),
            'goods_price' => Yii::t('app', '商品价格'),
            'goods_promotion_price' => Yii::t('app', '商品促销价格'),
            'goods_promotion_type' => Yii::t('app', '促销类型 0无促销，1团购，2限时折扣'),
            'goods_marketprice' => Yii::t('app', '市场价'),
            'goods_serial' => Yii::t('app', '商家编号'),
            'goods_storage_alarm' => Yii::t('app', '库存报警值'),
            'goods_click' => Yii::t('app', '商品点击数量'),
            'goods_salenum' => Yii::t('app', '销售数量'),
            'goods_collect' => Yii::t('app', '收藏数量'),
            'attr' => Yii::t('app', '规格属性'),
            'goods_spec' => Yii::t('app', '商品规格序列化'),
            'goods_body' => Yii::t('app', '商品详情'),
            'mobile_body' => Yii::t('app', '手机端详情'),
            'goods_storage' => Yii::t('app', '商品库存'),
            'goods_pic' => Yii::t('app', '商品主图'),
            'goods_image' => Yii::t('app', '商品相册'),
            'goods_state' => Yii::t('app', '商品状态 0下架，1正常，10违规（禁售）'),
            'goods_addtime' => Yii::t('app', '商品添加时间'),
            'goods_edittime' => Yii::t('app', '商品编辑时间'),
            'goods_vat' => Yii::t('app', '是否开具增值税发票 1是，0否'),
            'evaluation_good_star' => Yii::t('app', '好评星级'),
            'evaluation_count' => Yii::t('app', '评价数'),
            'is_virtual' => Yii::t('app', '是否为虚拟商品 1是，0否'),
            'is_appoint' => Yii::t('app', '是否是预约商品 1是，0否'),
            'is_presell' => Yii::t('app', '是否是预售商品 1是，0否'),
            'have_gift' => Yii::t('app', '是否拥有赠品'),
            'isdelete' => Yii::t('app', '是否删除'),
            'type' => Yii::t('app', '商品类型:1现金商品 2积分商品 3混合商品'),
            'integral' => Yii::t('app', '积分'),
            'goods_weight' => Yii::t('app', '重量 单位KG'),
            'is_second_kill' => Yii::t('app', '是否秒杀'),
            'second_kill_groups' => Yii::t('app', '秒杀时段'),
            'second_price' => Yii::t('app', '秒杀价格'),
            'second_limit' => Yii::t('app', '秒杀限制'),
            'goods_commend' => Yii::t('app', '商品推荐 1是，0否 默认0'),
            'goods_hot' => Yii::t('app', '热销产品'),
            'third_code' => Yii::t('app', '商品代号'),
            'unit' => Yii::t('app', '商品单位,例如套，户'),
            'admin_score' => Yii::t('app', '管理员打分'),
        ];
    }

    public  function getUser(){
        $model=User::findOne($this->user_id);
        return ($model)?:(new User());
    }
}
