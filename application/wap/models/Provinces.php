<?php

namespace wap\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "provinces".
 *
 * @property string $id
 * @property string $cname
 * @property string $upid
 * @property string $ename
 * @property string $pinyin
 * @property integer $level
 */
class Provinces extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'provinces';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'upid', 'level'], 'integer'],
            [['cname', 'ename', 'pinyin'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cname' => Yii::t('app', 'Cname'),
            'upid' => Yii::t('app', 'Upid'),
            'ename' => Yii::t('app', 'Ename'),
            'pinyin' => Yii::t('app', 'Pinyin'),
            'level' => Yii::t('app', 'Level'),
        ];
    }

    public static function getRegion($parentId=1)
    {
        $result = static::find()->where(['upid'=>$parentId])->asArray()->all();
        return ArrayHelper::map($result, 'id', 'cname');
    }

    public static function getModel($uip){

        $model=Provinces::findOne(['id'=>$uip]);
        if (empty($model)){
            $model=new Provinces();
        }
        return $model->upid;
    }

    public static function getlistname($id, $list,$leve=1){

        $model=  Provinces::findOne($id);
        array_unshift($list,$model->cname);
        if ($model->upid>0&&$model->level>$leve){
            $list= self::getlistname($model->upid,$list);
        }
        return $list;
    }

    public static function getname($id, $string='',$leve=1){

        $model=  Provinces::findOne($id);
        $string= $model->cname.$string;
        if ($model->upid>0&&$model->level>$leve){
            $string= self::getname($model->upid,$string);
        }
        return $string;
    }

    public static function getlistid($id, $list,$leve=1){

        $model=  Provinces::findOne($id);
        array_unshift($list,array('id'=>$model->id,'name'=>$model->cname));
        if ($model->upid>0&&$model->level>$leve){
            $list= self::getlistid($model->upid,$list);
        }
        return $list;
    }
}
