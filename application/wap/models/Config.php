<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "config".
 *
 * @property string $id
 * @property string $cKey
 * @property string $cValue
 * @property string $createTime
 * @property string $modifyTime
 */
class Config extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cValue'], 'string'],
            [['createTime', 'modifyTime'], 'safe'],
            [['cKey'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', '编号'),
            'cKey' => Yii::t('app', '键值'),
            'cValue' => Yii::t('app', '值'),
            'createTime' => Yii::t('app', '创建时间'),
            'modifyTime' => Yii::t('app', '修改时间'),
        ];
    }
}
