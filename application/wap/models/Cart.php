<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property string $id
 * @property string $buyerId
 * @property string $goodsId
 * @property string $goodsName
 * @property string $goodsPrice
 * @property string $totalPrice
 * @property string $goodsIntegral
 * @property integer $goodsNum
 * @property string $attr
 * @property string $attrInfo
 * @property string $sku_id
 * @property string $goods_image
 * @property string $integral
 * @property integer $ispay
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['buyerId', 'goodsId', 'goodsIntegral', 'goodsNum', 'sku_id', 'ispay'], 'integer'],
            [['goodsPrice', 'totalPrice', 'integral'], 'number'],
            [['attr', 'attrInfo'], 'string'],
            [['goodsName', 'goods_image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'buyerId' => Yii::t('app', '买家id'),
            'goodsId' => Yii::t('app', '商品id'),
            'goodsName' => Yii::t('app', '商品名称'),
            'goodsPrice' => Yii::t('app', '商品积分价格'),
            'totalPrice' => Yii::t('app', '总积分价格'),
            'goodsIntegral' => Yii::t('app', '商品积分'),
            'goodsNum' => Yii::t('app', '购买商品数量'),
            'attr' => Yii::t('app', '规格'),
            'attrInfo' => Yii::t('app', '规格详情'),
            'sku_id' => Yii::t('app', 'sku编号'),
            'goods_image' => Yii::t('app', '商品图片'),
            'integral' => Yii::t('app', '商品积分'),
            'ispay' => Yii::t('app', 'Ispay'),
        ];
    }


    public function getAmountLimit()
    {
        if ((int)$this->sku_id) {
            $sku = IntergralGoodsSku::findOne([
                'goods_id' => $this->goodsId,
                'id' => $this->sku_id
            ]);

            $amount = $sku ? $sku->num : 0;
        } else {
            $goods = IntergralGoods::findOne(['id' => $this->goodsId]);
            $amount = $goods->goods_storage;
        }

        return $amount;
    }

    public function getGoodsInfo()
    {
        $goods = IntergralGoods::findOne(['id' => $this->goodsId]);

        return $goods ?:(new IntergralGoods());
    }

    public function getSkuInfo()
    {
        $skuInfo = IntergralGoodsSku::findOne([
            'goods_id' => $this->goodsId,
            'id' => $this->sku_id
        ]);

        return $skuInfo?:(new IntergralGoodsSku());
    }

    public function getGoodsSku()
    {
        return $this->getSkuInfo();
    }

    public function getAttrInfo()
    {
        $skuInfo = $this->getSkuInfo();
        if (!strlen($skuInfo->attr)) {
            return '';
        }

        $skuIds = json_decode($skuInfo->attr,true);

        $goodSkus = IntergralGoodsAttr::find()->where([
            'and',
            ['in', 'id', $skuIds]
        ])->all();

        $info = '';
        foreach ($goodSkus AS $goodSku) {;
            $info .= "{$goodSku->groupInfo->group_name}:{$goodSku->attr_name}, ";
        }

        return $info;
    }

}
