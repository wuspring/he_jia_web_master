<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "brand".
 *
 * @property string $id
 * @property string $user_id
 * @property string $brand_name
 * @property string $brand_initial
 * @property string $brand_pic
 * @property integer $brand_sort
 * @property integer $brand_recommend
 * @property string $class_id
 * @property integer $show_type
 * @property integer $is_top
 * @property string $floor_id
 */
class Brand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brand';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'brand_sort', 'brand_recommend', 'class_id', 'show_type', 'is_top', 'floor_id'], 'integer'],
            [['brand_name', 'brand_pic'], 'string', 'max' => 100],
            [['brand_initial'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', '索引ID'),
            'user_id' => Yii::t('app', '商铺Id'),
            'brand_name' => Yii::t('app', '品牌名称'),
            'brand_initial' => Yii::t('app', '品牌首字母'),
            'brand_pic' => Yii::t('app', '图片'),
            'brand_sort' => Yii::t('app', '排序'),
            'brand_recommend' => Yii::t('app', '推荐，0为否，1为是，默认为0'),
            'class_id' => Yii::t('app', '所属分类id'),
            'show_type' => Yii::t('app', '品牌展示类型 0表示图片 1表示文字 '),
            'is_top' => Yii::t('app', '是否置顶'),
            'floor_id' => Yii::t('app', '展示楼层'),
        ];
    }
}
