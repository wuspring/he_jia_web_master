<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "integral_log".
 *
 * @property integer $id
 * @property integer $memberId
 * @property string $orderId
 * @property integer $pay_integral
 * @property integer $old_integral
 * @property integer $now_integral
 * @property string $create_time
 * @property string $expand
 */
class IntegralLog extends \yii\db\ActiveRecord
{
    const INTEGRALTYPE = 'signIn';

    const ORDER_ID_LOGIN = 'login';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'integral_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['memberId', 'pay_integral', 'old_integral', 'now_integral'], 'integer'],
            [['create_time'], 'safe'],
            [['expand','type'], 'string'],
            [['orderId'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'memberId' => Yii::t('app', '会员'),
            'orderId' => Yii::t('app', 'Order ID'),
            'pay_integral' => Yii::t('app', '消耗积分'),
            'old_integral' => Yii::t('app', '原有积分'),
            'now_integral' => Yii::t('app', '现有积分'),
            'create_time' => Yii::t('app', '添加时间'),
            'expand' => Yii::t('app', 'Expand'),
            'type' =>Yii::t('app', '积分类型'),
        ];
    }

    public function getDic(){
        return [
            self::INTEGRALTYPE => '签到',
        ];
    }

    public function getDicInfo(){

        $dicCont= $this->getDic();

        return isset($dicCont[$this->type])?$dicCont[$this->type]:'-';
    }

}
