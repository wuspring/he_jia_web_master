<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "member_level".
 *
 * @property integer $id
 * @property string $name
 * @property integer $score
 * @property string $sale
 */
class MemberLevel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id', 'score'], 'integer'],
            [['sale'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '等级名称'),
            'score' => Yii::t('app', '等级分数'),
            'sale' => Yii::t('app', '等级折扣率'),
        ];
    }
}
