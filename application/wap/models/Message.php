<?php

namespace wap\models;;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property integer $member_id
 * @property string $info
 * @property string $createTime
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id'], 'integer'],
            [['info'], 'string'],
            [['createTime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', '会员id'),
            'info' => Yii::t('app', '消息类容'),
            'createTime' => Yii::t('app', 'Create Time'),
        ];
    }
}
