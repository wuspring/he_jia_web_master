<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "store_info".
 *
 * @property string $id
 * @property string $user_id
 * @property integer $provinces_id
 * @property string $tab_text
 * @property string $describe
 * @property integer $store_amount
 * @property string $store_info
 * @property integer $judge_amount
 * @property string $score
 * @property string $advance_goods_id
 * @property string $order_goods_id
 * @property string $store_coupon_id
 * @property string $default_shop
 */
class StoreInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'provinces_id', 'store_amount', 'judge_amount'], 'integer'],
            [['tab_text', 'describe', 'store_info', 'advance_goods_id', 'order_goods_id', 'store_coupon_id'], 'string'],
            [['score'], 'number'],
            [['default_shop'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', '用户ID'),
            'provinces_id' => Yii::t('app', '所在城市'),
            'tab_text' => Yii::t('app', '宣传标识语'),
            'describe' => Yii::t('app', '店铺描述'),
            'store_amount' => Yii::t('app', '门店数量'),
            'store_info' => Yii::t('app', '门店详情'),
            'judge_amount' => Yii::t('app', '评价数量'),
            'score' => Yii::t('app', '评价分数'),
            'advance_goods_id' => Yii::t('app', '爆款预约商品'),
            'order_goods_id' => Yii::t('app', '下订单 享特价商品'),
            'store_coupon_id' => Yii::t('app', '店铺优惠劵'),
            'default_shop' => Yii::t('app', '默认门店'),
        ];
    }
}
