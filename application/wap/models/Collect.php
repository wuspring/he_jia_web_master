<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "collect".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $goods_id
 * @property string $create_time
 * @property string $type
 * @property integer $shop_id
 */
class Collect extends \yii\db\ActiveRecord
{

    const  TYPE_STORE='STORE';
    const  TYPE_GOOD='GOOD';
    const  TYPE_RESERVE_GOOD='RESERVE_GOOD';
    const  TYPE_SUBSCRIBE_GOOD='SUBSCRIBE_GOOD';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'collect';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'goods_id', 'shop_id','ticket_id'], 'integer'],
            [['create_time'], 'safe'],
            [['type','type_wap'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', '会员ID'),
            'goods_id' => Yii::t('app', '商品ID'),
            'create_time' => Yii::t('app', '添加时间'),
            'type' => Yii::t('app', '类型'),
            'shop_id' => Yii::t('app', '店铺id'),
            'ticket_id' => Yii::t('app', '展会id'),
            'type_wap' => Yii::t('app', '类型_手机端'),
        ];
    }
    public function  getTypeDic(){
        return [
            self::TYPE_GOOD => '商品收藏',
            self::TYPE_STORE => '店铺收藏',
            self::TYPE_RESERVE_GOOD => '预定收藏',
            self::TYPE_SUBSCRIBE_GOOD => '预约收藏'
        ];
    }

    public function  getTypeInfo(){

        $dic = $this->getTypeDic();

        return isset($dic[$this->type]) ?  $dic[$this->type] : '-';

    }
}
