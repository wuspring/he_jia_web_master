<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "fitup_attr".
 *
 * @property string $id
 * @property string $name
 * @property integer $pid
 * @property integer $sort
 * @property integer $is_del
 * @property string $create_time
 */
class FitupAttr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fitup_attr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pid', 'sort', 'is_del'], 'integer'],
            [['create_time'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '装修类型'),
            'pid' => Yii::t('app', '父类'),
            'sort' => Yii::t('app', '排序'),
            'is_del' => Yii::t('app', '软删除'),
            'create_time' => Yii::t('app', '时间'),
        ];
    }
}
