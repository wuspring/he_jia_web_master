<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "intergral_goods_attr_group".
 *
 * @property string $id
 * @property string $group_name
 * @property integer $is_delete
 */
class IntergralGoodsAttrGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_goods_attr_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_delete'], 'integer'],
            [['group_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', '属性组编号'),
            'group_name' => Yii::t('app', '属性组名称'),
            'is_delete' => Yii::t('app', '是否删除'),
        ];
    }
}
