<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property string $id
 * @property string $title
 * @property string $keyword
 * @property string $description
 * @property string $parent_id
 * @property integer $is_index
 * @property string $icon
 * @property string $rank
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['parent_id', 'is_index', 'rank'], 'integer'],
            [['title', 'keyword', 'description', 'icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', '栏目标题'),
            'keyword' => Yii::t('app', '关键字'),
            'description' => Yii::t('app', '描述'),
            'parent_id' => Yii::t('app', '上级目录'),
            'is_index' => Yii::t('app', 'Is Index'),
            'icon' => Yii::t('app', 'Icon'),
            'rank' => Yii::t('app', '栏目权重'),
        ];
    }
}
