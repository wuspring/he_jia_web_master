<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "jingxiaoshang".
 *
 * @property string $id
 * @property integer $member_id
 * @property string $name
 * @property string $phone
 * @property string $status
 * @property string $province
 * @property string $city
 * @property string $region
 * @property string $level
 * @property string $good_ids
 * @property string $create_time
 */
class Jingxiaoshang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jingxiaoshang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'status'], 'required'],
            [['member_id', 'province', 'city', 'region'], 'integer'],
            [['good_ids'], 'string'],
            [['create_time'], 'safe'],
            [['name', 'level'], 'string', 'max' => 255],
            [['phone', 'status'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', 'Member ID'),
            'name' => Yii::t('app', '姓名'),
            'phone' => Yii::t('app', '手机号码'),
            'status' => Yii::t('app', '申请状态'),
            'province' => Yii::t('app', '经销商省编码'),
            'city' => Yii::t('app', '经销商市编码'),
            'region' => Yii::t('app', '经销商区编码'),
            'level' => Yii::t('app', '经销商等级'),
            'good_ids' => Yii::t('app', '代理商品'),
            'create_time' => Yii::t('app', '申请时间'),
        ];
    }
}
