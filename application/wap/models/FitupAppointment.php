<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "fitup_appointment".
 *
 * @property string $id
 * @property integer $member_id
 * @property string $name
 * @property string $mobile
 * @property string $create_time
 * @property integer $is_del
 * @property integer $is_deal
 * @property string $dispatch_people
 * @property string $remarks
 */
class FitupAppointment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fitup_appointment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'is_del', 'is_deal','user_id'], 'integer'],
            [['create_time'], 'safe'],
            [['remarks'], 'string'],
            [['name', 'mobile', 'dispatch_people'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', '用户id'),
            'name' => Yii::t('app', '名字'),
            'mobile' => Yii::t('app', '手机号'),
            'create_time' => Yii::t('app', '时间'),
            'is_del' => Yii::t('app', '软删除'),
            'is_deal' => Yii::t('app', '是否处理'),
            'dispatch_people' => Yii::t('app', '派遣人员'),
            'remarks' => Yii::t('app', '备注'),
            'user_id' => Yii::t('app', '预约的店铺'),
        ];
    }
}
