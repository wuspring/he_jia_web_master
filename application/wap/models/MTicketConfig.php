<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "m_ticket_config".
 *
 * @property string $id
 * @property integer $ticket_id
 * @property string $banners
 * @property string $colors
 * @property string $guiders
 * @property string $ad_1
 * @property string $ad_2
 * @property string $ad_3
 * @property string $ad_4
 * @property string $ad_ticket_address
 * @property string $create_time
 * @property string $top_guiders
 */
class MTicketConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm_ticket_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id'], 'integer'],
            [['banners', 'top_guiders', 'guiders', 'ad_1', 'ad_2', 'ad_3', 'ad_4', 'ad_ticket_address'], 'string'],
            [['create_time'], 'safe'],
            [['colors'], 'string', 'max' => 255],
            [['ticket_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticket_id' => 'Ticket ID',
            'banners' => 'Banners',
            'colors' => 'Colors',
            'guiders' => 'Guiders',
            'ads' => 'Ads',
            'create_time' => 'Create Time',
        ];
    }

    public function getTicket()
    {
        $ticket = Ticket::findOne($this->ticket_id);
        return $ticket ? : (new Ticket());
    }
}
