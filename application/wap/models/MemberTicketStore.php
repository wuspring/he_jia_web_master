<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "member_ticket_store".
 *
 * @property string $id
 * @property string $member_id
 * @property integer $ticket_id
 * @property string $store_id
 */
class MemberTicketStore extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member_ticket_store';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'ticket_id', 'store_id'], 'integer'],
            [['member_id', 'ticket_id', 'store_id'], 'unique', 'targetAttribute' => ['member_id', 'ticket_id', 'store_id'], 'message' => 'The combination of 会员ID, 展会ID and 店铺ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', '会员ID'),
            'ticket_id' => Yii::t('app', '展会ID'),
            'store_id' => Yii::t('app', '店铺ID'),
        ];
    }
}
