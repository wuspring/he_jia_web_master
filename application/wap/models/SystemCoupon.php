<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "system_coupon".
 *
 * @property string $id
 * @property string $store_id
 * @property string $member_id
 * @property string $ticket_id
 * @property string $provinces_id
 * @property string $money
 * @property integer $status
 * @property string $code
 * @property string $create_time
 */
class SystemCoupon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_coupon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id', 'member_id', 'ticket_id', 'provinces_id', 'status'], 'integer'],
            [['ticket_id', 'provinces_id'], 'required'],
            [['money'], 'number'],
            [['create_time'], 'safe'],
            [['code'], 'string', 'max' => 32],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'store_id' => Yii::t('app', '店铺编号'),
            'member_id' => Yii::t('app', 'member ID'),
            'ticket_id' => Yii::t('app', '优惠券名称'),
            'provinces_id' => Yii::t('app', '城市ID'),
            'money' => Yii::t('app', '抵扣金额'),
            'status' => Yii::t('app', '券状态'),
            'code' => Yii::t('app', '券码'),
            'create_time' => Yii::t('app', 'Create Time'),
        ];
    }

    public function create()
    {
        if (!strlen($this->code) and !$this->status) {
            $key = (string)$this->ticket_id;
            strlen($this->ticket_id) < 2 and $key = "0{$key}";

            $dic = '01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $key = $key . createRandKey(6, $dic);

            $record = self::findOne([
                'code' => $key
            ]);

            if ($record) {
                return $this->create();
            }

            $this->code = $key;
            $this->create_time = date('Y-m-d H:i:s');
        }

        return $this->save();
    }
}
