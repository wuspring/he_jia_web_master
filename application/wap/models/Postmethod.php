<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "postmethod".
 *
 * @property integer $id
 * @property string $name
 * @property string $expressid
 * @property string $code
 * @property integer $type
 * @property string $first_condition
 * @property string $first_money
 * @property string $other_condition
 * @property string $other_money
 * @property integer $status
 * @property string $createTime
 * @property string $send_area
 * @property string $not_area
 * @property integer $is_default
 */
class Postmethod extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'postmethod';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['expressid', 'type', 'status', 'is_default'], 'integer'],
            [['first_condition', 'first_money', 'other_condition', 'other_money'], 'number'],
            [['createTime'], 'safe'],
            [['send_area', 'not_area'], 'string'],
            [['name', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '模版名称'),
            'expressid' => Yii::t('app', '物流公司id'),
            'code' => Yii::t('app', '物流公司编号'),
            'type' => Yii::t('app', '1 重量计费 2按件计费'),
            'first_condition' => Yii::t('app', '首重'),
            'first_money' => Yii::t('app', 'First Money'),
            'other_condition' => Yii::t('app', 'Other Condition'),
            'other_money' => Yii::t('app', 'Other Money'),
            'status' => Yii::t('app', 'Status'),
            'createTime' => Yii::t('app', 'Create Time'),
            'send_area' => Yii::t('app', '配送区域'),
            'not_area' => Yii::t('app', '不配送区域'),
            'is_default' => Yii::t('app', '默认'),
        ];
    }
}
