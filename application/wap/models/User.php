<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $username
 * @property string $password
 * @property integer $role
 * @property integer $sex
 * @property string $birthday
 * @property integer $status
 * @property string $avatarTm
 * @property string $avatar
 * @property string $email
 * @property string $createTime
 * @property string $modifyTime
 * @property string $last_visit
 * @property integer $userType
 * @property string $authkey
 * @property string $accessToken
 * @property string $remainder
 * @property string $nickname
 * @property string $assignment
 * @property integer $provinces_id
 * @property string $gc_ids
 * @property integer $user_type
 * @property string $end_time
 * @property integer $change_city
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role', 'sex', 'status', 'userType', 'provinces_id', 'user_type', 'change_city'], 'integer'],
            [['birthday', 'createTime', 'modifyTime', 'last_visit', 'end_time'], 'safe'],
            [['remainder'], 'number'],
            [['gc_ids'], 'string'],
            [['username', 'password', 'avatarTm', 'avatar', 'email', 'authkey', 'accessToken', 'nickname', 'assignment'], 'string', 'max' => 255],
            [['username'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', '用户名'),
            'password' => Yii::t('app', '密码'),
            'role' => Yii::t('app', '角色'),
            'sex' => Yii::t('app', '性别'),
            'birthday' => Yii::t('app', '生日'),
            'status' => Yii::t('app', '状态'),
            'avatarTm' => Yii::t('app', '头像缩略图'),
            'avatar' => Yii::t('app', '头像'),
            'email' => Yii::t('app', '邮箱'),
            'createTime' => Yii::t('app', '创建时间'),
            'modifyTime' => Yii::t('app', '修改时间'),
            'last_visit' => Yii::t('app', '最后登录时间'),
            'userType' => Yii::t('app', '用户类型'),
            'authkey' => Yii::t('app', 'Authkey'),
            'accessToken' => Yii::t('app', 'Access Token'),
            'remainder' => Yii::t('app', 'Remainder'),
            'nickname' => Yii::t('app', '用户名称'),
            'assignment' => Yii::t('app', '用户权限'),
            'provinces_id' => Yii::t('app', '所在城市'),
            'gc_ids' => Yii::t('app', '归属分类'),
            'user_type' => Yii::t('app', '用户类型'),
            'end_time' => Yii::t('app', '店铺有效期'),
            'change_city' => Yii::t('app', '可切换城市'),
        ];
    }



    public function refreshCache()
    {
        $this->modifyTime = date('Y-m-d H:i:s');

        return $this->save();
    }

    public  function getStoreCount(){

        $storeCount = StoreShop::find()->andWhere(['user_id'=>$this->id])->count();
        return empty($storeCount)?0:$storeCount;
    }
}
