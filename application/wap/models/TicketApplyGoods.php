<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "ticket_apply_goods".
 *
 * @property string $id
 * @property integer $ticket_id
 * @property integer $user_id
 * @property integer $good_id
 * @property string $good_pic
 * @property string $good_price
 * @property string $type
 * @property string $create_time
 * @property string $good_amount
 * @property integer $status
 * @property integer $is_index
 * @property integer $is_zhanhui
 * @property integer $sort
 */
class TicketApplyGoods extends \yii\db\ActiveRecord
{

    const TYPE_ORDER = 'ORDER';
    const TYPE_COUPON = 'COUPON';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_apply_goods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ticket_id', 'user_id', 'good_id', 'good_amount', 'status', 'is_index', 'is_zhanhui', 'sort'], 'integer'],
            [['good_pic'], 'string'],
            [['good_price'], 'number'],
            [['create_time'], 'safe'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ticket_id' => Yii::t('app', '展会ID'),
            'user_id' => Yii::t('app', '商铺ID'),
            'good_id' => Yii::t('app', '商品ID'),
            'good_pic' => Yii::t('app', '商品活动图片'),
            'good_price' => Yii::t('app', '商品价格'),
            'type' => Yii::t('app', '商品类型'),
            'create_time' => Yii::t('app', 'Create Time'),
            'good_amount' => Yii::t('app', '库存'),
            'status' => Yii::t('app', '审核状态'),
            'is_index' => Yii::t('app', '首页置顶'),
            'sort' => Yii::t('app', '排序'),
        ];
    }

    /**
     * 获取类型描述
     * @return array
     */
    public function getTypeDic()
    {
        return [
            self::TYPE_COUPON => '爆款预约',
//            self::TYPE_ORDER => '下订单 享特价'
            self::TYPE_ORDER => '预存 享特价'
        ];
    }

    /**
     * 关联展会
     * @return Ticket
     */
    public function getTicket()
    {
        $ticket = Ticket::findOne([
            'id' => $this->ticket_id,
        ]);
        return $ticket ? :(new Ticket());
    }

    /**
     * 关联商品
     * @return null|Goods|\yii\db\ActiveQuery
     */
    public function getGoods(){
        $model = Goods::findOne($this->good_id);
        if(empty($model)){
            $model = new Goods();
            return $model;
        }
        return $this->hasOne(Goods::className(),['id'=>'good_id']);
    }

    /**
     * 关联店铺
     * @return User
     */
    public function getUser()
    {
        $user = User::findOne($this->user_id);
        return $user ?: (new User());
    }

    public static function primaryKey()
    {
        return ['id'];
    }
}
