<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "pay_sn_order_relation".
 *
 * @property string $id
 * @property string $pay_sn
 * @property string $orderid
 * @property integer $member_id
 * @property integer $status
 * @property string $pay_amount
 * @property string $create_time
 * @property string $type
 */
class PaySnOrderRelation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pay_sn_order_relation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pay_sn', 'orderid', 'member_id', 'status', 'create_time'], 'required'],
            [['member_id', 'status'], 'integer'],
            [['pay_amount'], 'number'],
            [['create_time'], 'safe'],
            [['pay_sn', 'orderid'], 'string', 'max' => 50],
            [['type'], 'string', 'max' => 255],
            [['pay_sn', 'orderid'], 'unique', 'targetAttribute' => ['pay_sn', 'orderid'], 'message' => 'The combination of 支付单号 and 订单号 has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pay_sn' => Yii::t('app', '支付单号'),
            'orderid' => Yii::t('app', '订单号'),
            'member_id' => Yii::t('app', '会员ID'),
            'status' => Yii::t('app', '状态'),
            'pay_amount' => Yii::t('app', '支付金额'),
            'create_time' => Yii::t('app', '添加时间'),
            'type' => Yii::t('app', '订单类型'),
        ];
    }
}
