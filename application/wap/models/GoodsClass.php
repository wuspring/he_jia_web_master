<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "goods_class".
 *
 * @property string $id
 * @property string $name
 * @property string $fid
 * @property string $icoImg
 * @property integer $sort
 * @property string $createTime
 * @property string $modifyTime
 * @property integer $is_del
 * @property integer $is_filter
 * @property string $keywords
 * @property integer $is_show
 */
class GoodsClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fid', 'sort', 'is_del', 'is_filter', 'is_show'], 'integer'],
            [['createTime', 'modifyTime'], 'safe'],
            [['keywords'], 'string'],
            [['name', 'icoImg'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '名称'),
            'fid' => Yii::t('app', '父级'),
            'icoImg' => Yii::t('app', '图标'),
            'sort' => Yii::t('app', '排序'),
            'createTime' => Yii::t('app', 'Create Time'),
            'modifyTime' => Yii::t('app', 'Modify Time'),
            'is_del' => Yii::t('app', '1是删除'),
            'is_filter' => Yii::t('app', 'Is Filter'),
            'keywords' => Yii::t('app', 'Keywords'),
            'is_show' => Yii::t('app', '1为不显示'),
        ];
    }

    //从顶层逐级向下获取子类
    public static function getLists($pid = 0, &$lists = array(), $deep = 1) {
        $parentData = GoodsClass::find()->where(['fid'=>$pid])->orderBy('sort asc')->all();
        if (!empty($parentData)) {
            $fen = "|";
            for ($i=0;  $i< $deep; $i++) {
                $fen .= '---';
            }

            foreach ($parentData as $key => $value) {
                $value['name'] =$fen.$value['name'];
                $id = $value->id;
                $lists[] = $value;
                self::getLists($id, $lists, ++$deep); //进入子类之前深度+1
                --$deep; //从子类退出之后深度-1
            }
        }
        return $lists;
    }
}
