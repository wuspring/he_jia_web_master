<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "system_coupon_log".
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $coupon_id
 * @property integer $goods_id
 * @property string $type
 * @property string $create_time
 */
class SystemCouponLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_coupon_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'coupon_id'], 'required'],
            [['user_id', 'coupon_id', 'goods_id'], 'integer'],
            [['create_time'], 'safe'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', '商户ID'),
            'coupon_id' => Yii::t('app', '系统优惠券ID'),
            'goods_id' => Yii::t('app', '商品ID'),
            'type' => Yii::t('app', '类型'),
            'create_time' => Yii::t('app', '使用时间'),
        ];
    }
}
