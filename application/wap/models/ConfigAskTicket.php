<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "coupon".
 *
 * @property string $id
 * @property string $store_id
 * @property string $name
 * @property string $desc
 * @property string $pic_url
 * @property integer $discount_type
 * @property string $min_price
 * @property string $sub_price
 * @property string $discount
 * @property integer $expire_type
 * @property integer $expire_day
 * @property string $begin_time
 * @property string $end_time
 * @property string $addtime
 * @property integer $is_delete
 * @property integer $total_count
 * @property integer $is_join
 * @property integer $sort
 * @property integer $is_integral
 * @property integer $integral
 * @property string $price
 * @property integer $total_num
 * @property integer $type
 * @property integer $user_num
 * @property string $goods_id_list
 */
class ConfigAskTicket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id', 'discount_type', 'expire_type', 'expire_day', 'is_delete', 'total_count', 'is_join', 'sort', 'is_integral', 'integral', 'total_num', 'type', 'user_num'], 'integer'],
            [['name', 'addtime'], 'required'],
            [['min_price', 'sub_price', 'discount', 'price'], 'number'],
            [['begin_time', 'end_time', 'addtime'], 'safe'],
            [['name', 'goods_id_list'], 'string', 'max' => 255],
            [['desc'], 'string', 'max' => 2000],
            [['pic_url'], 'string', 'max' => 2048],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'store_id' => Yii::t('app', '店铺编号'),
            'name' => Yii::t('app', '优惠券名称'),
            'desc' => Yii::t('app', '优惠券介绍'),
            'pic_url' => Yii::t('app', '图片'),
            'discount_type' => Yii::t('app', '优惠券类型：1=满减，2=折扣'),
            'min_price' => Yii::t('app', '最低消费金额'),
            'sub_price' => Yii::t('app', '优惠金额'),
            'discount' => Yii::t('app', '折扣率'),
            'expire_type' => Yii::t('app', '到期类型：1=领取后N天过期，2=指定有效期'),
            'expire_day' => Yii::t('app', '有效天数，expire_type=1时'),
            'begin_time' => Yii::t('app', '有效期开始时间'),
            'end_time' => Yii::t('app', '有效期结束时间'),
            'addtime' => Yii::t('app', 'Addtime'),
            'is_delete' => Yii::t('app', 'Is Delete'),
            'total_count' => Yii::t('app', '发放总数量'),
            'is_join' => Yii::t('app', '是否加入领券中心 1--不加入领券中心 2--加入领券中心'),
            'sort' => Yii::t('app', '排序按升序排列'),
            'is_integral' => Yii::t('app', '是否加入积分商城 1--不加入 2--加入'),
            'integral' => Yii::t('app', '兑换需要积分数量'),
            'price' => Yii::t('app', '售价'),
            'total_num' => Yii::t('app', '积分商城发放总数'),
            'type' => Yii::t('app', '券类型1店铺券 2 商品券,3,外部投放券 4通用券'),
            'user_num' => Yii::t('app', '每人限制兑换数量'),
            'goods_id_list' => Yii::t('app', '商品'),
        ];
    }
}
