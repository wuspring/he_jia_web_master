<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "store_gc_score".
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $provinces_id
 * @property integer $gc_id
 * @property string $score
 * @property string $admin_score
 */
class StoreGcScore extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_gc_score';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'provinces_id', 'gc_id'], 'required'],
            [['user_id', 'provinces_id', 'gc_id', 'score', 'admin_score'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', '商户Id'),
            'provinces_id' => Yii::t('app', 'city id'),
            'gc_id' => Yii::t('app', 'goods_class id'),
            'score' => Yii::t('app', '原始分值'),
            'admin_score' => Yii::t('app', '管理员打分'),
        ];
    }
}
