<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "ticket_info".
 *
 * @property integer $id
 * @property integer $ticket_id
 * @property integer $provinces_id
 * @property string $type
 * @property string $ticket_name
 * @property string $ticket_amount
 */
class TicketInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id', 'provinces_id'], 'required'],
            [['ticket_id', 'provinces_id', 'ticket_amount'], 'integer'],
            [['type', 'ticket_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ticket_id' => Yii::t('app', 'ticket id'),
            'provinces_id' => Yii::t('app', '城市 ID'),
            'type' => Yii::t('app', '类型'),
            'ticket_name' => Yii::t('app', '票名'),
            'ticket_amount' => Yii::t('app', '需求总数量'),
        ];
    }

    public function getTicket()
    {
        $ticket = Ticket::findOne($this->ticket_id);
        return $ticket ?:(new Ticket());
    }
}
