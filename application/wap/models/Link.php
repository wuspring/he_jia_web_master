<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "link".
 *
 * @property string $id
 * @property string $name
 * @property string $url
 * @property string $imgval
 * @property integer $type
 * @property string $createTime
 * @property string $modifyTime
 * @property string $city
 */
class Link extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'city'], 'integer'],
            [['createTime', 'modifyTime'], 'safe'],
            [['name', 'url', 'imgval'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '链接名称'),
            'url' => Yii::t('app', '链接地址'),
            'imgval' => Yii::t('app', '图标地址'),
            'type' => Yii::t('app', '链接类型1文字2 图片'),
            'createTime' => Yii::t('app', 'Create Time'),
            'modifyTime' => Yii::t('app', 'Modify Time'),
            'city' => Yii::t('app', '城市编号'),
        ];
    }
}
