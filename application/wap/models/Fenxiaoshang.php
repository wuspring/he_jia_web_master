<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "fenxiaoshang".
 *
 * @property string $id
 * @property integer $member_id
 * @property string $name
 * @property string $phone
 * @property string $recommend
 * @property string $status
 * @property string $create_time
 */
class Fenxiaoshang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fenxiaoshang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id'], 'required'],
            [['member_id'], 'integer'],
            [['create_time'], 'safe'],
            [['name', 'recommend'], 'string', 'max' => 255],
            [['phone', 'status'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', 'Member ID'),
            'name' => Yii::t('app', '姓名'),
            'phone' => Yii::t('app', '手机号码'),
            'recommend' => Yii::t('app', '推荐人信息'),
            'status' => Yii::t('app', '申请状态'),
            'create_time' => Yii::t('app', '申请时间'),
        ];
    }
}
