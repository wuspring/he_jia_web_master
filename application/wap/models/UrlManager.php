<?php


namespace wap\models;
use common\models\IpMemory;
use common\models\Provinces;
use DL\Project\TecentMapApi;
use DL\service\CacheService;
use DL\vendor\ConfigService;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\caching\Cache;
use yii\web\Request;
use yii\web\UrlRule;

class UrlManager extends  \yii\web\UrlManager
{

    public function parseRequest($request)
    {
        if ($this->enablePrettyUrl) {
            $pathInfo = $request->getPathInfo();
            /* @var $rule UrlRule */
            foreach ($this->rules as $rule) {
                if (($result = $rule->parseRequest($this, $request)) !== false) {
                    return $result;
                }
            }

            if ($this->enableStrictParsing) {
                return false;
            }

            Yii::trace('No matching URL rules. Using default URL parsing logic.', __METHOD__);

            // Ensure, that $pathInfo does not end with more than one slash.
            if (strlen($pathInfo) > 1 && substr_compare($pathInfo, '//', -2, 2) === 0) {
                return false;
            }

            $suffix = (string) $this->suffix;
            if ($suffix !== '' && $pathInfo !== '') {
                $n = strlen($this->suffix);
                if (substr_compare($pathInfo, $this->suffix, -$n, $n) === 0) {
                    $pathInfo = substr($pathInfo, 0, -$n);
                    if ($pathInfo === '') {
                        // suffix alone is not allowed
                        return false;
                    }
                } else {
                    // suffix doesn't match
                    return false;
                }
            }

            return [$pathInfo, []];
        } else {
            Yii::trace('Pretty URL not enabled. Using default URL parsing logic.', __METHOD__);
            $route = $request->getQueryParam($this->routeParam, '');
            if (is_array($route)) {
                $route = '';
            }

            return [(string) $route, []];
        }
    }

    public function createUrl($params)
    {
        $params = (array) $params;
        $anchor = isset($params['#']) ? '#' . $params['#'] : '';
        unset($params['#'], $params[$this->routeParam]);

        $route = trim($params[0], '/');


        unset($params[0]);
        if ($route==="index/index"){
            $file  = 'log.txt';//要写入文件的文件名（可以是任意文件名），如果文件不存在，将会创建一个
            $content = $route."|";

            file_put_contents($file, $content,FILE_APPEND);// 这个函数支持版本(PHP 5)
            $cityid=$this->getRequestCity();
            $provinces = Provinces::findOne($cityid);
            return $provinces->pinyin;
        }elseif(strpos($route,'jz') !== false)
        {
            $url=str_replace('jz','jbh',$route);

            if (!empty($params) && ($query = http_build_query($params)) !== '') {
                $url .= '?' . $query;
            }
            $cityid=$this->getRequestCity();
            $provinces = Provinces::findOne($cityid);
            return  $provinces->pinyin.'/'.$url;
        }

        $baseUrl = $this->showScriptName || !$this->enablePrettyUrl ? $this->getScriptUrl() : $this->getBaseUrl();

        if ($this->enablePrettyUrl) {
            $cacheKey = $route . '?';
            foreach ($params as $key => $value) {
                if ($value !== null) {
                    $cacheKey .= $key . '&';
                }
            }

            $url = $this->getUrlFromCache($cacheKey, $route, $params);

            if ($url === false) {
                $cacheable = true;
                foreach ($this->rules as $rule) {
                    /* @var $rule UrlRule */
                    if (!empty($rule->defaults) && $rule->mode !== UrlRule::PARSING_ONLY) {
                        // if there is a rule with default values involved, the matching result may not be cached
                        $cacheable = false;
                    }
                    if (($url = $rule->createUrl($this, $route, $params)) !== false) {
                        if ($cacheable) {
                            $this->setRuleToCache($cacheKey, $rule);
                        }
                        break;
                    }
                }
            }

            if ($url !== false) {
                if (strpos($url, '://') !== false) {
                    if ($baseUrl !== '' && ($pos = strpos($url, '/', 8)) !== false) {
                        return substr($url, 0, $pos) . $baseUrl . substr($url, $pos) . $anchor;
                    } else {
                        return $url . $baseUrl . $anchor;
                    }
                } else {
                    return "$baseUrl/{$url}{$anchor}";
                }
            }

            if ($this->suffix !== null) {
                $route .= $this->suffix;
            }
            if (!empty($params) && ($query = http_build_query($params)) !== '') {
                $route .= '?' . $query;
            }

            return "$baseUrl/{$route}{$anchor}";
        } else {
            $url = "$baseUrl?{$this->routeParam}=" . urlencode($route);
            if (!empty($params) && ($query = http_build_query($params)) !== '') {
                $url .= '&' . $query;
            }

            return $url . $anchor;
        }
    }

    /**
     * 获取当前城市信息
     *
     * @return int provinces id
     */
    protected function getRequestCity()
    {

        $cityid= \yii::$app->session->get('citybianhao');
        if(isset($cityid)){
            return $cityid;

        }else{
            // 默认城市
            $cityid = ConfigService::init(ConfigService::EXPAND)->get('default_city');
            $ip = $_SERVER['REMOTE_ADDR'];
            if (!strlen($ip) and isset($_SERVER['  HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['  HTTP_X_FORWARDED_FOR'];
            }

            $ipRelation = (array)CacheService::init()->get('__IP_RELATION_INFO__');
            if (isset($ipRelation[$ip])) {
                $requestId = $ipRelation[$ip];
            } else {
                $uniqueKey = md5(trim($ip));

                $ipInfo = IpMemory::findOne([
                    'unique_key' => $uniqueKey
                ]);

                if ($ipInfo) {
                    $requestId = $ipInfo->provinces_id;
                } else {
                    $position = TecentMapApi::init()->getPositionByIp();
                    if($position->status==0){
                        $provinces = Provinces::findOne($position->result->ad_info->adcode);
                        $requestId = $provinces ? $provinces->id : $cityid;
                    }

                    $ipInfo = new IpMemory();
                    $ipInfo->ip = $ip;
                    $ipInfo->provinces_id = $requestId;
                    $ipInfo->unique_key = $uniqueKey;
                    $ipInfo->save();
                    $ipRelation[$ip] = $requestId;
                    CacheService::init()->set('__IP_RELATION_INFO__', $ipRelation);
                    $cityid= $requestId;
                }

            }

        }


        return $cityid;
    }
}