<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "fitup_case".
 *
 * @property string $id
 * @property string $name
 * @property string $fitup_company
 * @property string $cover_pic
 * @property string $address
 * @property string $design
 * @property string $area
 * @property integer $house_type
 * @property integer $design_style
 * @property string $budget
 * @property string $tab_text
 * @property string $tab
 * @property string $create_time
 * @property integer $is_del
 * @property integer $user_id
 * @property integer $is_recommend
 * @property string $provinces_id
 */
class FitupCase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fitup_case';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['house_type', 'design_style', 'is_del', 'user_id', 'is_recommend'], 'integer'],
            [['tab_text'], 'string'],
            [['create_time'], 'safe'],
            [['name', 'fitup_company', 'cover_pic', 'address', 'design', 'area', 'budget', 'tab', 'provinces_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'tab'),
            'name' => Yii::t('app', '名字'),
            'fitup_company' => Yii::t('app', '装修公司'),
            'cover_pic' => Yii::t('app', '封面图'),
            'address' => Yii::t('app', '地址'),
            'design' => Yii::t('app', '设计师'),
            'area' => Yii::t('app', '建筑面积'),
            'house_type' => Yii::t('app', '户型'),
            'design_style' => Yii::t('app', '设计风格'),
            'budget' => Yii::t('app', '预算'),
            'tab_text' => Yii::t('app', 'tab中包含的值'),
            'tab' => Yii::t('app', 'tab数组'),
            'create_time' => Yii::t('app', 'Create Time'),
            'is_del' => Yii::t('app', 'Is Del'),
            'user_id' => Yii::t('app', '店铺id'),
            'is_recommend' => Yii::t('app', '是否推荐'),
            'provinces_id' => Yii::t('app', '城市id'),
        ];
    }

    public function getFitupAttr($attrId){

        $attr = FitupAttr::findOne($attrId);

        return empty($attr->name)?'-':$attr->name;

    }

}
