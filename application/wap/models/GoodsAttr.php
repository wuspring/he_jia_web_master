<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "goods_attr".
 *
 * @property string $id
 * @property string $group_id
 * @property string $attr_name
 * @property integer $is_delete
 * @property integer $is_default
 */
class GoodsAttr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_attr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'is_delete', 'is_default'], 'integer'],
            [['attr_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'group_id' => Yii::t('app', 'Group ID'),
            'attr_name' => Yii::t('app', '属性名称'),
            'is_delete' => Yii::t('app', '是否删除'),
            'is_default' => Yii::t('app', '是否默认属性'),
        ];
    }
}
