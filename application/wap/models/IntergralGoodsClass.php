<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "intergral_goods_class".
 *
 * @property string $id
 * @property string $name
 * @property string $fid
 * @property string $icoImg
 * @property integer $sort
 * @property string $createTime
 * @property string $modifyTime
 * @property integer $is_del
 * @property integer $is_filter
 * @property string $keywords
 */
class IntergralGoodsClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_goods_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fid', 'sort', 'is_del', 'is_filter'], 'integer'],
            [['createTime', 'modifyTime'], 'safe'],
            [['keywords'], 'string'],
            [['name', 'icoImg'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '名称'),
            'fid' => Yii::t('app', '父级'),
            'icoImg' => Yii::t('app', '图标'),
            'sort' => Yii::t('app', '排序'),
            'createTime' => Yii::t('app', 'Create Time'),
            'modifyTime' => Yii::t('app', 'Modify Time'),
            'is_del' => Yii::t('app', '1是删除'),
            'is_filter' => Yii::t('app', 'Is Filter'),
            'keywords' => Yii::t('app', 'Keywords'),
        ];
    }
}
