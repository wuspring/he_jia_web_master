<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "second_kill_log".
 *
 * @property string $id
 * @property string $sk_date
 * @property integer $good_id
 * @property integer $sk_num
 * @property string $key
 * @property integer $amount
 */
class SecondKillLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'second_kill_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sk_date'], 'safe'],
            [['good_id', 'sk_num', 'amount'], 'integer'],
            [['key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sk_date' => Yii::t('app', '秒杀日期'),
            'good_id' => Yii::t('app', '商品ID'),
            'sk_num' => Yii::t('app', '秒杀时段'),
            'key' => Yii::t('app', '秒杀时段秘钥'),
            'amount' => Yii::t('app', 'Amount'),
        ];
    }
}
