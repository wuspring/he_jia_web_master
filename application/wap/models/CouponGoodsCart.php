<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "coupon_goods_cart".
 *
 * @property string $id
 * @property integer $coupon_id
 * @property string $mcoupon_id
 * @property integer $good_id
 * @property string $good_price
 */
class CouponGoodsCart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupon_goods_cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coupon_id', 'mcoupon_id', 'good_id'], 'integer'],
            [['good_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'coupon_id' => Yii::t('app', '店铺优惠券'),
            'mcoupon_id' => Yii::t('app', 'member_store_coupon id'),
            'good_id' => Yii::t('app', 'Good ID'),
            'good_price' => Yii::t('app', 'Good Price'),
        ];
    }
}
