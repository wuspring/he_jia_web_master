<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "goods_coupon".
 *
 * @property string $id
 * @property integer $goods_id
 * @property integer $user_id
 * @property string $money
 * @property integer $valid_day
 * @property integer $num
 * @property string $describe
 * @property string $condition
 * @property integer $is_show
 * @property integer $is_del
 * @property string $create_time
 * @property string $type
 * @property string $ticket_id
 */
class GoodsCoupon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_coupon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['goods_id', 'user_id', 'valid_day', 'num', 'is_show', 'is_del', 'ticket_id'], 'integer'],
            [['money', 'condition'], 'number'],
            [['describe'], 'string'],
            [['create_time'], 'safe'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'goods_id' => Yii::t('app', 'Goods ID'),
            'user_id' => Yii::t('app', '商铺id'),
            'money' => Yii::t('app', '金额'),
            'valid_day' => Yii::t('app', '优惠劵的有效天数'),
            'num' => Yii::t('app', '数量'),
            'describe' => Yii::t('app', '描述'),
            'condition' => Yii::t('app', '满减条件'),
            'is_show' => Yii::t('app', 'Is Show'),
            'is_del' => Yii::t('app', '软删除'),
            'create_time' => Yii::t('app', '添加时间'),
            'type' => Yii::t('app', '票据类型 ：普通票，展会票'),
            'ticket_id' => Yii::t('app', '展会票ID'),
        ];
    }

    public  function getUser(){
        $user=User::findOne($this->user_id);

        return $user?:(new User());
    }

    public function getTicket()
    {
        $result = false;
        if ($this->ticket_id) {
            $ticket = Ticket::findOne($this->ticket_id);
            $ticket and $result = $ticket;
        }

        return $result;
    }
}
