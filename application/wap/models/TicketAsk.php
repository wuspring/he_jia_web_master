<?php

namespace wap\models;

use Yii;

/**
 * This is the model class for table "ticket_ask".
 *
 * @property string $id
 * @property string $name
 * @property integer $provinces_id
 * @property string $ticket_id
 * @property string $ticket_amount
 * @property string $mobile
 * @property string $channel
 * @property string $address
 * @property string $resource
 * @property string $shipping_code
 * @property integer $status
 * @property string $create_time
 * @property string $member_id
 */
class TicketAsk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_ask';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provinces_id', 'ticket_id', 'ticket_amount', 'status', 'member_id'], 'integer'],
            [['address'], 'string'],
            [['create_time'], 'safe'],
            [['name', 'mobile', 'channel', 'resource', 'shipping_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '联系人姓名'),
            'provinces_id' => Yii::t('app', '城市信息'),
            'ticket_id' => Yii::t('app', '索票ID'),
            'ticket_amount' => Yii::t('app', '索票数量'),
            'mobile' => Yii::t('app', '手机号码'),
            'channel' => Yii::t('app', '推广渠道'),
            'address' => Yii::t('app', '索票收货地址'),
            'resource' => Yii::t('app', '获取来源渠道'),
            'shipping_code' => Yii::t('app', '物流单号'),
            'status' => Yii::t('app', '状态'),
            'create_time' => Yii::t('app', 'Create Time'),
            'member_id' => Yii::t('app', 'Member ID'),
        ];
    }


    /**
     *  获取  索票票据表 信息
     */
    public  function  getTicket(){
        $ticket = Ticket::findOne($this->ticket_id);
        return $ticket ?: (new Ticket());
    }

}
