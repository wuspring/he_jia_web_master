<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/10
 * Time: 17:17
 */

namespace wap\logic;


use wap\models\Goods;
use wap\models\Order;
use wap\models\OrderGoods;
use wap\models\Ticket;
use wap\models\TicketApplyGoods;
use wap\models\User;
use yii;

class MemberOrderLoginc
{

    public  static  function  init(){

        return new self();
    }

    /**
     * Notes: 查询预定订单   订单的不同状态的数量
     * User: qin
     * Date: 2019/6/10
     * Time: 17:18
     */
    public  function  getOrderState(){
        $data=Order::find()->andFilterWhere([
            'buyer_id'=>Yii::$app->user->id,
            'type'=>Order::TYPE_ZHAN_HUI,
            'ticket_type' => TicketApplyGoods::TYPE_ORDER
        ])->all();
         $allCount =0;
         $daiZhiFu = 0;
         $daiShiYong = 0;
         $daiPingJia = 0;

        foreach ($data as $k=>$v){
            $allCount+=1;
            if ($v->order_state == Order::STATUS_WAIT){
                $daiZhiFu+=1;
            }elseif ($v->order_state == Order::STATUS_PAY){
                $daiShiYong+=1;
            }elseif ($v->order_state == Order::STATUS_RECEIVED){
                $daiPingJia+=1;
            }
        }
        return ['allCount'=>$allCount,'daiZhiFu'=>$daiZhiFu,'daiShiYong'=>$daiShiYong,'daiPingJia'=>$daiPingJia];
    }

    /**
     * Notes: 获取商品信息
     * User: qin
     * Date: 2019/6/10
     * Time: 17:56
     */
    public  function getGoodInfor($orderid){

        $good =OrderGoods::find()->andWhere(['order_id'=>$orderid])->one();

        return $good;
    }

    /**
     * Notes: 获取商品详细信息
     * User: qin
     * Date: 2019/6/10
     * Time: 18:53
     */
    public function getGoodDetail($goodId){

        $good =Goods::findOne($goodId);

        return $good;
    }
    /**
     * Notes: 获取店铺信息
     * User: qin
     * Date: 2019/6/10
     * Time: 17:57
     */
    public function getStoreInfor($storeId){

        $store = User::findOne($storeId);
        return $store;
    }

    /**
     * Notes: 获取 预定商品的 定金
     * User: qin
     * Date: 2019/6/10
     * Time: 18:36
     */
    public  function getDingJin($ticketId){

        $ticket = Ticket::findOne($ticketId);

        return $ticket;
    }
}