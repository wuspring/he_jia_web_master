<?php
namespace wap\logic;
use  Yii;
/**
 * Class AdvertisingLogic
 * @author wufeng
 * @date 2019-6-5
 * 手机端广告逻辑类
 */
class AdvertisingLogic extends \wap\models\Advertising
{
    /**
     * 获取指定城市下、固定广告位的广告列表
     * @param $adid  广告位ID
     * @param string $cityId  城市ID
     * @return array
     */
    static public function adList($adid,$cityId=''){
        $list = [];
        $query = \wap\models\Advertising::find()->where(['isShow'=>1,'adid'=>$adid]);
        if(!empty($cityId)){
            $query->andFilterWhere(['province_id'=>$cityId]);
        }
        $adList = $query->orderBy('sort desc')->asArray()->all();
        if(!empty($adList)){
            foreach ($adList as $key=>$item){
                $item['picture'] = strpos($item['picture'],'http')===false?(Yii::$app->request->hostInfo.$item['picture']):$item['picture'];
                $list[$key] = $item;
            }
        }
        return $list;
    }
}