<?php
namespace wap\logic;
use wap\models\News;
use wap\models\Newsclassify;

/**
 * Class NewsLogic
 * @package wap\logic
 * @author wufeng
 * @date 2019-6-10
 *
 * 资讯表逻辑类
 */
class NewsLogic
{
    /**
     * 获取资讯列表
     * @param array $where 查询条件
     * @param array $options 翻页参数设置
     * @return array
     */
    static public function newsList($where=[],$options=['size'=>10,'page'=>1]){
        $list = [];
        $page = isset($options['page'])?intval($options['page']):1;
        $size = isset($options['size'])?intval($options['size']):10;
        $query= News::find()->where($where)->orderBy(['createTime'=>SORT_DESC]);
        $newsList=$query->offset(($page-1)* $size)->limit($size)->all();
        if(!empty($newsList)){
            foreach ($newsList as $key=>$item){
                $newsClass = Newsclassify::findOne($item->cid);
                $arr = [];
                $arr['id'] = $item->id;
                $arr['title'] = $item->title;
                $arr['cidName'] = !empty($newsClass)?$newsClass->name:'';
                $arr['themeImg'] = $item->themeImg;
                $arr['clickRate'] = $item->clickRate;
                $arr['createTime'] = $item->createTime;
                $arr['createDate'] = date('Y-m-d',strtotime($item->createTime));
                $list[$key] = $arr;
            }
        }
        return $list;
    }

    /**
     * 增加点击量
     * @param $id  资讯ID
     * @param $num 点击量
     */
    static public function clickInt($id,$num=1){
        $model = News::findOne($id);
        $model->clickRate += $num;
        $model->save();
    }

    /**
     * 获取资讯数量
     * @author wufeng
     * @date 2019-6-10
     * @param array $where  查询条件
     * @return int
     */
    static public function getNewsCount($where=[]){
        $query= News::find()->where($where);
        $count=$query->count();
        return $count?intval($count):0;
    }
}