<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/11
 * Time: 10:41
 */

namespace wap\logic;

use wap\models\IntergralOrder;
use Yii;

class IntegralOrderLogic
{

    public  static function  init(){

        return new  self();
    }

    /**
     * Notes: 查询当前订单 不同状态的数量
     * User: qin
     * Date: 2019/6/11
     * Time: 10:41
     */
    public  function getAppointmentOrderCount(){


        $intergralOrder = IntergralOrder::find()->andFilterWhere(['buyer_id' => Yii::$app->user->id])->all();
        $allNum = 0;
        $daiFaNum = 0;
        $daiShouNum = 0;
        $daiPingNum = 0;
        foreach ($intergralOrder as $k => $v) {
            $allNum += 1;
            switch ($v->order_state) {
                case 0:
                    break;
                case 1:
                    $daiFaNum += 1;
                    break;
                case 2:
                    $daiShouNum += 1;
                    break;
                case 3:
                    $daiPingNum += 1;
                    break;
                case 4:
                    break;
            }

        }

        return [
            'allNum' => $allNum,
            'daiFaNum' => $daiFaNum,
            'daiShouNum' => $daiShouNum,
            'daiPingNum' => $daiPingNum,
        ];
    }




}