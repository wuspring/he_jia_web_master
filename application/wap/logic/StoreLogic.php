<?php
namespace wap\logic;

use common\models\Goods;
use common\models\GoodsCoupon;
use common\models\Member,
    common\models\AccountLog;
use common\models\StoreGcScore;
use common\models\StoreInfo;
use common\models\StoreShop;
use common\models\Ticket;
use common\models\TicketApply;
use common\models\TicketApplyGoods;
use common\models\TicketApplyGoodsData;
use common\models\User;
use common\models\UserActive;
use DL\Project\CityExpand;
use DL\Project\CityTicket;
use DL\Project\Store;
use DL\service\CacheService;
use DL\service\UrlService;
use DL\vendor\ErWeiMa;
use wap\models\Collect;
use yii\helpers\ArrayHelper;

/**
 * Class StoreLogic
 * 店铺信息类
 *
 * @package wap\logic
 * @author daorli
 */
class StoreLogic
{
    private $_workDaysDic;
    protected static $usefulStoreIds = [];
    protected static $usefulStoreDatas = [];

    public function __construct()
    {
        $this->_workDaysDic = [
            '1' => '星期一',
            '2' => '星期二',
            '3' => '星期三',
            '4' => '星期四',
            '5' => '星期五',
            '6' => '星期六',
            '7' => '星期日',
        ];

        if (!self::$usefulStoreIds) {
            $usefulStoreIds = [];
            $ids = UserActive::find()->where([])->all();
             arrayGroupsAction($ids, function ($data) use (&$usefulStoreIds){
                 if ($data->provinces_id) {
                     $usefulStoreIds[$data->provinces_id][] = $data->id;
                 } else {
                     $usefulStoreIds[0][] = $data->id;
                 }
            });

            self::$usefulStoreIds = $usefulStoreIds;
        }
    }

    public static function init()
    {
        return new self();
    }

    /**
     * 获取当前启用的临时店铺、永久店铺ID
     *
     * @param int $provincesId provinces id
     *
     * @return array
     */
    public function usefulStoreIds($provincesId=0)
    {
        $ids = [];
        if ($provincesId) {
            $ids = isset(self::$usefulStoreIds[$provincesId])
                ? self::$usefulStoreIds[$provincesId] : [0];
        } else {
            arrayGroupsAction(self::$usefulStoreIds, function ($data) use (&$ids) {

                $ids = array_merge($ids, $data);
            });

            $ids = array_unique($ids);
        }
        return $ids;
    }

    /**
     * 获取店铺列表
     * @author wufeng
     * @date 2019-6-11
     * @param array $where 查询条件
     * @param array $options 页面参数
     * @param array $ex_where 额外参数
     * @return array|\yii\db\ActiveRecord[]
     */
    public function storeList($where=[],$options=['page'=>1,'size'=>10],$ex_where=[]){
        $list = [];
        $query = \wap\models\User::find()->alias('u')->leftJoin('store_info ui','u.id = ui.user_id')
            ->select('u.*,ui.tab_text,ui.store_amount,ui.judge_amount');
        if(!empty($where)){
            $query->where($where);
        }
        if(!empty($ex_where)){
            $query->andFilterWhere($ex_where);
        }
        if(isset($options['sort'])){
            $sort = $options['sort'];
        }else{
            $sort = 'createTime ASC';
        }
        $list = $query->orderBy($sort)
            ->offset(($options['page']-1)*$options['size'])
            ->limit($options['size'])->asArray()->all();
        $list = arrayGroupsAction($list,function ($item){
            $info = $item;
            $tab_text = [];
            if(!empty($info['tab_text']) && $info['tab_text']!='[]'){
                $tabls = json_decode($info['tab_text'],true);
                foreach ($tabls as $key=>$val){
                    list($name, $desc) = explode('@', $val);
                    $tab_text[$key] = [
                        'name'=> $name,
                        'desc'=> $desc
                    ];
                }
            }else{
                $cityInfo = CityExpand::init()->get($info['provinces_id'], CityExpand::TYPE_CITY_EXPAND);
                if (isset($cityInfo->tag)) {
                    $tab_text = call_user_func(function($str) {
                        $tabDatas = json_decode($str, true);
                        return $tabDatas ? arrayGroupsAction($tabDatas, function ($str) {
                            list($tab, $info) = explode('@', $str);
                            return ['name' => $tab, 'desc' => $info];
                        }) : [];
                    }, $cityInfo->tag);
                }
            }
            $info['tab_text'] = $tab_text;

            $goods = [];
            $orderGoods = (array)TicketApplyGoods::find()->where([
                'user_id' => $info['id']
            ])->orderBy('type desc, sort desc')->all();

            if ($orderGoods) {
                $goods = arrayGroupsAction($orderGoods, function ($apply){
                    $goodsInfo = $apply->attributes;
                    return $goodsInfo;
                });
            }
            $info['goods_list'] = array_values($goods);

            //展会号
            $ticketInfos = CityTicket::init($info['provinces_id'])->currentTickets();
            $ticketIds = ArrayHelper::getColumn($ticketInfos,'id');
            $ticket = \wap\models\TicketApply::find()->where([
                'and',
                ['=','user_id',$info['id']],
                ['in','ticket_id',$ticketIds],
                ['=','status',1]
            ])->one();
            $info['zw_pos'] = !empty($ticket)?$ticket->zw_pos:'';
            $info['ticketId'] = !empty($ticket)?$ticket->ticket_id:0;
            //店铺优惠券
            $coupons = $this->coupons($info['id']);
            if ($coupons) {
                $coupons = arrayGroupsAction($coupons, function ($coupon){
                    $couponInfo = $coupon->attributes;
                    return $couponInfo;
                });
            }
            $info['coupons'] = $coupons;
            // 评论数量
            $info['judge_amount'] = $info['judge_amount']?$info['judge_amount']:0;
            // 体验店数量
            $info['store_amount'] = $info['store_amount']?$info['store_amount']:0;
            return $info;
        });
        return $list;
    }

    /**
     * 获取店铺数量
     * @author wufeng
     * @date 2019-6-11
     * @param array $where 查询条件
     * @param array $ex_where 额外条件
     * @return mixed
     */
    public function storeListCount($where=[],$ex_where=[]){
        $query = \wap\models\User::find()->alias('u')->leftJoin('store_info ui','u.id = ui.user_id');
        if(!empty($where)){
            $query->where($where);
        }
        if(!empty($ex_where)){
            $query->andFilterWhere($ex_where);
        }
        return $query->count();
    }
    /**
     * 获取店铺详情 （手机）
     * @author wufeng
     * @date 2019-6-11
     * @param integer $userId 店铺ID
     * @return mixed
     */
    public function storeInfo($userId, $isNormal=true){
        $filter = [
            'id' => (int)$userId,
            'assignment' => User::ASSIGNMENT_HOU_TAI,
        ];
        $isNormal and $filter = array_merge($filter, ['status' => 1]);
        $store = User::findOne($filter);
        if (!$store) {
            throw new \Exception("未找到有效商家", 404);
        }
        $storeInfo = StoreInfo::findOne([
            'user_id' => $store->id
        ]);
        if (!$storeInfo) {
            throw new \Exception("店铺信息异常", 500);
        }
        $storeInfoData = array_merge($storeInfo->attributes, [
            'avatarTm' => $store->avatarTm,
            'avatar' => $store->avatar,
            'nickname' => $store->nickname,
            'assignment' => $store->assignment,
        ]);
        $storeInfoData['provincesInfo'] = $storeInfo->provincesInfo;
        $storeInfoData['avatarTm'] = translateAbsolutePath($storeInfoData['avatarTm']);
        $storeInfoData['tabs'] = call_user_func(function($str) {
            $tabDatas = json_decode($str, true);
            return $tabDatas ? arrayGroupsAction($tabDatas, function ($str) {
                list($tab, $info) = explode('@', $str);
                return ['tab' => $tab, 'info' => $info];
            }) : [];
        }, $storeInfoData['tab_text']);

        // 推荐优惠券
        $storeInfoData['coupons'] = $this->coupons($store->id);
        /**
         * 店铺评分
         */
        $scoreDatas = StoreGcScore::findAll(['user_id' =>$storeInfo->user_id]);
        $scores = [];
        arrayGroupsAction($scoreDatas, function ($scoreData) use (&$scores) {
            $scores[$scoreData->gc_id] = $scoreData;
        });

        $storeInfoData['StoreGcScore'] = $scores;

        /**
         * 默认店铺 StoreShop / false
         */
        $storeInfoData['defaultShop'] = $storeInfo->defaultShop;

        // 是否参加展会
        $storeInfoData['join_zh'] = $this->checkJoinZH($storeInfo->user_id);

        // 下订金 享特价
        $storeInfoData['order_goods'] = [];
        //爆款预约
        $storeInfoData['advance_goods'] = [];
        //全部商品
        $storeInfoData['shop_goods'] = [];
        $shop_goods = Goods::find()->where([
            'user_id'=>$storeInfo->user_id,
            'goods_state'=>1,
            'isdelete'=>0
        ])->all();
        $storeInfoData['shop_goods'] = !empty($shop_goods)? $shop_goods : [];

        $ticketInfos = CityTicket::init($store->provinces_id)->currentTickets();
        $ticketIds = ArrayHelper::getColumn($ticketInfos,'id');
        //展会号
        $ticket = \wap\models\TicketApply::find()->where([
            'and',
            ['=','user_id',$storeInfo->user_id],
            ['in','ticket_id',$ticketIds],
            ['=','status',1]
        ])->one();
        $storeInfoData['zw_pos'] = !empty($ticket)?$ticket->zw_pos:'';
        $storeInfoData['ticket_id'] = !empty($ticket)?$ticket->ticket_id:0;

        $orderGoods = (array)TicketApplyGoods::find()->where([
            'and',
            ['user_id' => $store->id],
            ['status' => 1],
            ['in','ticket_id',$ticketIds],
//            ['type' => TicketApplyGoods::TYPE_ORDER]
        ])->all();

        if (!empty($orderGoods)) {
            foreach ($orderGoods as $key=>$model){
                $info = $model->attributes;
                $info['goods_name'] = $model->goods->goods_name;
                $info['goods_marketprice'] = $model->goods->goods_marketprice;
                $info['unit'] = $model->goods->unit;
                $info['order_price'] = $model->ticket->order_price;

                switch ($model->type){
                    case \wap\models\TicketApplyGoods::TYPE_ORDER:
                        $storeInfoData['order_goods'][] = (object)$info;break;
                    case \wap\models\TicketApplyGoods::TYPE_COUPON:
                        $storeInfoData['advance_goods'][] = (object)$info;break;
                }
            }
        }
        // 门店列表
        $storeInfoData['shop_list'] = $this->shops($storeInfo->user_id);
        // 是否收藏
        $storeInfoData['is_collect'] = 0;  //未收藏
        $memberId = \Yii::$app->user->id;
        $collect = Collect::find()->where([
            'member_id'=>$memberId,
            'type'=> Collect::TYPE_STORE,
            'type_wap'=> Collect::TYPE_STORE,
            'shop_id'=>$storeInfo->user_id
        ])->one();
        if (!empty($collect)){
            $storeInfoData['is_collect'] = 1;
        }

        return (object)$storeInfoData;
    }

    /**
     * 获取商铺信息
     *
     * @param int  $userId   store id
     * @param bool $isNormal is normal
     *
     * @return object
     * @throws \Exception
     */
    public function info($userId, $isNormal=true)
    {
        $normalInfo = $isNormal ? 'true' : 'false';

        $filter = [
            'id' => (int)$userId,
            'assignment' => User::ASSIGNMENT_HOU_TAI,
        ];

        $isNormal and $filter = array_merge($filter, ['status' => 1]);

        $store = User::findOne($filter);
        if (!$store) {
            throw new \Exception("未找到有效商家", 404);
        }

        $storeCache = $this->storeCache($store);
        if ($storeCache) {
            if (isset($storeCache['info'][$normalInfo])) {
                return $storeCache['info'][$normalInfo];
            }
        }

        $storeInfo = StoreInfo::findOne([
            'user_id' => $store->id
        ]);
        if (!$storeInfo) {
            throw new \Exception("店铺信息异常", 500);
        }

        $storeInfoData = array_merge($storeInfo->attributes, [
            'avatarTm' => $store->avatarTm,
            'avatar' => $store->avatar,
            'nickname' => $store->nickname,
            'assignment' => $store->assignment,
        ]);
        $storeInfoData['provincesInfo'] = $storeInfo->provincesInfo;
        $storeInfoData['avatarTm'] = translateAbsolutePath($storeInfoData['avatarTm']);
        $storeInfoData['tabs'] = call_user_func(function($str) {
            $tabDatas = json_decode($str, true);
            return $tabDatas ? arrayGroupsAction($tabDatas, function ($str) {
                list($tab, $info) = explode('@', $str);

                return ['tab' => $tab, 'info' => $info];
            }) : [];
        }, $storeInfoData['tab_text']);

        $erWeiMaFile = '/public/erweima/' . md5("store_{$store->id}") . '.png';
        // is_file($erWeiMaFile) or ErWeiMa::init()->draw('http://new.52hejia.com' . UrlService::build(['dian-pu/index', 'storeId' => $store->id]), ROOT_PATH . $erWeiMaFile);
        is_file($erWeiMaFile) or ErWeiMa::init()->draw('http://new.52hejia.com' . UrlService::build(['shop/detail', 'id' => $store->id]), ROOT_PATH . $erWeiMaFile);
        $storeInfoData['erWeiMa'] = translateAbsolutePath($erWeiMaFile);
        // 推荐优惠券
        $storeInfoData['coupons'] = $this->coupons($store->id);

        /**
         * 店铺评分
         */
        $scoreDatas = StoreGcScore::findAll(['user_id' =>$storeInfo->user_id]);
        $scores = [];
        arrayGroupsAction($scoreDatas, function ($scoreData) use (&$scores) {
            $scores[$scoreData->gc_id] = $scoreData;
        });

        $storeInfoData['StoreGcScore'] = $scores;

        /**
         * 默认店铺 StoreShop / false
         */
        $storeInfoData['defaultShop'] = $storeInfo->defaultShop;

        // 是否参加展会
        $storeInfoData['join_zh'] = $this->checkJoinZH($storeInfo->user_id);

        // 下订金 享特价
        $storeInfoData['order_goods'] = [];
        //爆款预约
        $storeInfoData['advance_goods'] = [];

        $ticketInfos = CityTicket::init($store->provinces_id)->currentTickets();

        if ($ticketInfos) {
            foreach ($ticketInfos AS $type => $ticketApply) {
                $storeInfoData['order_goods'][$type] = [];
                $storeInfoData['advance_goods'][$type] = [];
                $storeInfoData['ticket_apply'][$type] = [];
                if ($ticketApply) {

                    $apply = TicketApply::find()->where([
                        'and',
                        ['=', 'user_id', $userId],
                        ['=', 'status', 1],
                        ['=', 'ticket_id', $ticketApply->id]
                    ])->one();
                    $storeInfoData['ticket_apply'][$type] = $apply;

                    $orderGoods = (array)TicketApplyGoods::findAll([
                        'user_id' => $store->id,
                        'ticket_id' => $ticketApply->id,
                        'type' => TicketApplyGoods::TYPE_ORDER
                    ]);

                    if ($orderGoods) {
                        $orderGoods = arrayGroupsAction($orderGoods, function ($apply) use ($ticketApply) {
                            $good = $apply->goods;

                            $good->ticket_price = $ticketApply->order_price;
                            $good->goods_price = $apply->good_price;
                            $good->goods_pic = $apply->good_pic;
                            $good->goods_storage = $apply->good_amount;

                            return $good;
                        });
                    }
                    $storeInfoData['order_goods'][$type] = $orderGoods;

                    $advanceGoods = (array)TicketApplyGoods::findAll([
                        'user_id' => $store->id,
                        'ticket_id' => $ticketApply->id,
                        'type' => TicketApplyGoods::TYPE_COUPON
                    ]);
                    if ($advanceGoods) {
                        $advanceGoods = arrayGroupsAction($advanceGoods, function ($apply) {
                            $good = $apply->goods;

                            $good->goods_price = $apply->good_price;
                            $good->goods_pic = $apply->good_pic;
                            $good->goods_storage = $apply->good_amount;

                            return $good;
                        });
                    }

                    $storeInfoData['advance_goods'][$type] = $advanceGoods;
                }
            }
        }

        unset($storeInfoData['id']);

        $storeCache['info'][$normalInfo] = (object)$storeInfoData;
        $this->storeCache($store, $storeCache);

        return (object)$storeInfoData;
    }

    /**
     * 门店信息
     *
     * @param int $userId store id
     *
     * @return array
     */
    public function shops($userId)
    {
        $shops = StoreShop::findAll([
            'user_id' => $userId
        ]);

        return arrayGroupsAction($shops, function ($shop) {
            return Store::init()->shopInfo($shop->id);
        });
    }

    /**
     * 门店信息
     *
     * @param int $shopId
     *
     * @return object
     */
    public function shopInfo($shopId)
    {
        $shop = StoreShop::findOne([
            'id' => $shopId
        ]);

        $data = $shop->attributes;
        $data['provincesInfo'] = $shop->provincesInfo->cname;
        $data['workDaysInfo'] = arrayGroupsAction(
            json_decode($data['work_days'], true),
            function ($day) {
                return isset($this->_workDaysDic[$day]) ? $this->_workDaysDic[$day] : '';
            });
        $data['picitures'] = explode(',', $data['picitures']);

        return (object)$data;
    }

    /**
     * 获取在售商品数量
     *
     * @param int   $userId
     * @param array $filter
     *
     * @return int
     */
    public function goodsAmount($userId, $filter=[])
    {
        $query = [
            'and',
            ['=', 'user_id', $userId],
            ['=', 'goods_state', 1],
            ['=', 'isdelete', 0],
        ];
        $filter and $query = array_merge($query, arrayGroupsAction($filter, function ($val, $field) {
            return ['=', $field, $val];
        }));

        return Goods::find()->where($query)->count();
    }

    /**
     * 获取店铺在售商品
     * @param int   $userId store id
     * @param array $filter
     * @param array $limit
     *
     * @return array
     */
    public function goods($userId,  $filter=[], $limit=[], $useFul=true)
    {
        $query = [
            'and',
            ['=', 'user_id', $userId]
        ];

        if ($useFul) {
            $filter['goods_state'] = 1;
            $filter['isdelete'] = 0;
        }

        $filter and $query = array_merge($query, arrayGroupsAction($filter, function ($val, $field) {
            switch ($field) {
                case 'ids_no' :
                    $result = ["not in", 'id', $val];
                    break;
                default :
                    $result = ['=', $field, $val];
            }
            return $result;
        }));


        $goodsModel = Goods::find()->where($query);

        if ($limit) {
            list($start, $amount) = $limit;
            $goodsModel->offset($start)->limit($amount);
        }

        return $goodsModel->all();
    }

    /**
     * 获取店铺优惠券
     *
     * @param $userId
     *
     * @return array
     */
    public function coupons($userId)
    {
        $goods = $this->goods($userId);
        $goodsIds = $goods ? arrayGroupsAction($goods, function ($good) {
            return $good->id;
        }) : [0];

        // 添加店铺通用优惠券
        $goodsIds[] = -1;

        GoodsCoupon::updateAll([
            'is_del' => 1
        ], "user_id='{$userId}' and `goods_id` not in (" . implode(',', $goodsIds) . ')' );

        return GoodsCoupon::find()->where([
            'and',
            ['=', 'user_id', $userId],
            ['in', 'goods_id', $goodsIds],
            ['=', 'is_del', 0],
            ['=', 'is_show', 1],
        ])->all();
    }

    /**
     * 店铺参加展会商品
     *
     * @param int $userId user id
     *
     * @return array
     */
    public function goodsJoinZH($userId)
    {
        $store = User::findOne([
            'id' => $userId,
            'assignment' => User::ASSIGNMENT_HOU_TAI
        ]);

        $tickets = CityTicket::init($store->provinces_id)->currentFactionTickets();

        $goods = [];
        if ($tickets) {
            $ticketIds = array_keys($tickets);
            $goods = (array)TicketApplyGoods::find()->where([
                'and',
                ['in', 'ticket_id', $ticketIds],
                ['=', 'user_id', $store->id],
            ])->all();
        }

        return $goods;
    }

    /**
     * 判断是否参加展会
     *
     * @param $userId
     * @param array $type 展会类型
     */
    public function checkJoinZH($userId, $type=[])
    {
        $store = User::findOne([
            'id' => $userId,
            'assignment' => User::ASSIGNMENT_HOU_TAI
        ]);

        $tickets = CityTicket::init($store->provinces_id)->currentTickets($type);
        $ticketIds = [];
        call_user_func(function ($ticket, $data) {
            foreach ($ticket AS $ticketInfo) {
                if ($ticketInfo) {
                    $data['index'] = array_merge($data['index'], [$ticketInfo->id]);
                }
            }
        }, $tickets, ['index' => &$ticketIds]);

        $ticketIds or $ticketIds = [0];

        $apply = TicketApply::find()->where([
            'and',
            ['=', 'user_id', $userId],
            ['=', 'status', 1],
            ['in', 'ticket_id', $ticketIds]
        ])->all();

        return (bool)count($apply);
    }

    public function joinZH($userId)
    {return false;
        $ticketInfo = [
            Ticket::TYPE_JIA_ZHUANG,
            Ticket::TYPE_JIE_HUN,
            Ticket::TYPE_YUN_YING,
        ];

        $result = [];
        $store = User::findOne([
            'id' => $userId,
            'assignment' => User::ASSIGNMENT_HOU_TAI
        ]);
        foreach ($ticketInfo AS $info) {
            $ticket = CityTicket::init($store->provinces_id)->getTicket($info, Ticket::CA_TYPE_ZHAN_HUI);
            $information = [];
            if ($ticket) {
                $ta = TicketApply::findOne([
                    'user_id' => $store->id,
                    'ticket_id' => $ticket->id,
                    'status' => 1,
                ]);

                if ($ta) {
                    $information = [];
//                    $information['good_ids'] = json_decode($information['good_ids'], true);
//                    $information['good_ids'] or $information['good_ids'] = [0];
//                    $information['goods'] = Goods::find()->where([
//                        'in', 'id', $information['good_ids']
//                    ])->all();

                }
            }

            $result[$info] = $information;
        }

        return $result;
    }

    protected function storeCache(User $store, $data=null)
    {
        $key = $this->cacheKey($store);

        if (is_null($data)) {
            return CacheService::init()->get($key);
        }

        CacheService::init()->set($key, (array)$data);
    }

    protected function cacheKey(User $store)
    {
       $ticketInfos = CityTicket::init($store->provinces_id)->currentFactionTickets();
       $ticketIds = $ticketInfos ? array_keys($ticketInfos) : [0];
       $ticketKey = implode('-', $ticketIds);

       return "store_info_{$store->id}_{$store->modifyTime}_{$ticketKey}_cache_key";
    }
}