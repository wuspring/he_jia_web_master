<?php
namespace wap\logic;

use wap\models\Address,
    wap\models\Goods,
    wap\models\Member,
    wap\models\Order,
    wap\models\OrderGoods;

use wap\models\GoodsCoupon;

use wap\models\SystemCoupon;
use wap\models\Ticket;
use wap\models\TicketApplyGoods;
use wap\models\TicketInfo;
use DL\Project\IntegralReward;

/**
 * Class OrderService
 *
 * @package DL\service
 * @author  daorli
 */
class OrderLogic
{
    public static function init()
    {
        return new self();
    }

    /**
     * 订单预下单
     *
     * @param $memberId
     * @param $deviceId     // 设备ID
     * @param $getDate      // 取货日期
     *
     * @return Order
     * @throws \Exception
     */
    public function createNewEmptyOrder($memberId, $userId, $ctype, $ticketType=Order::TICKET_TYPE_ORDER, $ticketId=0)
    {
        $order = new Order();
        $member = Member::findOne($memberId);

        if (!$member) {
            throw new \Exception("用户未找到");
        }

        $order->orderid = $order->getNewOrderNumber();
        $order->buyer_id = $member->id;
        $order->user_id = $userId;

        $order->order_state = $order::STATUS_WAIT;
        $order->order_amount = 0;
        $order->freight = 0;
        $order->evaluation_state = 0;
        $order->add_time = date('Y-m-d H:i:s');
        $order->type = $ctype;
        $order->ticket_type = $ticketType;
        $order->ticket_id = $ticketId;

        $order->pay_method = $order::PAY_MECHOD_WECHAT;
        $order->save();

        return $order;
    }

    /**
     * add goods to order
     *
     * @param string $orderId      order id
     * @param int    $goodsId      goods id
     * @param int    $amount       goods amount
     * @param int    $skuId        sku id
     * @param int    $pricePercent price percent 价格打折率
     *
     * @return Order
     * @throws \Exception
     */
    public function addGoodsToOrder($orderId, $goodsId, $couponId=0)
    {
        $order = Order::findOne([
            'orderid' => $orderId
        ]);
        if (!$order) {
            throw new \Exception("未找到有效订单");
        }

        $goods = Goods::findOne($goodsId);
        if (!$goods) {
            throw new \Exception("未找到商品信息");
        }


        $integral = $goods->integral; // 积分
        $pic = $goods->goods_pic; // 商品主图

        $orderGoods = new OrderGoods();
        $orderGoods->order_id = $order->orderid;
        $orderGoods->goods_id = $goods->id;
        $orderGoods->goods_pic = $pic;
        $orderGoods->goods_name = $goods->goods_name;
        $orderGoods->goods_price = $goods->goods_price;
        $orderGoods->sell_price = $goods->goods_marketprice;
        $orderGoods->goods_integral = $integral;
        $orderGoods->fallinto_state = '';               // 分成状态
        $orderGoods->fallInto = 0;          // 分成金额
        $orderGoods->sku_id = 0;
        $orderGoods->goods_num = 1;
        $orderGoods->attr = '';

        $orderGoods->buyer_id = $order->buyer_id;
        $orderGoods->createTime = date('Y-m-d');
        $orderGoods->modifyTime = date('Y-m-d');

        $orderGoods->goods_pay_price = $orderGoods->goods_price * $orderGoods->goods_num;

        $orderGoods->save();

        $goodsPrice = OrderGoods::find()->where([
            'order_id' => $order->orderid,
        ])->sum('sell_price * goods_num');

        $goodsIntegral = OrderGoods::find()->where([
            'order_id' => $order->orderid,
        ])->sum('goods_integral');

        $trueSellPrice = OrderGoods::find()->where([
            'order_id' => $order->orderid,
        ])->sum('goods_pay_price');

        $orderPayAmount = 0;

        if ($couponId) {
            $goodsCoupon = GoodsCoupon::findOne($couponId);
            $order->coupon_id = $goodsCoupon->id;
            $trueSellPrice = $trueSellPrice - $goodsCoupon->money;
            $trueSellPrice >0 or $trueSellPrice=0;
        }

        $order->pay_amount = $trueSellPrice;
        $order->goods_amount = $goodsPrice;
        $order->order_amount = $orderPayAmount;
        $order->integral_amount = IntegralReward::buyGoods($trueSellPrice);        // 增长积分
        $order->buyer_name = $order->member->wxnick;
        $order->receiver_mobile = $order->member->mobile;

        if (!$order->save()) {
            throw new \Exception('小二不小心把订单弄丢了~');
        };

        return $order;
    }

    /**
     * 添加预定商品
     *
     * @param $orderId
     * @param $goodsId
     * @param $amount
     * @param $payPrice
     * @param string $attrDetailInfo
     * @return Order|null
     * @throws \Exception\
     */
    public function addOrderGoodToOrder($orderId, $goodsId, $amount=1, $ticketId=0, $attrDetailInfo='')
    {
        $order = Order::findOne([
            'orderid' => $orderId
        ]);
        if (!$order) {
            throw new \Exception("未找到有效订单");
        }

        $ticketInfo = TicketInfo::findOne([
            'ticket_id' => $ticketId
        ]);
        if (!$ticketInfo) {
            throw new \Exception("未找到该票据信息");
        }

        $ticketGoodInfo = TicketApplyGoods::findOne([
            'ticket_id' => $ticketInfo->ticket_id,
            'good_id' => $goodsId,
            'type' => $order->ticket_type
        ]);
        if (!$ticketGoodInfo) {
            throw new \Exception("该商品未参加活动");
        }

        if ($ticketGoodInfo->good_amount < $amount) {
            throw new \Exception("库存不足");
        }

        $goods = Goods::findOne($ticketGoodInfo->good_id);
        if (!$goods) {
            throw new \Exception("未找到商品信息");
        }

        $integral = $goods->integral; // 积分
        $pic = $goods->goods_pic; // 商品主图

        $orderGoods = new OrderGoods();
        $orderGoods->order_id = $order->orderid;
        $orderGoods->goods_id = $ticketGoodInfo->good_id;
        $orderGoods->goods_pic = $pic;
        $orderGoods->goods_name = $goods->goods_name;
        $orderGoods->goods_price = $ticketGoodInfo->good_price;
        $orderGoods->sell_price = $goods->goods_marketprice;
        $orderGoods->goods_integral = $integral;
        $orderGoods->fallinto_state = '';               // 分成状态
        $orderGoods->fallInto = 0;          // 分成金额
        $orderGoods->sku_id = 0;
        $orderGoods->goods_num = $amount;
        $orderGoods->attr = $attrDetailInfo;

        $orderGoods->buyer_id = $order->buyer_id;
        $orderGoods->createTime = date('Y-m-d');
        $orderGoods->modifyTime = date('Y-m-d');

        $orderGoods->goods_pay_price = $orderGoods->goods_price * $orderGoods->goods_num;

        switch ($ticketGoodInfo->type) {
            case TicketApplyGoods::TYPE_ORDER :

                $coupon = new SystemCoupon();
                $coupon->store_id = $goods->user_id;
                $coupon->member_id = $order->buyer_id;
                $coupon->ticket_id = $ticketInfo->ticket_id;
                $coupon->provinces_id = $ticketInfo->ticket->citys;
                $coupon->money = $ticketInfo->ticket->order_price;
                $coupon->status = 0;

                if ($coupon->create()) {
                    $order->ticket_id =$ticketInfo->ticket_id;
                    $order->relation_coupon = $coupon->id;
                }
                break;
            case TicketApplyGoods::TYPE_COUPON :
        }
        $orderGoods->save();

        $goodsPrice = OrderGoods::find()->where([
            'order_id' => $order->orderid,
        ])->sum('sell_price * goods_num');

        $goodsIntegral = OrderGoods::find()->where([
            'order_id' => $order->orderid,
        ])->sum('goods_integral');

        $trueSellPrice = OrderGoods::find()->where([
            'order_id' => $order->orderid,
        ])->sum('goods_pay_price');

        switch ($ticketGoodInfo->type) {
            case TicketApplyGoods::TYPE_COUPON :
                $orderPayAmount = 0;
                break;
            case TicketApplyGoods::TYPE_ORDER :
                $orderPayAmount = $ticketInfo->ticket->order_price * $orderGoods->goods_num;
                break;
        }

        $order->pay_amount = $trueSellPrice;
        $order->goods_amount = $goodsPrice;
        $order->order_amount = $orderPayAmount;
        $order->integral_amount = IntegralReward::buyGoods($trueSellPrice);        // 增长积分
        $order->buyer_name = $order->member->wxnick;
        $order->receiver_mobile = $order->member->mobile;

        $ticketGoodInfo->good_amount = $ticketGoodInfo->good_amount - $orderGoods->goods_num;
        $ticketGoodInfo->save();

        if (!$order->save()) {
            throw new \Exception('小二不小心把订单弄丢了~');
        };

        return $order;
    }

    /**
     * set address
     *
     * @param $orderId
     * @param $addressId
     *
     * @return Order
     * @throws \Exception
     */
    public function setAddress($orderId, $addressId)
    {
        $order = Order::findOne([
            "orderid" => $orderId,
            "order_state" => Order::STATUS_WAIT
        ]);
        if (!$order) {
            throw new \Exception("未找到有效订单信息");
        }

        $address = Address::findOne([
            'id' => $addressId,
        ]);
        if (!$address) {
            throw new \Exception("未找到地址信息");
        }
        $order->buyer_name = $order->member->wxnick;
        $order->receiver = $address->id;
        $order->receiver_name = $address->name;
        $order->receiver_address = "{$address->provinceInfo} {$address->address}";
        $order->receiver_mobile = $address->mobile;
        if (!$order->save()) {
            throw new \Exception("保存订单地址失败");
        }

        return $order;
    }

    /**
     * 检测为未支付订单过期订单为 失效订单
     * @author wufeng
     * @date 2019-12-12
     */
    public function checkUnpayOrder(){
        $now = date('Y-m-d H:i:s');
        $orderIds = Order::find()->alias('o')->where(['o.order_state'=>Order::STATUS_WAIT])
            ->leftJoin(['t'=>Ticket::tableName()],'t.id=o.ticket_id')
            ->andWhere(['<=','t.end_date',$now])->select(['o.orderid'])->column();
        if(!empty($orderIds)){
            Order::updateAll(['order_state'=>Order::STATUS_CLOSE],['in','orderid',$orderIds]);
        }
    }
}