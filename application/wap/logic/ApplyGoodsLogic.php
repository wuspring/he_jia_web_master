<?php
namespace wap\logic;
use DL\Project\Store;
use wap\models\Goods;
use wap\models\Ticket;
use wap\models\TicketApplyGoods;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ApplyGoodsLogic
 * @package wap\logic
 * 展会商品逻辑类
 * @author  wufeng
 * @date 2019-6-5
 */
class ApplyGoodsLogic
{
    /**
     * 获取展会首页推荐 预存享特价
     * @param array $cityId 城市ID
     * @param $opts 页面参数
     * @return array
     */
    static public function getApplyGoodsList($cityId,$opts=['size'=>4,'page'=>1],$ex_where=[]){
        $list = [];
        //获取城市下所有开展的展会ID
        $ticketIds = self::getCityExpIds($cityId);
        $usefulStoreIds = Store::init()->usefulStoreIds();
        $query = TicketApplyGoods::find()->where([
            'and',
            ['in', 'ticket_apply_goods.ticket_id', $ticketIds],
            ['=', 'ticket_apply_goods.type', TicketApplyGoods::TYPE_ORDER],
            ['in', 'ticket_apply_goods.user_id', $usefulStoreIds],
            ['=', 'ticket_apply_goods.is_index', 1]
        ])->leftJoin('goods as g', 'g.id = ticket_apply_goods.good_id')
            ->orderBy('g.admin_score desc')->select('ticket_apply_goods.*');

        if(!empty($ex_where)){
            $query->andFilterWhere($ex_where);
        }

        $dingJinsGoods = $query->offset(($opts['page']-1)*$opts['size'])->limit($opts['size'])->all();

        $list = arrayGroupsAction($dingJinsGoods, function ($gid) {
            $good = Goods::findOne($gid->good_id);
            $good or $good = (new Goods());
            $info = $good->attributes;
            $info['goods_price'] = $gid->good_price;
            $info['goods_pic'] = $gid->good_pic;
            $info['goods_storage'] = $gid->good_amount;
            $info['ticket_money'] = $gid->ticket->order_price;
            $info['ticket_id'] = $gid->ticket->id;
            $info['type'] = $gid->ticket->type;
            return (object)$info;
        });
        return $list;
    }

    /**
     * 获取爆品预约商品列表
     * @author  wufeng
     * @date 2019-6-6
     * @param $cityId 城市ID
     * @param $opts 页面参数
     * @return array
     */
    static public function getSubscribeGoodsList($cityId,$opts=['size'=>4,'page'=>1],$ex_where=[]){
        $list = [];
        $ticketIds = self::getCityExpIds($cityId);
        $usefulStoreIds = Store::init()->usefulStoreIds();
        $query = TicketApplyGoods::find()->where([
            'and',
            ['g.goods_state'=>1,'g.isdelete'=>0],
            ['in', 'ticket_apply_goods.ticket_id', $ticketIds],
            ['=', 'ticket_apply_goods.type', TicketApplyGoods::TYPE_COUPON],
            ['in', 'ticket_apply_goods.user_id', $usefulStoreIds]
        ])->leftJoin('goods as g', 'g.id = ticket_apply_goods.good_id')->select('ticket_apply_goods.*');

        if(isset($opts['is_index'])){
            $query->andFilterWhere(['ticket_apply_goods.is_index'=>$opts['is_index']]);
        }

        if(!empty($ex_where)){
            $query->andFilterWhere($ex_where);
        }

        $reMaisData = $query->orderBy('g.admin_score desc')->offset(($opts['page']-1)*$opts['size'])->limit($opts['size'])->all();

        $list = arrayGroupsAction($reMaisData, function ($gid) {
            $good = Goods::findOne($gid->good_id);
            $good or $good = (new Goods());
            $info = $good->attributes;
            $info['goods_price'] = $gid->good_price;
            $info['goods_pic'] = $gid->good_pic;
            $info['goods_storage'] = $gid->good_amount;

            $info['store'] = $good->user;
            $info['type'] = $gid->ticket->type;
            $info['ticket_id'] = $gid->ticket->id;
            return (object)$info;
        });
        return $list;
    }

    /**
     * 获取爆品预约商品数量
     * @author  wufeng
     * @date 2019-6-6
     * @param $cityId  城市ID
     * @param array $opts 参数设置 is_index 是否首页显示
     * @return int|string
     */
    static public function getSubscribeGoodsCount($cityId,$opts=[],$ex_where=[]){
        $ticketIds = self::getCityExpIds($cityId);
        $usefulStoreIds = Store::init()->usefulStoreIds();
        $query = TicketApplyGoods::find()->where([
            'and',
            ['in', 'ticket_apply_goods.ticket_id', $ticketIds],
            ['=', 'ticket_apply_goods.type', TicketApplyGoods::TYPE_COUPON],
            ['in', 'ticket_apply_goods.user_id', $usefulStoreIds]
        ])->leftJoin('goods as g', 'g.id = ticket_apply_goods.good_id');
        if(isset($opts['is_index'])){
            $query->andFilterWhere(['ticket_apply_goods.is_index'=>$opts['is_index']]);
        }
        if(!empty($ex_where)){
            $query->andFilterWhere($ex_where);
        }
        return $query->count();
    }

    /**
     * 获取城市下所有开展的展会ID
     * @param $cityId  城市ID
     * @return array
     */
    static public function getCityExpIds($cityId){
        $exops = Ticket::find()->where(['citys'=>$cityId,'status'=>1])->all();
        return ArrayHelper::getColumn($exops,'id');
    }
}