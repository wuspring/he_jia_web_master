<?php
namespace wap\logic;
use wap\models\Collect;
use wap\models\Comment;
use wap\models\Goods;
use wap\models\StoreShop;
use  Yii;
use yii\helpers\ArrayHelper;


/**
 * 商品收藏逻辑
 * @package DL\service
 */
class CollectLogic
{

    public  static  function  init(){

        return new self();
    }

    /**
     * Notes: 获取用户的收藏数量
     * User: qin
     * Date: 2019/6/10
     * Time: 10:51
     */
   public function getCollectCount(){

       $collect = Collect::findAll(['member_id'=>Yii::$app->user->id]);
       $store_count =0;
       $reserve_count =0;
       $subscribe_count =0;

       foreach ($collect as $k=>$v){

           switch ($v->type_wap){
               case Collect::TYPE_STORE:
                   $store_count+=1;
                   break;
               case Collect::TYPE_SUBSCRIBE_GOOD:
                   $subscribe_count+=1;
                   break;
               case Collect::TYPE_RESERVE_GOOD:
                   $reserve_count+=1;
                   break;
               default:
         }
       }

       return array('store'=>$store_count,'reserve'=>$reserve_count,'subscribe'=>$subscribe_count);
   }

    /**
     * Notes:店铺的评论数量
     * param  $storeId 店铺id
     * User: qin
     * Date: 2019/6/10
     * Time: 11:49
     */
   public  function getAllEvaluate($storeId=0){
        $goods = Goods::findAll([
            'user_id' => $storeId
        ]);
       $goodIdArr=ArrayHelper::getColumn($goods,'id');
       $goodIdArr or $goodIdArr = [0];
       $shopComment=Comment::find()->andFilterWhere(['in','goods_id',$goodIdArr])->all();
       return  $shopComment;
   }

    /**
     * Notes: 店铺的体验店数量
     * param   $storeId 店铺id
     * User: qin
     * Date: 2019/6/10
     * Time: 11:50
     */
    public  function getAllAttempt($storeId=0){
        $shops = StoreShop::findAll([
            'user_id' => $storeId
        ]);
        return $shops;
    }
}