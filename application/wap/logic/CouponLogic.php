<?php
namespace wap\logic;
use wap\models\Collect;
use wap\models\Comment;
use wap\models\Goods;
use wap\models\MemberStoreCoupon;
use wap\models\StoreShop;
use wap\models\User;
use  Yii;
use yii\helpers\ArrayHelper;


/**
 *  优惠劵逻辑
 * @package DL\service
 */
class CouponLogic
{

    public  static  function  init(){

        return new self();
    }

    /**
     * Notes: 更改当前用户的  is_expire
     * User: qin
     * Date: 2019/6/10
     * Time: 15:23
     */
    public function updateExpire(){

        $data=MemberStoreCoupon::find()->andFilterWhere(['member_id' => Yii::$app->user->id])->all();
         $now = date('Y-m-d');
        foreach ($data as $k=>$v){
            if ($v->end_time < $now){
                MemberStoreCoupon::updateAll(['is_expire'=>1],['id'=>$v->id]);
            }
        }
    }
    /**
     * Notes: 获取优惠劵状态
     * User: qin
     * Date: 2019/6/10
     * Time: 14:58
     * return  1 待使用 2 待评价 3 已使用 4 已过期
     */
    public  function getCouponState($couponId){

        $memberStoreCoupon = MemberStoreCoupon::findOne($couponId);
        $now = date('Y-y-d H:i:s');
        if ($memberStoreCoupon->is_use == 1){
//                if ($memberStoreCoupon->is_evaluate == 0){
//                    return 2;  //带评价
//                }else{
//
//                }
            return 3;  //已使用

        }else{

            if ($memberStoreCoupon->is_expire == 1){
                return 4; //已过期
            }else{
                return 1; //待使用
            }
        }

    }

    /**
     * Notes:查询店铺名称
     * User: qin
     * Date: 2019/6/10
     * Time: 15:53
     */
    public function  getStoreInfo($storeId){

        $store = User::findOne($storeId);
        return $store;
    }

    /**
     * Notes: 查询商品信息
     * User: qin
     * Date: 2019/6/10
     * Time: 15:54
     */
    public  function getGoodInfo($goodId){

        $good = Goods::findOne($goodId);
        return empty($good)?'':$good;
    }

    /**
     * Notes: 获取当前优惠劵的各种状态数量
     * User: qin
     * Date: 2019/6/10
     * Time: 16:21
     */
    public  function  getCouponCount(){
        $data=MemberStoreCoupon::find()->andFilterWhere(['member_id' => Yii::$app->user->id])->all();
        $allCount=0;
        $daiShiYong =0;
        $daiPingJia = 0;
        $yiShiYong = 0;
        $yiGuoqi = 0;
        foreach ($data as $key=>$val){
            $allCount+=1;
           $couponState = $this->getCouponState($val->id);
           if ($couponState==1){
               $daiShiYong+=1;
           }else if ($couponState==2){
               $daiPingJia+=1;
           }else if ($couponState==3){
               $yiShiYong+=1;
           }else if ($couponState==4){
               $yiGuoqi+=1;
           }
        }

        return ['allCount'=>$allCount,'daiShiYong'=>$daiShiYong,'daiPingJia'=>$daiPingJia,'yiShiYong'=>$yiShiYong,'yiGuoqi'=>$yiGuoqi];

    }
}