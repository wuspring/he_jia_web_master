<?php
namespace wap\logic;
use common\models\MemberForm;
use common\wxpay\JsApiPay;
use wap\models\Member;

/**
 * Class ThirdResourceLoginLogic
 * 第三方登陆
 * @package wap\logic
 * @author daorli
 */
class ThirdResourceLoginLogic
{
    /**
     * @author daorli
     * @date 2019-6-24
     *
     * 微信快捷登陆
     */
    public static function wechatLogin()
    {
        try {
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') == false) {
                stopFlow("您的浏览器不支持微信登陆");
            }

            $jsApi = new JsApiPay();
            $openId = $jsApi->GetOpenid();

            $member = Member::findOne([
                'wxopenid' => $openId
            ]);
            if (!$member) {
                $member->username = $member->getNewUserName();
                $member->mobile = '';
                $member->wxnick = '用户_' . createRandKey();
                $member->avatar = "";
                $member->role = 0;
                $member->sex = 1;
                $member->nickname = $member->wxnick;
                $member->createTime = date('Y-m-d H:i:s');
                $member->status = 1;
                $member->modifyTime = $member->createTime;
                $member->wxopenid = $member->$openId;
                $member->password = \Yii::$app->security->generatePasswordHash('123');
                if (!$member->save()){
                    throw new \Exception("创建新用户失败");
                }
            }

            $login = new MemberForm();
            $login->autologin($member->id);

            return $member;
        } catch (\Exception $e) {
            return false;
        }

    }


}