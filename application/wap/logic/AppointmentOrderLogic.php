<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/11
 * Time: 10:41
 */

namespace wap\logic;


use wap\models\Goods;
use wap\models\Order;
use wap\models\OrderGoods;
use wap\models\TicketApplyGoods;
use wap\models\User;
use Yii;

class AppointmentOrderLogic
{

    public  static function  init(){

        return new  self();
    }

    /**
     * Notes: 查询当前订单 不同状态的数量
     * User: qin
     * Date: 2019/6/11
     * Time: 10:41
     */
    public  function getAppointmentOrderCount(){

        $allCount = 0;
        $daiShiYong = 0;
        $daiPingJia = 0;
        $data=Order::find()->andFilterWhere([
            'buyer_id'=>Yii::$app->user->id,
            'type'=>Order::TYPE_ZHAN_HUI,
            'ticket_type' => TicketApplyGoods::TYPE_COUPON
        ])->all();

        foreach ($data as $k=>$v){
            $allCount+=1;
            if($v->order_state == Order::STATUS_PAY || $v->order_state == Order::STATUS_WAIT){
                $daiShiYong+=1;
            }elseif ($v->order_state == Order::STATUS_RECEIVED){
                $daiPingJia+=1;
            }
        }
        return ['allCount'=>$allCount,'daiShiYong'=>$daiShiYong,'daiPingJia'=>$daiPingJia];
    }
    /**
     * Notes:查询店铺名称
     * User: qin
     * Date: 2019/6/10
     * Time: 15:53
     */
    public function  getStoreInfo($storeId){

        $store = User::findOne($storeId);
        return $store;
    }
    /**
     * Notes: 订单商品的信息
     * param  orderGoods 中的order_id
     * User: qin
     * Date: 2019/6/10
     * Time: 15:54
     */
    public  function getGoodOrderInfo($orderId){

        $goodOrderInfo = OrderGoods::findOne(['order_id'=>$orderId]);
        return empty($goodOrderInfo)?'':$goodOrderInfo;
    }
    /**
     * Notes: 查询商品信息
     * User: qin
     * Date: 2019/6/10
     * Time: 15:54
     */
    public  function getGoodInfo($goodId){

        $good = Goods::findOne($goodId);
        return empty($good)?'':$good;
    }

}