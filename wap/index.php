<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'test');
define('PAGE_URL', '/wap/index.php');

/**
 * daorli base concat
 */
define('MODULE_NAME', 'wap');
defined('RENDER_PAGE') or define('RENDER_PAGE', true);
define('ROOT_PATH', __DIR__ . '/..');
defined('APP_BASE_PATH') or define('APP_BASE_PATH', ROOT_PATH . '/application');

defined('APP_PATH') or define('APP_PATH', APP_BASE_PATH . '/'. MODULE_NAME);

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../common/config/bootstrap.php');

ini_set('date.timezone','Asia/Shanghai');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../common/config/main.php'),
    require(__DIR__ . '/../common/config/main-local.php'),
    require(APP_PATH . '/config/main.php'),
    require(APP_PATH . '/config/main-local.php'),

    [
        'runtimePath' => RUNTIME_PATH
    ]
);

if (!isMobile()) {
	header('Location : /../index.php');
	die();
}

//isset($_GET['r']) or $_GET['r'] = 'index/index';

$application = new yii\web\Application($config);
$application->language='zh-CN';
$application->run();
