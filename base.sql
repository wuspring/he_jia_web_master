/*
 Navicat Premium Data Transfer

 Source Server         : ^localhost
 Source Server Type    : MySQL
 Source Server Version : 50714
 Source Host           : localhost:3306
 Source Schema         : base

 Target Server Type    : MySQL
 Target Server Version : 50714
 File Encoding         : 65001

 Date: 28/03/2019 13:20:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account_log
-- ----------------------------
DROP TABLE IF EXISTS `account_log`;
CREATE TABLE `account_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型 money消费记录',
  `money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '变动金额',
  `integral` int(10) NULL DEFAULT 0 COMMENT '积分',
  `remark` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '说明',
  `member_id` int(11) NULL DEFAULT NULL COMMENT '会员ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '变动时间',
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '变动记录号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '账户余额与积分记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) NULL DEFAULT 0 COMMENT '会员id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货人姓名',
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省',
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '市',
  `region` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '地区',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  `tel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '座机',
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `isDefault` int(11) NULL DEFAULT 0 COMMENT '是否默认地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for adsense
-- ----------------------------
DROP TABLE IF EXISTS `adsense`;
CREATE TABLE `adsense`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '广告位名称',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态:1有效 0无效',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `modifyTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for advertising
-- ----------------------------
DROP TABLE IF EXISTS `advertising`;
CREATE TABLE `advertising`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adid` bigint(20) NULL DEFAULT NULL COMMENT '广告位id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `picture` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `pictureTm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '缩略图',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转地址',
  `isShow` int(11) NULL DEFAULT 1 COMMENT '是否显示1显示0为不显示',
  `describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `clickRate` int(11) NULL DEFAULT 0 COMMENT '点击量',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `isTime` int(11) NULL DEFAULT 0 COMMENT '是否是时间广告',
  `startTime` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `endTime` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for auth_assignment
-- ----------------------------
DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment`  (
  `item_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`item_name`, `user_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `created_at`(`created_at`) USING BTREE,
  INDEX `item_name`(`item_name`) USING BTREE,
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员授权表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_assignment
-- ----------------------------
INSERT INTO `auth_assignment` VALUES ('系统管理员', '1', 1448545349);
INSERT INTO `auth_assignment` VALUES ('超级管理员', '1', 1553741488);

-- ----------------------------
-- Table structure for auth_item
-- ----------------------------
DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item`  (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `rule_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` int(11) NULL DEFAULT NULL,
  `updated_at` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE,
  INDEX `rule_name`(`rule_name`) USING BTREE,
  INDEX `type`(`type`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `created_at`(`created_at`) USING BTREE,
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理权权限条目' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_item
-- ----------------------------
INSERT INTO `auth_item` VALUES ('/*', 2, NULL, NULL, NULL, 1476842780, 1476842780);
INSERT INTO `auth_item` VALUES ('/account-log/*', 2, NULL, NULL, NULL, 1540110210, 1540110210);
INSERT INTO `auth_item` VALUES ('/account-log/create', 2, NULL, NULL, NULL, 1540110210, 1540110210);
INSERT INTO `auth_item` VALUES ('/account-log/delete', 2, NULL, NULL, NULL, 1540110210, 1540110210);
INSERT INTO `auth_item` VALUES ('/account-log/index', 2, NULL, NULL, NULL, 1540110210, 1540110210);
INSERT INTO `auth_item` VALUES ('/account-log/update', 2, NULL, NULL, NULL, 1540110210, 1540110210);
INSERT INTO `auth_item` VALUES ('/account-log/view', 2, NULL, NULL, NULL, 1540110210, 1540110210);
INSERT INTO `auth_item` VALUES ('/address/*', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/address/create', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/address/delete', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/address/index', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/address/update', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/address/view', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/admin/*', 2, NULL, NULL, NULL, 1476842762, 1476842762);
INSERT INTO `auth_item` VALUES ('/adsense/*', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/adsense/create', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/adsense/delete', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/adsense/index', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/adsense/update', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/adsense/view', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/advertising/*', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/advertising/create', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/advertising/delete', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/advertising/index', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/advertising/update', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/advertising/view', 2, NULL, NULL, NULL, 1540092038, 1540092038);
INSERT INTO `auth_item` VALUES ('/brand/*', 2, NULL, NULL, NULL, 1540093969, 1540093969);
INSERT INTO `auth_item` VALUES ('/brand/create', 2, NULL, NULL, NULL, 1540093969, 1540093969);
INSERT INTO `auth_item` VALUES ('/brand/delete', 2, NULL, NULL, NULL, 1540093969, 1540093969);
INSERT INTO `auth_item` VALUES ('/brand/index', 2, NULL, NULL, NULL, 1540093969, 1540093969);
INSERT INTO `auth_item` VALUES ('/brand/update', 2, NULL, NULL, NULL, 1540093969, 1540093969);
INSERT INTO `auth_item` VALUES ('/brand/view', 2, NULL, NULL, NULL, 1540093969, 1540093969);
INSERT INTO `auth_item` VALUES ('/cart/*', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/cart/create', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/cart/delete', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/cart/index', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/cart/update', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/cart/view', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/category/create', 2, NULL, NULL, NULL, 1550036475, 1550036475);
INSERT INTO `auth_item` VALUES ('/category/delete', 2, NULL, NULL, NULL, 1550036475, 1550036475);
INSERT INTO `auth_item` VALUES ('/category/index', 2, NULL, NULL, NULL, 1550036475, 1550036475);
INSERT INTO `auth_item` VALUES ('/category/update', 2, NULL, NULL, NULL, 1550036475, 1550036475);
INSERT INTO `auth_item` VALUES ('/category/view', 2, NULL, NULL, NULL, 1550036475, 1550036475);
INSERT INTO `auth_item` VALUES ('/config/*', 2, NULL, NULL, NULL, 1519828210, 1519828210);
INSERT INTO `auth_item` VALUES ('/config/create', 2, NULL, NULL, NULL, 1532499965, 1532499965);
INSERT INTO `auth_item` VALUES ('/config/delete', 2, NULL, NULL, NULL, 1532499965, 1532499965);
INSERT INTO `auth_item` VALUES ('/config/expand', 2, NULL, NULL, NULL, 1550035793, 1550035793);
INSERT INTO `auth_item` VALUES ('/config/fenxiao', 2, NULL, NULL, NULL, 1542978076, 1542978076);
INSERT INTO `auth_item` VALUES ('/config/index', 2, NULL, NULL, NULL, 1519828320, 1519828320);
INSERT INTO `auth_item` VALUES ('/config/integral', 2, NULL, NULL, NULL, 1550035932, 1550035932);
INSERT INTO `auth_item` VALUES ('/config/jingxiao', 2, NULL, NULL, NULL, 1542978067, 1542978067);
INSERT INTO `auth_item` VALUES ('/config/system', 2, NULL, NULL, NULL, 1520212460, 1520212460);
INSERT INTO `auth_item` VALUES ('/config/update', 2, NULL, NULL, NULL, 1532499965, 1532499965);
INSERT INTO `auth_item` VALUES ('/config/view', 2, NULL, NULL, NULL, 1532499965, 1532499965);
INSERT INTO `auth_item` VALUES ('/config/weixin', 2, NULL, NULL, NULL, 1540107107, 1540107107);
INSERT INTO `auth_item` VALUES ('/config/youfei', 2, NULL, NULL, NULL, 1543830750, 1543830750);
INSERT INTO `auth_item` VALUES ('/coupon-auto-send/*', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/coupon-auto-send/create', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/coupon-auto-send/delete', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/coupon-auto-send/index', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/coupon-auto-send/update', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/coupon-auto-send/view', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/coupon/*', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/coupon/create', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/coupon/delete', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/coupon/index', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/coupon/update', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/coupon/view', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/express/create', 2, NULL, NULL, NULL, 1540900520, 1540900520);
INSERT INTO `auth_item` VALUES ('/express/delete', 2, NULL, NULL, NULL, 1540900520, 1540900520);
INSERT INTO `auth_item` VALUES ('/express/index', 2, NULL, NULL, NULL, 1540900520, 1540900520);
INSERT INTO `auth_item` VALUES ('/express/list', 2, NULL, NULL, NULL, 1540900520, 1540900520);
INSERT INTO `auth_item` VALUES ('/express/update', 2, NULL, NULL, NULL, 1540900520, 1540900520);
INSERT INTO `auth_item` VALUES ('/express/view', 2, NULL, NULL, NULL, 1540900520, 1540900520);
INSERT INTO `auth_item` VALUES ('/fenxiaoshang/*', 2, NULL, NULL, NULL, 1542869587, 1542869587);
INSERT INTO `auth_item` VALUES ('/fenxiaoshang/apply', 2, NULL, NULL, NULL, 1542870504, 1542870504);
INSERT INTO `auth_item` VALUES ('/fenxiaoshang/create', 2, NULL, NULL, NULL, 1542869912, 1542869912);
INSERT INTO `auth_item` VALUES ('/fenxiaoshang/delete', 2, NULL, NULL, NULL, 1542869912, 1542869912);
INSERT INTO `auth_item` VALUES ('/fenxiaoshang/index', 2, NULL, NULL, NULL, 1542869912, 1542869912);
INSERT INTO `auth_item` VALUES ('/fenxiaoshang/update', 2, NULL, NULL, NULL, 1542869912, 1542869912);
INSERT INTO `auth_item` VALUES ('/fenxiaoshang/view', 2, NULL, NULL, NULL, 1542869912, 1542869912);
INSERT INTO `auth_item` VALUES ('/goods-attr-group/*', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-attr-group/create', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-attr-group/delete', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-attr-group/index', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-attr-group/update', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-attr-group/view', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-attr/*', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-attr/create', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/goods-attr/delete', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/goods-attr/index', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/goods-attr/update', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/goods-attr/view', 2, NULL, NULL, NULL, 1540092039, 1540092039);
INSERT INTO `auth_item` VALUES ('/goods-class/*', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-class/create', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-class/delete', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-class/index', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-class/searchlist', 2, NULL, NULL, NULL, 1540093969, 1540093969);
INSERT INTO `auth_item` VALUES ('/goods-class/update', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-class/view', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-sku/*', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-sku/create', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-sku/delete', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-sku/index', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-sku/update', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods-sku/view', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods/*', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods/create', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods/delete', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods/index', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods/update', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/goods/view', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/jingxiaoshang/*', 2, NULL, NULL, NULL, 1542870521, 1542870521);
INSERT INTO `auth_item` VALUES ('/jingxiaoshang/apply', 2, NULL, NULL, NULL, 1542870619, 1542870619);
INSERT INTO `auth_item` VALUES ('/jingxiaoshang/create', 2, NULL, NULL, NULL, 1542870685, 1542870685);
INSERT INTO `auth_item` VALUES ('/jingxiaoshang/delete', 2, NULL, NULL, NULL, 1542870626, 1542870626);
INSERT INTO `auth_item` VALUES ('/jingxiaoshang/index', 2, NULL, NULL, NULL, 1542870542, 1542870542);
INSERT INTO `auth_item` VALUES ('/jingxiaoshang/update', 2, NULL, NULL, NULL, 1542870638, 1542870638);
INSERT INTO `auth_item` VALUES ('/jingxiaoshang/view', 2, NULL, NULL, NULL, 1542870566, 1542870566);
INSERT INTO `auth_item` VALUES ('/level/*', 2, NULL, NULL, NULL, 1476842767, 1476842767);
INSERT INTO `auth_item` VALUES ('/level/create', 2, NULL, NULL, NULL, 1532499965, 1532499965);
INSERT INTO `auth_item` VALUES ('/level/delete', 2, NULL, NULL, NULL, 1532499965, 1532499965);
INSERT INTO `auth_item` VALUES ('/level/index', 2, NULL, NULL, NULL, 1532499965, 1532499965);
INSERT INTO `auth_item` VALUES ('/level/update', 2, NULL, NULL, NULL, 1532499965, 1532499965);
INSERT INTO `auth_item` VALUES ('/level/view', 2, NULL, NULL, NULL, 1532499965, 1532499965);
INSERT INTO `auth_item` VALUES ('/member-coupon/*', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/member-coupon/create', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/member-coupon/delete', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/member-coupon/index', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/member-coupon/update', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/member-coupon/view', 2, NULL, NULL, NULL, 1540092040, 1540092040);
INSERT INTO `auth_item` VALUES ('/member/*', 2, NULL, NULL, NULL, 1476842770, 1476842770);
INSERT INTO `auth_item` VALUES ('/member/create', 2, NULL, NULL, NULL, 1532499965, 1532499965);
INSERT INTO `auth_item` VALUES ('/member/delete', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/member/index', 2, NULL, NULL, NULL, 1532499965, 1532499965);
INSERT INTO `auth_item` VALUES ('/member/update', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/member/view', 2, NULL, NULL, NULL, 1532499965, 1532499965);
INSERT INTO `auth_item` VALUES ('/message/*', 2, NULL, NULL, NULL, 1476842774, 1476842774);
INSERT INTO `auth_item` VALUES ('/message/create', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/message/delete', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/message/index', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/message/update', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/message/view', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/money/*', 2, NULL, NULL, NULL, 1543311904, 1543311904);
INSERT INTO `auth_item` VALUES ('/money/analysis', 2, NULL, NULL, NULL, 1544668185, 1544668185);
INSERT INTO `auth_item` VALUES ('/money/create', 2, NULL, NULL, NULL, 1543311919, 1543311919);
INSERT INTO `auth_item` VALUES ('/money/delete', 2, NULL, NULL, NULL, 1543311919, 1543311919);
INSERT INTO `auth_item` VALUES ('/money/index', 2, NULL, NULL, NULL, 1543311891, 1543311891);
INSERT INTO `auth_item` VALUES ('/money/update', 2, NULL, NULL, NULL, 1543311919, 1543311919);
INSERT INTO `auth_item` VALUES ('/money/view', 2, NULL, NULL, NULL, 1543311919, 1543311919);
INSERT INTO `auth_item` VALUES ('/news/*', 2, NULL, NULL, NULL, 1540626636, 1540626636);
INSERT INTO `auth_item` VALUES ('/news/create', 2, NULL, NULL, NULL, 1540626636, 1540626636);
INSERT INTO `auth_item` VALUES ('/news/delete', 2, NULL, NULL, NULL, 1540626636, 1540626636);
INSERT INTO `auth_item` VALUES ('/news/index', 2, NULL, NULL, NULL, 1540626636, 1540626636);
INSERT INTO `auth_item` VALUES ('/news/isshow', 2, NULL, NULL, NULL, 1540626636, 1540626636);
INSERT INTO `auth_item` VALUES ('/news/recommend', 2, NULL, NULL, NULL, 1540626636, 1540626636);
INSERT INTO `auth_item` VALUES ('/news/update', 2, NULL, NULL, NULL, 1540626636, 1540626636);
INSERT INTO `auth_item` VALUES ('/news/view', 2, NULL, NULL, NULL, 1540626636, 1540626636);
INSERT INTO `auth_item` VALUES ('/newsclassify/*', 2, NULL, NULL, NULL, 1540626636, 1540626636);
INSERT INTO `auth_item` VALUES ('/newsclassify/create', 2, NULL, NULL, NULL, 1540626636, 1540626636);
INSERT INTO `auth_item` VALUES ('/newsclassify/delete', 2, NULL, NULL, NULL, 1540626636, 1540626636);
INSERT INTO `auth_item` VALUES ('/newsclassify/index', 2, NULL, NULL, NULL, 1540626636, 1540626636);
INSERT INTO `auth_item` VALUES ('/newsclassify/update', 2, NULL, NULL, NULL, 1540626636, 1540626636);
INSERT INTO `auth_item` VALUES ('/newsclassify/view', 2, NULL, NULL, NULL, 1540626636, 1540626636);
INSERT INTO `auth_item` VALUES ('/order-goods/*', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/order-goods/create', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/order-goods/delete', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/order-goods/index', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/order-goods/update', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/order-goods/view', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/order/*', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/order/create', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/order/delete', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/order/index', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/order/reback', 2, NULL, NULL, NULL, 1543268200, 1543268200);
INSERT INTO `auth_item` VALUES ('/order/update', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/order/view', 2, NULL, NULL, NULL, 1540092041, 1540092041);
INSERT INTO `auth_item` VALUES ('/payment/*', 2, NULL, NULL, NULL, 1540092519, 1540092519);
INSERT INTO `auth_item` VALUES ('/payment/create', 2, NULL, NULL, NULL, 1540092519, 1540092519);
INSERT INTO `auth_item` VALUES ('/payment/delete', 2, NULL, NULL, NULL, 1540092519, 1540092519);
INSERT INTO `auth_item` VALUES ('/payment/index', 2, NULL, NULL, NULL, 1540092518, 1540092518);
INSERT INTO `auth_item` VALUES ('/payment/update', 2, NULL, NULL, NULL, 1540092519, 1540092519);
INSERT INTO `auth_item` VALUES ('/payment/view', 2, NULL, NULL, NULL, 1540092518, 1540092518);
INSERT INTO `auth_item` VALUES ('/postmethod/create', 2, NULL, NULL, NULL, 1540900542, 1540900542);
INSERT INTO `auth_item` VALUES ('/postmethod/delete', 2, NULL, NULL, NULL, 1540900542, 1540900542);
INSERT INTO `auth_item` VALUES ('/postmethod/index', 2, NULL, NULL, NULL, 1540900520, 1540900520);
INSERT INTO `auth_item` VALUES ('/postmethod/update', 2, NULL, NULL, NULL, 1540900542, 1540900542);
INSERT INTO `auth_item` VALUES ('/postmethod/view', 2, NULL, NULL, NULL, 1540900542, 1540900542);
INSERT INTO `auth_item` VALUES ('/site/*', 2, NULL, NULL, NULL, 1476842776, 1476842776);
INSERT INTO `auth_item` VALUES ('/site/captcha', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/site/diqu', 2, NULL, NULL, NULL, 1522832188, 1522832188);
INSERT INTO `auth_item` VALUES ('/site/error', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/site/index', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/site/login', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/site/logout', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/user/*', 2, NULL, NULL, NULL, 1476842778, 1476842778);
INSERT INTO `auth_item` VALUES ('/user/create', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/user/delete', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/user/index', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/user/mymessage', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/user/myview', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/user/update', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('/user/view', 2, NULL, NULL, NULL, 1532499966, 1532499966);
INSERT INTO `auth_item` VALUES ('后台用户', 1, '后台用户', NULL, NULL, 1546071224, 1546071224);
INSERT INTO `auth_item` VALUES ('系统管理员', 1, NULL, NULL, NULL, 1448545005, 1448545005);
INSERT INTO `auth_item` VALUES ('超级管理员', 2, NULL, NULL, NULL, 1448544909, 1448544909);

-- ----------------------------
-- Table structure for auth_item_child
-- ----------------------------
DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child`  (
  `parent` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `child` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`parent`, `child`) USING BTREE,
  INDEX `child`(`child`) USING BTREE,
  INDEX `parent`(`parent`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员权限关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_item_child
-- ----------------------------
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/account-log/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/account-log/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/account-log/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/account-log/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/account-log/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/account-log/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/address/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/address/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/address/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/address/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/address/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/address/view');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/*');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/assignment/*');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/assignment/assign');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/assignment/index');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/assignment/search');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/assignment/view');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/default/*');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/default/index');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/menu/*');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/menu/create');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/menu/delete');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/menu/index');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/menu/update');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/menu/view');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/permission/*');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/permission/assign');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/permission/create');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/permission/delete');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/permission/index');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/permission/search');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/permission/update');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/permission/view');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/role/*');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/role/assign');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/role/create');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/role/delete');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/role/index');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/role/search');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/role/update');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/role/view');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/route/*');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/route/assign');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/route/create');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/route/index');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/route/search');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/rule/*');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/rule/create');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/rule/delete');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/rule/index');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/rule/update');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/admin/rule/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/adsense/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/adsense/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/adsense/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/adsense/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/adsense/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/adsense/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/advertising/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/advertising/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/advertising/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/advertising/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/advertising/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/advertising/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/backup/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/backup/default/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/backup/default/clean');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/backup/default/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/backup/default/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/backup/default/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/backup/default/restore');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/backup/default/upload');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/brand/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/brand/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/brand/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/brand/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/brand/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/brand/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/cart/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/cart/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/cart/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/cart/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/cart/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/cart/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/category/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/category/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/category/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/category/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/category/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/config/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/config/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/config/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/config/fenxiao');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/config/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/config/jingxiao');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/config/system');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/config/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/config/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/config/weixin');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/coupon-auto-send/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/coupon-auto-send/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/coupon-auto-send/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/coupon-auto-send/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/coupon-auto-send/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/coupon-auto-send/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/coupon/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/coupon/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/coupon/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/coupon/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/coupon/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/coupon/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/express/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/express/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/express/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/express/list');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/express/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/express/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/fenxiaoshang/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/fenxiaoshang/apply');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/fenxiaoshang/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/fenxiaoshang/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/fenxiaoshang/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/fenxiaoshang/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/fenxiaoshang/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-attr-group/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-attr-group/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-attr-group/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-attr-group/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-attr-group/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-attr-group/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-attr/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-attr/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-attr/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-attr/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-attr/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-attr/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-class/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-class/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-class/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-class/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-class/searchlist');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-class/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-class/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-sku/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-sku/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-sku/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-sku/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-sku/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods-sku/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/goods/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/jingxiaoshang/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/jingxiaoshang/apply');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/jingxiaoshang/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/jingxiaoshang/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/jingxiaoshang/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/jingxiaoshang/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/jingxiaoshang/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/level/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/level/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/level/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/level/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/level/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/level/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/member-coupon/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/member-coupon/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/member-coupon/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/member-coupon/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/member-coupon/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/member-coupon/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/member/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/member/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/member/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/member/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/member/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/member/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/money/analysis');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/money/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/money/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/money/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/money/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/money/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/news/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/news/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/news/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/news/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/news/isshow');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/news/recommend');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/news/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/news/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/newsclassify/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/newsclassify/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/newsclassify/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/newsclassify/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/newsclassify/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/newsclassify/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/order-goods/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/order-goods/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/order-goods/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/order-goods/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/order-goods/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/order-goods/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/order/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/order/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/order/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/order/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/order/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/order/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/payment/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/payment/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/payment/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/payment/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/payment/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/payment/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/postmethod/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/postmethod/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/postmethod/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/postmethod/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/postmethod/view');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/site/*');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/site/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/site/captcha');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/site/captcha');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/site/diqu');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/site/error');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/site/error');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/site/index');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/site/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/site/login');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/site/login');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/site/logout');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/site/logout');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/user/*');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/user/*');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/user/create');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/user/create');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/user/delete');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/user/delete');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/user/index');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/user/index');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/user/mymessage');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/user/myview');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/user/update');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/user/update');
INSERT INTO `auth_item_child` VALUES ('系统管理员', '/user/view');
INSERT INTO `auth_item_child` VALUES ('超级管理员', '/user/view');

-- ----------------------------
-- Table structure for auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule`  (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` int(11) NULL DEFAULT NULL,
  `updated_at` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `created_at`(`created_at`) USING BTREE,
  INDEX `updated_at`(`updated_at`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员权限规则表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for brand
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '索引ID',
  `brand_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌名称',
  `brand_initial` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌首字母',
  `brand_pic` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `brand_sort` tinyint(3) UNSIGNED NULL DEFAULT 0 COMMENT '排序',
  `brand_recommend` tinyint(1) NULL DEFAULT 0 COMMENT '推荐，0为否，1为是，默认为0',
  `class_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '所属分类id',
  `show_type` tinyint(1) NULL DEFAULT 0 COMMENT '品牌展示类型 0表示图片 1表示文字 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '品牌表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `buyerId` bigint(20) NULL DEFAULT 0 COMMENT '买家id',
  `goodsId` bigint(20) NULL DEFAULT 0 COMMENT '商品id',
  `goodsName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `goodsPrice` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '商品价格',
  `goodsIntegral` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '商品积分',
  `goodsNum` int(11) NULL DEFAULT 1 COMMENT '购买商品数量',
  `attr` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '规格',
  `attrInfo` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '规格详情',
  `sku_id` bigint(20) NULL DEFAULT NULL COMMENT 'sku编号',
  `goods_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品图片',
  `integral` decimal(12, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '商品积分',
  `ispay` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '购物车' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '栏目标题',
  `keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关键字',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `parent_id` int(12) UNSIGNED NULL DEFAULT NULL COMMENT '上级目录',
  `is_index` tinyint(5) NULL DEFAULT 0,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rank` int(2) UNSIGNED NOT NULL DEFAULT 0 COMMENT '栏目权重',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `rank`(`rank`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '栏目分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for collect
-- ----------------------------
DROP TABLE IF EXISTS `collect`;
CREATE TABLE `collect`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_id` int(11) NULL DEFAULT NULL COMMENT '会员ID',
  `goods_id` int(11) NULL DEFAULT NULL COMMENT '商品ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '收藏表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NULL DEFAULT NULL,
  `goods_id` int(11) NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'good 好评 mid  中评 low 差评',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `imgs` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '上传图片',
  `member_id` int(11) NULL DEFAULT 0 COMMENT '会员ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '评论时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评论表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for commission_log
-- ----------------------------
DROP TABLE IF EXISTS `commission_log`;
CREATE TABLE `commission_log`  (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `memberId` int(11) NULL DEFAULT 0 COMMENT '会员',
  `orderId` bigint(20) NULL DEFAULT 0,
  `amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '订单金额',
  `money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '分成金额',
  `status` int(1) NULL DEFAULT 0 COMMENT '领取状态 0表示未领取 1表示已分配',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分销商分成表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `cKey` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '键值',
  `cValue` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '值',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES (1, 'system', '{\"name\":\"\",\"urladdress\":\"\",\"keywords\":\"\",\"description\":\"\",\"telephone\":\"\",\"companyAddress\":\"\",\"icp\":\"\",\"notice\":\"\",\"company\":\"\",\"integral\":\"1\",\"discount\":\"100\",\"logo\":\"\\/uploads\\/config\\/logo\\/201812131451184147558338.jpg\",\"erweima\":\"\\/uploads\\/config\\/erweima\\/201812121817244318190610.png\",\"qq\":\"\",\"order_rank\":\"2\",\"waplogo\":\"\\/uploads\\/config\\/waplogo\\/201712010248160093972471.png\"}', '2018-03-05 09:13:22', '2019-01-24 08:00:16');
INSERT INTO `config` VALUES (2, 'weixin', '{\"name\":\"\",\"wxid\":\"\",\"wxnumber\":\"\",\"appid\":\"\",\"appsecret\":\"\",\"token\":\"weixin\",\"aesencodingkey\":\"\",\"notifyUrl\":\"*****\\/notify.php\"}', '2016-07-15 11:38:47', '2016-07-15 11:38:49');
INSERT INTO `config` VALUES (3, 'fenxiao', '{\"state\":\"on\",\"fashion\":\"2\",\"cash\":\"off\",\"auditing\":\"off\",\"mincash\":\"1\",\"maxcash\":\"500\",\"lists\":[{\"money\":\"60\",\"percent\":\"60\"},{\"money\":\"20\",\"percent\":\"20\"},{\"money\":\"10\",\"percent\":\"10\"}],\"freezeday\":0,\"poundage\":0}', '2016-07-15 11:39:18', '2016-07-15 11:39:22');
INSERT INTO `config` VALUES (4, 'wxmenu', '{\"state\":\"on\",\"fashion\":\"2\",\"cash\":\"off\",\"auditing\":\"off\",\"mincash\":\"1\",\"maxcash\":\"500\",\"lists\":[{\"money\":\"60\",\"percent\":\"10\"},{\"money\":\"10\",\"percent\":\"10\"},{\"money\":\"10\",\"percent\":\"10\"},{\"money\":\"11\",\"percent\":\"11\"}]}', '2016-08-01 15:49:21', '2016-08-01 15:49:23');
INSERT INTO `config` VALUES (5, 'jingxiao', '{\"state\":\"on\",\"fashion\":\"2\",\"cash\":\"off\",\"auditing\":\"off\",\"mincash\":\"1\",\"maxcash\":\"500\",\"lists\":[{\"money\":\"60\",\"percent\":\"10\"},{\"money\":\"10\",\"percent\":\"10\"},{\"money\":\"10\",\"percent\":\"10\"}],\"freezeday\":0,\"poundage\":0}', '2016-07-15 11:39:18', '2016-07-15 11:39:22');
INSERT INTO `config` VALUES (6, 'youfei', '{\"youfei\":\"1\"}', '2016-07-15 11:39:18', '2016-07-15 11:39:22');
INSERT INTO `config` VALUES (7, 'INDEX_EXPAND', '{\"logo\":\"\\/public\\/images\\/logo.png\",\"times\":\"\",\"map_key\":\"\",\"seller_code\":\"0023\",\"device_key\":\"7\",\"device_secret\":\"c5ee600-35e7-11e7-ba76-23fb85a6b085\",\"tel_phone\":\"\",\"welcome_words\":\"\",\"device_api\":\"http:\\/\\/175.6.71.238:8090\",\"access_key_id\":\"\",\"access_key_secret\":\"\",\"sms_code\":\"\"}', NULL, NULL);
INSERT INTO `config` VALUES (8, 'INDEX_INTEGRAL', '{\"charge_integral\":0,\"buy_goods_type\":\"\",\"buy_goods_integral\":0}', NULL, NULL);

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `store_id` bigint(11) NULL DEFAULT 0 COMMENT '店铺编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '优惠券名称',
  `desc` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '优惠券介绍',
  `pic_url` varchar(2048) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `discount_type` smallint(6) NOT NULL DEFAULT 1 COMMENT '优惠券类型：1=满减，2=折扣',
  `min_price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '最低消费金额',
  `sub_price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '优惠金额',
  `discount` decimal(3, 1) NOT NULL DEFAULT 10.0 COMMENT '折扣率',
  `expire_type` smallint(1) NOT NULL DEFAULT 1 COMMENT '到期类型：1=领取后N天过期，2=指定有效期',
  `expire_day` int(11) NOT NULL DEFAULT 0 COMMENT '有效天数，expire_type=1时',
  `begin_time` datetime(0) NULL DEFAULT NULL COMMENT '有效期开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '有效期结束时间',
  `addtime` datetime(0) NOT NULL,
  `is_delete` int(6) NOT NULL DEFAULT 0,
  `total_count` int(11) NOT NULL DEFAULT -1 COMMENT '发放总数量',
  `is_join` smallint(6) NOT NULL DEFAULT 1 COMMENT '是否加入领券中心 1--不加入领券中心 2--加入领券中心',
  `sort` int(11) NULL DEFAULT 100 COMMENT '排序按升序排列',
  `is_integral` smallint(6) NOT NULL DEFAULT 1 COMMENT '是否加入积分商城 1--不加入 2--加入',
  `integral` int(11) NOT NULL DEFAULT 0 COMMENT '兑换需要积分数量',
  `price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '售价',
  `total_num` int(11) NOT NULL DEFAULT 0 COMMENT '积分商城发放总数',
  `type` int(11) NULL DEFAULT 1 COMMENT '券类型1店铺券 2 商品券,3,外部投放券 4通用券',
  `user_num` int(11) NOT NULL DEFAULT 0 COMMENT '每人限制兑换数量',
  `goods_id_list` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `store_id`(`store_id`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE,
  INDEX `is_join`(`is_join`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '优惠券' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for coupon_auto_send
-- ----------------------------
DROP TABLE IF EXISTS `coupon_auto_send`;
CREATE TABLE `coupon_auto_send`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `event` int(11) NOT NULL DEFAULT 1 COMMENT '触发事件：1=分享，2=购买并付款',
  `send_times` int(11) NOT NULL DEFAULT 1 COMMENT '最多发放次数，0表示不限制',
  `addtime` int(11) NOT NULL DEFAULT 0,
  `is_delete` smallint(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `is_delete`(`is_delete`) USING BTREE,
  INDEX `store_id`(`store_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '优惠券自动发放' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for express
-- ----------------------------
DROP TABLE IF EXISTS `express`;
CREATE TABLE `express`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物流公司名称',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '显示',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司编号',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 97 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of express
-- ----------------------------
INSERT INTO `express` VALUES (1, 'AAE', 0, 'AAEWEB', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (2, 'Aramex', 0, 'ARAMEX', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (3, 'DHL', 0, 'DHL', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (4, 'DPEX', 0, 'DPEX', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (5, 'D速', 0, 'DEXP', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (6, 'EMS', 0, 'EMS', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (7, 'EWE', 0, 'EWE', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (8, 'FedEx', 0, 'FEDEX', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (9, 'FedEx国际', 0, 'FEDEXIN', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (10, 'PCA', 0, 'PCA', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (11, 'TNT', 0, 'TNT', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (12, 'UPS', 0, 'UPS', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (13, '安捷', 0, 'ANJELEX', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (14, '安能', 0, 'ANE', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (15, '安能快递', 0, 'ANEEX', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (16, '安信达', 0, 'ANXINDA', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (17, '百福东方', 0, 'EES', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (18, '百世快递', 0, 'HTKY', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (19, '百世快运', 0, 'BSKY', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (20, '程光', 0, 'FLYWAYEX', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (21, '大田', 0, 'DTW', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (22, '德邦', 0, 'DEPPON', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (23, '飞洋', 0, 'GCE', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (24, '凤凰', 0, 'PHOENIXEXP', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (25, '富腾达', 0, 'FTD', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (26, '共速达', 0, 'GSD', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (27, '国通', 0, 'GTO', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (28, '黑狗', 0, 'BLACKDOG', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (29, '恒路', 0, 'HENGLU', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (30, '鸿远', 0, 'HYE', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (31, '华企', 0, 'HQKY', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (32, '急先达', 0, 'JOUST', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (33, '加运美', 0, 'TMS', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (34, '佳吉', 0, 'JIAJI', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (35, '佳怡', 0, 'JIAYI', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (36, '嘉里物流', 0, 'KERRY', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (37, '锦程快递', 0, 'HREX', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (38, '晋越', 0, 'PEWKEE', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (39, '京东', 0, 'JD', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (40, '京广', 0, 'KKE', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (41, '九曳', 0, 'JIUYESCM', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (42, '跨越', 0, 'KYEXPRESS', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (43, '快捷', 0, 'FASTEXPRESS', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (44, '蓝天', 0, 'BLUESKY', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (45, '联昊通', 0, 'LTS', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (46, '龙邦', 0, 'LBEX', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (47, '壹米滴答', 0, 'YIMIDIDA', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (48, '日日顺物流', 0, 'RRS', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (49, '民航', 0, 'CAE', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (50, '能达', 0, 'ND56', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (51, '配思航宇', 0, 'PEISI', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (52, '平安快递', 0, 'EFSPOST', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (53, '秦远物流', 0, 'CHINZ56', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (54, '全晨', 0, 'QCKD', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (55, '全峰', 0, 'QFKD', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (56, '全一', 0, 'APEX', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (57, '如风达', 0, 'RFD', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (58, '三态', 0, 'SFC', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (59, '申通', 1, 'STO', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (60, '盛丰', 0, 'SFWL', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (61, '盛辉', 0, 'SHENGHUI', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (62, '顺达快递', 0, 'SDEX', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (63, '顺丰', 1, 'SFEXPRESS', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (64, '苏宁', 0, 'SUNING', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (65, '速尔', 0, 'SURE', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (66, '天地华宇', 0, 'HOAU', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (67, '天天', 0, 'TTKDEX', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (68, '万庚', 0, 'VANGEN', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (69, '万家物流', 0, 'WANJIA', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (70, '万象', 0, 'EWINSHINE', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (71, '文捷航空', 0, 'GZWENJIE', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (72, '新邦', 0, 'XBWL', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (73, '信丰', 0, 'XFEXPRESS', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (74, '亚风', 0, 'BROADASIA', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (75, '宜送', 0, 'YIEXPRESS', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (76, '易达通', 0, 'QEXPRESS', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (77, '易通达', 0, 'ETD', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (78, '优速', 0, 'UC56', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (79, '邮政包裹', 0, 'CHINAPOST', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (80, '原飞航', 0, 'YFHEX', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (81, '圆通', 1, 'YTO', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (82, '源安达', 0, 'YADEX', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (83, '远成', 0, 'YCGWL', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (84, '越丰', 0, 'YFEXPRESS', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (85, '运通', 0, 'YTEXPRESS', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (86, '韵达', 1, 'YUNDA', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (87, '宅急送', 1, 'ZJS', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (88, '芝麻开门', 0, 'ZMKMEX', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (89, '中国东方', 0, 'COE', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (90, '中铁快运', 0, 'CRE', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (91, '中铁物流', 0, 'ZTKY', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (92, '中通', 1, 'ZTO', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (93, '中通快运', 0, 'ZTO56', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (94, '中邮', 0, 'CNPL', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (95, '品俊快递', 0, 'PJKD', '2018-08-20 11:17:00', '2018-08-20 11:17:00');
INSERT INTO `express` VALUES (96, '汇通快递', 0, 'HTKY', '2018-08-20 11:17:00', '2018-08-20 11:17:00');

-- ----------------------------
-- Table structure for fallintolog
-- ----------------------------
DROP TABLE IF EXISTS `fallintolog`;
CREATE TABLE `fallintolog`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `orderId` bigint(20) NULL DEFAULT NULL COMMENT '订单编号',
  `memberId` bigint(20) NULL DEFAULT NULL COMMENT '会员id',
  `goodsId` bigint(20) NULL DEFAULT NULL COMMENT '商品id',
  `fallIntoMoney` decimal(10, 2) NULL DEFAULT NULL COMMENT '分成金额',
  `actualMoney` decimal(10, 2) NULL DEFAULT NULL COMMENT '实际分成',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `modifyTime` datetime(0) NULL DEFAULT NULL,
  `state` int(11) NULL DEFAULT 0 COMMENT '领取状态 0表示未领取 1表示已分配',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for fenxiaoshang
-- ----------------------------
DROP TABLE IF EXISTS `fenxiaoshang`;
CREATE TABLE `fenxiaoshang`  (
  `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_id` int(12) NOT NULL COMMENT 'Member ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `phone` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `recommend` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推荐人信息',
  `status` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '申请状态',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '申请时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `member_id`(`member_id`) USING BTREE,
  INDEX `phone`(`phone`) USING BTREE,
  INDEX `status`(`status`) USING BTREE,
  INDEX `create_time`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分销商申请表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `goods_jingle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品广告词',
  `gc_id` bigint(20) NULL DEFAULT NULL COMMENT '商品分类id',
  `egc_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '拓展分类ID',
  `use_attr` int(11) NULL DEFAULT 0 COMMENT '是否使用规格',
  `brand_id` bigint(20) NULL DEFAULT NULL COMMENT '品牌id',
  `goods_price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '商品价格',
  `goods_promotion_price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '商品促销价格',
  `goods_promotion_type` int(11) NULL DEFAULT 0 COMMENT '促销类型 0无促销，1团购，2限时折扣',
  `goods_marketprice` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '市场价',
  `goods_serial` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商家编号',
  `goods_storage_alarm` int(11) NULL DEFAULT 0 COMMENT '库存报警值',
  `goods_click` int(11) NULL DEFAULT 0 COMMENT '商品点击数量',
  `goods_salenum` int(11) NULL DEFAULT 0 COMMENT '销售数量',
  `goods_collect` int(11) NULL DEFAULT 0 COMMENT '收藏数量',
  `attr` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '规格属性',
  `goods_spec` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品规格序列化',
  `goods_body` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品详情',
  `mobile_body` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '手机端详情',
  `goods_storage` int(11) NULL DEFAULT 0 COMMENT '商品库存',
  `goods_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品主图',
  `goods_image` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品相册',
  `goods_state` int(11) NULL DEFAULT 0 COMMENT '商品状态 0下架，1正常，10违规（禁售）',
  `goods_addtime` datetime(0) NULL DEFAULT NULL COMMENT '商品添加时间',
  `goods_edittime` datetime(0) NULL DEFAULT NULL COMMENT '商品编辑时间',
  `goods_vat` int(11) NULL DEFAULT 0 COMMENT '是否开具增值税发票 1是，0否',
  `evaluation_good_star` float NULL DEFAULT 5 COMMENT '好评星级',
  `evaluation_count` int(11) NULL DEFAULT 0 COMMENT '评价数',
  `is_virtual` int(11) NULL DEFAULT 0 COMMENT '是否为虚拟商品 1是，0否',
  `is_appoint` int(11) NULL DEFAULT 0 COMMENT '是否是预约商品 1是，0否',
  `is_presell` int(11) NULL DEFAULT 0 COMMENT '是否是预售商品 1是，0否',
  `have_gift` int(11) NULL DEFAULT 0 COMMENT '是否拥有赠品',
  `isdelete` int(11) NULL DEFAULT 0 COMMENT '是否删除',
  `type` int(11) NULL DEFAULT 1 COMMENT '商品类型:1现金商品 2积分商品 3混合商品',
  `integral` int(11) NULL DEFAULT 0 COMMENT '积分',
  `goods_weight` decimal(10, 1) NULL DEFAULT 0.0 COMMENT '重量 单位KG',
  `is_second_kill` tinyint(1) UNSIGNED NULL DEFAULT NULL COMMENT '是否秒杀',
  `second_kill_groups` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '秒杀时段',
  `second_price` decimal(12, 2) NULL DEFAULT NULL COMMENT '秒杀价格',
  `second_limit` int(12) NULL DEFAULT NULL COMMENT '秒杀限制',
  `goods_commend` tinyint(1) NULL DEFAULT 0 COMMENT '商品推荐 1是，0否 默认0',
  `goods_hot` tinyint(1) UNSIGNED NULL DEFAULT NULL COMMENT '热销产品',
  `third_code` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品代号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_attr
-- ----------------------------
DROP TABLE IF EXISTS `goods_attr`;
CREATE TABLE `goods_attr`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NULL DEFAULT NULL,
  `attr_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性名称',
  `is_delete` int(11) NULL DEFAULT 0 COMMENT '是否删除',
  `is_default` int(11) NULL DEFAULT 0 COMMENT '是否默认属性',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '属性值列表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_attr_group
-- ----------------------------
DROP TABLE IF EXISTS `goods_attr_group`;
CREATE TABLE `goods_attr_group`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '属性组编号',
  `group_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性组名称',
  `is_delete` int(11) NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品属性组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_class
-- ----------------------------
DROP TABLE IF EXISTS `goods_class`;
CREATE TABLE `goods_class`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `fid` bigint(20) NULL DEFAULT 0 COMMENT '父级',
  `icoImg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `modifyTime` datetime(0) NULL DEFAULT NULL,
  `is_del` tinyint(1) NULL DEFAULT 0 COMMENT '1是删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_comment
-- ----------------------------
DROP TABLE IF EXISTS `goods_comment`;
CREATE TABLE `goods_comment`  (
  `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `goods_id` int(12) UNSIGNED NOT NULL COMMENT '商品ID',
  `member_id` int(12) UNSIGNED NOT NULL COMMENT '用户ID',
  `type` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '评论类型 good: 好评,medium: 中评,bad:差评 ',
  `number` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单编号',
  `score` decimal(10, 1) NOT NULL DEFAULT 0.0 COMMENT '评分',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '评论内容',
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '上传图片',
  `create_time` date NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `detail_id`(`goods_id`) USING BTREE,
  INDEX `user_id`(`member_id`) USING BTREE,
  INDEX `type`(`type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品评论表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_comment
-- ----------------------------
INSERT INTO `goods_comment` VALUES (1, 7, 6, 'GOOD', '201812290405135869', 5.0, '满意', '/uploads/jpeg/2018-12-29/7o6Mncpx5e.jpeg', '2018-12-29');
INSERT INTO `goods_comment` VALUES (2, 9, 6, 'GOOD', '201812290345125552', 5.0, '赞', '/uploads/png/2018-12-29/FVA04igpPu.png', '2018-12-29');
INSERT INTO `goods_comment` VALUES (3, 8, 6, 'BAD', '201812290339274169', 1.0, '不好', '', '2018-12-29');

-- ----------------------------
-- Table structure for goods_sku
-- ----------------------------
DROP TABLE IF EXISTS `goods_sku`;
CREATE TABLE `goods_sku`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'sku编码',
  `goods_id` bigint(20) NULL DEFAULT NULL COMMENT '商品编号',
  `attr` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '属性组合',
  `num` int(11) NULL DEFAULT 0 COMMENT '库存',
  `price` decimal(10, 0) NULL DEFAULT 0 COMMENT '价格',
  `integral` int(11) NULL DEFAULT 0 COMMENT '积分',
  `goods_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品编码',
  `picimg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品sku表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for history
-- ----------------------------
DROP TABLE IF EXISTS `history`;
CREATE TABLE `history`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_id` int(11) NULL DEFAULT NULL COMMENT '会员ID',
  `goods_id` int(11) NULL DEFAULT NULL COMMENT '商品ID',
  `add_time` date NULL DEFAULT NULL COMMENT '日期',
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '我的足迹' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of history
-- ----------------------------
INSERT INTO `history` VALUES (19, 7, 11, '2018-12-29', '2018-12-29 14:55:18');
INSERT INTO `history` VALUES (22, 6, 6, '2018-12-29', '2018-12-29 15:39:14');
INSERT INTO `history` VALUES (30, 5, 3, '2018-12-29', '2018-12-29 15:56:16');
INSERT INTO `history` VALUES (33, 6, 9, '2018-12-29', '2018-12-29 16:12:35');
INSERT INTO `history` VALUES (39, 6, 5, '2018-12-29', '2018-12-29 16:17:44');
INSERT INTO `history` VALUES (40, 6, 4, '2018-12-29', '2018-12-29 16:17:58');
INSERT INTO `history` VALUES (41, 6, 2, '2018-12-29', '2018-12-29 16:18:01');
INSERT INTO `history` VALUES (42, 5, 7, '2018-12-29', '2018-12-29 16:27:43');
INSERT INTO `history` VALUES (50, 14, 10, '2018-12-29', '2018-12-29 18:21:10');
INSERT INTO `history` VALUES (51, 14, 8, '2018-12-29', '2018-12-29 18:21:10');
INSERT INTO `history` VALUES (52, 7, 1, '2019-01-16', '2019-01-16 16:40:49');

-- ----------------------------
-- Table structure for integral_log
-- ----------------------------
DROP TABLE IF EXISTS `integral_log`;
CREATE TABLE `integral_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `memberId` int(11) NULL DEFAULT 0 COMMENT '会员',
  `orderId` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `pay_integral` int(11) NULL DEFAULT 0 COMMENT '消耗积分',
  `old_integral` int(11) NULL DEFAULT 0 COMMENT '原有积分',
  `now_integral` int(11) NULL DEFAULT 0 COMMENT '现有积分',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of integral_log
-- ----------------------------
INSERT INTO `integral_log` VALUES (1, 5, '201811280234403949', 0, 0, 0, '2018-12-28 13:22:36');
INSERT INTO `integral_log` VALUES (2, 5, '201811280236069040', 50, 0, 50, '2018-12-28 13:22:38');
INSERT INTO `integral_log` VALUES (3, 5, '201812290252556768', 40, 50, 10, '2018-12-29 14:53:07');
INSERT INTO `integral_log` VALUES (4, 5, '201812290354185532', -20, 10, 30, '2018-12-29 15:54:41');
INSERT INTO `integral_log` VALUES (5, 5, '201812290237178427', -20, 30, 50, '2018-12-29 15:54:50');
INSERT INTO `integral_log` VALUES (6, 5, '201812290355469836', -60, 50, 110, '2018-12-29 15:56:22');
INSERT INTO `integral_log` VALUES (7, 6, '201812290405135869', -50, 0, 50, '2018-12-29 16:08:24');
INSERT INTO `integral_log` VALUES (8, 6, '201812290345125552', -10, 50, 60, '2018-12-29 16:11:00');

-- ----------------------------
-- Table structure for jingxiaoshang
-- ----------------------------
DROP TABLE IF EXISTS `jingxiaoshang`;
CREATE TABLE `jingxiaoshang`  (
  `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_id` int(12) NOT NULL COMMENT 'Member ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `phone` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `status` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '申请状态',
  `province` int(12) UNSIGNED NULL DEFAULT NULL COMMENT '经销商省编码',
  `city` int(12) UNSIGNED NULL DEFAULT NULL COMMENT '经销商市编码',
  `region` int(12) UNSIGNED NULL DEFAULT NULL COMMENT '经销商区编码',
  `level` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '经销商等级',
  `good_ids` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '代理商品',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '申请时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `member_id`(`member_id`) USING BTREE,
  INDEX `phone`(`phone`) USING BTREE,
  INDEX `status`(`status`) USING BTREE,
  INDEX `create_time`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '经销商申请表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for level
-- ----------------------------
DROP TABLE IF EXISTS `level`;
CREATE TABLE `level`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '等级名称',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `score` int(11) NULL DEFAULT NULL COMMENT '升级分值',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `modifyTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of level
-- ----------------------------
INSERT INTO `level` VALUES (1, '普通会员', NULL, 1000, '2018-12-29 16:04:07', '2018-12-29 16:04:07');
INSERT INTO `level` VALUES (2, '一级会员', NULL, 5000, '2018-12-29 16:04:18', '2018-12-29 16:04:18');
INSERT INTO `level` VALUES (3, '二级会员', NULL, 10000, '2018-12-29 16:04:29', '2018-12-29 16:04:29');
INSERT INTO `level` VALUES (4, '三级会员', NULL, 30000, '2018-12-29 16:04:45', '2018-12-29 16:04:45');

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '帐号',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `role` int(11) NULL DEFAULT NULL COMMENT '角色名称',
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `sex` int(11) NULL DEFAULT NULL COMMENT '性别',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '生日',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  `avatarTm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像缩略图',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `expiryTime` datetime(0) NULL DEFAULT NULL COMMENT '会员过期时间',
  `isText` int(11) NULL DEFAULT 1 COMMENT '是否签署协议',
  `last_visit` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `userType` int(11) NULL DEFAULT NULL COMMENT '用户类别',
  `authkey` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权',
  `accessToken` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remainder` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '余额',
  `integral` int(11) NULL DEFAULT 0 COMMENT '积分',
  `rank` int(11) NULL DEFAULT 0 COMMENT '等级分数',
  `brokerage` decimal(10, 0) NULL DEFAULT 0 COMMENT '分销商佣金',
  `proxyBrokerage` decimal(10, 0) UNSIGNED NULL DEFAULT NULL COMMENT '经销商佣金',
  `pid` bigint(20) NULL DEFAULT 0 COMMENT '上一级',
  `wxopenid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信openid',
  `wxnick` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信昵称',
  `ticket` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qrodeUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推荐二维码',
  `is_fenxiao` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '是否是分销商',
  `is_daili` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '是否是代理商',
  `origin` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '所在区域',
  `cost_amount` decimal(12, 2) UNSIGNED NULL DEFAULT NULL COMMENT '消费总金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_coupon
-- ----------------------------
DROP TABLE IF EXISTS `member_coupon`;
CREATE TABLE `member_coupon`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `store_id` bigint(20) NULL DEFAULT NULL COMMENT '店铺编号',
  `member_id` bigint(20) NULL DEFAULT NULL COMMENT '会员编号',
  `coupon_id` bigint(20) NULL DEFAULT NULL COMMENT '优惠券编号',
  `coupon_auto_send_id` bigint(20) NULL DEFAULT 0 COMMENT '自动发放id',
  `begin_time` datetime(0) NULL DEFAULT NULL COMMENT '有效期开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '有效期结束时间',
  `is_expire` int(11) NULL DEFAULT 0 COMMENT '是否已过期：0=未过期，1=已过期',
  `is_use` int(11) NULL DEFAULT 0 COMMENT '是否已使用：0=未使用，1=已使用',
  `is_delete` int(11) NULL DEFAULT 0 COMMENT '是否删除',
  `addtime` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `type` int(11) NULL DEFAULT 2 COMMENT '领取类型 0--平台发放 1--自动发放 2--领取',
  `integral` int(11) NULL DEFAULT 0 COMMENT '兑换支付积分数量',
  `price` decimal(10, 0) NULL DEFAULT 0 COMMENT '兑换支付价格',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户-优惠券关系' ROW_FORMAT = Fixed;

-- ----------------------------
-- Table structure for member_level
-- ----------------------------
DROP TABLE IF EXISTS `member_level`;
CREATE TABLE `member_level`  (
  `id` int(12) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '等级名称',
  `score` int(20) NULL DEFAULT NULL COMMENT '等级分数',
  `sale` decimal(5, 2) NULL DEFAULT NULL COMMENT '等级折扣率',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parent` int(11) NULL DEFAULT NULL,
  `route` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `order` int(11) NULL DEFAULT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent`(`parent`) USING BTREE,
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, '权限管理', NULL, '/admin/default/index', 99, '<i class=\"menu-icon fa fa-cogs\"></i>');
INSERT INTO `menu` VALUES (2, '权限分配', 1, '/admin/assignment/index', 99, '<i class=\"menu-icon fa fa-cogs\"></i>');
INSERT INTO `menu` VALUES (3, '菜单管理', 1, '/admin/menu/index', 99, '<i class=\"menu-icon fa fa-cogs\"></i>');
INSERT INTO `menu` VALUES (4, '角色列表', 1, '/admin/role/index', 99, '<i class=\"menu-icon fa fa-cogs\"></i>');
INSERT INTO `menu` VALUES (5, '功能列表', 1, '/admin/route/index', 99, '<i class=\"menu-icon fa fa-cogs\"></i>');
INSERT INTO `menu` VALUES (6, '规则列表', 1, '/admin/permission/index', 99, '<i class=\"menu-icon fa fa-cogs\"></i>');
INSERT INTO `menu` VALUES (7, '管理员列表', 11, '/user/index', 6, '<i class=\"menu-icon fa fa-cogs\"></i>');
INSERT INTO `menu` VALUES (8, '会员管理', NULL, '/user/index', 3, '<i class=\"menu-icon fa  fa-users\"></i>');
INSERT INTO `menu` VALUES (9, '会员列表', 8, '/member/index', 1, '<i class=\"menu-icon fa  fa-users\"></i>');
INSERT INTO `menu` VALUES (10, '会员等级', 8, '/level/index', 2, '<i class=\"menu-icon fa  fa-users\"></i>');
INSERT INTO `menu` VALUES (11, '商城管理', NULL, NULL, 10, '<i class=\"menu-icon fa  fa-wrench\"></i>');
INSERT INTO `menu` VALUES (13, '商品管理', NULL, NULL, 1, '<i class=\"menu-icon fa fa-th\"></i>');
INSERT INTO `menu` VALUES (14, '商品分类', 13, '/goods-class/index', 1, NULL);
INSERT INTO `menu` VALUES (15, '商品列表', 13, '/goods/index', 2, NULL);
INSERT INTO `menu` VALUES (16, '发布商品', 13, '/goods/create', 3, NULL);
INSERT INTO `menu` VALUES (17, '订单管理', NULL, '/order/index', 2, '<i class=\"menu-icon fa fa-tachometer\"></i>');
INSERT INTO `menu` VALUES (18, '支付管理', 11, '/payment/index', 3, '<i class=\"menu-icon fa  fa-money\"></i>');
INSERT INTO `menu` VALUES (19, '基础设置', 11, '/config/expand', 1, NULL);
INSERT INTO `menu` VALUES (20, '品牌管理', 13, '/brand/index', 4, NULL);
INSERT INTO `menu` VALUES (21, '微信设置', 11, '/config/weixin', 2, NULL);
INSERT INTO `menu` VALUES (22, '财务管理', NULL, NULL, 5, '<i class=\"menu-icon fa  fa-briefcase\"></i>');
INSERT INTO `menu` VALUES (23, '消费记录', 22, '/account-log/index', 1, NULL);
INSERT INTO `menu` VALUES (24, '广告管理', NULL, NULL, 9, '<i class=\"menu-icon fa  fa-asterisk\"></i>');
INSERT INTO `menu` VALUES (25, '广告位管理', 24, '/adsense/index', 10, NULL);
INSERT INTO `menu` VALUES (27, '优惠券管理', NULL, NULL, 7, '<i class=\"menu-icon fa  fa-tags\"></i>');
INSERT INTO `menu` VALUES (28, '优惠券列表', 27, '/coupon/index', 1, NULL);
INSERT INTO `menu` VALUES (29, '网站留言', 11, '/message/index', 3, NULL);
INSERT INTO `menu` VALUES (30, '文章管理', NULL, NULL, 6, '<i class=\"menu-icon fa   fa-list-alt\"></i>');
INSERT INTO `menu` VALUES (31, '文章列表', 30, '/news/index', 1, NULL);
INSERT INTO `menu` VALUES (36, '申请管理', NULL, NULL, 4, '<i class=\"menu-icon fa fa-external-link \"></i>');
INSERT INTO `menu` VALUES (38, '经销商列表', 8, '/jingxiaoshang/index', 3, NULL);
INSERT INTO `menu` VALUES (39, '分销商列表', 8, '/fenxiaoshang/index', 4, NULL);
INSERT INTO `menu` VALUES (40, '经销商申请', 36, '/jingxiaoshang/apply', 1, NULL);
INSERT INTO `menu` VALUES (41, '分销商申请', 36, '/fenxiaoshang/apply', 2, NULL);
INSERT INTO `menu` VALUES (42, '经销商分成设置', 11, '/config/jingxiao', 7, NULL);
INSERT INTO `menu` VALUES (43, '分销商分成设置', 11, '/config/fenxiao', 8, NULL);
INSERT INTO `menu` VALUES (44, '订单列表', 17, '/order/index', 1, NULL);
INSERT INTO `menu` VALUES (45, '退货订单', 17, '/order/reback', 2, NULL);
INSERT INTO `menu` VALUES (46, '提现管理', 22, '/money/index', 2, NULL);
INSERT INTO `menu` VALUES (47, '运费设置', 11, '/postmethod/index', 6, NULL);
INSERT INTO `menu` VALUES (48, '数据统计', 22, '/money/analysis', 3, NULL);
INSERT INTO `menu` VALUES (49, '积分设置', 11, '/config/integral', 6, NULL);
INSERT INTO `menu` VALUES (50, '栏目管理', 13, '/category/index', 5, NULL);

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NULL DEFAULT NULL COMMENT '会员id',
  `info` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '消息类容',
  `createTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '网站留言' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration`  (
  `version` varchar(180) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `apply_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for money
-- ----------------------------
DROP TABLE IF EXISTS `money`;
CREATE TABLE `money`  (
  `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT,
  `member_id` int(12) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '提现人姓名',
  `type` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '提现类型',
  `money` decimal(12, 2) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '体现状态 0：审核中， 1：已完成',
  `create_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of money
-- ----------------------------
INSERT INTO `money` VALUES (1, 5, '-', 'CHARGE', 0.10, 1, '2018-12-29 14:51:05');
INSERT INTO `money` VALUES (2, 6, '莫失莫忘', 'MEMBER', 500.00, 1, '2018-12-29 16:01:32');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '文章编号',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章标题',
  `cid` bigint(20) NULL DEFAULT NULL COMMENT '分类id',
  `themeImg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主题图片',
  `themeImgTm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主题缩略图',
  `brief` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简介',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述',
  `seokey` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'seo关键词',
  `seoDepict` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'seo描述',
  `isShow` int(11) NULL DEFAULT 1 COMMENT '是否显示1显示 0不显示',
  `isRmd` int(11) NULL DEFAULT 0 COMMENT '是否推荐1推荐 0 不推荐',
  `clickRate` int(11) NULL DEFAULT 0 COMMENT '点击量',
  `writer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  `uid` bigint(20) NULL DEFAULT NULL COMMENT '发布人id',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `modifyTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for newsclassify
-- ----------------------------
DROP TABLE IF EXISTS `newsclassify`;
CREATE TABLE `newsclassify`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `fid` bigint(20) NULL DEFAULT 0 COMMENT '父级',
  `isShow` int(11) NULL DEFAULT 1 COMMENT '是否显示',
  `isRdm` int(11) NULL DEFAULT 0 COMMENT '是否推荐',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `modifyTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `orderid` bigint(20) NOT NULL DEFAULT 0 COMMENT '订单编号',
  `pay_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付单号',
  `buyer_id` bigint(20) NULL DEFAULT 0 COMMENT '买家id',
  `buyer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '买家姓名',
  `goods_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '商品总价格',
  `order_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '订单总价格',
  `integral_amount` int(11) NULL DEFAULT 0 COMMENT '订单积分',
  `pd_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '预存款支付金额',
  `freight` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '运费',
  `evaluation_state` tinyint(4) NULL DEFAULT 0 COMMENT '评价状态 0未评价，1已评价，2已过期未评价',
  `order_state` tinyint(4) NULL DEFAULT 0 COMMENT '订单状态：0(已取消)0(默认):未付款;1:已付款;2:已发货;3:已收货;',
  `receiver` bigint(20) NULL DEFAULT 0 COMMENT '收货人',
  `receiver_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `receiver_state` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `receiver_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `receiver_mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `receiver_zip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shipping` bigint(20) NULL DEFAULT NULL COMMENT '物流公司编号',
  `shipping_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物流单号',
  `deliveryTime` datetime(0) NULL DEFAULT NULL COMMENT '发货时间',
  `add_time` datetime(0) NULL DEFAULT NULL COMMENT '订单生成时间',
  `payment_time` datetime(0) NULL DEFAULT NULL COMMENT '支付(付款)时间',
  `finnshed_time` datetime(0) NULL DEFAULT NULL COMMENT '订单完成时间',
  `fallinto_state` int(11) NULL DEFAULT 0 COMMENT '分成状态',
  `coupon_id` int(12) UNSIGNED NULL DEFAULT NULL COMMENT '使用的优惠券信息',
  `pay_method` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付方式',
  `reback_status` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '退货状态',
  PRIMARY KEY (`orderid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_goods
-- ----------------------------
DROP TABLE IF EXISTS `order_goods`;
CREATE TABLE `order_goods`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NULL DEFAULT NULL COMMENT '订单id',
  `goods_id` bigint(20) NULL DEFAULT NULL COMMENT '商品id',
  `goods_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品图片',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `goods_price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '商品价格',
  `goods_integral` int(11) NULL DEFAULT 0 COMMENT '积分',
  `fallinto_state` int(11) NULL DEFAULT 0 COMMENT '分成状态',
  `goods_num` int(11) NULL DEFAULT 1 COMMENT '商品数量',
  `fallInto` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '分成金额',
  `goods_pay_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '商品实际成交价',
  `buyer_id` bigint(20) NULL DEFAULT NULL COMMENT '买家ID',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `modifyTime` datetime(0) NULL DEFAULT NULL,
  `sku_id` bigint(20) NULL DEFAULT NULL COMMENT 'sku编号',
  `attr` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '规则',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 319 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_goods
-- ----------------------------
INSERT INTO `order_goods` VALUES (1, 201811200547467205, 1, '/uploads/goods/e75515e2e090227a119413e1c721e043.jpg', '苹果', 60.00, 0, NULL, 2, 0.00, 60.00, 1, '2018-11-20 00:00:00', '2018-11-20 00:00:00', NULL, '蓝色, 25g');
INSERT INTO `order_goods` VALUES (2, 201811200605352204, 1, '/uploads/goods/e75515e2e090227a119413e1c721e043.jpg', '苹果', 60.00, 0, NULL, 2, 0.00, 60.00, 1, '2018-11-20 00:00:00', '2018-11-20 00:00:00', NULL, '蓝色, 25g');
INSERT INTO `order_goods` VALUES (3, 201811200605352204, 1, '/uploads/goods/3e661a842f1aebd97de36532700cabc8.jpg', '苹果', 20.00, 0, NULL, 4, 0.00, 20.00, 1, '2018-11-20 00:00:00', '2018-11-20 00:00:00', NULL, '绿色, 10g');
INSERT INTO `order_goods` VALUES (4, 201811200726500323, 2, '/uploads/goods/e58fd59b830b2e581363bb11231f4c53.png', '干果', 30.00, 1500, NULL, 1, 0.00, 30.00, 1, '2018-11-20 00:00:00', '2018-11-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (5, 201811200734436750, 2, '/uploads/goods/e58fd59b830b2e581363bb11231f4c53.png', '干果', 30.00, 1500, NULL, 1, 0.00, 30.00, 1, '2018-11-20 00:00:00', '2018-11-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (6, 201811200734503623, 2, '/uploads/goods/e58fd59b830b2e581363bb11231f4c53.png', '干果', 30.00, 1500, NULL, 1, 0.00, 30.00, 1, '2018-11-20 00:00:00', '2018-11-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (7, 201811200750265247, 2, '/uploads/goods/e58fd59b830b2e581363bb11231f4c53.png', '干果', 30.00, 1500, NULL, 1, 0.00, 30.00, 1, '2018-11-20 00:00:00', '2018-11-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (8, 201811200750539412, 2, '/uploads/goods/e58fd59b830b2e581363bb11231f4c53.png', '干果', 30.00, 1500, NULL, 1, 0.00, 30.00, 1, '2018-11-20 00:00:00', '2018-11-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (9, 201811200751414077, 2, '/uploads/goods/e58fd59b830b2e581363bb11231f4c53.png', '干果', 30.00, 1500, NULL, 1, 0.00, 30.00, 1, '2018-11-20 00:00:00', '2018-11-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (10, 201811200751571367, 2, '/uploads/goods/e58fd59b830b2e581363bb11231f4c53.png', '干果', 30.00, 1500, NULL, 1, 0.00, 30.00, 1, '2018-11-20 00:00:00', '2018-11-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (11, 201811200752403625, 2, '/uploads/goods/e58fd59b830b2e581363bb11231f4c53.png', '干果', 30.00, 1500, NULL, 1, 0.00, 30.00, 1, '2018-11-20 00:00:00', '2018-11-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (12, 201811210652129984, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 3, 0.00, 10.00, 1, '2018-11-21 00:00:00', '2018-11-21 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (13, 201811210654365409, 1, '/uploads/goods/d81b310daebdc085ffe0a54c5692377b.jpg', '苹果', 25.00, 0, NULL, 3, 0.00, 25.00, 1, '2018-11-21 00:00:00', '2018-11-21 00:00:00', 0, '绿色, 25g');
INSERT INTO `order_goods` VALUES (14, 201811210655126167, 1, '/uploads/goods/b32bfc07c34e09944c5779d1f1f47e0a.jpg', '苹果', 40.00, 0, NULL, 1, 0.00, 40.00, 1, '2018-11-21 00:00:00', '2018-11-21 00:00:00', 0, '蓝色, 10g');
INSERT INTO `order_goods` VALUES (15, 201811210656005803, 1, '/uploads/goods/3e661a842f1aebd97de36532700cabc8.jpg', '苹果', 20.00, 0, NULL, 1, 0.00, 20.00, 1, '2018-11-21 00:00:00', '2018-11-21 00:00:00', 0, '绿色, 10g');
INSERT INTO `order_goods` VALUES (16, 201811210657025439, 1, '/uploads/goods/b0f53e05b81e2f7a72ce933a2e621db6.jpg', '苹果', 36.00, 0, NULL, 1, 0.00, 36.00, 1, '2018-11-21 00:00:00', '2018-11-21 00:00:00', 0, '红色, 10g');
INSERT INTO `order_goods` VALUES (17, 201811210657278101, 1, '/uploads/goods/b0f53e05b81e2f7a72ce933a2e621db6.jpg', '苹果', 36.00, 0, NULL, 1, 0.00, 36.00, 1, '2018-11-21 00:00:00', '2018-11-21 00:00:00', 0, '红色, 10g');
INSERT INTO `order_goods` VALUES (18, 201811210657418231, 1, '/uploads/goods/d81b310daebdc085ffe0a54c5692377b.jpg', '苹果', 25.00, 0, NULL, 1, 0.00, 25.00, 1, '2018-11-21 00:00:00', '2018-11-21 00:00:00', 0, '绿色, 25g');
INSERT INTO `order_goods` VALUES (19, 201811210705017300, 1, '/uploads/goods/d81b310daebdc085ffe0a54c5692377b.jpg', '苹果', 25.00, 0, NULL, 1, 0.00, 25.00, 1, '2018-11-21 00:00:00', '2018-11-21 00:00:00', 0, '绿色, 25g');
INSERT INTO `order_goods` VALUES (20, 201811230631005150, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-23 00:00:00', '2018-11-23 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (21, 201811230155183209, 1, '/uploads/goods/3e661a842f1aebd97de36532700cabc8.jpg', '苹果', 20.00, 0, NULL, 1, 0.00, 20.00, 2, '2018-11-23 00:00:00', '2018-11-23 00:00:00', 0, '绿色, 10g');
INSERT INTO `order_goods` VALUES (22, 201811260550568024, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (23, 201811261048433495, 1, '/uploads/goods/e75515e2e090227a119413e1c721e043.jpg', '苹果', 60.00, 0, NULL, 1, 0.00, 60.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '蓝色, 25g');
INSERT INTO `order_goods` VALUES (24, 201811261136468100, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (25, 201811260137217317, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 4, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (26, 201811260151021965, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 4, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (27, 201811260153388891, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 4, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (28, 201811260205394950, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 4, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (29, 201811260210166150, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 4, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (30, 201811260219529221, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 4, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (31, 201811260225297442, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (32, 201811260225318795, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (33, 201811260230033462, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (34, 201811260238129608, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (35, 201811260244279872, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (36, 201811260246165226, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (37, 201811260247177608, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (38, 201811260248468035, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (39, 201811260259550421, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (40, 201811260303089800, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (41, 201811260602547840, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (42, 201811260625465916, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (43, 201811260628316519, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (44, 201811260629125593, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (45, 201811260629507986, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (46, 201811260752462620, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 10, 0.00, 10.00, 2, '2018-11-26 00:00:00', '2018-11-26 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (47, 201811270516194169, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (48, 201811270521175631, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (49, 201811270523542273, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (50, 201811270524582735, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (51, 201811270525302726, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (52, 201811270526016712, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (53, 201811271013513764, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 3, 0.00, 10.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (54, 201811270202184005, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (55, 201811270258300018, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 1, 0.00, 10.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (56, 201811270304134331, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 2, 0.00, 10.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (57, 201811270310569052, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 2, 0.00, 10.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (58, 201811270311426903, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 2, 0.00, 10.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (59, 201811270312068031, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 10.00, 50, NULL, 2, 0.00, 10.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (60, 201811270323128677, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 0.00, -50, NULL, 1, 0.00, 0.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (61, 201811270324244688, 4, '/uploads/goods/217c2ddaa6e520cc5915e1e6ecd004e6.jpg', '现货 蒙自石榴水果 新鲜包邮 软籽薄皮多汁 云南甜石榴', 99.00, 990, NULL, 1, 0.00, 99.00, 5, '2018-11-27 00:00:00', '2018-11-27 00:00:00', NULL, '默认');
INSERT INTO `order_goods` VALUES (62, 201811270342290288, 4, '/uploads/goods/217c2ddaa6e520cc5915e1e6ecd004e6.jpg', '现货 蒙自石榴水果 新鲜包邮 软籽薄皮多汁 云南甜石榴', 99.00, 990, NULL, 1, 0.00, 99.00, 5, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (63, 201811270342381165, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 0.00, -50, NULL, 1, 0.00, 0.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (64, 201811270345327562, 4, '/uploads/goods/217c2ddaa6e520cc5915e1e6ecd004e6.jpg', '现货 蒙自石榴水果 新鲜包邮 软籽薄皮多汁 云南甜石榴', 99.00, 990, NULL, 1, 0.00, 99.00, 5, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (65, 201811270346102885, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 0.00, -50, NULL, 1, 0.00, 0.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (66, 201811270348043231, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '苹果', 0.00, -50, NULL, 1, 0.00, 0.00, 2, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (67, 201811270529565427, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 1.00, NULL, NULL, 1, 0.00, 1.00, 5, '2018-11-27 00:00:00', '2018-11-27 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (68, 201811281133179374, 4, '/uploads/goods/217c2ddaa6e520cc5915e1e6ecd004e6.jpg', '现货 蒙自石榴水果 新鲜包邮 软籽薄皮多汁 云南甜石榴', 99.00, 990, NULL, 1, 0.00, 99.00, 5, '2018-11-28 00:00:00', '2018-11-28 00:00:00', NULL, '默认');
INSERT INTO `order_goods` VALUES (69, 201811281154508749, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 5, '2018-11-28 00:00:00', '2018-11-28 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (70, 201811280236069040, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果', 0.00, -50, NULL, 1, 0.00, 0.00, 5, '2018-11-28 00:00:00', '2018-11-28 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (71, 201811290406458596, 10, '/uploads/goods/c40e7d1aee51ddb85ef8f28466972544.jpg', '贵州茅台 汉酱51度500mL单瓶装酱香型白酒 苏宁易购自营', 699.00, 6990, NULL, 3, 0.00, 699.00, 5, '2018-11-29 00:00:00', '2018-11-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (72, 201811290421447089, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 2, '2018-11-29 00:00:00', '2018-11-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (73, 201811290430063928, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 2, '2018-11-29 00:00:00', '2018-11-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (74, 201811290432233944, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 1.00, NULL, NULL, 1, 0.00, 1.00, 2, '2018-11-29 00:00:00', '2018-11-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (75, 201811290433564447, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 1.00, NULL, NULL, 1, 0.00, 1.00, 2, '2018-11-29 00:00:00', '2018-11-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (76, 201811290434321014, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 1.00, NULL, NULL, 1, 0.00, 1.00, 2, '2018-11-29 00:00:00', '2018-11-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (77, 201811290440168527, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 2, '2018-11-29 00:00:00', '2018-11-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (78, 201811290515169159, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -100, NULL, 2, 0.00, 0.00, 5, '2018-11-29 00:00:00', '2018-11-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (79, 201811290525506406, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 10, '2018-11-29 00:00:00', '2018-11-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (80, 201811290542207312, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 10, '2018-11-29 00:00:00', '2018-11-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (81, 201811300925417475, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 5, 0.00, 168.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (82, 201811300927337763, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (83, 201811300942551641, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 1, 0.00, 10.00, 10, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (84, 201811301021455241, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 1, 0.00, 10.00, 10, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (85, 201811301057077090, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 1, 0.00, 10.00, 11, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (86, 201811301106443706, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 1, 0.00, 10.00, 12, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (87, 201811300131380539, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, 0, NULL, 0, 0.00, 0.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (88, 201811300237325625, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 10, 0.00, 668.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (89, 201811300259359381, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -200, NULL, 4, 0.00, 0.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (90, 201811300352339777, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (91, 201811300352553307, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (92, 201811300353210897, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (93, 201811300353315033, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (94, 201811300353465504, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 2, 0.00, 168.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (95, 201811300353577096, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 15, 0.00, 168.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (96, 201811300426290295, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 5, 0.00, 668.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (97, 201811300432112021, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 6, 0.00, 668.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (98, 201811300529597928, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 1.00, NULL, NULL, 5, 0.00, 1.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (99, 201811300535149694, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (100, 201811300536477768, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 11, '2018-11-30 00:00:00', '2018-11-30 00:00:00', NULL, '默认');
INSERT INTO `order_goods` VALUES (101, 201811300538269378, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (102, 201811300747230912, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -50, NULL, 1, 0.00, 0.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (103, 201811300749328511, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -50, NULL, 1, 0.00, 0.00, 2, '2018-11-30 00:00:00', '2018-11-30 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (104, 201812030911137030, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 2, '2018-12-03 00:00:00', '2018-12-03 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (105, 201812030921441442, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 2, '2018-12-03 00:00:00', '2018-12-03 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (106, 201812030926303495, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 2, '2018-12-03 00:00:00', '2018-12-03 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (107, 201812030551210716, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 2, '2018-12-03 00:00:00', '2018-12-03 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (108, 201812041130422826, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 2, '2018-12-04 00:00:00', '2018-12-04 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (109, 201812041130587597, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 2, '2018-12-04 00:00:00', '2018-12-04 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (110, 201812041133076426, 3, '', '精品包装18个装苹果', 3.00, 0, NULL, 2, 0.00, 3.00, 2, '2018-12-04 00:00:00', '2018-12-04 00:00:00', 0, '红色, 小');
INSERT INTO `order_goods` VALUES (111, 201812041139380218, 3, '', '精品包装18个装苹果', 3.00, 0, NULL, 2, 0.00, 3.00, 2, '2018-12-04 00:00:00', '2018-12-04 00:00:00', 11, '红色, 小');
INSERT INTO `order_goods` VALUES (112, 201812041154028857, 3, '', '精品包装18个装苹果', 3.00, 0, NULL, 3, 0.00, 3.00, 2, '2018-12-04 00:00:00', '2018-12-04 00:00:00', 11, '红色, 小');
INSERT INTO `order_goods` VALUES (113, 201812110402307321, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 15, '2018-12-11 00:00:00', '2018-12-11 00:00:00', NULL, '默认');
INSERT INTO `order_goods` VALUES (114, 201812110406397727, 3, '', '精品包装18个装苹果', 3.00, 0, NULL, 1, 0.00, 3.00, 17, '2018-12-11 00:00:00', '2018-12-11 00:00:00', 11, '红色, 小');
INSERT INTO `order_goods` VALUES (115, 201812110406391084, 3, '', '精品包装18个装苹果', 3.00, 0, NULL, 1, 0.00, 3.00, 17, '2018-12-11 00:00:00', '2018-12-11 00:00:00', 11, '红色, 小');
INSERT INTO `order_goods` VALUES (116, 201812110445572141, 4, '/uploads/goods/217c2ddaa6e520cc5915e1e6ecd004e6.jpg', '现货 蒙自石榴水果 新鲜包邮 软籽薄皮多汁 云南甜石榴', 99.00, 990, NULL, 1, 0.00, 99.00, 17, '2018-12-11 00:00:00', '2018-12-11 00:00:00', NULL, '默认');
INSERT INTO `order_goods` VALUES (117, 201812120613418645, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 2, '2018-12-12 00:00:00', '2018-12-12 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (118, 201812120614057498, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 2, '2018-12-12 00:00:00', '2018-12-12 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (119, 201812120614255964, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 2, '2018-12-12 00:00:00', '2018-12-12 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (120, 201812120618573053, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 2, 0.00, 0.01, 2, '2018-12-12 00:00:00', '2018-12-12 00:00:00', NULL, '默认');
INSERT INTO `order_goods` VALUES (121, 201812120618573053, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 3, 0.00, 10.00, 2, '2018-12-12 00:00:00', '2018-12-12 00:00:00', NULL, '默认');
INSERT INTO `order_goods` VALUES (122, 201812120747151359, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 6, '2018-12-12 00:00:00', '2018-12-12 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (123, 201812120750375916, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 6, '2018-12-12 00:00:00', '2018-12-12 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (124, 201812131155218612, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 2, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (125, 201812130156122834, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 1, 0.00, 10.00, 23, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (126, 201812130205036659, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 2, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (127, 201812130206037690, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 2, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (128, 201812130207093017, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 1, 0.00, 10.00, 23, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (129, 201812130207263735, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 23, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (130, 201812130207512322, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 2, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (131, 201812130209066911, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 2, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (132, 201812130212467446, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 2, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (133, 201812130247346736, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 15, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (134, 201812130248204412, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 15, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (135, 201812130249208173, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 1, 0.00, 10.00, 15, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (136, 201812130249303488, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 15, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (137, 201812130250201791, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 15, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (138, 201812130313133402, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 15, '2018-12-13 00:00:00', '2018-12-13 00:00:00', NULL, '默认');
INSERT INTO `order_goods` VALUES (139, 201812130313133402, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 15, '2018-12-13 00:00:00', '2018-12-13 00:00:00', NULL, '默认');
INSERT INTO `order_goods` VALUES (140, 201812130321166480, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 15, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (141, 201812130325478574, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 1, 0.00, 10.00, 15, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (142, 201812130402271473, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 23, '2018-12-13 00:00:00', '2018-12-13 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (143, 201812130639557719, 4, '/uploads/goods/217c2ddaa6e520cc5915e1e6ecd004e6.jpg', '现货 蒙自石榴水果 新鲜包邮 软籽薄皮多汁 云南甜石榴', 99.00, 990, NULL, 1, 0.00, 99.00, 8, '2018-12-13 00:00:00', '2018-12-13 00:00:00', NULL, '默认');
INSERT INTO `order_goods` VALUES (144, 201812170620062186, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 1, 0.00, 10.00, 27, '2018-12-17 00:00:00', '2018-12-17 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (145, 201812180137125921, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 5, 0.00, 10.00, 26, '2018-12-18 00:00:00', '2018-12-18 00:00:00', NULL, '默认');
INSERT INTO `order_goods` VALUES (146, 201812180137125921, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 5, 0.00, 10.00, 26, '2018-12-18 00:00:00', '2018-12-18 00:00:00', NULL, '默认');
INSERT INTO `order_goods` VALUES (147, 201812180239133591, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 8, 0.00, 668.00, 26, '2018-12-18 00:00:00', '2018-12-18 00:00:00', NULL, '默认');
INSERT INTO `order_goods` VALUES (148, 201812190351158057, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 29, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (149, 201812190352371109, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 29, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (150, 201812190529492409, 8, '/uploads/goods/ea7b1be14b9c10051e453a4c804214a4.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 8.00, 0, NULL, 5, 0.00, 8.00, 26, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 8, '350ml');
INSERT INTO `order_goods` VALUES (151, 201812190529492409, 8, '/uploads/goods/6f131307c700fe0a1da03d8e5f0d0dbb.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 4.00, 0, NULL, 4, 0.00, 4.00, 26, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 9, '150ml');
INSERT INTO `order_goods` VALUES (152, 201812190538407983, 8, '/uploads/goods/ea7b1be14b9c10051e453a4c804214a4.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 8.00, 0, NULL, 1, 0.00, 8.00, 30, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 8, '350ml');
INSERT INTO `order_goods` VALUES (153, 201812190538407983, 8, '/uploads/goods/6f131307c700fe0a1da03d8e5f0d0dbb.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 4.00, 0, NULL, 2, 0.00, 4.00, 30, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 9, '150ml');
INSERT INTO `order_goods` VALUES (154, 201812190606345339, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 3, 0.00, 10.00, 27, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (155, 201812190607193006, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 3, 0.00, 10.00, 26, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (156, 201812190610315685, 8, '/uploads/goods/6f131307c700fe0a1da03d8e5f0d0dbb.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 4.00, 0, NULL, 3, 0.00, 4.00, 26, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 9, '150ml');
INSERT INTO `order_goods` VALUES (157, 201812190610559323, 8, '/uploads/goods/6f131307c700fe0a1da03d8e5f0d0dbb.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 4.00, 0, NULL, 3, 0.00, 4.00, 27, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 9, '150ml');
INSERT INTO `order_goods` VALUES (158, 201812190615255643, 8, '/uploads/goods/ea7b1be14b9c10051e453a4c804214a4.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 8.00, 0, NULL, 2, 0.00, 8.00, 27, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 8, '350ml');
INSERT INTO `order_goods` VALUES (159, 201812190616313355, 8, '/uploads/goods/9c14ef8a29c8788a3515446ee38c0b6d.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 0, NULL, 1, 0.00, 10.00, 27, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 7, '500ml');
INSERT INTO `order_goods` VALUES (160, 201812190648026391, 8, '/uploads/goods/6f131307c700fe0a1da03d8e5f0d0dbb.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 4.00, 0, NULL, 1, 0.00, 4.00, 27, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 9, '150ml');
INSERT INTO `order_goods` VALUES (161, 201812190720281738, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 27, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (162, 201812190720298943, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 27, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (163, 201812190728562735, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -100, NULL, 2, 0.00, 0.00, 27, '2018-12-19 00:00:00', '2018-12-19 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (164, 201812200123389477, 8, '/uploads/goods/9c14ef8a29c8788a3515446ee38c0b6d.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 0, NULL, 1, 0.00, 10.00, 28, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 7, '500ml');
INSERT INTO `order_goods` VALUES (165, 201812200124489412, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 28, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (166, 201812200127563517, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 28, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (167, 201812200129033474, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 28, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (168, 201812200131082692, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 28, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (169, 201812200141044569, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 32, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (170, 201812200147004774, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 33, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (171, 201812200150150014, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 34, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (172, 201812200153526390, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 35, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (173, 201812200240523655, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 6, 0.00, 0.01, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (174, 201812200240534504, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 6, 0.00, 0.01, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (175, 201812200248315083, 12, '/uploads/goods/a6f607a7c3dcc7b5f45d3181af48e112.png', '测试商品', 100.00, 1, NULL, 5, 0.00, 100.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (176, 201812200308052528, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 36, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (177, 201812200308196237, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 8, 0.00, 0.01, 36, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (178, 201812200311273902, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 36, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (179, 201812200311583811, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 90, 0.00, 0.01, 36, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (180, 201812200315207111, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 36, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (181, 201812200328109917, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, -1, 0.00, 0.01, 26, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (182, 201812200329392231, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -300, NULL, 6, 0.00, 0.00, 36, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (183, 201812200350537345, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 26, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (184, 201812200430000070, 8, '/uploads/goods/9c14ef8a29c8788a3515446ee38c0b6d.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 0, NULL, 2, 0.00, 10.00, 26, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 7, '500ml');
INSERT INTO `order_goods` VALUES (185, 201812200447511702, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 26, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (186, 201812200449023559, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 26, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (187, 201812200538012611, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -100, NULL, 2, 0.00, 0.00, 26, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (188, 201812200553367931, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 37, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (189, 201812200559486654, 10, '/uploads/goods/c40e7d1aee51ddb85ef8f28466972544.jpg', '贵州茅台 汉酱51度500mL单瓶装酱香型白酒 苏宁易购自营', 699.00, 6990, NULL, 11, 0.00, 699.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (190, 201812200600269217, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 2, 0.00, 668.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (191, 201812200605414842, 15, '/uploads/goods/bffa19ebd09ec96684e2a9b0e905f7d3.jpg', '测试商品', 10.00, -3, NULL, 9, 0.00, 10.00, 36, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (192, 201812200605427889, 14, '/uploads/goods/de88d2d341e775df8211de56dea9aa0e.jpg', '阿胶', 125.00, NULL, NULL, 2, 0.00, 125.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (193, 201812200606470446, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 28, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (194, 201812200608545440, 8, '/uploads/goods/6f131307c700fe0a1da03d8e5f0d0dbb.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 4.00, 0, NULL, 1, 0.00, 4.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 9, '150ml');
INSERT INTO `order_goods` VALUES (195, 201812200610410776, 12, '/uploads/goods/a6f607a7c3dcc7b5f45d3181af48e112.png', '测试商品', 100.00, 1, NULL, 4, 0.00, 100.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (196, 201812200610410776, 10, '/uploads/goods/c40e7d1aee51ddb85ef8f28466972544.jpg', '贵州茅台 汉酱51度500mL单瓶装酱香型白酒 苏宁易购自营', 699.00, 6990, NULL, 2, 0.00, 699.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (197, 201812200610410776, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (198, 201812200610410776, 8, '/uploads/goods/6f131307c700fe0a1da03d8e5f0d0dbb.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 4.00, 0, NULL, 1, 0.00, 4.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 9, '150ml');
INSERT INTO `order_goods` VALUES (199, 201812200613437283, 8, '/uploads/goods/6f131307c700fe0a1da03d8e5f0d0dbb.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 4.00, 0, NULL, 4, 0.00, 4.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 9, '150ml');
INSERT INTO `order_goods` VALUES (200, 201812200613437283, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 3, 0.00, 168.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (201, 201812200613437283, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (202, 201812200613437283, 10, '/uploads/goods/c40e7d1aee51ddb85ef8f28466972544.jpg', '贵州茅台 汉酱51度500mL单瓶装酱香型白酒 苏宁易购自营', 699.00, 6990, NULL, 2, 0.00, 699.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (203, 201812200615010446, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (204, 201812200615010446, 8, '/uploads/goods/6f131307c700fe0a1da03d8e5f0d0dbb.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 4.00, 0, NULL, 4, 0.00, 4.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 9, '150ml');
INSERT INTO `order_goods` VALUES (205, 201812200616206855, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 4, 0.00, 668.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (206, 201812200616206855, 8, '/uploads/goods/ea7b1be14b9c10051e453a4c804214a4.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 8.00, 0, NULL, 6, 0.00, 8.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 8, '350ml');
INSERT INTO `order_goods` VALUES (207, 201812200617193930, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 26, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (208, 201812200619549724, 11, '/uploads/goods/ddf5872542bb29833923ff5e1fed93e5.jpg', '测试商品', 0.01, 1, NULL, 2, 0.00, 0.01, 29, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (209, 201812200621092138, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 36, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (210, 201812200626166408, 8, '/uploads/goods/ea7b1be14b9c10051e453a4c804214a4.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 8.00, 0, NULL, 1, 0.00, 8.00, 36, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 8, '350ml');
INSERT INTO `order_goods` VALUES (211, 201812200629241173, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 36, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (212, 201812200635557383, 15, '/uploads/goods/bffa19ebd09ec96684e2a9b0e905f7d3.jpg', '测试商品', 10.00, -3, NULL, 1, 0.00, 10.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (213, 201812200636052852, 14, '/uploads/goods/de88d2d341e775df8211de56dea9aa0e.jpg', '阿胶', 125.00, NULL, NULL, 1, 0.00, 125.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (214, 201812200656007839, 2, '/uploads/goods/e58fd59b830b2e581363bb11231f4c53.png', '干果', 30.00, 1500, NULL, 1, 0.00, 30.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (215, 201812200656308631, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (216, 201812200656305883, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (217, 201812200657279713, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 26, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (218, 201812200700144351, 14, '/uploads/goods/de88d2d341e775df8211de56dea9aa0e.jpg', '阿胶', 125.00, NULL, NULL, 1, 0.00, 125.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (219, 201812200701002020, 15, '/uploads/goods/bffa19ebd09ec96684e2a9b0e905f7d3.jpg', '测试商品', 10.00, -3, NULL, 1, 0.00, 10.00, 26, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (220, 201812200706507117, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 36, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (221, 201812200707253212, 14, '/uploads/goods/de88d2d341e775df8211de56dea9aa0e.jpg', '阿胶', 125.00, NULL, NULL, 1, 0.00, 125.00, 26, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (222, 201812200707511300, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 36, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (223, 201812200707562519, 10, '/uploads/goods/c40e7d1aee51ddb85ef8f28466972544.jpg', '贵州茅台 汉酱51度500mL单瓶装酱香型白酒 苏宁易购自营', 699.00, 6990, NULL, 1, 0.00, 699.00, 38, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (224, 201812200708158356, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 36, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (225, 201812200708338836, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 46, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (226, 201812200711429546, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 36, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (227, 201812200714442500, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 46, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (228, 201812200717100248, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 26, '2018-12-20 00:00:00', '2018-12-20 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (229, 201812210242184056, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -150, NULL, 3, 0.00, 0.00, 36, '2018-12-21 00:00:00', '2018-12-21 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (230, 201812210243507855, 14, '/uploads/goods/de88d2d341e775df8211de56dea9aa0e.jpg', '阿胶', 125.00, NULL, NULL, 1, 0.00, 125.00, 38, '2018-12-21 00:00:00', '2018-12-21 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (231, 201812210245386331, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 10, 0.00, 0.01, 38, '2018-12-21 00:00:00', '2018-12-21 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (232, 201812210249180575, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -150, NULL, 3, 0.00, 0.00, 36, '2018-12-21 00:00:00', '2018-12-21 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (233, 201812240901556007, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -50, NULL, 1, 0.00, 0.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (234, 201812240937485763, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -150, NULL, 3, 0.00, 0.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (235, 201812241020380227, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -50, NULL, 1, 0.00, 0.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (236, 201812241112040173, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, 0, NULL, 0, 0.00, 0.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (237, 201812241124572793, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -50, NULL, 1, 0.00, 0.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (238, 201812241130504723, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -50, NULL, 1, 0.00, 0.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (239, 201812241147102735, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -50, NULL, 1, 0.00, 0.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (240, 201812241147219591, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -50, NULL, 1, 0.00, 0.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (241, 201812241147316179, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -100, NULL, 2, 0.00, 0.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (242, 201812241150121733, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -50, NULL, 1, 0.00, 0.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (243, 201812241150251201, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -100, NULL, 2, 0.00, 0.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (244, 201812241152530554, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -50, NULL, 1, 0.00, 0.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (245, 201812241202039217, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -100, NULL, 2, 0.00, 0.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (246, 201812240108238426, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (247, 201812240108350348, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 3, 0.00, 668.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (248, 201812240109497552, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 3, 0.00, 668.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (249, 201812240110369268, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -100, NULL, 2, 0.00, 0.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (250, 201812240210358472, 8, '/uploads/goods/6f131307c700fe0a1da03d8e5f0d0dbb.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 4.00, 0, NULL, 1, 0.00, 4.00, 49, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 9, '150ml');
INSERT INTO `order_goods` VALUES (251, 201812240213083286, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 49, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (252, 201812240214197957, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 49, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (253, 201812240216074811, 11, '/uploads/goods/ddf5872542bb29833923ff5e1fed93e5.jpg', '测试商品', 0.01, 1, NULL, 2, 0.00, 0.01, 29, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (254, 201812240216188112, 11, '/uploads/goods/ddf5872542bb29833923ff5e1fed93e5.jpg', '测试商品', 0.01, 1, NULL, 1, 0.00, 0.01, 29, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (255, 201812240218449174, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -50, NULL, 1, 0.00, 0.00, 49, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (256, 201812240220211454, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 29, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (257, 201812240220477684, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 29, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (258, 201812240221255700, 3, '', '精品包装18个装苹果', 3.00, 0, NULL, 1, 0.00, 3.00, 29, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 11, '红色, 小');
INSERT INTO `order_goods` VALUES (259, 201812240222012298, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 29, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (260, 201812240222029095, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 46, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (261, 201812240222373749, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (262, 201812240222550460, 11, '/uploads/goods/ddf5872542bb29833923ff5e1fed93e5.jpg', '测试商品', 0.01, 1, NULL, 1, 0.00, 0.01, 29, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (263, 201812240225005359, 14, '/uploads/goods/de88d2d341e775df8211de56dea9aa0e.jpg', '阿胶', 125.00, NULL, NULL, 1, 0.00, 125.00, 29, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (264, 201812240225346853, 10, '/uploads/goods/c40e7d1aee51ddb85ef8f28466972544.jpg', '贵州茅台 汉酱51度500mL单瓶装酱香型白酒 苏宁易购自营', 699.00, 6990, NULL, 1, 0.00, 699.00, 29, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (265, 201812240226290633, 12, '/uploads/goods/a6f607a7c3dcc7b5f45d3181af48e112.png', '测试商品', 100.00, 1, NULL, 1, 0.00, 100.00, 29, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (266, 201812240226550869, 12, '/uploads/goods/a6f607a7c3dcc7b5f45d3181af48e112.png', '测试商品', 100.00, 1, NULL, 1, 0.00, 100.00, 29, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (267, 201812240438376830, 16, '/uploads/goods/08d94ada544ca7cdf40453d3eed23257.jpg', '测试1', 18.00, 10, NULL, 8, 0.00, 18.00, 49, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (268, 201812240511143356, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 36, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (269, 201812240514302426, 16, '/uploads/goods/08d94ada544ca7cdf40453d3eed23257.jpg', '测试1', 18.00, 10, NULL, 10, 0.00, 18.00, 38, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (270, 201812240517155259, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -100, NULL, 2, 0.00, 0.00, 36, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (271, 201812240518411865, 16, '/uploads/goods/08d94ada544ca7cdf40453d3eed23257.jpg', '测试1', 18.00, 10, NULL, 11, 0.00, 18.00, 36, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (272, 201812240520169473, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 49, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (273, 201812240521024936, 16, '/uploads/goods/08d94ada544ca7cdf40453d3eed23257.jpg', '测试1', 18.00, 10, NULL, 10, 0.00, 18.00, 49, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (274, 201812240526092348, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -50, NULL, 1, 0.00, 0.00, 49, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (275, 201812240528454546, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 52, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (276, 201812240529331693, 16, '/uploads/goods/08d94ada544ca7cdf40453d3eed23257.jpg', '测试1', 18.00, 10, NULL, 12, 0.00, 18.00, 38, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (277, 201812240540357693, 8, '/uploads/goods/6f131307c700fe0a1da03d8e5f0d0dbb.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 4.00, 0, NULL, 20, 0.00, 4.00, 38, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 9, '150ml');
INSERT INTO `order_goods` VALUES (278, 201812240547530877, 1, '/uploads/goods/c421ce9cb33c34b353b9e5305888f64b.jpg', '好吃的苹果~~积分商品', 0.00, -250, NULL, 5, 0.00, 0.00, 38, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (279, 201812240604303665, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 52, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (280, 201812240604531378, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 52, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (281, 201812240606553525, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 52, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (282, 201812240613308820, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 52, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (283, 201812240613308820, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 52, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (284, 201812240726587370, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 26, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (285, 201812240838590735, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 32, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (286, 201812240838590735, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 32, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (287, 201812240841126367, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 32, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (288, 201812240841126367, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 1, 0.00, 10.00, 32, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (289, 201812240842266940, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 32, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (290, 201812240843140819, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 32, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (291, 201812240843293800, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 32, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (292, 201812240847219638, 21, '/uploads/goods/b7ebb898bbcb93f3413db4b9dab0dc2e.jpg', '芒果', 0.00, -2400, NULL, 3, 0.00, 0.00, 38, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (293, 201812240848329627, 15, '/uploads/goods/bffa19ebd09ec96684e2a9b0e905f7d3.jpg', '测试商品', 10.00, -3, NULL, 1, 0.00, 10.00, 38, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (294, 201812240851536460, 8, '/uploads/goods/9c14ef8a29c8788a3515446ee38c0b6d.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 0, NULL, 1, 0.00, 10.00, 38, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 7, '500ml');
INSERT INTO `order_goods` VALUES (295, 201812240859439290, 7, '/uploads/goods/c7425af81535e7f9f6046c815159c587.jpg', 'Moutai/茅台迎宾53度茅台迎宾酒500ml 酒厂直供', 668.00, 6680, NULL, 1, 0.00, 668.00, 49, '2018-12-24 00:00:00', '2018-12-24 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (296, 201812250700168520, 8, '/uploads/goods/6f131307c700fe0a1da03d8e5f0d0dbb.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 4.00, 0, NULL, 1, 0.00, 4.00, 55, '2018-12-25 00:00:00', '2018-12-25 00:00:00', 9, '150ml');
INSERT INTO `order_goods` VALUES (297, 201812251019438798, 8, '/uploads/goods/059e2e8aa5cf02d0f39950d546ff6d51.jpg', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 90, NULL, 1, 0.00, 10.00, 28, '2018-12-25 00:00:00', '2018-12-25 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (298, 201812251020427472, 9, '/uploads/goods/a73e4c1014476351a9c7ca4254e03731.jpg', '俄罗斯进口沙皇伏特加Vodka 洋酒烈酒鸡尾酒基酒500ml', 168.00, 1680, NULL, 1, 0.00, 168.00, 28, '2018-12-25 00:00:00', '2018-12-25 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (299, 201812251020427472, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 28, '2018-12-25 00:00:00', '2018-12-25 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (300, 201812251045431486, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 17, '2018-12-25 00:00:00', '2018-12-25 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (301, 201812251052180465, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 1, 0.00, 0.01, 16, '2018-12-25 00:00:00', '2018-12-25 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (302, 201812251052276859, 5, '/uploads/goods/f6a49219be69293c97c483801187578d.jpg', '黄桃新鲜水果当季10批发包邮整箱斤现摘现发水蜜桃应季', 0.01, NULL, NULL, 6, 0.00, 0.01, 16, '2018-12-25 00:00:00', '2018-12-25 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (303, 201812251052431093, 8, '/uploads/goods/9c14ef8a29c8788a3515446ee38c0b6d.png', '进口洋酒 baileys 百利甜酒 奶油利口酒力娇酒鸡尾酒女士最爱', 10.00, 0, NULL, 1, 0.00, 10.00, 15, '2018-12-25 00:00:00', '2018-12-25 00:00:00', 7, '500ml');
INSERT INTO `order_goods` VALUES (304, 201812251103175785, 3, '/uploads/goods/6ec50581a3c422c32ef3fba6641307c2.jpg', '精品包装18个装苹果', 0.01, NULL, NULL, 1, 0.00, 0.01, 21, '2018-12-25 00:00:00', '2018-12-25 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (305, 201812290214400940, 1, '/uploads/goods/35ac5c2eeac0aaa4c380c0b86179538d.jpg', '测试1', 0.10, 20, NULL, 1, 0.00, 0.10, 5, '2018-12-29 00:00:00', '2018-12-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (306, 201812290237178427, 1, '/uploads/goods/35ac5c2eeac0aaa4c380c0b86179538d.jpg', '测试1', 0.10, 20, NULL, 1, 0.00, 0.10, 5, '2018-12-29 00:00:00', '2018-12-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (307, 201812290251258487, 2, '/uploads/goods/90c7619f139c802fae3e5427fd992cd7.jpg', '测试2', 120.00, 10, NULL, 1, 0.00, 120.00, 5, '2018-12-29 00:00:00', '2018-12-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (308, 201812290252556768, 11, '/uploads/goods/9c7eed052f7d8c8ba4b998b16e5d7bd9.jpg', '测试1', 0.00, -40, NULL, 1, 0.00, 0.00, 5, '2018-12-29 00:00:00', '2018-12-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (309, 201812290337421617, 8, '/uploads/goods/87ac1b898c9075e859145a38dfe5cfc8.jpg', '测试8', 45.00, 30, NULL, 5, 0.00, 45.00, 6, '2018-12-29 00:00:00', '2018-12-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (310, 201812290339274169, 8, '/uploads/goods/87ac1b898c9075e859145a38dfe5cfc8.jpg', '测试8', 45.00, 30, NULL, 3, 0.00, 45.00, 6, '2018-12-29 00:00:00', '2018-12-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (311, 201812290345125552, 9, '/uploads/goods/1cbcd41116d4d809240e444436aa2316.jpg', '测试9', 15.00, 5, NULL, 2, 0.00, 15.00, 6, '2018-12-29 00:00:00', '2018-12-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (312, 201812290347043217, 5, '/uploads/goods/eb883b6fa5c9b5c08543fa5657f05f32.jpg', '测试5', 65.00, 0, NULL, 1, 0.00, 65.00, 12, '2018-12-29 00:00:00', '2018-12-29 00:00:00', 1, '200g');
INSERT INTO `order_goods` VALUES (313, 201812290354107390, 1, '/uploads/goods/35ac5c2eeac0aaa4c380c0b86179538d.jpg', '测试1', 0.10, 20, NULL, 1, 0.00, 0.10, 5, '2018-12-29 00:00:00', '2018-12-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (314, 201812290354185532, 1, '/uploads/goods/35ac5c2eeac0aaa4c380c0b86179538d.jpg', '测试1', 0.10, 20, NULL, 1, 0.00, 0.10, 5, '2018-12-29 00:00:00', '2018-12-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (315, 201812290355469836, 3, '/uploads/goods/74d6f415a8c52ef5db6a8b761b8e4496.jpg', '测试3', 189.00, 15, NULL, 4, 0.00, 189.00, 5, '2018-12-29 00:00:00', '2018-12-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (316, 201812290403195780, 5, '/uploads/goods/e5daba9641e4c659a86898c341f12f65.jpg', '测试5', 95.00, 0, NULL, 7, 0.00, 95.00, 6, '2018-12-29 00:00:00', '2018-12-29 00:00:00', 2, '500g');
INSERT INTO `order_goods` VALUES (317, 201812290405135869, 7, '/uploads/goods/888da5f4fd27ef9424dd0a80a7254232.jpg', '测试7', 100.00, 50, NULL, 1, 0.00, 100.00, 6, '2018-12-29 00:00:00', '2018-12-29 00:00:00', 0, '默认');
INSERT INTO `order_goods` VALUES (318, 201901160440569196, 1, '/uploads/goods/35ac5c2eeac0aaa4c380c0b86179538d.jpg', '测试1', 0.10, 20, NULL, 4, 0.00, 0.10, 7, '2019-01-16 00:00:00', '2019-01-16 00:00:00', 0, '默认');

-- ----------------------------
-- Table structure for order_reback
-- ----------------------------
DROP TABLE IF EXISTS `order_reback`;
CREATE TABLE `order_reback`  (
  `orderid` bigint(20) NOT NULL DEFAULT 0 COMMENT '订单编号',
  `pay_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付单号',
  `reback_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付单号',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '状态',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '订单完成时间',
  PRIMARY KEY (`orderid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_reback
-- ----------------------------
INSERT INTO `order_reback` VALUES (0, NULL, NULL, 0, '2018-11-30 19:06:07');
INSERT INTO `order_reback` VALUES (201811300538269378, 'sn201811300610496454', 're201811300659443064', 0, '2018-12-03 09:15:56');
INSERT INTO `order_reback` VALUES (201812030911137030, 'sn201812030913208835', 're201812030915293373', 0, '2018-12-03 09:17:28');
INSERT INTO `order_reback` VALUES (201812030921441442, 'sn201812030922403582', 're201812030924372610', 0, '2018-12-03 09:24:37');
INSERT INTO `order_reback` VALUES (201812030926303495, 'sn201812030927186858', 're201812030928255495', 0, '2018-12-03 09:28:25');
INSERT INTO `order_reback` VALUES (201812200350537345, 'sn201812200351047478', 're201812200351330907', 0, '2018-12-20 15:51:33');
INSERT INTO `order_reback` VALUES (201812240606553525, 'sn201812240607451524', 're201812240610554635', 0, '2018-12-24 18:10:55');
INSERT INTO `order_reback` VALUES (201812240842266940, 'sn201812240842368667', 're201812240843013014', 0, '2018-12-24 20:43:01');
INSERT INTO `order_reback` VALUES (201812251052180465, 'sn201812251056501919', 're201812251058234062', 0, '2018-12-25 10:58:23');

-- ----------------------------
-- Table structure for pay_sn_order_relation
-- ----------------------------
DROP TABLE IF EXISTS `pay_sn_order_relation`;
CREATE TABLE `pay_sn_order_relation`  (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pay_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '支付单号',
  `orderid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单号',
  `member_id` int(20) NOT NULL COMMENT '会员ID',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  `pay_amount` decimal(12, 2) NULL DEFAULT NULL COMMENT '支付金额',
  `create_time` datetime(0) NOT NULL COMMENT '添加时间',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单类型',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `pay_sn`(`pay_sn`, `orderid`) USING BTREE,
  INDEX `pay_sn_2`(`pay_sn`) USING BTREE,
  INDEX `orderid`(`orderid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单支付关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付代码名称',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付名称',
  `payment_config` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '支付接口配置信息',
  `state` int(11) NULL DEFAULT 0 COMMENT '接口状态0禁用1启用',
  `createTime` datetime(0) NULL DEFAULT NULL,
  `modifyTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of payment
-- ----------------------------
INSERT INTO `payment` VALUES (1, 'wechat', '微信支付', NULL, 1, NULL, NULL);

-- ----------------------------
-- Table structure for postmethod
-- ----------------------------
DROP TABLE IF EXISTS `postmethod`;
CREATE TABLE `postmethod`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模版名称',
  `expressid` bigint(20) NULL DEFAULT NULL COMMENT '物流公司id',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物流公司编号',
  `type` int(1) NULL DEFAULT NULL COMMENT '1 重量计费 2按件计费',
  `first_condition` decimal(10, 1) NULL DEFAULT 0.0 COMMENT '首重',
  `first_money` decimal(10, 2) NULL DEFAULT 0.00,
  `other_condition` decimal(10, 1) NULL DEFAULT 0.0,
  `other_money` decimal(10, 2) NULL DEFAULT 0.00,
  `status` smallint(1) NULL DEFAULT 0,
  `createTime` datetime(0) NULL DEFAULT NULL,
  `send_area` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配送区域',
  `not_area` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '不配送区域',
  `is_default` smallint(1) NULL DEFAULT 0 COMMENT '默认',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for provinces
-- ----------------------------
DROP TABLE IF EXISTS `provinces`;
CREATE TABLE `provinces`  (
  `id` bigint(20) NOT NULL DEFAULT 0,
  `cname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `upid` bigint(20) NULL DEFAULT NULL,
  `ename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pinyin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `level` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of provinces
-- ----------------------------
INSERT INTO `provinces` VALUES (1, '全国', 0, NULL, NULL, 0);
INSERT INTO `provinces` VALUES (110000, '北京市', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (110100, '北京市', 110000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (110101, '东城区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110102, '西城区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110105, '朝阳区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110106, '丰台区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110107, '石景山区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110108, '海淀区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110109, '门头沟区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110111, '房山区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110112, '通州区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110113, '顺义区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110114, '昌平区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110115, '大兴区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110116, '怀柔区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110117, '平谷区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110118, '密云区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (110119, '延庆区', 110100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120000, '天津市', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (120100, '天津市', 120000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (120101, '和平区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120102, '河东区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120103, '河西区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120104, '南开区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120105, '河北区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120106, '红桥区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120110, '东丽区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120111, '西青区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120112, '津南区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120113, '北辰区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120114, '武清区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120115, '宝坻区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120116, '滨海新区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120117, '宁河区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120118, '静海区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (120119, '蓟州区', 120100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130000, '河北省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (130100, '石家庄市', 130000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (130102, '长安区', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130104, '桥西区', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130105, '新华区', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130107, '井陉矿区', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130108, '裕华区', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130109, '藁城区', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130110, '鹿泉区', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130111, '栾城区', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130121, '井陉县', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130123, '正定县', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130125, '行唐县', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130126, '灵寿县', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130127, '高邑县', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130128, '深泽县', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130129, '赞皇县', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130130, '无极县', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130131, '平山县', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130132, '元氏县', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130133, '赵县', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130181, '辛集市', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130183, '晋州市', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130184, '新乐市', 130100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130200, '唐山市', 130000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (130202, '路南区', 130200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130203, '路北区', 130200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130204, '古冶区', 130200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130205, '开平区', 130200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130207, '丰南区', 130200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130208, '丰润区', 130200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130209, '曹妃甸区', 130200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130223, '滦县', 130200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130224, '滦南县', 130200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130225, '乐亭县', 130200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130227, '迁西县', 130200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130229, '玉田县', 130200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130281, '遵化市', 130200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130283, '迁安市', 130200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130300, '秦皇岛市', 130000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (130302, '海港区', 130300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130303, '山海关区', 130300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130304, '北戴河区', 130300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130306, '抚宁区', 130300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130321, '青龙满族自治县', 130300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130322, '昌黎县', 130300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130324, '卢龙县', 130300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130400, '邯郸市', 130000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (130402, '邯山区', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130403, '丛台区', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130404, '复兴区', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130406, '峰峰矿区', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130407, '肥乡区', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130408, '永年区', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130423, '临漳县', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130424, '成安县', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130425, '大名县', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130426, '涉县', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130427, '磁县', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130430, '邱县', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130431, '鸡泽县', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130432, '广平县', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130433, '馆陶县', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130434, '魏县', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130435, '曲周县', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130481, '武安市', 130400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130500, '邢台市', 130000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (130502, '桥东区', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130503, '桥西区', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130521, '邢台县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130522, '临城县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130523, '内丘县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130524, '柏乡县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130525, '隆尧县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130526, '任县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130527, '南和县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130528, '宁晋县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130529, '巨鹿县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130530, '新河县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130531, '广宗县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130532, '平乡县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130533, '威县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130534, '清河县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130535, '临西县', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130581, '南宫市', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130582, '沙河市', 130500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130600, '保定市', 130000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (130602, '竞秀区', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130606, '莲池区', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130607, '满城区', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130608, '清苑区', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130609, '徐水区', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130623, '涞水县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130624, '阜平县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130626, '定兴县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130627, '唐县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130628, '高阳县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130629, '容城县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130630, '涞源县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130631, '望都县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130632, '安新县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130633, '易县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130634, '曲阳县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130635, '蠡县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130636, '顺平县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130637, '博野县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130638, '雄县', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130681, '涿州市', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130682, '定州市', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130683, '安国市', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130684, '高碑店市', 130600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130700, '张家口市', 130000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (130702, '桥东区', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130703, '桥西区', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130705, '宣化区', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130706, '下花园区', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130708, '万全区', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130709, '崇礼区', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130722, '张北县', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130723, '康保县', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130724, '沽源县', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130725, '尚义县', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130726, '蔚县', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130727, '阳原县', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130728, '怀安县', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130730, '怀来县', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130731, '涿鹿县', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130732, '赤城县', 130700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130800, '承德市', 130000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (130802, '双桥区', 130800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130803, '双滦区', 130800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130804, '鹰手营子矿区', 130800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130821, '承德县', 130800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130822, '兴隆县', 130800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130824, '滦平县', 130800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130825, '隆化县', 130800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130826, '丰宁满族自治县', 130800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130827, '宽城满族自治县', 130800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130828, '围场满族蒙古族自治县', 130800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130881, '平泉市', 130800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130900, '沧州市', 130000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (130902, '新华区', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130903, '运河区', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130921, '沧县', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130922, '青县', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130923, '东光县', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130924, '海兴县', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130925, '盐山县', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130926, '肃宁县', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130927, '南皮县', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130928, '吴桥县', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130929, '献县', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130930, '孟村回族自治县', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130981, '泊头市', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130982, '任丘市', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130983, '黄骅市', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (130984, '河间市', 130900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131000, '廊坊市', 130000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (131002, '安次区', 131000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131003, '广阳区', 131000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131022, '固安县', 131000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131023, '永清县', 131000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131024, '香河县', 131000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131025, '大城县', 131000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131026, '文安县', 131000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131028, '大厂回族自治县', 131000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131081, '霸州市', 131000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131082, '三河市', 131000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131100, '衡水市', 130000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (131102, '桃城区', 131100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131103, '冀州区', 131100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131121, '枣强县', 131100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131122, '武邑县', 131100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131123, '武强县', 131100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131124, '饶阳县', 131100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131125, '安平县', 131100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131126, '故城县', 131100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131127, '景县', 131100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131128, '阜城县', 131100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (131182, '深州市', 131100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140000, '山西省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (140100, '太原市', 140000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (140105, '小店区', 140100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140106, '迎泽区', 140100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140107, '杏花岭区', 140100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140108, '尖草坪区', 140100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140109, '万柏林区', 140100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140110, '晋源区', 140100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140121, '清徐县', 140100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140122, '阳曲县', 140100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140123, '娄烦县', 140100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140181, '古交市', 140100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140200, '大同市', 140000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (140202, '城区', 140200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140203, '矿区', 140200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140211, '南郊区', 140200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140212, '新荣区', 140200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140221, '阳高县', 140200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140222, '天镇县', 140200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140223, '广灵县', 140200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140224, '灵丘县', 140200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140225, '浑源县', 140200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140226, '左云县', 140200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140227, '大同县', 140200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140300, '阳泉市', 140000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (140302, '城区', 140300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140303, '矿区', 140300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140311, '郊区', 140300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140321, '平定县', 140300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140322, '盂县', 140300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140400, '长治市', 140000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (140402, '城区', 140400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140411, '郊区', 140400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140421, '长治县', 140400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140423, '襄垣县', 140400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140424, '屯留县', 140400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140425, '平顺县', 140400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140426, '黎城县', 140400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140427, '壶关县', 140400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140428, '长子县', 140400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140429, '武乡县', 140400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140430, '沁县', 140400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140431, '沁源县', 140400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140481, '潞城市', 140400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140500, '晋城市', 140000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (140502, '城区', 140500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140521, '沁水县', 140500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140522, '阳城县', 140500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140524, '陵川县', 140500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140525, '泽州县', 140500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140581, '高平市', 140500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140600, '朔州市', 140000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (140602, '朔城区', 140600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140603, '平鲁区', 140600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140621, '山阴县', 140600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140622, '应县', 140600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140623, '右玉县', 140600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140624, '怀仁县', 140600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140700, '晋中市', 140000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (140702, '榆次区', 140700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140721, '榆社县', 140700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140722, '左权县', 140700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140723, '和顺县', 140700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140724, '昔阳县', 140700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140725, '寿阳县', 140700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140726, '太谷县', 140700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140727, '祁县', 140700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140728, '平遥县', 140700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140729, '灵石县', 140700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140781, '介休市', 140700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140800, '运城市', 140000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (140802, '盐湖区', 140800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140821, '临猗县', 140800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140822, '万荣县', 140800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140823, '闻喜县', 140800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140824, '稷山县', 140800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140825, '新绛县', 140800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140826, '绛县', 140800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140827, '垣曲县', 140800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140828, '夏县', 140800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140829, '平陆县', 140800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140830, '芮城县', 140800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140881, '永济市', 140800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140882, '河津市', 140800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140900, '忻州市', 140000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (140902, '忻府区', 140900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140921, '定襄县', 140900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140922, '五台县', 140900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140923, '代县', 140900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140924, '繁峙县', 140900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140925, '宁武县', 140900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140926, '静乐县', 140900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140927, '神池县', 140900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140928, '五寨县', 140900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140929, '岢岚县', 140900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140930, '河曲县', 140900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140931, '保德县', 140900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140932, '偏关县', 140900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (140981, '原平市', 140900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141000, '临汾市', 140000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (141002, '尧都区', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141021, '曲沃县', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141022, '翼城县', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141023, '襄汾县', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141024, '洪洞县', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141025, '古县', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141026, '安泽县', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141027, '浮山县', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141028, '吉县', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141029, '乡宁县', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141030, '大宁县', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141031, '隰县', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141032, '永和县', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141033, '蒲县', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141034, '汾西县', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141081, '侯马市', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141082, '霍州市', 141000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141100, '吕梁市', 140000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (141102, '离石区', 141100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141121, '文水县', 141100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141122, '交城县', 141100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141123, '兴县', 141100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141124, '临县', 141100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141125, '柳林县', 141100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141126, '石楼县', 141100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141127, '岚县', 141100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141128, '方山县', 141100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141129, '中阳县', 141100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141130, '交口县', 141100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141181, '孝义市', 141100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (141182, '汾阳市', 141100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150000, '内蒙古自治区', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (150100, '呼和浩特市', 150000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (150102, '新城区', 150100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150103, '回民区', 150100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150104, '玉泉区', 150100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150105, '赛罕区', 150100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150121, '土默特左旗', 150100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150122, '托克托县', 150100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150123, '和林格尔县', 150100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150124, '清水河县', 150100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150125, '武川县', 150100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150200, '包头市', 150000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (150202, '东河区', 150200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150203, '昆都仑区', 150200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150204, '青山区', 150200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150205, '石拐区', 150200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150206, '白云鄂博矿区', 150200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150207, '九原区', 150200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150221, '土默特右旗', 150200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150222, '固阳县', 150200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150223, '达尔罕茂明安联合旗', 150200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150300, '乌海市', 150000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (150302, '海勃湾区', 150300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150303, '海南区', 150300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150304, '乌达区', 150300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150400, '赤峰市', 150000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (150402, '红山区', 150400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150403, '元宝山区', 150400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150404, '松山区', 150400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150421, '阿鲁科尔沁旗', 150400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150422, '巴林左旗', 150400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150423, '巴林右旗', 150400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150424, '林西县', 150400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150425, '克什克腾旗', 150400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150426, '翁牛特旗', 150400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150428, '喀喇沁旗', 150400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150429, '宁城县', 150400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150430, '敖汉旗', 150400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150500, '通辽市', 150000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (150502, '科尔沁区', 150500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150521, '科尔沁左翼中旗', 150500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150522, '科尔沁左翼后旗', 150500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150523, '开鲁县', 150500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150524, '库伦旗', 150500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150525, '奈曼旗', 150500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150526, '扎鲁特旗', 150500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150581, '霍林郭勒市', 150500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150600, '鄂尔多斯市', 150000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (150602, '东胜区', 150600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150603, '康巴什区', 150600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150621, '达拉特旗', 150600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150622, '准格尔旗', 150600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150623, '鄂托克前旗', 150600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150624, '鄂托克旗', 150600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150625, '杭锦旗', 150600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150626, '乌审旗', 150600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150627, '伊金霍洛旗', 150600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150700, '呼伦贝尔市', 150000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (150702, '海拉尔区', 150700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150703, '扎赉诺尔区', 150700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150721, '阿荣旗', 150700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150722, '莫力达瓦达斡尔族自治旗', 150700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150723, '鄂伦春自治旗', 150700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150724, '鄂温克族自治旗', 150700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150725, '陈巴尔虎旗', 150700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150726, '新巴尔虎左旗', 150700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150727, '新巴尔虎右旗', 150700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150781, '满洲里市', 150700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150782, '牙克石市', 150700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150783, '扎兰屯市', 150700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150784, '额尔古纳市', 150700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150785, '根河市', 150700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150800, '巴彦淖尔市', 150000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (150802, '临河区', 150800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150821, '五原县', 150800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150822, '磴口县', 150800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150823, '乌拉特前旗', 150800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150824, '乌拉特中旗', 150800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150825, '乌拉特后旗', 150800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150826, '杭锦后旗', 150800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150900, '乌兰察布市', 150000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (150902, '集宁区', 150900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150921, '卓资县', 150900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150922, '化德县', 150900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150923, '商都县', 150900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150924, '兴和县', 150900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150925, '凉城县', 150900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150926, '察哈尔右翼前旗', 150900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150927, '察哈尔右翼中旗', 150900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150928, '察哈尔右翼后旗', 150900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150929, '四子王旗', 150900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (150981, '丰镇市', 150900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152200, '兴安盟', 150000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (152201, '乌兰浩特市', 152200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152202, '阿尔山市', 152200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152221, '科尔沁右翼前旗', 152200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152222, '科尔沁右翼中旗', 152200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152223, '扎赉特旗', 152200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152224, '突泉县', 152200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152500, '锡林郭勒盟', 150000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (152501, '二连浩特市', 152500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152502, '锡林浩特市', 152500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152522, '阿巴嘎旗', 152500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152523, '苏尼特左旗', 152500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152524, '苏尼特右旗', 152500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152525, '东乌珠穆沁旗', 152500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152526, '西乌珠穆沁旗', 152500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152527, '太仆寺旗', 152500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152528, '镶黄旗', 152500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152529, '正镶白旗', 152500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152530, '正蓝旗', 152500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152531, '多伦县', 152500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152900, '阿拉善盟', 150000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (152921, '阿拉善左旗', 152900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152922, '阿拉善右旗', 152900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (152923, '额济纳旗', 152900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210000, '辽宁省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (210100, '沈阳市', 210000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (210102, '和平区', 210100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210103, '沈河区', 210100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210104, '大东区', 210100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210105, '皇姑区', 210100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210106, '铁西区', 210100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210111, '苏家屯区', 210100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210112, '浑南区', 210100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210113, '沈北新区', 210100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210114, '于洪区', 210100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210115, '辽中区', 210100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210123, '康平县', 210100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210124, '法库县', 210100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210181, '新民市', 210100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210200, '大连市', 210000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (210202, '中山区', 210200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210203, '西岗区', 210200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210204, '沙河口区', 210200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210211, '甘井子区', 210200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210212, '旅顺口区', 210200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210213, '金州区', 210200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210214, '普兰店区', 210200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210224, '长海县', 210200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210281, '瓦房店市', 210200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210283, '庄河市', 210200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210300, '鞍山市', 210000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (210302, '铁东区', 210300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210303, '铁西区', 210300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210304, '立山区', 210300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210311, '千山区', 210300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210321, '台安县', 210300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210323, '岫岩满族自治县', 210300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210381, '海城市', 210300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210400, '抚顺市', 210000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (210402, '新抚区', 210400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210403, '东洲区', 210400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210404, '望花区', 210400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210411, '顺城区', 210400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210421, '抚顺县', 210400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210422, '新宾满族自治县', 210400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210423, '清原满族自治县', 210400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210500, '本溪市', 210000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (210502, '平山区', 210500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210503, '溪湖区', 210500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210504, '明山区', 210500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210505, '南芬区', 210500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210521, '本溪满族自治县', 210500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210522, '桓仁满族自治县', 210500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210600, '丹东市', 210000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (210602, '元宝区', 210600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210603, '振兴区', 210600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210604, '振安区', 210600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210624, '宽甸满族自治县', 210600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210681, '东港市', 210600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210682, '凤城市', 210600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210700, '锦州市', 210000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (210702, '古塔区', 210700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210703, '凌河区', 210700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210711, '太和区', 210700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210726, '黑山县', 210700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210727, '义县', 210700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210781, '凌海市', 210700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210782, '北镇市', 210700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210800, '营口市', 210000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (210802, '站前区', 210800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210803, '西市区', 210800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210804, '鲅鱼圈区', 210800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210811, '老边区', 210800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210881, '盖州市', 210800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210882, '大石桥市', 210800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210900, '阜新市', 210000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (210902, '海州区', 210900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210903, '新邱区', 210900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210904, '太平区', 210900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210905, '清河门区', 210900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210911, '细河区', 210900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210921, '阜新蒙古族自治县', 210900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (210922, '彰武县', 210900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211000, '辽阳市', 210000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (211002, '白塔区', 211000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211003, '文圣区', 211000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211004, '宏伟区', 211000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211005, '弓长岭区', 211000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211011, '太子河区', 211000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211021, '辽阳县', 211000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211081, '灯塔市', 211000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211100, '盘锦市', 210000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (211102, '双台子区', 211100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211103, '兴隆台区', 211100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211104, '大洼区', 211100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211122, '盘山县', 211100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211200, '铁岭市', 210000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (211202, '银州区', 211200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211204, '清河区', 211200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211221, '铁岭县', 211200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211223, '西丰县', 211200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211224, '昌图县', 211200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211281, '调兵山市', 211200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211282, '开原市', 211200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211300, '朝阳市', 210000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (211302, '双塔区', 211300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211303, '龙城区', 211300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211321, '朝阳县', 211300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211322, '建平县', 211300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211324, '喀喇沁左翼蒙古族自治县', 211300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211381, '北票市', 211300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211382, '凌源市', 211300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211400, '葫芦岛市', 210000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (211402, '连山区', 211400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211403, '龙港区', 211400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211404, '南票区', 211400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211421, '绥中县', 211400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211422, '建昌县', 211400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (211481, '兴城市', 211400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220000, '吉林省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (220100, '长春市', 220000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (220102, '南关区', 220100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220103, '宽城区', 220100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220104, '朝阳区', 220100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220105, '二道区', 220100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220106, '绿园区', 220100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220112, '双阳区', 220100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220113, '九台区', 220100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220122, '农安县', 220100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220182, '榆树市', 220100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220183, '德惠市', 220100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220200, '吉林市', 220000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (220202, '昌邑区', 220200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220203, '龙潭区', 220200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220204, '船营区', 220200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220211, '丰满区', 220200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220221, '永吉县', 220200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220281, '蛟河市', 220200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220282, '桦甸市', 220200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220283, '舒兰市', 220200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220284, '磐石市', 220200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220300, '四平市', 220000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (220302, '铁西区', 220300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220303, '铁东区', 220300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220322, '梨树县', 220300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220323, '伊通满族自治县', 220300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220381, '公主岭市', 220300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220382, '双辽市', 220300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220400, '辽源市', 220000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (220402, '龙山区', 220400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220403, '西安区', 220400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220421, '东丰县', 220400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220422, '东辽县', 220400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220500, '通化市', 220000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (220502, '东昌区', 220500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220503, '二道江区', 220500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220521, '通化县', 220500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220523, '辉南县', 220500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220524, '柳河县', 220500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220581, '梅河口市', 220500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220582, '集安市', 220500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220600, '白山市', 220000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (220602, '浑江区', 220600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220605, '江源区', 220600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220621, '抚松县', 220600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220622, '靖宇县', 220600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220623, '长白朝鲜族自治县', 220600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220681, '临江市', 220600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220700, '松原市', 220000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (220702, '宁江区', 220700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220721, '前郭尔罗斯蒙古族自治县', 220700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220722, '长岭县', 220700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220723, '乾安县', 220700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220781, '扶余市', 220700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220800, '白城市', 220000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (220802, '洮北区', 220800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220821, '镇赉县', 220800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220822, '通榆县', 220800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220881, '洮南市', 220800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (220882, '大安市', 220800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (222400, '延边朝鲜族自治州', 220000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (222401, '延吉市', 222400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (222402, '图们市', 222400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (222403, '敦化市', 222400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (222404, '珲春市', 222400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (222405, '龙井市', 222400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (222406, '和龙市', 222400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (222424, '汪清县', 222400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (222426, '安图县', 222400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230000, '黑龙江省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (230100, '哈尔滨市', 230000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (230102, '道里区', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230103, '南岗区', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230104, '道外区', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230108, '平房区', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230109, '松北区', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230110, '香坊区', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230111, '呼兰区', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230112, '阿城区', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230113, '双城区', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230123, '依兰县', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230124, '方正县', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230125, '宾县', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230126, '巴彦县', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230127, '木兰县', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230128, '通河县', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230129, '延寿县', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230183, '尚志市', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230184, '五常市', 230100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230200, '齐齐哈尔市', 230000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (230202, '龙沙区', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230203, '建华区', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230204, '铁锋区', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230205, '昂昂溪区', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230206, '富拉尔基区', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230207, '碾子山区', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230208, '梅里斯达斡尔族区', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230221, '龙江县', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230223, '依安县', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230224, '泰来县', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230225, '甘南县', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230227, '富裕县', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230229, '克山县', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230230, '克东县', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230231, '拜泉县', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230281, '讷河市', 230200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230300, '鸡西市', 230000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (230302, '鸡冠区', 230300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230303, '恒山区', 230300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230304, '滴道区', 230300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230305, '梨树区', 230300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230306, '城子河区', 230300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230307, '麻山区', 230300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230321, '鸡东县', 230300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230381, '虎林市', 230300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230382, '密山市', 230300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230400, '鹤岗市', 230000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (230402, '向阳区', 230400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230403, '工农区', 230400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230404, '南山区', 230400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230405, '兴安区', 230400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230406, '东山区', 230400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230407, '兴山区', 230400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230421, '萝北县', 230400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230422, '绥滨县', 230400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230500, '双鸭山市', 230000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (230502, '尖山区', 230500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230503, '岭东区', 230500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230505, '四方台区', 230500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230506, '宝山区', 230500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230521, '集贤县', 230500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230522, '友谊县', 230500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230523, '宝清县', 230500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230524, '饶河县', 230500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230600, '大庆市', 230000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (230602, '萨尔图区', 230600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230603, '龙凤区', 230600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230604, '让胡路区', 230600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230605, '红岗区', 230600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230606, '大同区', 230600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230621, '肇州县', 230600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230622, '肇源县', 230600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230623, '林甸县', 230600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230624, '杜尔伯特蒙古族自治县', 230600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230700, '伊春市', 230000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (230702, '伊春区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230703, '南岔区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230704, '友好区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230705, '西林区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230706, '翠峦区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230707, '新青区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230708, '美溪区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230709, '金山屯区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230710, '五营区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230711, '乌马河区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230712, '汤旺河区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230713, '带岭区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230714, '乌伊岭区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230715, '红星区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230716, '上甘岭区', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230722, '嘉荫县', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230781, '铁力市', 230700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230800, '佳木斯市', 230000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (230803, '向阳区', 230800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230804, '前进区', 230800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230805, '东风区', 230800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230811, '郊区', 230800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230822, '桦南县', 230800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230826, '桦川县', 230800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230828, '汤原县', 230800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230881, '同江市', 230800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230882, '富锦市', 230800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230883, '抚远市', 230800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230900, '七台河市', 230000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (230902, '新兴区', 230900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230903, '桃山区', 230900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230904, '茄子河区', 230900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (230921, '勃利县', 230900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231000, '牡丹江市', 230000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (231002, '东安区', 231000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231003, '阳明区', 231000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231004, '爱民区', 231000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231005, '西安区', 231000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231025, '林口县', 231000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231081, '绥芬河市', 231000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231083, '海林市', 231000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231084, '宁安市', 231000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231085, '穆棱市', 231000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231086, '东宁市', 231000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231100, '黑河市', 230000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (231102, '爱辉区', 231100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231121, '嫩江县', 231100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231123, '逊克县', 231100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231124, '孙吴县', 231100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231181, '北安市', 231100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231182, '五大连池市', 231100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231200, '绥化市', 230000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (231202, '北林区', 231200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231221, '望奎县', 231200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231222, '兰西县', 231200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231223, '青冈县', 231200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231224, '庆安县', 231200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231225, '明水县', 231200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231226, '绥棱县', 231200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231281, '安达市', 231200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231282, '肇东市', 231200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (231283, '海伦市', 231200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (232700, '大兴安岭地区', 230000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (232702, '松岭区', 232700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (232721, '呼玛县', 232700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (232722, '塔河县', 232700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (232723, '漠河县', 232700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310000, '上海市', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (310100, '上海市', 310000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (310101, '黄浦区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310104, '徐汇区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310105, '长宁区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310106, '静安区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310107, '普陀区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310109, '虹口区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310110, '杨浦区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310112, '闵行区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310113, '宝山区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310114, '嘉定区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310115, '浦东新区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310116, '金山区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310117, '松江区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310118, '青浦区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310120, '奉贤区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (310151, '崇明区', 310100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320000, '江苏省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (320100, '南京市', 320000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (320102, '玄武区', 320100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320104, '秦淮区', 320100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320105, '建邺区', 320100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320106, '鼓楼区', 320100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320111, '浦口区', 320100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320113, '栖霞区', 320100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320114, '雨花台区', 320100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320115, '江宁区', 320100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320116, '六合区', 320100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320117, '溧水区', 320100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320118, '高淳区', 320100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320200, '无锡市', 320000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (320205, '锡山区', 320200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320206, '惠山区', 320200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320211, '滨湖区', 320200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320213, '梁溪区', 320200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320214, '新吴区', 320200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320281, '江阴市', 320200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320282, '宜兴市', 320200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320300, '徐州市', 320000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (320302, '鼓楼区', 320300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320303, '云龙区', 320300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320305, '贾汪区', 320300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320311, '泉山区', 320300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320312, '铜山区', 320300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320321, '丰县', 320300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320322, '沛县', 320300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320324, '睢宁县', 320300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320381, '新沂市', 320300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320382, '邳州市', 320300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320402, '天宁区', 320400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320404, '钟楼区', 320400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320411, '新北区', 320400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320412, '武进区', 320400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320413, '金坛区', 320400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320481, '溧阳市', 320400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320500, '苏州市', 320000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (320505, '虎丘区', 320500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320506, '吴中区', 320500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320507, '相城区', 320500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320508, '姑苏区', 320500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320509, '吴江区', 320500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320581, '常熟市', 320500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320582, '张家港市', 320500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320583, '昆山市', 320500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320585, '太仓市', 320500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320600, '南通市', 320000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (320602, '崇川区', 320600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320611, '港闸区', 320600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320612, '通州区', 320600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320621, '海安县', 320600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320623, '如东县', 320600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320681, '启东市', 320600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320682, '如皋市', 320600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320684, '海门市', 320600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320700, '连云港市', 320000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (320703, '连云区', 320700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320706, '海州区', 320700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320707, '赣榆区', 320700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320722, '东海县', 320700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320723, '灌云县', 320700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320724, '灌南县', 320700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320800, '淮安市', 320000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (320803, '淮安区', 320800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320804, '淮阴区', 320800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320812, '清江浦区', 320800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320813, '洪泽区', 320800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320826, '涟水县', 320800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320830, '盱眙县', 320800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320831, '金湖县', 320800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320900, '盐城市', 320000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (320902, '亭湖区', 320900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320903, '盐都区', 320900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320904, '大丰区', 320900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320921, '响水县', 320900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320922, '滨海县', 320900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320923, '阜宁县', 320900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320924, '射阳县', 320900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320925, '建湖县', 320900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (320981, '东台市', 320900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321000, '扬州市', 320000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (321002, '广陵区', 321000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321003, '邗江区', 321000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321012, '江都区', 321000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321023, '宝应县', 321000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321081, '仪征市', 321000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321084, '高邮市', 321000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321100, '镇江市', 320000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (321102, '京口区', 321100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321111, '润州区', 321100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321112, '丹徒区', 321100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321181, '丹阳市', 321100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321182, '扬中市', 321100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321183, '句容市', 321100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321200, '泰州市', 320000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (321202, '海陵区', 321200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321203, '高港区', 321200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321204, '姜堰区', 321200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321281, '兴化市', 321200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321282, '靖江市', 321200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321283, '泰兴市', 321200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321300, '宿迁市', 320000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (321302, '宿城区', 321300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321311, '宿豫区', 321300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321322, '沭阳县', 321300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321323, '泗阳县', 321300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (321324, '泗洪县', 321300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330000, '浙江省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (330100, '杭州市', 330000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (330102, '上城区', 330100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330103, '下城区', 330100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330104, '江干区', 330100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330105, '拱墅区', 330100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330106, '西湖区', 330100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330108, '滨江区', 330100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330109, '萧山区', 330100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330110, '余杭区', 330100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330111, '富阳区', 330100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330122, '桐庐县', 330100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330127, '淳安县', 330100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330182, '建德市', 330100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330185, '临安区', 330100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330200, '宁波市', 330000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (330203, '海曙区', 330200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330205, '江北区', 330200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330206, '北仑区', 330200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330211, '镇海区', 330200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330212, '鄞州区', 330200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330213, '奉化区', 330200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330225, '象山县', 330200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330226, '宁海县', 330200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330281, '余姚市', 330200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330282, '慈溪市', 330200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330300, '温州市', 330000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (330302, '鹿城区', 330300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330303, '龙湾区', 330300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330304, '瓯海区', 330300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330305, '洞头区', 330300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330324, '永嘉县', 330300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330326, '平阳县', 330300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330327, '苍南县', 330300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330328, '文成县', 330300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330329, '泰顺县', 330300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330381, '瑞安市', 330300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330382, '乐清市', 330300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330400, '嘉兴市', 330000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (330402, '南湖区', 330400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330411, '秀洲区', 330400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330421, '嘉善县', 330400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330424, '海盐县', 330400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330481, '海宁市', 330400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330482, '平湖市', 330400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330483, '桐乡市', 330400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330500, '湖州市', 330000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (330502, '吴兴区', 330500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330503, '南浔区', 330500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330521, '德清县', 330500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330522, '长兴县', 330500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330523, '安吉县', 330500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330600, '绍兴市', 330000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (330602, '越城区', 330600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330603, '柯桥区', 330600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330604, '上虞区', 330600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330624, '新昌县', 330600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330681, '诸暨市', 330600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330683, '嵊州市', 330600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330700, '金华市', 330000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (330702, '婺城区', 330700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330703, '金东区', 330700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330723, '武义县', 330700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330726, '浦江县', 330700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330727, '磐安县', 330700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330781, '兰溪市', 330700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330782, '义乌市', 330700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330783, '东阳市', 330700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330784, '永康市', 330700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330800, '衢州市', 330000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (330802, '柯城区', 330800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330803, '衢江区', 330800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330822, '常山县', 330800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330824, '开化县', 330800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330825, '龙游县', 330800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330881, '江山市', 330800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330900, '舟山市', 330000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (330902, '定海区', 330900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330903, '普陀区', 330900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330921, '岱山县', 330900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (330922, '嵊泗县', 330900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331000, '台州市', 330000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (331002, '椒江区', 331000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331003, '黄岩区', 331000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331004, '路桥区', 331000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331022, '三门县', 331000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331023, '天台县', 331000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331024, '仙居县', 331000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331081, '温岭市', 331000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331082, '临海市', 331000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331083, '玉环市', 331000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331100, '丽水市', 330000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (331102, '莲都区', 331100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331121, '青田县', 331100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331122, '缙云县', 331100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331123, '遂昌县', 331100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331124, '松阳县', 331100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331125, '云和县', 331100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331126, '庆元县', 331100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331127, '景宁畲族自治县', 331100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (331181, '龙泉市', 331100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340000, '安徽省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (340100, '合肥市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (340102, '瑶海区', 340100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340103, '庐阳区', 340100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340104, '蜀山区', 340100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340111, '包河区', 340100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340121, '长丰县', 340100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340122, '肥东县', 340100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340123, '肥西县', 340100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340124, '庐江县', 340100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340181, '巢湖市', 340100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340200, '芜湖市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (340202, '镜湖区', 340200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340203, '弋江区', 340200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340207, '鸠江区', 340200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340208, '三山区', 340200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340221, '芜湖县', 340200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340222, '繁昌县', 340200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340223, '南陵县', 340200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340225, '无为县', 340200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340300, '蚌埠市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (340302, '龙子湖区', 340300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340303, '蚌山区', 340300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340304, '禹会区', 340300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340311, '淮上区', 340300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340321, '怀远县', 340300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340322, '五河县', 340300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340323, '固镇县', 340300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340400, '淮南市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (340402, '大通区', 340400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340403, '田家庵区', 340400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340404, '谢家集区', 340400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340405, '八公山区', 340400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340406, '潘集区', 340400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340421, '凤台县', 340400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340422, '寿县', 340400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340500, '马鞍山市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (340503, '花山区', 340500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340504, '雨山区', 340500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340506, '博望区', 340500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340521, '当涂县', 340500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340522, '含山县', 340500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340523, '和县', 340500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340600, '淮北市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (340602, '杜集区', 340600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340603, '相山区', 340600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340604, '烈山区', 340600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340621, '濉溪县', 340600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340700, '铜陵市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (340705, '铜官区', 340700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340706, '义安区', 340700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340711, '郊区', 340700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340722, '枞阳县', 340700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340800, '安庆市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (340802, '迎江区', 340800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340803, '大观区', 340800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340811, '宜秀区', 340800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340822, '怀宁县', 340800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340824, '潜山县', 340800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340825, '太湖县', 340800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340826, '宿松县', 340800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340827, '望江县', 340800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340828, '岳西县', 340800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (340881, '桐城市', 340800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341000, '黄山市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (341002, '屯溪区', 341000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341003, '黄山区', 341000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341004, '徽州区', 341000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341021, '歙县', 341000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341022, '休宁县', 341000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341023, '黟县', 341000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341024, '祁门县', 341000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341100, '滁州市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (341102, '琅琊区', 341100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341103, '南谯区', 341100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341122, '来安县', 341100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341124, '全椒县', 341100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341125, '定远县', 341100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341126, '凤阳县', 341100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341181, '天长市', 341100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341182, '明光市', 341100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341200, '阜阳市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (341202, '颍州区', 341200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341203, '颍东区', 341200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341204, '颍泉区', 341200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341221, '临泉县', 341200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341222, '太和县', 341200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341225, '阜南县', 341200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341226, '颍上县', 341200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341282, '界首市', 341200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341300, '宿州市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (341302, '埇桥区', 341300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341321, '砀山县', 341300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341322, '萧县', 341300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341323, '灵璧县', 341300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341324, '泗县', 341300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341500, '六安市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (341502, '金安区', 341500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341503, '裕安区', 341500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341504, '叶集区', 341500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341522, '霍邱县', 341500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341523, '舒城县', 341500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341524, '金寨县', 341500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341525, '霍山县', 341500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341600, '亳州市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (341602, '谯城区', 341600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341621, '涡阳县', 341600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341622, '蒙城县', 341600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341623, '利辛县', 341600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341700, '池州市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (341702, '贵池区', 341700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341721, '东至县', 341700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341722, '石台县', 341700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341723, '青阳县', 341700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341800, '宣城市', 340000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (341802, '宣州区', 341800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341821, '郎溪县', 341800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341822, '广德县', 341800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341823, '泾县', 341800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341824, '绩溪县', 341800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341825, '旌德县', 341800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (341881, '宁国市', 341800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350000, '福建省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (350100, '福州市', 350000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (350102, '鼓楼区', 350100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350103, '台江区', 350100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350104, '仓山区', 350100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350105, '马尾区', 350100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350111, '晋安区', 350100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350121, '闽侯县', 350100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350122, '连江县', 350100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350123, '罗源县', 350100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350124, '闽清县', 350100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350125, '永泰县', 350100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350128, '平潭县', 350100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350181, '福清市', 350100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350182, '长乐市', 350100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350200, '厦门市', 350000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (350203, '思明区', 350200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350205, '海沧区', 350200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350206, '湖里区', 350200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350211, '集美区', 350200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350212, '同安区', 350200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350213, '翔安区', 350200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350300, '莆田市', 350000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (350302, '城厢区', 350300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350303, '涵江区', 350300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350304, '荔城区', 350300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350305, '秀屿区', 350300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350322, '仙游县', 350300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350400, '三明市', 350000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (350402, '梅列区', 350400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350403, '三元区', 350400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350421, '明溪县', 350400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350423, '清流县', 350400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350424, '宁化县', 350400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350425, '大田县', 350400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350426, '尤溪县', 350400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350427, '沙县', 350400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350428, '将乐县', 350400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350429, '泰宁县', 350400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350430, '建宁县', 350400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350481, '永安市', 350400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350500, '泉州市', 350000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (350502, '鲤城区', 350500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350503, '丰泽区', 350500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350504, '洛江区', 350500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350505, '泉港区', 350500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350521, '惠安县', 350500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350524, '安溪县', 350500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350525, '永春县', 350500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350526, '德化县', 350500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350527, '金门县', 350500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350581, '石狮市', 350500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350582, '晋江市', 350500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350583, '南安市', 350500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350600, '漳州市', 350000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (350602, '芗城区', 350600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350603, '龙文区', 350600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350622, '云霄县', 350600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350623, '漳浦县', 350600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350624, '诏安县', 350600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350625, '长泰县', 350600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350626, '东山县', 350600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350627, '南靖县', 350600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350628, '平和县', 350600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350629, '华安县', 350600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350681, '龙海市', 350600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350700, '南平市', 350000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (350702, '延平区', 350700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350703, '建阳区', 350700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350721, '顺昌县', 350700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350722, '浦城县', 350700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350723, '光泽县', 350700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350724, '松溪县', 350700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350725, '政和县', 350700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350781, '邵武市', 350700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350782, '武夷山市', 350700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350783, '建瓯市', 350700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350800, '龙岩市', 350000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (350802, '新罗区', 350800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350803, '永定区', 350800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350821, '长汀县', 350800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350823, '上杭县', 350800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350824, '武平县', 350800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350825, '连城县', 350800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350881, '漳平市', 350800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350900, '宁德市', 350000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (350902, '蕉城区', 350900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350921, '霞浦县', 350900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350922, '古田县', 350900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350923, '屏南县', 350900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350924, '寿宁县', 350900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350925, '周宁县', 350900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350926, '柘荣县', 350900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350981, '福安市', 350900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (350982, '福鼎市', 350900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360000, '江西省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (360100, '南昌市', 360000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (360102, '东湖区', 360100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360103, '西湖区', 360100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360104, '青云谱区', 360100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360105, '湾里区', 360100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360111, '青山湖区', 360100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360112, '新建区', 360100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360121, '南昌县', 360100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360123, '安义县', 360100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360124, '进贤县', 360100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360200, '景德镇市', 360000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (360202, '昌江区', 360200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360203, '珠山区', 360200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360222, '浮梁县', 360200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360281, '乐平市', 360200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360300, '萍乡市', 360000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (360302, '安源区', 360300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360313, '湘东区', 360300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360321, '莲花县', 360300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360322, '上栗县', 360300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360323, '芦溪县', 360300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360400, '九江市', 360000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (360402, '濂溪区', 360400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360403, '浔阳区', 360400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360421, '柴桑区', 360400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360423, '武宁县', 360400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360424, '修水县', 360400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360425, '永修县', 360400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360426, '德安县', 360400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360428, '都昌县', 360400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360429, '湖口县', 360400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360430, '彭泽县', 360400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360481, '瑞昌市', 360400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360482, '共青城市', 360400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360483, '庐山市', 360400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360500, '新余市', 360000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (360502, '渝水区', 360500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360521, '分宜县', 360500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360600, '鹰潭市', 360000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (360602, '月湖区', 360600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360622, '余江县', 360600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360681, '贵溪市', 360600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360700, '赣州市', 360000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (360702, '章贡区', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360703, '南康区', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360704, '赣县区', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360722, '信丰县', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360723, '大余县', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360724, '上犹县', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360725, '崇义县', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360726, '安远县', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360727, '龙南县', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360728, '定南县', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360729, '全南县', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360730, '宁都县', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360731, '于都县', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360732, '兴国县', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360733, '会昌县', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360734, '寻乌县', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360735, '石城县', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360781, '瑞金市', 360700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360800, '吉安市', 360000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (360802, '吉州区', 360800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360803, '青原区', 360800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360821, '吉安县', 360800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360822, '吉水县', 360800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360823, '峡江县', 360800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360824, '新干县', 360800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360825, '永丰县', 360800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360826, '泰和县', 360800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360827, '遂川县', 360800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360828, '万安县', 360800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360829, '安福县', 360800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360830, '永新县', 360800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360881, '井冈山市', 360800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360900, '宜春市', 360000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (360902, '袁州区', 360900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360921, '奉新县', 360900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360922, '万载县', 360900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360923, '上高县', 360900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360924, '宜丰县', 360900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360925, '靖安县', 360900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360926, '铜鼓县', 360900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360981, '丰城市', 360900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360982, '樟树市', 360900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (360983, '高安市', 360900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361000, '抚州市', 360000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (361002, '临川区', 361000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361003, '东乡区', 361000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361021, '南城县', 361000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361022, '黎川县', 361000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361023, '南丰县', 361000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361024, '崇仁县', 361000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361025, '乐安县', 361000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361026, '宜黄县', 361000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361027, '金溪县', 361000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361028, '资溪县', 361000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361030, '广昌县', 361000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361100, '上饶市', 360000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (361102, '信州区', 361100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361103, '广丰区', 361100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361121, '上饶县', 361100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361123, '玉山县', 361100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361124, '铅山县', 361100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361125, '横峰县', 361100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361126, '弋阳县', 361100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361127, '余干县', 361100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361128, '鄱阳县', 361100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361129, '万年县', 361100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361130, '婺源县', 361100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (361181, '德兴市', 361100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370000, '山东省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (370100, '济南市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (370102, '历下区', 370100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370103, '市中区', 370100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370104, '槐荫区', 370100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370105, '天桥区', 370100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370112, '历城区', 370100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370113, '长清区', 370100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370114, '章丘区', 370100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370124, '平阴县', 370100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370125, '济阳县', 370100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370126, '商河县', 370100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370200, '青岛市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (370202, '市南区', 370200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370203, '市北区', 370200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370211, '黄岛区', 370200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370212, '崂山区', 370200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370213, '李沧区', 370200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370214, '城阳区', 370200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370281, '胶州市', 370200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370282, '即墨市', 370200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370283, '平度市', 370200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370285, '莱西市', 370200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370300, '淄博市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (370302, '淄川区', 370300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370303, '张店区', 370300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370304, '博山区', 370300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370305, '临淄区', 370300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370306, '周村区', 370300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370321, '桓台县', 370300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370322, '高青县', 370300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370323, '沂源县', 370300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370400, '枣庄市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (370402, '市中区', 370400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370403, '薛城区', 370400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370404, '峄城区', 370400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370405, '台儿庄区', 370400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370406, '山亭区', 370400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370481, '滕州市', 370400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370500, '东营市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (370502, '东营区', 370500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370503, '河口区', 370500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370505, '垦利区', 370500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370522, '利津县', 370500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370523, '广饶县', 370500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370600, '烟台市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (370602, '芝罘区', 370600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370611, '福山区', 370600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370612, '牟平区', 370600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370613, '莱山区', 370600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370634, '长岛县', 370600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370681, '龙口市', 370600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370682, '莱阳市', 370600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370683, '莱州市', 370600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370684, '蓬莱市', 370600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370685, '招远市', 370600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370686, '栖霞市', 370600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370687, '海阳市', 370600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370700, '潍坊市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (370702, '潍城区', 370700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370703, '寒亭区', 370700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370704, '坊子区', 370700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370705, '奎文区', 370700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370724, '临朐县', 370700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370725, '昌乐县', 370700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370781, '青州市', 370700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370782, '诸城市', 370700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370783, '寿光市', 370700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370784, '安丘市', 370700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370785, '高密市', 370700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370786, '昌邑市', 370700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370800, '济宁市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (370811, '任城区', 370800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370812, '兖州区', 370800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370826, '微山县', 370800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370827, '鱼台县', 370800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370828, '金乡县', 370800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370829, '嘉祥县', 370800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370830, '汶上县', 370800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370831, '泗水县', 370800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370832, '梁山县', 370800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370881, '曲阜市', 370800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370883, '邹城市', 370800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370900, '泰安市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (370902, '泰山区', 370900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370911, '岱岳区', 370900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370921, '宁阳县', 370900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370923, '东平县', 370900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370982, '新泰市', 370900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (370983, '肥城市', 370900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371000, '威海市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (371002, '环翠区', 371000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371003, '文登区', 371000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371082, '荣成市', 371000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371083, '乳山市', 371000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371100, '日照市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (371102, '东港区', 371100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371103, '岚山区', 371100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371121, '五莲县', 371100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371122, '莒县', 371100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371200, '莱芜市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (371202, '莱城区', 371200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371203, '钢城区', 371200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371300, '临沂市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (371302, '兰山区', 371300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371311, '罗庄区', 371300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371312, '河东区', 371300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371321, '沂南县', 371300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371322, '郯城县', 371300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371323, '沂水县', 371300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371324, '兰陵县', 371300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371325, '费县', 371300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371326, '平邑县', 371300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371327, '莒南县', 371300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371328, '蒙阴县', 371300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371329, '临沭县', 371300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371400, '德州市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (371402, '德城区', 371400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371403, '陵城区', 371400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371422, '宁津县', 371400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371423, '庆云县', 371400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371424, '临邑县', 371400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371425, '齐河县', 371400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371426, '平原县', 371400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371427, '夏津县', 371400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371428, '武城县', 371400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371481, '乐陵市', 371400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371482, '禹城市', 371400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371500, '聊城市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (371502, '东昌府区', 371500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371521, '阳谷县', 371500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371522, '莘县', 371500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371523, '茌平县', 371500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371524, '东阿县', 371500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371525, '冠县', 371500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371526, '高唐县', 371500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371581, '临清市', 371500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371600, '滨州市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (371602, '滨城区', 371600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371603, '沾化区', 371600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371621, '惠民县', 371600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371622, '阳信县', 371600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371623, '无棣县', 371600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371625, '博兴县', 371600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371626, '邹平县', 371600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371700, '菏泽市', 370000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (371702, '牡丹区', 371700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371703, '定陶区', 371700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371721, '曹县', 371700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371722, '单县', 371700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371723, '成武县', 371700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371724, '巨野县', 371700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371725, '郓城县', 371700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371726, '鄄城县', 371700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (371728, '东明县', 371700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410000, '河南省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (410100, '郑州市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (410102, '中原区', 410100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410103, '二七区', 410100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410104, '管城回族区', 410100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410105, '金水区', 410100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410106, '上街区', 410100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410108, '惠济区', 410100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410122, '中牟县', 410100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410181, '巩义市', 410100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410182, '荥阳市', 410100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410183, '新密市', 410100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410184, '新郑市', 410100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410185, '登封市', 410100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410200, '开封市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (410202, '龙亭区', 410200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410203, '顺河回族区', 410200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410204, '鼓楼区', 410200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410205, '禹王台区', 410200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410212, '祥符区', 410200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410221, '杞县', 410200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410222, '通许县', 410200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410223, '尉氏县', 410200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410225, '兰考县', 410200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410300, '洛阳市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (410302, '老城区', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410303, '西工区', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410304, '瀍河回族区', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410305, '涧西区', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410306, '吉利区', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410311, '洛龙区', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410322, '孟津县', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410323, '新安县', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410324, '栾川县', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410325, '嵩县', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410326, '汝阳县', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410327, '宜阳县', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410328, '洛宁县', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410329, '伊川县', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410381, '偃师市', 410300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410400, '平顶山市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (410402, '新华区', 410400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410403, '卫东区', 410400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410404, '石龙区', 410400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410411, '湛河区', 410400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410421, '宝丰县', 410400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410422, '叶县', 410400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410423, '鲁山县', 410400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410425, '郏县', 410400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410481, '舞钢市', 410400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410482, '汝州市', 410400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410500, '安阳市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (410502, '文峰区', 410500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410503, '北关区', 410500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410505, '殷都区', 410500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410506, '龙安区', 410500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410522, '安阳县', 410500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410523, '汤阴县', 410500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410526, '滑县', 410500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410527, '内黄县', 410500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410581, '林州市', 410500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410600, '鹤壁市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (410602, '鹤山区', 410600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410603, '山城区', 410600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410611, '淇滨区', 410600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410621, '浚县', 410600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410622, '淇县', 410600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410700, '新乡市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (410702, '红旗区', 410700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410703, '卫滨区', 410700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410704, '凤泉区', 410700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410711, '牧野区', 410700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410721, '新乡县', 410700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410724, '获嘉县', 410700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410725, '原阳县', 410700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410726, '延津县', 410700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410727, '封丘县', 410700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410728, '长垣县', 410700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410781, '卫辉市', 410700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410782, '辉县市', 410700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410800, '焦作市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (410802, '解放区', 410800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410803, '中站区', 410800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410804, '马村区', 410800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410811, '山阳区', 410800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410821, '修武县', 410800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410822, '博爱县', 410800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410823, '武陟县', 410800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410825, '温县', 410800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410882, '沁阳市', 410800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410883, '孟州市', 410800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410900, '濮阳市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (410902, '华龙区', 410900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410922, '清丰县', 410900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410923, '南乐县', 410900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410926, '范县', 410900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410927, '台前县', 410900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (410928, '濮阳县', 410900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411000, '许昌市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (411002, '魏都区', 411000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411003, '建安区', 411000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411024, '鄢陵县', 411000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411025, '襄城县', 411000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411081, '禹州市', 411000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411082, '长葛市', 411000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411100, '漯河市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (411102, '源汇区', 411100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411103, '郾城区', 411100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411104, '召陵区', 411100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411121, '舞阳县', 411100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411122, '临颍县', 411100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411200, '三门峡市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (411202, '湖滨区', 411200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411203, '陕州区', 411200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411221, '渑池县', 411200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411224, '卢氏县', 411200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411281, '义马市', 411200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411282, '灵宝市', 411200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411300, '南阳市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (411302, '宛城区', 411300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411303, '卧龙区', 411300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411321, '南召县', 411300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411322, '方城县', 411300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411323, '西峡县', 411300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411324, '镇平县', 411300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411325, '内乡县', 411300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411326, '淅川县', 411300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411327, '社旗县', 411300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411328, '唐河县', 411300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411329, '新野县', 411300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411330, '桐柏县', 411300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411381, '邓州市', 411300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411400, '商丘市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (411402, '梁园区', 411400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411403, '睢阳区', 411400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411421, '民权县', 411400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411422, '睢县', 411400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411423, '宁陵县', 411400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411424, '柘城县', 411400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411425, '虞城县', 411400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411426, '夏邑县', 411400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411481, '永城市', 411400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411500, '信阳市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (411502, '浉河区', 411500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411503, '平桥区', 411500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411521, '罗山县', 411500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411522, '光山县', 411500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411523, '新县', 411500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411524, '商城县', 411500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411525, '固始县', 411500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411526, '潢川县', 411500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411527, '淮滨县', 411500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411528, '息县', 411500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411600, '周口市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (411602, '川汇区', 411600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411621, '扶沟县', 411600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411622, '西华县', 411600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411623, '商水县', 411600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411624, '沈丘县', 411600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411625, '郸城县', 411600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411626, '淮阳县', 411600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411627, '太康县', 411600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411628, '鹿邑县', 411600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411681, '项城市', 411600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411700, '驻马店市', 410000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (411702, '驿城区', 411700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411721, '西平县', 411700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411722, '上蔡县', 411700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411723, '平舆县', 411700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411724, '正阳县', 411700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411725, '确山县', 411700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411726, '泌阳县', 411700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411727, '汝南县', 411700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411728, '遂平县', 411700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (411729, '新蔡县', 411700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (419001, '济源市', 419000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420000, '湖北省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (420100, '武汉市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (420102, '江岸区', 420100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420103, '江汉区', 420100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420104, '硚口区', 420100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420105, '汉阳区', 420100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420106, '武昌区', 420100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420107, '青山区', 420100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420111, '洪山区', 420100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420112, '东西湖区', 420100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420113, '汉南区', 420100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420114, '蔡甸区', 420100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420115, '江夏区', 420100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420116, '黄陂区', 420100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420117, '新洲区', 420100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420200, '黄石市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (420202, '黄石港区', 420200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420203, '西塞山区', 420200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420204, '下陆区', 420200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420205, '铁山区', 420200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420222, '阳新县', 420200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420281, '大冶市', 420200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420300, '十堰市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (420302, '茅箭区', 420300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420303, '张湾区', 420300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420304, '郧阳区', 420300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420322, '郧西县', 420300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420323, '竹山县', 420300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420324, '竹溪县', 420300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420325, '房县', 420300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420381, '丹江口市', 420300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420500, '宜昌市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (420502, '西陵区', 420500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420503, '伍家岗区', 420500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420504, '点军区', 420500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420505, '猇亭区', 420500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420506, '夷陵区', 420500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420525, '远安县', 420500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420526, '兴山县', 420500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420527, '秭归县', 420500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420528, '长阳土家族自治县', 420500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420529, '五峰土家族自治县', 420500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420581, '宜都市', 420500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420582, '当阳市', 420500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420583, '枝江市', 420500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420600, '襄阳市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (420602, '襄城区', 420600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420606, '樊城区', 420600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420607, '襄州区', 420600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420624, '南漳县', 420600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420625, '谷城县', 420600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420626, '保康县', 420600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420682, '老河口市', 420600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420683, '枣阳市', 420600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420684, '宜城市', 420600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420700, '鄂州市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (420702, '梁子湖区', 420700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420703, '华容区', 420700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420704, '鄂城区', 420700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420800, '荆门市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (420802, '东宝区', 420800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420804, '掇刀区', 420800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420821, '京山县', 420800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420822, '沙洋县', 420800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420881, '钟祥市', 420800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420900, '孝感市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (420902, '孝南区', 420900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420921, '孝昌县', 420900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420922, '大悟县', 420900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420923, '云梦县', 420900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420981, '应城市', 420900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420982, '安陆市', 420900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (420984, '汉川市', 420900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421000, '荆州市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (421002, '沙市区', 421000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421003, '荆州区', 421000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421022, '公安县', 421000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421023, '监利县', 421000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421024, '江陵县', 421000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421081, '石首市', 421000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421083, '洪湖市', 421000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421087, '松滋市', 421000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421100, '黄冈市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (421102, '黄州区', 421100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421121, '团风县', 421100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421122, '红安县', 421100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421123, '罗田县', 421100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421124, '英山县', 421100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421125, '浠水县', 421100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421126, '蕲春县', 421100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421127, '黄梅县', 421100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421181, '麻城市', 421100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421182, '武穴市', 421100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421200, '咸宁市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (421202, '咸安区', 421200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421221, '嘉鱼县', 421200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421222, '通城县', 421200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421223, '崇阳县', 421200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421224, '通山县', 421200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421281, '赤壁市', 421200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421300, '随州市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (421303, '曾都区', 421300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421321, '随县', 421300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (421381, '广水市', 421300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (422800, '恩施土家族苗族自治州', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (422801, '恩施市', 422800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (422802, '利川市', 422800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (422822, '建始县', 422800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (422823, '巴东县', 422800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (422825, '宣恩县', 422800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (422826, '咸丰县', 422800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (422827, '来凤县', 422800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (422828, '鹤峰县', 422800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (429004, '仙桃市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (429005, '潜江市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (429006, '天门市', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (429021, '神农架林区', 420000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (430000, '湖南省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (430100, '长沙市', 430000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (430102, '芙蓉区', 430100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430103, '天心区', 430100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430104, '岳麓区', 430100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430105, '开福区', 430100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430111, '雨花区', 430100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430112, '望城区', 430100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430121, '长沙县', 430100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430181, '浏阳市', 430100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430182, '宁乡市', 430100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430200, '株洲市', 430000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (430202, '荷塘区', 430200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430203, '芦淞区', 430200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430204, '石峰区', 430200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430211, '天元区', 430200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430221, '株洲县', 430200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430223, '攸县', 430200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430224, '茶陵县', 430200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430225, '炎陵县', 430200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430281, '醴陵市', 430200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430300, '湘潭市', 430000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (430302, '雨湖区', 430300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430304, '岳塘区', 430300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430321, '湘潭县', 430300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430381, '湘乡市', 430300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430382, '韶山市', 430300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430400, '衡阳市', 430000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (430405, '珠晖区', 430400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430406, '雁峰区', 430400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430407, '石鼓区', 430400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430408, '蒸湘区', 430400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430412, '南岳区', 430400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430421, '衡阳县', 430400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430422, '衡南县', 430400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430423, '衡山县', 430400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430424, '衡东县', 430400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430426, '祁东县', 430400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430481, '耒阳市', 430400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430482, '常宁市', 430400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430500, '邵阳市', 430000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (430502, '双清区', 430500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430503, '大祥区', 430500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430511, '北塔区', 430500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430521, '邵东县', 430500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430522, '新邵县', 430500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430523, '邵阳县', 430500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430524, '隆回县', 430500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430525, '洞口县', 430500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430527, '绥宁县', 430500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430528, '新宁县', 430500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430529, '城步苗族自治县', 430500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430581, '武冈市', 430500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430600, '岳阳市', 430000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (430602, '岳阳楼区', 430600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430603, '云溪区', 430600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430611, '君山区', 430600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430621, '岳阳县', 430600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430623, '华容县', 430600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430624, '湘阴县', 430600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430626, '平江县', 430600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430681, '汨罗市', 430600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430682, '临湘市', 430600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430700, '常德市', 430000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (430702, '武陵区', 430700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430703, '鼎城区', 430700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430721, '安乡县', 430700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430722, '汉寿县', 430700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430723, '澧县', 430700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430724, '临澧县', 430700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430725, '桃源县', 430700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430726, '石门县', 430700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430781, '津市市', 430700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430800, '张家界市', 430000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (430802, '永定区', 430800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430811, '武陵源区', 430800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430821, '慈利县', 430800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430822, '桑植县', 430800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430900, '益阳市', 430000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (430902, '资阳区', 430900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430903, '赫山区', 430900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430921, '南县', 430900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430922, '桃江县', 430900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430923, '安化县', 430900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (430981, '沅江市', 430900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431000, '郴州市', 430000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (431002, '北湖区', 431000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431003, '苏仙区', 431000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431021, '桂阳县', 431000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431022, '宜章县', 431000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431023, '永兴县', 431000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431024, '嘉禾县', 431000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431025, '临武县', 431000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431026, '汝城县', 431000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431027, '桂东县', 431000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431028, '安仁县', 431000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431081, '资兴市', 431000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431100, '永州市', 430000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (431102, '零陵区', 431100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431103, '冷水滩区', 431100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431121, '祁阳县', 431100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431122, '东安县', 431100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431123, '双牌县', 431100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431124, '道县', 431100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431125, '江永县', 431100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431126, '宁远县', 431100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431127, '蓝山县', 431100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431128, '新田县', 431100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431129, '江华瑶族自治县', 431100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431200, '怀化市', 430000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (431202, '鹤城区', 431200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431221, '中方县', 431200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431222, '沅陵县', 431200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431223, '辰溪县', 431200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431224, '溆浦县', 431200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431225, '会同县', 431200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431226, '麻阳苗族自治县', 431200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431227, '新晃侗族自治县', 431200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431228, '芷江侗族自治县', 431200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431229, '靖州苗族侗族自治县', 431200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431230, '通道侗族自治县', 431200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431281, '洪江市', 431200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431300, '娄底市', 430000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (431302, '娄星区', 431300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431321, '双峰县', 431300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431322, '新化县', 431300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431381, '冷水江市', 431300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (431382, '涟源市', 431300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (433100, '湘西土家族苗族自治州', 430000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (433101, '吉首市', 433100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (433122, '泸溪县', 433100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (433123, '凤凰县', 433100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (433124, '花垣县', 433100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (433125, '保靖县', 433100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (433126, '古丈县', 433100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (433127, '永顺县', 433100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (433130, '龙山县', 433100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440000, '广东省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (440100, '广州市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (440103, '荔湾区', 440100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440104, '越秀区', 440100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440105, '海珠区', 440100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440106, '天河区', 440100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440111, '白云区', 440100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440112, '黄埔区', 440100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440113, '番禺区', 440100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440114, '花都区', 440100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440115, '南沙区', 440100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440117, '从化区', 440100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440118, '增城区', 440100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440200, '韶关市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (440203, '武江区', 440200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440204, '浈江区', 440200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440205, '曲江区', 440200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440222, '始兴县', 440200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440224, '仁化县', 440200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440229, '翁源县', 440200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440232, '乳源瑶族自治县', 440200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440233, '新丰县', 440200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440281, '乐昌市', 440200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440282, '南雄市', 440200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440300, '深圳市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (440303, '罗湖区', 440300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440304, '福田区', 440300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440305, '南山区', 440300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440306, '宝安区', 440300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440307, '龙岗区', 440300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440308, '盐田区', 440300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440309, '龙华区', 440300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440310, '坪山区', 440300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440400, '珠海市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (440402, '香洲区', 440400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440403, '斗门区', 440400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440404, '金湾区', 440400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440500, '汕头市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (440507, '龙湖区', 440500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440511, '金平区', 440500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440512, '濠江区', 440500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440513, '潮阳区', 440500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440514, '潮南区', 440500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440515, '澄海区', 440500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440523, '南澳县', 440500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440600, '佛山市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (440604, '禅城区', 440600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440605, '南海区', 440600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440606, '顺德区', 440600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440607, '三水区', 440600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440608, '高明区', 440600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440700, '江门市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (440703, '蓬江区', 440700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440704, '江海区', 440700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440705, '新会区', 440700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440781, '台山市', 440700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440783, '开平市', 440700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440784, '鹤山市', 440700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440785, '恩平市', 440700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440800, '湛江市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (440802, '赤坎区', 440800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440803, '霞山区', 440800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440804, '坡头区', 440800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440811, '麻章区', 440800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440823, '遂溪县', 440800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440825, '徐闻县', 440800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440881, '廉江市', 440800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440882, '雷州市', 440800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440883, '吴川市', 440800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440900, '茂名市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (440902, '茂南区', 440900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440904, '电白区', 440900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440981, '高州市', 440900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440982, '化州市', 440900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (440983, '信宜市', 440900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441200, '肇庆市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (441202, '端州区', 441200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441203, '鼎湖区', 441200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441204, '高要区', 441200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441223, '广宁县', 441200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441224, '怀集县', 441200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441225, '封开县', 441200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441226, '德庆县', 441200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441284, '四会市', 441200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441300, '惠州市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (441302, '惠城区', 441300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441303, '惠阳区', 441300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441322, '博罗县', 441300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441323, '惠东县', 441300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441324, '龙门县', 441300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441400, '梅州市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (441402, '梅江区', 441400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441403, '梅县区', 441400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441422, '大埔县', 441400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441423, '丰顺县', 441400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441424, '五华县', 441400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441426, '平远县', 441400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441427, '蕉岭县', 441400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441481, '兴宁市', 441400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441500, '汕尾市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (441502, '城区', 441500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441521, '海丰县', 441500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441523, '陆河县', 441500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441581, '陆丰市', 441500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441600, '河源市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (441602, '源城区', 441600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441621, '紫金县', 441600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441622, '龙川县', 441600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441623, '连平县', 441600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441624, '和平县', 441600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441625, '东源县', 441600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441700, '阳江市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (441702, '江城区', 441700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441704, '阳东区', 441700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441721, '阳西县', 441700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441781, '阳春市', 441700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441800, '清远市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (441802, '清城区', 441800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441803, '清新区', 441800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441821, '佛冈县', 441800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441823, '阳山县', 441800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441825, '连山壮族瑶族自治县', 441800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441826, '连南瑶族自治县', 441800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441881, '英德市', 441800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441882, '连州市', 441800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (441900, '东莞市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (442000, '中山市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (445100, '潮州市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (445102, '湘桥区', 445100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (445103, '潮安区', 445100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (445122, '饶平县', 445100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (445200, '揭阳市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (445202, '榕城区', 445200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (445203, '揭东区', 445200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (445222, '揭西县', 445200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (445224, '惠来县', 445200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (445281, '普宁市', 445200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (445300, '云浮市', 440000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (445302, '云城区', 445300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (445303, '云安区', 445300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (445321, '新兴县', 445300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (445322, '郁南县', 445300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (445381, '罗定市', 445300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450000, '广西壮族自治区', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (450100, '南宁市', 450000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (450102, '兴宁区', 450100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450103, '青秀区', 450100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450105, '江南区', 450100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450107, '西乡塘区', 450100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450108, '良庆区', 450100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450109, '邕宁区', 450100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450110, '武鸣区', 450100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450123, '隆安县', 450100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450124, '马山县', 450100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450125, '上林县', 450100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450126, '宾阳县', 450100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450127, '横县', 450100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450200, '柳州市', 450000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (450202, '城中区', 450200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450203, '鱼峰区', 450200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450204, '柳南区', 450200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450205, '柳北区', 450200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450206, '柳江区', 450200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450222, '柳城县', 450200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450223, '鹿寨县', 450200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450224, '融安县', 450200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450225, '融水苗族自治县', 450200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450226, '三江侗族自治县', 450200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450300, '桂林市', 450000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (450302, '秀峰区', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450303, '叠彩区', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450304, '象山区', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450305, '七星区', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450311, '雁山区', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450312, '临桂区', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450321, '阳朔县', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450323, '灵川县', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450324, '全州县', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450325, '兴安县', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450326, '永福县', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450327, '灌阳县', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450328, '龙胜各族自治县', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450329, '资源县', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450330, '平乐县', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450331, '荔浦县', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450332, '恭城瑶族自治县', 450300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450400, '梧州市', 450000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (450403, '万秀区', 450400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450405, '长洲区', 450400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450406, '龙圩区', 450400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450421, '苍梧县', 450400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450422, '藤县', 450400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450423, '蒙山县', 450400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450481, '岑溪市', 450400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450500, '北海市', 450000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (450502, '海城区', 450500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450503, '银海区', 450500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450512, '铁山港区', 450500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450521, '合浦县', 450500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450600, '防城港市', 450000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (450602, '港口区', 450600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450603, '防城区', 450600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450621, '上思县', 450600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450681, '东兴市', 450600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450700, '钦州市', 450000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (450702, '钦南区', 450700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450703, '钦北区', 450700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450721, '灵山县', 450700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450722, '浦北县', 450700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450800, '贵港市', 450000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (450802, '港北区', 450800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450803, '港南区', 450800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450804, '覃塘区', 450800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450821, '平南县', 450800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450881, '桂平市', 450800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450900, '玉林市', 450000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (450902, '玉州区', 450900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450903, '福绵区', 450900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450921, '容县', 450900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450922, '陆川县', 450900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450923, '博白县', 450900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450924, '兴业县', 450900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (450981, '北流市', 450900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451000, '百色市', 450000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (451002, '右江区', 451000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451021, '田阳县', 451000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451022, '田东县', 451000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451023, '平果县', 451000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451024, '德保县', 451000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451026, '那坡县', 451000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451027, '凌云县', 451000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451028, '乐业县', 451000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451029, '田林县', 451000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451030, '西林县', 451000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451031, '隆林各族自治县', 451000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451081, '靖西市', 451000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451100, '贺州市', 450000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (451102, '八步区', 451100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451103, '平桂区', 451100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451121, '昭平县', 451100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451122, '钟山县', 451100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451123, '富川瑶族自治县', 451100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451200, '河池市', 450000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (451202, '金城江区', 451200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451203, '宜州区', 451200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451221, '南丹县', 451200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451222, '天峨县', 451200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451223, '凤山县', 451200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451224, '东兰县', 451200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451225, '罗城仫佬族自治县', 451200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451226, '环江毛南族自治县', 451200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451227, '巴马瑶族自治县', 451200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451228, '都安瑶族自治县', 451200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451229, '大化瑶族自治县', 451200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451300, '来宾市', 450000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (451302, '兴宾区', 451300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451321, '忻城县', 451300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451322, '象州县', 451300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451323, '武宣县', 451300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451324, '金秀瑶族自治县', 451300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451381, '合山市', 451300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451400, '崇左市', 450000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (451402, '江州区', 451400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451421, '扶绥县', 451400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451422, '宁明县', 451400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451423, '龙州县', 451400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451424, '大新县', 451400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451425, '天等县', 451400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (451481, '凭祥市', 451400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (460000, '海南省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (460100, '海口市', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (460105, '秀英区', 460100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (460106, '龙华区', 460100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (460107, '琼山区', 460100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (460108, '美兰区', 460100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (460200, '三亚市', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (460201, '三亚市', 460200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (460202, '海棠区', 460200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (460203, '吉阳区', 460200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (460204, '天涯区', 460200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (460205, '崖州区', 460200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (460321, '西沙群岛', 460300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (460322, '南沙群岛', 460300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (460323, '中沙群岛', 460300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (460400, '儋州市', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (469001, '五指山市', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (469002, '琼海市', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (469005, '文昌市', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (469006, '万宁市', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (469007, '东方市', 460000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (469021, '定安县', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (469022, '屯昌县', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (469023, '澄迈县', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (469024, '临高县', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (469025, '白沙黎族自治县', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (469026, '昌江黎族自治县', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (469027, '乐东黎族自治县', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (469028, '陵水黎族自治县', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (469029, '保亭黎族苗族自治县', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (469030, '琼中黎族苗族自治县', 460000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (500000, '重庆市', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (500100, '重庆市', 500000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (500101, '万州区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500102, '涪陵区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500103, '渝中区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500104, '大渡口区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500105, '江北区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500106, '沙坪坝区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500107, '九龙坡区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500108, '南岸区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500109, '北碚区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500110, '綦江区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500111, '大足区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500112, '渝北区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500113, '巴南区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500114, '黔江区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500115, '长寿区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500116, '江津区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500117, '合川区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500118, '永川区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500119, '南川区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500120, '璧山区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500151, '铜梁区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500152, '潼南区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500153, '荣昌区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500154, '开州区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500155, '梁平区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500156, '武隆区', 500100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500229, '城口县', 500200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500230, '丰都县', 500200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500231, '垫江县', 500200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500233, '忠县', 500200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500235, '云阳县', 500200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500236, '奉节县', 500200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500237, '巫山县', 500200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500238, '巫溪县', 500200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500240, '石柱土家族自治县', 500200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500241, '秀山土家族苗族自治县', 500200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500242, '酉阳土家族苗族自治县', 500200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (500243, '彭水苗族土家族自治县', 500200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510000, '四川省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (510100, '成都市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (510104, '锦江区', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510105, '青羊区', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510106, '金牛区', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510107, '武侯区', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510108, '成华区', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510112, '龙泉驿区', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510113, '青白江区', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510114, '新都区', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510115, '温江区', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510116, '双流区', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510117, '郫都区', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510121, '金堂县', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510129, '大邑县', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510131, '蒲江县', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510132, '新津县', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510181, '都江堰市', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510182, '彭州市', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510183, '邛崃市', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510184, '崇州市', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510185, '简阳市', 510100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510300, '自贡市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (510302, '自流井区', 510300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510303, '贡井区', 510300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510304, '大安区', 510300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510311, '沿滩区', 510300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510321, '荣县', 510300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510322, '富顺县', 510300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510400, '攀枝花市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (510402, '东区', 510400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510403, '西区', 510400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510411, '仁和区', 510400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510421, '米易县', 510400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510422, '盐边县', 510400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510500, '泸州市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (510502, '江阳区', 510500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510503, '纳溪区', 510500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510504, '龙马潭区', 510500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510521, '泸县', 510500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510522, '合江县', 510500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510524, '叙永县', 510500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510525, '古蔺县', 510500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510600, '德阳市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (510603, '旌阳区', 510600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510623, '中江县', 510600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510626, '罗江区', 510600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510681, '广汉市', 510600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510682, '什邡市', 510600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510683, '绵竹市', 510600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510700, '绵阳市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (510703, '涪城区', 510700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510704, '游仙区', 510700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510705, '安州区', 510700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510722, '三台县', 510700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510723, '盐亭县', 510700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510725, '梓潼县', 510700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510726, '北川羌族自治县', 510700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510727, '平武县', 510700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510781, '江油市', 510700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510800, '广元市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (510802, '利州区', 510800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510811, '昭化区', 510800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510812, '朝天区', 510800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510821, '旺苍县', 510800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510822, '青川县', 510800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510823, '剑阁县', 510800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510824, '苍溪县', 510800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510900, '遂宁市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (510903, '船山区', 510900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510904, '安居区', 510900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510921, '蓬溪县', 510900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510922, '射洪县', 510900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (510923, '大英县', 510900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511000, '内江市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (511002, '市中区', 511000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511011, '东兴区', 511000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511024, '威远县', 511000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511025, '资中县', 511000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511083, '隆昌市', 511000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511100, '乐山市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (511102, '市中区', 511100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511111, '沙湾区', 511100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511112, '五通桥区', 511100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511113, '金口河区', 511100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511123, '犍为县', 511100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511124, '井研县', 511100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511126, '夹江县', 511100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511129, '沐川县', 511100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511132, '峨边彝族自治县', 511100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511133, '马边彝族自治县', 511100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511181, '峨眉山市', 511100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511300, '南充市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (511302, '顺庆区', 511300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511303, '高坪区', 511300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511304, '嘉陵区', 511300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511321, '南部县', 511300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511322, '营山县', 511300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511323, '蓬安县', 511300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511324, '仪陇县', 511300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511325, '西充县', 511300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511381, '阆中市', 511300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511400, '眉山市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (511402, '东坡区', 511400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511403, '彭山区', 511400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511421, '仁寿县', 511400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511423, '洪雅县', 511400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511424, '丹棱县', 511400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511425, '青神县', 511400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511500, '宜宾市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (511502, '翠屏区', 511500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511503, '南溪区', 511500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511521, '宜宾县', 511500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511523, '江安县', 511500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511524, '长宁县', 511500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511525, '高县', 511500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511526, '珙县', 511500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511527, '筠连县', 511500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511528, '兴文县', 511500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511529, '屏山县', 511500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511600, '广安市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (511602, '广安区', 511600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511603, '前锋区', 511600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511621, '岳池县', 511600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511622, '武胜县', 511600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511623, '邻水县', 511600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511681, '华蓥市', 511600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511700, '达州市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (511702, '通川区', 511700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511703, '达川区', 511700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511722, '宣汉县', 511700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511723, '开江县', 511700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511724, '大竹县', 511700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511725, '渠县', 511700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511781, '万源市', 511700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511800, '雅安市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (511802, '雨城区', 511800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511803, '名山区', 511800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511822, '荥经县', 511800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511823, '汉源县', 511800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511824, '石棉县', 511800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511825, '天全县', 511800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511826, '芦山县', 511800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511827, '宝兴县', 511800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511900, '巴中市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (511902, '巴州区', 511900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511903, '恩阳区', 511900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511921, '通江县', 511900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511922, '南江县', 511900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (511923, '平昌县', 511900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (512000, '资阳市', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (512002, '雁江区', 512000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (512021, '安岳县', 512000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (512022, '乐至县', 512000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513200, '阿坝藏族羌族自治州', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (513201, '马尔康市', 513200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513221, '汶川县', 513200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513222, '理县', 513200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513223, '茂县', 513200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513224, '松潘县', 513200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513225, '九寨沟县', 513200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513226, '金川县', 513200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513227, '小金县', 513200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513228, '黑水县', 513200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513230, '壤塘县', 513200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513231, '阿坝县', 513200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513232, '若尔盖县', 513200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513233, '红原县', 513200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513300, '甘孜藏族自治州', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (513301, '康定市', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513322, '泸定县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513323, '丹巴县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513324, '九龙县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513325, '雅江县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513326, '道孚县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513327, '炉霍县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513328, '甘孜县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513329, '新龙县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513330, '德格县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513331, '白玉县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513332, '石渠县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513333, '色达县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513334, '理塘县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513335, '巴塘县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513336, '乡城县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513337, '稻城县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513338, '得荣县', 513300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513400, '凉山彝族自治州', 510000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (513401, '西昌市', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513422, '木里藏族自治县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513423, '盐源县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513424, '德昌县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513425, '会理县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513426, '会东县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513427, '宁南县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513428, '普格县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513429, '布拖县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513430, '金阳县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513431, '昭觉县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513432, '喜德县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513433, '冕宁县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513434, '越西县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513435, '甘洛县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513436, '美姑县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (513437, '雷波县', 513400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520000, '贵州省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (520100, '贵阳市', 520000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (520102, '南明区', 520100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520103, '云岩区', 520100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520111, '花溪区', 520100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520112, '乌当区', 520100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520113, '白云区', 520100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520115, '观山湖区', 520100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520121, '开阳县', 520100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520122, '息烽县', 520100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520123, '修文县', 520100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520181, '清镇市', 520100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520200, '六盘水市', 520000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (520201, '钟山区', 520200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520203, '六枝特区', 520200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520221, '水城县', 520200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520281, '盘州市', 520200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520300, '遵义市', 520000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (520302, '红花岗区', 520300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520303, '汇川区', 520300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520304, '播州区', 520300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520322, '桐梓县', 520300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520323, '绥阳县', 520300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520324, '正安县', 520300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520325, '道真仡佬族苗族自治县', 520300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520326, '务川仡佬族苗族自治县', 520300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520327, '凤冈县', 520300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520328, '湄潭县', 520300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520329, '余庆县', 520300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520330, '习水县', 520300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520381, '赤水市', 520300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520382, '仁怀市', 520300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520400, '安顺市', 520000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (520402, '西秀区', 520400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520403, '平坝区', 520400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520422, '普定县', 520400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520423, '镇宁布依族苗族自治县', 520400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520424, '关岭布依族苗族自治县', 520400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520425, '紫云苗族布依族自治县', 520400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520500, '毕节市', 520000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (520502, '七星关区', 520500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520521, '大方县', 520500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520522, '黔西县', 520500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520523, '金沙县', 520500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520524, '织金县', 520500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520525, '纳雍县', 520500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520526, '威宁彝族回族苗族自治县', 520500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520527, '赫章县', 520500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520600, '铜仁市', 520000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (520602, '碧江区', 520600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520603, '万山区', 520600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520621, '江口县', 520600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520622, '玉屏侗族自治县', 520600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520623, '石阡县', 520600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520624, '思南县', 520600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520625, '印江土家族苗族自治县', 520600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520626, '德江县', 520600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520627, '沿河土家族自治县', 520600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (520628, '松桃苗族自治县', 520600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522300, '黔西南布依族苗族自治州', 520000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (522301, '兴义市', 522300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522322, '兴仁县', 522300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522323, '普安县', 522300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522324, '晴隆县', 522300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522325, '贞丰县', 522300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522326, '望谟县', 522300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522327, '册亨县', 522300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522328, '安龙县', 522300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522600, '黔东南苗族侗族自治州', 520000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (522601, '凯里市', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522622, '黄平县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522623, '施秉县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522624, '三穗县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522625, '镇远县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522626, '岑巩县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522627, '天柱县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522628, '锦屏县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522629, '剑河县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522630, '台江县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522631, '黎平县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522632, '榕江县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522633, '从江县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522634, '雷山县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522635, '麻江县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522636, '丹寨县', 522600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522700, '黔南布依族苗族自治州', 520000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (522701, '都匀市', 522700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522702, '福泉市', 522700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522722, '荔波县', 522700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522723, '贵定县', 522700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522725, '瓮安县', 522700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522726, '独山县', 522700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522727, '平塘县', 522700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522728, '罗甸县', 522700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522729, '长顺县', 522700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522730, '龙里县', 522700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522731, '惠水县', 522700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (522732, '三都水族自治县', 522700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530000, '云南省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (530100, '昆明市', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (530102, '五华区', 530100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530103, '盘龙区', 530100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530111, '官渡区', 530100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530112, '西山区', 530100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530113, '东川区', 530100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530114, '呈贡区', 530100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530115, '晋宁区', 530100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530124, '富民县', 530100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530125, '宜良县', 530100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530126, '石林彝族自治县', 530100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530127, '嵩明县', 530100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530128, '禄劝彝族苗族自治县', 530100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530129, '寻甸回族彝族自治县', 530100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530181, '安宁市', 530100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530300, '曲靖市', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (530302, '麒麟区', 530300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530303, '沾益区', 530300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530321, '马龙县', 530300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530322, '陆良县', 530300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530323, '师宗县', 530300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530324, '罗平县', 530300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530325, '富源县', 530300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530326, '会泽县', 530300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530381, '宣威市', 530300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530400, '玉溪市', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (530402, '红塔区', 530400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530403, '江川区', 530400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530422, '澄江县', 530400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530423, '通海县', 530400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530424, '华宁县', 530400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530425, '易门县', 530400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530426, '峨山彝族自治县', 530400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530427, '新平彝族傣族自治县', 530400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530428, '元江哈尼族彝族傣族自治县', 530400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530500, '保山市', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (530502, '隆阳区', 530500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530521, '施甸县', 530500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530523, '龙陵县', 530500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530524, '昌宁县', 530500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530581, '腾冲市', 530500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530600, '昭通市', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (530602, '昭阳区', 530600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530621, '鲁甸县', 530600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530622, '巧家县', 530600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530623, '盐津县', 530600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530624, '大关县', 530600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530625, '永善县', 530600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530626, '绥江县', 530600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530627, '镇雄县', 530600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530628, '彝良县', 530600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530629, '威信县', 530600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530630, '水富县', 530600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530700, '丽江市', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (530702, '古城区', 530700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530721, '玉龙纳西族自治县', 530700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530722, '永胜县', 530700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530723, '华坪县', 530700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530724, '宁蒗彝族自治县', 530700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530800, '普洱市', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (530802, '思茅区', 530800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530821, '宁洱哈尼族彝族自治县', 530800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530822, '墨江哈尼族自治县', 530800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530823, '景东彝族自治县', 530800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530824, '景谷傣族彝族自治县', 530800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530825, '镇沅彝族哈尼族拉祜族自治县', 530800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530826, '江城哈尼族彝族自治县', 530800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530827, '孟连傣族拉祜族佤族自治县', 530800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530828, '澜沧拉祜族自治县', 530800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530829, '西盟佤族自治县', 530800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530900, '临沧市', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (530902, '临翔区', 530900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530921, '凤庆县', 530900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530922, '云县', 530900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530923, '永德县', 530900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530924, '镇康县', 530900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530925, '双江拉祜族佤族布朗族傣族自治县', 530900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530926, '耿马傣族佤族自治县', 530900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (530927, '沧源佤族自治县', 530900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532300, '楚雄彝族自治州', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (532301, '楚雄市', 532300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532322, '双柏县', 532300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532323, '牟定县', 532300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532324, '南华县', 532300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532325, '姚安县', 532300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532326, '大姚县', 532300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532327, '永仁县', 532300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532328, '元谋县', 532300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532329, '武定县', 532300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532331, '禄丰县', 532300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532500, '红河哈尼族彝族自治州', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (532501, '个旧市', 532500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532502, '开远市', 532500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532503, '蒙自市', 532500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532504, '弥勒市', 532500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532523, '屏边苗族自治县', 532500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532524, '建水县', 532500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532525, '石屏县', 532500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532527, '泸西县', 532500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532528, '元阳县', 532500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532529, '红河县', 532500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532530, '金平苗族瑶族傣族自治县', 532500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532531, '绿春县', 532500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532532, '河口瑶族自治县', 532500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532600, '文山壮族苗族自治州', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (532601, '文山市', 532600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532622, '砚山县', 532600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532623, '西畴县', 532600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532624, '麻栗坡县', 532600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532625, '马关县', 532600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532626, '丘北县', 532600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532627, '广南县', 532600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532628, '富宁县', 532600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532800, '西双版纳傣族自治州', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (532801, '景洪市', 532800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532822, '勐海县', 532800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532823, '勐腊县', 532800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532900, '大理白族自治州', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (532901, '大理市', 532900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532922, '漾濞彝族自治县', 532900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532923, '祥云县', 532900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532924, '宾川县', 532900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532925, '弥渡县', 532900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532926, '南涧彝族自治县', 532900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532927, '巍山彝族回族自治县', 532900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532928, '永平县', 532900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532929, '云龙县', 532900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532930, '洱源县', 532900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532931, '剑川县', 532900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (532932, '鹤庆县', 532900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (533100, '德宏傣族景颇族自治州', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (533102, '瑞丽市', 533100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (533103, '芒市', 533100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (533122, '梁河县', 533100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (533123, '盈江县', 533100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (533124, '陇川县', 533100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (533300, '怒江傈僳族自治州', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (533301, '泸水市', 533300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (533323, '福贡县', 533300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (533324, '贡山独龙族怒族自治县', 533300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (533325, '兰坪白族普米族自治县', 533300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (533400, '迪庆藏族自治州', 530000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (533401, '香格里拉市', 533400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (533422, '德钦县', 533400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (533423, '维西傈僳族自治县', 533400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540000, '西藏自治区', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (540100, '拉萨市', 540000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (540102, '城关区', 540100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540103, '堆龙德庆区', 540100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540121, '林周县', 540100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540122, '当雄县', 540100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540123, '尼木县', 540100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540124, '曲水县', 540100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540126, '达孜县', 540100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540127, '墨竹工卡县', 540100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540200, '日喀则市', 540000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (540202, '桑珠孜区', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540221, '南木林县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540222, '江孜县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540223, '定日县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540224, '萨迦县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540225, '拉孜县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540226, '昂仁县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540227, '谢通门县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540228, '白朗县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540229, '仁布县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540230, '康马县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540231, '定结县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540232, '仲巴县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540233, '亚东县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540234, '吉隆县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540235, '聂拉木县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540236, '萨嘎县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540237, '岗巴县', 540200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540300, '昌都市', 540000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (540302, '卡若区', 540300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540321, '江达县', 540300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540322, '贡觉县', 540300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540323, '类乌齐县', 540300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540324, '丁青县', 540300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540325, '察雅县', 540300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540326, '八宿县', 540300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540327, '左贡县', 540300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540328, '芒康县', 540300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540329, '洛隆县', 540300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540330, '边坝县', 540300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540400, '林芝市', 540000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (540402, '巴宜区', 540400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540421, '工布江达县', 540400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540422, '米林县', 540400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540423, '墨脱县', 540400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540424, '波密县', 540400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540425, '察隅县', 540400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540426, '朗县', 540400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540500, '山南市', 540000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (540502, '乃东区', 540500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540521, '扎囊县', 540500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540522, '贡嘎县', 540500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540523, '桑日县', 540500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540524, '琼结县', 540500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540525, '曲松县', 540500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540526, '措美县', 540500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540527, '洛扎县', 540500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540528, '加查县', 540500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540529, '隆子县', 540500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540530, '错那县', 540500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (540531, '浪卡子县', 540500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542400, '那曲地区', 540000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (542421, '那曲县', 542400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542422, '嘉黎县', 542400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542423, '比如县', 542400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542424, '聂荣县', 542400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542425, '安多县', 542400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542426, '申扎县', 542400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542427, '索县', 542400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542428, '班戈县', 542400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542429, '巴青县', 542400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542430, '尼玛县', 542400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542431, '双湖县', 542400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542500, '阿里地区', 540000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (542521, '普兰县', 542500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542522, '札达县', 542500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542523, '噶尔县', 542500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542524, '日土县', 542500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542525, '革吉县', 542500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542526, '改则县', 542500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (542527, '措勤县', 542500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610000, '陕西省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (610100, '西安市', 610000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (610102, '新城区', 610100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610103, '碑林区', 610100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610104, '莲湖区', 610100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610111, '灞桥区', 610100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610112, '未央区', 610100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610113, '雁塔区', 610100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610114, '阎良区', 610100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610115, '临潼区', 610100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610116, '长安区', 610100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610117, '高陵区', 610100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610118, '鄠邑区', 610100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610122, '蓝田县', 610100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610124, '周至县', 610100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610200, '铜川市', 610000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (610202, '王益区', 610200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610203, '印台区', 610200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610204, '耀州区', 610200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610222, '宜君县', 610200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610300, '宝鸡市', 610000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (610302, '渭滨区', 610300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610303, '金台区', 610300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610304, '陈仓区', 610300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610322, '凤翔县', 610300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610323, '岐山县', 610300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610324, '扶风县', 610300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610326, '眉县', 610300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610327, '陇县', 610300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610328, '千阳县', 610300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610329, '麟游县', 610300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610330, '凤县', 610300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610331, '太白县', 610300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610400, '咸阳市', 610000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (610402, '秦都区', 610400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610403, '杨陵区', 610400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610404, '渭城区', 610400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610422, '三原县', 610400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610423, '泾阳县', 610400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610424, '乾县', 610400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610425, '礼泉县', 610400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610426, '永寿县', 610400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610427, '彬县', 610400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610428, '长武县', 610400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610429, '旬邑县', 610400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610430, '淳化县', 610400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610431, '武功县', 610400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610481, '兴平市', 610400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610500, '渭南市', 610000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (610502, '临渭区', 610500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610503, '华州区', 610500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610522, '潼关县', 610500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610523, '大荔县', 610500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610524, '合阳县', 610500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610525, '澄城县', 610500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610526, '蒲城县', 610500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610527, '白水县', 610500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610528, '富平县', 610500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610581, '韩城市', 610500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610582, '华阴市', 610500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610600, '延安市', 610000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (610602, '宝塔区', 610600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610603, '安塞区', 610600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610621, '延长县', 610600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610622, '延川县', 610600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610623, '子长县', 610600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610625, '志丹县', 610600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610626, '吴起县', 610600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610627, '甘泉县', 610600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610628, '富县', 610600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610629, '洛川县', 610600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610630, '宜川县', 610600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610631, '黄龙县', 610600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610632, '黄陵县', 610600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610700, '汉中市', 610000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (610702, '汉台区', 610700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610721, '南郑区', 610700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610722, '城固县', 610700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610723, '洋县', 610700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610724, '西乡县', 610700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610725, '勉县', 610700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610726, '宁强县', 610700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610727, '略阳县', 610700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610728, '镇巴县', 610700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610729, '留坝县', 610700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610730, '佛坪县', 610700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610800, '榆林市', 610000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (610802, '榆阳区', 610800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610803, '横山区', 610800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610822, '府谷县', 610800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610824, '靖边县', 610800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610825, '定边县', 610800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610826, '绥德县', 610800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610827, '米脂县', 610800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610828, '佳县', 610800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610829, '吴堡县', 610800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610830, '清涧县', 610800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610831, '子洲县', 610800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610881, '神木市', 610800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610900, '安康市', 610000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (610902, '汉滨区', 610900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610921, '汉阴县', 610900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610922, '石泉县', 610900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610923, '宁陕县', 610900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610924, '紫阳县', 610900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610925, '岚皋县', 610900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610926, '平利县', 610900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610927, '镇坪县', 610900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610928, '旬阳县', 610900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (610929, '白河县', 610900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (611000, '商洛市', 610000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (611002, '商州区', 611000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (611021, '洛南县', 611000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (611022, '丹凤县', 611000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (611023, '商南县', 611000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (611024, '山阳县', 611000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (611025, '镇安县', 611000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (611026, '柞水县', 611000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620000, '甘肃省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (620100, '兰州市', 620000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (620102, '城关区', 620100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620103, '七里河区', 620100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620104, '西固区', 620100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620105, '安宁区', 620100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620111, '红古区', 620100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620121, '永登县', 620100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620122, '皋兰县', 620100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620123, '榆中县', 620100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620201, '嘉峪关市', 620200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620300, '金昌市', 620000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (620302, '金川区', 620300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620321, '永昌县', 620300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620400, '白银市', 620000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (620402, '白银区', 620400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620403, '平川区', 620400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620421, '靖远县', 620400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620422, '会宁县', 620400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620423, '景泰县', 620400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620500, '天水市', 620000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (620502, '秦州区', 620500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620503, '麦积区', 620500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620521, '清水县', 620500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620522, '秦安县', 620500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620523, '甘谷县', 620500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620524, '武山县', 620500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620525, '张家川回族自治县', 620500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620600, '武威市', 620000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (620602, '凉州区', 620600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620621, '民勤县', 620600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620622, '古浪县', 620600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620623, '天祝藏族自治县', 620600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620702, '甘州区', 620700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620721, '肃南裕固族自治县', 620700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620722, '民乐县', 620700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620723, '临泽县', 620700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620724, '高台县', 620700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620725, '山丹县', 620700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620800, '平凉市', 620000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (620802, '崆峒区', 620800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620821, '泾川县', 620800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620822, '灵台县', 620800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620823, '崇信县', 620800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620824, '华亭县', 620800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620825, '庄浪县', 620800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620826, '静宁县', 620800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620900, '酒泉市', 620000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (620902, '肃州区', 620900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620921, '金塔县', 620900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620922, '瓜州县', 620900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620923, '肃北蒙古族自治县', 620900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620924, '阿克塞哈萨克族自治县', 620900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620981, '玉门市', 620900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (620982, '敦煌市', 620900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621000, '庆阳市', 620000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (621002, '西峰区', 621000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621021, '庆城县', 621000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621022, '环县', 621000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621023, '华池县', 621000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621024, '合水县', 621000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621025, '正宁县', 621000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621026, '宁县', 621000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621027, '镇原县', 621000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621100, '定西市', 620000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (621102, '安定区', 621100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621121, '通渭县', 621100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621122, '陇西县', 621100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621123, '渭源县', 621100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621124, '临洮县', 621100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621125, '漳县', 621100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621126, '岷县', 621100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621200, '陇南市', 620000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (621202, '武都区', 621200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621221, '成县', 621200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621222, '文县', 621200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621223, '宕昌县', 621200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621224, '康县', 621200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621225, '西和县', 621200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621226, '礼县', 621200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621227, '徽县', 621200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (621228, '两当县', 621200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (622900, '临夏回族自治州', 620000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (622901, '临夏市', 622900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (622921, '临夏县', 622900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (622922, '康乐县', 622900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (622923, '永靖县', 622900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (622924, '广河县', 622900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (622925, '和政县', 622900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (622926, '东乡族自治县', 622900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (622927, '积石山保安族东乡族撒拉族自治县', 622900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (623000, '甘南藏族自治州', 620000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (623001, '合作市', 623000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (623021, '临潭县', 623000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (623022, '卓尼县', 623000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (623023, '舟曲县', 623000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (623024, '迭部县', 623000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (623025, '玛曲县', 623000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (623026, '碌曲县', 623000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (623027, '夏河县', 623000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (630000, '青海省', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (630100, '西宁市', 630000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (630102, '城东区', 630100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (630103, '城中区', 630100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (630104, '城西区', 630100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (630105, '城北区', 630100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (630121, '大通回族土族自治县', 630100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (630122, '湟中县', 630100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (630123, '湟源县', 630100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (630200, '海东市', 630000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (630202, '乐都区', 630200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (630203, '平安区', 630200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (630222, '民和回族土族自治县', 630200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (630223, '互助土族自治县', 630200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (630224, '化隆回族自治县', 630200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (630225, '循化撒拉族自治县', 630200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632200, '海北藏族自治州', 630000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (632221, '门源回族自治县', 632200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632222, '祁连县', 632200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632223, '海晏县', 632200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632224, '刚察县', 632200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632321, '同仁县', 632300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632322, '尖扎县', 632300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632323, '泽库县', 632300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632324, '河南蒙古族自治县', 632300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632521, '共和县', 632500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632522, '同德县', 632500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632523, '贵德县', 632500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632524, '兴海县', 632500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632525, '贵南县', 632500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632600, '果洛藏族自治州', 630000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (632621, '玛沁县', 632600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632622, '班玛县', 632600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632623, '甘德县', 632600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632624, '达日县', 632600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632625, '久治县', 632600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632626, '玛多县', 632600, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632700, '玉树藏族自治州', 630000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (632701, '玉树市', 632700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632722, '杂多县', 632700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632723, '称多县', 632700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632724, '治多县', 632700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632725, '囊谦县', 632700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632726, '曲麻莱县', 632700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632800, '海西蒙古族藏族自治州', 630000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (632801, '格尔木市', 632800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632802, '德令哈市', 632800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632821, '乌兰县', 632800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632822, '都兰县', 632800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632823, '天峻县', 632800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632857, '大柴旦行政委员会', 632800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632858, '冷湖行政委员会', 632800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (632859, '茫崖行政委员会', 632800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640000, '宁夏回族自治区', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (640100, '银川市', 640000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (640104, '兴庆区', 640100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640105, '西夏区', 640100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640106, '金凤区', 640100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640121, '永宁县', 640100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640122, '贺兰县', 640100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640181, '灵武市', 640100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640200, '石嘴山市', 640000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (640202, '大武口区', 640200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640205, '惠农区', 640200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640221, '平罗县', 640200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640300, '吴忠市', 640000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (640302, '利通区', 640300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640303, '红寺堡区', 640300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640323, '盐池县', 640300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640324, '同心县', 640300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640381, '青铜峡市', 640300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640400, '固原市', 640000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (640402, '原州区', 640400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640422, '西吉县', 640400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640423, '隆德县', 640400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640424, '泾源县', 640400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640425, '彭阳县', 640400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640500, '中卫市', 640000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (640502, '沙坡头区', 640500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640521, '中宁县', 640500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (640522, '海原县', 640500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650000, '新疆维吾尔自治区', 1, NULL, NULL, 1);
INSERT INTO `provinces` VALUES (650100, '乌鲁木齐市', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (650102, '天山区', 650100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650103, '沙依巴克区', 650100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650104, '新市区', 650100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650105, '水磨沟区', 650100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650106, '头屯河区', 650100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650107, '达坂城区', 650100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650109, '米东区', 650100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650121, '乌鲁木齐县', 650100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650200, '克拉玛依市', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (650202, '独山子区', 650200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650203, '克拉玛依区', 650200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650204, '白碱滩区', 650200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650205, '乌尔禾区', 650200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650400, '吐鲁番市', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (650402, '高昌区', 650400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650421, '鄯善县', 650400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650422, '托克逊县', 650400, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650500, '哈密市', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (650502, '伊州区', 650500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650521, '巴里坤哈萨克自治县', 650500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (650522, '伊吾县', 650500, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652300, '昌吉回族自治州', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (652301, '昌吉市', 652300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652302, '阜康市', 652300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652323, '呼图壁县', 652300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652324, '玛纳斯县', 652300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652325, '奇台县', 652300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652327, '吉木萨尔县', 652300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652328, '木垒哈萨克自治县', 652300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652700, '博尔塔拉蒙古自治州', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (652701, '博乐市', 652700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652702, '阿拉山口市', 652700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652722, '精河县', 652700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652723, '温泉县', 652700, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652800, '巴音郭楞蒙古自治州', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (652801, '库尔勒市', 652800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652822, '轮台县', 652800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652823, '尉犁县', 652800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652824, '若羌县', 652800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652825, '且末县', 652800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652826, '焉耆回族自治县', 652800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652827, '和静县', 652800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652828, '和硕县', 652800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652829, '博湖县', 652800, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652900, '阿克苏地区', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (652901, '阿克苏市', 652900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652922, '温宿县', 652900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652923, '库车县', 652900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652924, '沙雅县', 652900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652925, '新和县', 652900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652926, '拜城县', 652900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652927, '乌什县', 652900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652928, '阿瓦提县', 652900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (652929, '柯坪县', 652900, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653000, '克孜勒苏柯尔克孜自治州', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (653001, '阿图什市', 653000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653022, '阿克陶县', 653000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653023, '阿合奇县', 653000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653024, '乌恰县', 653000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653100, '喀什地区', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (653101, '喀什市', 653100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653121, '疏附县', 653100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653122, '疏勒县', 653100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653123, '英吉沙县', 653100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653124, '泽普县', 653100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653125, '莎车县', 653100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653126, '叶城县', 653100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653127, '麦盖提县', 653100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653128, '岳普湖县', 653100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653129, '伽师县', 653100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653130, '巴楚县', 653100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653131, '塔什库尔干塔吉克自治县', 653100, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653200, '和田地区', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (653201, '和田市', 653200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653221, '和田县', 653200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653222, '墨玉县', 653200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653223, '皮山县', 653200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653224, '洛浦县', 653200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653225, '策勒县', 653200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653226, '于田县', 653200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (653227, '民丰县', 653200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654000, '伊犁哈萨克自治州', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (654002, '伊宁市', 654000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654003, '奎屯市', 654000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654004, '霍尔果斯市', 654000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654021, '伊宁县', 654000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654022, '察布查尔锡伯自治县', 654000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654023, '霍城县', 654000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654024, '巩留县', 654000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654025, '新源县', 654000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654026, '昭苏县', 654000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654027, '特克斯县', 654000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654028, '尼勒克县', 654000, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654200, '塔城地区', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (654201, '塔城市', 654200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654202, '乌苏市', 654200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654221, '额敏县', 654200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654223, '沙湾县', 654200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654224, '托里县', 654200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654225, '裕民县', 654200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654226, '和布克赛尔蒙古自治县', 654200, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654300, '阿勒泰地区', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (654301, '阿勒泰市', 654300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654321, '布尔津县', 654300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654322, '富蕴县', 654300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654323, '福海县', 654300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654324, '哈巴河县', 654300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654325, '青河县', 654300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (654326, '吉木乃县', 654300, NULL, NULL, 3);
INSERT INTO `provinces` VALUES (659001, '石河子市', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (659002, '阿拉尔市', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (659003, '图木舒克市', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (659004, '五家渠市', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (659005, '北屯市', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (659007, '双河市', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (659008, '可克达拉市', 650000, NULL, NULL, 2);
INSERT INTO `provinces` VALUES (659009, '昆玉市', 653200, NULL, NULL, 3);

-- ----------------------------
-- Table structure for proxy_log
-- ----------------------------
DROP TABLE IF EXISTS `proxy_log`;
CREATE TABLE `proxy_log`  (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `memberId` int(11) NULL DEFAULT 0 COMMENT '会员',
  `orderId` bigint(20) NULL DEFAULT 0 COMMENT '订单id',
  `goodId` int(12) UNSIGNED NULL DEFAULT NULL COMMENT '商品id',
  `amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '订单金额',
  `money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '分成金额',
  `status` int(1) NULL DEFAULT 0 COMMENT '领取状态 0表示未领取 1表示已分配',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '经销商分成表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of proxy_log
-- ----------------------------
INSERT INTO `proxy_log` VALUES (1, 3, 201811290515169159, NULL, 0.00, 0.00, 0, '2018-11-29 17:17:36');
INSERT INTO `proxy_log` VALUES (2, 3, 201811301057077090, NULL, 10.00, 6.00, 1, '2018-11-30 10:58:21');
INSERT INTO `proxy_log` VALUES (3, 3, 201811301106443706, NULL, 10.00, 6.00, 1, '2018-11-30 11:09:07');
INSERT INTO `proxy_log` VALUES (4, 10, 201812190538407983, NULL, 8.00, 4.00, 1, '2018-12-19 17:39:51');
INSERT INTO `proxy_log` VALUES (5, 10, 201812190538407983, NULL, 8.00, 4.00, 1, '2018-12-19 17:39:51');
INSERT INTO `proxy_log` VALUES (6, 29, 201812190610559323, NULL, 12.00, 7.00, 0, '2018-12-19 18:11:48');
INSERT INTO `proxy_log` VALUES (7, 29, 201812190616313355, 159, 10.00, 6.00, 1, '2018-12-19 18:16:41');
INSERT INTO `proxy_log` VALUES (8, 29, 201812190648026391, 160, 4.00, 2.00, 0, '2018-12-19 18:50:51');
INSERT INTO `proxy_log` VALUES (9, 38, 201812200311583811, 179, 0.90, 0.00, 0, '2018-12-20 15:12:32');
INSERT INTO `proxy_log` VALUES (10, 38, 201812200311273902, 178, 0.01, 0.00, 0, '2018-12-20 15:13:16');
INSERT INTO `proxy_log` VALUES (11, 38, 201812200315207111, 180, 668.00, 400.00, 0, '2018-12-20 15:15:33');
INSERT INTO `proxy_log` VALUES (12, 38, 201812200329392231, 182, 0.00, 0.00, 0, '2018-12-20 15:30:31');
INSERT INTO `proxy_log` VALUES (13, 38, 201812200350537345, 183, 0.01, 0.00, 0, '2018-12-20 15:51:12');
INSERT INTO `proxy_log` VALUES (14, 38, 201812200430000070, 184, 20.00, 12.00, 1, '2018-12-20 16:30:12');
INSERT INTO `proxy_log` VALUES (15, 38, 201812200447511702, 185, 168.00, 100.00, 1, '2018-12-20 16:48:35');
INSERT INTO `proxy_log` VALUES (16, 38, 201812200538012611, 187, 0.00, 0.00, 0, '2018-12-20 17:44:35');
INSERT INTO `proxy_log` VALUES (17, 38, 201812200553367931, 188, 668.00, 400.00, 1, '2018-12-20 17:55:17');
INSERT INTO `proxy_log` VALUES (18, 38, 201812200605414842, 191, 90.00, 54.00, 0, '2018-12-20 18:06:03');
INSERT INTO `proxy_log` VALUES (19, 38, 201812200308196237, 177, 0.08, 0.00, 0, '2018-12-20 18:06:58');
INSERT INTO `proxy_log` VALUES (20, 38, 201812200606470446, 193, 168.00, 100.00, 0, '2018-12-20 18:07:08');
INSERT INTO `proxy_log` VALUES (21, 38, 201812200621092138, 209, 168.00, 16.00, 1, '2018-12-20 18:21:23');
INSERT INTO `proxy_log` VALUES (22, 38, 201812200657279713, 217, 668.00, 66.00, 0, '2018-12-20 18:57:35');
INSERT INTO `proxy_log` VALUES (23, 38, 201812240222373749, 261, 0.01, 0.00, 1, '2018-12-24 19:29:04');
INSERT INTO `proxy_log` VALUES (24, 38, 201812240859439290, 295, 668.00, 66.00, 0, '2018-12-24 20:59:47');
INSERT INTO `proxy_log` VALUES (25, 38, 201812251019438798, 297, 10.00, 1.00, 0, '2018-12-25 10:19:50');
INSERT INTO `proxy_log` VALUES (26, 38, 201812251020427472, 298, 168.00, 16.00, 0, '2018-12-25 10:20:52');
INSERT INTO `proxy_log` VALUES (27, 38, 201812251020427472, 299, 0.01, 0.00, 0, '2018-12-25 10:20:52');
INSERT INTO `proxy_log` VALUES (28, 38, 201812251052180465, 301, 0.01, 0.00, 0, '2018-12-25 10:56:57');
INSERT INTO `proxy_log` VALUES (29, 6, 201812290354185532, 314, 0.10, 0.00, 1, '2018-12-29 15:54:20');
INSERT INTO `proxy_log` VALUES (30, 6, 201812290355469836, 315, 756.00, 453.00, 1, '2018-12-29 15:55:51');

-- ----------------------------
-- Table structure for recharge
-- ----------------------------
DROP TABLE IF EXISTS `recharge`;
CREATE TABLE `recharge`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NULL DEFAULT NULL,
  `money` decimal(10, 2) NULL DEFAULT NULL,
  `state` int(11) NULL DEFAULT NULL,
  `createTime` datetime(0) NULL DEFAULT NULL,
  `payTime` datetime(0) NULL DEFAULT NULL,
  `pay_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for second_kill_log
-- ----------------------------
DROP TABLE IF EXISTS `second_kill_log`;
CREATE TABLE `second_kill_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sk_date` date NULL DEFAULT NULL COMMENT '秒杀日期',
  `good_id` int(11) NOT NULL DEFAULT 0 COMMENT '商品ID',
  `sk_num` int(5) NOT NULL DEFAULT -1 COMMENT '秒杀时段',
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '秒杀时段秘钥',
  `amount` int(12) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '秒杀记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `role` int(11) NULL DEFAULT NULL COMMENT '角色',
  `sex` int(11) NULL DEFAULT 0 COMMENT '性别',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '生日',
  `status` int(255) NULL DEFAULT 1 COMMENT '状态',
  `avatarTm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像缩略图',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `createTime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifyTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `last_visit` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `userType` int(11) NULL DEFAULT NULL COMMENT '用户类型',
  `authkey` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `accessToken` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remainder` decimal(10, 0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', '$2y$13$Y7psSUjREttue9Z0ghuJ0eyPTQ9KDLbFOV956Dm0nEpwGMa8Zmj3i', 1, 1, '2015-05-29 14:17:57', 1, '/uploads/user/headPicThumbnail/201812131040501514971702.jpg', '/uploads/user/headPic/201812131040507037750279.jpg', '1', '2015-05-29 14:18:21', '2015-05-29 14:18:23', '2015-04-27 15:09:01', 1, NULL, NULL, 0);

SET FOREIGN_KEY_CHECKS = 1;
