/**
 *  grid upload ctrl with expand
 *
 *  @author  daorli
 */
var GridUploadIco = (function ($) {
    /**
     * grid with upload path
     *
     * @param dom  create        grid dom
     * @param bool autoIncrement grid auto increment
     *
     * @required    base_upload
     * @constructor
     */
    var ColumnsCtrl = function (dom, autoIncrement) {
        this.dom = dom;
        this.data = {};
        this.dataLength = 0;
        this.autoIncrement = autoIncrement == undefined ? true : autoIncrement;
        this.uniqueKey = this.dom.attr('id');

        // 数组前缀
        var prefixKey = dom.attr('data-prefix');
        this.inputPrefixId = prefixKey==undefined ? 'pic_' : prefixKey;
    };

    ColumnsCtrl.prototype = {
        init : function () {
            var that = this,
                table = $('<table>').attr('data-id', 'pic-lists-' + that.uniqueKey).addClass('table');

            that.data = {};
            var values = that.dom.val(),
                cacheData;
            if (values.length) {
                var infos = JSON.parse(values);
                for (var i in infos) {
                    cacheData = infos[i].split('@');
                    if (cacheData[1] != undefined) {
                        cacheData[1] = cacheData[1].split('^');
                    }
                    that.data[i] = cacheData;
                    // that.dataLength++;
                }
            }
            that.dataLength = Object.keys(that.data).length;

            $('body').find('table[data-id="pic-lists-' + that.uniqueKey + '"]').each(function () {
                $(this).remove();
            });

            that.table = table;

            var num = Object.keys(that.data).length,
                currentNum = 1,
                increment;
            if (num) {
                for (var i in that.data) {
                    increment = false;
                    if (that.autoIncrement) {
                        if (num == currentNum ) {
                            increment = true;
                        }
                    }

                    that.build(this.data[i], i, increment);
                    currentNum++;
                }
            } else {
                that.dataLength = 1;
                that.build([], that.inputPrefixId + that.dataLength, that.autoIncrement);
            }

            that.dom.parent('div').append(this.table);
            that.dom.hide();
        },

        build : function (data, num, expand) {
            var that = this;

            if (data[1] == undefined) {
                data[1] = [];
            }

            var tr = $('<tr>').attr({'tr-id': num + '_' + that.uniqueKey, 'data-val' : num}),
                detailId = $('<input>').attr({'data-id' : 'detail-id'}).addClass('form-control').val(data[0]).on(
                    'input propertychange', function () {
                    that.setData();
                }),
                picPath = $('<input>').addClass('form-control').attr({
                    'id' : that.inputPrefixId + num + '_' + that.uniqueKey + '_1',
                    'data-id' : 'pics1', 'data-type' : num + '_' + that.uniqueKey + '_1'
                }).val((data[1][0] == undefined ? '' : data[1][0]))
                    .bind('setValueSuccess', function () {
                        var id = $(this).attr('data-type'),
                            val = uploadFiles[id] == undefined ? '' : uploadFiles[id].shift();
                        $(this).val(val);
                        uploadInfo[id] = [];
                        that.setData();
                        that.init();
                    }),
                picPath2 = $('<input>').addClass('form-control').attr({
                'id' : that.inputPrefixId + num + '_' + that.uniqueKey + '_2',
                'data-id' : 'pics2', 'data-type' : num + '_' + that.uniqueKey + '_2'
                }).val((data[1][1] == undefined ? '' : data[1][1]))
                    .bind('setValueSuccess', function () {
                        var id = $(this).attr('data-type'),
                            val = uploadFiles[id] == undefined ? '' : uploadFiles[id].shift();

                        $(this).val(val);
                        uploadInfo[id] = [];
                        that.setData();
                        that.init();
                }),
                expandButtonInput = $('<input>').attr({'data-id' : 'expand-button'}).addClass('form-control').on(
                    'input propertychange', function () {
                        that.setData();
                    }),
                expandInfo = $('<button>').attr({'type' : 'button'}).addClass('btn btn-default').css('margin', '5px').text('添加').bind(
                    'click',
                    function () {
                        that.setData();
                        // that.init();
                        that.build([], that.inputPrefixId + (that.dataLength+1), true);
                        that.dataLength = that.dataLength+1;
                    }
                );

            deleteInfo = $('<button>').attr({'type': 'button', 'data-val' : num + '_' + that.uniqueKey }).addClass('btn btn-danger').text('删除')
                .bind('click', function () {
                    var val = $(this).attr('data-val');
                that.table.find('tr[tr-id="' + val + '"]').eq(0).remove();
                that.setData();
            });

            var uploadInfo = $('<button>').attr({'type': 'button', 'data-id' : num + '_' + that.uniqueKey + '_1'}).addClass('btn btn-primary')
                .css('margin', '5px')
                .text('默认图标').bind('click', function (){
                that.setData();
                var id = $(this).attr('data-id');

                upload(uploadPath, $('input[id="' + that.inputPrefixId + id + '"]'));
            });

            var uploadInfo2 = $('<button>').attr({'type': 'button', 'data-id' : num + '_' + that.uniqueKey + '_2'}).addClass('btn btn-primary')
                .css('margin', '5px')
                .text('选中图标').bind('click', function (){
                    that.setData();
                    var id = $(this).attr('data-id');

                    upload(uploadPath, $('input[id="' + that.inputPrefixId + id + '"]'));
                });

            if (data[2] != undefined) {
                expandButtonInput.val(data[3]);
            }

            if (!expand && that.autoIncrement) {
                picPath.attr('disabled', true);
                expandInfo.css('display', 'none');
                uploadInfo.css('display', 'none');
                tr.attr('data-tr', 'true');
            } else {
                deleteInfo.css('display', 'none');
            }

            tr.append(
                $('<td>').html('<b>' + num + ': </b>'),

                 $('<td>').append(
                     $('<b>').text('链接：'),
                     detailId
                 ),
                $('<td>').append(
                    $('<b>').text('链接文字：'),
                    expandButtonInput
                ),
                $('<td>').append(
                    $('<b>').text('默认图标：'),
                    picPath
                ),
                $('<td>').append(
                    $('<b>').text('选中图标：'),
                    picPath2
                ),
                $('<td>').append(uploadInfo, uploadInfo2, deleteInfo, expandInfo)
            );

            this.table.append(tr, tr);
        },

        setData : function () {
            var that = this;
            result = {};
            that.table.find('tr').each(function () {
                var id = $(this).attr('data-val'),
                    detailId = $(this).find('input[data-id="detail-id"]').eq(0).val(),
                    pic = $(this).find('input[data-id="pics1"]').eq(0).val(),
                    pic2 = $(this).find('input[data-id="pics2"]').eq(0).val(),
                    expand = $(this).find('textarea[data-id="expand-area"]').eq(0).val(),
                    expandButton = $(this).find('input[data-id="expand-button"]').eq(0).val();

                    if ((pic.length > 0 || expandButton.length > 0 || detailId.length > 0) || !that.autoIncrement) {
                        result[id] = [detailId, pic + '^' + pic2, expand, expandButton].join('@');
                    }
            });

            that.dom.val(JSON.stringify(result));
        }
    };

    return function (dom, autoIncrement) {
        var c = new ColumnsCtrl(dom, autoIncrement);
        return c.init();
    }
})($);
