/**
 *  grid upload ctrl with expand
 *
 *  @author  daorli
 */
var GridUploadExpand = (function ($) {
    /**
     * grid with upload path
     *
     * @param dom  create        grid dom
     * @param bool autoIncrement grid auto increment
     *
     * @required    base_upload
     * @constructor
     */
    var ColumnsCtrl = function (dom, autoIncrement, config) {
        this.dom = dom;
        this.data = {};
        this.dataLength = 0;
        this.autoIncrement = autoIncrement == undefined ? true : autoIncrement;
        this.uniqueKey = this.dom.attr('id');

        this.config = config == undefined ? {} : config;
        // 数组前缀
        var prefixKey = dom.attr('data-prefix');
        this.inputPrefixId = prefixKey==undefined ? 'pic_' : prefixKey;
    };

    ColumnsCtrl.prototype = {
        init : function () {
            var that = this,
                table = $('<table>').attr('data-id', 'pic-lists-' + that.uniqueKey).addClass('table');

            that.data = {};
            var values = that.dom.val();
            if (values.length) {
                var infos = JSON.parse(values);
                for (var i in infos) {
                    that.data[i] = infos[i].split('@');
                    // that.dataLength++;
                }
            }
            that.dataLength = Object.keys(that.data).length;

            $('body').find('table[data-id="pic-lists-' + that.uniqueKey + '"]').each(function () {
                $(this).remove();
            });

            that.table = table;

            var num = Object.keys(that.data).length,
                currentNum = 1,
                increment;
            if (num) {
                for (var i in that.data) {
                    increment = false;
                    if (that.autoIncrement) {
                        if (num == currentNum ) {
                            increment = true;
                        }
                    }

                    that.build(this.data[i], i, increment);
                    currentNum++;
                }
            } else {
                that.dataLength = 1;
                that.build([], that.inputPrefixId + that.dataLength, that.autoIncrement);
            }

            that.dom.parent('div').append(this.table);
            that.dom.hide();
        },

        build : function (data, num, expand) {
            var that = this;
            var tr = $('<tr>').attr({'tr-id': num + '_' + that.uniqueKey, 'data-val' : num}),
                detailId = $('<input>').attr({'data-id' : 'detail-id'}).addClass('form-control').val(data[0]),
                picPath = $('<input>').addClass('form-control').attr({
                    'id' : that.inputPrefixId + num + '_' + that.uniqueKey,
                    'data-id' : 'pics', 'data-type' : num + '_' + that.uniqueKey
                }).val(data[1])
                    .bind('setValueSuccess', function () {
                        var id = $(this).attr('data-type'),
                            val = uploadFiles[id] == undefined ? '' : uploadFiles[id].shift();
                        $(this).val(val);
                        uploadInfo[id] = [];
                        that.setData();
                        that.init();
                    }).bind('input onpropertychange',function () {
                        that.setData();
                    }),
                expandInput = $('<textarea>').attr({'data-id' : 'expand-area'})
                    .addClass('form-control').bind('input onpropertychange',function () {
                    that.setData();
                }),
                expandButtonInput = $('<input>').attr({'data-id' : 'expand-button'}).addClass('form-control').bind('input onpropertychange',function () {
                    that.setData();
                }),
                expandInfo = $('<button>').attr({'type' : 'button'}).addClass('btn btn-default').css('margin', '5px').text('添加').bind(
                    'click',
                    function () {
                        that.setData();
                        // that.init();
                        that.build([], that.inputPrefixId + (that.dataLength+1), true);
                        that.dataLength = that.dataLength + 1;
                    }
                );

            deleteInfo = $('<button>').attr({'type': 'button', 'data-val' : num + '_' + that.uniqueKey }).addClass('btn btn-danger').text('删除')
                .bind('click', function () {
                    var val = $(this).attr('data-val');
                that.table.find('tr[tr-id="' + val + '"]').eq(0).remove();
                that.setData();
            });

            var uploadInfo = $('<button>').attr({'type': 'button', 'data-id' : num + '_' + that.uniqueKey}).addClass('btn btn-primary')
                .css('margin', '5px')
                .text('上传').bind('click', function (){
                that.setData();
                var id = $(this).attr('data-id');

                upload(uploadPath, $('input[id="' + that.inputPrefixId + id + '"]'));
            });

            if (data[2] != undefined) {
                expandInput.val(data[2]);
            }
            if (data[3] != undefined) {
                expandButtonInput.val(data[3]);
            }

            if (!expand && that.autoIncrement) {
                picPath.attr('disabled', true);
                expandInfo.css('display', 'none');
                uploadInfo.css('display', 'none');
                tr.attr('data-tr', 'true');
            } else {
                deleteInfo.css('display', 'none');
            }

            tr.append(
                $('<td>').html('<b>' + num + ': </b>'),
                 $('<td>').append(
                     $('<b>').text((that.config.title1 == undefined ? '链接' : that.config.title1) + '：'),
                     detailId
                 ),
                $('<td>').append(
                    $('<b>').text((that.config.title2 == undefined ?  '图片' : that.config.title2) + '：'),
                    picPath
                ),
                $('<td>').append(
                    $('<b>').text((that.config.title3 == undefined ? '按钮文字' : that.config.title3) + '：'),
                    expandButtonInput
                ),
                $('<td>').append(
                    $('<b>').text((that.config.title4 == undefined ? '说明' : that.config.title4) + '：'),
                    expandInput
                ),
                $('<td>').append(uploadInfo, deleteInfo, expandInfo)
            );

            this.table.append(tr, tr);
        },

        setData : function () {
            var that = this;
            result = {};
            that.table.find('tr').each(function () {console.log($(this));
                var id = $(this).attr('data-val'),
                    detailId = $(this).find('input[data-id="detail-id"]').eq(0).val(),
                    pic = $(this).find('input[data-id="pics"]').eq(0).val(),
                    expand = $(this).find('textarea[data-id="expand-area"]').eq(0).val(),
                    expandButton = $(this).find('input[data-id="expand-button"]').eq(0).val();

                console.log(expandButton);
                    if (pic.length > 0 || !that.autoIncrement) {
                        result[id] = [detailId, pic, expand, expandButton].join('@');
                    }
            });

            that.dom.val(JSON.stringify(result));
        }
    };

    return function (dom, autoIncrement, config) {
        var c = new ColumnsCtrl(dom, autoIncrement, config);
        return c.init();
    }
})($);
