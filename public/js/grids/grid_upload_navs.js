/**
 *  grid upload ctrl with expand
 *
 *  @author  daorli
 */
var GridUploadExpandNavs = (function ($) {
    /**
     * grid with upload path
     *
     * @param dom  create        grid dom
     * @param bool autoIncrement grid auto increment
     *
     * @required    base_upload
     * @constructor
     */
    var ColumnsCtrl = function (dom, autoIncrement, config) {
        this.dom = dom;
        this.data = {};
        this.dataLength = 0;
        this.autoIncrement = autoIncrement == undefined ? true : autoIncrement;
        this.uniqueKey = this.dom.attr('id');

        this.config = config == undefined ? {} : config;
        // 数组前缀
        var prefixKey = dom.attr('data-prefix');
        this.inputPrefixId = prefixKey==undefined ? 'pic_' : prefixKey;
    };

    ColumnsCtrl.prototype = {
        init : function () {
            var that = this,
                table = $('<table>').attr('data-id', 'pic-lists-' + that.uniqueKey).addClass('table');

            that.data = {};
            var values = that.dom.val();
            console.log(values);
            if (values.length) {
                var infos = JSON.parse(values);

                for (var i in infos) {
                    that.data[i] = infos[i].split('@');
                    // that.dataLength++;
                }
            }
            // that.dataLength = Object.keys(that.data).length;
            if (Object.keys(that.data)) {
                var key = Object.keys(that.data).pop();

                if (key != undefined) {
                    var keyInfo = key.split('_');
                    that.dataLength = parseInt(keyInfo[1])
                } else {
                    that.dataLength = Object.keys(that.data).length;
                }
            }

            $('body').find('table[data-id="pic-lists-' + that.uniqueKey + '"]').each(function () {
                $(this).remove();
            });

            that.table = table;

            var num = Object.keys(that.data).length,
                currentNum = 1,
                increment;


            that.table.append(
                $('<tr>').append(
                    $('<td>').append(
                        $('<b>').text((that.config.title3 == undefined ? '名称' : that.config.title3) + '：')
                    ),
                    $('<td>').append(
                        $('<b>').text((that.config.title1 == undefined ? '链接' : that.config.title1) + '：')
                    ),
                    $('<td>').append(
                        $('<b>').text((that.config.title2 == undefined ?  '操作' : that.config.title2) + '：')
                    )
                )
            );

            if (num) {
                for (var i in that.data) {
                    increment = false;
                    if (that.autoIncrement) {
                        if (num == currentNum ) {
                            increment = true;
                        }
                    }

                    that.build(this.data[i], i, increment);
                    currentNum++;
                }
            } else {
                that.dataLength = 1;
                that.build([], that.inputPrefixId + that.dataLength, that.autoIncrement);
            }

            that.dom.parent('div').append(this.table);
            that.dom.hide();
        },

        build : function (data, num, expand) {
            var that = this;
            var tr = $('<tr>').attr({'tr-id': num + '_' + that.uniqueKey, 'data-val' : num}),
                detailId = $('<input>').attr({'data-id' : 'detail-id'}).addClass('form-control').val(data[0]),
                expandButtonInput = $('<input>').attr({'data-id' : 'expand-button'}).addClass('form-control').bind('input onpropertychange',function () {
                    that.setData();
                }),
                expandInfo = $('<button>').attr({'type' : 'button'}).addClass('btn btn-default').css('margin', '5px').text('添加').bind(
                    'click',
                    function () {
                        that.setData();
                        that.build([], that.inputPrefixId + (that.dataLength+1), true);
                        that.dataLength = that.dataLength + 1;

                        $(this).parent().find('[data-id="delete"]').eq(0).show();
                        $(this).hide();
                    }
                );

            deleteInfo = $('<button>').attr({'type': 'button', 'data-val' : num + '_' + that.uniqueKey, 'data-id' : 'delete'}).addClass('btn btn-danger').text('删除')
                .bind('click', function () {
                    var val = $(this).parents('tr[tr-id]').attr('tr-id');
                    that.table.find('tr[tr-id="' + val + '"]').eq(0).remove();
                    that.setData();
                });

            if (data[1] != undefined) {
                expandButtonInput.val(data[1]);
            }

            if (!expand && that.autoIncrement) {
                tr.attr('data-tr', 'true');
            } else {
                deleteInfo.css('display', 'none');
            }

            var upButton = $('<button>').attr({'type': 'button', 'data-id' : num + '_' + that.uniqueKey, title : '上移'}).addClass('btn btn-default')
                    .css('margin-right', '10px')
                    .text('↑').bind('click', function (){
                        var thatTr = $(this).parents('tr[tr-id]'),
                            upTr = thatTr.prev('tr[tr-id]');
                        if (upTr.length) {
                            var tId = thatTr.attr('tr-id'),
                                uId = upTr.attr('tr-id'),
                                tNum = thatTr.attr('data-val'),
                                uNum = upTr.attr('data-val');

                            upTr.before(thatTr.attr({'tr-id': uId, 'data-val' : uNum}));
                            upTr.attr({'tr-id' : tId, 'data-val' : tNum});

                            that.setData();
                            that.init();
                        }
                    }),
                downButton = $('<button>').attr({'type': 'button', 'data-id' : num + '_' + that.uniqueKey, title : '下移'}).addClass('btn btn-default')
                    .css('margin-right', '10px')
                    .text('↓').bind('click', function (){
                        var thatTr = $(this).parents('tr[tr-id]'),
                            upTr = thatTr.next('tr[tr-id]');
                        if (upTr.length) {
                            var tId = thatTr.attr('tr-id'),
                                uId = upTr.attr('tr-id'),
                                tNum = thatTr.attr('data-val'),
                                uNum = upTr.attr('data-val');

                            upTr.after(thatTr.attr({'tr-id': uId, 'data-val' : uNum}));
                            upTr.attr({'tr-id' : tId, 'data-val' : tNum});

                            that.setData();
                            that.init();
                        }
                    });

            tr.append(
                $('<td>').append(
                    expandButtonInput
                ),
                $('<td>').append(
                    detailId
                ),
                $('<td>').append(upButton, downButton, deleteInfo, expandInfo)
            );

            this.table.append(tr, tr);
        },

        setData : function () {
            var that = this;
            result = {};
            that.table.find('tr[tr-id]').each(function () {
                var id = $(this).attr('data-val'),
                    detailId = $(this).find('input[data-id="detail-id"]').eq(0).val(),
                    expandButton = $(this).find('input[data-id="expand-button"]').eq(0).val();

                if (expandButton.length > 0 || !that.autoIncrement) {
                    result[id] = [detailId, expandButton].join('@');
                }
            });

            that.dom.val(JSON.stringify(result));
        }
    };

    return function (dom, autoIncrement, config) {
        var c = new ColumnsCtrl(dom, autoIncrement, config);
        return c.init();
    }
})($);