/**
 *  grid input ctrl
 *
 *  @author  daorli
 */
var GridInput = (function ($) {
    /**
     * grid with input
     *
     * @param dom  create        grid dom
     * @param bool autoIncrement grid auto increment
     *
     * @constructor
     */
    var ColumnsCtrl = function (dom, autoIncrement) {
        this.dom = dom;
        this.data = {};
        this.dataLength = 0;

        this.label_1 = typeof (label_1) == 'undefined' ? '标签' : label_1;
        this.label_2 = typeof (label_2) == 'undefined' ? '说明' : label_2;

        this.groupUniqueKey = this.dom.attr('name');
        this.autoIncrement = autoIncrement == undefined ? true : autoIncrement;
    };

    ColumnsCtrl.prototype = {
        init : function () {
            var that = this,
                table = $('<table>').attr('data-id', 'pic-lists' + that.groupUniqueKey);

            that.data = {};
            var values = that.dom.val();
            if (values.length) {
                var infos = JSON.parse(values);
                this.dataLength = infos.length;
                for(var i=0; i<this.dataLength; i++) {
                    that.data[i] = infos[i].split('@');
                }
            }

            $('body').find('table[data-id="pic-lists' + that.groupUniqueKey + '"]').each(function () {
                $(this).remove();
            });

            that.table = table;

            for (var i in that.data) {
                this.build(this.data[i], i, !that.autoIncrement);
            }

            // create empty column
            if (that.autoIncrement) {
                that.build([], that.dataLength+1, true);
            }

            this.dom.parent('div').append(this.table);
            this.dom.hide();
        },

        build : function (data, num, expand) {
            var that = this;
            var tr = $('<tr>').attr('tr-id', that.groupUniqueKey + num),
                detailId = $('<input>').attr({'data-id' : 'detail-id'}).addClass('form-control').val(data[0]).on(
                    'input propertychange',
                    function () {
                        that.setData();
                    }
                ),
                picPath = $('<input>').attr({
                    'id': that.groupUniqueKey + num, 'data-type' : 'input_' + that.groupUniqueKey + num, 'data-id' : 'pic'
                }).addClass('form-control').val(data[1]).on(
                    'input propertychange',
                    function () {
                        that.setData();
                    }
                ),
                expandInfo = $('<button>').attr({'type': 'button', 'class' : 'btn btn-primary'}).text('添加').bind(
                    'click',
                    function () {
                        that.setData();
                        that.init();
                    }
                );

            deleteInfo = $('<button>').attr({'type': 'button', 'class' : 'btn btn-danger'}).text('删除').bind('click', function () {
                that.table.find('tr[tr-id="' + that.groupUniqueKey + num + '"]').eq(0).remove();
                that.setData();
            });

            if (!expand) {
                // picPath.attr('disabled', true);
                expandInfo.css('display', 'none');
                tr.attr('data-tr', 'true');
            } else {
                deleteInfo.css('display', 'none');
            }

            tr.append(
                $('<td>').append(
                    $('<b>').text(that.label_1 + '：'),
                    detailId
                ),
                $('<td>').append(
                    $('<b>').text(that.label_2 + '：'),
                    picPath
                ).css('padding-left', '15px'),
                $('<td>').append(
                    deleteInfo, expandInfo
                ).css('padding-left', '25px')
            );

            this.table.append(tr);
        },

        setData : function () {
            var that = this;
            result = [];
            that.table.find('tr').each(function () {
                var detailId = $(this).find('input[data-id="detail-id"]').eq(0).val(),
                    pic = $(this).find('input[data-id="pic"]').eq(0).val();

                if (pic.length || !that.autoIncrement) {
                    result.push(detailId + '@' + pic);
                }
            });

            that.dom.val(JSON.stringify(result));
        }
    };

    return function (dom, autoIncrement) {
        var c = new ColumnsCtrl(dom, autoIncrement);
        return c.init();
    }
})($);
