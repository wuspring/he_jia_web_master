/**
 *  grid input ctrl
 *
 *  @author  daorli
 */
var InputSelectCtrl = (function ($){
    // select dom
    var Dom = '',
        SelectDom = $('<div>').attr({'data-id' : 'InSeCtrl_Select'})
            .addClass('input-select').css('display', 'none'),
        InputDom = $('<input>').attr({'data-id' : 'InSeCtrl_Input'}),
        PreviewDom = $('<div>').attr({'data-id' : 'InSeCtrl_Preview'});

    /**
     * input with select
     *
     * @param dom grid dom
     *
     * @const InputSelectCtrlRequestPath request to get select data
     * @const InputSelectCtrlDataPath    request to get data info
     *
     * @constructor
     */
    var InSeCtrl = function (dom) {
        this.dom = dom;
        this.data = dom.val();

        // set global param
        Dom = this.dom;

        if (Dom.attr('placeholder') != undefined) {
            InputDom.attr('placeholder', Dom.attr('placeholder'));
        }
    };

    InSeCtrl.prototype = {
        init : function () {
            var that = this,
                triggerInput = InputDom.addClass('form-control')
                    .bind('input onpropertychange', that.changeVal);
            // .blur(function () {
            //     that.selectDom.hide();
            // });



            that.dom.css('display', 'none').after(
                triggerInput,
                SelectDom,
                PreviewDom
            );

            buildPreviewLists();
        },
        changeVal : function () {
            var that = this,
                inputDom = $('input[data-id="InSeCtrl_Input"]'),
                value = inputDom.val();

            if (!value.length) {
                SelectDom.hide();
                return false;
            }
            $.ajax({
                url: InputSelectCtrlRequestPath,
                type: 'get',
                dataType: 'json',
                async:false,
                data : {key : value},
                success: function (res) {
                    if (res.status) {
                        buildSelect(res.data);
                        return false;
                    }

                    console.log(res.msg);
                    alert("网络开小差了，请稍后重试");
                },
                fail: function (res) {
                    console.log(res.msg);
                    alert("网络异常，请稍后重试~");
                }
            })
        },
    };

    // create select options
    var buildSelect = function(data) {
        SelectDom.empty();
        if (data.length) {
            for (var i=0; i<data.length; i++) {
                var list = $('<a>').attr({
                    'data-id' : 'InSeCtrl_Option',
                    'data-val' : data[i].storeId,
                }).bind('click', SaveInput);

                list.append(
                    $('<img>').attr('src', data[i].avatarTm),
                    $('<span>').text(data[i].nickname)
                );
                SelectDom.append(list)
            }

        } else {
            SelectDom.append($('<a>').attr({
                'href' : 'javascript:void(0)'
            }).text('暂无信息'))
        }

        SelectDom.show();
    };

    // save input val
    var SaveInput = function () {
        var that = $(this),
            val = JSON.parse(Dom.val()),
            storeId = parseInt(that.attr('data-val'));

        if (val.indexOf(storeId) < 0) {
            val.push(storeId);
            Dom.val(JSON.stringify(val));
        }

        SelectDom.hide();
        InputDom.val('');
        buildPreviewLists();
    };

    var deleteInputVal = function () {
        var that = $(this),
            val = JSON.parse(Dom.val()),
            storeId = parseInt(that.attr('data-val')),
            newData = [];

        console.log(storeId);
        for (var i=0; i<val.length; i++) {
            console.log(parseInt(val[i]));
            if (storeId != parseInt(val[i])) {
                newData.push(parseInt(val[i]));
            }
        }

        Dom.val(JSON.stringify(newData));
        buildPreviewLists();
    };

    var buildPreviewLists = function () {
        var vals = JSON.parse(Dom.val());

        $.ajax({
            url: InputSelectCtrlDataPath,
            type: 'get',
            dataType: 'json',
            async : false,
            data : {storeIds : vals},
            success: function (res) {
                if (res.status) {
                    PreviewDom.empty();
                    for (var i=0; i<res.data.length; i++) {
                        var info = $('<div>').attr({
                            'data-id' : 'InSeCtrl_Preview_Info',
                            'data-val' : res.data[i].storeId
                        }).append(
                            $('<img>').attr('src', res.data[i].avatarTm),
                            $('<span>').text(res.data[i].nickname)
                        ).bind('click', deleteInputVal);
                        PreviewDom.append(info);
                    }

                    return false;
                }

                alert("网络开小差了，请稍后重试");
            }
        })
    };

    return function (dom) {
        var i = new InSeCtrl(dom);
        return i.init();
    };
})($);