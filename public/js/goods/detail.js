// 添加购物车
var addCartFunc = function (dom) {
    var amount = $('input[name="amount"]').val(),
        skuId = dom.attr('data-sku');

    if (skuId == undefined) {
        alert("请选择购买商品的规格信息");
        return false;
    }

    addCart(GOOD_ID, skuId, parseInt(amount));

};
$('a[data-id="btn_cart"]').click(function () {
    addCartFunc($(this));
});

// 增减控件
$('a[data-id="amountCtrl"]').click(function () {
    var type = $(this).attr('data-type'),
        amountDom = $('input[name="amount"]');

    switch (type) {
        case 'plus' :
            var val = parseInt(amountDom.val()) + 1;
            break;
        case 'reduce' :
            var val = parseInt(amountDom.val()) - 1;
            break;
    }

    amountDom.val(val).change();
});

// 数量输入控件
$('input[name="amount"]').change(function () {
    var val = $(this).val(),
        limit = $(this).attr('data-limit');
    if (!/^\d+$/.test(val)) {
        val = 0;
    }
    val = parseInt(val);

    if (val < 0 ) {
        alert("请输入有效的商品数量");
        val = 0;
    }
    if (val > parseInt(limit)) {
        alert("小二正在努力补货中...");
        val = limit;
    }
    $(this).val(val);
});


// 立即购买
var addBuyFunc = function (dom) {
    if (dom.attr('data-sku') == undefined) {
        alert("请选择购买商品的规格信息");
        return false;
    }

    var amount = $('input[name="amount"]').val(),
        sku_id = dom.attr('data-sku'),
        query = {};
    query.id = GOOD_ID;
    query.amount = amount;
    query.sku = sku_id;


    window.location.href = rebuildQueryString(ORDER_ROUT_PAY, query);
};
$('a[data-id="btn_buy"]').click(function () {
    addBuyFunc($(this))
});


$('a[class="pdl_collect"]').click(function () {
    addCollect($(this));
});

// 添加商品到购物车
var addCart = function (detailId, skuId, amount) {
    if (userInfo.length < 1) {
        alert("您尚未登录");
        return false;
    }
    $.ajax({
        url: CART_ROUTE_ADD,
        type:'get',
        data:{id : detailId, skuId : skuId, amount : amount},
        dataType:'json',
        success:function(data){
            if (data.status) {
                alert("添加购物车成功");
                refreshCart();
                return true;
            }
            if (data.msg.length) {
                alert(data.msg);
            } else {
                alert("添加失败");
            }
        }
    });
};


// 删除购物车物品
var removeCart = function (that) {
    if (userInfo.length < 1) {
        return false;
    }
    var id = that.attr('data-info');

    $.ajax({
        url: CART_ROUTE_DELETE,
        type:'get',
        data:{id : id},
        dataType:'json',
        success:function(data){
            if (data.status) {
                refreshCart();
                window.location.reload();
                return true;
            }

            if (data.msg.length) {
                alert(data.msg);
            } else {
                alert("删除失败");
            }
        }
    })
};


var refreshCart = function () {
    var dom = $('div[data-id="cart"]');

    if (dom.length) {
        $.ajax({
            url: CART_ROUTE_INFO,
            type:'get',
            dataType:'json',
            success:function(data){
                if (data.status) {
                    dom.empty().append(
                        $('<a>').attr('href', CART_ROUTE_INDEX)
                            .append(
                                $('<em>').text(data.data.amount),
                                $('<img>').attr('src', '/public/index/images/icon_cart1.png')
                            )
                    );
                    if (data.data.amount < 1) {
                        dom.hide();
                    } else {
                        dom.show();
                    }
                }

            }
        })
    }

};

