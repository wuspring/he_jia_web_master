function addgroup(group,index) {
    var html='       <div class="panel attr-group">\n' +
        '                        <div>\n' +
        '                            <b>'+group.group_name+'</b>\n' +
        '                            <a data-id="'+group.id+'" data-index="'+index+'" href="javascript:void(0);" class="attr-group-delete">×</a>\n' +
        '                        </div>\n' +
        '                        <div class="attr-list">\n' +
        '                            <div style="display: inline-block; width: 200px; margin-top: 0.5rem;">\n' +
        '                                <div class="input-group attr-input-group" style="border-radius: 0px;">\n' +
        '                                    <span class="input-group-addon" style="padding: 0.35rem; font-size: 0.8rem;">规格值</span>\n' +
        '                                    <input placeholder="如红色、白色" class="form-control form-control-sm add-attr-input">\n' +
        '                                    <span class="input-group-btn">\n' +
        '                                        <a data-id="'+group.id+'"  data-index="'+index+'" href="javascript:" class="btn btn-secondary btn-sm add-attr-btn">添加</a>\n' +
        '                                    </span>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>';

    return html;
}

function addgrouplist(group,index) {
    var html='<div class="panel attr-group"><div><b>'+group.group_name+'</b>';

    html+='<a data-id="'+group.id+'" data-index="'+index+'" href="javascript:void(0);" class="attr-group-delete">×</a></div>';
    html+='<div class="attr-list">';
    $(group.attr).each(function (key,attr) {
         html+='<div class="attr-item"><span class="attr-name">'+attr.attr_name+'</span> <a group-id="'+attr.group_id+'" data-id="'+attr.id+'" href="javascript:" class="attr-delete">×</a></div>';

    });

   html+='                            <div style="display: inline-block; width: 200px; margin-top: 0.5rem;">\n' +
        '                                <div class="input-group attr-input-group" style="border-radius: 0px;">\n' +
        '                                    <span class="input-group-addon" style="padding: 0.35rem; font-size: 0.8rem;">规格值</span>\n' +
        '                                    <input placeholder="如红色、白色" class="form-control form-control-sm add-attr-input">\n' +
        '                                    <span class="input-group-btn">\n' +
        '                                        <a data-id="'+group.id+'"  data-index="'+index+'" href="javascript:" class="btn btn-secondary btn-sm add-attr-btn">添加</a>\n' +
        '                                    </span>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>';

    return html;
}

function addAttr(attr) {
    var html='<div class="attr-item"><span class="attr-name">'+attr.attr_name+'</span> <a group-id="'+attr.group_id+'" data-id="'+attr.id+'" href="javascript:" class="attr-delete">×</a></div>';
    return html;
}

function caratTable() {
    $("#guigezu").html("");
    var arrayColumn = new Array();//指定列，用来合并哪些列
    var columnIndex = 0;
    var table = $('<table id="process" border="1" cellpadding="1" cellspacing="0" style="width:100%;padding:5px;"></table>');
    table.appendTo($("#guigezu"));

    var thead = $("<thead></thead>");
    thead.appendTo(table);
    var trHead = $("<tr></tr>");
    trHead.appendTo(thead);

   //创建表头
    var arrayInfor=new Array();
    $.each(attrgroup, function (index, item) {
        arrayColumn.push(columnIndex);
        columnIndex++;
        var td = $("<th>" + item.group_name + "</th>");
        td.appendTo(trHead);
        if (item.attr.length>0){
            arrayInfor.push(item.attr);
        }

    });

    var itemColumHead = $('<th style="width: 100px;">积分</th><th style="width: 100px;">库存</th> ');
    itemColumHead.appendTo(trHead);
    var itemColumHead2 = $("<th >商家编码</th><th >图标</th>");
    itemColumHead2.appendTo(trHead);
    var tbody = $("<tbody></tbody>");
    tbody.appendTo(table);

         var zuheDate = group(arrayInfor);
    if (zuheDate.length > 0) {
        //创建行
        if (goodssku.length>0){
            if (attrgroup.length!=goodssku[0].attr.length){
                goodssku=[];
            }
        }

        $.each(zuheDate, function (index, item) {

            var tr = $("<tr></tr>");
            tr.appendTo(tbody);
            var attrdata=new Array();
            if (item instanceof Array) {
                $.each(item, function (i, values) {

                    if (values instanceof Array){
                       $(values).each(function (v,val) {

                           attrdata.push(val.id);
                           var td = $("<td>" + val.attr_name + "</td>");
                           td.appendTo(tr);
                       });
                    } else{
                        attrdata.push(values.id);
                        var td = $("<td>" + values.attr_name + "</td>");
                        td.appendTo(tr);
                    }


                });
            }else{
                var td = $("<td>" + item.attr_name + "</td>");
                attrdata.push(item.id);
                td.appendTo(tr);
            }
            var sku={id:'',goods_id:'',attr:[],num:0,price:0,integral:0,goods_code:'',picimg:''};
            sku.attr=attrdata;
            var issku=true;

            $(goodssku).each(function (key,item) {
                if(item.attr.sort().toString()==attrdata.sort().toString()){
                    issku=false;
                    var td11 = $('<td ><input name="sku_integral" data-attr=\''+JSON.stringify(item.attr)+'\' class="form-control" type="number" value="'+item.integral+'"></td>');
                    td11.appendTo(tr);
                    var td2 = $('<td ><input name="sku_num" data-attr=\''+JSON.stringify(item.attr)+'\' class="form-control" type="number" value="'+item.num+'"></td>');
                    td2.appendTo(tr);
                    var td3 = $('<td ><input name="sku_code" data-attr=\''+JSON.stringify(item.attr)+'\' class="form-control" type="text" value="'+item.goods_code+'"></td>');
                    td3.appendTo(tr);
                    var td4 = $('<td ><div class="input-group input-group-sm"><input type="file" style="display: none" id="skuimgfile'+key+'"><input name="sku_picimg" data-attr=\''+JSON.stringify(item.attr)+'\' class="input-sm form-control"  value="'+item.picimg+'"> <span class="input-group-btn"><a href="javascript:" data-toggle="tooltip" data-placement="bottom" title="上传文件" class="btn btn-secondary upload-attr-pic"><span class="fa fa-cloud-upload"></span></a></span> <span class="input-group-btn"><a href="javascript:" data-toggle="tooltip" data-placement="bottom" title="从文件库选择" class="btn btn-secondary select-attr-pic"><span class="fa fa-th"></span></a></span> <span class="input-group-btn"><a href="javascript:" data-toggle="tooltip" data-placement="bottom" title="删除文件" class="btn btn-secondary delete-attr-pic"><span class="fa fa-trash-o"></span></a></span></div>');
                    td4.appendTo(tr);
                }
            });
            if (issku){
                var td11 = $('<td ><input name="sku_integral" data-attr=\''+JSON.stringify(attrdata)+'\' class="form-control" type="number" value="0"></td>');
                td11.appendTo(tr);
                var td2 = $('<td ><input name="sku_num" data-attr=\''+JSON.stringify(attrdata)+'\' class="form-control" type="number" value="0"></td>');
                td2.appendTo(tr);
                var td3 = $('<td ><input name="sku_code" data-attr=\''+JSON.stringify(attrdata)+'\' class="form-control" type="text" value=""></td>');
                td3.appendTo(tr);
                var td4 = $('<td ><div class="input-group input-group-sm"><input type="file" style="display: none" id="skuimgfile'+goodssku.length+'"><input name="sku_picimg" data-attr=\''+JSON.stringify(attrdata)+'\' class="input-sm form-control" > <span class="input-group-btn"><a href="javascript:" data-toggle="tooltip" data-placement="bottom" title="上传文件" class="btn btn-secondary upload-attr-pic"><span class="fa fa-cloud-upload"></span></a></span> <span class="input-group-btn"><a href="javascript:" data-toggle="tooltip" data-placement="bottom" title="从文件库选择" class="btn btn-secondary select-attr-pic"><span class="fa fa-th"></span></a></span> <span class="input-group-btn"><a href="javascript:" data-toggle="tooltip" data-placement="bottom" title="删除文件" class="btn btn-secondary delete-attr-pic"><span class="fa fa-trash-o"></span></a></span></div>');
                td4.appendTo(tr);
                goodssku.push(sku);
            }


        });

    }


    //结束创建Table表
    arrayColumn.pop();//删除数组中最后一项
    //合并单元格
    $(table).mergeCell({
        // 目前只有cols这么一个配置项, 用数组表示列的索引,从0开始
        cols: arrayColumn
    });
}

function group(groupdata) {
    var len = groupdata.length;
    if (len >= 2) {
        var arr1 = groupdata[0];
        var arr2 = groupdata[1];
        var len1 = groupdata[0].length;
        var len2 = groupdata[1].length;
        var newlen = len1 * len2;
        var temp = new Array(newlen);
        var index = 0;
        for (var i = 0; i < len1; i++) {
            for (var j = 0; j < len2; j++) {
                temp[index]=[];
                temp[index].push(arr1[i]);
                temp[index].push(arr2[j]);
                //  temp[index] = arr1[i].attr_name + "," + arr2[j].attr_name;
                index++;
            }
        }
        var newArray = new Array(len - 1);
        newArray[0] = temp;
        if (len > 2) {
            var _count = 1;
            for (var i = 2; i < len; i++) {
                newArray[_count] = groupdata[i];

                _count++;
            }
        }

        return group(newArray);
    }
    else {
        return groupdata[0];
    }
}

$.fn.mergeCell = function (options) {
    return this.each(function () {
        var cols = options.cols;
        for (var i = cols.length - 1; cols[i] != undefined; i--) {
            // fixbug console调试
            // console.debug(cols[i]);
            mergeCell($(this), cols[i]);
        }
        dispose($(this));
    });
};
function mergeCell($table, colIndex) {
    $table.data('col-content', ''); // 存放单元格内容
    $table.data('col-rowspan', 1); // 存放计算的rowspan值 默认为1
    $table.data('col-td', $()); // 存放发现的第一个与前一行比较结果不同td(jQuery封装过的), 默认一个"空"的jquery对象
    $table.data('trNum', $('tbody tr', $table).length); // 要处理表格的总行数, 用于最后一行做特殊处理时进行判断之用
    // 进行"扫面"处理 关键是定位col-td, 和其对应的rowspan
    $('tbody tr', $table).each(function (index) {
        // td:eq中的colIndex即列索引
        var $td = $('td:eq(' + colIndex + ')', this);
        // 取出单元格的当前内容
        var currentContent = $td.html();
        // 第一次时走此分支
        if ($table.data('col-content') == '') {
            $table.data('col-content', currentContent);
            $table.data('col-td', $td);
        } else {
            // 上一行与当前行内容相同
            if ($table.data('col-content') == currentContent) {
                // 上一行与当前行内容相同则col-rowspan累加, 保存新值
                var rowspan = $table.data('col-rowspan') + 1;
                $table.data('col-rowspan', rowspan);
                // 值得注意的是 如果用了$td.remove()就会对其他列的处理造成影响
                $td.hide();
                // 最后一行的情况比较特殊一点
                // 比如最后2行 td中的内容是一样的, 那么到最后一行就应该把此时的col-td里保存的td设置rowspan
                if (++index == $table.data('trNum'))
                    $table.data('col-td').attr('rowspan', $table.data('col-rowspan'));
            } else { // 上一行与当前行内容不同
                // col-rowspan默认为1, 如果统计出的col-rowspan没有变化, 不处理
                if ($table.data('col-rowspan') != 1) {
                    $table.data('col-td').attr('rowspan', $table.data('col-rowspan'));
                }
                // 保存第一次出现不同内容的td, 和其内容, 重置col-rowspan
                $table.data('col-td', $td);
                $table.data('col-content', $td.html());
                $table.data('col-rowspan', 1);
            }
        }
    });
}
// 同样是个private函数 清理内存之用
function dispose($table) {
    $table.removeData();
}