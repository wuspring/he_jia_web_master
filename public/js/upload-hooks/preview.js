/**
 *  upload hook : preview image
 *
 *  @author  daorli
 */

// .eg config
// config  = [
//     {category : 'hospital', id : 'picture', type : 'pictures'},
// ];

// preview config
var previewCtrl = function (config) {
    for (var i=0; i<config.length; i++) {
        var info = config[i];
        var pictureInput = $('input[id="' + info.category  + '-' + info.id + '"]');
        pictureInput.attr({'data-id' : info.id, 'data-type' : info.type, type : 'hidden'});
        pictureInput.on('setValueSuccess', function (){
            var that = $(this),
                type = that.attr('data-type');
            console.log(uploadFiles[type]);
            that.val(uploadFiles[type]);
            viewFiles(type);
        });
        pictureInput.parent('div').append($('<div>').attr('data-id', info.type + '-view'));
        pictureInput.parent('div').append($('<div>').attr('data-id', info.id + '_build').css('display', 'none'));
        pictureInput.parent('div').append($('<button>').attr({'type': 'button', 'data-id': 'upload', 'data-info' : info.id}).text('上传').bind('click',uploadStart));

        // pictureInput.parent('div').append($('<button>').attr({'type': 'button', 'data-id': 'reset', 'data-info': info.id}).css({'margin-left' : '10px'}).text('重置').bind('click',resetUpload));

        var inputVal = pictureInput.val();
        uploadFiles[pictureInput.attr('data-type')] = inputVal.length ? pictureInput.val().split(',') : [];
        viewFiles(pictureInput.attr('data-type'));
    }
};

// preview lists shadow
var viewFiles = function (uploadType) {
    var viewDom = $('div[data-id="'+ uploadType +'-view"]');
    viewDom.empty();
    var list = uploadFiles[uploadType];
    for (var i=0; i<list.length; i++) {
        var img = $('<img>').attr({src : list[i], width : '90px',}).css({'margin-right': '10px', 'margin-bottom': '10px'});
        img.bind('click', function () {
            var path = $(this).attr('src');
            window.open(path);
        });
        viewDom.append(img);
    }
};

// upload button
var uploadStart = function() {
    var id = $(this).attr('data-info'),
        dom = $("input[data-id='" + id + "']");

    uploadFiles[dom.attr('data-type')] = [];
    // viewFiles(dom.attr('data-type'));
    dom.val('');

    upload(uploadPath, dom);
};

// reset button
var resetUpload = function () {
    var id = $(this).attr('data-info'),
        dom = $('input[data-id="' + id + '"]');
    uploadFiles[dom.attr('data-type')] = [];
    viewFiles(dom.attr('data-type'));

    dom.val('');
};


