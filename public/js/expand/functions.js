// 数组冒泡排序
var numArraySort = function (array)
{
    var tmp;
    for(i=0; i<array.length-1; i++) {
        for(j=0;j<array.length-1-i; j++){
            if(parseInt(array[j])<parseInt(array[j+1])) {
                tmp = array[j];
                array[j] = array[j+1];
                array[j+1] = tmp;
            }
        }
    }
    return array;
};

/**
 * 商品数量增减控件
 * @author daorli
 *
 * @param id
 * @param value
 * @param valueMax
 * @returns {jQuery}
 */
var createIncDecButton = function (id, value, valueMax, callback) {
    valueMax = Math.abs(valueMax);
    var input = $('<input>').attr({'type': 'number', 'data-id' : id, 'data-max' : valueMax}).val(value),
        inc = $('<label>').addClass('icon icon_plus').text('+').attr({'data-id' : id, 'data-max' : valueMax}),
        dec = $('<label>').addClass('icon icon_reduce').text('-').attr({'data-id' : id}),
        callbackFunc = (typeof (callback) == 'function') ? callback : function(){};

    input.bind('blur', function () {
        var value = parseInt($(this).val()),
            max = parseInt($(this).attr('data-max')),
            setValue = value > max ? max : value;

        if (isNaN(value)) {
            setValue = 1;
        }

        if (setValue < 0) {
            setValue = 0;
        }

        $(this).val(setValue);
        callbackFunc($(this));
    });

    inc.bind('click', function () {
        var id = $(this).attr('data-id'),
            maxValue = $(this).attr('data-max'),
            input = $('input[data-id="' + id + '"]');

        var value = parseInt(input.val()) + 1;
        value = value > maxValue ? maxValue : value;
        input.val(value);
        callbackFunc(input);
    });

    dec.bind('click', function () {
        var id = $(this).attr('data-id'),
            input = $('input[data-id="' + id + '"]');

        var value = parseInt(input.val()) - 1;
        value = value < 0 ? 0 : value;
        input.val(value);
        callbackFunc(input);
    });

    return $('<div>').addClass('box').append(
        inc,
        $('<div>').addClass('am-form-group').append(input),
        dec
    );
};

window.alert = function(msg, callback) {
    var height = window.screen.height;
    var dom = $('<div>').attr({
        id : 'globalAlterShadow',
        tabindex : '-1',
        role : 'dialog',
        'aria-labelledby' : 'myModalLabel',
        'style' : 'padding-right: 17px; display: block;z-index:1052'
    }).addClass('modal fade show'),
        content = $('<p>').text(msg),
        button = $('<button>').attr({
            'type' : 'button',
            'class' : 'btn btn-warning'
        }).css('width', '62%').text('确定'),
        domBody;

    domBody = $('<div>').addClass('modal-content').append(
            $('<div>').addClass('modal-header').append($('<h4>').addClass('modal-title pl-3').text('提醒')),
            $('<div>').addClass('modal-body pt-5 pb-4').append(content),
            $('<div>').addClass('modal-footer').append(button)
        );

    button.bind('click', function () {
        dom.modal('hide');
        setTimeout(function () {
            dom.remove();
            if (typeof (callback) == 'function') {
                callback();
            }
        }, 1500);
    });

    $('body').append(
        dom.append(
            $('<div>').attr({
                'class' : 'modal-dialog',
                'role' : 'document'
            }).append(domBody).css({
                'max-width': '380px',
                'margin-top' : ((height - 256 * 1.5 ) / 2) + 'px'
            })
        )
    );

    dom.modal();
    dom.next('.modal-backdrop.fade.show').css({
        'z-index' : 1051
    });
};

var parseQueryString = function (url) {
    var request = url.split("?"),
        path = request[0],
        items = request[1].split("&");
    var query = {}, param;
    for(var i = 0, l = items.length; i < l; i++){
        param = items[i].split("=");
        query[param[0]] = param[1];
    }

    return {path : path, query : query};
};

var rebuildQueryString = function (path, query) {
    var param = [], splitKey;
    for (var i in query) {
        param.push(i + '=' + query[i]);
    }

    splitKey = path.indexOf('?') > -1 ? '&' : '?';
    return path + splitKey + param.join('&');
};

var getDeviceInfo = function () {
    var width = document.documentElement.scrollWidth || document.body.scrollWidth,
        height = document.documentElement.scrollHeight || document.body.scrollHeight;

    return {
        width : width,
        height : height
    };
};

var isJSON = function (str) {
    if (typeof str == 'string') {
        try {
            var obj=JSON.parse(str);
            if(typeof obj == 'object' && obj ){
                return true;
            }else{
                return false;
            }

        } catch(e) {
            return false;
        }
    }
};

var autoDomHeight = function (dom) {
  var wHeight =  window.screen.height,
      content = dom.find('.modal-content').eq(0),
      dHeight = content.height();

    if (dHeight) {
        content.css('margin-top', ((wHeight - 1.1 * dHeight) / 2) + 'px');
        return false;
    }

    console.log('未获取到弹框高度');
};

