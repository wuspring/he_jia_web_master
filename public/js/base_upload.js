/**
 *  attachment uploader ctrl
 *
 *  @author  daorli
 */
var uploadFiles = {};

var upload = (function ($) {
    var UploadClass = function (url, dom) {
        this.url = url;
        this.infoDom = dom;
        this.infoDomType = dom.attr('data-type');

        if (typeof (uploadFiles[this.infoDomType]) == 'undefined') {
            uploadFiles[this.infoDomType] = [];
        }
        this.container = uploadFiles[this.infoDomType];

        // auto create base dom
        this.Dom = $('div[data-id="' + dom.attr('data-id') + '_build"]');
        if (this.Dom.length < 1) {
            this.Dom = $('<div>').attr('data-id', dom.attr('data-id') + '_build"]')
                .hide();
            $('body').append(this.Dom);
        }
    };

    UploadClass.prototype = {
        init : function ()
        {
            this.Dom.empty();

            this._build();
        },
        _build : function () {
            var that = this;
            $('form[data-id="upload"]').each(function () {
                $(this).remove();
            });

            that.form = $('<form>').attr('enctype', 'multipart/form-data').attr('method', 'post').attr('data-id', 'upload');

            that.input = $('<input>').attr('type', 'file').attr({'name': 'img', 'accept' : 'image/gif, image/jpeg, image/png, video/*'});

            if (typeof(MORE_FILES) != 'undefined') {
                that.input.attr({'name' : 'img[]', 'multiple' : "multiple"});
            }
            that.input.bind('change', function () {
                that._upload();
            });
            that.form.append(that.input);
            that.Dom.append(that.form);
            that.input.click();

        },
        _upload : function () {
            var that = this,
                form = that.Dom.find('form[data-id="upload"]').eq(0);

            $.ajax({
                url: that.url,
                type: "POST",
                data:  new FormData(form[0]),
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    data = JSON.parse(data);

                    switch (typeof (data.data)) {
                        case 'string' :
                            that.container.push(data.data);
                            break;
                        case 'object' :
                            $.extend(that.container, that.container.concat(data.data));
                            break;
                    }

                    that._setValue();
                    alert("上传成功！");
                },
                error: function () {
                    alert("上传失败！");
                }
            });
        },
        _setValue : function () {
            // this.infoDom.val(this.container);
            this.infoDom.trigger('setValueSuccess');
        }
    };

    return function (url, dom) {
        var u = new UploadClass(url, dom);
        return u.init();
    };
})($);