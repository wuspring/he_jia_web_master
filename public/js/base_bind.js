// 爆款预约成功
var orderPriceSuccess = function (key, type, callback) {
    var data = {};

    if (typeof (callback) != 'function') {
        callback = function () {};
    }

    data.key = key;
    data.type = type;

    $.ajax({
        url: JOIN_ORDER_PRICE_SUCCESS,
        data: data,
        success: function( data ) {
            var dom = $('#globalLoginShadow');

            if (isJSON(data)) {
                var obj = JSON.parse(data);
                alert (obj.msg, callback);
                return false;
            }

            if (dom != undefined) {
                dom.empty().html(data).modal();
                autoDomHeight(dom);
                return false;
            }
        },
    });
};

$(function () {
    var globalLoginShadow = $('#globalLoginShadow');

    // 预定商品 领取系统优惠券
    $('[data-order]').click(function(){
        var that = $(this),
            data = {};

        data.goodId = that.attr('data-val');
        data.type = that.attr('data-type');

        window.location.href = rebuildQueryString(REQUEST_CREATE_ORDER, data);
    });

    // 爆款预约
    $('[data-order-price]').click(function(){
        var that = $(this),
            data = {};

        data.goodId = that.attr('data-val');
        data.type = that.attr('data-type');

        if (!Object.keys(userInfo).length) {

            $.ajax({
                url: JOIN_ORDER_PRICE,
                data: data,
                success: function( data ) {
                    var dom = $('#globalLoginShadow');

                    if (isJSON(data)) {
                        var obj = JSON.parse(data);

                        if (obj.status) {

                            dom.modal('hide');
                            orderPriceSuccess(obj.data.key, function () {
                                window.location.href = obj.data.href;
                            });
                            return false;
                        }

                        alert(obj.msg, function () {
                            dom.modal('hide');
                        });

                        return false;
                    }

                    if (dom != undefined) {
                        dom.empty().html(data).modal();
                        autoDomHeight(dom);
                        return false;
                    }
                },
            });
            return false;
        }

        $.ajax({
            url: REQUEST_CREATE_P_ORDER,
            data: data,
            success: function( res ) {
                if (isJSON(res)) {
                    var obj = JSON.parse(res);
                    var dom = $('#globalCouponSuccessShadow');
                    if(dom!=undefined){
                        dom.modal('hide');
                    }
                    if (obj.status) {
                        orderPriceSuccess(obj.data.key, obj.data.type, function () {
                            window.location.href = obj.data.href;
                        });

                        return false;
                    }

                    alert(obj.msg);
                    return false;
                }

            },
        });

        // window.location.href = href;
    });



    //点击领取优惠劵
    $("[data-id='getCoupin']").on('click',function () {
        var data = {};

        data.id = $(this).attr('data-val');
        data.storeId = $(this).attr('data-store');

        if (!Object.keys(userInfo).length) {

            $.ajax({
                url: JOIN_STORE_COUPON,
                data: data,
                success: function( data ) {
                    var dom = $('#globalLoginShadow');

                    if (isJSON(data)) {
                        var obj = JSON.parse(data);

                        if (obj.status) {
                            orderPriceSuccess(obj.data.key, obj.data.type, function () {
                                window.location.href = obj.data.href;
                            });
                            return false;
                        }

                        alert (obj.msg);

                        return false;
                    }

                    if (dom != undefined) {
                        dom.empty().html(data).modal();
                        autoDomHeight(dom);
                        return false;
                    }
                },
            });

            return false;
        }

        $.get(GET_STORE_COUPON, data, function (res) {
            if (res.code==1) {
                // alert(res.msg);
                orderPriceSuccess(res.data.key, res.data.type, function () {
                    window.location.href = res.data.href;
                });
                // window.location.reload();
                return false;
            }

            alert(res.msg)

        },'json');
    });

    //点击预约店铺
    $("[data-id='getStore']").on('click',function () {
        var data = {};
        data.id = $(this).attr('data-val');
        data.type = $(this).attr('data-type');

        if (!Object.keys(userInfo).length) {
            $.ajax({
                url: GET_STORE_APPOINT,
                data: data,
                success: function( data ) {
                    var dom = $('#globalLoginShadow');

                    if (isJSON(data)) {
                        var obj = JSON.parse(data);

                        if (obj.status) {
                            orderPriceSuccess(obj.data.key, obj.data.type, function () {
                                window.location.href = obj.data.href;
                            });
                            return false;
                        }

                        alert(obj.msg);

                        return false;
                    }

                    if (dom != undefined) {
                        dom.empty().html(data).modal();
                        autoDomHeight(dom);
                        return false;
                    }
                },
            });

            return false;
        }

        $.get(GET_STORE_APPOINT, data, function (res) {
            if (res.status) {
                // alert(res.msg);
                orderPriceSuccess(res.data.key, res.data.type, function () {
                    window.location.href = res.data.href;
                });
                // window.location.reload();
                return false;
            }

            alert(res.msg)

        },'json');
    });

    //装修学堂的 问题提交
    $("[data-id='submitProblem']").on('click',function () {
        var  url=SUBMIT_PROBLEM;
        var text=$(".password_form_li").children('.textarea').val();

        if (text.length < 1) {
            alert("请输入您提问的问题");
            return false;
        }
        $.get(url,{text:text},function (res) {

            if (res.code==1 || res.code==0){
                alert(res.msg);
                window.location.reload();
            }
            if (res.code==2){
                alert(res.msg, function () {
                    window.location.href=LOGIN_PATH;
                });
            }

        },'json');

    })


    $('[ data-id="goDetail"]').click(function () {
        var href = $(this).attr('data-href');

        if (href.length) {
            window.location.href = href;
        }
    })
});

