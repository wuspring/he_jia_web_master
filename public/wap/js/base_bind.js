window.alert = function(msg, callback, config) {
    config = config == undefined ? {} : config;

    var bottonText = config.button == undefined ? '确定' : config.button,
        dom = $('<div>').attr({
            id : 'globalAlterShadow',
            tabindex : '-1',
            role : 'dialog',
            'aria-labelledby' : 'myModalLabel',
            'style' : 'display: block;z-index:1052'
        }).addClass('modal modal_explain modal_explain_sm fade show'),
        content = $('<p>').text(msg),
        button = $('<button>').attr({
            'type' : 'button',
            'class' : 'btn'
        }).text(bottonText),
        domBody;
    title = config.title == undefined ? '提醒' : config.title;

    domBody = $('<div>').addClass('modal-content').append(
        $('<div>').addClass('modal-header').append($('<p>').addClass('p_title').text(title)),
        $('<div>').addClass('modal-body').append(content),
        $('<div>').addClass('modal-footer').append(button)
    );

    button.bind('click', function () {
        dom.modal('hide');
        setTimeout(function () {
            dom.remove();
            if (typeof (callback) == 'function') {
                callback();
            }
        }, 1500);
    });

    $('body').append(
        dom.append(
            $('<div>').attr({
                'class' : 'modal-dialog',
                'role' : 'document'
            }).append(domBody).css({
                'max-width': '380px',
                // 'margin-top' : '3rem'
                'margin-top' : '12rem'
            })
        )
    );

    dom.modal();
    dom.next('.modal-backdrop.fade.show').css({
        'z-index' : 1051
    });
};

var autoDomHeight = function (dom) {
    var wHeight =  window.screen.height,
        content = dom.find('.modal-content').eq(0),
        dHeight = content.height();
console.log(wHeight);console.log(dHeight);
    if (dHeight) {
        content.css('margin-top', ((wHeight - 1.5 * dHeight) / 2) + 'px');
        return false;
    }

    console.log('未获取到弹框高度');
};

// 预定店铺
$('body').on('click','[data-id="order-store"]',function () {
    var data = {};

    data.ticketId = $(this).attr('data-ticket');
    data.storeId = $(this).attr('data-store');

    var dom = $('#globalLoginShadow');

    $.get(GET_ORDER_STORE, data, function (res) {
        if (isJSON(res)) {
            var obj = JSON.parse(res);

            if (obj.status) {
                alertSuccessHtml(obj.data.href);
                return false;
            }

            alert (obj.msg);

            return false;
        }

        if (dom != undefined) {
            dom.empty().html(res).modal();
            autoDomHeight(dom);
            return false;
        }

    });
});

// 爆款预约
$('body').on('click','[data-id="get-coupon"]',function () {
    var data = {};

    data.ticketId = $(this).attr('data-ticket');
    data.goodsId = $(this).attr('data-good');

    if ($(this).attr('data-source') != undefined) {
        data.source = 1;
    }

    var dom = $('#globalLoginShadow');

    $.get(GET_GOOD_PRICE, data, function (res) {
        if (isJSON(res)) {
            var obj = JSON.parse(res);

            if (obj.status) {
                alertSuccessHtml(obj.data.href, obj.data.goodsId, 'get-coupon');
                return false;
            }

            alert (obj.msg);

            return false;
        }

        if (dom != undefined) {
            dom.empty().html(res).modal();
            autoDomHeight(dom);
            return false;
        }

    });
});

// 领取优惠券
$('body').on('click','[data-id="store-coupon"]',function () {
    var that = $(this),
        data = {};

    data.couponId = $(this).attr('data-coupon');
    data.ticketId = $(this).attr('data-ticket');

    var dom = $('#globalLoginShadow');

    $.get(GET_STORE_COUPONS, data, function (res) {
        if (isJSON(res)) {
            var obj = JSON.parse(res);

            if (obj.status) {
                alertCouponSuccessHtml(obj.data.href, obj.data.couponId);
                return false;
            }

            alert (obj.msg);

            return false;
        }

        if (dom != undefined) {
            dom.empty().html(res).modal();
            autoDomHeight(dom);
            return false;
        }

    });
});

// 预定商品 预存享特价
$('body').on('click','[data-id="order-create"]',function () {
    var data = {};

    data.ticketId = $(this).attr('data-ticket');
    data.goodsId = $(this).attr('data-good');

    var dom = $('#globalLoginShadow');

    $.get(GET_ORDER_GOODS, data, function (res) {
        if (isJSON(res)) {
            var obj = JSON.parse(res);

            if (obj.status) {
                window.location.href = obj.data.location;
                return false;
            }

            alert (obj.msg);

            return false;
        }

        if (dom != undefined) {
            dom.empty().html(res).modal();
            autoDomHeight(dom);
            return false;
        }

    });
});

var alertSuccessHtml = function(href, id, type) {
    var dom = $('#globalSuccessShadow');
    $.get(href, {}, function (res) {
        dom.empty().html(res).modal();
        autoDomHeight(dom);
    });

    if (type != undefined && id != undefined) {
        // 领取减库存
        $('[data-id="storage"][data-good="' + id  + '"]').each(function () {
            var num = $(this).text();

            if (parseInt(num)) {
                $(this).text(parseInt(num) - 1);
            }
        });

        switch (type) {
            case 'get-coupon' :
                $('[data-id="' + type + '"][data-good="' + id  + '"]').each(function () {
                    var text = $(this).text();

                    $(this).text('已' + text.substr(-2)).addClass('disabled');
                });
                break;
        }

    }
};

var alertCouponSuccessHtml = function(href, couponId) {
    var dom = $('#globalCouponSuccessShadow');
    $.get(href, {}, function (res) {
        dom.empty().html(res).modal();
        autoDomHeight(dom);
    });

    if (couponId != undefined) {
        // 领取减库存
        $('[data-id="storage"][data-coupon="' + couponId  + '"]').each(function () {
            var num = $(this).text();

            if (parseInt(num)) {
                $(this).text(parseInt(num) - 1);
            }
        });

        $('[data-id="store-coupon"][data-coupon="' + couponId  + '"]').each(function () {
            var html = $(this).html();
            html = html.replace(/立即<br>领取/, '已<br>领取');

            $(this).html(html);
            $(this).parents('.com_coupon_con').addClass('ed');
        });
    }
};

var isJSON = function (str) {
    if (typeof str == 'string') {
        try {
            var obj=JSON.parse(str);
            if(typeof obj == 'object' && obj ){
                return true;
            }else{
                return false;
            }
        } catch(e) {
            return false;
        }
    }
};

var refreshButton = function () {

    // 领取优惠券
    $('[data-id="store-coupon"]').each(function () {
        var that = $(this),
            id = that.attr('data-coupon');
        if (hasCouponIds.indexOf(id) > -1) {
            var html = that.html();
            html = html.replace(/立即<br>领取/, '已<br>领取');
            that.html(html).parent('div').addClass('ed');
        }
    });

    // 爆款预约
    $('[data-id="get-coupon"]').each(function () {
        var that = $(this),
            id = that.attr('data-good');

        if (hasCouponGoods.indexOf(id) > -1) {
            var text = that.text();

            if (/预约$/.test(text)) {
                $(this).text('已' + text.substr(-2)).addClass('disabled');
            }
        }
    });

    // 预定商品
    $('[data-id="order-create"]').each(function () {
        var that = $(this),
            id = that.attr('data-good');

        if (hasCouponGoods.indexOf(id) > -1) {
            var text = that.text();
            $(this).text('已' + text.substr(-2)).addClass('disabled');
        }
    });
};

function isWeiXin(){
    var ua = window.navigator.userAgent.toLowerCase();
    if(ua.match(/MicroMessenger/i) == 'micromessenger'){
        return true;
    }else{
        return false;
    }
}