<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\base\Object;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property string $gc_ids
 * @property string $assignment
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_type
 * @property string $end_time
 * @property string $modifyTime
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $updated_at;
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_TYPE = 1;

    public $created_at;

    const ASSIGNMENT_HOU_TAI = '后台用户';
    const ASSIGNMENT_GUAN_LI_YUAN = '系统管理员';
    const ASSIGNMENT_BIAN_JI = '网站编辑';

    const USER_TYPE_FOREVER = 1;
    const USER_TYPE_LIMBO = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getUserTypeDic()
    {
        return [
            self::USER_TYPE_FOREVER => '永久店铺',
            self::USER_TYPE_LIMBO => '临时店铺'
        ];
    }

    public function getUserTypeInfo()
    {
        $dic = $this->getUserTypeDic();
        return isset($dic[$this->user_type]) ? $dic[$this->user_type] :  '-';
    }

    public function attributeLabels()
    {
        return [
            'username' => '用户名',
            'sex' => '性别',
            'status' => '状态',
            'last_visit' => '最后访问时间',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username','unique','message'=>'用户名已占用'],
            [[ 'sex', 'status', 'userType','provinces_id', 'user_type', 'change_city'], 'integer'],
            [['birthday', 'createTime', 'modifyTime', 'last_visit', 'created_at', 'nickname', 'gc_ids', 'end_time'], 'safe'],
            [['username', 'password', 'avatar','avatarTm', 'email', 'authkey', 'accessToken', 'assignment'], 'string', 'max' => 255]
        ];
    }

    public function getCityInfo()
    {
        $name = '';
        if (strlen($this->provinces_id)) {
            $pro = Provinces::findOne($this->provinces_id);
            $pro and $name = $pro->cname;
        }
        return $name;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * 所属栏目信息
     */
    public function getGcInfo()
    {
        $storeGcs = StoreGcScore::findAll([
            'user_id' => $this->id
        ]);

        $info = [];
        if ($storeGcs) {
            $gcIds = arrayGroupsAction($storeGcs, function ($storeGc) {
                return $storeGc->gc_id;
            });

            $info = GoodsClass::find()->where(['in', 'id', $gcIds])->all();
        }
        return $info;
    }
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authkey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {

        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->authkey = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getStatusDic()
    {
        return ['1'=>'正常','0'=>'禁用'];
    }

    public function getStatusInfo()
    {
        $dic = $this->getStatusDic();
        return isset($dic[$this->status]) ? $dic[$this->status] : '-';
    }

    public function getAssignmentDic()
    {
        return [
            self::ASSIGNMENT_HOU_TAI => '商户',
            self::ASSIGNMENT_GUAN_LI_YUAN => '管理员'
        ];
    }

    public function getAdminAssignmentDic()
    {
        return [
            self::ASSIGNMENT_GUAN_LI_YUAN => '管理员',
            self::ASSIGNMENT_BIAN_JI => '网站编辑'
        ];
    }

    public function getAssignmentInfo()
    {
        $dic = $this->getAssignmentDic();
        return isset($dic[$this->assignment]) ? $dic[$this->assignment] : '-';
    }

    /**
     *  获取StoreInfor表中的特价商品
     */
    public  function getStoreInfor($userId){
        $store=StoreInfo::findOne(['user_id'=>$userId]);
        $arr=json_decode($store->order_goods_id);
        $goodArr=array();
        if (!empty($arr)){
          $goodArr=Goods::find()->andWhere(['in','gc_id',$arr])->limit(4)->all();
        }
       return $goodArr;
    }
    /**
     *  获取StoreInfor表中的 tab-tax
     */
    public function  getStoreInforTab($userId){
        $store=StoreInfo::findOne(['user_id'=>$userId]);
        $stArr=json_decode($store->tab_text);
        $tabArr=[];
          foreach ($stArr as $k=>$v){
              if ($k<3){
                 $exStr=explode('@',$v);
                  $tabArr[$exStr[0]]=$exStr[1];
              }
          }
         return $tabArr;
    }


    public function refreshCache()
    {
        $this->modifyTime = date('Y-m-d H:i:s');

        return $this->save();
    }
}
