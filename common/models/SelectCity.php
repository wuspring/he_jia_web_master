<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "select_city".
 *
 * @property integer $id
 * @property integer $provinces_id
 * @property string $name
 * @property string $create_time
 */
class SelectCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'select_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provinces_id'], 'integer'],
            [['create_time'], 'safe'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'provinces_id' => 'Provinces ID',
            'name' => 'Name',
            'create_time' => 'Create Time',
        ];
    }

    /**
     *  获取城市名字
     */
    public  function  getCityName(){

        $provinces=Provinces::findOne($this->provinces_id);

        return  empty($provinces)?'-':$provinces->cname;
     }

    /**
     *  获取所有城市
     *   编号=>名字
     */
   public static  function  getAllCityName(){

       $selctCity=SelectCity::find()->all();
       $selctCityArr=[];
       foreach ($selctCity as $k=>$v){
              $pro=Provinces::findOne($v->provinces_id);
               $selctCityArr[$v->provinces_id]=$pro->cname;
       }
       return $selctCityArr;
   }


     /**
      *  获取城市列表
      *  $arrCity=['河南'=>[['','''],['','']]]
      */
     public  static function  getProvinceCity(){

         $selectCity=SelectCity::find()->all();
         $arrCity=[];
         foreach ($selectCity as $k=>$v){
             $provinces=Provinces::findOne($v->provinces_id);
             $provinces_parent=Provinces::findOne($provinces->upid);
             $arrCity[$provinces_parent->cname][]=[$v->provinces_id,$provinces->cname];
         }
         return $arrCity;

     }








}
