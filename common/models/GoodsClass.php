<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "goods_class".
 *
 * @property string $id
 * @property string $name
 * @property string $fid
 * @property string $icoImg
 * @property integer $sort
 * @property string $createTime
 * @property string $modifyTime
 * @property string $keywords
 */
class GoodsClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fid', 'sort', 'is_filter','is_del','is_show'], 'integer'],
            [['createTime', 'modifyTime', 'keywords'], 'safe'],
            [['name', 'icoImg'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '名称'),
            'fid' => Yii::t('app', '父级'),
            'icoImg' => Yii::t('app', '图标'),
            'sort' => Yii::t('app', '排序'),
            'createTime' => Yii::t('app', 'Create Time'),
            'modifyTime' => Yii::t('app', 'Modify Time'),
            'is_filter' => Yii::t('app', '筛选条件'),
            'is_del' => Yii::t('app', '软删除'),
            'is_show' => Yii::t('app', '是否显示'),
        ];
    }

    public  function getParent($pid){
        $treeData= array();
        $tree = GoodsClass::find()->where(['fid' => $pid,'is_del'=>0])->orderBy('sort asc')->all();
        foreach ($tree as $key => $value) {
            $treeData []= array(
                'id'=>$value->id,
                'name'=>$value->name,
                'tree'=>$this->getParent($value->id),
            );
        }
        return  $treeData;
    }
    public static function displayLists($pid = 0, $selectid = 1,$str="") {
        $result = self::getLists($pid);

        foreach ($result as $key => $value) {
            $selected = "";
            if ( $selectid == $value->id ) {
                $selected = 'selected';
            }
            $count = GoodsClass::find()->where(['fid' => $value->id])->count();
            if ($count == 0) {
                $str .= '<option value='.$value->id.' '.$selected.'>'.$value->name.'</option>';
            }else{
                $str .= '<optgroup label='.$value->name.'></optgroup>';
            }

        }
        return $str .= '</optgroup></select>';
    }

    public static function displayListsselt($pid = 0, $selectid = 1,$str="") {
        $result = self::getLists($pid);

        foreach ($result as $key => $value) {
            $selected = "";
            if ( $selectid == $value->id ) {
                $selected = 'selected';
            }

            $str .= '<option value='.$value->id.' '.$selected.'>'.$value->name.'</option>';


        }
        return $str .= '</optgroup></select>';
    }

    //从顶层逐级向下获取子类
    public static function getLists($pid = 0, &$lists = array(), $deep = 1) {
        $parentData = GoodsClass::find()->where(['fid'=>$pid])->orderBy('sort asc')->all();
        if (!empty($parentData)) {
            $fen = "|";
            for ($i=0;  $i< $deep; $i++) {
                $fen .= '---';
            }

            foreach ($parentData as $key => $value) {
                $value['name'] =$fen.$value['name'];
                $id = $value->id;
                $lists[] = $value;
                self::getLists($id, $lists, ++$deep); //进入子类之前深度+1
                --$deep; //从子类退出之后深度-1
            }
        }
        return $lists;
    }

    public function getSonCategory()
    {
        $sons = GoodsClass::find()->where(['fid'=>$this->id,'is_show'=>0])->orderBy('sort asc, id asc')->all();
        return $sons ?: [];
    }

    /**
     * 递归获取子栏目
     * @param int $categoryId
     * @param array $categorys
     * @return array
     */
    public function getSonCategoryByDG($categoryId=0, $categorys=[])
    {
        $categoryId = $categoryId ? : $this->id;
        $sonCategorys = self::findAll([
            'fid' => $categoryId,
            'is_del' => 0
        ]);

        if ($sonCategorys) {
            $categorys = array_merge($categorys, $sonCategorys);

            foreach ($sonCategorys AS $sonCategory) {
                $categorys = $this->getSonCategoryByDG($sonCategory->id, $categorys);
            }
        }
        return $categorys;
    }

}
