<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "money".
 *
 * @property string $id
 * @property integer $member_id
 * @property string $name
 * @property string $type
 * @property string $money
 * @property integer $status
 * @property string $create_time
 */
class Money extends \yii\db\ActiveRecord
{
    const TYPE_CHARGE = 'CHARGE';  // 充值;

    const TYPE_JINGXIOASHANG = 'JINGXIAOSHANG'; // 经销商提现
    const TYPE_FENXIAOSHANG = 'FENXIAOSHANG'; // 分销商提现
    const TYPE_MEMBER = 'MEMBER'; // 用户提现


    const STATUS_WAIT = '0';
    const STATUS_FINISH = '1';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'money';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'money', 'status', 'create_time'], 'required'],
            [['member_id', 'status'], 'integer'],
            [['money'], 'number'],
            [['create_time'], 'safe'],
            [['name', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => '用户ID',
            'name' => '用户名',
            'type' => '操作类型',
            'money' => '金额',
            'status' => '状态',
            'create_time' => '创建时间',
        ];
    }

    public function getMember()
    {
        $member = Member::findOne($this->member_id);
        return $member ?:(new Member());
    }

    public function getTypeDic()
    {
        return [
            self::TYPE_JINGXIOASHANG => '经销商提现',
            self::TYPE_FENXIAOSHANG => '分销商提现',
            self::TYPE_MEMBER => '用户提现',
            self::TYPE_CHARGE => '用户充值'
        ];
    }

    public function getTypeInfo()
    {
        $dic = $this->getTypeDic();
        return isset($dic[$this->type]) ? $dic[$this->type] : '-';
    }


    public function getStatusDic()
    {
        return [
            self::STATUS_WAIT => '待提现',
            self::STATUS_FINISH => '已完成'
        ];
    }

    public function getStatusInfo()
    {
        $dic = $this->getStatusDic();
        return isset($dic[$this->status]) ? $dic[$this->status] : '-';
    }
}
