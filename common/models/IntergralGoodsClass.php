<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "intergral_goods_class".
 *
 * @property string $id
 * @property string $name
 * @property string $fid
 * @property string $icoImg
 * @property integer $sort
 * @property string $createTime
 * @property string $modifyTime
 * @property integer $is_del
 * @property integer $is_filter
 * @property string $keywords
 */
class IntergralGoodsClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_goods_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fid', 'sort', 'is_del', 'is_filter'], 'integer'],
            [['createTime', 'modifyTime'], 'safe'],
            [['keywords'], 'string'],
            [['name', 'icoImg'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '栏目名称',
            'fid' => 'Fid',
            'icoImg' => '图标',
            'sort' => '权重',
            'createTime' => 'Create Time',
            'modifyTime' => 'Modify Time',
            'is_del' => 'Is Del',
            'is_filter' => 'Is Filter',
            'keywords' => 'Keywords',
        ];
    }

    public  function getParent($pid){
        $treeData= array();
        $tree = IntergralGoodsClass::find()->where(['fid' => $pid])->orderBy('sort asc')->all();
        foreach ($tree as $key => $value) {
            $treeData []= array(
                'id'=>$value->id,
                'name'=>$value->name,
                'tree'=>$this->getParent($value->id),
            );
        }
        return  $treeData;
    }
    public static function displayLists($pid = 0, $selectid = 1,$str="") {
        $result = self::getLists($pid);

        foreach ($result as $key => $value) {
            $selected = "";
            if ( $selectid == $value->id ) {
                $selected = 'selected';
            }
            $count = IntergralGoodsClass::find()->where(['fid' => $value->id])->count();
            if ($count == 0) {
                $str .= '<option value='.$value->id.' '.$selected.'>'.$value->name.'</option>';
            }else{
                $str .= '<optgroup label='.$value->name.'></optgroup>';
            }

        }
        return $str .= '</optgroup></select>';
    }

    public static function displayListsselt($pid = 0, $selectid = 1,$str="") {
        $result = self::getLists($pid);

        foreach ($result as $key => $value) {
            $selected = "";
            if ( $selectid == $value->id ) {
                $selected = 'selected';
            }

            $str .= '<option value='.$value->id.' '.$selected.'>'.$value->name.'</option>';


        }
        return $str .= '</optgroup></select>';
    }

    //从顶层逐级向下获取子类
    public static function getLists($pid = 0, &$lists = array(), $deep = 1) {
        $parentData = IntergralGoodsClass::find()->where(['fid'=>$pid])->orderBy('sort asc')->all();
        if (!empty($parentData)) {
            $fen = "|";
            for ($i=0;  $i< $deep; $i++) {
                $fen .= '---';
            }

            foreach ($parentData as $key => $value) {
                $value['name'] =$fen.$value['name'];
                $id = $value->id;
                $lists[] = $value;
                self::getLists($id, $lists, ++$deep); //进入子类之前深度+1
                --$deep; //从子类退出之后深度-1
            }
        }
        return $lists;
    }

    public function getSonCategory()
    {
        $sons = IntergralGoodsClass::find()->where(['fid'=>$this->id])->orderBy('sort asc')->all();
        return $sons ?: [];
    }

    /**
     * 递归获取子栏目
     * @param int $categoryId
     * @param array $categorys
     * @return array
     */
    public function getSonCategoryByDG($categoryId=0, $categorys=[])
    {
        $categoryId = $categoryId ? : $this->id;
        $sonCategorys = self::findAll([
            'fid' => $categoryId,
            'is_del' => 0
        ]);

        if ($sonCategorys) {
            $categorys = array_merge($categorys, $sonCategorys);

            foreach ($sonCategorys AS $sonCategory) {
                $categorys = $this->getSonCategoryByDG($sonCategory->id, $categorys);
            }
        }
        return $categorys;
    }
}
