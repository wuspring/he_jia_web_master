<?php

namespace common\models;

use common\models\Goods;
use common\models\GoodsAttr;
use common\models\GoodsSku;
use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property string $id
 * @property string $buyerId
 * @property string $goodsId
 * @property string $goodsName
 * @property string $goodsPrice
 * @property integer $goodsNum
 * @property string $attr
 * @property string $sku_id
 * @property string $goods_image
 * @property integer $ispay
 */
class Cart extends \yii\db\ActiveRecord
{
    const TYPE_DETAIL = 0; // 普通商品
    const TYPE_INTERGRAL = 1; // 积分商品
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['buyerId', 'goodsId', 'goodsNum', 'ispay', 'goodsIntegral'], 'integer'],
            [['goodsPrice', 'totalPrice'], 'number'],
            [['attr', 'attrInfo'], 'string'],
            [['goodsName', 'goods_image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'buyerId' => Yii::t('app', '买家id'),
            'goodsId' => Yii::t('app', '商品id'),
            'goodsName' => Yii::t('app', '商品名称'),
            'goodsPrice' => Yii::t('app', '商品价格'),
            'goodsNum' => Yii::t('app', '购买商品数量'),
            'attr' => Yii::t('app', '规格'),
            'sku_id' => Yii::t('app', 'sku编号'),
            'goods_image' => Yii::t('app', '商品图片'),
            'ispay' => Yii::t('app', 'Ispay'),
        ];
    }

    public function getAmountLimit()
    {
        if ((int)$this->sku_id) {
            $sku = IntergralGoodsSku::findOne([
                'goods_id' => $this->goodsId,
                'id' => $this->sku_id
            ]);

            $amount = $sku ? $sku->num : 0;
        } else {
            $goods = IntergralGoods::findOne(['id' => $this->goodsId]);
            $amount = $goods->goods_storage;
        }

        return $amount;
    }

    public function getGoodsInfo()
    {
        $goods = IntergralGoods::findOne(['id' => $this->goodsId]);

        return $goods ?:(new IntergralGoods());
    }

    public function getSkuInfo()
    {
        $skuInfo = IntergralGoodsSku::findOne([
            'goods_id' => $this->goodsId,
            'id' => $this->sku_id
        ]);

        return $skuInfo?:(new IntergralGoodsSku());
    }

    public function getGoodsSku()
    {
        return $this->getSkuInfo();
    }

    public function getAttrInfo()
    {
        $skuInfo = $this->getSkuInfo();
        if (!strlen($skuInfo->attr)) {
            return '';
        }

        $skuIds = json_decode($skuInfo->attr,true);

        $goodSkus = IntergralGoodsAttr::find()->where([
            'and',
            ['in', 'id', $skuIds]
        ])->all();

        $info = '';
        foreach ($goodSkus AS $goodSku) {
            $info .= "{$goodSku->groupInfo->group_name}:{$goodSku->attr_name}, ";
        }

        return $info;
    }


}
