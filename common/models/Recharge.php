<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "recharge".
 *
 * @property integer $id
 * @property integer $memberId
 * @property string $money
 * @property integer $state
 * @property string $createTime
 * @property string $payTime
 * @property string $pay_sn
 */
class Recharge extends \yii\db\ActiveRecord
{
    const WAIT = '0';
    const FINISH = '1';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recharge';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['memberId', 'state'], 'integer'],
            [['money'], 'number'],
            [['createTime', 'payTime'], 'safe'],
            [['pay_sn'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'memberId' => Yii::t('app', 'Member ID'),
            'money' => Yii::t('app', 'Money'),
            'state' => Yii::t('app', 'State'),
            'createTime' => Yii::t('app', 'Create Time'),
            'payTime' => Yii::t('app', 'Pay Time'),
            'pay_sn' => Yii::t('app', 'Pay Sn'),
        ];
    }

    public function getNewSn()
    {
        $orderNum = 'charge' . date('Ymdhis') . createRandKey(4);

        $order = self::findOne([
            'pay_sn' => $orderNum
        ]);
        return $order ? $this->getNewSn() : $orderNum;
    }
}
