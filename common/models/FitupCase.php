<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "fitup_case".
 *
 * @property string $id
 * @property string $fitup_company
 * @property string $address
 * @property string $design
 * @property string $area
 * @property integer $house_type
 * @property integer $design_stype
 * @property string $budget
 * @property string $tab_text
 * @property string $tab
 * @property string $keywords
 * @property string $description
  * @property string $create_time
 * @property integer $is_del
 */
class FitupCase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fitup_case';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['house_type', 'design_style', 'is_del','user_id','is_recommend'], 'integer'],
            [['tab_text', 'keywords', 'description'], 'string'],
            [['create_time'], 'safe'],
            [['fitup_company', 'address', 'design', 'area', 'budget', 'tab','cover_pic','name','provinces_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'tab'),
            'fitup_company' => Yii::t('app', '装修公司'),
            'cover_pic' => Yii::t('app', '封面图'),
            'address' => Yii::t('app', '地址'),
            'design' => Yii::t('app', '设计师'),
            'area' => Yii::t('app', '建筑面积'),
            'house_type' => Yii::t('app', '户型'),
            'design_style' => Yii::t('app', '设计风格'),
            'budget' => Yii::t('app', '预算'),
            'tab_text' => Yii::t('app', 'tab中包含的值'),
            'tab' => Yii::t('app', 'tab数组'),
            'create_time' => Yii::t('app', 'Create Time'),
            'is_del' => Yii::t('app', 'Is Del'),
        ];
    }

    /**
     * 获取装修子类
     */
    public  function  getFitupChilder($pid){
      $model=FitupAttr::findAll(['pid'=>$pid]);
      $modelArr=ArrayHelper::map($model,'id','name');
      return $modelArr?:([]);
    }
    /**
     * 获取类型
     */
    public  function getFitupAttr($id){

        $model=FitupAttr::findOne($id);

        return $model?:(new FitupAttr());
    }
    /**
     *  选择店铺
     */
    public  function  getUser(){

        $user=User::findAll(['status'=>1,'assignment'=>User::ASSIGNMENT_HOU_TAI]);
        $userArr=ArrayHelper::map($user,'id','nickname');

        return $userArr?:([]);
    }
  /**
   *   获取店铺
   */
   public  function  getStore(){
       $store=User::findOne($this->user_id);

       return $store?:(new  User());
   }

    /**
     * 筛选城市列表
     */
    public  function getSelectCity(){
        $selectCity=SelectCity::find()->all();
        $selectCityArr=ArrayHelper::map($selectCity,'provinces_id','name');
        return $selectCityArr?:([]);
    }

    /**
     * 城市列表
     */
    public  function getCityName(){

        $cityArr=json_decode($this->provinces_id);
        $cityStr='';
        if (!empty($cityArr)){
            foreach ($cityArr as $k=>$v){
                $selectCity=Provinces::findOne($v);
                $cityStr.=$selectCity->cname.',';
            }
            $cityStr=substr($cityStr,0,-1);
        }
        return $cityStr;
    }






}
