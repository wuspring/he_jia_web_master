<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "news".
 *
 * @property string $id
 * @property string $title
 * @property string $cid
 * @property string $themeImg
 * @property string $themeImgTm
 * @property string $brief
 * @property string $content
 * @property string $seokey
 * @property string $seoDepict
 * @property integer $isShow
 * @property integer $isRmd
 * @property integer $clickRate
 * @property string $writer
 * @property string $uid
 * @property string $createTime
 * @property string $modifyTime
 */
class News extends \yii\db\ActiveRecord
{
    const ACTIVITY_TYPE_JIE_HUN = 'JIE_HUN';
    const ACTIVITY_TYPE_JIA_ZHUANG = 'JIA_ZHUANG';
    const ACTIVITY_TYPE_YUN_YING= 'YUN_YING';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cid', 'isShow', 'isRmd', 'clickRate', 'uid', 'sort'], 'integer'],
            [['content'], 'string'],
            [['createTime', 'modifyTime'], 'safe'],
            [['title', 'themeImg', 'themeImgTm', 'brief', 'seokey', 'seoDepict', 'writer','type','provinces_id'], 'string', 'max' => 255],
            [['cid'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', '文章编号'),
            'title' => Yii::t('app', '文章标题'),
            'type'=> Yii::t('app', '活动类型'),
            'cid' => Yii::t('app', '文章分类'),
            'themeImg' => Yii::t('app', '主题图片'),
            'themeImgTm' => Yii::t('app', '主题缩略图'),
            'brief' => Yii::t('app', '简介'),
            'content' => Yii::t('app', '描述'),
            'seokey' => Yii::t('app', 'seo关键词'),
            'seoDepict' => Yii::t('app', 'seo描述'),
            'isShow' => Yii::t('app', '是否显示'),//1显示 0不显示
            'isRmd' => Yii::t('app', '是否推荐'),//1推荐 0 不推荐
            'clickRate' => Yii::t('app', '点击量'),
            'writer' => Yii::t('app', '作者'),
            'uid' => Yii::t('app', '发布人id'),
            'createTime' => Yii::t('app', 'Create Time'),
            'modifyTime' => Yii::t('app', 'Modify Time'),
            'provinces_id' => Yii::t('app', '城市id'),
        ];
    }
    public  function getClass()
    {
        $model=Newsclassify::findOne($this->cid);
        if (empty($model)) {
            $model= new Newsclassify();
            $model->name="无";
            
        }
        return  $model;
    }

    /**
     * @param int $pid
     * @param array $list
     * @return array
     *  获取资讯分类
     *
     */
    public function  getNewsClass($pid=0,$list=array()){
        $calsslist= Newsclassify::find()->where(['fid'=>$pid])->orderBy(" sort desc")->all();
        foreach ($calsslist as $key=>$item){
            $count= Newsclassify::find()->where(['fid'=>$item->id])->count();
            if ($count>0){
                $list[$item->name]= $this->getNewsClass($item->id);
            }else{
                $list[$item->id]='|-'.$item->name;
            }
        }
        return $list;
    }

    public function getTypeDic()
    {
        return [
            self::ACTIVITY_TYPE_JIA_ZHUANG => '家装采购',
            self::ACTIVITY_TYPE_JIE_HUN=> '结婚采购',
            self::ACTIVITY_TYPE_YUN_YING => '孕婴展采购'
        ];
    }


    public function getTypeInfo()
    {
        $dic = $this->getTypeDic();

        return isset($dic[$this->type]) ? $dic[$this->type] : '-';
    }

    /**
     * 筛选城市列表
     */
    public  function getSelectCity(){
        $selectCity=SelectCity::find()->all();
        $selectCityArr=ArrayHelper::map($selectCity,'provinces_id','name');
        return $selectCityArr?:([]);
    }

    /**
     * 城市列表
     */
    public  function getCityName(){

        $cityArr=json_decode($this->provinces_id);
        $cityStr='';
        if (!empty($cityArr)){
            foreach ($cityArr as $k=>$v){
                $selectCity=Provinces::findOne($v);
                $cityStr.=$selectCity->cname.',';
            }
            $cityStr=substr($cityStr,0,-1);
        }
       return $cityStr;
    }






}
