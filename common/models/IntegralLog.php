<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "integral_log".
 *
 * @property integer $id
 * @property integer $memberId
 * @property integer $orderId
 * @property integer $pay_integral
 * @property integer $old_integral
 * @property integer $now_integral
 * @property string $create_time
 * @property string $expand
 */
class IntegralLog extends \yii\db\ActiveRecord
{
    const TYPE_CHARGE = 'CHARGE'; // 充值
    const TYPE_ORDER = 'ORDER'; // 订单购物
    const TYPE_COUPON = 'COUPON'; // 购买优惠券
    const TYPE_GIFT = 'GIFT'; // 购买礼品


    const ORDER_ID_LOGIN = 'login';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'integral_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['memberId', 'pay_integral', 'old_integral', 'now_integral'], 'integer'],
            [['orderId', 'create_time', 'expand'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'memberId' => 'Member ID',
            'orderId' => 'Order ID',
            'pay_integral' => 'Pay Integral',
            'old_integral' => 'Old Integral',
            'now_integral' => 'Now Integral',
            'create_time' => 'Create Time',
        ];
    }

    public function getOrder()
    {
        $order = Order::findOne([
            'orderid' => $this->orderId
        ]);

        return $order ?: (new Order());
    }

    public function getExchangeInfo()
    {
        $info = '-';
        if (in_array($this->type, [self::TYPE_COUPON, self::TYPE_GIFT])) {
            switch ($this->type) {
                case self::TYPE_COUPON :
                    $coupon = Coupon::findOne($this->detail_id);
                    $coupon and $info = $coupon->name;
                    break;
                case  self::TYPE_GIFT :
                    $gift = Gift::findOne($this->detail_id);
                    $gift and $info = $gift->goods_name;
                    break;
            }
        }

        return $info;
    }


}
