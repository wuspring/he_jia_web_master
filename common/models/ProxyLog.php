<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "proxy_log".
 *
 * @property integer $id
 * @property integer $memberId
 * @property string $orderId
 * @property string $amount
 * @property string $money
 * @property integer $status
 * @property string $create_time
 */
class ProxyLog extends \yii\db\ActiveRecord
{
    const STATUS_WAIT = 0;      // 未领取
    const STATUS_FINISH = 1;    // 已分配

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proxy_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['memberId', 'orderId', 'goodId','status'], 'integer'],
            [['amount', 'money'], 'number'],
            [['create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'memberId' => 'Member ID',
            'orderId' => 'Order ID',
            'amount' => 'Amount',
            'money' => 'Money',
            'status' => 'Status',
            'create_time' => 'Create Time',
        ];
    }


    public function getOrder()
    {
        $order = Order::findOne([
            'orderid' => $this->orderId
        ]);

        return $order ? :(new Order());
    }

    public function getStatusInfo()
    {
        $dic = [
            self::STATUS_FINISH => '已结算',
            self::STATUS_WAIT => '待结算'
        ];
        return $dic[$this->status];
    }
}
