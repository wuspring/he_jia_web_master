<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property string $id
 * @property string $title
 * @property string $keyword
 * @property string $description
 * @property string $parent_id
 * @property string $rank
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['parent_id', 'is_index', 'rank'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['keyword', 'description','icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '栏目标题',
            'keyword' => '关键字',
            'description' => '描述',
            'parent_id' => '上级栏目',
            'icon' => '缩略图',
            'is_index' => '栏目属性',
            'rank' => '权重',
        ];
    }

    public function getParentCategory()
    {
        return self::hasOne(self::className(), ['id' => 'parent_id']);
    }
}
