<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ticket_apply_goods_data".
 *
 * @property string $id
 * @property integer $ticket_id
 * @property integer $user_id
 * @property integer $good_id
 * @property string $good_pic
 * @property string $good_price
 * @property string $type
 * @property string $create_time
 * @property string $good_amount
 * @property integer $status
 * @property integer $is_index
 * @property integer $is_zhanhui
 */
class TicketApplyGoodsData extends \yii\db\ActiveRecord
{
    const TYPE_ORDER = 'ORDER';
    const TYPE_COUPON = 'COUPON';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_apply_goods_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id', 'user_id', 'good_id', 'good_amount', 'status', 'is_index', 'is_zhanhui', 'sort'], 'integer'],
            [['good_pic'], 'string'],
            [['good_price'], 'number'],
            [['create_time'], 'safe'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticket_id' => 'Ticket ID',
            'user_id' => 'User ID',
            'good_id' => 'Good ID',
            'good_pic' => 'Good Pic',
            'good_price' => 'Good Price',
            'type' => 'Type',
            'create_time' => 'Create Time',
            'good_amount' => 'Good Amount',
            'status' => 'Status',
            'sort'=>'sort'
        ];
    }


    public function getTypeDic()
    {
        return [
            self::TYPE_COUPON => '爆款预约',
            self::TYPE_ORDER => '预存 享特价'
        ];
    }

    public function getTypeInfo()
    {
        $dic = $this->getTypeDic();
        return isset($dic[$this->type]) ? $dic[$this->type] : '-';
    }

    public function getGoods()
    {
        $good = Goods::findOne($this->good_id);
        return $good ? : (new Goods());
    }

    public function getTicket()
    {
        $ticket = Ticket::findOne([
            'id' => $this->ticket_id,
        ]);

        return $ticket ? :(new Ticket());
    }
    public function getTicketlist()
    {
        $ticket = Ticket::find()->all();
        $listData=ArrayHelper::map($ticket,'id','name');
        return $listData;

    }

    /**
     * @return array
     *  筛选出正在进行的展会和没有即将开始的展会
     */
    public function getStartTicketlist($priId=0)
    {
        $nowTime=date('Y-m-d H:i:s');

        $filter = [
            'and',
            ['>','end_date',$nowTime],
            ['=','status',intval(Ticket::STATUS_NORMAL)]
        ];
        if ($priId) {
        	$filter[] = ['=', 'citys', $priId];
        }

        $ticket = Ticket::find()->andFilterWhere($filter)->all();
        $listData=ArrayHelper::map($ticket,'id','name');
        return $listData;

    }

    public function getUser()
    {
        $user = User::findOne($this->user_id);
        return $user ?: (new User());
    }
}
