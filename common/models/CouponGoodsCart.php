<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "coupon_goods_cart".
 *
 * @property string $id
 * @property integer $coupon_id
 * @property string $mcoupon_id
 * @property integer $good_id
 * @property string $good_price
 */
class CouponGoodsCart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupon_goods_cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coupon_id', 'mcoupon_id', 'good_id'], 'integer'],
            [['good_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'coupon_id' => 'Coupon ID',
            'mcoupon_id' => 'Mcoupon ID',
            'good_id' => 'Good ID',
            'good_price' => 'Good Price',
        ];
    }
}
