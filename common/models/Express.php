<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "express".
 *
 * @property string $id
 * @property string $name
 * @property string $type
 * @property string $createTime
 * @property string $updateTime
 */
class Express extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'express';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['createTime', 'updateTime'], 'safe'],
            [['name', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '快递公司名称'),
            'type' => Yii::t('app', '编码'),
            'status' => Yii::t('app', '启用'),
            'createTime' => Yii::t('app', '添加时间'),
            'updateTime' => Yii::t('app', '更新时间'),
        ];
    }
}
