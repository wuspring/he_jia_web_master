<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "intergral_goods_attr".
 *
 * @property string $id
 * @property string $group_id
 * @property string $attr_name
 * @property integer $is_delete
 * @property integer $is_default
 */
class IntergralGoodsAttr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_goods_attr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'is_delete', 'is_default'], 'integer'],
            [['attr_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'attr_name' => 'Attr Name',
            'is_delete' => 'Is Delete',
            'is_default' => 'Is Default',
        ];
    }

    public function getGroupInfo()
    {
        $group = IntergralGoodsAttrGroup::findOne([
            'id' => $this->group_id
        ]);
        return $group?:(new IntergralGoodsAttrGroup());
    }
}
