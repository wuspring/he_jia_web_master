<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "postmethod".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $type
 * @property integer $first_condition
 * @property integer $first_money
 * @property integer $other_condition
 * @property integer $other_money
 * @property integer $status
 * @property string $createTime
 * @property string $send_area
 * @property string $not_area
 */
class Postmethod extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'postmethod';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'status','is_default','expressid'], 'integer'],
            [['createTime'], 'safe'],
            [['first_condition', 'first_money', 'other_condition', 'other_money'], 'number'],
            [['send_area', 'not_area'], 'string'],
            [['name', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '模版名称'),
            'code' => Yii::t('app', '物流公司编号'),
//            'type' => Yii::t('app', '1 重量计费 2按件计费'),
            'type' => Yii::t('app', '2按件计费'),
            'first_condition' => Yii::t('app', '首重'),
            'first_money' => Yii::t('app', '首费'),
            'other_condition' => Yii::t('app', '续重'),
            'other_money' => Yii::t('app', '续费'),
            'status' => Yii::t('app', '状态'),
            'createTime' => Yii::t('app', '添加时间'),
            'send_area' => Yii::t('app', '配送区域'),
            'not_area' => Yii::t('app', '不配送区域'),
            'is_default' => Yii::t('app', '是否包邮'),
            'expressid'=> Yii::t('app', '物流公司'),
        ];
    }

    public function getExpress(){
      $model=  Express::findOne(['id'=>$this->expressid]);
      if (empty($model)){
          $model=new Express();
      }
      return $model;
    }
}
