<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property string $id
 * @property string $code
 * @property string $name
 * @property string $payment_config
 * @property integer $state
 * @property string $createTime
 * @property string $modifyTime
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_config'], 'string'],
            [['state'], 'integer'],
            [['createTime', 'modifyTime'], 'safe'],
            [['code', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', '支付代码名称'),
            'name' => Yii::t('app', '支付名称'),
            'payment_config' => Yii::t('app', '支付接口配置信息'),
            'state' => Yii::t('app', '接口状态0禁用1启用'),
            'createTime' => Yii::t('app', 'Create Time'),
            'modifyTime' => Yii::t('app', 'Modify Time'),
        ];
    }
}
