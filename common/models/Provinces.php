<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "provinces".
 *
 * @property string $id
 * @property string $cname
 * @property string $upid
 * @property string $ename
 * @property string $pinyin
 * @property integer $level
 * @property string $title
 * @property string $keywords
 * @property string $description
 */
class Provinces extends \yii\db\ActiveRecord
{

    public $center;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'provinces';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'upid', 'level'], 'integer'],
            [['title', 'keywords', 'description'], 'string'],
            [['cname', 'ename', 'pinyin'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cname' => Yii::t('app', '名称'),
            'upid' => Yii::t('app', '上级ID'),
            'ename' => Yii::t('app', 'Ename'),
            'pinyin' => Yii::t('app', '别名'),
            'level' => Yii::t('app', '层级'),

            'title' => Yii::t('app', '标题'),
            'keywords' => Yii::t('app', '关键字'),
            'description' => Yii::t('app', '网站描述'),
        ];
    }

    static public function provinceCity(){
        $provinces = Provinces::find()->where(['upid'=>1])->asArray()->all();
        if(!empty($provinces)){
            foreach ($provinces as $key=>$val){
                $provinces[$key]['child'] = Provinces::find()->where(['upid'=>$val['id']])->asArray()->all();
            }
        }
        return $provinces;
    }



    public static function getRegion($parentId=1)
    {
        $result = static::find()->where(['upid'=>$parentId])->asArray()->all();
        return ArrayHelper::map($result, 'id', 'cname');
    }

    public static function getModel($uip){

        $model=Provinces::findOne(['id'=>$uip]);
        if (empty($model)){
            $model=new Provinces();
        }
        return $model->upid;
    }

    public static function getlistname($id, $list,$leve=1){

        $model=  Provinces::findOne($id);
        array_unshift($list,$model->cname);
        if ($model->upid>0&&$model->level>$leve){
            $list= self::getlistname($model->upid,$list);
        }
        return $list;
    }

    public static function getname($id, $string='',$leve=1){

        $model=  Provinces::findOne($id);
        $string= $model->cname.$string;
        if ($model->upid>0&&$model->level>$leve){
            $string= self::getname($model->upid,$string);
        }
        return $string;
    }

    public static function getlistid($id, $list,$leve=1){

        $model=  Provinces::findOne($id);
        array_unshift($list,array('id'=>$model->id,'name'=>$model->cname));
        if ($model->upid>0&&$model->level>$leve){
            $list= self::getlistid($model->upid,$list);
        }
        return $list;
    }
}
