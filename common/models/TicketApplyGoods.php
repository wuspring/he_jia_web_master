<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ticket_apply_goods".
 *
 * @property string $id
 * @property integer $ticket_id
 * @property integer $user_id
 * @property integer $good_id
 * @property integer $good_amount
 * @property integer $is_zhanhui
 * @property integer $is_index
 * @property string $good_pic
 * @property string $good_price
 * @property string $type
 * @property string $create_time
 */
class TicketApplyGoods extends \yii\db\ActiveRecord
{
    const TYPE_ORDER = 'ORDER';
    const TYPE_COUPON = 'COUPON';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_apply_goods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id', 'user_id', 'good_id', 'good_amount', 'is_index','sort', 'is_zhanhui'], 'integer'],
            [['good_pic'], 'string'],
            [['good_price'], 'number'],
            [['create_time'], 'safe'],
            [['type'], 'string', 'max' => 255],
            [['type'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticket_id' => 'Ticket ID',
            'user_id' => 'User ID',
            'good_id' => 'Good ID',
            'good_pic' => 'Good Pic',
            'good_price' => 'Good Price',
            'type' => 'Type',
            'create_time' => 'Create Time',
            'sort'=>'sort'
        ];
    }

    public function getTypeDic()
    {
        return [
            self::TYPE_COUPON => '爆款预约',
//            self::TYPE_ORDER => '下订单 享特价'
            self::TYPE_ORDER => '预存 享特价'
        ];
    }

    public function getTypeInfo()
    {
        $dic = $this->getTypeDic();
        return isset($dic[$this->type]) ? $dic[$this->type] : '-';
    }

    public function getGoods()
    {
        $good = Goods::findOne($this->good_id);
        return $good ? : (new Goods());
    }

    public function getTicket()
    {
        $ticket = Ticket::findOne([
            'id' => $this->ticket_id,
        ]);
        return $ticket ? :(new Ticket());
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public function getUser()
    {
        $user = User::findOne($this->user_id);
        return $user ?: (new User());
    }
}
