<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Sms".
 *
 * @property integer $id
 * @property integer $phone
 * @property string $type
 * @property string $content
 * @property string $create_time
 */
class Sms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Sms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'type', 'content', 'create_time'], 'required'],
            [['phone'], 'integer'],
            [['create_time'], 'safe'],
            [['type'], 'string', 'max' => 32],
            [['content'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'phone' => Yii::t('app', 'Phone'),
            'type' => Yii::t('app', 'Type'),
            'content' => Yii::t('app', 'Content'),
            'create_time' => Yii::t('app', 'Create Time'),
        ];
    }
}
