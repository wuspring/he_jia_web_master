<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "commission_log".
 *
 * @property integer $id
 * @property integer $memberId
 * @property integer $orderId
 * @property string $amount
 * @property string $money
 * @property integer $status
 * @property string $create_time
 */
class CommissionLog extends \yii\db\ActiveRecord
{
    const STATUS_WAIT = 0;      // 未领取
    const STATUS_FINISH = 1;    // 已分配

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['memberId', 'orderId', 'status'], 'integer'],
            [['amount', 'money'], 'number'],
            [['create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'memberId' => Yii::t('app', '会员'),
            'orderId' => Yii::t('app', 'Order ID'),
            'amount' => Yii::t('app', '订单金额'),
            'money' => Yii::t('app', '分成金额'),
            'status' => Yii::t('app', '领取状态 0表示未领取 1表示已分配'),
            'create_time' => Yii::t('app', '添加时间'),
        ];
    }

    public function getOrder()
    {
        $order = Order::findOne([
            'orderid' => $this->orderId
        ]);

        return $order ? :(new Order());
    }

    public function getStatusInfo()
    {
        $dic = [
            self::STATUS_FINISH => '已结算',
            self::STATUS_WAIT => '待结算'
        ];
        return $dic[$this->status];
    }
}
