<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pay_sn_order_relation".
 *
 * @property string $id
 * @property string $pay_sn
 * @property string $orderid
 * @property integer $member_id
 * @property integer $status
 * @property string $pay_amount
 * @property string $create_time
 */
class PaySnOrderRelation extends \yii\db\ActiveRecord
{
    const ORDER = 'ORDER';
    const CHARGE =  'CHARGE';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pay_sn_order_relation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pay_sn', 'orderid', 'member_id', 'status', 'create_time'], 'required'],
            [['member_id', 'status'], 'integer'],
            [['pay_amount'], 'number'],
            [['create_time'], 'safe'],
            [['pay_sn', 'orderid'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pay_sn' => 'Pay Sn',
            'orderid' => 'Orderid',
            'member_id' => 'Member ID',
            'status' => 'Status',
            'pay_amount' => 'Pay Amount',
            'create_time' => 'Create Time',
        ];
    }


}
