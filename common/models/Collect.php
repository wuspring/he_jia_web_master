<?php

namespace common\models;

use DL\Project\Store;
use Yii;

/**
 * This is the model class for table "collect".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $goods_id
 * @property string $create_time
 */
class Collect extends \yii\db\ActiveRecord
{

    const  TYPE_STORE='STORE';
    const  TYPE_GOOD='GOOD';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'collect';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'goods_id','shop_id','ticket_id'], 'integer'],
            [['type','type_wap'], 'string', 'max' => 255],
            [['create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', '会员ID'),
            'goods_id' => Yii::t('app', '商品ID'),
            'create_time' => Yii::t('app', '添加时间'),
            'ticket_id' => Yii::t('app', '展会id'),
            'type_wap' => Yii::t('app', '类型_手机端'),
        ];
    }

    /**
     * 关联商品表
     */
    public function getGoods()
    {
        $good = Goods::findOne($this->goods_id);
        return $good ?: (new Goods());
    }
    /**
     * 关联店铺
     */
    public function getStore()
    {
        $user =User::findOne($this->shop_id);
        return $user ?: (new User());
    }

    public function  getTypeDic(){
        return [
          self::TYPE_GOOD => '商品收藏',
          self::TYPE_STORE => '店铺收藏'
        ];
    }

    public function  getTypeInfo(){

        $dic = $this->getTypeDic();

        return isset($dic[$this->type]) ?  $dic[$this->type] : '-';

    }

}
