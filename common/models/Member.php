<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "member".
 *
 * @property string $id
 * @property string $username
 * @property string $password
 * @property string $nickname
 * @property integer $role
 * @property integer $sex
 * @property string $birthday
 * @property string $status
 * @property string $avatarTm
 * @property string $avatar
 * @property string $email
 * @property string $createTime
 * @property string $modifyTime
 * @property string $last_visit
 * @property integer $userType
 * @property string $authkey
 * @property string $accessToken
 * @property string $remainder
 * @property integer $integral
 * @property integer $rank
 */
class Member extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $parent_name = '';

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_TYPE = 1;

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role', 'sex', 'userType', 'integral', 'rank', 'pid','is_del'], 'integer'],
            [['birthday', 'createTime', 'modifyTime', 'last_visit', 'origin'], 'safe'],
            [['remainder', 'brokerage', 'proxyBrokerage', 'is_fenxiao', 'is_daili', 'cost_amount'], 'number'],
            [['username', 'password', 'nickname', 'status', 'avatarTm', 'avatar', 'email', 'authkey', 'accessToken', 'wxopenid', 'wxnick','ticket','qrodeUrl','birthday_year'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', '帐号'),
            'password' => Yii::t('app', '密码'),
            'nickname' => Yii::t('app', '昵称'),
            'role' => Yii::t('app', '角色名称'),
            'sex' => Yii::t('app', '性别男是，女是2'),
            'birthday' => Yii::t('app', '生日'),
            'status' => Yii::t('app', '状态'),
            'avatarTm' => Yii::t('app', '头像缩略图'),
            'avatar' => Yii::t('app', '头像'),
            'email' => Yii::t('app', '邮箱'),
            'createTime' => Yii::t('app', '创建时间'),
            'modifyTime' => Yii::t('app', '修改时间'),
            'last_visit' => Yii::t('app', '最后登录时间'),
            'userType' => Yii::t('app', '用户类别'),
            'authkey' => Yii::t('app', '授权'),
            'accessToken' => Yii::t('app', 'Access Token'),
            'remainder' => Yii::t('app', '余额'),
            'integral' => Yii::t('app', '积分'),
            'rank' => Yii::t('app', '等级分数'),
            'birthday_year' => Yii::t('app', '出生年月日'),
            'is_del' => Yii::t('app', '软删除'),
        ];
    }

    /**
     * 获取推荐人名称
     * @author 黄西方
     * @date 2018-10-28
     */
    public function getParentName()
    {
        if($this->pid){
            $model = Member::findOne($this->pid);
            return $model->nickname;
        }else{
            return '无';
        }
    }

    public function getPmember()
    {
        $member = Member::findOne($this->pid);
        return $member ? $member->wxnick : '平台';
    }

    public function getFenxiaoMoney()
    {
        $money = CommissionLog::find()->where([
            'memberId' => $this->id,
            'status' => CommissionLog::STATUS_FINISH
        ])->sum('money');

        return $money ? : 0;
    }

    /**
     * 获取我的团队成员 ID
     * @author 黄西方
     * @date 2018-10-28
     */
    public function getChildTeam($level=1,$pid=0){
        !$pid && $pid = $this->pid;
        $ids = array();
        if($level==1){
            $members = Member::find()->where(['status'=>1,'pid'=>$pid])->orderBy('createTime DESC')->all();
            $ids  = ArrayHelper::getColumn($members,'id');
            return $ids;
        }elseif ($level==2){
            $members = Member::find()->where(['status'=>1,'pid'=>$pid])->orderBy('createTime DESC')->all();
            $first  = ArrayHelper::getColumn($members,'id');
            if(!empty($first)){
                $second = Member::find()->where(['status'=>1])->andWhere(['in','pid',$first])
                    ->orderBy('createTime DESC')->all();
                $ids = ArrayHelper::getColumn($second,'id');
            }
            return $ids;
        }else{
            $members = Member::find()->where(['status'=>1,'pid'=>$pid])->orderBy('createTime DESC')->all();
            $first  = ArrayHelper::getColumn($members,'id');
            if(!empty($first)){
                $second_members = Member::find()->where(['status'=>1])->andWhere(['in','pid',$first])
                    ->orderBy('createTime DESC')->all();
                $second = ArrayHelper::getColumn($second_members,'id');
                if(!empty($second)){
                    $third = Member::find()->where(['status'=>1])->andWhere(['in','pid',$second])
                        ->orderBy('createTime DESC')->all();
                    $ids = ArrayHelper::getColumn($third,'id');
                }
            }
            return $ids;
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new \Exception('"findIdentityByAccessToken" is not implemented.');
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->authkey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * 获取个人消费总金额
     * @return mixed
     */
    public function getPayAmount()
    {
        $money = Order::find()->where([
            'and',
            ['=', 'buyer_id', $this->id],
            ['not in', 'order_state', [Order::STATUS_WAIT, Order::STATUS_REFUSE, Order::STATUS_CLOSE]]
        ])->sum('goods_amount');

        return $money ?: 0;
    }

    /**
     * 获取用户等级
     */
    public function getLevel()
    {
        $level = Level::find()->where([
            '>=', 'score', (int)$this->cost_amount
        ])->orderBy('id asc')->one();

        return $level ? $level->name : 'VIP会员';
    }

    /**
     *  获取近50年
     */
    public  function  getYear(){

        $years = array();
        $currentYear = date('Y');
        for ($i=0; $i<50; $i++)
        {
            $years[$i] = $currentYear - $i;
        }
        return  $years;
    }
    /**
     * 获取月
     */
    public  function getMoth(){
        $moth = array();
        for ($i=1; $i<13; $i++)
        {
            $moth[$i] =$i;
        }
        return  $moth;
    }

    /**
     *  获取天
     */
    public  function  getDay(){

        $day = array();
        for ($i=1; $i<31; $i++)
        {
            $day[$i] = $i;
        }
        return  $day;
    }

    /**
     * 获取当前日期
     */
    public  function getBirthdayArr(){
        if (empty($this->birthday_year)){
            $arr=explode('-',"1700-13-00");
        }else{
            $arr=explode('-',$this->birthday_year);
        }
        return $arr;
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function validatePassword($password)
    {

        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function getNewUserName()
    {
        $str = 'at_' . createRandKey('10','abcedfghijklimnopqrstuvwxyz0123456789');
        $member = Member::findOne([
            'username' => $str
        ]);

        return $member ? $this->getNewUserName() : $str;

    }
}
