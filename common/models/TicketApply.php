<?php

namespace common\models;

use DL\Project\Store;
use Yii;

/**
 * This is the model class for table "ticket_apply".
 *
 * @property string $id
 * @property string $user_id
 * @property integer $ticket_id
 * @property string $zw_pos
 * @property string $status
 * @property string $good_ids
 * @property string $reason
 * @property string $create_time
 */
class TicketApply extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_apply';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ticket_id', 'status'], 'integer'],
            [['create_time'], 'safe'],
            [['zw_pos'], 'string', 'max' => 255],
            [['good_ids', 'reason'], 'string'],
            [['user_id', 'ticket_id'], 'unique', 'targetAttribute' => ['user_id', 'ticket_id'], 'message' => 'The combination of User ID and Ticket ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'ticket_id' => 'Ticket ID',
            'zw_pos' => 'Zw Pos',
            'status' => 'Status',
            'create_time' => 'Create Time',
        ];
    }

    public function create()
    {
        if (!strlen($this->zw_pos)) {
            $key = (string)$this->ticket_id;
            strlen($this->ticket_id) < 2 and $key = "0{$key}";

            $dic = '01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $key = $key . createRandKey(6, $dic);

            $record = self::findOne([
                'zw_pos' => $key
            ]);

            if ($record) {
                return $this->create();
            }

            $this->zw_pos = $key;
            $this->create_time = date('Y-m-d H:i:s');

            $info = $this->getTicketInfo();
            $info->ticket_amount++;
            $info->save();
        }
        $this->good_ids =  '[]';   // 已弃用
        $this->status = 0;
        return $this->save();
    }
    public function create1()
    {
        if (!strlen($this->zw_pos)) {
            $key = (string)$this->ticket_id;
            strlen($this->ticket_id) < 2 and $key = "0{$key}";

            $dic = '01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $key = $key . createRandKey(6, $dic);

            $record = self::findOne([
                'zw_pos' => $key
            ]);

            if ($record) {
                return $this->create();
            }

            $this->zw_pos = $key;
            $this->create_time = date('Y-m-d H:i:s');

            $info = $this->getTicketInfo();
            $info->ticket_amount++;
            $info->save();
        }
        return $this->save();
    }
    public function getStore()
    {
        try {
            $store = \DL\Project\Store::init()->info($this->user_id, false);
        } catch (\Exception $e) {
            $store = new \common\models\User();
            $store->nickname = '商铺状态异常';
        }

        return $store;
    }

    public function getTicketInfo()
    {
        $info = TicketInfo::findOne([
            'ticket_id' => $this->ticket_id,
            'provinces_id' => $this->store->provinces_id,
        ]);

        return $info ? : (new TicketInfo());
    }

    public function getTicket()
    {
        $ticket = \common\models\Ticket::findOne($this->ticket_id);
        return $ticket ? : (new Ticket());
    }

    public function getStatusDic()
    {
        return [
            '0' => '待审核',
            '1' => '同意',
            '2' => '不同意',
        ];
    }

    public function getStatusInfo()
    {
        $dic = $this->getStatusDic();

        return $dic[$this->status];
    }
}
