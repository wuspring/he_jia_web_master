<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "account_log".
 *
 * @property integer $id
 * @property string $type
 * @property string $money
 * @property string $integral
 * @property string $remark
 * @property integer $member_id
 * @property string $create_time
 */
class AccountLog extends \yii\db\ActiveRecord
{
    const INPUT = 'INPUT';      // 存入
    const OUTPUT = 'OUTPUT';    // 支出

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['money'], 'number'],
            [['member_id'], 'integer'],
            [['create_time'], 'safe'],
            [['type'], 'string', 'max' => 20],
            [['integral'], 'number', 'max' => 10],
            [['remark', 'sn'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', '类型'),
            'money' => Yii::t('app', '变动金额'),
            'integral' => Yii::t('app', '积分'),
            'remark' => Yii::t('app', '说明'),
            'member_id' => Yii::t('app', '会员ID'),
            'create_time' => Yii::t('app', '变动时间'),
        ];
    }

    public function getTypeDic()
    {
        return [
            self::INPUT => '收入',
            self::OUTPUT => '支出'
        ];
    }

    public function getTypeInfo()
    {
        $dic = $this->getTypeDic();
        return isset($dic[$this->type]) ? $dic[$this->type] : '-';
    }

    public function getMember()
    {
        $member = Member::findOne($this->member_id);
        return $member ?:(new Member());
    }
}
