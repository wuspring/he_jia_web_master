<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "intergral_order".
 *
 * @property string $orderid
 * @property string $pay_sn
 * @property string $user_id
 * @property string $buyer_id
 * @property string $buyer_name
 * @property string $goods_amount
 * @property string $order_amount
 * @property integer $integral_amount
 * @property string $pd_amount
 * @property string $freight
 * @property integer $evaluation_state
 * @property integer $order_state
 * @property string $receiver
 * @property string $receiver_name
 * @property string $receiver_state
 * @property string $receiver_address
 * @property string $receiver_mobile
 * @property string $receiver_zip
 * @property string $shipping
 * @property string $shipping_code
 * @property string $deliveryTime
 * @property string $add_time
 * @property string $payment_time
 * @property string $finnshed_time
 * @property integer $fallinto_state
 * @property string $coupon_id
 * @property string $pay_method
 * @property integer $reback_status
 * @property string $type
 * @property integer $ticket_id
 * @property integer $expand
 * @property integer $relation_coupon
 * @property string $pay_amount
 * @property string $ticket_type
 */
class IntergralOrder extends \yii\db\ActiveRecord
{
    const TYPE_PUBLIC = 'PUBLIC';   // 普通订单
    const TYPE_GROUP = 'GROUP';     // 团购订单

    const STATUS_WAIT_PAY = '0'; // 待付款
    const STATUS_WAIT_SEND = '1'; // 待发货
    const STATUS_WAIT_RECIVE = '2'; // 待收货
    const STATUS_WAIT_JUDGE = '3';  // 待评价
//  const STATUS_WAIT_SHARE = 'wait_share'; // 待分享
    const STATUS_FINISH = '4'; // 完成
    const STATUS_CLOSE = '5'; // 交易关闭
    const STATUS_SERVICE = '6'; //正在申请售后
    const STATUS_F_SERVICE = '7'; //已退换
    const STATUS_WAIT_GROUP = '8'; // 待团购

    const WULIU_WAIT_ACCESS = '-'; // 待确认接受订单;
    const WULIU_ACCESS = 'access'; // 确认接受订单;
    const WULIU_CHOOSE = 'choose'; // 分拣配货
    const WULIU_SEND = 'send';     // 已发货
    const WULIU_RECIVE = 'recive';  // 确认收货


    const PAY_CODE_WECHAT = 'wechat';  // 支付方式：微信
    const PAY_CODE_ACCOUNT = 'account';  // 支付方式：账户
    const PAY_CODE_ALIPAY = 'alipay';  // 支付方式：账户
    const PAY_CODE_ZERO = 'zero';  // 支付方式：0支付


    const ORDER_TYPE_NORMAL = 'NORMAL';   // 普通订单
    const ORDER_TYPE_INTEGRAL = 'INTEGRAL'; // 积分订单

    const EXPAND_WORKDAY = '仅工作日送货';
    const EXPAND_WEEKEND = '仅周末送货';
    const EXPAND_ANYTIME = '任何时间均可';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderid'], 'required'],
            [['orderid', 'user_id', 'buyer_id', 'evaluation_state', 'order_state', 'receiver', 'shipping', 'fallinto_state', 'coupon_id', 'reback_status', 'ticket_id', 'relation_coupon'], 'integer'],
            [['goods_amount', 'order_amount', 'pd_amount', 'freight', 'pay_amount', 'integral_amount'], 'number'],
            [['deliveryTime', 'add_time', 'payment_time', 'finnshed_time', 'expand'], 'safe'],
            [['pay_sn', 'buyer_name', 'receiver_name', 'receiver_state', 'receiver_address', 'receiver_mobile', 'receiver_zip', 'shipping_code', 'type', 'ticket_type'], 'string', 'max' => 255],
            [['pay_method'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'orderid' => '订单编号',
            'pay_sn' => '支付编号',
            'user_id' => 'User ID',
            'buyer_id' => 'Buyer ID',
            'buyer_name' => '买家名称',
            'goods_amount' => '订单积分',
            'order_amount' => '订单积分',
            'integral_amount' => '订单积分',
            'pd_amount' => 'Pd Amount',
            'freight' => 'Freight',
            'evaluation_state' => 'Evaluation State',
            'order_state' => 'Order State',
            'receiver' => 'Receiver',
            'receiver_name' => '收货人',
            'receiver_state' => 'Receiver State',
            'receiver_address' => '收货地址',
            'receiver_mobile' => '联系方式',
            'receiver_zip' => 'Receiver Zip',
            'shipping' => 'Shipping',
            'shipping_code' => 'Shipping Code',
            'deliveryTime' => 'Delivery Time',
            'add_time' => '订单创建时间',
            'payment_time' => '订单支付时间',
            'finnshed_time' => '订单完成时间',
            'fallinto_state' => 'Fallinto State',
            'coupon_id' => 'Coupon ID',
            'pay_method' => 'Pay Method',
            'reback_status' => 'Reback Status',
            'type' => 'Type',
            'ticket_id' => 'Ticket ID',
            'relation_coupon' => 'Relation Coupon',
            'pay_amount' => 'Pay Amount',
            'ticket_type' => 'Ticket Type',
        ];
    }

    public function getOrderStatusDic()
    {
        $dic = [
            self::STATUS_WAIT_PAY => '待支付',
            self::STATUS_WAIT_SEND => '待发货',
            self::STATUS_WAIT_RECIVE => '待收货',
            self::STATUS_WAIT_JUDGE => '待评价',
            self::STATUS_FINISH => '完成',
            self::STATUS_CLOSE => '已关闭',
        ];

        return $dic;
    }

    /**
     * 获取订单状态
     */
    public function getOrderState()
    {
        $dic = $this->getOrderStatusDic();

        return isset($dic[$this->order_state]) ? $dic[$this->order_state] : '';
    }

    /**
     * 生成随机数
     */
    public function randomNum($length=10)
    {
        $str = '';
        for($i = 0; $i < $length; $i++) {
            $str .= mt_rand(0, 9);
        }
        return $str;
    }

    public function getNewNumber()
    {
        $string =  '0819' . date('ymd') . createRandKey(6);
        $order = self::findOne([
            'orderid' => $string
        ]);

        return $order ? $this->getNewNumber() : $string;
    }

    public function getNewPaySn()
    {
        $orderPaySn = 'bn' . date('Ymdhis') . createRandKey(4);
        $order = Order::findOne([
            'pay_sn' => $orderPaySn
        ]);

        return $order ? $this->getNewPaySn() : $orderPaySn;
    }

    public function getTypeInfo()
    {

        $dic = [
            self::TYPE_PUBLIC => '普通订单',
            self::TYPE_GROUP => '团购订单',
        ];

        return $dic[$this->type];
    }

    public function getGoods()
    {
        return $this->getOrderGoodsList($this->orderid);
    }

    public function getOrderGoodsList($orderid){
        $list = [];
        $goods_list = IntergralOrderGoods::find()->where(['order_id'=>$orderid])->orderBy('id ASC')->all();
        foreach ($goods_list as $key=>$item){
            $list[] = (object)[
                'goods_id'=>$item->goods_id,
                'goods_pic'=> Yii::$app->request->hostInfo.$item->goods_pic,
                'goods_name'=>$item->goods_name,
                'goods_price'=>$item->goods_price,
                'goods_integral'=>$item->goods_integral,
                'goods_pay_price'=>$item->goods_pay_price,
                'goods_num'=>$item->goods_num,
                'attr'=>$item->attr,
            ];
        }
        return $list;
    }
}
