<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ticket_info".
 *
 * @property integer $ticket_id
 * @property integer $provinces_id
 * @property string $ticket_name
 * @property string $ticket_amount
 * @property string $type
 */
class TicketInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id'], 'required'],
            [['ticket_id', 'provinces_id', 'ticket_amount'], 'integer'],
            [['ticket_name', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ticket_id' => 'Ticket ID',
            'provinces_id' => 'Provinces ID',
            'ticket_name' => 'Ticket Name',
            'ticket_amount' => 'Ticket Amount',
        ];
    }


    public function getTicket()
    {
        $ticket = Ticket::findOne($this->ticket_id);
        return $ticket ?:(new Ticket());
    }

    /**
     * 获取城市名称
     */
    public  function getCityName() {

       $provinces=Provinces::findOne($this->provinces_id);

       return empty($provinces)?'-':$provinces->cname;
    }


}
