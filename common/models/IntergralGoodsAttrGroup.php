<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "intergral_goods_attr_group".
 *
 * @property string $id
 * @property string $group_name
 * @property integer $is_delete
 */
class IntergralGoodsAttrGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_goods_attr_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_delete'], 'integer'],
            [['group_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_name' => 'Group Name',
            'is_delete' => 'Is Delete',
        ];
    }
}
