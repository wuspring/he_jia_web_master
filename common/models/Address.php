<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property string $id
 * @property string $memberId
 * @property string $name
 * @property string $province
 * @property string $city
 * @property string $region
 * @property string $address
 * @property string $tel
 * @property string $mobile
 * @property integer $isDefault
 */
class Address extends \yii\db\ActiveRecord
{
    public $provincesInfo='';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['memberId', 'isDefault'], 'integer'],
            [['name', 'province', 'city', 'region', 'address', 'tel', 'mobile','post_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'memberId' => Yii::t('app', '会员id'),
            'name' => Yii::t('app', '收货人姓名'),
            'province' => Yii::t('app', '省'),
            'city' => Yii::t('app', '市'),
            'region' => Yii::t('app', '地区'),
            'address' => Yii::t('app', '详细地址'),
            'tel' => Yii::t('app', '座机'),
            'post_code' => Yii::t('app', '邮编'),
            'mobile' => Yii::t('app', '手机号'),
            'isDefault' => Yii::t('app', '是否默认地址'),
        ];
    }

    /**
     * 获取省市区- 地址
     * @param int $provincesId
     * @return mixed|string
     */
    public function getProvinceInfo($provincesId=0)
    {
        $provincesId or $provincesId = $this->region;

        $provinces = Provinces::findOne(['id' => $provincesId]);
        if (!$provinces) {
            return '-';
        }

        $this->provincesInfo = "{$provinces->cname}, {$this->provincesInfo}";

        if ($provinces->upid <= 1) {
            return $this->provincesInfo;
        }

        return $this->getProvinceInfo($provinces->upid);
    }

    /**
     * @param $value
     * @return string
     */
    public function getProvinces($value)
    {
        $province = Provinces::findOne([
            'id' => $value
        ]);
        return $province ? $province->cname : '-';
    }

    public function getProvince($id)
    {
        $province = Provinces::findOne([
            'id' => $id
        ]);

        return $province ?$province->cname : '-';
    }
}
