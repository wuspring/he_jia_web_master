<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "store_shop".
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $provinces_id
 * @property string $store_name
 * @property string $description
 * @property string $address
 * @property string $work_days
 * @property string $work_times
 * @property string $notice
 * @property string $picitures
 */
class StoreShop extends \yii\db\ActiveRecord
{

    private $_workDaysDic = [
        '1' => '星期一',
        '2' => '星期二',
        '3' => '星期三',
        '4' => '星期四',
        '5' => '星期五',
        '6' => '星期六',
        '7' => '星期日',
    ];

    public  $province_id;
    public  $district_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_shop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'provinces_id'], 'required'],
            [['user_id', 'provinces_id','province_id','district_id'], 'integer'],
            [['lat', 'lng'], 'double'],
            [['description', 'address', 'work_times', 'notice', 'picitures', 'mobile'], 'string'],
            [['store_name', 'work_days'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'provinces_id' => '城市',
            'store_name' => 'Store Name',
            'description' => 'Description',
            'address' => 'Address',
            'work_days' => 'Work Days',
            'work_times' => 'Work Times',
            'notice' => 'Notice',
            'picitures' => 'Picitures',
            'lat'=>'纬度',
            'lng'=>'经度'
        ];
    }

    public function getProvincesInfo()
    {
        $provinces = Provinces::findOne($this->provinces_id);
        return $provinces ? : (new Provinces());
    }

    public function getWorkDaysInfo()
    {
        $data = (array)json_decode($this->work_days, true);
        return implode(' ', arrayGroupsAction(
            $data,
            function ($day) {
                return isset($this->_workDaysDic[$day]) ? $this->_workDaysDic[$day] : '';
            }));
    }
}
