<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%floor_index}}".
 *
 * @property string $id
 * @property string $img
 * @property string $zh_img
 * @property string $brand
 * @property string $good_class_id
 * @property string $provinces_id
 * @property string $create_time
 * @property string $zh_first_imgs
 */
class FloorIndex extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'floor_index';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['img', 'zh_img'], 'string'],
            [['create_time', 'good_class_id', 'provinces_id', 'zh_first_imgs'], 'safe'],
            [['brand','type', 'provinces_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'img' => Yii::t('app', '楼层图片'),
            'brand' => Yii::t('app', '品牌'),
            'type'=> Yii::t('app', '楼层类型'),
            'create_time' => Yii::t('app', '添加时间'),
        ];
    }
    /**
     *  查询对应图片—首页使用
     */
    public  static  function  getImgArr($type){

        $floor_index=FloorIndex::findOne(['type'=>$type]);

        return json_decode($floor_index->img);

    }

    public function getGoodClass()
    {
        $goodsClass = GoodsClass::findOne($this->good_class_id);
        return $goodsClass ? : (new GoodsClass());
    }

    public function getProvinces()
    {
        $goodsClass = Provinces::findOne($this->provinces_id);
        return $goodsClass ? : (new Provinces());
    }

    /**
     *  查询对应店铺—首页使用
     */
    public  static  function  getBrandArr($type){

        $floor_index=FloorIndex::findOne(['type'=>$type]);

        return json_decode($floor_index->brand);

    }

    /**
     * 查询店铺列表
     */
    public  function  getShopList(){

        $shoparr=User::find()->andFilterWhere(['in','id',json_decode($this->brand)])->all();
        return $shoparr;
    }

}
