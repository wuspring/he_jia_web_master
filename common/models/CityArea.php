<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "city_area".
 *
 * @property string $id
 * @property string $name
 * @property integer $pid
 * @property string $zipcode
 * @property integer $sort
 */
class CityArea extends \yii\db\ActiveRecord
{ 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city_area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pid'], 'required'],
            [['id', 'pid', 'sort'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['zipcode'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'pid' => Yii::t('app', 'Pid'),
            'zipcode' => Yii::t('app', 'Zipcode'),
            'sort' => Yii::t('app', 'Sort'),
        ];
    }
}
