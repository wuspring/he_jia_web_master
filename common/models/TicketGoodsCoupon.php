<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ticket_goods_coupon".
 *
 * @property string $id
 * @property string $ticket_id
 * @property integer $goods_coupon_id
 * @property string $sort
 */
class TicketGoodsCoupon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_goods_coupon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id', 'goods_coupon_id', 'sort'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticket_id' => 'Ticket ID',
            'goods_coupon_id' => '商品券ID',
            'sort' => '权重',
        ];
    }

    public function getGoodsCoupon()
    {
        $goodsCoupon = GoodsCoupon::findOne($this->goods_coupon_id);
        return $goodsCoupon ?: (new GoodsCoupon());
    }
}
