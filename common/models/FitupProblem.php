<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fitup_problem".
 *
 * @property string $id
 * @property string $problem
 * @property integer $member_id
 * @property string $create_time
 * @property integer $is_del
 * @property integer $is_show
 */
class FitupProblem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fitup_problem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['id', 'member_id', 'is_del', 'is_show','provinces_id','answer_num', 'sort'], 'integer'],
            [['problem'], 'string'],
            [['create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'problem' => Yii::t('app', '用户提出的问题'),
            'member_id' => Yii::t('app', '会员id'),
            'create_time' => Yii::t('app', '时间'),
            'is_del' => Yii::t('app', '软删除'),
            'is_show' => Yii::t('app', '后台控制是否显示'),
            'provinces_id' => Yii::t('app', '城市id'),
            'answer_num' => Yii::t('app', '回答数量'),
        ];
    }

    public function getMember(){

        $model=Member::findOne($this->member_id);

        return $model?:(new Member());

    }

    public  function getAnswerNum(){

        $num=FitupAnswer::find()->andFilterWhere(['problem_id'=>$this->id])->count();
        return $num;
    }

    public  function  getAnswerObj(){

        $answer=FitupAnswer::findAll(['problem_id'=>$this->id]);

        return $answer?:([]);
    }

    /**
     * 查询第一回答的问题
     */
    public  function  getOneAnswer(){

        $answer=FitupAnswer::find()->andFilterWhere(['problem_id'=>$this->id])->one();

        return $answer?:(new  FitupAnswer());
    }


}
