<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "system_coupon".
 *
 * @property string $id
 * @property string $store_id
 * @property string $member_id
 * @property string $ticket_id
 * @property string $provinces_id
 * @property string $money
 * @property integer $status
 * @property string $code
 * @property string $create_time
 */
class SystemCoupon extends \yii\db\ActiveRecord
{
    const STATUS_FALSE = '0';   // 未生效
    const STATUS_TRUE = '1';    // 已生效
    const STATUS_USED = '2';    // 已使用

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_coupon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id', 'member_id', 'ticket_id', 'provinces_id', 'status'], 'integer'],
            [['ticket_id', 'provinces_id'], 'required'],
            [['money'], 'number'],
            [['create_time'], 'safe'],
            [['code'], 'string', 'max' => 32],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_id' => 'Store ID',
            'member_id' => 'Member ID',
            'ticket_id' => 'Ticket ID',
            'provinces_id' => 'Provinces ID',
            'money' => 'Money',
            'status' => 'Status',
            'code' => 'Code',
            'create_time' => 'Create Time',
        ];
    }

    public function getOrder()
    {
        $order = Order::findOne([
            'relation_coupon' => $this->id,
            'ticket_id' => $this->ticket_id,
            'type' => Order::TYPE_ZHAN_HUI
        ]);

        if (!$order) {
            throw new \Exception("数据异常！未找到订单");
        }

        return $order;
    }
    public function create()
    {
        if (!strlen($this->code) and !$this->status) {
            $key = (string)$this->ticket_id;
            strlen($this->ticket_id) < 2 and $key = "0{$key}";

            $dic = '01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $key = $key . createRandKey(6, $dic);

            $record = self::findOne([
                'code' => $key
            ]);

            if ($record) {
                return $this->create();
            }

            $this->code = $key;
            $this->create_time = date('Y-m-d H:i:s');
        }

        return $this->save();
    }

    public function getMember()
    {
        $member = Member::findOne($this->member_id);
        return $member ? : (new Member());
    }
}
