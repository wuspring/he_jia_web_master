<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "intergral_order_goods".
 *
 * @property string $id
 * @property string $order_id
 * @property string $goods_id
 * @property string $goods_pic
 * @property string $goods_name
 * @property string $goods_price
 * @property integer $goods_integral
 * @property integer $fallinto_state
 * @property integer $goods_num
 * @property string $fallInto
 * @property string $goods_pay_price
 * @property string $buyer_id
 * @property string $createTime
 * @property string $modifyTime
 * @property string $sku_id
 * @property string $attr
 * @property string $sell_price
 */
class IntergralOrderGoods extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_order_goods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'goods_id', 'goods_integral', 'fallinto_state', 'goods_num', 'buyer_id', 'sku_id'], 'integer'],
            [['goods_price', 'fallInto', 'goods_pay_price', 'sell_price'], 'number'],
            [['createTime', 'modifyTime'], 'safe'],
            [['attr'], 'string'],
            [['goods_pic', 'goods_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'goods_id' => 'Goods ID',
            'goods_pic' => 'Goods Pic',
            'goods_name' => 'Goods Name',
            'goods_price' => 'Goods Price',
            'goods_integral' => 'Goods Integral',
            'fallinto_state' => 'Fallinto State',
            'goods_num' => 'Goods Num',
            'fallInto' => 'Fall Into',
            'goods_pay_price' => 'Goods Pay Price',
            'buyer_id' => 'Buyer ID',
            'createTime' => 'Create Time',
            'modifyTime' => 'Modify Time',
            'sku_id' => 'Sku ID',
            'attr' => 'Attr',
            'sell_price' => 'Sell Price',
        ];
    }
}
