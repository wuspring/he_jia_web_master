<?php

namespace common\models;

use DL\Project\Store;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "store_gc_score".
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $provinces_id
 * @property integer $gc_id
 * @property integer $admin_score
 * @property string $score
 */
class StoreGcScore extends \yii\db\ActiveRecord
{
    public $a_score;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_gc_score';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'provinces_id', 'gc_id'], 'required'],
            [['user_id', 'provinces_id', 'gc_id', 'score', 'admin_score'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'provinces_id' => 'Provinces ID',
            'gc_id' => 'Gc ID',
            'score' => 'Score',
        ];
    }

    public function getStore()
    {
        try {
            $store = Store::init()->info($this->user_id);
            if (!$store) {
                throw new \Exception("未找到有效店铺");
            }
            return $store;
        } catch (\Exception $e) {
            stopFlow($e->getMessage());
        }

    }
    /**
     * get GoodGlass
     *
     * @return GoodsClass
     */
    public function getGcInfo()
    {
        return GoodsClass::findOne($this->gc_id);
    }
    /**
     *  查询种类名称
     */
    public function getGoodClass(){

        $goodClass=GoodsClass::findOne($this->gc_id);

        return ($goodClass)?:(new  GoodsClass());
    }
    /**
     *  查询这类gc_id 对用的优惠劵
     */
    public static  function  getGoodsCoupon($provinces_id,$gc_id){

        $gc_three_son=GoodsClass::findAll(['fid'=>$gc_id,'is_del'=>0]);
        $storeGc=StoreGcScore::find()->andFilterWhere(['provinces_id'=>$provinces_id,'gc_id'=>$gc_id])->groupBy('user_id')->all();
        $userArrId=ArrayHelper::getColumn($storeGc,'user_id');

        $goodsCoupon=GoodsCoupon::find()->alias('gc')
                                        ->leftJoin('goods as goods','goods.id =gc.goods_id')
                                        ->andFilterWhere(['in','gc.user_id',$userArrId])
                                        ->andFilterWhere(['in','goods.gc_id',$gc_three_son])->limit(6)->all();

         return $goodsCoupon;
    }
}
