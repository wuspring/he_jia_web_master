<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "goods_sk_log".
 *
 * @property string $id ID
 * @property string $date 日期
 * @property int $goods_id 商品ID
 * @property string $limbo_index 秒杀时段
 * @property int $num 当日商品数量
 */
class GoodsSkLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goods_sk_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['goods_id', 'limbo_index', 'num'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'goods_id' => 'Goods ID',
            'limbo_index' => 'Limbo Index',
            'num' => 'Num',
        ];
    }
}
