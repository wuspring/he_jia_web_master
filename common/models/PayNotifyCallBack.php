<?php
namespace common\models;

use Yii;
use common\models\WxPayApi;
use common\models\WxPayNotify;
use common\models\WxPayOrderQuery;
use common\models\WxPayMact;
use common\models\JsApiPay;

class PayNotifyCallBack extends WxPayNotify
{
	//查询订单
	public function Queryorder($transaction_id)
	{
		$input = new WxPayOrderQuery();
		$input->SetTransaction_id($transaction_id);
		$result = WxPayApi::orderQuery($input);
		// Log::DEBUG("query:" . json_encode($result));
		if(array_key_exists("return_code", $result)&& array_key_exists("result_code", $result)&& $result["return_code"] == "SUCCESS"&& $result["result_code"] == "SUCCESS")
		{
            $model=Order::findOne($result["out_trade_no"]);
			if ($model->state==1) {

                $model->pay_sn=$transaction_id;
                $model->state=2;
                $model->payCode = '微信支付';
                $model->paymentTime=date('Y-m-d H:i:s');

				if($model and $model->save()){
				    // todo 库存操作



					return true;
				}else{
					return false;
				}
			}else{
				return true;
			}	
		}
		return false;
	}

    public function setMember($member,$order){

            if(strtotime($member->expiryTime)>strtotime(date("Y-m-d H:i:s"))){
                $member->expiryTime =date("Y-m-d H:i:s",strtotime("+".$order->num."  month",strtotime( $member->expiryTime)));
            }else{
                $member->expiryTime =date("Y-m-d H:i:s",strtotime("+".$order->num."  month",strtotime( date("Y-m-d H:i:s"))));
            }

            $member->role=$order->levelid;
            $member->save();
    }

	public function fengcheng($user,$order,$fenxiao,$bianhao){
		$member=Member::findOne($user->pid);
		//判断分成模式1为固定金额分成 2为商品金额分成
		if ($member->pid>0) {
			$this->fenxiao($member,$order,$bianhao,$fenxiao);
			$this->fengcheng($member,$order,$fenxiao,$bianhao+1);
		}else{
			$this->fenxiao($member,$order,$bianhao,$fenxiao);
		}
	}
    /**
    * 生成随机数
    */
    public  function randomNum($length=10)
    {
        $str = '';
        for($i = 0; $i < $length; $i++) {
            $str .= mt_rand(0, 9);
        }
        return $str;
    }
	public function fenxiao($member,$order,$bianhao,$fenxiao){
		

			$money=$fenxiao->lists[$bianhao]->percent/100*$order->amount;

			if ($fenxiao->fashion=="1") {
				$money=$fenxiao->lists[$bianhao]->money;
			}
    		
			$log=new Fallintolog();
			$log->orderId=$order->orderId;
			$log->levelId=$order->levelid;
			$log->memberId=$member->id;
			$log->fallIntoMoney=$order->amount;
			$log->createTime=date('Y-m-d H:i:s');
			$log->modifyTime=$log->createTime;



				if ($fenxiao->cash=='on') {

					$txState=$this->Mact($member->wxopenid,$money,$order->orderId,$order->levelid,$member->id);
					if ($txState) {
						$member->brokerage=$member->brokerage+$money;
					}else{
						$member->remainder=$member->remainder+$money;	
					}
				}else{
					$member->remainder=$member->remainder+$money;
				}	
				$log->actualMoney=$money;
				

				if ($member->save()) {
					$log->state=1;
				}else{
					$log->state=0;
				}



			$log->save();
            $order->isFallInto=1;
            $order->save();
			$this->PidMember($member,$money,$order);
		
	}
	public function Mact($openid,$money,$orderid,$goodsid,$memberid){
	   	$runtdata= $this->getMact($openid,$money,"余额体现");
      	if ($runtdata['return_code']=="SUCCESS") {
         	if ($runtdata['result_code']=="SUCCESS") {
 				$cash=new Cash();
				$cash->state=2;
				$cash->createTime=date('Y-m-d H:i:s');
				$cash->modifyTime=$cash->createTime;
				$cash->payOrderNum=$runtdata['partner_trade_no'];
				$cash->goodsOrderid=$orderid;
				$cash->presentTime=date('Y-m-d H:i:s');
				$cash->goodsId=$goodsid;
				$cash->type=1;
				$cash->money=$money;
				$cash->memberId=$memberid;
				$cash->save();
             	return true;
         	}else{
         		return false;
         	}
      	}else{
      		return false;
      	}
	}
    private function getMact($openId,$money,$desc){
       $input = new WxPayMact();
       $out_trade_no="9".date("YmdHis").$this->randomNum(3);

       $input->SetPartner_trade_no($out_trade_no);
       $input->SetOpenid($openId);
       $input->SetCheck_name("NO_CHECK");
       $input->SetAmount($money*100);
       $input->SetDesc($desc);

       $order = WxPayApi::mactOrder($input);  
       return $order;
    }
	//重写回调处理函数
	public function NotifyProcess($data, &$msg)
	{
		//Log::DEBUG("call back:" . json_encode($data));
		$notfiyOutput = array();
		
		if(!array_key_exists("transaction_id", $data)){
			$msg = "输入参数不正确";
			return false;
		}
		//查询订单，判断订单真实性
		if(!$this->Queryorder($data["transaction_id"])){
			$msg = "订单查询失败";
			return false;
		}
		return true;
	}

    public static function notify($callback, &$msg)
    {
        //获取通知的数据
        if (isset($GLOBALS['HTTP_RAW_POST_DATA'])) {
            $xml = $GLOBALS['HTTP_RAW_POST_DATA'];
        } else {
            $xml = file_get_contents('php://input');
        }

        if (!strlen($xml)) {
            return false;
        }

        //如果返回成功则验证签名
        try {
            $result = WxPayResults::Init($xml);
        } catch (WxPayException $e){
            $msg = $e->errorMessage();
            return false;
        }

        return call_user_func($callback, $result);
    }
}