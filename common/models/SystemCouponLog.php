<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "system_coupon_log".
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $coupon_id
 * @property integer $goods_id
 * @property string $type
 * @property string $create_time
 */
class SystemCouponLog extends \yii\db\ActiveRecord
{
    const TYPE_NORMAL = 'NORMAL';    // 正常
    const TYPE_CHANGE = 'CHANGE';    // 改签

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_coupon_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'coupon_id'], 'required'],
            [['user_id', 'coupon_id', 'goods_id'], 'integer'],
            [['create_time', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'coupon_id' => 'Coupon ID',
            'goods_id' => 'Goods ID',
            'create_time' => 'Create Time',
        ];
    }

    public function getStore()
    {
        $store = User::findOne($this->user_id);
        return $store?:(new User());
    }

    public function getCoupon()
    {
        $coupon = SystemCoupon::findOne($this->coupon_id);
        return $coupon?:(new SystemCoupon());
    }

    public function getGoods()
    {
        $goods = Goods::findOne($this->goods_id);
        return $goods?:(new Goods());
    }
}
