<?php

namespace common\models;

use DL\Project\Store;
use Yii;

/**
 * This is the model class for table "store_info".
 *
 * @property string $id
 * @property string $user_id
 * @property string $tab_text
 * @property string $describe
 * @property integer $store_amount
 * @property integer $provinces_id
 * @property string $store_info
 * @property string $default_shop
 * @property string $advance_goods_id
 * @property string $order_goods_id
 * @property string $store_coupon_id
 * @property double $score
 * @property integer $judge_amount
 */
class StoreInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'store_amount', 'judge_amount', 'provinces_id'], 'integer'],
            [['tab_text', 'describe', 'store_info', 'advance_goods_id', 'order_goods_id', 'store_coupon_id', 'default_shop'], 'string'],
            [['score'], 'number'],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'tab_text' => 'Tab Text',
            'describe' => 'Describe',
            'store_amount' => 'Store Amount',
            'store_info' => 'Store Info',
            'judge_amount' => 'Judge Amount',
        ];
    }

    public function getUser()
    {
        $user = User::findOne($this->user_id);
        return $user ? : (new User());
    }

    public function getProvincesInfo()
    {
        $provinces = Provinces::findOne($this->provinces_id);
        return $provinces ? : (new Provinces());
    }

    /**
     * @return null
     */
    public function getDefaultShop()
    {
        $shopId = strlen($this->default_shop) ? (int)$this->default_shop :0;

        $shop = StoreShop::findOne([
            'user_id' => $this->user_id,
            'id' => $shopId
        ]);
        $shop or $shop = StoreShop::findOne([
            'user_id' => $this->user_id,
        ]);

        return $shop ? Store::init()->shopInfo($shop->id) : (false);
    }
}
