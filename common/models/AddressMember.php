<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property string $id
 * @property string $memberId
 * @property string $name
 * @property string $province
 * @property string $city
 * @property string $region
 * @property string $address
 * @property string $tel
 * @property string $mobile
 * @property integer $isDefault
 */
class AddressMember extends \yii\db\ActiveRecord
{
    public $provincesInfo='';
    const TYPE_DA_LU= 'DA_LU';
    const TYPE_XIANG_GANG= 'XIANG_HANG';
    const TYPE_TAI_WAN= 'TAI_WAN';
    const TYPE_MEI_GUO= 'MEI_GUO';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'mobile', 'name'], 'required', 'message' => '请填写'],
            ['mobile', 'match', 'pattern' => '/^[1][345678][0-9]{9}$/', 'message' => '手机号格式填写不正确'],
            [['mobile', 'isDefault'], 'integer'],
            [['name', 'province', 'city', 'region', 'address', 'tel', 'mobile','post_code','nation'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nation' => Yii::t('app', '国家'),
            'memberId' => Yii::t('app', '会员id'),
            'name' => Yii::t('app', '收货人姓名'),
            'province' => Yii::t('app', '省'),
            'city' => Yii::t('app', '市'),
            'region' => Yii::t('app', '地区'),
            'address' => Yii::t('app', '详细地址'),
            'tel' => Yii::t('app', '座机'),
            'post_code' => Yii::t('app', '邮编'),
            'mobile' => Yii::t('app', '手机号'),
            'isDefault' => Yii::t('app', '是否默认地址'),
        ];
    }

    public function getTypeDic()
    {
        return [
            self::TYPE_DA_LU => '中国大陆',
            self::TYPE_MEI_GUO => '美国',
            self::TYPE_TAI_WAN => '中国台湾',
            self::TYPE_XIANG_GANG=>'中国香港'
        ];
    }

    public function getTypeInfo()
    {
        $dic = $this->getTypeDic();

        return isset($dic[$this->nation]) ? $dic[$this->nation] : '-';
    }

    /**
     * 获取会员信息
     */
    public  function getMember(){

        $member=Member::findOne($this->memberId);

        return $member?:new Member();
    }

    /**
     * 获取省市区- 地址
     * @param int $provincesId
     * @return mixed|string
     */
    public function getProvinceInfo($provincesId=0)
    {
        $provincesId or $provincesId = $this->region;

        $provinces = Provinces::findOne(['id' => $provincesId]);
        if (!$provinces) {
            return '-';
        }

        $this->provincesInfo = "{$provinces->cname} {$this->provincesInfo}";

        if ($provinces->upid <= 1) {
            return $this->provincesInfo;
        }

        return $this->getProvinceInfo($provinces->upid);
    }

    /**
     * @param $value
     * @return string
     */
    public function getProvinces($value)
    {
        $province = Provinces::findOne([
            'id' => $value
        ]);
        return $province ? $province->cname : '-';
    }

    public function getProvince($id)
    {
        $province = Provinces::findOne([
            'id' => $id
        ]);

        return $province ?$province->cname : '-';
    }
}
