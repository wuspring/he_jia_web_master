<?php
namespace common\models;

class WxPayException
{
	public function errorMessage()
	{
		return $this->getMessage();
	}
}
