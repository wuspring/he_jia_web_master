<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "goods".
 *
 * @property string $id
 * @property string $goods_name
 * @property string $goods_jingle
 * @property string $gc_id
 * @property integer $use_attr
 * @property string $brand_id
 * @property string $goods_price
 * @property string $goods_promotion_price
 * @property integer $goods_promotion_type
 * @property string $goods_marketprice
 * @property string $goods_serial
 * @property integer $goods_storage_alarm
 * @property integer $goods_click
 * @property integer $goods_salenum
 * @property integer $goods_collect
 * @property string $attr
 * @property string $goods_spec
 * @property string $goods_body
 * @property string $mobile_body
 * @property integer $goods_storage
 * @property string $goods_image
 * @property integer $goods_state
 * @property string $goods_addtime
 * @property string $goods_edittime
 * @property integer $goods_vat
 * @property integer $goods_commend
 * @property double $evaluation_good_star
 * @property integer $evaluation_count
 * @property integer $is_virtual
 * @property integer $is_appoint
 * @property integer $is_presell
 * @property integer $have_gift
 * @property integer $user_id
 * @property integer $isdelete
 * @property integer $type
 * @property integer $integral
 * @property integer $admin_score
  * @property string $keywords
 * @property string $description
 */
class Goods extends \yii\db\ActiveRecord
{
    public $good_amount;
    const TYPE_MONEY = '1'; // 现金商品
    const TYPE_INTEGRAL = '2'; // 积分商品
    const TYPE_FIX = '3'; // 混合商品

    public  $ticketType;
    public $ticket_price;   // 预定金额 定金
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gc_id', 'is_second_kill', 'egc_id', 'goods_hot', 'use_attr', 'brand_id', 'goods_promotion_type', 'goods_storage_alarm', 'goods_click', 'goods_salenum', 'goods_collect', 'goods_storage', 'goods_state', 'goods_vat', 'goods_commend', 'evaluation_count', 'is_virtual', 'is_appoint', 'is_presell', 'have_gift', 'isdelete', 'type', 'integral', 'user_id', 'admin_score'], 'integer'],
            [['goods_price', 'goods_promotion_price', 'goods_marketprice', 'second_price', 'evaluation_good_star','goods_weight', 'second_limit'], 'number'],
            [['attr', 'second_kill_groups', 'goods_spec', 'goods_body', 'mobile_body', 'goods_image','goods_jingle', 'keywords', 'description'], 'string'],
            [['goods_addtime', 'goods_edittime', 'third_code'], 'safe'],
            [['goods_name','goods_serial','goods_pic','unit'], 'string', 'max' => 255],
            [['is_virtual', 'is_appoint','is_presell','have_gift','goods_vat','goods_promotion_type','use_attr','isdelete'], 'default','value' => 0],
            [['goods_image'],'default','value' =>json_encode(array())],
            [['goods_addtime','goods_edittime'],'default','value' =>date('Y-m-d H:i:s')],
            [['user_id'],'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'goods_name' => Yii::t('app', '商品名称'),
            'goods_jingle' => Yii::t('app', '商品广告词'),
            'gc_id' => Yii::t('app', '商品分类id'),
            'egc_id' =>  Yii::t('app', '栏目专区'),
            'use_attr' => Yii::t('app', '是否使用规格'),
            'brand_id' => Yii::t('app', '品牌id'),
            'goods_price' => Yii::t('app', '商品价格'),
            'goods_promotion_price' => Yii::t('app', '商品促销价格'),
            'goods_promotion_type' => Yii::t('app', '促销类型 0无促销，1团购，2限时折扣'),
            'goods_marketprice' => Yii::t('app', '市场价'),
            'goods_serial' => Yii::t('app', '商家编号'),
            'goods_storage_alarm' => Yii::t('app', '库存报警值'),
            'goods_click' => Yii::t('app', '商品点击数量'),
            'goods_salenum' => Yii::t('app', '销售数量'),
            'goods_collect' => Yii::t('app', '收藏数量'),
            'attr' => Yii::t('app', '规格属性'),
            'goods_spec' => Yii::t('app', '商品规格序列化'),
            'goods_body' => Yii::t('app', '商品详情'),
            'mobile_body' => Yii::t('app', '手机端详情'),
            'goods_storage' => Yii::t('app', '商品库存'),
            'goods_image' => Yii::t('app', '商品相册'),
            'goods_state' => Yii::t('app', '商品状态 0下架，1正常，10违规（禁售）'),
            'goods_addtime' => Yii::t('app', '商品添加时间'),
            'goods_edittime' => Yii::t('app', '商品编辑时间'),
            'goods_vat' => Yii::t('app', '是否开具增值税发票 1是，0否'),
            'goods_commend' => Yii::t('app', '商品推荐 1是，0否 默认0'),
            'evaluation_good_star' => Yii::t('app', '好评星级'),
            'evaluation_count' => Yii::t('app', '评价数'),
            'is_virtual' => Yii::t('app', '是否为虚拟商品 1是，0否'),
            'is_appoint' => Yii::t('app', '是否是预约商品 1是，0否'),
            'is_presell' => Yii::t('app', '是否是预售商品 1是，0否'),
            'have_gift' => Yii::t('app', '是否拥有赠品'),
            'isdelete' => Yii::t('app', '是否删除'),
            'type' => Yii::t('app', '商品类型:1现金商品 2积分商品 3混合商品'),
            'integral' => Yii::t('app', '积分'),
            'goods_pic' => Yii::t('app', '商品主图'),
            'goods_weight' => Yii::t('app', '重量'),
            'second_price' => '秒杀价格',
            'second_limit' => '时段秒杀数量限制',
            'unit' => '单位,套,件,户',
            'admin_score' => '管理员打分',
        ];
    }

    public function  getGoodsClass($pid=0,$list=array()){
        $calsslist= GoodsClass::find()->where(['fid'=>$pid])->orderBy(" sort desc")->all();
        foreach ($calsslist as $key=>$item){
            $count= GoodsClass::find()->where(['fid'=>$item->id])->count();
            if ($count>0){
                $list[$item->name]= $this->getGoodsClass($item->id);
            }else{
                $list[$item->id]='|-'.$item->name;
            }
        }
        return $list;
    }


    public function setThirdCode()
    {
        $good = Goods::find()->where([])->orderBy('id desc')->one();
//        $gift = Gift::find()->where([])->orderBy('id desc')->one();
        $goodNum = $good ? $good->id : 0;
//        $giftNum = $gift ? $gift->id : 0;

//        $this->third_code = $goodNum + $giftNum + 1000;
        $this->third_code = $goodNum + 1000;
    }

    public function getGclass(){
        $model=GoodsClass::findOne(['id'=>$this->gc_id]);
        if (empty($model)){
            $model=new GoodsClass();
        }
        return $model;
    }

       public  function getUser(){
           $model=User::findOne($this->user_id);

           return ($model)?:(new User());
       }


    public  function getUserById($id){
        $model=User::findOne($id);

        return ($model)?:(new User());
    }


    public function getBrand(){
        $userId = Yii::$app->user->id;
        $list=  Brand::find()->where([
            'user_id' => $userId
        ])->asArray()->all();

        $listData=ArrayHelper::map($list,'id','brand_name');
        return $listData;
    }

    public function getBranddata(){
        $model=Brand::findOne(['id'=>$this->brand_id]);
        if (empty($model)){
            $model=new Brand();
            $model->brand_name='';
            $model->brand_pic='';
            $model->show_type=1;
        }
        return $model;
    }

    /**
     * 商品是否被收藏
     * @param int $memberId
     * @return int
     */
    public function getIsCollect($memberId=0)
    {
        $collect = Collect::findOne([
            'member_id' => $memberId,
            'goods_id' => $this->id
        ]);

        return $collect ? 1 : 0;
    }

    public function getTypeInfo()
    {
        $dic = [
            self::TYPE_MONEY => '现金商品',
            self::TYPE_INTEGRAL => '积分商品',
            self::TYPE_FIX => '混合商品'
        ];

        return isset($dic[$this->type]) ?  $dic[$this->type] : '-';
    }

    public function getStore()
    {
       $storeInfo = [
           'user_id' => 0,
           'nickname' => '',
           'provinces_id' => '',
           'avatarTm' => ''
       ];

       $store = User::findOne($this->user_id);
       if ($store) {
           $storeInfo['user_id'] = $store->id;
           $storeInfo['nickname'] = $store->nickname;
           $storeInfo['provinces_id'] = $store->provinces_id;
           $storeInfo['avatarTm'] = $store->avatarTm;
       }

       return (object)$storeInfo;
    }

    public  function getGoodsCoupon(){

        $goodCoupon=GoodsCoupon::findAll(['goods_id'=>$this->id,'is_del'=>0]);
        return $goodCoupon? : [];
    }

}
