<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property string $order_id
 * @property integer $goods_id
 * @property string $type
 * @property string $content
 * @property string $imgs
 * @property integer $member_id
 * @property string $create_time
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'goods_id', 'member_id'], 'integer'],
            [['spzl', 'shfw'], 'double'],
            [['content', 'imgs'], 'string'],
            [['create_time'], 'safe'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'goods_id' => Yii::t('app', 'Goods ID'),
            'type' => Yii::t('app', 'good 好评 mid  中评 low 差评'),
            'content' => Yii::t('app', '内容'),
            'imgs' => Yii::t('app', '上传图片'),
            'member_id' => Yii::t('app', '会员ID'),
            'create_time' => Yii::t('app', '评论时间'),
            'spzl' => Yii::t('app', '商品质量'),
            'shfw' => Yii::t('app', '商品服务'),
        ];
    }
    public  function getMember(){
        $model=Member::findOne(['id'=>$this->member_id]);
        if (empty($model)){
            $model=new Member();
        }
        return $model;
    }

    public  function  getImgArr(){

        $arr=json_decode($this->imgs);
        ($arr) or $arr=[];
        return $arr;
    }

}
