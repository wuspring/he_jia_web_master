<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $goods_id
 * @property string $add_time
 * @property string $create_time
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'goods_id'], 'integer'],
            [['add_time', 'create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'goods_id' => 'Goods ID',
            'add_time' => 'Add Time',
            'create_time' => 'Create Time',
        ];
    }

    /**
     * 关联商品表
     */
    public function getGoods()
    {
        $good = Goods::findOne($this->goods_id);
        return $good ?: (new Goods());
    }
}
