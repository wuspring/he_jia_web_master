<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "member_coupon".
 *
 * @property string $id
 * @property string $store_id
 * @property string $member_id
 * @property string $coupon_id
 * @property string $coupon_auto_send_id
 * @property string $begin_time
 * @property string $end_time
 * @property integer $is_expire
 * @property integer $is_use
 * @property integer $is_delete
 * @property string $addtime
 * @property integer $type
 * @property integer $integral
 * @property string $price
 */
class MemberStoreCoupon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member_store_coupon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id', 'member_id', 'coupon_id', 'coupon_auto_send_id', 'is_expire', 'is_use', 'is_delete', 'type', 'integral','ticket_id','good_id', 'ticket_id'], 'integer'],
            [['begin_time', 'end_time', 'addtime', 'code'], 'safe'],
            [['price','good_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'store_id' => Yii::t('app', '店铺编号'),
            'member_id' => Yii::t('app', '会员编号'),
            'coupon_id' => Yii::t('app', '优惠券编号'),
            'coupon_auto_send_id' => Yii::t('app', '自动发放id'),
            'begin_time' => Yii::t('app', '有效期开始时间'),
            'end_time' => Yii::t('app', '有效期结束时间'),
            'is_expire' => Yii::t('app', '是否已过期：0=未过期，1=已过期'),
            'is_use' => Yii::t('app', '是否已使用：0=未使用，1=已使用'),
            'is_delete' => Yii::t('app', '是否删除'),
            'addtime' => Yii::t('app', '添加时间'),
            'type' => Yii::t('app', '领取类型 0--平台发放 1--自动发放 2--领取'),
            'integral' => Yii::t('app', '兑换支付积分数量'),
            'price' => Yii::t('app', '兑换支付价格'),
            'ticket_id' => Yii::t('app', '票据id'),
            'good_id' => Yii::t('app', '商品id'),
            'good_price' => Yii::t('app', '商品活动价格'),
        ];
    }

    public function getGoodsCoupon()
    {
        $coupon = GoodsCoupon::findOne($this->coupon_id);
        return $coupon ?: (new GoodsCoupon());
    }

    public function getMember()
    {
        $member = Member::findOne($this->member_id);
        return $member ?: (new Member());
    }

    public  function getUser(){

        $user=User::findOne($this->store_id);
        return $user?:(new  User());
    }

    public function create()
    {
        if (!strlen($this->code)) {
            $key = (string)'ST' . date('Y');

            $dic = '01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $key = $key . createRandKey(6, $dic);

            $record = self::findOne([
                'code' => $key
            ]);

            if ($record) {
                return $this->create();
            }

            $this->code = $key;
        }

        return $this->save();
    }
}
