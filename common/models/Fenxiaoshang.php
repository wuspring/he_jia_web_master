<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fenxiaoshang".
 *
 * @property string $id
 * @property integer $member_id
 * @property string $name
 * @property string $phone
 * @property string $recommend
 * @property string $status
 * @property string $create_time
 */
class Fenxiaoshang extends \yii\db\ActiveRecord
{
    const STATUS_WAIT = 'WAIT';         // 等待审核
    const STATUS_APPLY = 'APPLY';       // 同意申请
    const STATUS_REFUSE = 'REFUSE';     // 拒绝申请

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fenxiaoshang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id'], 'required'],
            [['member_id'], 'integer'],
            [['create_time'], 'safe'],
            [['name', 'recommend'], 'string', 'max' => 255],
            [['phone', 'status'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'recommend' => 'Recommend',
            'status' => 'Status',
            'create_time' => 'Create Time',
        ];
    }

    public function getMember()
    {
        $member = Member::findOne($this->member_id);
        return $member?: (new Member());
    }
}
