<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "intergral_luck_draw".
 *
 * @property string $id
 * @property integer $member_id
 * @property integer $prize_terms
 * @property integer $draw_id
 * @property string $create_time
 * @property integer $is_del
 * @property integer $is_show
 */
class IntergralLuckDraw extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_luck_draw';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'prize_terms', 'draw_id', 'is_del', 'is_show'], 'integer'],
            [['create_time','number'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', '会员id'),
            'prize_terms' => Yii::t('app', '抽奖是第几期的'),
            'draw_id' => Yii::t('app', '抽奖对应的id'),
            'create_time' => Yii::t('app', '时间'),
            'is_del' => Yii::t('app', '软删除'),
            'is_show' => Yii::t('app', '是否显示'),
        ];
    }

    public  function getMember(){
        $member=Member::findOne($this->member_id);
        return $member?:(new  Member());
    }

    /**
     * 积分转盘
     */
    public  function  getIntergralTurntable(){

        $intergralTurn=IntergralTurntable::findOne($this->draw_id);

        return $intergralTurn?:(new IntergralTurntable());
    }

    /**
     * @return mixed|string
     * 创建积分订单号
     */
    public function getNewOrderNumber()
    {
        $orderNum = 'JF'.date('Ymdhis') . createRandKey(4);

        $order = self::findOne([
            'number' => $orderNum
        ]);
        return $order ? $this->getNewOrderNumber() : $orderNum;
    }
}
