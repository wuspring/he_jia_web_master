<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "intergral_treasure".
 *
 * @property string $id
 * @property string $goods_name
 * @property string $from_id
 * @property string $goods_jingle
 * @property string $gc_id
 * @property integer $use_attr
 * @property string $goods_integral
 * @property string $goods_marketprice
 * @property string $goods_serial
 * @property integer $goods_storage_alarm
 * @property integer $goods_click
 * @property integer $goods_salenum
 * @property integer $goods_collect
 * @property string $attr
 * @property string $goods_spec
 * @property string $goods_body
 * @property string $mobile_body
 * @property string $goods_storage
 * @property string $goods_pic
 * @property string $goods_image
 * @property integer $goods_state
 * @property string $goods_addtime
 * @property string $goods_edittime
 * @property string $end_time
 * @property integer $goods_vat
 * @property double $evaluation_good_star
 * @property integer $evaluation_count
 * @property integer $is_virtual
 * @property integer $is_appoint
 * @property integer $is_presell
 * @property integer $have_gift
 * @property integer $isdelete
 * @property string $goods_weight
 * @property integer $is_second_kill
 * @property integer $goods_hot
 * @property string treasure_log_ids
 */
class IntergralTreasure extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_treasure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_id', 'gc_id', 'use_attr', 'goods_storage_alarm', 'goods_click', 'goods_salenum', 'goods_collect', 'goods_storage', 'goods_state', 'goods_vat', 'evaluation_count', 'is_virtual', 'is_appoint', 'is_presell', 'have_gift', 'isdelete', 'is_second_kill', 'goods_hot'], 'integer'],
            [['goods_integral', 'goods_marketprice', 'evaluation_good_star', 'goods_weight'], 'number'],
            [['attr', 'goods_spec', 'goods_body', 'mobile_body', 'goods_image', 'treasure_log_ids', 'keywords', 'description'], 'string'],
            [['goods_addtime', 'goods_edittime', 'end_time'], 'safe'],
            [['goods_name', 'goods_serial', 'goods_pic'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'goods_name' => '商品名称',
            'from_id' => 'From ID',
            'goods_jingle' => '商品规格',
            'gc_id' => 'Gc ID',
            'use_attr' => 'Use Attr',
            'goods_integral' => '单次夺宝积分',
            'goods_marketprice' => 'Goods Marketprice',
            'goods_serial' => 'Goods Serial',
            'goods_storage_alarm' => 'Goods Storage Alarm',
            'goods_click' => 'Goods Click',
            'goods_salenum' => '已参加人次',
            'goods_collect' => 'Goods Collect',
            'attr' => 'Attr',
            'goods_spec' => 'Goods Spec',
            'goods_body' => '商品详情',
            'mobile_body' => '售后保障',
            'goods_storage' => '开奖所需人次',
            'goods_pic' => 'Goods Pic',
            'goods_image' => 'Goods Image',
            'goods_state' => 'Goods State',
            'goods_addtime' => 'Goods Addtime',
            'goods_edittime' => 'Goods Edittime',
            'goods_vat' => 'Goods Vat',
            'evaluation_good_star' => 'Evaluation Good Star',
            'evaluation_count' => 'Evaluation Count',
            'is_virtual' => 'Is Virtual',
            'is_appoint' => 'Is Appoint',
            'is_presell' => 'Is Presell',
            'have_gift' => 'Have Gift',
            'isdelete' => 'Isdelete',
            'goods_weight' => 'Goods Weight',
            'is_second_kill' => 'Is Second Kill',
            'goods_hot' => 'Goods Hot',
        ];
    }

    public function getSuccessLog()
    {
        $logIds = json_decode($this->treasure_log_ids, true);
        $logIds or $logIds = [0];
        $logs = IntergralTreasureLog::find()->where(['in', 'id', $logIds])->all();
        return $logs;
    }
}
