<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jingxiaoshang".
 *
 * @property string $id
 * @property integer $member_id
 * @property string $name
 * @property string $phone
 * @property string $status
 * @property string $create_time
 */
class Jingxiaoshang extends \yii\db\ActiveRecord
{
    const STATUS_WAIT = 'WAIT';         // 等待审核
    const STATUS_APPLY = 'APPLY';       // 同意申请
    const STATUS_REFUSE = 'REFUSE';     // 拒绝申请

    const LEVEL_PROVINCE = 'PROVINCE';  // 省级代理
    const LEVEL_CITY = 'CITY';          // 市级代理
    const LEVEL_REGION = 'REGION';      // 区级代理

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jingxiaoshang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id'], 'required'],
            [['member_id', 'province', 'city', 'region'], 'integer'],
            [['create_time', 'good_ids'], 'safe'],
            [['name', 'level' ], 'string', 'max' => 255],
            [['phone', 'status'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'status' => 'Status',
            'create_time' => 'Create Time',
        ];
    }

    public function getMember()
    {
        $member = Member::findOne($this->member_id);
        return $member?: (new Member());
    }

    public function getLevelDic()
    {
        $dic = [
            self::LEVEL_PROVINCE => '省级代理',
            self::LEVEL_CITY => '市级代理',
            self::LEVEL_REGION => '区级代理',
        ];
        return $dic;
    }

    public function getLevelInfo()
    {
        $dic = $this->getLevelDic();
        $level = strlen($this->level) ? $this->level : '-';
        return isset($dic[$level]) ? $dic[$level] : '-';
    }
}
