<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "intergral_goods_sku".
 *
 * @property string $id
 * @property string $goods_id
 * @property string $attr
 * @property integer $num
 * @property string $price
 * @property integer $integral
 * @property string $goods_code
 * @property string $picimg
 */
class IntergralGoodsSku extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_goods_sku';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['goods_id', 'num', 'integral'], 'integer'],
            [['attr'], 'string'],
            [['price'], 'number'],
            [['goods_code', 'picimg'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'goods_id' => 'Goods ID',
            'attr' => 'Attr',
            'num' => 'Num',
            'price' => 'Price',
            'integral' => 'Integral',
            'goods_code' => 'Goods Code',
            'picimg' => 'Picimg',
        ];
    }

    public function getAttrInfo()
    {
        if (!strlen($this->attr)) {
            return '默认';
        }

        $skuIds = json_decode($this->attr,true);

        $goodSkus = IntergralGoodsAttr::find()->where([
            'and',
            ['in', 'id', $skuIds]
        ])->all();

        $info = '';
        foreach ($goodSkus AS $goodSku) {
            $info .= "{$goodSku->groupInfo->group_name}:{$goodSku->attr_name}, ";
        }

        return $info;
    }
}
