<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fitup_attr".
 *
 * @property string $id
 * @property string $type_name
 * @property integer $pid
 * @property string $children_name
 * @property integer $sort
 * @property integer $is_del
 * @property string $create_time
 */
class FitupAttr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fitup_attr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pid', 'sort', 'is_del'], 'integer'],
            [['create_time'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '装修类型'),
            'pid' => Yii::t('app', '父类'),
            'sort' => Yii::t('app', '排序'),
            'is_del' => Yii::t('app', '软删除'),
            'create_time' => Yii::t('app', '时间'),
        ];
    }
    public  function getParent($pid){
        $treeData= array();
        $tree = FitupAttr::find()->where(['pid' => $pid])->orderBy('sort asc')->all();
        foreach ($tree as $key => $value) {
            $treeData []= array(
                'id'=>$value->id,
                'name'=>$value->name,
                'tree'=>$this->getParent($value->id),
            );
        }
        return  $treeData;
    }
    public static function displayListsselt($pid = 0, $selectid = 1,$str="") {
        $result = self::getLists($pid);

        foreach ($result as $key => $value) {
            $selected = "";
            if ( $selectid == $value->id ) {
                $selected = 'selected';
            }

            $str .= '<option value='.$value->id.' '.$selected.'>'.$value->name.'</option>';


        }
        return $str .= '</optgroup></select>';
    }
    //从顶层逐级向下获取子类
    public static function getLists($pid = 0, &$lists = array(), $deep = 1) {
        $parentData = FitupAttr::find()->where(['pid'=>$pid])->orderBy('sort asc')->all();
        if (!empty($parentData)) {
            $fen = "|";
            for ($i=0;  $i< $deep; $i++) {
                $fen .= '---';
            }

            foreach ($parentData as $key => $value) {
                $value['name'] =$fen.$value['name'];
                $id = $value->id;
                $lists[] = $value;
                self::getLists($id, $lists, ++$deep); //进入子类之前深度+1
                --$deep; //从子类退出之后深度-1
            }
        }
        return $lists;
    }


}
