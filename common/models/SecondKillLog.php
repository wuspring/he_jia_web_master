<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "second_kill_log".
 *
 * @property string $id
 * @property string $key
 * @property integer $amount
 * @property integer $good_id
 * @property integer $sk_num
 */
class SecondKillLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'second_kill_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'good_id', 'sk_num'], 'integer'],
            [['sk_date'], 'safe'],
            [['key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sk_date' => '秒杀日期',
            'good_id' => '商品ID',
            'sk_num' => '秒杀时段',
            'key' => '秒杀时段秘钥',
            'amount' => '秒杀数量',
        ];
    }
}
