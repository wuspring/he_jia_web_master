<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "intergral_treasure_log".
 *
 * @property string $id
 * @property integer $member_id
 * @property integer $treasure_id
 * @property integer $integral
 * @property string $key
 * @property string $create_time
 * @property string $apply_group
 */
class IntergralTreasureLog extends \yii\db\ActiveRecord
{
    public $times;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_treasure_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'treasure_id'], 'integer'],
            [['treasure_id'], 'required'],
            [['create_time', 'apply_group'], 'safe'],
            [['integral'], 'number'],
            [['key'], 'string', 'max' => 255],
            [['treasure_id', 'key'], 'unique', 'targetAttribute' => ['treasure_id', 'key'], 'message' => 'The combination of Treasure ID and Key has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'treasure_id' => 'Treasure ID',
            'integral' => 'Integral',
            'key' => 'Key',
            'create_time' => 'Create Time',
        ];
    }

    public function getMember()
    {
        $member = Member::findOne($this->member_id);
        return $member ?: (new Member());
    }

    public function getCreateKey()
    {
        $key = (10000 + $this->id) .  date('md') . createRandKey(6);

        $record = self::findOne([
            'key' => $key
        ]);

        return $record ? $this->getCreateKey() : $key;
    }
}
