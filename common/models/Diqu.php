<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "diqu".
 *
 * @property integer $id
 * @property string $sheng
 * @property string $chengshi
 * @property string $diqu
 * @property string $sfgb
 * @property string $csgb
 * @property string $dqgb
 */
class Diqu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'diqu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sheng', 'chengshi', 'diqu', 'sfgb', 'csgb', 'dqgb'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sheng' => Yii::t('app', 'Sheng'),
            'chengshi' => Yii::t('app', 'Chengshi'),
            'diqu' => Yii::t('app', 'Diqu'),
            'sfgb' => Yii::t('app', 'Sfgb'),
            'csgb' => Yii::t('app', 'Csgb'),
            'dqgb' => Yii::t('app', 'Dqgb'),
        ];
    }
}
