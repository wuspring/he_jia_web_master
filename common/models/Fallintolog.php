<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fallintolog".
 *
 * @property string $id
 * @property string $orderId
 * @property string $memberId
 * @property string $goodsId
 * @property string $ fallIntoMoney
 * @property string $actualMoney
 * @property string $createTime
 * @property string $modifyTime
 */
class Fallintolog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fallintolog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderId', 'memberId', 'goodsId','state','type'], 'integer'],
            [['fallIntoMoney', 'actualMoney'], 'number'],
            [['createTime', 'modifyTime'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'orderId' => Yii::t('app', '订单编号'),
            'memberId' => Yii::t('app', '会员'),
            'goodsId' => Yii::t('app', '商品名称'),
            'fallIntoMoney' => Yii::t('app', '分成金额'),
            'actualMoney' => Yii::t('app', '实际分成'),
            'createTime' => Yii::t('app', '分成时间'),
            'modifyTime' => Yii::t('app', 'Modify Time'),
             'state' => Yii::t('app', 'State'),
             'type'=> Yii::t('app', 'State'),
        ];
    }
        public  function getMember()
    {
        return $this->hasOne(Member::className(), ['id' =>'memberId']);
    }
    public  function getGoods()
    {
        return $this->hasOne(Goods::className(), ['id' =>'goodsId']);
    }
}
