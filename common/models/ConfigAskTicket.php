<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "config_ask_ticket".
 *
 * @property string $id
 * @property string $provinces_id
 * @property string $type
 * @property string $data
 */
class ConfigAskTicket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config_ask_ticket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provinces_id'], 'integer'],
            [['data'], 'string'],
            [['type'], 'string', 'max' => 255],
            [['provinces_id', 'type'], 'unique', 'targetAttribute' => ['provinces_id', 'type'], 'message' => 'The combination of Provinces ID and Type has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'provinces_id' => 'Provinces ID',
            'type' => 'Type',
            'data' => 'Data',
        ];
    }
}
