<?php
namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\Html;
use yii\imagine\Image;
use yii;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile|Null file attribute
     */
    public $file;
    public $fileDoc;
    public $fileVideo;
    public $fileall;
    public $allfile;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
          return [
            [['file'], 'file', 'extensions' => 'jpg,png,jpeg,bmp', 'mimeTypes' => 'image/jpg,image/png,image/jpeg,image/bmp',],
            [['fileDoc'],'file','extensions' => 'doc,docx,pdf,txt,pptx,ppt',],
            [['fileVideo'],'file','extensions' => 'flv,swf,mov,3gp,mp4,f4v','maxSize' => 800*1024*1024],
              [['fileall'],'file'],
              [['allfile'],'file'],
        ];
    }

  
    public function randomFilename() {
        return $this->randomNum();
    }

    public function createImagePathWithExtension($extension,$destDir="/uploads/backend/user/"){
        $datePart = date("YmdHis");
        if (!file_exists($destDir)) {
            if(false === mkdir($destDir, 0755, true)){
               // throw new CHttpException(403, '没有图片目录操作权限 ');
            }
        }
        return $destDir . $datePart.$this->randomFilename().'.'.$extension;
    }
    public function createImagePathMD5($extension,$destDir="/uploads/backend/user/",$file){
        $datePart = $this->mymd5($file);
        if (!file_exists($destDir)) {
            if(false === mkdir($destDir, 0755, true)){
                // throw new CHttpException(403, '没有图片目录操作权限 ');
            }
        }
        return $destDir . $datePart.'.'.$extension;
    }
    function mymd5( $file ) {
        $fragment = 65536;

        $rh = fopen($file, 'rb');
        $size = filesize($file);

        $part1 = fread( $rh, $fragment );
        fseek($rh, $size-$fragment);
        $part2 = fread( $rh, $fragment);
        fclose($rh);

        return md5( $part1.$part2 );
    }
    /**
    * 生成随机数
    */
    public  function randomNum($length=10)
    {
        $str = '';
        for($i = 0; $i < $length; $i++) {
            $str .= mt_rand(0, 9);
        }

        return $str;
    }

    public  function createThumbnail($filename, $thumbnailFilename, $thumbnailWidth, $thumbnailHeight)
    {
    	
        $absoluteFilename = Yii::getAlias("@webroot") . $filename;
        $absoluteThumbnail = Yii::getAlias("@webroot") . $thumbnailFilename;
        $destPath = dirname($absoluteThumbnail);//echo $destPath;exit;
       
        // 创建图片子目录。
        if (!file_exists($destPath)) {
            if(false === mkdir($destPath, 0755, true)){
                throw new CHttpException(403, '没有图片目录操作权限 ');
            }       
        }
        Image::thumbnail( $absoluteFilename, $thumbnailWidth, $thumbnailHeight)->save(Yii::getAlias($absoluteThumbnail), ['quality' => 50]);
    }

    public function saveImage($uploadedFile, $filename){
        $destFile =Yii::getAlias("@webroot") . $filename;
        $destPath = dirname($destFile);//echo $destPath;exit;
        
        // 创建图片子目录。
        if (!file_exists($destPath)) {
            if(false === mkdir($destPath, 0755, true)){
                throw new CHttpException(403, '没有图片目录操作权限 ');
            }       
        }

      $uploadedFile->saveAs($destFile);
    }

    public function deleteImage($filePath)
    {
        if (!empty($filePath)) {
            $originFile = Yii::getAlias("@webroot") . $filePath;
            if (file_exists($originFile)) {
                unlink($originFile);
            }
        }
    }

}