<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property integer $id
 * @property string $icon
 * @property string $name
 * @property string $desc
 * @property string $provinces_id
 * @property string $create_time
 * @property integer $sort
 * @property string $pos
 */
class Service extends \yii\db\ActiveRecord
{
    const POS_HOME = 'HOME';
    const POS_TICKET_HOME = 'TICKET_HOME';
    const POS_TICKET = 'TICKET';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort'], 'integer'],
            [['create_time'], 'safe'],
            [['icon', 'name', 'desc', 'provinces_id','pos'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'icon' => '图标',
            'name' => '标题',
            'desc' => '副标题',
            'provinces_id' => 'Provinces ID',
            'create_time' => '添加时间',
            'sort' => '排序（从小到大）',
            'pos' => '位置',
        ];
    }

    public function getPosDesc(){
        $info = [
            'HOME'=>'首页',
            'TICKET'=>'索票页',
            'TICKET_HOME'=>'展会首页',
        ];
        return $info[$this->pos];
    }
}
