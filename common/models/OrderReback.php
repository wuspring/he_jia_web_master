<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_reback".
 *
 * @property string $orderid
 * @property string $pay_sn
 * @property string $reback_sn
 * @property integer $status
 * @property string $create_time
 */
class OrderReback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_reback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderid'], 'required'],
            [['orderid', 'status'], 'integer'],
            [['create_time'], 'safe'],
            [['pay_sn', 'reback_sn'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'orderid' => 'Orderid',
            'pay_sn' => 'Pay Sn',
            'reback_sn' => 'Reback Sn',
            'status' => 'Status',
            'create_time' => 'Create Time',
        ];
    }

    public function getNewRepaySn()
    {
        $orderPaySn = 're' . date('Ymdhis') . createRandKey(4);

        $paySn = OrderReback::findOne([
            'reback_sn' => $orderPaySn
        ]);

        return $paySn ? $this->getNewRepaySn() : $orderPaySn;
    }
}
