<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "channel".
 *
 * @property string $id
 * @property string $channel
 * @property string $create_time
 */
class Channel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'channel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['channel'], 'string'],
            [['create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'channel' => 'Channel',
            'create_time' => 'Create Time',
        ];
    }
}
