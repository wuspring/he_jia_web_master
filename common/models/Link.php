<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "link".
 *
 * @property string $id
 * @property string $name
 * @property string $url
 * @property string $imgval
 * @property integer $type
 * @property string $createTime
 * @property string $modifyTime
 */
class Link extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type','city'], 'integer'],
            [['createTime', 'modifyTime'], 'safe'],
            [['name', 'imgval','url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '链接名称'),
            'url' => Yii::t('app', 'Url'),
            'imgval' => Yii::t('app', '图片'),
            'type' => Yii::t('app', '链接类型'),
            'createTime' => Yii::t('app', '创建时间'),
            'modifyTime' => Yii::t('app', '修改时间'),
            'city' => Yii::t('app', '城市编号'),
        ];
    }
}
