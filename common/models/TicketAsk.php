<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ticket_ask".
 *
 * @property string $id
 * @property string $name
 * @property integer $provinces_id
 * @property string $ticket_id
 * @property string $ticket_amount
 * @property string $mobile
 * @property string $address
 * @property string $resource
 * @property string $shipping_code
 * @property integer $status
 * @property integer $member_id
 * @property string $create_time
 */
class TicketAsk extends \yii\db\ActiveRecord
{
    const STATUS_DEFAULT = '0';
    const STATUS_SEND = '1';
    const STATUS_REFUSE = '2';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_ask';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provinces_id', 'ticket_id', 'ticket_amount', 'status', 'member_id'], 'integer'],
            [['address', 'shipping_code'], 'string'],
            [['create_time'], 'safe'],
            [['name', 'mobile', 'resource'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'provinces_id' => 'Provinces ID',
            'ticket_id' => 'Ticket ID',
            'ticket_amount' => 'Ticket Amount',
            'mobile' => 'Mobile',
            'address' => 'Address',
            'resource' => 'Resource',
            'shipping_code' => 'shipping code',
            'status' => 'Status',
            'create_time' => 'Create Time',
        ];
    }

    public function getStatusDic()
    {
        return [
            self::STATUS_DEFAULT => '待送出',
            self::STATUS_SEND => '已送出',
            self::STATUS_REFUSE => '关闭'
        ];
    }

    public function getStatusInfo()
    {
        $dic = $this->getStatusDic();
        $status = (int)$this->status;
        return isset($dic[$status]) ? $dic[$status] : '-';
    }
    /**
     *  获取城市名字
     */
    public  function  getCityName(){

        $provinces=Provinces::findOne($this->provinces_id);

        return $provinces;
    }

    /**
     *  获取  索票票据表 信息
     */
    public  function  getTicket(){
        $ticket = Ticket::findOne($this->ticket_id);
        return $ticket ?: (new Ticket());
    }


}
