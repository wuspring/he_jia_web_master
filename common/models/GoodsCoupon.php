<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%goods_coupon}}".
 *
 * @property string $id
 * @property int $goods_id
 * @property string $money 金额
 * @property string $condition 满减条件
 * @property string $create_time 添加时间
 * @property int $ticket_id
 * @property string $type
 */
class GoodsCoupon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%goods_coupon}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['goods_id','is_del','user_id','num','valid_day', 'ticket_id'], 'integer'],
            [['money', 'condition'], 'number'],
            [['create_time', 'is_show'], 'safe'],
            [['describe', 'type'],'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'goods_id' => 'Goods ID',
            'money' => '金额',
            'condition' => '满减条件',
            'create_time' => '添加时间',
            'describe'=>'描述',
            'is_del'=>'软删除',
            'user_id'=>'商铺id',
            'num'=>'已领取数量',
            'valid_day'=>'优惠劵的有效天数'
        ];
    }

    public function getGoods()
    {
        $good = Goods::findOne($this->goods_id);

        return $good ? : (new Goods());
    }

    public  static  function addNum($id){

        $goodCoupon=GoodsCoupon::findOne($id);
        if ($goodCoupon){
            $goodCoupon->num+=1;
            $goodCoupon->save();
        }
    }
    public  function getUser(){
        $user=User::findOne($this->user_id);

        return $user?:(new User());
    }

//    public function getTicket()
//    {
//        GoodsCoupon
//    }

}
