<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "newsclassify".
 *
 * @property string $id
 * @property string $name
 * @property string $fid
 * @property integer $isShow
 * @property integer $isRdm
 * @property integer $sort
 * @property string $createTime
 * @property string $modifyTime
 */
class Newsclassify extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'newsclassify';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fid', 'isShow', 'isRdm', 'sort'], 'integer'],
            [['createTime', 'modifyTime'], 'safe'],
            [['name','icoImg'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', '名称'),
            'icoImg' => Yii::t('app', '图标'),
            'fid' => Yii::t('app', '父级'),
            'isShow' => Yii::t('app', '是否显示'),
            'isRdm' => Yii::t('app', '是否推荐'),
            'sort' => Yii::t('app', '排序'),
            'createTime' => Yii::t('app', 'Create Time'),
            'modifyTime' => Yii::t('app', 'Modify Time'),
        ];
    }
    public  function getParent($pid){
        $treeData= array();
        $tree = Newsclassify::find()->where(['fid' => $pid])->orderBy('sort asc')->all();
        foreach ($tree as $key => $value) {
             $treeData []= array(
                'id'=>$value->id,
                'name'=>$value->name,
                'tree'=>$this->getParent($value->id),
                );
        }
        return  $treeData;
    }
    public static function displayLists($pid = 0, $selectid = 1,$str="") {
        $result = self::getLists($pid);
       
        foreach ($result as $key => $value) {
            $selected = "";
            if ( $selectid == $value->id ) {
                $selected = 'selected';
            }
            $count = Newsclassify::find()->where(['fid' => $value->id])->count();
            if ($count == 0) {
                $str .= '<option value='.$value->id.' '.$selected.'>'.$value->name.'</option>';
            }else{
                 $str .= '<optgroup label='.$value->name.'></optgroup>';
            }
            
        }

        return $str .= '</optgroup></select>';
    }
    public static function displayListsselt($pid = 0, $selectid = 1,$str="") {
        $result = self::getLists($pid);
       
        foreach ($result as $key => $value) {
            $selected = "";
            if ( $selectid == $value->id ) {
                $selected = 'selected';
            }
       
            $str .= '<option value='.$value->id.' '.$selected.'>'.$value->name.'</option>';
         
            
        }
        return $str .= '</optgroup></select>';
    }
       //从顶层逐级向下获取子类
    public static function getLists($pid = 0, $lists = array(), $deep = 1) {

        $parentData = Newsclassify::find()->where(['fid'=>$pid])->orderBy('sort asc')->all();

        if (!empty($parentData)) {
            $fen = "|";
            for ($i=0;  $i< $deep; $i++) { 
                $fen .= '---';
            }

            foreach ($parentData as $key => $value) {
                $value['name'] =$fen.$value['name'];
                $id = $value->id;
                $lists[] = $value;
                self::getLists($id, $lists, ++$deep); //进入子类之前深度+1
                --$deep; //从子类退出之后深度-1
            }
       }
        return $lists;
    }

    /**
     * 获取资讯文章 4条
     */
    public function  getNewsContent($cityId='650100'){
        $newsArr=News::find()->andFilterWhere(['cid'=>$this->id])->andFilterWhere(['like','provinces_id',$cityId])->limit(4)->all();
        return $newsArr?:([]);
    }

    /**
     * 获取资讯文章 4条
     */
    public function  getChildrenNewsContent($cityId='650100'){
         $children=Newsclassify::findAll(['isShow'=>1,'fid'=>$this->id]);
          $childrenNewsClass=ArrayHelper::getColumn($children,'id');
         if (empty($childrenNewsClass)){
             $childrenNewsClass=[0];
         }
        $newsArr=News::find()->andFilterWhere(['in','cid',$childrenNewsClass])->andFilterWhere(['like','provinces_id',$cityId])->limit(4)->all();
        return $newsArr?:([]);
    }


    /**
     * 获取资讯文章 3条
     */
    public function  getNewsContentThree($cityId='650100'){
        $children=Newsclassify::findAll(['isShow'=>1,'fid'=>$this->id]);
        $childrenNewsClass=ArrayHelper::getColumn($children,'id');
        if (empty($childrenNewsClass)){
            $childrenNewsClass=[0];
        }

        $newsArr=News::find()->andFilterWhere(['in','cid',$childrenNewsClass])->andFilterWhere(['like','provinces_id',$cityId])->limit(3)->all();
        return $newsArr?:([]);
    }
   /**
    * 获取一级目录下面的 二级目录选择3个
    */
   public  function getLevelSecond(){
       $children=Newsclassify::find()->andFilterWhere(['isShow'=>1,'fid'=>$this->id])->limit(3)->all();

       return $children?:([]);
   }

   /**
    *  获取一级目录下面的二级目录id
    */
   public  function getLevelSecondId($fid){

       $children=Newsclassify::find()->andFilterWhere(['isShow'=>1,'fid'=>$fid])->all();
       $childrenArr=ArrayHelper::getColumn($children,'id');
       return $childrenArr?:([]);
   }


}
