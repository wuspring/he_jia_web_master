<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "goods_comment".
 *
 * @property string $id
 * @property string $goods_id
 * @property string $member_id
 * @property string $type
 * @property string $number
 * @property string $score
 * @property string $content
 * @property string $image
 * @property string $create_time
 */
class GoodsComment extends \yii\db\ActiveRecord
{
    /**
     * 好评
     */
    const TYPE_GOOD = 'GOOD';

    /**
     * 中评
     */
    const TYPE_MEDIUM = 'MEDIUM';

    /**
     * 差评
     */
    const TYPE_BAD = 'BAD';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['goods_id', 'member_id', 'type', 'number'], 'required'],
            [['goods_id', 'member_id'], 'integer'],
            [['score'], 'number'],
            [['content', 'image'], 'string'],
            [['create_time'], 'safe'],
            [['type', 'number'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'goods_id' => 'Goods ID',
            'member_id' => 'Member ID',
            'type' => 'Type',
            'number' => 'Number',
            'score' => 'Score',
            'content' => 'Content',
            'image' => 'Image',
            'create_time' => 'Create Time',
        ];
    }

    public function getMember()
    {
        $member = $this->hasOne(Member::className(), ['id' => 'member_id']);

        return $member ? : (new Member());
    }

    public function getGood()
    {
        $good = Goods::findOne($this->goods_id);

        return $good ? : (new Goods());
    }

    public function getCommentAmount($type='', $id=0)
    {
        $filter = [
            'goods_id' => $id ?:$this->goods_id
        ];

        strlen($type) and $filter['type'] = $type;
        return self::find()->where($filter)->count();
    }
}
