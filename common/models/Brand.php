<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "brand".
 *
 * @property string $id
 * @property string $brand_name
 * @property string $brand_initial
 * @property string $brand_pic
 * @property integer $brand_sort
 * @property integer $brand_recommend
 * @property integer $user_id
 * @property string $class_id
 * @property integer $show_type
 */
class Brand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brand';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_sort', 'brand_recommend', 'class_id', 'user_id', 'show_type'], 'integer'],
            [['brand_name', 'brand_pic'], 'string', 'max' => 100],
            [['brand_initial'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', '索引ID'),
            'brand_name' => Yii::t('app', '品牌名称'),
            'brand_initial' => Yii::t('app', '品牌首字母'),
            'brand_pic' => Yii::t('app', '品牌Logo'),
            'brand_sort' => Yii::t('app', '排序'),
            'brand_recommend' => Yii::t('app', '推荐'),
            'class_id' => Yii::t('app', '所属分类id'),
            'show_type' => Yii::t('app', '品牌展示类型'),
        ];
    }
}
