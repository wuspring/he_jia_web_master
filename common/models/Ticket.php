<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ticket".
 *
 * @property integer $id
 * @property integer $couple_nums
 * @property string $name
 * @property string $type
 * @property string $citys
 * @property string $ca_type
 * @property string $address
 * @property integer $status
 * @property string $create_time
 * @property string $end_date
 * @property string $ask_ticket_start_date
 * @property string $ask_ticket_end_date
  * @property string $title
 * @property string $keywords
 * @property string $description
 */
class Ticket extends \yii\db\ActiveRecord
{
    const STATUS_NORMAL = '1';
    const STATUS_END = '0';

    const TYPE_JIE_HUN = 'JIE_HUN';
    const TYPE_JIA_ZHUANG = 'JIA_ZHUANG';
    const TYPE_YUN_YING = 'YUN_YING';

    const CA_TYPE_CAI_GOU = 'CAI_GOU';    // 采购类索票
    const CA_TYPE_ZHAN_HUI = 'ZHAN_HUI';  // 展会类索票

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['id'], 'required'],
            [['id', 'status', 'couple_nums','virtual_number'], 'integer'],
            [['citys', 'address', 'keywords', 'description'], 'string'],
            [['order_price', 'lng', 'lat', 'ticket_price'], 'number'],
            [['create_time', 'cover_pic', 'arrive_method', 'arrive_method','open_date', 'ask_ticket_start_date', 'ask_ticket_end_date', 'end_date', 'notice_words'], 'safe'],
            [['order_price'], 'required'],
            [['name', 'type', 'ca_type', 'open_date', 'open_place'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '展会ID',
            'name' => 'Name',
            'type' => 'Type',
            'citys' => 'Citys',
            'status' => 'Status',
            'order_price' => '预定金额',
            'create_time' => 'Create Time',
            'ticket_price' => '票价',
            'virtual_number'=>'虚拟报名人数'
        ];
    }

    public function getStatusDic()
    {
        return [
            self::STATUS_NORMAL => '启用',
            self::STATUS_END => '关闭'
        ];
    }

    public function getStatusInfo()
    {
        $dic = $this->getStatusDic();

        return isset($dic[$this->status]) ? $dic[$this->status] : '-';
    }

    public function getTypeDic()
    {
        return [
            self::TYPE_JIE_HUN => '婚庆展',
            self::TYPE_JIA_ZHUANG => '家装展',
            self::TYPE_YUN_YING => '孕婴展'
        ];
    }

    public function getTypeInfo()
    {
        $dic = $this->getTypeDic();

        return isset($dic[$this->type]) ? $dic[$this->type] : '-';
    }

    /**
     * 根据 citys 获取城市名
     */
    public  function  getCityName(){

        $cityArr=json_decode($this->citys);
        $cityStr='';
        foreach ($cityArr as $k=>$v){
            $provinces=Provinces::findOne($v);
            $cityStr.=$provinces->cname.',';
        }

        return  $cityStr;
    }


    public  function getTicketInfo(){

        $ticketInfo = TicketInfo::findOne(['ticket_id'=>$this->id]);

        return $ticketInfo ? : (new TicketInfo());

    }

    /**
     * 查询对应的 ticket_infor
     */
    public  function getTicketInfor(){

        $ticketInfor=TicketInfo::find()->andWhere(['ticket_id'=>$this->id])->all();

        return $ticketInfor;

    }

    public function getAmount()
    {
        return TicketInfo::find()->where([
            'ticket_id' => $this->id
        ])->sum('ticket_amount');
    }
}
