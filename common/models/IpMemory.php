<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ip_memory".
 *
 * @property string $id
 * @property string $ip
 * @property string $unique_key
 * @property string $provinces_id
 */
class IpMemory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ip_memory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provinces_id'], 'integer'],
            [['ip', 'unique_key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'unique_key' => 'Unique Key',
            'provinces_id' => 'Provinces ID',
        ];
    }
}
