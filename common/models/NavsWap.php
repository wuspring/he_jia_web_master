<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "navs_wap".
 *
 * @property integer $id
 * @property string $provinces_id
 * @property string $type
 * @property string $name
 * @property string $icon
 * @property string $url
 * @property integer $status
 * @property integer $sort
 * @property string $create_time
 * @property string $modify_time
 * @property string $icon_selected
 */
class NavsWap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'navs_wap';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'sort'], 'integer'],
            [['create_time', 'modify_time'], 'safe'],
            [['provinces_id', 'type', 'name', 'icon','icon_selected', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provinces_id' => Yii::t('app', '城市ID'),
            'type' => Yii::t('app', '类型（首页或其它）'),
            'name' => Yii::t('app', '菜单名称'),
            'icon' => Yii::t('app', '菜单图标'),
            'url' => Yii::t('app', '菜单链接'),
            'status' => Yii::t('app', '状态'),
            'sort' => Yii::t('app', '排序'),
            'create_time' => Yii::t('app', '添加时间'),
            'modify_time' => Yii::t('app', '更新时间'),
            'icon_selected' => Yii::t('app', '图标选中'),
        ];
    }

    /**
     *  获取城市名字
     */
    public  function  getCityName(){
        $provinces=Provinces::findOne($this->provinces_id);
        return  empty($provinces)?'-':$provinces->cname;
    }
}
