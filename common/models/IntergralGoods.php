<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "intergral_goods".
 *
 * @property string $id
 * @property string $goods_name
 * @property string $goods_jingle
 * @property integer $user_id
 * @property string $gc_id
 * @property integer $use_attr
 * @property string $goods_integral
 * @property string $goods_marketprice
 * @property string $goods_serial
 * @property integer $goods_storage_alarm
 * @property integer $goods_click
 * @property integer $goods_salenum
 * @property integer $goods_collect
 * @property string $attr
 * @property string $goods_spec
 * @property string $goods_body
 * @property string $mobile_body
 * @property integer $goods_storage
 * @property string $goods_pic
 * @property string $goods_image
 * @property integer $goods_state
 * @property string $goods_addtime
 * @property string $goods_edittime
 * @property integer $goods_vat
 * @property double $evaluation_good_star
 * @property integer $evaluation_count
 * @property integer $is_virtual
 * @property integer $is_appoint
 * @property integer $is_presell
 * @property integer $have_gift
 * @property integer $isdelete
 * @property string $goods_weight
 * @property integer $is_second_kill
 * @property integer $goods_hot
 */
class IntergralGoods extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intergral_goods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gc_id', 'use_attr', 'goods_storage_alarm', 'goods_click', 'goods_salenum', 'goods_collect', 'goods_storage', 'goods_state', 'goods_vat', 'evaluation_count', 'is_virtual', 'is_appoint', 'is_presell', 'have_gift', 'isdelete', 'is_second_kill', 'goods_hot'], 'integer'],
            [['goods_integral', 'goods_marketprice', 'evaluation_good_star', 'goods_weight'], 'number'],
            [['attr', 'goods_spec', 'goods_body', 'mobile_body', 'goods_image', 'keywords', 'description'], 'string'],
            [['goods_addtime', 'goods_edittime'], 'safe'],
            [['goods_name', 'goods_jingle', 'goods_serial', 'goods_pic'], 'string', 'max' => 255],
            [['goods_image'],'default','value' =>json_encode(array())],
            [['goods_addtime','goods_edittime'],'default','value' =>date('Y-m-d H:i:s')],
            [['is_virtual', 'is_appoint','is_presell','have_gift','goods_vat','use_attr','isdelete'], 'default','value' => 0],
        ];


    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'goods_name' => 'Goods Name',
            'goods_jingle' => 'Goods Jingle',
            'user_id' => 'User ID',
            'gc_id' => 'Gc ID',
            'use_attr' => 'Use Attr',
            'goods_integral' => 'Goods Integral',
            'goods_marketprice' => 'Goods Marketprice',
            'goods_serial' => 'Goods Serial',
            'goods_storage_alarm' => 'Goods Storage Alarm',
            'goods_click' => 'Goods Click',
            'goods_salenum' => 'Goods Salenum',
            'goods_collect' => 'Goods Collect',
            'attr' => 'Attr',
            'goods_spec' => 'Goods Spec',
            'goods_body' => 'Goods Body',
            'mobile_body' => 'Mobile Body',
            'goods_storage' => 'Goods Storage',
            'goods_pic' => 'Goods Pic',
            'goods_image' => 'Goods Image',
            'goods_state' => 'Goods State',
            'goods_addtime' => 'Goods Addtime',
            'goods_edittime' => 'Goods Edittime',
            'goods_vat' => 'Goods Vat',
            'evaluation_good_star' => 'Evaluation Good Star',
            'evaluation_count' => 'Evaluation Count',
            'is_virtual' => 'Is Virtual',
            'is_appoint' => 'Is Appoint',
            'is_presell' => 'Is Presell',
            'have_gift' => 'Have Gift',
            'isdelete' => 'Isdelete',
            'goods_weight' => 'Goods Weight',
            'is_second_kill' => 'Is Second Kill',
            'goods_hot' => 'Goods Hot',
        ];
    }

    public function  getIntergralGoodsClass($pid=0,$list=array()){
        $calsslist= IntergralGoodsClass::find()->where(['fid'=>$pid])->orderBy(" sort desc")->all();
        foreach ($calsslist as $key=>$item){
            $count= IntergralGoodsClass::find()->where(['fid'=>$item->id])->count();
            if ($count>0){
                $list[$item->name]= $this->getIntergralGoodsClass($item->id);
            }else{
                $list[$item->id]='|-'.$item->name;
            }
        }
        return $list;
    }


    public function setThirdCode()
    {
        $good = IntergralGoods::find()->where([])->orderBy('id desc')->one();
//        $gift = Gift::find()->where([])->orderBy('id desc')->one();
        $goodNum = $good ? $good->id : 0;
//        $giftNum = $gift ? $gift->id : 0;

//        $this->third_code = $goodNum + $giftNum + 1000;
        $this->third_code = $goodNum + 1000;
    }

    public function getGclass(){
        $model=IntergralGoodsClass::findOne(['id'=>$this->gc_id]);
        if (empty($model)){
            $model=new GoodsClass();
        }
        return $model;
    }

    public  function getUser(){
        $model=User::findOne($this->user_id);

        return ($model)?:(new User());
    }



    public function getBranddata(){
        $model=Brand::findOne(['id'=>$this->brand_id]);
        if (empty($model)){
            $model=new Brand();
            $model->brand_name='';
            $model->brand_pic='';
            $model->show_type=1;
        }
        return $model;
    }

    /**
     * 商品是否被收藏
     * @param int $memberId
     * @return int
     */
    public function getIsCollect($memberId=0)
    {
        $collect = Collect::findOne([
            'member_id' => $memberId,
            'goods_id' => $this->id
        ]);

        return $collect ? 1 : 0;
    }

    public function getTypeInfo()
    {
        $dic = [
            self::TYPE_MONEY => '现金商品',
            self::TYPE_INTEGRAL => '积分商品',
            self::TYPE_FIX => '混合商品'
        ];

        return isset($dic[$this->type]) ?  $dic[$this->type] : '-';
    }

    public function getStore()
    {
        $storeInfo = [
            'user_id' => 0,
            'nickname' => '',
            'provinces_id' => '',
            'avatarTm' => ''
        ];

        $store = User::findOne($this->user_id);
        if ($store) {
            $storeInfo['user_id'] = $store->id;
            $storeInfo['nickname'] = $store->nickname;
            $storeInfo['provinces_id'] = $store->provinces_id;
            $storeInfo['avatarTm'] = $store->avatarTm;
        }

        return (object)$storeInfo;
    }

    public  function getGoodsCoupon(){

        $goodCoupon=GoodsCoupon::findAll(['goods_id'=>$this->id,'is_del'=>0]);
        return $goodCoupon? : [];
    }
}
