<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "member_ticket_store".
 *
 * @property string $id
 * @property string $member_id
 * @property integer $ticket_id
 * @property string $store_id
 */
class MemberTicketStore extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member_ticket_store';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'ticket_id', 'store_id'], 'integer'],
            [['member_id', 'ticket_id', 'store_id'], 'unique', 'targetAttribute' => ['member_id', 'ticket_id', 'store_id'], 'message' => 'The combination of Member ID, Ticket ID and Store ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'ticket_id' => 'Ticket ID',
            'store_id' => 'Store ID',
        ];
    }
}
