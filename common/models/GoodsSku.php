<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "goods_sku".
 *
 * @property string $id
 * @property string $goods_id
 * @property string $attr
 * @property integer $num
 * @property string $price
 * @property integer $integral
 * @property string $goods_code
 * @property string $picimg
 */
class GoodsSku extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_sku';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['goods_id', 'num', 'integral'], 'integer'],
            [['attr'], 'string'],
            [['price'], 'number'],
            [['goods_code', 'picimg'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'sku编码'),
            'goods_id' => Yii::t('app', '商品编号'),
            'attr' => Yii::t('app', '属性组合'),
            'num' => Yii::t('app', '库存'),
            'price' => Yii::t('app', '价格'),
            'integral' => Yii::t('app', '积分'),
            'goods_code' => Yii::t('app', '商品编码'),
            'picimg' => Yii::t('app', '图片'),
        ];
    }

    public function getAttrInfo()
    {
        if (!strlen($this->attr)) {
            return '默认';
        }

        $skuIds = json_decode($this->attr,true);

        $goodSkus = GoodsAttr::find()->where([
            'and',
            ['in', 'id', $skuIds]
        ])->all();

        $info = '';
        foreach ($goodSkus AS $goodSku) {
            $info .= "{$goodSku->groupInfo->group_name}:{$goodSku->attr_name}, ";
        }

        return $info;
    }
}
