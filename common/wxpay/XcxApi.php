<?php
namespace common\wxpay;

use Yii;
use yii\base\Model;
use app\models\Config;
// use app\models\WxPayConfig;
// use app\models\WxPayException;
// use app\models\WxPayDataBase;
// use app\Models\Config;

class XcxApi extends Model
{
    public $APPID;
    public $APPSECRET;
    public $MCHID;
    public $KEY;

    public function __construct()
    {

        $this->APPID = Yii::$app->params['weixin']['appid'];
        $this->APPSECRET = Yii::$app->params['weixin']['appsecret'];
        // $this->MCHID =$model->mchid;
        // $this->KEY =$model->key;
    }

    public function GetOpenid($code)
    {
        //通过code获得openid
        $openid = $this->GetOpenidFromMp($code);
        return $openid;
    }


    public function getUserInfo($openid = null)
    {

        //触发微信返回code码
        //$baseUrl = urlencode(Yii::$app->request->hostInfo.Yii::$app->request->getUrl());

        $url = $this->__CreateOauthUrlForToken();
        $tokendata = $this->GetObject($url);
        $token = $tokendata['access_token'];
        $wxuser = $this->GetUserFromMp($token, $openid);
        return $wxuser;
    }

    public function getQrodeB($uid,$page,$width=430)
    {
        $url = $this->__CreateOauthUrlForToken();

        $tokendata = $this->GetObject($url);
        $token = $tokendata['access_token'];
        $data = array(
            'scene' => $uid,
            'page' =>$page,
            'width'=>$width,
        );
        $url = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=' . $token;
        $wxuser = $this->GetQrcode(json_encode($data),$url);
        return $wxuser;
    }
    public function getQrodeC($page,$width=430)
    {
        $url = $this->__CreateOauthUrlForToken();

        $tokendata = $this->GetObject($url);
        $token = $tokendata['access_token'];
        $data = array(
            'path' =>$page,
            'width'=>$width,
        );
        $url = 'https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode?access_token=' . $token;
        $wxuser = $this->GetQrcode(json_encode($data,true),$url);
        return $wxuser;
    }
    public function setMenu($json)
    {
        $url = $this->__CreateOauthUrlForToken();
        $tokendata = $this->GetObject($url);
        $token = $tokendata['access_token'];
        $menuurl = " https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" . $token;
        $wxdata = $this->GetPost($menuurl, $json);

        return $wxdata;
    }

    public function setDelMenu()
    {
        $url = $this->__CreateOauthUrlForToken();
        $tokendata = $this->GetObject($url);
        $token = $tokendata['access_token'];
        $menuurl = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=" . $token;
        $wxdata = $this->GetObject($menuurl);
        return $wxdata;
    }

    public function getMenu()
    {
        $url = $this->__CreateOauthUrlForToken();
        $tokendata = $this->GetObject($url);
        $token = $tokendata['access_token'];
        $menuurl = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=" . $token;
        $wxdata = $this->GetObject($menuurl);
        return $wxdata;
    }

    public function GetJsApiParameters($UnifiedOrderResult)
    {
//	    print_r($UnifiedOrderResult);exit;
        if (!array_key_exists("appid", $UnifiedOrderResult) || !array_key_exists("prepay_id", $UnifiedOrderResult) || $UnifiedOrderResult['prepay_id'] == "") {

            echo '参数错误';yii::$app->end();
        }
        $jsapi = new WxPayJsApiPay();
        $jsapi->SetAppid($UnifiedOrderResult["appid"]);
        $time = time();
        $timeStamp = "$time";
        $jsapi->SetTimeStamp($timeStamp);
        $jsapi->SetNonceStr(WxPayApi::getNonceStr());
        $jsapi->SetPackage("prepay_id=" . $UnifiedOrderResult['prepay_id']);
        $jsapi->SetSignType("MD5");
        $jsapi->SetPaySign($jsapi->MakeSign());
        $parameters = json_encode($jsapi->GetValues());
        return $parameters;
    }

    public function GetOpenidFromMp($code)
    {
        $url = $this->__CreateOauthUrlForOpenid($code);
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //运行curl，结果以jason形式返回
        $res = curl_exec($ch);
        curl_close($ch);
        //取出openid
        $data = json_decode($res, true);
       // $openid = $data['openid'];

        return $data;
    }

    public function GetUserFromMp($token, $openid)
    {
        $url = $this->__CreateOauthUrlForUser($token, $openid);
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //运行curl，结果以jason形式返回
        $res = curl_exec($ch);
        curl_close($ch);
        //取出openid
        $data = json_decode($res, true);


        return $data;
    }

    public function GetObject($url)
    {

        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //运行curl，结果以jason形式返回
        $res = curl_exec($ch);
        curl_close($ch);
        //取出openid
        $data = json_decode($res, true);
        return $data;
    }

    public function GetQrcode($json,$url)
    {

        $ch = curl_init();
        //设置超时

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //运行curl，结果以jason形式返回
        $res = curl_exec($ch);
        print_r( $res );exit;
        curl_close($ch);
        //取出openid
        $data = json_decode($res, true);
        return $data;
    }

    public function setTemplateMessage($template_id, $openid, $mdata)
    {

        $message = array(
            'touser' => $openid,
            'template_id' => $template_id,
            'url' => $mdata['url'],
            'data' => $mdata['data'],
            'color' => $mdata['color']
        );
        return $this->sendMessage(json_encode($message));

    }

    public function addTemplateid($json)
    {
        $url = $this->__CreateOauthUrlForToken();
        $tokendata = $this->GetObject($url);
        $token = $tokendata['access_token'];
        $url = "https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token=" . $token;
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        //运行curl，结果以jason形式返回
        $res = curl_exec($ch);
        // print_r( $res );exit;
        curl_close($ch);
        //取出openid
        $data = json_decode($res, true);
        return $data;
    }

    public function sendMessage($json)
    {
        $url = $this->__CreateOauthUrlForToken();
        $tokendata = $this->GetObject($url);
        $token = $tokendata['access_token'];
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $token;
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        //运行curl，结果以jason形式返回
        $res = curl_exec($ch);
        // print_r( $res );exit;
        curl_close($ch);
        //取出openid
        $data = json_decode($res, true);
        return $data;
    }

    public function GetPost($url, $json)
    {

        $ch = curl_init();

        $url = $this->__CreateOauthUrlForToken();

        $tokendata = $this->GetObject($url);
        $token = $tokendata['access_token'];
        curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" . $token);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        if (curl_errno($ch)) {
            return curl_error($ch);
        }

        curl_close($ch);
        $data = json_decode($res, true);
        return $data;
        // //运行curl，结果以jason形式返回
        //       $res = curl_exec($ch);
        //       print_r($res);exit;
        // curl_close($ch);
        // //取出openid
        // $data = json_decode($res,true);
        // return $data;
    }

    private function ToUrlParams($urlObj)
    {
        $buff = "";
        foreach ($urlObj as $k => $v) {
            if ($k != "sign") {
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }

    private function __CreateOauthUrlForCode($redirectUrl)
    {
        $urlObj["appid"] = $this->APPID;
        $urlObj["redirect_uri"] = "$redirectUrl";
        $urlObj["response_type"] = "code";
        $urlObj["scope"] = "snsapi_base";
        $urlObj["state"] = "STATE" . "#wechat_redirect";
        $bizString = $this->ToUrlParams($urlObj);
        return "https://open.weixin.qq.com/connect/oauth2/authorize?" . $bizString;

    }

    private function __CreateOauthUrlForToken()
    {
        $urlObj["grant_type"] = "client_credential";
        $urlObj["appid"] = $this->APPID;
        $urlObj["secret"] = $this->APPSECRET;
        $bizString = $this->ToUrlParams($urlObj);
        return "https://api.weixin.qq.com/cgi-bin/token?" . $bizString;
    }

    private function __CreateOauthUrlForUser($token, $openid)
    {
        $urlObj["openid"] = $openid;
        $urlObj["access_token"] = $token;
        $urlObj["lang"] = "zh_CN";
        $bizString = $this->ToUrlParams($urlObj);
        return "https://api.weixin.qq.com/cgi-bin/user/info?" . $bizString;
    }

    private function __CreateOauthUrlForOpenid($code)
    {
        $urlObj["appid"] = $this->APPID;
        $urlObj["secret"] = $this->APPSECRET;
        $urlObj["js_code"] = $code;
        $urlObj["grant_type"] = "authorization_code";
        $bizString = $this->ToUrlParams($urlObj);
        return "https://api.weixin.qq.com/sns/jscode2session?" . $bizString;
    }


    private function __CreateOauthUrlForTicket($token,$code)
    {
        $urlObj["appid"] = $this->APPID;
        $urlObj["secret"] = $this->APPSECRET;
        $urlObj["code"] = $code;
        $urlObj["grant_type"] = "authorization_code";
        $bizString = $this->ToUrlParams($urlObj);
        return "https://api.weixin.qq.com/sns/oauth2/access_token?" . $bizString;
    }
}