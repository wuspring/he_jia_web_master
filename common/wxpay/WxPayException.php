<?php
/**
 * Created by PhpStorm.
 * User: 何孝林
 * Date: 2018/11/2
 * Time: 16:34
 */

namespace common\wxpay;


use Exception;

class WxPayException extends Exception {
    public function errorMessage()
    {
        return $this->getMessage();
    }
}