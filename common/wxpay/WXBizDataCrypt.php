<?php
/**
 * Created by PhpStorm.
 * User: 何孝林
 * Date: 2018/7/10
 * Time: 9:45
 */

namespace common\wxpay;


class WXBizDataCrypt
{
    private $appid;
    private $sessionKey;

    /**
     * 构造函数
     * @param $sessionKey string 用户在小程序登录后获取的会话密钥
     * @param $appid string 小程序的appid
     */
    public function __construct( $appid, $sessionKey)
    {
        $this->sessionKey = $sessionKey;
        $this->appid = $appid;
    }


    /**
     * 检验数据的真实性，并且获取解密后的明文.
     * @param $encryptedData string 加密的用户数据
     * @param $iv string 与用户数据一同返回的初始向量
     * @param $data string 解密后的原文
     *
     * @return int 成功0，失败返回对应的错误码
     */
    public function decryptData( $encryptedData, $iv)
    {
        $json=array('code'=>0,'data'=>array());
        if (strlen($this->sessionKey) != 24) {
            $json['code']=ErrorCode::$IllegalAesKey;
            return $json;
        }
        $aesKey=base64_decode($this->sessionKey);


        if (strlen($iv) != 24) {
            $json['code']=ErrorCode::$IllegalIv;
            return $json;
		}
        $aesIV=base64_decode($iv);

        $aesCipher=base64_decode($encryptedData);

        $result=openssl_decrypt( $aesCipher, "AES-128-CBC", $aesKey, 1, $aesIV);

        $dataObj=json_decode( $result );
        if( $dataObj  == NULL )
        {
            $json['code']=ErrorCode::$IllegalBuffer;
            return $json;

		}
        if( $dataObj->watermark->appid != $this->appid )
        {
            $json['code']=ErrorCode::$IllegalBuffer;
            return $json;

		}
        $json['data'] = $result;
        $json['code']=ErrorCode::$OK;
        return $json;
	}
}