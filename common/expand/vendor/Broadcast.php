<?php
namespace DL\vendor;
/**
 * 广播
 * Class Broadcast
 * @author  daorli
 */
class Broadcast
{
    public $config;
    public $requestUrl;

    public function __construct()
    {
        $text = \app\models\Config::find()->where(['cKey'=>'weixin'])->one();
        $this->requestUrl = 'https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=';

        $this->config = json_decode($text->cValue);
    }

    public function sendOrder($orderId)
    {
        try {
            $orderInfo = \app\models\Order::find()->where(['id' => $orderId])->one();

            if (empty($orderInfo)) {
                throw new \Exception('get order info failed', 500);
            }

            $groupOrder = \app\models\GroupOrder::find()->where(['id' => $orderInfo['group_id']])->one();

            $member = \app\models\Member::find()->where(["id" => $orderInfo->user_id])->one();
            if (empty($member)) {
                throw new \Exception("user is not exist", 500);
            }

            $data = [
                'touser' => $member->wxopenid,
                'template_id' => BROADCAST_BUY_SUCCESS_ID,  // 购买成功通知模板
                'form_id' => $this->getFormId($member->id),
                'data' => $this->_setData([
                    'keyword1' => $orderInfo->number,
                    'keyword2' => $groupOrder->title,
                ])
            ];

            $accessToken = $this->getToken();
            $result = $this->_request($this->requestUrl . $accessToken, $data, 1);

            return $result['errcode'] == '0';
        } catch (\Exception $e) {
            return false;
        }

    }

    public function orderStatusChange($orderId, $information="")
    {
        try {
            $orderInfo = \app\models\Order::find()->where(['id' => $orderId])->one();

            if (empty($orderInfo)) {
                throw new \Exception('get order info failed', 500);
            }

            $groupOrder = \app\models\GroupOrder::find()->where(['id' => $orderInfo['group_id']])->one();

            $member = \app\models\Member::find()->where(["id" => $orderInfo->user_id])->one();
            if (empty($member)) {
                throw new \Exception("user is not exist", 500);
            }

            $data = [
                'touser' => $member->wxopenid,
                'template_id' => BROADCAST_STATUS_CHANGE_ID,    // 订单状态通知模板
                'form_id' => $this->getFormId($member->id),
                'data' => $this->_setData([
                    'keyword1' => $groupOrder->title,
                    'keyword2' => $orderInfo->number,
                    'keyword3' => $orderInfo->create_time,
                    'keyword4' => $orderInfo->send_number,
                    'keyword5' => $orderInfo->send_time,
                    'keyword6' => $information
                ]),
                'page' => '/pages/transform/transform?number=' . $orderInfo->number
            ];

            $accessToken = $this->getToken();
            $result = $this->_request($this->requestUrl . $accessToken, $data, 1);

            return $result['errcode'] == '0';
        } catch (\Exception $e) {
            return false;
        }
    }

    protected function getFormId($userId)
    {
        $time = date("Y-m-d H:i:s", strtotime('-7 days'));
        $formIdInfo = \app\models\FormIdRecord::find()->where([
            'user_id' => $userId
        ])->andWhere(['>=', 'create_time', $time])->one();

        if (!$formIdInfo) {
            throw new \Exception("form id is null");
        }

        $id = $formIdInfo->form_id and $formIdInfo->delete();

        return $id;
    }

    protected function getToken()
    {
        $request = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";

        $result = $this->_request(sprintf($request, $this->config->appid, $this->config->appsecret));

        return $result['access_token'];
    }

    private function _request($url, $data=[], $isPost=0)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($ch,CURLOPT_HEADER,0);

        if ($isPost) {
            $data = json_encode($data);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            ));
            curl_setopt($ch, CURLOPT_POST, $isPost);//post提交方式
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        $data = curl_exec($ch);//运行curl
        curl_close($ch);

        return json_decode($data, true);
    }

    private function _setData($data)
    {
        $result = [];

        foreach ($data AS $key => $d) {
            $result[$key] = ['value' => $d];
        }

        return $result;
    }
}