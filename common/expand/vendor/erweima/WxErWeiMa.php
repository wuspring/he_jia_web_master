<?php
namespace DL\vendor;
/**
 * Class WxErWeiMa
 *
 * @package DL\vendor
 * @author  daorli
 */
class WxErWeiMa
{
    protected $config;

    public function __construct($appId, $appSecret)
    {
        $this->appId = $appId;
        $this->appSecret = $appSecret;
    }

    public function create($file, $redirectPath, $query=[], $isForever=true)
    {
        if ($isForever) {

            $request = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=%s';    // 永久二维码
            $field = [
                'scene' => http_build_query($query),
                'page' => $redirectPath
            ];
        } else {
            $request = 'https://api.weixin.qq.com/wxa/getwxacode?access_token=%s';    // 临时二维码
            $field = [
                'path' => $redirectPath
            ];
        }

        $color = [
            'r' => '0',
            'g' => '0',
            'b' => '0'
        ];
        $baseField = [
            'width' => 430,             // 二维码的宽度,
            'auto_color' => false,      // 自动配置线条颜色
            'line_color' => $color,     //使用 rgb 设置颜色
            'is_hyaline' => false       // 是否需要透明底色
        ];
        $accessToken = $this->getAccessToken();
        $result = $this->_request(sprintf($request, $accessToken), array_merge($field, $baseField), true, false);

        return file_put_contents($file, $result);
    }

    protected function getAccessToken()
    {
        $request = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";

        $result = $this->_request(sprintf($request, $this->appId, $this->appSecret));

        return $result['access_token'];
    }

    private function _request($url, $data=[], $isPost=0, $format=true)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($ch,CURLOPT_HEADER,0);

        if ($isPost) {
            $data = json_encode($data);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            ));
            curl_setopt($ch, CURLOPT_POST, $isPost);//post提交方式
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        $data = curl_exec($ch);
        curl_close($ch);

        return $format ? json_decode($data, true) : $data;
    }
}