<?php
namespace DL\vendor;

include EXPAND_PATH . '/phpqrcode/phpqrcode.php';
/**
 * Class ErWeiMa
 *
 * @package DL\vendor
 * @author  daorli
 */
class ErWeiMa
{
    /**
     * 容错率 有被覆盖的区域还能识别de
     * @var int
     * L（QR_ECLEVEL_L，7%）M（QR_ECLEVEL_M，15%）
     * Q（QR_ECLEVEL_Q，25%）H（QR_ECLEVEL_H，30%）
     */
    public $level;

    // @var int 生成图片大小
    public $size;

    // @var int 二维码周围边框空白区域间距值
    public $margin;

    // @var bool 保存二维码并显示
    public $saveAndPrint;

    public function __construct($level=QR_ECLEVEL_L, $size=3, $margin=4, $saveandprint=false)
    {
        $this->level = $level;
        $this->size = $size;
        $this->margin = $margin;
        $this->saveandprint = $saveandprint;
    }

    public static function init($level=QR_ECLEVEL_L, $size=3, $margin=4, $saveandprint=false)
    {
        return new self($level, $size, $margin, $saveandprint);
    }

    public function draw($text, $outfile=false)
    {
        if ($outfile) {
            $path = pathinfo($outfile);
            if (isset($path['dirname'])) {
                is_dir($path['dirname']) or mkdir($path['dirname'], 777, true);
            }
        }
        $enc = \QRencode::factory($this->level, $this->size, $this->margin);
        return $enc->encodePNG($text, $outfile, $this->saveandprint);
    }
}