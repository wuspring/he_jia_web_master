<?php
namespace DL;

/**
 * Class WechatPay
 *
 * @package DL
 * @author  daorli
 */
class WechatPay
{
    protected $appid;
    protected $mch_id;
    protected $key;
    protected $openid;
    protected $out_trade_no;
    protected $body;
    protected $total_fee;
    protected $notrify_url;
    protected $out_refund_no;

    function __construct($appid, $mch_id, $key, $notrify_url)
    {
        $this->appid = $appid;
        $this->mch_id = "{$mch_id}";
        $this->key = $key;
        $this->notrify_url = $notrify_url;
    }

    public function pay($openid, $out_trade_no, $body, $total_fee)
    {
        $this->openid = $openid;
        $this->out_trade_no = $out_trade_no;
        $this->body = $body;
        $this->total_fee = $total_fee;

        //统一下单接口
        return $this->weixinapp();
    }

    /**
     * @return string
     * ps : 流程处理完后 需输出 die('SUCCESS') 通知微信确认；
     */
    public static function notify()
    {
        $xml = file_get_contents("php://input");
//        $log = json_decode(
//            json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)),
//            true
//        );

        $outTradeNo = null;
        if (strlen($xml)) {
            $log = static::xmlToArray($xml);
            $outTradeNo = $log['out_trade_no'];
        }

        // 返回外部订单号
        return $outTradeNo;
    }

    //统一下单接口
    private function unifiedorder()
    {
        $url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
        $parameters = array(
            'appid' => $this->appid,                //小程序ID
            'mch_id' => $this->mch_id,              //商户号
            'nonce_str' => $this->createNoncestr(), //随机字符串
            'body' => $this->body,                  //商品描述
            'out_trade_no' => $this->out_trade_no,
            'total_fee' => $this->total_fee,        //总金额 单位 分
            'spbill_create_ip' => '192.168.0.161',  //终端IP
            'notify_url' => $this->notrify_url,     //通知地址  确保外网能正常访问
            'openid' => $this->openid,              //用户id
            'trade_type' => 'JSAPI'                 //交易类型
        );
        //统一下单签名
        $parameters['sign'] = $this->getSign($parameters);
        $xmlData = $this->arrayToXml($parameters);
        $return = self::xmlToArray($this->postXmlCurl($xmlData, $url, 60));

        return $return;
    }

    private static function postXmlCurl($xml, $url, $second = 30)
    {
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); //严格校验
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
        set_time_limit(0);

        //运行curl
        $data = curl_exec($ch);

        //返回结果
        if ($data) {
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            throw new \Exception("curl出错，错误码:$error");
        }
    }

    protected function arrayToXml($arr)
    {
        $xml = "<root>";
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                $xml .= "<" . $key . ">" . $this->arrayToXml($val) . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            }
        }
        $xml .= "</root>";
        return $xml;
    }


    protected static function xmlToArray($xml)
    {
        libxml_disable_entity_loader(true);
        $xmlstring = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        $val = json_decode(json_encode($xmlstring), true);
        return $val;
    }

    private function weixinapp()
    {
        $unifiedorder = $this->unifiedorder();

        $parameters = array(
            'appId' => $this->appid,                                //小程序ID
            'timeStamp' => (string)time(),                          //时间戳
            'nonceStr' => $this->createNoncestr(),                  //随机串
            'package' => 'prepay_id=' . $unifiedorder['prepay_id'], //数据包
            'signType' => 'MD5'                                     //签名方式
        );

        $parameters['paySign'] = $this->getSign($parameters);
        return $parameters;
    }

    protected function createNoncestr($length = 32)
    {
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    protected function getSign($Obj)
    {
        foreach ($Obj as $k => $v) {
            $Parameters[$k] = $v;
        }
        //签名步骤一：按字典序排序参数
        ksort($Parameters);
        $String = $this->formatBizQueryParaMap($Parameters, false);
        //签名步骤二：在string后加入KEY
        $String = $String . "&key=" . $this->key;
        //签名步骤三：MD5加密
        $String = md5($String);
        //签名步骤四：所有字符转为大写
        $result_ = strtoupper($String);
        return $result_;
    }

    private function formatBizQueryParaMap($paraMap, $urlencode)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if ($urlencode) {
                $v = urlencode($v);
            }
            $buff .= $k . "=" . $v . "&";
        }

        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }
}