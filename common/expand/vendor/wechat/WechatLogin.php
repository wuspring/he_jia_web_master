<?php
namespace DL\vendor;

/**
 * Class WechatLogin
 *
 * @package DL\classes
 * @author  daorli
 */
class WechatLogin
{
    public $appId;
    public $secret;

    public function __construct()
    {
        $this->appId = WECHAT_APP_ID;
        $this->secret = WECHAT_APP_SECRET;
    }

    public static function init()
    {
        return new self();
    }

    public function login()
    {
        $redirectUri = urlencode ( $_SERVER['SERVER_NAME'] . WECHAT_REDIRECT_TO );
        $url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$this->appId}&redirect_uri={$redirectUri}&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect";

        header("Location:{$url}");
        die;
    }

    protected function user()
    {

    }
}