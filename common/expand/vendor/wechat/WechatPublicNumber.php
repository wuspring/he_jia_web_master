<?php
namespace DL\vendor;

/**
 * Class WechatPublicNumber
 *
 * @package DL\classes
 * @author  daorli
 */
class WechatPublicNumber
{
    public $appId;
    public $secret;

    public static $var = [];

    public function __construct()
    {
        $this->appId = WECHAT_APP_ID;
        $this->secret = WECHAT_APP_SECRET;
    }

    /**
     * @return WechatPublicNumber
     */
    public static function init()
    {
        return new self();
    }

    /**
     * get user info
     *
     * @return array
     */
    public function getUserInfo()
    {
        try {
            if (isset($_GET['code'])) {
                return $this->getWechatUserInfo($_GET['code']);
            } else {
                $this->getCode();
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    /*
     * get access code
     */
    protected function getCode()
    {
        $redirectUri = urlencode ( getServerHost() . WECHAT_REDIRECT_TO );
        $url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$this->appId}&redirect_uri={$redirectUri}&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect";

        header("Location:{$url}");
        die;
    }

    /**
     * get wechat user info
     *
     * @param string $code code
     *
     * @return mixed
     */
    protected function getWechatUserInfo($code)
    {
        $oauth2Url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={$this->appId}&secret={$this->secret}&code={$code}&grant_type=authorization_code";
        $oauth2 = $this->_request($oauth2Url);

        static::$var['code'] = $code;
        static::$var['openid'] = $oauth2['openid'];
        static::$var['access_token'] = $oauth2['access_token'];

        $openid = $oauth2['openid'];
        $accessToken = $oauth2['access_token'];

        $getUserInfoUrl = "https://api.weixin.qq.com/sns/userinfo?access_token={$accessToken}&openid={$openid}";

        $userinfo = $this->_request($getUserInfoUrl);

        return $userinfo;
    }

    private function _request($url, $data=[], $second = 30)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
        set_time_limit(0);

        $data = curl_exec($ch);

        if (!$data) {
            $error = curl_errno($ch);
            throw new \Exception("curl出错，错误码:$error");
        }

        curl_close($ch);
        return json_decode($data, true);
    }
}