<?php
namespace DL;

class WechatRefund extends WechatPay
{
    private $_sslcert_path;    //证书路径
    private $_sslkey_path;     //证书路径
    private $_url = 'https://api.mch.weixin.qq.com/secapi/pay/refund';

    protected $refund_fee;

    public function __construct($appid, $mch_id, $key, $notrify_url){
        parent::__construct($appid, $mch_id, $key, $notrify_url);

        $this->_sslcert_path = ROOT_PATH . '/uploads/pay/wx/apiclient_cert.pem';
        $this->_sslkey_path = ROOT_PATH . '/uploads/pay/wx/apiclient_key.pem';
    }

    /**
     * 微信支付退款
     *
     * @param string $old_out_trade_no   退款外部订单号
     * @param string $new_out_trade_no   新生成的退款记录外部订单
     * @param float  $totalFee           退款订单全部金额
     * @param float  $refundFee          真实退款金额
     *
     * @return mixed
     */
    public function refund($old_out_trade_no, $new_out_trade_no, $totalFee, $refundFee)
    {
        $this->total_fee = $totalFee;
        $this->refund_fee =$refundFee;
        $this->out_refund_no = $new_out_trade_no;
        $this->out_trade_no = $old_out_trade_no;

        $result = $this->wxrefundapi();

        if (($result['return_code']=='SUCCESS') && ($result['result_code']=='SUCCESS')) {
            return true;
            //退款成功
        } else if(($result['return_code']=='FAIL') || ($result['result_code']=='FAIL')) {
            //退款失败
            //原因//            return (empty($result['err_code_des'])?$result['return_msg']:$result['err_code_des']);
            return false;
        } else {
            return false;
        }

//        return $result;
    }

    public function d_refund($old_out_trade_no, $new_out_trade_no, $totalFee, $refundFee)
    {
        $this->total_fee = $totalFee;
        $this->refund_fee =$refundFee;
        $this->out_refund_no = $new_out_trade_no;
        $this->out_trade_no = $old_out_trade_no;

        $result = $this->wxrefundapi();

        if (($result['return_code']=='SUCCESS') && ($result['result_code']=='SUCCESS')) {
            return true;
            //退款成功
        } else if(($result['return_code']=='FAIL') || ($result['result_code']=='FAIL')) {
            //退款失败
            //原因
//            return (empty($result['err_code_des'])?$result['return_msg']:$result['err_code_des']);
            return $result;
            return false;
        } else {
            return $result;
            return false;
        }

//        return $result;
    }

    private function wxrefundapi()
    {
        //通过微信api进行退款流程
        $parma = array(
            'appid'=> $this->appid,
            'mch_id'=> $this->mch_id,
            'nonce_str'=> $this->createNoncestr(),
            'out_refund_no'=> $this->out_refund_no,
            'out_trade_no'=> $this->out_trade_no,
            'total_fee'=> $this->total_fee,
            'refund_fee'=> $this->refund_fee,
        );

        $parma['sign'] = $this->getSign($parma);
        $xmldata = $this->arrayToXml($parma);
        $xmlresult = $this->postXmlSSLCurl($xmldata);
        $result = $this->xmlToArray($xmlresult);
        return $result;
    }

    //需要使用证书的请求
    protected function postXmlSSLCurl($xml,$second=30)
    {
        $ch = curl_init();
        //超时时间
        curl_setopt($ch,CURLOPT_TIMEOUT,$second);
        //这里设置代理，如果有的话
        //curl_setopt($ch,CURLOPT_PROXY, '8.8.8.8');
        //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
        curl_setopt($ch,CURLOPT_URL, $this->_url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,FALSE);

        //设置header
        curl_setopt($ch,CURLOPT_HEADER,FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);

        //设置证书
        //使用证书：cert 与 key 分别属于两个.pem文件
        //默认格式为PEM，可以注释
        curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
        curl_setopt($ch,CURLOPT_SSLCERT, $this->_sslcert_path);
        //默认格式为PEM，可以注释
        curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
        curl_setopt($ch,CURLOPT_SSLKEY, $this->_sslkey_path);
        //post提交方式
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$xml);
        $data = curl_exec($ch);
        //返回结果

        if ($data) {
            curl_close($ch);
            return $data;
        } else {
            $error = curl_error($ch);
            echo "curl出错，错误码:$error"."<br>";
            curl_close($ch);
            return false;
        }

    }
}