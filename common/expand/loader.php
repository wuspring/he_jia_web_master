<?php
/**
 * author daorli
 * time 2017.04.13 23:04:27
 */
namespace DL;

/**
 * Class Load
 *
 * @package DL
 * @author  daorli
 */
class Load
{
    public static $var = [];
    public static $disLoadDir = [];

    /**
     * load static params
     *
     * @param string $var
     *
     * @return array
     */
    public static function vars($var='')
    {
        $vars = static::$var;
        if (DL_DEBUG) {
            $mocks = is_file(RUNTIME_PATH . '/mocks.php') ? require(RUNTIME_PATH . '/mocks.php') : [];
            $vars = array_merge($vars, $mocks);
        }

        return strlen($var) ? $vars[$var] : $vars;
    }

    /**
     * require dirs
     *
     * @param array $dirs
     * @param string $expName
     */
    public static function dirs($dirs=[], $expName='\.php')
    {
        foreach ($dirs AS $path) {
            static::path($path);
        }

    }

    /**
     * require all file in dir
     *
     * @param string $dir dir
     * @param string $expName
     */
    public static function path($path, $expName='\.php')
    {
        if (is_dir($path) && !in_array($path, self::$disLoadDir)) {
            if ($handle = opendir($path)) {
                while ( ($fileInfo = readdir($handle)) !== false ) {
                    if ($fileInfo == '.' || $fileInfo == '..') {
                        continue;
                    }

                    $fileRoad = rtrim($path, '/') . '/' . $fileInfo;

                    if (is_dir($fileRoad)) {
                        static::path($fileRoad);
                        continue;
                    }

                    if (file_exists($fileRoad) && preg_match('/' . $expName . '/', $fileRoad)) {
                        require_once($fileRoad);
                    }
                }
                closedir($handle);
            }
        }
    }


}
