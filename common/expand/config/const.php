<?php
/**
 * const
 *
 * @author  daorli
 */
/**
 * debug
 */
defined('DL_DEBUG') or define('DL_DEBUG', false);

/**
 * define path
 */
defined('ADMIN_URL') or define('ADMIN_URL', getServerHost());
defined('MODULE_NAME') or define('MODULE_NAME', 'index');

defined('ROOT_PATH') or define('ROOT_PATH', realpath(__DIR__ . '/../..'));
defined('EXPAND_PATH') or define('EXPAND_PATH', ROOT_PATH . '/expand');
defined('RUNTIME_PATH') or define('RUNTIME_PATH', ROOT_PATH . '/runtimes/' . MODULE_NAME);
defined('PUBLIC_PATH') or define('PUBLIC_PATH', ROOT_PATH . '/public');

/**
 * smarty config
 */
defined('SMARTY_DEBUG') or define('SMARTY_DEBUG', false);
defined('SMARTY_LEFT_DELIMITER') or define('SMARTY_LEFT_DELIMITER', '{{');
defined('SMARTY_RIGHT_DELIMITER') or define('SMARTY_RIGHT_DELIMITER', '}}');
defined('SMARTY_IS_CACHE') or define('SMARTY_IS_CACHE', false);
defined('SMARTY_CACHE_DIR') or define('SMARTY_CACHE_DIR', EXPAND_PATH . '/runtime/smarty');
defined('SMARTY_STATIC_PATH') or define('SMARTY_STATIC_PATH', ROOT_PATH . '/static');
defined('SMARTY_TPL_PATH') or define('SMARTY_TPL_PATH', ROOT_PATH . '/frontend/views');

defined('PAGE_ROLL') or define('PAGE_ROLL', '5');
defined('PAGE_PARAM') or define('PAGE_PARAM', 'p');
defined('PAGE_URL') or define('PAGE_URL', '/index.php');

/**
 * pay status
 */
defined('PAY_TRUE') or define('PAY_TRUE', 1);
defined('PAY_FALSE') or define('PAY_FALSE', 0);


/**
 * wechat info
 */
defined('WECHAT_APP_ID') or define('WECHAT_APP_ID', '');
defined('WECHAT_APP_SECRET') or define('WECHAT_APP_SECRET', '');
defined('WECHAT_APP_MCH_ID') or define('WECHAT_APP_MCH_ID', '');
defined('WECHAT_APP_MCH_SECRET') or define('WECHAT_APP_MCH_SECRET', '');
defined('WECHAT_APP_PAY_NOTIFY_URL') or define('WECHAT_APP_PAY_NOTIFY_URL', getServerHost() . '/notify.php');

defined('WECHAT_REDIRECT_TO') or define('WECHAT_REDIRECT_TO', '/index.php?r=index/index');

/**
 * 邮费
 */
defined('YOU_FEI') or define('YOU_FEI', '0.01');

/**
 * 分销设置
 */
defined('IS_FENXIAO') or define('IS_FENXIAO', true);

/**
 * 展示方式 API / 页面
 */
defined('RENDER_PAGE') or define('RENDER_PAGE', false);

/**
 * web set
 */
defined('WEB_TITLE') or define('WEB_TITLE', '');

defined('MAP_KEY') or define('MAP_KEY', 'YLQBZ-4M5LX-PBP44-TG3PB-XAD35-RYF2Q');
