<?php
namespace DL\service;

use common\models\IntergralGoodsAttr;
use common\models\IntergralGoodsAttrGroup;
use common\models\IntergralGoodsSku;
use common\models\Order,
    common\models\OrderGoods;
use common\models\GoodsComment;
use common\models\IntergralGoods;
use DL\Project\SecondKill;
use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * Class IntergralGoodsService
 *
 * @package DL\service
 * @author  daorli
 */
class IntergralGoodsService
{
    /**
     * get goods detail
     *
     * @param int $goodsId  goods id
     * @param int $date     date (20181111)
     * @param int $deviceId device id
     *
     * @return object
     * @throws \Exception
     */
   public static function detail($goodsId)
   {
       $goods = IntergralGoods::findOne([
           'id' => $goodsId,
           'goods_state' => 1,
           'isdelete' => 0
       ]);

       if (!$goods) {
           throw new \Exception("商品信息不存在");
       }

       $goodsInfo = $goods->attributes;

       $goodsInfo['goods_pic'] = translateAbsolutePath($goodsInfo['goods_pic']);

       $goodsInfo['goods_image'] = arrayGroupsAction(json_decode($goods->goods_image, true), function ($array) {
           return translateAbsolutePath($array['imgurl']);
       });
       $goodsInfo['first_goods_image'] = reset($goodsInfo['goods_image']);

       $goodsInfo['goods_integral'] = $goods->goods_integral;

//       $goodsInfo['goodsAttr'] = [];
//       if ($goods->use_attr) {
//           $result = IntergralGoodsSku::findAll([
//               'goods_id' => $goods->id
//           ]);
//
//           $goodsInfo['goodsAttr'] = $result;
//       }

       // 商品规格
       $goodsSkuInfos = IntergralGoodsSku::findAll([
           'goods_id' => $goods->id
       ]);
       $goodsInfo['skuInfo'] = arrayGroupsAction($goodsSkuInfos, function ($goodsSkuInfo) {
           return $goodsSkuInfo->getAttrInfo();
       });
       // 规格设置数据
       $goodsInfo['skuInfoData'] = [];
       $goodsInfo['attrInfoData'] = [];
       $goodsInfo['AttrData'] = [];
       if ($goods->use_attr) {
           $skuDatas = IntergralGoodsSku::findAll([
               'goods_id' => $goods->id
           ]);

           $skuDataInfos = [];
           $attrDataInfos = [];

           $cacheAttrIds = [];$test = [];
           // sku info
           foreach ($skuDatas AS $skuData) {
               $skuAttr = json_decode($skuData->attr, true);$test[] = $skuAttr;
               rsort($skuAttr);

               $cacheAttrIds = array_merge($skuAttr, $cacheAttrIds);
               $key = implode('-', $skuAttr);
               $skuDataInfos[$key] = [
                   'id' => $skuData->id,
                   'num' => $skuData->num,
                   'price' => $skuData->integral,
                   'picimg' => translateAbsolutePath($skuData->picimg),
               ];
           }

           $cacheAttrIds = array_unique($cacheAttrIds);
           $attrDatas = IntergralGoodsAttr::find()->where([
               'in', 'id', $cacheAttrIds
           ])->all();
           // attr info
           foreach ($attrDatas AS $attrData) {
               if (!isset($attrDataInfos[$attrData->group_id])) {
                   $attrGroup = IntergralGoodsAttrGroup::findOne($attrData->group_id);
                   $attrDataInfos[$attrData->group_id] = [
                       'name' => $attrGroup->group_name,
                   ];
               }

               $attrDataInfos[$attrData->group_id]['sons'][$attrData->id] = [
                   'name' => $attrData->attr_name
               ];
           }

           // sku info
           $goodsInfo['skuInfoData'] = $skuDataInfos;
           $goodsInfo['attrInfoData'] = $attrDataInfos;
       }

       return (object)$goodsInfo;
   }

   public static function getRestAmount($goodsId, $deviceId, $date)
   {
       $goods = IntergralGoods::findOne([
           'id' => $goodsId,
           'goods_state' => 1
       ]);

       $ordersIds = Order::findAll([
            'receiver_mobile' => $date,
            'order_state' => Order::STATUS_SEND
       ]);
        if ($ordersIds) {
            $ordersIds = arrayGroupsAction($ordersIds, function ($order) {
                return $order->orderid;
            });
        } else {
            $ordersIds = ['00000'];
        }

        $ordersIds = implode("','", $ordersIds);

//        $restAmount = $goods->goods_storage - $sellAmount ;

        return $goods->goods_storage ? : 0;
   }
}