<?php
namespace DL\vendor;

use common\models\Config;
use DL\service\CacheService;

class ConfigService
{
    const EXPAND = 'INDEX_EXPAND';      // 拓展配置
    const INTEGRAL = 'INDEX_INTEGRAL';  // 积分配置

    private $_configData;

    public function __construct($cKey='')
    {
        $cacheKey = date('Ymd') . "CONFIG_adlkz,bqwe234{$cKey}";

        $configs = CacheService::init()->get($cacheKey);
        if (!$configs) {
            $filter = [];
            strlen($cKey) and $filter['cKey'] = $cKey;
            $configs = Config::find()->where($filter)->all();

            CacheService::init()->set($cacheKey, $configs);
        }

        $_config = [];
        if ($configs) {
            $amount = count($configs);
            for ($i=$amount; $i>0; $i--) {
                $_config = array_merge($_config, json_decode($configs[$i-1]->cValue, true));
            }
        }

        $_config or die("获取{$cKey}失败");

        $this->_configData = (object)$_config;
    }

    public static function init($cKey='')
    {
        return new self($cKey);
    }

    public function get($name)
    {
       return isset($this->_configData->$name) ? $this->_configData->$name : false;
    }
}