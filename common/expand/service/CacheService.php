<?php
namespace DL\service;
use DL\Project\FileCache;

/**
 * Class CacheService
 *
 * @package DL\service
 * @author  daorli
 */
class CacheService
{
    protected $cacheCtrl;

    /**
     * CacheService constructor.
     *
     * @param int $duration cache expire time
     */
    public function __construct($duration=100)
    {
        $this->cacheCtrl = new FileCache();
        $this->cacheCtrl->cachePath = ROOT_PATH . '/runtimes/data';
        $this->cacheCtrl->defaultDuration = $duration;
        $this->cacheCtrl->init();
    }

    public static function init($duration=100)
    {
        return new self($duration);
    }

    /**
     * set cache
     *
     * @param $key
     * @param array $data
     *
     * @return bool
     */
    public function set($key, $data=[])
    {
        return $this->cacheCtrl->set($key, $data);
    }

    /**
     * get cache
     *
     * @param $key
     *
     * @return mixed
     */
    public function get($key)
    {
        return $this->cacheCtrl->get($key);
    }

    /**
     * delete
     *
     * @param $key
     *
     * @return bool
     */
    public function delete($key)
    {
        return $this->cacheCtrl->delete($key);
    }

    /**
     * force flush all cache
     *
     * @return bool
     */
    public function flush()
    {
        return $this->cacheCtrl->gc(true, false);
    }
}