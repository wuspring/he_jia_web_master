<?php
namespace DL\vendor;
use common\models\Address;
use common\models\CommissionLog;
use common\models\Config;
use common\models\Jingxiaoshang;
use common\models\Member;
use common\models\Order;
use common\models\OrderGoods;
use common\models\ProxyLog;

/**
 * Class FenxiaoService
 * 三级分销功能
 *
 * @package DL\vendor
 * @author  daorli
 */
class FenxiaoService
{
    public $order;      // 订单信息

    public $percent;    // 分销商分成比例
    public $type;       // 分销商分成类型
    public $jxPercent;    // 经销商分成比例
    public $jxType;       // 经销商分成类型

    public function __construct($order)
    {
        $this->order = $order;
        $config = Config::findOne(['cKey' => 'fenxiao']);
        $configInfo = json_decode($config->cValue, true);
        $this->percent = array_slice($configInfo['lists'], 0, 3);

        switch ($configInfo['fashion']) {
            case '1' :
                $this->type = 'money';
                break;
            case '2' :
            default :
                $this->type = 'percent';
                break;
        }

        $config = Config::findOne(['cKey' => 'jingxiao']);
        $configInfo = json_decode($config->cValue, true);
        $this->jxPercent = array_slice($configInfo['lists'], 0, 3);

        switch ($configInfo['fashion']) {
            case '1' :
                $this->jxType = 'money';
                break;
            case '2' :
            default :
                $this->jxType = 'percent';
                break;
        }
    }

    /**
     * 预分成订单
     * @param Order $order
     */
    public static function order(Order $order)
    {
        $service = new self($order);
        $service->setThirdFenxiaoPercent($order->buyer_id);
        $service->setProxyers();
    }

    /**
     * 实际分成订单
     *
     * @param Order $order
     */
    public static function sureOrder(Order $order)
    {
        $fenxiaoLog = CommissionLog::findAll([
            'orderId' => $order->orderid,
            'status' => CommissionLog::STATUS_WAIT
        ]);

        if ($fenxiaoLog) {
            foreach ($fenxiaoLog AS $fenxiao) {
                $member = Member::findOne($fenxiao->memberId);
                $member->brokerage += $fenxiao->money;
                if ($member->save()) {
                    $fenxiao->status = $fenxiao::STATUS_FINISH;
                    $fenxiao->save();
                }
            }
        }

        $jingxiaoLog = ProxyLog::findAll([
            'orderId' => $order->orderid,
            'status' => CommissionLog::STATUS_WAIT
        ]);

        if ($jingxiaoLog) {
            foreach ($jingxiaoLog AS $jingxiao) {
                $member = Member::findOne($jingxiao->memberId);
                $member->proxyBrokerage += $jingxiao->money;
                if ($member->save()) {
                    $jingxiao->status = $fenxiao::STATUS_FINISH;
                    $jingxiao->save();
                }
            }
        }
    }

    /**
     * 设置三级分销比例
     *
     * @param int $memberId
     * @param int $step
     *
     * @return bool
     */
    public function setThirdFenxiaoPercent($memberId=0, $step=0)
    {
        $memberId < 0 and $memberId = $this->order->member->id;
        $member = Member::findOne($memberId);

        if (!(int)$member->pid) {
            return false;
        }

        $parentMember = Member::findOne($member->pid);
        $commissionLog = new CommissionLog();
        $commissionLog->memberId = $parentMember->id;
        $commissionLog->orderId = $this->order->orderid;
        $commissionLog->amount = $this->order->goods_amount;

        switch ($this->type) {
            case 'money' :
                $commissionLog->money = $this->percent[$step][$this->type];
                break;
            case 'percent' :
                $commissionLog->money = intval($this->order->goods_amount * $this->percent[$step][$this->type] / 100);
                break;
        }

        $commissionLog->status = $commissionLog::STATUS_WAIT;
        $commissionLog->create_time = date('Y-m-d H:i:s');

        $commissionLog->save();


        if ($parentMember->pid) {
            $step++;
            if ($step < 3) {
                $this->setThirdFenxiaoPercent($parentMember->id, $step);
            }
        }
    }

    /**
     * 设置代理商分成
     */
    public function setProxyers()
    {
        $address = Address::findOne($this->order->receiver);
        if ($address) {
            $goods =  OrderGoods::find()->where(['order_id'=>$this->order->orderid])
                ->orderBy('id ASC')->all();

            foreach ($goods AS $good) {
                foreach ([
                             Jingxiaoshang::LEVEL_PROVINCE,
                             Jingxiaoshang::LEVEL_CITY,
                             Jingxiaoshang::LEVEL_REGION,
                         ] AS $level => $type) {

                    $member = $this->_getOriginOwner($address, $type, $good['goods_id']);
                    $member and $this->_setOriginPercent($member->id, $good, $level);
                }
            }

        }
    }

    /**
     * 获取省、市、区等级区域经销商
     *
     * @param Address $address
     * @param $type
     * @param $goodId
     *
     * @return bool
     */
    private function _getOriginOwner(Address $address, $type, $goodId)
    {
        $filter = [
            'and',
            ['=', 'status', Jingxiaoshang::STATUS_APPLY]
        ];

        switch ($type) {
            case Jingxiaoshang::LEVEL_PROVINCE:
                $filter = array_merge($filter, [
                    ['=', 'level', $type],
                    ['=', 'province', $address->province]
                ]);
                break;
            case Jingxiaoshang::LEVEL_CITY:
                $filter = array_merge($filter, [
                    ['=', 'level', $type],
                    ['=', 'province', $address->city]
                ]);
                break;
            case Jingxiaoshang::LEVEL_REGION:
                $filter = array_merge($filter, [
                    ['=', 'level', $type],
                    ['=', 'province', $address->region]
                ]);
                break;
            default :
                return false;
        }
        $jingxiaoshangs = Jingxiaoshang::find()->where($filter)->all();

        $member = null;
        if ($jingxiaoshangs) {
            foreach ($jingxiaoshangs AS $jingxiaoshang) {
                $ids = explode(',', $jingxiaoshang->good_ids);

                if (in_array($goodId, $ids)) {
                    $member = $jingxiaoshang;
                }
            }
        }

        return $member;
    }

    /**
     * 设置分成金额
     *
     * @param $memberId
     * @param OrderGoods $good
     * @param $level
     */
    private function _setOriginPercent($memberId, OrderGoods $good, $level)
    {
        $proxyLog = new ProxyLog();
        $proxyLog->memberId = $memberId;
        $proxyLog->orderId = $good->order_id;
        $proxyLog->amount = $good->goods_price * $good->goods_num;

        switch ($this->jxType) {
            case 'money' :
                $proxyLog->money = $this->jxPercent[$level][$this->jxType];
                break;
            case 'percent' :
                $proxyLog->money = intval($proxyLog->amount * $this->percent[$level][$this->jxType] / 100);
                break;
        }

        $proxyLog->status = ProxyLog::STATUS_WAIT;
        $proxyLog->create_time = date('Y-m-d H:i:s');
        $proxyLog->save();
    }
}