<?php
namespace dlExpand;

/**
 * Class Position
 * @package dlExpand
 */
class Position
{
    public $lat;
    public $lng;
    public $radius; // 星球半径
    public function __construct($nowLat, $nowLng)
    {
        $this->lat = $nowLat;
        $this->lng = $nowLng;

        $this->radius = 6378.137;  // km
    }

    public function calculateFar( $articles, $float=2)
    {
        $articleLists = $articles->lists;

        foreach ($articleLists AS $key => $articleList) {
            $posLat = $articleList['addressInfo']['lat'];
            $posLng = $articleList['addressInfo']['lng'];

            $articleLists[$key]['far'] = $this->calculateFarByLocation($posLat, $posLng, $float);
        }

        return $articleLists;
    }

    /**
     * calculate around by far
     *
     * @param float $far far km
     *
     * @return array
     */
    public function calculateAround($far=0)
    {
        $degree = (24901 * 1609) / 360.0;
        $raidusMile = $far * 1000;

        $radiusLat = $raidusMile / $degree;

        $mpdLng = $degree * cos($this->lat * ( M_PI / 180));
        $radiusLng = $raidusMile / $mpdLng;

        $minLat = $this->lat - $radiusLat;
        $maxLat = $this->lat + $radiusLat;
        $minLng = $this->lng - $radiusLng;
        $maxLng = $this->lng + $radiusLng;

        return [
            'lat' => [$minLat, $maxLat],
            'lng' => [$minLng, $maxLng]
        ];
    }

    /**
     * calculate far by location
     *
     * @param float $posLat position lat
     * @param float $posLng position lng
     * @param int   $float  float
     *
     * @return float
     */
    public function calculateFarByLocation($posLat, $posLng, $float=2)
    {
        $rad = floatval(M_PI / 180.0);

        $lat1 = floatval($posLat) * $rad;
        $lon1 = floatval($posLng) * $rad;
        $lat2 = floatval($this->lat) * $rad;
        $lon2 = floatval($this->lng) * $rad;

        $theta = $lon2 - $lon1;

        $dist = acos(sin($lat1) * sin($lat2) + cos($lat1) * cos($lat2) * cos($theta));

        if ($dist < 0 ) {
            $dist += M_PI;
        }

        return round($dist * $this->radius, $float); // km
    }

}