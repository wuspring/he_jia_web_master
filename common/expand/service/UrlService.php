<?php
namespace DL\service;

use yii\helpers\Url;

/**
 * Class UrlService
 *
 * @package DL\service
 * @author  daorli
 */
class UrlService
{
    /**
     * build url
     *
     * @param mixed  $route   str/array
     * @param string $baseUrl base url
     * @param bool   $scheme  scheme
     *
     * @return string
     */
    public static function build($route, $baseUrl='', $scheme=false)
    {
        $urlInfo = Url::toRoute($route, $scheme);

        $search = defined('PAGE_URL') ? trim(PAGE_URL, '/') : false;
        $replace = strlen($baseUrl) ? $baseUrl : false;

        return $search && $replace ? str_replace($search, $replace, $urlInfo) : $urlInfo;
    }

    /**
     * valid url by login
     *
     * @param mixed $route              route
     * @param mixed $defaultNoLoginUrl  default not login route
     * @param bool  $formatUrl          check if format
     *
     * @return string
     */
    public static function buildUrlByLogin($route, $defaultNoLoginUrl, $formatUrl=true)
    {
        if(empty(\Yii::$app->user->identity)) {
            $url = strlen($defaultNoLoginUrl) ? ($formatUrl ? self::build($defaultNoLoginUrl) : $defaultNoLoginUrl) : 'javascript:void(0)';
        } else {
            $url = self::build($route);
        }

        return $url;
    }
}