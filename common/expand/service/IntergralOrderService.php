<?php
namespace DL\service;

use common\models\Address,
    common\models\IntergralGoods,
    common\models\Member,
    common\models\Order,
    common\models\IntergralOrderGoods,
    common\models\Cart,
    common\models\IntergralGoodsSku;
use common\models\IntergralOrder;
use DL\Project\Credit,
    DL\Project\SecondKill;


/**
 * Class OrderService
 *
 * @package DL\service
 * @author  daorli
 */
class IntergralOrderService
{
    /**
     * @return OrderService
     */
    public static function init()
    {
        return new self();
    }

    /**
     * create sigo goods order
     *
     * @param Member $member  member
     * @param int    $goodsId goods id
     * @param int    $amount  goods amount
     * @param int    $skuId   sku id
     *
     * @return Order
     * @throws \Exception
     */
    public function create(Member $member, $goodsId, $amount, $skuId=0)
    {
        if ($amount < 1) {
            throw new \Exception("请输入购买商品的数量", 400);
        }
        $goods = IntergralGoods::findOne([
            'id' => $goodsId,
            'goods_state' => 1,
            'isdelete' => 0
        ]);
        if (!$goods) {
            throw new \Exception("该商品已下架", 404);
        }

        // 商品价格判断
        $price = $goods->goods_integral;
        $pic = $goods->goods_pic;
        $skuInfo = '默认';

        $integral = 0;
        if ($skuId) {
            $goodsSku = IntergralGoodsSku::findOne([
                'goods_id' => $goods->id,
                'id' => $skuId
            ]);

            if (!$goodsSku) {
                throw new \Exception("商品信息规格有误", 400);
            }
            $price = $goodsSku->integral;
            $pic = $goodsSku->picimg;
            $skuInfo = $goodsSku->attrInfo;
        }

        $scOrder  = new IntergralOrder();
        $scOrder->orderid = $scOrder->getNewNumber();
        $scOrder->buyer_id = $member->id;
        $scOrder->buyer_name = $member->nickname;

        switch ((int)2) {
            // 积分商品
            case 2 :
                $payIntegral = $amount * $price;
                $totalPrice = $amount * $price;

                $member = Member::findOne($member->id);
                if ($member->integral < $payIntegral) {
                    throw new \Exception("您的积分不足", 400);
                }

                break;
            default :
                throw new \Exception("获取商品信息失败", 500);
        }

        $scOrder->goods_amount = $payIntegral;
        $scOrder->order_amount = $payIntegral;
        $scOrder->integral_amount = $payIntegral;

        $scOrder->order_state = $scOrder::STATUS_WAIT_PAY;

        $scOrder->receiver = 0;
        $scOrder->add_time = date('Y-m-d H:i:s');
        $scOrder->expand = $scOrder::EXPAND_ANYTIME;

        $defaultAddress = Address::findOne([
            'memberId' => $member->id,
            'isDefault' => 1,
            'is_delete' => 0
        ]);
        if ($defaultAddress) {
            $scOrder->receiver = $defaultAddress->id;
            $scOrder->receiver_name = $defaultAddress->name;
            $scOrder->receiver_mobile = $defaultAddress->mobile;
            $scOrder->receiver_address = $defaultAddress->provinceInfo . ' ' . $defaultAddress->address;
        } else {
            $scOrder->receiver = 0;
        }

        $scOrder->order_state = $scOrder::STATUS_WAIT_PAY;

        if (!$scOrder->save()) {
            throw new \Exception("创建订单失败，请稍后重试 ", 500);
        }

        $orderPhoto = new IntergralOrderGoods();
        $orderPhoto->order_id = $scOrder->orderid;
        $orderPhoto->goods_id = $goods->id;
        $orderPhoto->goods_pic = $pic;
        $orderPhoto->buyer_id = $member->id;
        $orderPhoto->goods_name = $goods->goods_name;
        $orderPhoto->goods_price = $price;

        $orderPhoto->goods_integral = $integral;
        $orderPhoto->fallinto_state = 0;
        $orderPhoto->goods_num = $amount;
        $orderPhoto->fallInto = $totalPrice;
        $orderPhoto->goods_pay_price = $totalPrice;
        $orderPhoto->createTime = date('Y-m-d H:i:s');
        $orderPhoto->sku_id = $skuId;
        $orderPhoto->attr = $skuInfo;
        $orderPhoto->modifyTime = date('Y-m-d H:i:s');

        $orderPhoto->save();     // 创建商品快照

        // 邮费
        $scOrder->freight = 0;
        $scOrder->save();

        return $scOrder;
    }
}