<?php
namespace DL\service;

use common\models\Cart;
use common\models\Collect;
use common\models\Comment;
use common\models\Config,
    common\models\Address,
    common\models\Goods,
    common\models\GoodsSku,
    common\models\Member,
    common\models\Order,
    common\models\OrderGoods;
use common\models\Devices;
use common\models\GoodsComment;
use common\models\GoodsDateLimitSetting;
use common\models\GoodsDateSetting;
use DL\Project\SecondKill;
use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * Class GoodsService
 *
 * @package DL\service
 * @author  daorli
 */
class GoodsService
{
    /**
     * get goods detail
     *
     * @param int $goodsId  goods id
     * @param int $date     date (20181111)
     * @param int $deviceId device id
     *
     * @return object
     * @throws \Exception
     */
   public static function detail($goodsId, $date, $deviceId, $memberId=0)
   {
       $unSecondKill = 99;
       $goods = Goods::findOne($goodsId);

       if (!$goods) {
           throw new \Exception("商品信息不存在");
       }

       $goods->goods_image = arrayGroupsAction(json_decode($goods->goods_image, true), function ($array) {
           return translateAbsolutePath($array['imgurl']);
       });
       $formatDate = date('Y-m-d', strtotime($date));
       $goodsInfo = $goods->attributes;
       $goodsSetting = GoodsDateSetting::findOne([
           'goods_id' => $goodsId,
           'device_id' => $deviceId,
           'date' => $formatDate
       ]);

       if (!$goodsSetting) {
           $goodsSetting = new GoodsDateSetting();
           $goodsSetting->date = $formatDate;
           $goodsSetting->goods_id = $goods->id;
           $goodsSetting->device_id = $deviceId;
           $goodsSetting->num = $goods->goods_storage;
           $goodsSetting->lock_num = 0;

           $goodsSetting->save();
       }

       $goodsInfo['secondKillNow'] = 0;
       switch ($goods->is_second_kill) {
           case 1 :
               $goodsInfo['restAmount'] = 0;
               $goodsInfo['goods_price'] = $goods->goods_price;
               if ($date == date('Ymd') and SecondKill::init()->checkGoodsStatus($goods->id, false)) {
                   $restAmount = SecondKill::init()->restAmount($goods->id);

                   $secondKillLimbo =  SecondKill::init()->getNowLimbo();
                   $keys = array_keys($secondKillLimbo);
                   $limboInfo = array_values($secondKillLimbo);

                   if ($restAmount) {
                       $goodsInfo['sk_price'] = $goods->second_price; // 秒杀价格
                       $goodsInfo['secondKillNow'] = 1;
                       $goodsInfo['secondKillLimboIndex'] = reset($keys);
                       $goodsInfo['secondKillLimbo'] = reset($limboInfo);
                       $goodsInfo['secondKillRestPercent'] = ceil($restAmount * 100 / $goods->second_limit);
                       $goodsInfo['secondKillSellPercent'] = ceil(($goods->second_limit - $restAmount) * 100 / $goods->second_limit);
                       $goodsInfo['secondKillRestAmount'] = SecondKill::init()->restAmount($goods->id);
                   } else {
                       $goodsInfo['sk_price'] = $goods->second_price; // 秒杀价格
                       $goodsInfo['secondKillNow'] = 1;
                       $goodsInfo['secondKillLimboIndex'] = reset($keys);
                       $goodsInfo['secondKillLimbo'] = reset($limboInfo);
                       $goodsInfo['secondKillRestPercent'] = 0;
                       $goodsInfo['secondKillSellPercent'] = 100;
                       $goodsInfo['secondKillRestAmount'] = 0;
                   }
               }
               break;
           case 0 :
           default :
               $goodsInfo['goods_price'] = $goods->goods_price;
       }

       $goodsInfo['restAmount'] > 0 or $goodsInfo['restAmount'] = 0;

       $goodsInfo['goods_pic'] = translateAbsolutePath($goodsInfo['goods_pic']);
       $goodsInfo['goods_storage'] = $goodsSetting->num;
       $goodsInfo['goods_commend'] = $goodsSetting->goods_commend;
       $goodsInfo['goods_hot'] = $goodsSetting->goods_hot;
       $goodsInfo['goods_is_time'] = (int)$goodsSetting->goods_is_time;
       $goodsInfo['goods_times'] = json_decode($goodsSetting->goods_times, true);
       $goodsInfo['goodsTimesInfo'] = [];
       // 推荐、热销只显示一个
       $goodsInfo['mark'] = '';
       $goodsInfo['isRecommend'] = 0;
       $goodsInfo['isHot'] = 0;
       if ($goodsSetting->goods_commend) {
           $goodsInfo['mark'] = '推荐';
           $goodsInfo['isRecommend'] = 1;
       }
       if ($goodsSetting->goods_hot) {
           $goodsInfo['mark'] = '热销';
           $goodsInfo['isHot'] = 1;
       }
       if ($goodsInfo['secondKillNow']) {
           $goodsInfo['mark'] = '秒杀';
           $goodsInfo['isRecommend'] = 0;
           $goodsInfo['isHot'] = 0;
       }

       // 购物车中数量
       $goodsInfo['cartNum'] = 0;
        // 收藏夹
       $goodsInfo['isCollect'] = 0;
       // 评论数
       $goodsInfo['commentNum'] = GoodsComment::find()->where(['goods_id' => $goods->id])->count();

       if ($memberId) {
           $cart = Cart::findOne([
               'buyerId' => $memberId,
               'goodsId' => $goods->id,
               'date' => $date,
               'deviceId' => $deviceId,
               'attr' => 99
           ]);
           $cart and $goodsInfo['cartNum'] = $cart->goodsNum;

           $collect = Collect::findOne([
               'member_id' => $memberId,
               'goods_id' => $goods->id
           ]);
           $collect and $goodsInfo['isCollect'] = 1;
       }

       if ($goodsInfo['goods_is_time']) {
           $details = GoodsDateLimitSetting::findAll([
               'date' => $date,
               'goods_id' => $goods->id
           ]);

           if ($details) {
               foreach ($details AS $detail) {
                   $goodsInfo['goodsTimesInfo'][] = $detail->attributes;
               }
           }
       }

       return (object)$goodsInfo;
   }

   public static function getRestAmount($goodsId, $deviceId, $date)
   {
       $goods = Goods::findOne([
           'id' => $goodsId,
           'goods_state' => 1
       ]);

       $device = Devices::findOne($deviceId);
       if (!in_array($goods->id, json_decode($device->goods_id, true))) {
           throw new \Exception("该设备并未售卖该商品");
       }

       $ordersIds = Order::findAll([
            'receiver' => $device->id,
            'receiver_mobile' => $date,
            'order_state' => Order::STATUS_SEND
       ]);
        if ($ordersIds) {
            $ordersIds = arrayGroupsAction($ordersIds, function ($order) {
                return $order->orderid;
            });
        } else {
            $ordersIds = ['00000'];
        }

        $ordersIds = implode("','", $ordersIds);
        $sellAmount = (int)OrderGoods::findBySql("select * from order_goods where goods_id='{$goods->id}' and `order_id` in ('{$ordersIds}')")->sum('goods_num');

        $restAmount = $goods->goods_storage - $sellAmount ;

        return $restAmount ? : 0;
   }

    /**
     * check sale condition
     *
     * @param $deviceId
     * @param $date
     * @param $memberId
     *
     * @return bool
     */
   public static function checkSaleCondition($deviceId, $date, $memberId=0)
   {
       $dateCommendGoods = GoodsDateSetting::findAll([
           'date' => date('Y-m-d', strtotime($date)),
           'device_id' => $deviceId,
           'goods_commend' => 1
       ]);

       if (!$dateCommendGoods) {
           return false;
       }

       $dateCommendGoodIds = ArrayHelper::getColumn($dateCommendGoods, 'goods_id');

       $cartInfo = Cart::find()->where([
           'and',
           ['=', 'buyerId', $memberId],
           ['=', 'deviceId', $deviceId],
           ['=', 'date', $date],
           ['in', 'goodsId', $dateCommendGoodIds]
       ])->all();

       return (bool)$cartInfo;
   }

   public static function search($deviceId, $date, $filter=[], $limit=[])
   {
       try {
           $device = Devices::findOne($deviceId);
           if (!$device) {
               throw new \Exception("未找到有效设备");
           }
           // 售卖的商品ID
           $ids = json_decode($device->goods_id, true);

           $limitBy = $limit ? "limit  {$limit[0]}, {$limit[1]}" : "";
           $orderBy = 'order by id desc';

           $filterDate = date('Y-m-d', strtotime($date));
           $impIds = implode(',', $ids);

           $memberId = 0;
           if (isset($filter['memberId'])) {
               $memberId = $filter['memberId'];
               unset($filter['memberId']);
           }
           $where = '';
           if ($filter) {
               $filterGroup = [];
               foreach ($filter AS $field => $val) {
                   switch ($field) {
                       case 'likeTitle' :
                           $filterGroup[] = "`goods_name` like '%{$val}%'";
                           break;
                       default :
                           $filterGroup[] = "`{$field}`='{$val}'";
                   }

               }
               $where = 'where ' . implode(' AND ', $filterGroup);
           }

           $goodsDetail = Goods::findBySql("select * from (select 
                            id,
                            goods_name,
                            goods_price as price,
                            ifnull((select num from goods_date_setting where `goods_id`= `goods`.id and date='{$filterDate}' and device_id='{$device->id}'), goods_storage) as num,
                            goods_commend,
                            goods_hot,
                            ifnull((select `date` from goods_date_setting where `goods_id`= `goods`.id and date='{$filterDate}' and device_id='{$device->id}'), '{$date}') as `date`,
                            egc_id,
                            goods_salenum
                            from goods where id in ({$impIds}) and `type` in (1) and `goods_state`='1' and `isdelete`='0') as `table` {$where} {$orderBy} {$limitBy}")->all();

           $goods = [];
           if ($goodsDetail) {
               foreach ($goodsDetail AS $goodDetail) {
                   $goods[] = (array)GoodsService::detail($goodDetail->id, $date, $device->id, $memberId);
               }
           }

           return $goods;
       } catch (\Exception $e) {
           die($e->getMessage());
           return [];
       }
   }
}