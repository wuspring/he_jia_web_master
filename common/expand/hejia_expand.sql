drop PROCEDURE if exists databaseInit;
drop PROCEDURE if exists setTicketInfos;
DELIMITER $$
create procedure databaseInit()
BEGIN
    CREATE TABLE if not exists `memory_tickets`  (
      `id` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
      `ticket_id` int(12) NULL,
      `provinces_id` int(12) UNSIGNED NULL,
      PRIMARY KEY (`id`)
    ) ENGINE = MEMORY;

    CREATE TABLE if not exists `ip_memory` (
      `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
      `ip` varchar(255) DEFAULT NULL,
      `unique_key` varchar(255) DEFAULT NULL,
      `provinces_id` int(10) unsigned DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `ip` (`ip`),
      KEY `unique_key` (`unique_key`)
  ) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='IP字典表';

  drop view if exists `user_active`;
  create view `user_active` as select `id`,`provinces_id` from `user` where `status`='1' and (`user_type`='1'  or `end_time` > DATE_FORMAT(date_sub(now(),interval 1 day), '%Y-%m-%d'));

  drop view if exists `ticket_apply_goods`;
  create view `ticket_apply_goods` as SELECT * from `ticket_apply_goods_data` WHERE `status`='1';
END $$

create procedure setTicketInfos(in ticketId int)
BEGIN
      declare now int;
      declare amount int;
			declare proId int;

      select  `id`, `name`, `citys`,`type` into @id, @name, @citys, @type from `ticket` where `id`= ticketId;

      set now = 0;

      set @jsonData = REPLACE(@citys, '[', '');
      set @jsonData = REPLACE(@jsonData, ']', '');
      set @jsonData = REPLACE(@jsonData, '"', '');

      set @str = SUBSTRING_INDEX(@jsonData, ',', 1);

        if CHAR_LENGTH(@jsonData)>0 then
         set proId = CONVERT(@str, SIGNED);

          insert `memory_tickets` (`ticket_id`, `provinces_id`) value (@id, proId);
          if not exists (select `id` from `ticket_info` where `ticket_id`=@id and `provinces_id`= proId)
            then
            insert `ticket_info` (`ticket_id`, `ticket_name`, `provinces_id`, `ticket_amount`,`type`)
              value (@id, @name, proId, 0, @type);
          end if;
          end if;

      delete from `ticket_info` where `provinces_id` not in (select `provinces_id` from `memory_tickets` where `ticket_id`=@id)
        and `ticket_id`= @id;
      delete from `memory_tickets` where `ticket_id`=@id;
END $$
DELIMITER;

drop procedure if exists `createUpdateStoreUser`;
DELIMITER $$
create procedure `createUpdateStoreUser` (in userId int)
BEGIN
  if exists (select `id` from `user` where `id` = userId and assignment='后台用户')
    THEN
    select `id`,`gc_ids`,`provinces_id`,`status` into @id, @gc_ids, @provinces_id, @status from `user` where `id` = userId and assignment='后台用户';
    if not exists (select `id` from `store_info` where `user_id`=@id and `provinces_id` = @provinces_id)
      then
        insert `store_info` (`user_id`, `provinces_id`) value (@id, @provinces_id);
    END if;

    if (@status > 0) then
      set @gc_ids = REPLACE(@gc_ids, '[', '');
      set @gc_ids = REPLACE(@gc_ids, ']', '');
      set @gc_ids = REPLACE(@gc_ids, '"', '');

      if CHAR_LENGTH (@gc_ids) < 1 then set @gc_ids = '0'; end if;
      --  清除并更新店铺信息
      delete from `store_gc_score` where `user_id` = @id and `gc_id` not in (@gc_ids);
      while CHAR_LENGTH(@gc_ids)>0 do
          set @str = SUBSTRING_INDEX(@gc_ids, ',', 1);

           -- 减去多余','
           set @rest = length(@gc_ids) - length(@str) - 1;
           if @rest <= 0 then set @rest = 0; end if;

           set @gc_ids = right (@gc_ids, @rest);

           set @gcId = CONVERT(@str, SIGNED);
            if not exists (select `id` from `store_gc_score` where `user_id`=@id and `gc_id`= @gcId)
              then
              insert `store_gc_score` (`user_id`, `provinces_id`, `gc_id`)
                value (@id, @provinces_id, @gcId);
            end if;
        end while;
      else
        delete from `store_gc_score` where `user_id` = @id;
      end if;

  END if;
END $$
DELIMITER;

drop trigger if exists `insert_ticket_info_by_ticket`;
drop trigger if exists `update_ticket_info_by_ticket`;
DELIMITER $$
create trigger `insert_ticket_info_by_ticket` AFTER INSERT ON `ticket` FOR EACH ROW
BEGIN
  if (NEW.status = '1') then
    if (length(NEW.citys) > 0) then
      call setTicketInfos(NEW.id);
    else
      set @notIn = '0';
      delete from `ticket_info` where `provinces_id` not in (@notIn) and `ticket_id`=NEW.id;
    end if;
  else
    delete from `ticket_info` where `ticket_id`=NEW.id;
  end if;
END$$
create trigger `update_ticket_info_by_ticket` AFTER UPDATE ON `ticket` FOR EACH ROW
BEGIN
  if (NEW.status = '1') then
    if (length(NEW.citys) > 0) then
      call setTicketInfos(NEW.id);
    else
      set @notIn = '0';
      delete from `ticket_info` where `provinces_id` not in (@notIn) and `ticket_id`=NEW.id;
    end if;
  else
    delete from `ticket_info` where `ticket_id`=NEW.id;
  end if;
END$$
DELIMITER;

drop trigger if exists `insert_new_user`;
drop trigger if exists `update_user`;
DELIMITER$$
create trigger `insert_new_user` AFTER INSERT ON `user` FOR EACH ROW
BEGIN
  insert `auth_assignment` (`item_name`, `user_id`) value (NEW.assignment, NEW.id);

  if (NEW.assignment='后台用户') then
--     insert `store_info` (`user_id`, `provinces_id`) value (NEW.id, NEW.provinces_id);
    call createUpdateStoreUser(NEW.id);
  end if;
END $$
create trigger `update_user` AFTER UPDATE ON `user` FOR EACH ROW
BEGIN
  if (NEW.assignment='后台用户') then
    call createUpdateStoreUser(NEW.id);
  end if;
END $$
DELIMITER;

call databaseInit();
