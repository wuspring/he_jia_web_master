<?php

/**
 * expand  functions
 *
 *  @author  daorli
 */
function getServerHost()
{
    $https = false;
    if (isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) == 'on')) {
        $https = true;
    }

    $pageURL = $https ? 'https' : 'http';
    $pageURL .= '://';

    $port = intval($_SERVER['SERVER_PORT']);

    if (($https && $port == 443) || ($https == false && $port == 80)) {
        $pageURL .= trim($_SERVER['SERVER_NAME']);
    } else {
        $pageURL .= trim($_SERVER['SERVER_NAME']) . ':' . trim($_SERVER['SERVER_PORT']);
    }

    return $pageURL;
}

/**
 * translate absolute path
 * @author  daorli
 *
 * @param string $path path
 *
 * @return string
 */
function translateAbsolutePath($path) {
    if (preg_match('/^http[s]?/', $path)) {
        return $path;
    }

    return getServerHost() . '/' . ltrim($path, '/');
}

/**
 * project  function
 * @author  daorli
 *
 * @param string $string
 * @param int    $picWidth
 *
 * @return string
 */
function build_picture($string, $picWidth=120) {
    $pics = explode(',', $string);

    $imgHtml = '';
    $tmp = '<img src="%s" alt="" style=\'width:' . $picWidth . 'px;display: inline-block\' />';
    foreach ($pics AS $pic) {
        $imgHtml .= (sprintf($tmp, $pic) . "\r\n");
    }

    return $imgHtml;
}

/**
 * create rand key
 * @author  daorli
 *
 * @param int    $strLen     string length
 * @param string $dictionary dictionary
 *
 * @return string
 */
function createRandKey($strLen=8, $dictionary='0123456789')
{
    $max = strlen($dictionary)-1;

    $name = '';
    for ($i=0;$i<$strLen;$i++) {
        $name .= $dictionary[rand(0, $max)];
    }

    return $name;
}

/**
 * 获取字符串首字母
 * @author  wufeng
 *
 * @param string   $str
 * @return string
 */

function getFirstCharter($str) {
    if (empty($str)) {
        return '';
    }
    $fchar = ord($str{0});
    if ($fchar >= ord('A') && $fchar <= ord('z')) return strtoupper($str{0});
    $s1 = iconv('UTF-8', 'gb2312', $str);
    $s2 = iconv('gb2312', 'UTF-8', $s1);
    $s = $s2 == $str ? $s1 : $str;
    $asc = ord($s{0}) * 256 + ord($s{1}) - 65536;

    if ($asc >= -20319 && $asc <= -20284) return 'A';
    if ($asc >= -20283 && $asc <= -19776) return 'B';
    if ($asc >= -19775 && $asc <= -19219) return 'C';
    if ($asc >= -19218 && $asc <= -18711) return 'D';
    if ($asc >= -18710 && $asc <= -18527) return 'E';
    if ($asc >= -18526 && $asc <= -18240) return 'F';
    if ($asc >= -18239 && $asc <= -17923) return 'G';
    if ($asc >= -17922 && $asc <= -17418) return 'H';
    if ($asc >= -17417 && $asc <= -16475) return 'J';
    if ($asc >= -16474 && $asc <= -16213) return 'K';
    if ($asc >= -16212 && $asc <= -15641) return 'L';
    if ($asc >= -15640 && $asc <= -15166) return 'M';
    if ($asc >= -15165 && $asc <= -14923) return 'N';
    if ($asc >= -14922 && $asc <= -14915) return 'O';
    if ($asc >= -14914 && $asc <= -14631) return 'P';
    if ($asc >= -14630 && $asc <= -14150) return 'Q';
    if ($asc >= -14149 && $asc <= -14091) return 'R';
    if ($asc >= -14090 && $asc <= -13319) return 'S';
    if ($asc >= -13318 && $asc <= -12839) return 'T';
    if ($asc >= -12838 && $asc <= -12557) return 'W';
    if ($asc >= -12556 && $asc <= -11848) return 'X';
    if ($asc >= -11847 && $asc <= -11056) return 'Y';
    if ($asc >= -11055 && $asc <= -10247) return 'Z';

    return null;
}

/**
 * 手机号*替换中间4位
 * @author 黄西方
 */
function substr_cut($user_name)
{
    $strlen = mb_strlen($user_name, 'utf-8');

    $firstStr = mb_substr($user_name, 0, 4, 'utf-8');
    $lastStr = mb_substr($user_name, -4, 4, 'utf-8');
    return $strlen == 2 ? $firstStr . str_repeat('*', mb_strlen($user_name, 'utf-8') - 1) : $firstStr . str_repeat("*", $strlen - 7) . $lastStr;
}

/**
 * valid field exists
 * @author daorli
 *
 * @param array $requestFields
 * @param array $data
 *
 * @return bool
 * @throws Exception
 */
function validFieldsExists($requestFields=[], $data)
{
    $fields = array_keys($data);
    foreach ($requestFields AS $field) {
        if (!in_array($field, $fields)) {
            throw new \Exception("required field[{$field}] is not exist");
        }
    }
    return true;
}

/**
 * api send success
 * @author daorli
 *
 * @param string $message send message
 * @param array  $data    array
 */
function apiSendSuccess($message='ok', $data=[])
{
    $request = [
        'status' => 1,
        'msg' => $message,
        'data' => $data
    ];

    die(json_encode($request));
}

/**
 * api send error
 * @author daorli
 *
 * @param string $message send message
 */
function apiSendError($message="failed")
{
    $request = [
        'status' => 0,
        'msg' => $message
    ];

    die(json_encode($request));
}

/**
 * format object to array
 * @author
 *
 * @param array $objLists object list
 *
 * @return array
 */
function formatObjLists($objLists)
{
    $array = [];

    foreach ($objLists AS $objList) {
        $array[] = $objList->attributes;
    }

    return $array;
}

/*
* rebuild tree
* @author daorli
*
* @param array  $array     array
* @param int    $type      1:顺序菜单 2树状菜单
* @param int    $parentId  parent id
* @param int    $loopTimes loop times
* @param string $upField   up field
 *
* @return array
*/
function rebuildTree($array=[], $type=1, $parentId=0, $loopTimes=0, $upField = 'fid')
{
    $column = [];
    if($type == 2) {
        foreach($array as $key => $vo) {
            if ($vo[$upField] == $parentId) {
                $column[$key] = $vo;
                $column [$key]['sons'] = rebuildTree($array, $type = 2, $vo['id'], $loopTimes + 1, $upField);
            }
        }
    } else {
        foreach($array as $key => $vo){
            if($vo[$upField] == $parentId){
                $column[] = $vo;
                $column =array_merge($column, rebuildTree($array, $type=1, $vo['id'],$loopTimes + 1, $upField));
            }
        }
    }

    return array_values($column);
}

/**
 * decode js input upload
 *
 * @param string $string json string
 *
 * @return array
 */
function decodeJsInputUpload($string)
{
    $result = [];
    if (strlen($string)) {
        $datas = json_decode($string, true);
        if ($datas) {
            $result = arrayGroupsAction($datas, function ($data) {
                return explode('@', $data);
            }, true);
        }
    }

    return $result;
}

/**
 * decode js input upload ico
 *
 * @param string $string json string
 *
 * @return array
 */
function decodeJsInputUploadIco($string)
{
    $result = [];
    if (strlen($string)) {
        $datas = json_decode($string, true);
        if ($datas) {
            $result = arrayGroupsAction($datas, function ($data) {
                return explode('@', $data);
            }, true);

            foreach ($datas AS $index => $guider) {
                list($href, $pics, $unUseful, $text) = explode('@', $guider);
                list($defaultIco, $selectIco) = strlen($pics) ? explode('^', $pics) : ['', ''];

                $result[$index] = [
                    'href' => $href,
                    'defaultIco' => $defaultIco,
                    'selectIco' => $selectIco,
                    'text' => $text
                ];
            }
        }
    }

    return $result;
}

/**
 * 前端停止流程
 * @author daorli
 *
 * @param string $msg
 * @param string $location
 */
function stopFlow($msg, $location = 'index/index', $status = false)
{
    if (RENDER_PAGE) {
        if ($location === false) {
            $href = 'window.location.href;';
        } else if (is_string($location) and preg_match('/^javascript/', (string)$location)) {
            $href = $location;
        } else {
            $href = \Yii::$app->getUrlManager()->createUrl($location);
        }

        switch (MODULE_NAME) {
            // PC 流程停止（成功、失败）设置
            case 'index' :
            // 手机端 流程停止（成功、失败）页面
            case 'wap' :
                $session = Yii::$app->session;
                $session->set('__ERROE_INFOMATION__', [
                    'msg' => $msg,
                    'href' => $href
                ]);

                if ($location !== false) {
                    $lHref = $status ? 'system/success' : 'system/error';
                    header("Location: " . \Yii::$app->getUrlManager()->createUrl($lHref));
                    die();
                }
                break;
            default :
                $location !== false and $href = "'{$href}'";
                $html = <<<HTML
        <html>
            <head>
                <meta charset="utf-8">
            </head>
            <body>
                <script>
                    alert('{$msg}');
                    window.location.href= {$href}
                </script>
            </body>
        </html>
HTML;

                die($html);
        }

    } else {
        apiSendError($msg);
    }
}

function arrayGroupsAction($array=[], $callback, $keepIndex=false) {
    $res = [];
    $array = (array)$array;

    foreach ($array AS $key => $value) {
        if ($keepIndex) {
            $res[$key] = call_user_func($callback, $value, $key);
        } else {
            $res[] = call_user_func($callback, $value, $key);
        }
    }
    return $res;
}

function rebuildFooterHtml($string)
{
    $html = '';

    if (strlen($string)) {
        foreach (explode('|', $string) AS $index => $href) {
            $template = $index ? '<li class="nav-item">%s</li>' : '<li class="nav-item li0">%s</li>';
            $html .= sprintf($template, $href);
        }
    }

    return $html;
}

function createUrlByCondition($truePath='', $failPath='', $condition, $formatPath=true)
{
    $url = $condition ? $truePath : $failPath;

    return $formatPath ? \DL\service\UrlService::build($url) : $url;
}


function getClientIp() {
    if(getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
        $ip = getenv('HTTP_CLIENT_IP');
    } elseif(getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
        $ip = getenv('HTTP_X_FORWARDED_FOR');
    } elseif(getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
        $ip = getenv('REMOTE_ADDR');
    } elseif(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return preg_match ( '/[\d\.]{7,15}/', $ip, $matches ) ? $matches [0] : '';
}

function isMobile()
    {
        // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
        if (isset ($_SERVER['HTTP_X_WAP_PROFILE'])) {
            return TRUE;
        }
        // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
        if (isset ($_SERVER['HTTP_VIA'])) {
            return stristr($_SERVER['HTTP_VIA'], "wap") ? TRUE : FALSE;// 找不到为flase,否则为TRUE
        }
        // 判断手机发送的客户端标志,兼容性有待提高
        if (isset ($_SERVER['HTTP_USER_AGENT'])) {
            $clientkeywords = array(
                'mobile',
                'nokia',
                'sony',
                'ericsson',
                'mot',
                'samsung',
                'htc',
                'sgh',
                'lg',
                'sharp',
                'sie-',
                'philips',
                'panasonic',
                'alcatel',
                'lenovo',
                'iphone',
                'ipod',
                'blackberry',
                'meizu',
                'android',
                'netfront',
                'symbian',
                'ucweb',
                'windowsce',
                'palm',
                'operamini',
                'operamobi',
                'openwave',
                'nexusone',
                'cldc',
                'midp',
                'wap',
                "iphone", "ipod", 'ipad'
            );
            // 从HTTP_USER_AGENT中查找手机浏览器的关键字
            if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
                return TRUE;
            }
        }
        if (isset ($_SERVER['HTTP_ACCEPT'])) { // 协议法，因为有可能不准确，放到最后判断
            // 如果只支持wml并且不支持html那一定是移动设备
            // 如果支持wml和html但是wml在html之前则是移动设备
            if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== FALSE) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === FALSE || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
                return TRUE;
            }
        }
        return FALSE;
    }

/**
 * 检测字符串是否utf-8 否则转换编码
 * @param $str
 * @return string
 */
    function iconvToUtf8($str){
        //检测当前字符串的编码格式
        $encoding=mb_detect_encoding($str,array("ASCII",'UTF-8',"GB2312","GBK",'BIG5'));
        // 如果字符串的编码格式不为UTF_8就转换编码格式
        if ($encoding!='UTF-8') {
            $str = mb_convert_encoding($str, 'UTF-8',$encoding);
        }
        return $str;
    }


