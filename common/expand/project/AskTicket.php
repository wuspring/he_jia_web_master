<?php
namespace DL\Project;

use common\models\ConfigAskTicket;
use DL\Base\Config;
use DL\service\CacheService;

class AskTicket extends Config
{
    public function __construct()
    {
        $this->dataConstruct = [
            'banner' => '',
            'index_top' => [
                'pic_1' => '@',
            ],
            'index_footer' => [
                'pic_1' => '@',
                'pic_2' => '@',
                'pic_3' => '@',
            ],
            'zhanhui_pic' => [
                'pic_1' => '@/public/index/images/_temp/skin/img_jia_ad01.jpg',
                'pic_2' => '@/public/index/images/_temp/skin/img_jia_ad02.jpg',
                'pic_3' => '@/public/index/images/_temp/skin/img_jia_ad03.jpg',
                'pic_4' => '@/public/index/images/_temp/skin/img_jia_ad04.jpg',
                'pic_5' => '@/public/index/images/_temp/skin/img_jia_ad05.jpg',
                'pic_6' => '@',
                'pic_7' => '@',
                'pic_8' => '@',
            ]
        ];
    }

    public static function init()
    {
        return new self();
    }
}