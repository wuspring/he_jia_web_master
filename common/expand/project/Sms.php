<?php
namespace DL\Project;

use common\models\Goods;
use common\models\GoodsCoupon;
use common\models\Member;
use common\models\Ticket;
use common\models\TicketApply;
use DL\vendor\ConfigService;
use yii\helpers\StringHelper;


class Sms
{
    private $_config;

    public $data;    // 信息内容

    protected $tpId;    //产品id
    protected $signature;

    public $msg;
    public function __construct()
    {
        $this->_config = [
            'username' => 'hjjbh',
            'password' => 'RwSYby8D',

//            'sys_wx' => 'hejia-4677878'
        ];

        ini_set('date.timezone','Asia/Shanghai');
    }

    public static function init()
    {
        return new self();
    }

    /**
     * 获取验证码
     * @param $phone
     * @param $code
     * @param $cityId
     */
    public function code($phone, $code, $cityId)
    {
        $this->tpId = '676767';
        $this->signature = '【和家家博会】';

        // 城市客服电话
        $cityConfig = CityExpand::init()->get($cityId, CityExpand::TYPE_CITY_EXPAND);
        $phoneTel = ConfigService::init('system')->get('telephone');
        $cityPhone = strlen($cityConfig->tel) ? $cityConfig->tel : $phoneTel;;

        $time = 30;
        $tmp = "{$this->signature}验证码{$code}（{$time}分钟内有效），如非本人操作请忽略此短信。
验证码关系重大，为了您的隐私安全，请勿将验证码告知他人。如有疑问请致电{$cityPhone}，谢谢~ ";

        return $this->send([
            'mobile' => $phone,
            'content' => $tmp
        ]);
    }


    /**
     * 门票预约
     */
    public function askTicktet($phone, $ticketId)
    {
        $this->tpId = '887362';
        $this->signature = '【和家家博会】';

        $ticket = Ticket::findOne($ticketId);

        // 城市客服电话
        $cityConfig = CityExpand::init()->get($ticket->citys, CityExpand::TYPE_CITY_EXPAND);
        $phoneTel = ConfigService::init('system')->get('telephone');
        $cityPhone = strlen($cityConfig->tel) ? $cityConfig->tel : $phoneTel;;

        $openDate = date('Y.m.d', strtotime($ticket->open_date));
        $endDate = date('m.d', strtotime($ticket->end_date));

        $tmp = "{$this->signature}恭喜您报名成功，{$ticket->name}将于{$openDate}~{$endDate}在{$ticket->open_place}盛大开幕！稍后会有客服人员与您电话沟通，请注意接听，咨询电话{$cityPhone}！";

        return $this->send([
            'mobile' => $phone,
            'content' => $tmp
        ]);
    }

    /**
     * 预存产品
     *
     * @param $phone
     * @param $goodId
     * @param $ticketId
     */
    public function yuCunChanPin($phone, $goodId, $ticketId)
    {
        $this->tpId = '887362';
        $this->signature = '【和家家博会】';

        $ticket = Ticket::findOne($ticketId);

        $good = Goods::findOne($goodId);
        $good or die("[Sms]商品不存在");

        $request = 2;
        $openDate = date('Y.m.d', strtotime($ticket->open_date));;
        $endDate = date('m.d', strtotime($ticket->end_date));

        $tmp = "{$this->signature}恭喜您已成功报名{$good->goods_name}预存产品，本产品价格需支付订金{$ticket->order_price}元可参与享受，活动现场转单成功{$ticket->order_price}可抵订金使用, 工作人员会在{$request}天内给您来电，请您注意接听电话；新疆家博会将于{$openDate}~{$endDate}日在{$ticket->open_place}盛大开幕，欢迎前来逛购！";

        return $this->send([
            'mobile' => $phone,
            'content' => $tmp
        ]);
    }

    /**
     * 爆款预约
     *
     * @param $phone
     * @param $goodId
     * @param $ticketId
     */
    public function baoKuanYuYue($phone, $goodId, $ticketId)
    {
        $this->tpId = '887362';
        $this->signature = '【和家家博会】';

        $ticket = Ticket::findOne($ticketId);

        $good = Goods::findOne($goodId);
        $good or die("[Sms]商品不存在");

        // 城市客服电话
        $cityConfig = CityExpand::init()->get($ticket->citys, CityExpand::TYPE_CITY_EXPAND);
        $phoneTel = ConfigService::init('system')->get('telephone');
        $cityPhone = strlen($cityConfig->tel) ? $cityConfig->tel : $phoneTel;;

        $openDate = date('Y.m.d', strtotime($ticket->open_date));;
        $endDate = date('m.d', strtotime($ticket->end_date));
        
        $tmp = "{$this->signature}恭喜您成功预约{$good->goods_name}！{$openDate}~{$endDate}前往{$ticket->open_place}现场看样，凭预约短信享特价购买权。有问题可致电{$cityPhone}。";

        return $this->send([
            'mobile' => $phone,
            'content' => $tmp
        ]);
    }

    /**
     * 地址栏
     *
     * @param $phone
     * @param $ticketId
     */
    public function diZhiLan($phone, $ticketId)
    {
        $this->tpId = '887362';
        $this->signature = '【和家家博会】';

        $ticket = Ticket::findOne($ticketId);

        $tmp = "{$this->signature} {$ticket->open_place}，地址：{$ticket->address}。";

        return $this->send([
            'mobile' => $phone,
            'content' => $tmp
        ]);
    }

    /**
     * 品牌预约
     *
     * @param $phone
     * @param $storeId
     * @param $ticketId
     * @throws \Exception
     */
    public function pinPaiYuYue($phone, $storeId, $ticketId)
    {
        $this->signature = '【和家家博会】';
        $this->tpId = '887362';

        $store = Store::init()->info($storeId);
        $store or die('[Sms]店铺不存在或已被禁用');

        $ticket = Ticket::findOne($ticketId);
        $openDate = date('Y.m.d', strtotime($ticket->open_date));;
        $endDate = date('m.d', strtotime($ticket->end_date));

        // 城市客服电话
        $cityConfig = CityExpand::init()->get($ticket->citys, CityExpand::TYPE_CITY_EXPAND);
        $phoneTel = ConfigService::init('system')->get('telephone');
        $cityPhone = strlen($cityConfig->tel) ? $cityConfig->tel : $phoneTel;;

        $tmp = "{$this->signature}恭喜您成功预约{$store->nickname}！{$openDate}~{$endDate}前往{$ticket->open_place}现场看样，凭预约短信享特价购买权。有问题可致电{$cityPhone}。";

        return $this->send([
            'mobile' => $phone,
            'content' => $tmp
        ]);
    }

    /**
     * 商户活动预约
     *
     * @param $phone
     * @param $storeId
     * @param $ticketId
     * @throws \Exception
     */
    public function storeHuoDongYuYue($phone, $storeId, $ticketId)
    {
        $this->signature = '【和家家博会】';
        $this->tpId = '887362';

        $store = Store::init()->info($storeId);
        $store or die('[Sms]店铺不存在或已被禁用');

        $apply = TicketApply::findOne([
            'ticket_id' => $ticketId,
            'user_id' => $store->user_id
        ]);

        $ticket = Ticket::findOne($ticketId);
        $openDate = date('Y.m.d', strtotime($ticket->open_date));;
        $endDate = date('m.d', strtotime($ticket->end_date));

        $tmp = "{$this->signature}亲爱的，您已成功预约{$store->nickname}活动，凭预约短信至商户展位参与，名额有限先到先得~地址：{$openDate}~{$endDate} {$ticket->open_place}，{$store->nickname} 展位号{$apply->zw_pos}~";

        return $this->send([
            'mobile' => $phone,
            'content' => $tmp
        ]);
    }

    /**
     * 优惠券预约
     *
     * @param $phone
     * @param $couponId
     * @param $ticketId
     * @throws \Exception
     */
    public function youHuiQuanYuYue($phone, $couponId, $ticketId)
    {
        $this->signature = '【和家家博会】';
        $this->tpId = '887362';

        try {
            $goodsCoupon = GoodsCoupon::findOne($couponId);

            $store = Store::init()->info($goodsCoupon->user_id);
            $store or die('[Sms]店铺不存在或已被禁用');

            $couponName = '全品类通用优惠券';
            if ($goodsCoupon->goods_id > 0) {
                $couponName = "{$goodsCoupon->goods->goods_name} 优惠券";
            }

            $ticket = Ticket::findOne($ticketId);

            $openDate = date('Y.m.d', strtotime($ticket->open_date));;
            $endDate = date('m.d', strtotime($ticket->end_date));

            if ($ticket) {
                $tmp = "{$this->signature}恭喜，您成功领取{$store->nickname} {$couponName}，和家家博会现场（{$openDate}~{$endDate} {$ticket->open_place}），请下单前出示本短信，下单后出示无效！";
            } else {
                $tmp = "{$this->signature}恭喜，您成功领取{$store->nickname} {$couponName}。";
            }

            return $this->send([
                'mobile' => $phone,
                'content' => $tmp
            ]);
        } catch (\Exception $e) {
//            die('[Sms]' . $e->getMessage());
            return false;
        }
    }

    /**
     * 注册会员
     *
     * @param $memberId
     * @param $initPwd
     */
    public function zhuCeHuiYuan($memberId, $initPwd)
    {
        $member = Member::findOne($memberId);

        // 城市客服电话
        $cityConfig = CityExpand::init()->get((int)$member->origin, CityExpand::TYPE_CITY_EXPAND);
        $phoneTel = ConfigService::init('system')->get('telephone');
        $cityPhone = strlen($cityConfig->tel) ? $cityConfig->tel : $phoneTel;;

        $tmp = "{$this->signature}你已成为和家家博会员，用户名：{$member->username}，初始密码：{$initPwd}。为了您的账号安全，请及时登录官网个人中心修改密码，有问题请致电{$cityPhone}";

        return $this->send([
            'mobile' => $member->mobile,
            'content' => $tmp
        ]);
    }

    protected function send_disabled($data)
    {
//        $data = [
//            'tpId' => (string)$this->tpId,
//            'records' => [$data],
//            'tKey' => (string)time(),
//            'signature' => $this->signature,
//        ];

        $data['tKey'] = (string)time();

        $data = array_merge($data, $this->_config);

        $data['password'] = md5( md5($data['password']) . $data['tKey']);

        file_put_contents(ROOT_PATH . '/runtimes/sms.log', json_encode($data) . "\r\n===============\r\n", FILE_APPEND);

        $data = json_encode($data);
        $headers = [
            "Content-type: application/json;charset='utf-8'"
        ];
        $curl = curl_init();
//        curl_setopt($curl, CURLOPT_URL, 'http://api.mix2.zthysms.com/v2/sendSmsTp');
        curl_setopt($curl, CURLOPT_URL, 'http://api.mix2.zthysms.com/v2/sendSms');
//        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $data = curl_exec($curl);
        curl_close($curl);

        $data = json_decode($data);

        if ($data->code != 200) {
            $this->msg = $data->msg;
        }

        return $data->code == 200;
    }

    protected function send($data)
    {
        $data['tkey'] 	= date('YmdHis');
        $data = array_merge($data, $this->_config);

        $data['password'] = md5( md5($data['password']) . $data['tkey']);

        $data['productid'] = $this->tpId;
        $data['xh'] = '';

        file_put_contents(ROOT_PATH . '/runtimes/sms.log', json_encode($data) . "\r\n===============\r\n", FILE_APPEND);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'http://www.ztsms.cn/sendNSms.do');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS,  http_build_query($data));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl); // 执行操作

        curl_close($curl);
        list($status) = explode(',', (string)$result);

        return $status == 1;
    }
}