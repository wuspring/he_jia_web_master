<?php
/**
 * Created by PhpStorm.
 * User: wufeng
 * Date: 2019/12/13
 * Time: 10:01
 */

namespace DL\Project;

use DL\vendor\ConfigService;
use Yii;
/**
 * 腾讯地图API
 * Class TecentMapApi
 * @package DL\Project
 */
class TecentMapApi
{
    private $key;

    public function __construct()
    {
        $this->key = ConfigService::init(ConfigService::EXPAND)->get('map_key');
    }

    static public function init()
    {
        return new self();
    }

    /**
     * 地址解析（腾讯地图）
     * @param $address  解析地址
     * @return bool|mixed|string  返回坐标字符串
     */
    public function addressToCoordinate($address){
        $key = $this->key;
        $url = 'https://apis.map.qq.com/ws/geocoder/v1/?address='.$address.'&key='.$key;
        $json = $this->getJsonGet($url);
        $json = json_decode($json);
        if($json->status==0){
            return implode(',',[$json->result->location->lat,$json->result->location->lng]);
        }
        return $json;
    }

    /**
     * 获取定位位置 - 腾讯地图
     * @return mixed
     */
    function getPositionByIp(){
        $key = $this->key;
        $ip = getClientIp();
        $url = "https://apis.map.qq.com/ws/location/v1/ip?key=".$key;
        $result = $this->getJsonGet($url);
        return json_decode($result);
    }

    /**
     * curl get 访问
     * @param $url
     * @return bool|string
     */
    function getJsonGet($url)
    {
        //初始化
        $ch = curl_init();
        //设置抓取的url
        curl_setopt($ch, CURLOPT_URL, $url);
        //设置头文件的信息作为数据流输出
        curl_setopt($ch, CURLOPT_HEADER, 0);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //执行命令
        $data = curl_exec($ch);
        if (curl_errno($ch)){
            return 'Curl error: ' . curl_error($ch);
        }
        //关闭URL请求
        curl_close($ch);
        //显示获得的数据
        return $data;
    }
}