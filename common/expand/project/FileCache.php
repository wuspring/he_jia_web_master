<?php
namespace DL\Project;

use \yii\caching\FileCache AS BaseFileCache;

class FileCache extends BaseFileCache
{
    /**
     * @var int default duration 持续时间
     */
    public $defaultDuration;
}