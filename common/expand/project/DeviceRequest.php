<?php
namespace DL\Project;

use common\models\Config;
use common\models\Member,
    common\models\AccountLog;
use yii\db\Exception;

/**
 * Class DeviceRequest
 * 售货机设备通讯接口
 *
 * @package DL\Project
 * @author  daorli
 */
class DeviceRequest
{
    private $_deviceApi;
    private $_deviceKey;
    private $_deviceSecret;
    private $_sellerCode;

    public function __construct()
    {
        $config = Config::findOne([
            'cKey' => 'INDEX_EXPAND'
        ]);

        $configInfo = json_decode($config->cValue);
        $this->_sellerCode = $configInfo->seller_code;
        $this->_deviceApi = $configInfo->device_api;
        $this->_deviceKey = $configInfo->device_key;
        $this->_deviceSecret = $configInfo->device_secret;
    }

    public static function init()
    {
        return new self();
    }

    public function request($path, $query=[])
    {
        try {
            $time = time() . '000';

            $salt = $this->_createToken($query, $time);

            $query = array_merge($query, [
                'key' => $this->_deviceKey,
                'timestamp' => $time,
                'sign' => $salt
            ]);

            $ch = curl_init();
            $url = $this->_buildUrl($path);

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json; charset=utf-8'
            ]);
//            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
            $query = json_encode($query);
            curl_setopt($ch, CURLOPT_POSTFIELDS , $query);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);

            $data = json_decode($output, true);
            if (!$output or !$data) {
                throw new Exception(curl_error($ch));
            }
            curl_close($ch);

            return $data;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    const DEVICE_LIST = 'DEVICE_LIST'; // 售货机列表
    const DEVICE_STATUS = 'DEVICE_STATUS'; // 售货机状态查询接口
    const DEVICE_GOODS = 'DEVICE_GOODS'; // 商品详情查询接口
    const DEVICE_LOCK_GOODS = 'DEVICE_GOODS'; // 锁定售货机货道商品库存接口
    const DEVICE_GOODS_LIST = 'DEVICE_GOODS_LIST'; // 售货机货道商品查询接口
    const DEVICE_TAKE_OUT_GOODS = 'DEVICE_TAKE_OUT_GOODS'; // 远程出货通知接口


    private function _buildUrl($path)
    {
        $dic = [
            self::DEVICE_LIST => '/service-api/api/queryMachine',
            self::DEVICE_STATUS => '/service-api/api/queryMachineState',
            self::DEVICE_GOODS => '/service-api/api/queryGoodDetails',
            self::DEVICE_LOCK_GOODS => '/service-api/api/lockMachineGoodRepertory',
            self::DEVICE_GOODS_LIST => '/service-api/api/queryMachineHdGood',
            self::DEVICE_TAKE_OUT_GOODS => '/service-order/higo/applyExportGoodsByDdbh'
        ];

        if (!in_array($path, array_keys($dic))) {
            throw new \Exception("路径无效");
        }

        return rtrim($this->_deviceApi, '/') . '/' . ltrim($dic[$path], '/');
    }

    private function _createToken($query, $time)
    {
        ksort($query);
        return md5($this->_deviceSecret . $time . http_build_query($query));
    }
}