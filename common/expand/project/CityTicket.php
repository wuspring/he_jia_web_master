<?php
namespace DL\Project;

use common\models\Goods;
use common\models\GoodsCoupon;
use common\models\Member,
    common\models\AccountLog;
use common\models\Provinces;
use common\models\StoreInfo;
use common\models\StoreShop;
use common\models\Ticket;
use common\models\TicketApply;
use common\models\TicketApplyGoods;
use common\models\User;

class CityTicket
{
    protected $provincesId;

    const TYPE_JIE_HUN = Ticket::TYPE_JIE_HUN;
    const TYPE_JIA_ZHUANG = Ticket::TYPE_JIA_ZHUANG;
    const TYPE_YUN_YING = Ticket::TYPE_YUN_YING;

    const CA_TYPE_CAI_GOU = Ticket::CA_TYPE_CAI_GOU;    // 采购类索票
    const CA_TYPE_ZHAN_HUI = Ticket::CA_TYPE_ZHAN_HUI;  // 展会类索票

    public function __construct($cityId)
    {
        $provinces = Provinces::findOne($cityId);

        if (!$provinces) {
            throw new \Exception('城市信息有误');
        }
        $this->provinces = $provinces;
    }

    public static function init($cityId)
    {
        return new self($cityId);
    }

    public function getTicket($type, $caType=Ticket::CA_TYPE_ZHAN_HUI)
    {
        $ticket = Ticket::findOne([
            'type' => $type,
            'ca_type' => $caType,
            'status' => 1,
            'citys' => $this->provinces->id
        ]);

        return $ticket ? : false;
    }

    /**
     * @param $type
     * @param string $caType
     * @return array
     */
    public function currentTickets($type=[], $caType=Ticket::CA_TYPE_ZHAN_HUI)
    {
        $ticketInfo = $type ? $type : [
            Ticket::TYPE_JIA_ZHUANG,
            Ticket::TYPE_JIE_HUN,
            Ticket::TYPE_YUN_YING,
        ];
        is_array($ticketInfo) or $ticketInfo = [$ticketInfo];

        $tickets = [];
        foreach ($ticketInfo AS $info) {
            $tickets[$info] = $this->getTicket($info, $caType);
        }

        return $tickets;
    }

    /**
     * 获取当前城市发行票据
     *
     * @return array
     */
    public function currentFactionTickets()
    {
        $tickets = $this->currentTickets();

        $ticketData = [];
        arrayGroupsAction($tickets, function ($ticket) use (&$ticketData) {
            if ($ticket) {
                 $ticketData[$ticket->id] = $ticket;
            }
        });
        return $ticketData;
    }

    /**
     * 获取当前城市所有参展商品
     * @param int $userId
     */
    public function currentGoodsIds($userId=0)
    {
        $tickets = CityTicket::init($this->provinces->id)->currentTickets();
        $ticketIds = arrayGroupsAction($tickets, function ($ticket) {
            return $ticket ? $ticket->id : '';
        });
        $ticketIds = array_filter($ticketIds, function ($id) {
            return strlen($id);
        });
        $ticketIds or $ticketIds =[0];
        $baseFilter = [
            'and',
            ['in', 'ticket_id', $ticketIds],
            ['=', 'status', 1],
        ];
        $userId and $baseFilter = array_merge($baseFilter, [['=', 'user_id', $userId]]);
        $goodDatas = TicketApply::find()->where($baseFilter)->all();
        $userIds = arrayGroupsAction($goodDatas, function ($goodsApply) {
            return $goodsApply->user_id;
        });
        $userIds or $userIds =[0];

//        // 用户参展商品
        $goodIds = [];
        $goods = TicketApplyGoods::find()->where([
            'and',
            ['in', 'ticket_id', $ticketIds],
            ['in', 'user_id', $userIds],
        ])->all();
        if ($goods) {
            $goodIds = arrayGroupsAction($goods, function ($goodsApply) {
                return $goodsApply->good_id;
            });
        }

        return $goodIds;
    }
}