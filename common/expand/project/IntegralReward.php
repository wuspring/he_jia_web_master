<?php
namespace DL\Project;
use DL\vendor\ConfigService;

/**
 * Class Integral Reward
 * 计算返还积分值
 *
 * @package DL\Project
 * @author  daorli
 */
class IntegralReward
{
    /**
     * 充值积分
     *
     * @param number $money 充值金额
     *
     * @return number
     */
    public static function charge($money)
    {
        return ConfigService::init(ConfigService::INTEGRAL)->get('charge_integral');
    }

    /**
     * 购物返积分
     *
     * @param number $money 购物金额
     *
     * @return int
     */
    public static function buyGoods($money)
    {
        $type = ConfigService::init(ConfigService::INTEGRAL)->get('buy_goods_type');
        $param = ConfigService::init(ConfigService::INTEGRAL)->get('buy_goods_integral');
        $type or $type = 1;
        switch ($type) {
            case 1 :    // 固定积分
                $score = $param;
                break;
            case 2 :    // 支付金额百分比
                $score =  intval(abs($money * $param / 100 ));
                break;
        }

        return $score;
    }

}