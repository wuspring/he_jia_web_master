<?php
namespace DL\Project;


use common\models\Config;
use common\models\Goods;
use common\models\Order,
    common\models\PaySnOrderRelation;
use common\models\SecondKillLog;

class SecondKill
{
    protected $limboTimeGroups;

    public function __construct()
    {
        $this->limboTimeGroups = [];
        $configModel = new Config();
        $configInfo = $configModel::find()->where([
            'cKey' => 'INDEX_EXPAND'
        ])->one();

        if ($configInfo) {
            $config = json_decode($configInfo->cValue);
            $times = explode(',', $config->times);
            in_array('00:00', $times) or $times = array_merge(['00:00'], $times);
            in_array('24:00', $times) or $times[] = '24:00';


            for ($i=0; $i < count($times)-1; $i++) {
                $this->limboTimeGroups[] = [trim($times[$i]), trim($times[$i + 1])];
            }
        }
    }

    public static function init()
    {
         return new self();
    }

    /**
     * 秒杀时段列表
     *
     * @return array
     */
    public function limbos()
    {
        return $this->limboTimeGroups;
    }

    /**
     * 获取当前时段秒杀列表
     *
     * @return array
     */
    public function nowLimbo()
    {
        $limbo = [];
        $index = $this->nowLimboIndex();

        if ($index !== false) {
            $limbo = $this->limboTimeGroups[$index];
        }

        return $limbo;
    }

    /**
     * 获取当前时段秒杀列表
     *
     * @return bool|mixed
     */
    public function nowLimboIndex()
    {
        $index = false;
        $nowTime = time();
        $date = date('Y-m-d', $nowTime);

        foreach ($this->limboTimeGroups AS $key => $limbo) {
            if (
                strtotime("{$date} {$limbo[0]}") <= $nowTime
                and strtotime("{$date} {$limbo[1]}") > $nowTime
            ) {
                $index = $key;
            }
        }

        return $index;
    }

    /**
     * check goods status
     *
     * @param $goodId
     *
     * @return bool
     * @throws \Exception
     */
    public function checkGoodsStatus($goodId, $limboIndex=false)
    {
        $good = Goods::findOne($goodId);

        if (!$good) {
            throw new\Exception("获取商品信息失败");
        }

        $groups = json_decode($good->second_kill_groups, true);

        $status = false;
        if ($groups) {
            if ($limboIndex === false) {
                $limboIndex = $this->nowLimboIndex();
            }

            if ($limboIndex !== false) {
                $status = in_array($limboIndex, $groups);
            }
        }
        return $status;
    }

    /**
     * rest amount
     *
     * @param $goodId
     *
     * @return int|mixed
     */
    public function restAmount($goodId)
    {
        $good = Goods::findOne($goodId);

        $index = $this->nowLimboIndex();
        if (!$good->is_second_kill || $index === false) {
            return 0;
        }

        $key = date('Ymd') . "-{$good->id}-{$index}";
        $log = SecondKillLog::findOne([
            'key' => $key
        ]);

        if (!$log) {
            $log = new SecondKillLog();
            $log->key = $key;
            $log->amount = 0;
            $log->save();
        }

        return ($good->second_limit - $log->amount > 0) ? ($good->second_limit - $log->amount) : 0;
    }

    /**
     * get now limbo
     *
     * @return array
     */
    public function getNowLimbo()
    {
        $limboIndex = $this->nowLimboIndex();

        return [
            $limboIndex => $this->limboTimeGroups[$limboIndex]
        ];
    }
}