<?php
namespace DL\Project;

use common\models\ConfigAskTicket;
use DL\Base\Config;
use DL\service\CacheService;

class CityExpand extends Config
{
    const TYPE_CITY_EXPAND = 'TYPE_CITY_EXPAND';

    public function __construct()
    {
        $this->dataConstruct = [
            'tel' => '',
        ];
    }

    public static function init()
    {
        return new self();
    }
}