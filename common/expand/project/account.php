<?php
namespace DL\Project;

use common\models\Member,
    common\models\AccountLog;

class Account
{
    public $model;
    public $accountLogModel;

    public static function member()
    {
        return new self(new Member());
    }

    public function __construct($model)
    {
        $this->model = $model;
        $this->accountLogModel = new AccountLog();
    }

}