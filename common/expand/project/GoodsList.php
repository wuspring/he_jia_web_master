<?php
namespace DL\Project;


use common\models\Goods;
use common\models\GoodsClass;
use common\models\History;
use yii\helpers\ArrayHelper;

class GoodsList
{
    public static function init()
    {
        return new self();
    }
    /**
     * 随机推荐
     * @param int $amount
     * @return array|\yii\db\ActiveRecord[]
     */
    public function rank($amount=10)
    {
        $rank = [
            ['goods_salenum' => 'desc'],
            ['id' => 'desc'],
            ['goods_collect' => 'desc'],
            ['goods_price' => 'asc'],
        ];


        return $this->_getGoods([], $rank[rand(0, count($rank)-1)], $amount);
    }

    /**
     * 历史记录
     * @param $memberId
     * @param int $limit
     * @return array
     */
    public function history($memberId, $limit=8)
    {
        $historys = History::find()->where([
            'member_id' => $memberId,
        ])->orderBy('create_time desc')->limit($limit)->all();

        return arrayGroupsAction($historys, function ($history) {
                $goods = $history->goods;

                if ($goods->goods_state == 1 and $goods->isdelete == 0) {
                    return $goods;
                }
        });
    }

    /**
     * 精准推荐
     * @param $memberId
     * @param int $limit
     * @return array|\yii\db\ActiveRecord[]
     */
    public function aimRecommend($memberId, $limit=8)
    {
        $recommends = Goods::findBySql("select * from `goods` as `ng` inner join (select `g`.`gc_id` from `goods` as `g` inner join `history` as `h` on `h`.`goods_id`=`g`.`id` where `h`.`member_id`='{$memberId}' and `g`.`goods_state`=1 and `g`.`isdelete`=0 order by `h`.`create_time` desc limit 0, 15) as `gc` on `gc`.`gc_id` =`ng`.`gc_id` where `ng`.`type` in (1, 2) order by `ng`.`id` desc limit 0, {$limit};")->all();

        return $recommends ? : $this->rank($limit);
    }

    /**
     *
     * @param $categoryId
     * @param int $limit
     */
    public function categoryGoods($categoryId, $order=[], $limit=8)
    {
        $goodClass = new GoodsClass();
        $categorys = $goodClass->getSonCategoryByDG($categoryId);
        $categoryIds = ArrayHelper::getColumn($categorys, 'id');
        $categoryIds = array_merge($categoryIds, [$categoryId]);

        $orderBy = 'id desc';
        if ($order) {
            $cacheOrder = [];
            foreach ($order AS $field => $rule) {
                $cacheOrder[] = "{$field} {$rule}";
            }

            $orderBy = implode(',', $cacheOrder);
        }

        return Goods::find()->where([
            'and',
            ['in', 'gc_id', $categoryIds],
            ['=', 'type', 1],
            ['=', 'isdelete', 0],
        ])->orderBy($orderBy)->limit($limit)->all();
    }

    private function _getGoods($filter=[], $order=[], $limit=10)
    {
        $filter = array_merge($filter, [
            'type' => 1,
            'isdelete' => 0
        ]);


        $newFilter = ['and'];
        foreach ($filter AS $key => $val) {
            $newFilter[] = ['=', $key, $val];
        }
        $newFilter[] = ['in', 'type', [1, 2]];

        $orderBy = 'id desc';
        if ($order) {
            $cacheOrder = [];
            foreach ($order AS $field => $rule) {
                $cacheOrder[] = "{$field} {$rule}";
            }

            $orderBy = implode(',', $cacheOrder);
        }

        return Goods::find()->where($newFilter)
            ->orderBy($orderBy)->limit($limit)->all();
    }
}