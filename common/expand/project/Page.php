<?php
namespace DL\Project;

use common\models\ConfigAskTicket,
    common\models\Member,
    common\models\AccountLog;
use DL\Base\Config;
use DL\service\CacheService;
use yii\base\Object;

class Page extends Config
{
    const PAGE_INDEX = 'PAGE_INDEX';    // 首页
    const PAGE_CAI_GOU_JIAZHUANG = 'PAGE_CAI_GOU_JIAZHUANG';    // 家装
    const PAGE_CAI_GOU_JIEHUN = 'PAGE_CAI_GOU_JIEHUN';          // 结婚
    const PAGE_CAI_GOU_YUNYING = 'PAGE_CAI_GOU_YUNYING';        // 孕婴

    public function __construct()
    {
        $this->dataConstruct = [
            'banner' => ['@/public/index/images/_temp/skin/banner_jia.jpg'],
            'expand' => [
                'huodong_1' => '@',
                'huodong_2' => '@',
                'huodong_3' => '@',
                'huodong_4' => '@',
            ],
            'huodongs' => [
                'huodong_1' => '@/public/index/images/_temp/index_ad01.jpg',
                'huodong_2' => '@/public/index/images/_temp/index_ad02.jpg',
                'huodong_3' => '@/public/index/images/_temp/index_ad03.jpg',
                'huodong_4' => '@/public/index/images/_temp/index_ad04.jpg',
                'huodong_5' => '@',
                'huodong_6' => '@',
                'huodong_7' => '@',
                'huodong_8' => '@',
            ],
        ];
    }

    public static function init()
    {
        return new self();
    }

    public function dictionarys()
    {
        return [
            Page::PAGE_INDEX => '首页',
            Page::PAGE_CAI_GOU_JIAZHUANG => '家装',
            Page::PAGE_CAI_GOU_JIEHUN => '结婚',
            Page::PAGE_CAI_GOU_YUNYING => '孕婴',
        ];
    }

    public function info($index)
    {
        $dictionarys = $this->dictionarys();

        return isset($dictionarys[$index]) ? $dictionarys[$index] : '-';
    }
}