<?php
namespace DL\Project;


use common\models\Config;
use common\models\Member;
use common\models\MemberLevel AS MemberLevelModel;
use common\models\Order,
    common\models\PaySnOrderRelation;
use common\models\SecondKillLog;

class MemberLevel
{
    protected $levelInfo = [];

    protected $memberInfo;

    public function __construct($memberId)
    {
        $this->memberInfo = Member::findOne($memberId);

        $this->levelInfo = [];
        $memberLevelModel = new MemberLevelModel();
        $levels = $memberLevelModel::find()->where([])->all();

        if ($levels) {
            $specialInfo = true;
            $rank = (int)$this->memberInfo->rank;
            for ($i=count($levels)-1; $i >=0 ; $i--) {
                if ($rank > $levels[$i]->score) {


                    $topLimbo = isset($levels[$i+1]) ? $levels[$i+1]->score : '-';

                    $this->levelInfo= [
                        'name' => $levels[$i]->name,
                        'sale' => $levels[$i]->sale,
                        'leftLimbo' => $levels[$i]->score,
                        'rightLimbo' => $topLimbo,
                        'rank' => $this->memberInfo->rank,
                        'rankPercent' => intval($this->memberInfo->rank * 100 / $levels[0]->score)
                    ];
                    $specialInfo = false;
                    break;
                }
            }

            if ($specialInfo) {
                $this->levelInfo= [
                    'name' => '普通会员',
                    'sale' => 1,
                    'leftLimbo' => 0,
                    'rightLimbo' => $levels[0]->score,
                    'rank' => $this->memberInfo->rank,
                    'rankPercent' => intval($this->memberInfo->rank * 100 / $levels[0]->score)
                ];
            }
        }
    }

    public static function init($memberId)
    {
         return new self($memberId);
    }

    /**
     * 会员等级列表
     *
     * @return array
     */
    public function info()
    {
        if (!$this->levelInfo) {
            $this->levelInfo = [
                'name' => '普通会员',
                'sale' => 1,
                'leftLimbo' => 0,
                'rightLimbo' => '-',
                'rank' => $this->memberInfo->rank,
                'rankPercent' => 0
            ];
        }

        return (object)$this->levelInfo;
    }

    /**
     * 会员价格计算
     *
     * @return
     */
    public function countPrice($price)
    {
        $info = $this->info();
        return round($price * $info->sale, 2);
    }

}