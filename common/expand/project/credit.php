<?php

namespace DL\Project;

/**
 * Class Credit
 * 积分计算规则
 *
 * @package expand
 * @author  daorli
 */
class Credit
{
    /**
     * 餐饮购买积分计算
     * @param $payMoney
     * @return int
     */
    public static function cyOrder($payMoney)
    {
        return (int)$payMoney;
    }

    public static function scOrder($payMoney)
    {
        return (int)$payMoney;
    }
}