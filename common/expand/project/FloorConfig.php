<?php
namespace DL\Project;

use common\models\FloorIndex;
use common\models\Goods;
use common\models\GoodsClass;
use common\models\GoodsCoupon;
use common\models\Member,
    common\models\AccountLog;
use common\models\StoreGcScore;
use common\models\StoreInfo;
use common\models\StoreShop;
use common\models\User;
use DL\service\CacheService;
use yii\helpers\ArrayHelper;

class FloorConfig
{
    private $_imgConfig;
    private $_zhImgConfig;

    private $_brandConfig;
    public static function init()
    {
        $self = new self();
        $self->_imgConfig = [
            'pic_1' => '@',
            'pic_2' => '@',
            'pic_3' => '@',
            'pic_4' => '@',
            'pic_5' => '@',
        ];
        $self->_zhImgConfig = [
//            'pic_1' => '@',
            'pic_2' => '@',
            'pic_3' => '@',
            'pic_4' => '@',
            'pic_5' => '@',
        ];

        $self->_brandConfig = [];

        return $self;
    }

    /**
     * 获取城市指定栏目配置
     *
     * @param $cityId
     * @param $cid
     *
     * @return object
     */
    public function get($cityId, $cid)
    {
        $model = FloorIndex::findOne([
            'good_class_id' => $cid,
            'provinces_id' => $cityId
        ]);
        if (!$model) {
            $model = new FloorIndex();
            $model->good_class_id = $cid;
            $model->provinces_id = $cityId;
            $model->img = json_encode($this->_imgConfig);
            $model->zh_img = json_encode($this->_zhImgConfig);
            $model->brand = json_encode($this->_brandConfig);
            $model->save();
        }
        $model->img = arrayGroupsAction(json_decode($model->img, true), function ($val) {
            strpos($val, '@') > -1 or $val .= '@';
            list($href, $img) = explode('@', $val);
            return [
                'src' => $img,
                'href' => $href
            ];
        }, true);
        $model->zh_img = arrayGroupsAction(json_decode($model->zh_img, true), function ($val) {
            strpos($val, '@') > -1 or $val .= '@';
            list($href, $img) = explode('@', $val);
            return [
                'src' => strlen($img) ? explode(',', $img) : [],
                'href' => $href
            ];
        }, true);

        $model->zh_first_imgs = arrayGroupsAction(json_decode($model->zh_first_imgs, true), function ($val) {
            strpos($val, '@') > -1 or $val .= '@';
            list($href, $img) = explode('@', $val);
            return [
                'src' => strlen($img) ? $img : '',
                'href' => $href
            ];
        }, false);

//        $model->zh_first_imgs = strlen($model->zh_first_imgs) ? explode(',', (string)$model->zh_first_imgs) : [];
        $brands = array_intersect(json_decode($model->brand, true), Store::init()->usefulStoreIds());
//        $model->brand = arrayGroupsAction($brands, function ($val) {
//            return Store::init()->info($val);
//        });
        $commendStores = StoreGcScore::find()->where([
            'and',
            ['=', 'provinces_id', $cityId],
            ['=', 'gc_id', $cid],
        ])->select('*, `score` * `admin_score` as `a_score`')->orderBy('a_score desc')->all();
       $model->brand = arrayGroupsAction($commendStores, function ($val) {
            return Store::init()->info($val->user_id);
        });

        return $model;
    }


    public function city($cityId)
    {
        $goodClasses = GoodsClass::find()->where([
            'and',
            ['<>', 'fid', 0],
            ['=', 'is_del', 0]
        ])->all();

        $goodClasses = ArrayHelper::index($goodClasses, 'id');

        return arrayGroupsAction($goodClasses, function ($goodClass) use ($cityId) {
            return $this->get($cityId, $goodClass->id);
        }, true);
    }
}