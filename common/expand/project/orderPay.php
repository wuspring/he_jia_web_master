<?php
namespace DL\Project;



use common\models\Order,
    common\models\PaySnOrderRelation;


class OrderPay
{
    /**
     * 支付订单组
     *
     * @param string $memberId member id
     * @param array  $orderIds order ids
     *
     * @return array
     * @throws \Exception
     */
    public static function payGroups($memberId, $orderIds=[], $payMethod=Order::PAY_MECHOD_WECHAT)
    {
        if (empty($orderIds)) {
            throw new \Exception("支付订单不可为空");
        }

        $orders = Order::find()->where([
            'and',
            ['in', 'orderid', $orderIds],
            ['=', 'order_state', Order::STATUS_WAIT]
        ])->all();

        $getOrderIds = arrayGroupsAction($orders, function ($order) {
            return $order->orderid;
        });

        $diff = array_diff($orderIds, $getOrderIds);
        if ($diff) {
            throw new \Exception('未查询到一下订单信息[' . implode(',', $diff) . ']');
        }

        $payTotalPrice = Order::find()->where([
            'and',
            ['in', 'orderid', $orderIds],
            ['=', 'order_state', Order::STATUS_WAIT]
        ])->sum('pay_amount');

        $paySn = self::createPaySn();
        foreach ($orders AS $order) {
            $order->pay_sn = $paySn;
            $order->pay_method = $payMethod;
            $order->save();

            $paySnOrderRelation = new PaySnOrderRelation();
            $paySnOrderRelation->pay_sn = $order->pay_sn;
            $paySnOrderRelation->orderid = $order->orderid;
            $paySnOrderRelation->member_id = $memberId;
            $paySnOrderRelation->status = 0;
            $paySnOrderRelation->pay_amount = $payTotalPrice;
            $paySnOrderRelation->create_time = date('Y-m-d');
            $paySnOrderRelation->type = $paySnOrderRelation::ORDER;
            $paySnOrderRelation->save();
        }

        return [
            'paySn' => $paySn,
            'totalPayPrice' => $payTotalPrice
        ];
    }

    protected static function createPaySn()
    {
        $orderPaySn = 'sn' . date('Ymdhis') . createRandKey(4);

        $paySn = PaySnOrderRelation::findOne([
            'pay_sn' => $orderPaySn
        ]);

        return $paySn ? self::createPaySn() : $orderPaySn;
    }

}