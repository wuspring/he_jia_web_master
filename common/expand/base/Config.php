<?php
namespace DL\Base;

use common\models\ConfigAskTicket;
use DL\service\CacheService;

abstract class Config
{
    protected $config;
    protected $dataConstruct;

    public function __construct()
    {
        $this->dataConstruct = [
            'banner' => '',
            'index_top' => '',
            'index_footer' => ''
        ];
    }

    /**
     * get
     *
     * @param int $cityId
     * @param int $ticketType
     * @param bool $isCache
     * @param bool $getData
     *
     * @return AskTicket|mixed
     */
    public function get($cityId, $ticketType, $isCache=true, $getData=true)
    {
        if ($isCache) {
            $key = $this->secretKey($cityId, $ticketType);
            $this->config = CacheService::init()->get($key);
        }

        if (!$this->config) {
            $this->config = ConfigAskTicket::findOne([
                'provinces_id' => $cityId,
                'type' => $ticketType
            ]);

            $this->config  = $this->config ?: $this->createInitCfg($cityId, $ticketType);
        }

        return $getData ? json_decode($this->config->data) : $this;
    }

    /**
     * set data
     *
     * @param \stdClass $data
     *
     * @return mixed
     */
    public function setData(\stdClass $data)
    {
        $key = $this->secretKey($this->config->provinces_id, $this->config->type);
        CacheService::init()->delete($key);

        $this->config->data = json_encode($data);
        return $this->config->save();
    }

    /**
     * create init config
     *
     * @param $cityId
     * @param $ticketType
     *
     * @return ConfigAskTicket
     */
    protected function createInitCfg($cityId, $ticketType)
    {
        $config = new ConfigAskTicket();
        $config->provinces_id = $cityId;
        $config->type = $ticketType;
        $config->data = json_encode($this->dataConstruct);
        $config->save();

        return $config;
    }

    private function secretKey($cityId, $ticketType)
    {
        return "INDEX_AT_{$cityId}_{$ticketType}";
    }

}