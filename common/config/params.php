<?php 
return array (
  'adminEmail' => 'admin@example.com',
  'supportEmail' => 'support@example.com',
  'user.passwordResetTokenExpire' => 3600,
  'imageDirectoryBasePath' => '/wamp/www/yii2',
  'system' => 
  array (
    'name' => '',
    'urladdress' => '',
    'keywords' => '',
    'description' => '',
    'telephone' => '',
    'companyAddress' => '',
    'icp' => '',
    'notice' => '',
    'company' => '',
    'integral' => '1',
    'discount' => '100',
    'logo' => '/uploads/config/logo/201812131451184147558338.jpg',
    'erweima' => '/uploads/config/erweima/201812121817244318190610.png',
    'qq' => '',
    'order_rank' => '2',
    'waplogo' => '/uploads/config/waplogo/201712010248160093972471.png',
  ),
  'weixin' => 
  array (
    'name' => '',
    'wxid' => '',
    'wxnumber' => '',
    'appid' => '',
    'appsecret' => '',
    'token' => 'weixin',
    'aesencodingkey' => '',
    'notifyUrl' => '*****/notify.php',
  ),
);