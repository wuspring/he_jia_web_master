<?php
/**
 * author daorli
 */

defined('ROOT_PATH') or define('ROOT_PATH', realpath(__DIR__ . '/../../'));
defined('COMMON_PATH') or define('COMMON_PATH', ROOT_PATH . '/common');
defined('EXPAND_PATH') or define('EXPAND_PATH', COMMON_PATH . '/expand');

// define notice level
if (defined('DL_MASTER')) {
    error_reporting(E_ERROR);
}

// base system
include EXPAND_PATH . '/functions.php';
include EXPAND_PATH . '/loader.php';

// expand
$requirePath = [
    EXPAND_PATH . '/config',
    EXPAND_PATH . '/base',
    EXPAND_PATH . '/project',
    EXPAND_PATH . '/service',
    EXPAND_PATH . '/vendor'
];
// include frame base file
\DL\Load::dirs($requirePath);

// build force dir
$forceBuildDirs = [
    RUNTIME_PATH,
    RUNTIME_PATH . '/session',
    RUNTIME_PATH . '/logs',
];
foreach ($forceBuildDirs AS $dir) {
    is_dir($dir) or mkdir($dir, 777, true);
}

// required Yii module
$modules = scandir(APP_BASE_PATH);
foreach ($modules AS $module) {
    if (in_array($module, ['.', '..'])) {
        continue;
    }

    Yii::setAlias($module, APP_BASE_PATH . "/{$module}");
}
// set runtime path
Yii::setAlias('runtime', RUNTIME_PATH);
