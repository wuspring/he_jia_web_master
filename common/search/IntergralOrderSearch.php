<?php

namespace common\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\IntergralOrder;

/**
 * IntergralOrderSearch represents the model behind the search form about `common\models\IntergralOrder`.
 */
class IntergralOrderSearch extends IntergralOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderid', 'user_id', 'buyer_id', 'integral_amount', 'evaluation_state', 'order_state', 'receiver', 'shipping', 'fallinto_state', 'coupon_id', 'reback_status', 'ticket_id', 'relation_coupon'], 'integer'],
            [['pay_sn', 'buyer_name', 'receiver_name', 'receiver_state', 'receiver_address', 'receiver_mobile', 'receiver_zip', 'shipping_code', 'deliveryTime', 'add_time', 'payment_time', 'finnshed_time', 'pay_method', 'type', 'ticket_type'], 'safe'],
            [['goods_amount', 'order_amount', 'pd_amount', 'freight', 'pay_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IntergralOrder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'orderid' => $this->orderid,
            'user_id' => $this->user_id,
            'buyer_id' => $this->buyer_id,
            'goods_amount' => $this->goods_amount,
            'order_amount' => $this->order_amount,
            'integral_amount' => $this->integral_amount,
            'pd_amount' => $this->pd_amount,
            'freight' => $this->freight,
            'evaluation_state' => $this->evaluation_state,
            'order_state' => $this->order_state,
            'receiver' => $this->receiver,
            'shipping' => $this->shipping,
            'deliveryTime' => $this->deliveryTime,
            'add_time' => $this->add_time,
            'payment_time' => $this->payment_time,
            'finnshed_time' => $this->finnshed_time,
            'fallinto_state' => $this->fallinto_state,
            'coupon_id' => $this->coupon_id,
            'reback_status' => $this->reback_status,
            'ticket_id' => $this->ticket_id,
            'relation_coupon' => $this->relation_coupon,
            'pay_amount' => $this->pay_amount,
        ]);

        $query->andFilterWhere(['like', 'pay_sn', $this->pay_sn])
            ->andFilterWhere(['like', 'buyer_name', $this->buyer_name])
            ->andFilterWhere(['like', 'receiver_name', $this->receiver_name])
            ->andFilterWhere(['like', 'receiver_state', $this->receiver_state])
            ->andFilterWhere(['like', 'receiver_address', $this->receiver_address])
            ->andFilterWhere(['like', 'receiver_mobile', $this->receiver_mobile])
            ->andFilterWhere(['like', 'receiver_zip', $this->receiver_zip])
            ->andFilterWhere(['like', 'shipping_code', $this->shipping_code])
            ->andFilterWhere(['like', 'pay_method', $this->pay_method])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'ticket_type', $this->ticket_type]);

        return $dataProvider;
    }
}
