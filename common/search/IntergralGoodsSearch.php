<?php

namespace common\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\IntergralGoods;

/**
 * IntergralGoodsSearch represents the model behind the search form about `common\models\IntergralGoods`.
 */
class IntergralGoodsSearch extends IntergralGoods
{
    public $inTop;
    public $inBotton;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gc_id', 'use_attr', 'goods_storage_alarm', 'goods_click', 'goods_salenum', 'goods_collect', 'goods_storage', 'goods_state', 'goods_vat', 'evaluation_count', 'is_virtual', 'is_appoint', 'is_presell', 'have_gift', 'isdelete', 'is_second_kill', 'goods_hot'], 'integer'],
            [['goods_name', 'goods_jingle', 'goods_serial', 'attr', 'goods_spec', 'goods_body', 'mobile_body', 'goods_pic', 'goods_image', 'goods_addtime', 'goods_edittime', 'inTop', 'inBotton'], 'safe'],
            [['goods_integral', 'goods_marketprice', 'evaluation_good_star', 'goods_weight'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IntergralGoods::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'gc_id' => $this->gc_id,
            'use_attr' => $this->use_attr,
            'goods_integral' => $this->goods_integral,
            'goods_marketprice' => $this->goods_marketprice,
            'goods_storage_alarm' => $this->goods_storage_alarm,
            'goods_click' => $this->goods_click,
            'goods_salenum' => $this->goods_salenum,
            'goods_collect' => $this->goods_collect,
            'goods_storage' => $this->goods_storage,
            'goods_state' => $this->goods_state,
            'goods_addtime' => $this->goods_addtime,
            'goods_edittime' => $this->goods_edittime,
            'goods_vat' => $this->goods_vat,
            'evaluation_good_star' => $this->evaluation_good_star,
            'evaluation_count' => $this->evaluation_count,
            'is_virtual' => $this->is_virtual,
            'is_appoint' => $this->is_appoint,
            'is_presell' => $this->is_presell,
            'have_gift' => $this->have_gift,
            'isdelete' => $this->isdelete,
            'goods_weight' => $this->goods_weight,
            'is_second_kill' => $this->is_second_kill,
            'goods_hot' => $this->goods_hot,
        ]);

        $query->andFilterWhere(['like', 'goods_name', $this->goods_name])
            ->andFilterWhere(['like', 'goods_jingle', $this->goods_jingle])
            ->andFilterWhere(['like', 'goods_serial', $this->goods_serial])
            ->andFilterWhere(['like', 'attr', $this->attr])
            ->andFilterWhere(['like', 'goods_spec', $this->goods_spec])
            ->andFilterWhere(['like', 'goods_body', $this->goods_body])
            ->andFilterWhere(['like', 'mobile_body', $this->mobile_body])
            ->andFilterWhere(['like', 'goods_pic', $this->goods_pic])
            ->andFilterWhere(['like', 'goods_image', $this->goods_image]);

        $this->inTop and $query->andFilterWhere(['<=', 'goods_integral', $this->inTop]);
        $this->inBotton and $query->andFilterWhere(['>=', 'goods_integral', $this->inBotton]);

        return $dataProvider;
    }
}
